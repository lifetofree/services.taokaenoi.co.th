using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_elearning")]
public class data_elearning
{
    public int return_code { get; set; }
    public string return_msg { get; set; }
    //master
    public m0_training[] el_m0_training_action { get; set; }
    public m0_training_type[] el_m0_training_type_action { get; set; }
    public m0_training_group[] el_m0_training_group_action { get; set; }
    public m0_training_branch[] el_m0_training_branch_action { get; set; }
    public m0_training_organ[] el_m0_training_organ_action { get; set; }
    public m0_runno[] el_m0_runno_action { get; set; }
    public m0_sche_trn_day[] el_m0_sche_trn_day_action { get; set; }
    public m0_email[] el_m0_email_action { get; set; }
    public m0_level[] el_m0_level_action { get; set; }
    public m0_evaluation_group[] el_m0_evaluation_group_action { get; set; }
    public m0_evaluation_form[] el_m0_evaluation_form_action { get; set; }
    public m0_target_group[] el_m0_target_group_action { get; set; }
    public m0_institution[] el_m0_institution_action { get; set; }
    public m0_lecturer[] el_m0_lecturer_action { get; set; }
    public m0_objective[] el_m0_objective_action { get; set; }

    public m0_meeting[] el_m0_meeting_action { get; set; }

    public m0_iso[] el_m0_iso_action { get; set; }

    //transection
    public traning_req[] el_traning_req_action { get; set; }
    public course[] el_course_action { get; set; }
    public training_plan[] el_training_plan_action { get; set; }
    public u_manage[] el_u_manage_action { get; set; }

    [XmlElement("el_training_course_action")]
    public training_course[] el_training_course_action { get; set; }

    //Report
    public training_rpt_plan[] el_report_plan_action { get; set; }


    //Lookup
    public employeeM[] employeeM_action { get; set; }
    public trainingLoolup[] trainingLoolup_action { get; set; }

    [XmlElement("trainingLoolup_Xml_action")]
    public trainingLoolup_Xml[] trainingLoolup_Xml_action { get; set; }
    [XmlElement("el_traning_req_Xml_action")]
    public traning_req_Xml[] el_traning_req_Xml_action { get; set; }


    // set u0/u1/u2/u3/u4
    [XmlElement("el_u0_course_list")]
    public el_u0_course_detail[] el_u0_course_list { get; set; }

    [XmlElement("el_u1_course_list")]
    public el_u1_course_detail[] el_u1_course_list { get; set; }

    [XmlElement("m0_course_cost_action")]
    public m0_course_cost[] m0_course_cost_action { get; set; }
    public U0_QuizDocument[] Boxu0_QuizDocument { get; set; }
    public M0_QuizDocument[] Boxm0_QuizDocument { get; set; }
    public M1_QuizDocument[] Boxm1_QuizDocument { get; set; }

    [XmlElement("m0_expert_salary_action")]
    public m0_expert_salary[] m0_expert_salary_action { get; set; }
    [XmlElement("m0_training_location_action")]
    public m0_training_location[] m0_training_location_action { get; set; }
    [XmlElement("el_u4_course_list")]
    public el_u4_course_detail[] el_u4_course_list { get; set; }

    [XmlElement("el_u5_course_list")]
    public el_u5_course_detail[] el_u5_course_list { get; set; }

    [XmlElement("Boxm1_ImportCost")]
    public M1_ImportCost[] Boxm1_ImportCost { get; set; }

    // set u0/u1/u2/u3/u4

}

// set u0/u1/u2/u3/u4
[Serializable]
public class el_u0_course_detail
{

    [XmlElement("course_date_month")]
    public int course_date_month { get; set; }
    [XmlElement("course_date_year")]
    public int course_date_year { get; set; }


    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }

    //el_u0_course
    [XmlElement("u0_course_idx")]
    public int u0_course_idx { get; set; }
    [XmlElement("course_no")]
    public string course_no { get; set; }
    [XmlElement("course_date")]
    public string course_date { get; set; }
    [XmlElement("course_name")]
    public string course_name { get; set; }
    [XmlElement("course_remark")]
    public string course_remark { get; set; }
    [XmlElement("course_type_etraining")]
    public int course_type_etraining { get; set; }
    [XmlElement("course_type_elearning")]
    public int course_type_elearning { get; set; }
    [XmlElement("course_priority")]
    public int course_priority { get; set; }
    [XmlElement("m0_training_group_idx_ref")]
    public int m0_training_group_idx_ref { get; set; }
    [XmlElement("m0_level_idx_ref")]
    public int m0_level_idx_ref { get; set; }
    [XmlElement("course_score")]
    public int course_score { get; set; }
    [XmlElement("level_code")]
    public int level_code { get; set; }
    [XmlElement("approve_status")]
    public int approve_status { get; set; }
    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }
    [XmlElement("node_idx")]
    public int node_idx { get; set; }
    [XmlElement("actor_idx")]
    public int actor_idx { get; set; }
    [XmlElement("approve_remark")]
    public string approve_remark { get; set; }
    [XmlElement("app_flag")]
    public int app_flag { get; set; }
    [XmlElement("app_user")]
    public int app_user { get; set; }
    [XmlElement("app_date")]
    public string app_date { get; set; }
    [XmlElement("TIDX_ref")]
    public int TIDX_ref { get; set; }
    [XmlElement("course_plan_status")]
    public string course_plan_status { get; set; }
    [XmlElement("m0_training_branch_idx_ref")]
    public int m0_training_branch_idx_ref { get; set; }
    [XmlElement("trn_plan_flag")]
    public int trn_plan_flag { get; set; }
    [XmlElement("course_assessment")]
    public int course_assessment { get; set; }
    [XmlElement("io_page_flag")]
    public string io_page_flag { get; set; }

    // All
    [XmlElement("u0_course_idx_ref")]
    public int u0_course_idx_ref { get; set; }
    [XmlElement("course_status")]
    public int course_status { get; set; }
    [XmlElement("course_created_by")]
    public int course_created_by { get; set; }
    [XmlElement("course_created_at")]
    public string course_created_at { get; set; }
    [XmlElement("course_updated_by")]
    public int course_updated_by { get; set; }
    [XmlElement("course_updated_at")]
    public string course_updated_at { get; set; }
    [XmlElement("zvideo_item")]
    public int zvideo_item { get; set; }

    //u6
    [XmlElement("u6_course_idx")]
    public int u6_course_idx { get; set; }
    [XmlElement("video_title")]
    public string video_title { get; set; }
    [XmlElement("video_description")]
    public string video_description { get; set; }
    [XmlElement("video_item")]
    public int video_item { get; set; }
    [XmlElement("score_through_per")]
    public decimal score_through_per { get; set; }
    [XmlElement("video_name")]
    public string video_name { get; set; }
    [XmlElement("video_images")]
    public string video_images { get; set; }

    //file Lookup
    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("training_group_name")]
    public string training_group_name { get; set; }

    [XmlElement("training_branch_name")]
    public string training_branch_name { get; set; }

    [XmlElement("u2_course_test_item")]
    public int u2_course_test_item { get; set; }

    [XmlElement("u3_course_evaluation_item")]
    public int u3_course_evaluation_item { get; set; }

    [XmlElement("target_name")]
    public string target_name { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("evaluation_group_name_th")]
    public string evaluation_group_name_th { get; set; }

    // Filter
    [XmlElement("filter_keyword")]
    public string filter_keyword { get; set; }

    [XmlElement("JobLevel")]
    public int JobLevel { get; set; }
    [XmlElement("zmonth")]
    public int zmonth { get; set; }
    [XmlElement("zyear")]
    public int zyear { get; set; }

    //meeting room
    [XmlElement("m1_idx")]
    public int m1_idx { get; set; }
    [XmlElement("m0_meeting_idx")]
    public int m0_meeting_idx { get; set; }
    [XmlElement("meeting_code")]
    public int meeting_code { get; set; }
    [XmlElement("meeting_name")]
    public string meeting_name { get; set; }
    [XmlElement("meeting_remark")]
    public string meeting_remark { get; set; }
    [XmlElement("meeting_file_name")]
    public string meeting_file_name { get; set; }
    [XmlElement("place_idx_ref")]
    public int place_idx_ref { get; set; }
    [XmlElement("positon_status")]
    public int positon_status { get; set; }
    [XmlElement("meeting_status")]
    public int meeting_status { get; set; }
    [XmlElement("meeting_created_by")]
    public int meeting_created_by { get; set; }
    [XmlElement("meeting_updated_by")]
    public int meeting_updated_by { get; set; }

    [XmlElement("TIDX_ref_value")]
    public string TIDX_ref_value { get; set; }

    [XmlElement("u0_course_idx_ref_pass_value")]
    public string u0_course_idx_ref_pass_value { get; set; }

    [XmlElement("pos_name")]
    public string pos_name { get; set; }

    [XmlElement("org_idx_ref")]
    public int org_idx_ref { get; set; }
    [XmlElement("RDeptID_ref")]
    public int RDeptID_ref { get; set; }
    [XmlElement("RSecID_ref")]
    public int RSecID_ref { get; set; }
    [XmlElement("u1_course_idx")]
    public int u1_course_idx { get; set; }

    [XmlElement("u0_course_idx_u2")]
    public int u0_course_idx_u2 { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("u2_main_course_idx")]
    public int u2_main_course_idx { get; set; }


}

[Serializable]
public class el_u1_course_detail
{

    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }

    [XmlElement("TIDX_value")]
    public string TIDX_value { get; set; }

    [XmlElement("u1_course_idx")]
    public int u1_course_idx { get; set; }
    [XmlElement("RSecID_ref")]
    public int RSecID_ref { get; set; }
    [XmlElement("RDeptID_ref")]
    public int RDeptID_ref { get; set; }
    [XmlElement("RPosIDX_ref")]
    public int RPosIDX_ref { get; set; }
    [XmlElement("course_qty")]
    public int course_qty { get; set; }
    [XmlElement("org_idx_ref")]
    public int org_idx_ref { get; set; }
    [XmlElement("TIDX_ref")]
    public int TIDX_ref { get; set; }

    // All
    [XmlElement("u0_course_idx_ref")]
    public int u0_course_idx_ref { get; set; }
    [XmlElement("course_status")]
    public int course_status { get; set; }
    [XmlElement("course_created_by")]
    public int course_created_by { get; set; }
    [XmlElement("course_created_at")]
    public string course_created_at { get; set; }
    [XmlElement("course_updated_by")]
    public int course_updated_by { get; set; }
    [XmlElement("course_updated_at")]
    public string course_updated_at { get; set; }
    [XmlElement("zvideo_item")]
    public int zvideo_item { get; set; }


}

[Serializable]
public class el_u4_course_detail
{

    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }

    [XmlElement("u0_course_idx_ref_pass")]
    public int u0_course_idx_ref_pass { get; set; }

    [XmlElement("course_status")]
    public int course_status { get; set; }

    [XmlElement("course_updated_by")]
    public int course_updated_by { get; set; }


}

[Serializable]
public class el_u5_course_detail
{

    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }

    [XmlElement("TIDX_flag")]
    public int TIDX_flag { get; set; }

    [XmlElement("TIDX_ref")]
    public int TIDX_ref { get; set; }

    // All
    [XmlElement("u0_course_idx_ref")]
    public int u0_course_idx_ref { get; set; }
    [XmlElement("course_status")]
    public int course_status { get; set; }
    [XmlElement("course_created_by")]
    public int course_created_by { get; set; }
    [XmlElement("course_created_at")]
    public string course_created_at { get; set; }
    [XmlElement("course_updated_by")]
    public int course_updated_by { get; set; }
    [XmlElement("course_updated_at")]
    public string course_updated_at { get; set; }
    [XmlElement("zvideo_item")]
    public int zvideo_item { get; set; }

}


#region P'cake
//master 
public class m0_training
{
    public int m0_training_idx { get; set; }
    public string training_code { get; set; }
    public string training_name { get; set; }
    public string training_remark { get; set; }
    public int training_flag { get; set; }
    public int training_status { get; set; }
    public int training_created_by { get; set; }
    public int training_updated_by { get; set; }
    public string filter_keyword { get; set; }
}
public class m0_training_type
{
    public int m0_training_type_idx { get; set; }
    public string training_type_name { get; set; }
    public string training_type_remark { get; set; }
    public int training_type_status { get; set; }
    public int training_type_created_by { get; set; }
    public int training_type_updated_by { get; set; }
    public string filter_keyword { get; set; }
}
public class m0_training_group
{
    public int m0_training_group_idx { get; set; }
    public string training_group_code { get; set; }
    public string training_group_name { get; set; }
    public string training_group_remark { get; set; }
    public int training_group_status { get; set; }
    public int training_group_created_by { get; set; }
    public int training_group_updated_by { get; set; }
    public string filter_keyword { get; set; }
    public string training_group_color { get; set; }
}
public class m0_training_branch
{
    public int m0_training_branch_idx { get; set; }
    public string training_branch_name { get; set; }
    public string training_branch_remark { get; set; }
    public int training_branch_status { get; set; }
    public int training_branch_created_by { get; set; }
    public int training_branch_updated_by { get; set; }
    public string filter_keyword { get; set; }
    public int m0_training_group_idx_ref { get; set; }
    public string training_group_name { get; set; }
}
public class m0_training_organ
{
    public int m0_training_organ_idx { get; set; }
    public string training_organ_code { get; set; }
    public string training_organ_name { get; set; }
    public string training_organ_remark { get; set; }
    public int training_organ_status { get; set; }
    public int training_organ_created_by { get; set; }
    public int training_organ_updated_by { get; set; }
    public string filter_keyword { get; set; }
}
public class m0_sche_trn_day
{
    public int m0_sche_trn_day_idx { get; set; }
    public string sche_trn_day_from { get; set; }
    public string sche_trn_day_to { get; set; }
    public string sche_trn_day_remark { get; set; }
    public int sche_trn_day_status { get; set; }
    public int sche_trn_day_created_by { get; set; }
    public int sche_trn_day_updated_by { get; set; }

    public int zItem { get; set; }
    public string filter_keyword { get; set; }
}

public class m0_email
{
    public int m0_email_idx { get; set; }
    public string email_name { get; set; }
    public string email_remark { get; set; }
    public int email_status { get; set; }
    public int email_created_by { get; set; }
    public int email_updated_by { get; set; }
    public string filter_keyword { get; set; }
}

public class m0_level
{
    public int m0_level_idx { get; set; }
    public int level_code { get; set; }
    public string level_name { get; set; }
    public string level_remark { get; set; }
    public int level_flag { get; set; }
    public int level_status { get; set; }
    public int level_created_by { get; set; }
    public int level_updated_by { get; set; }
    public string filter_keyword { get; set; }
}

public class m0_evaluation_group
{
    public int m0_evaluation_group_idx { get; set; }
    public int evaluation_group_item { get; set; }
    public string evaluation_group_name_th { get; set; }
    public string evaluation_group_name_en { get; set; }
    public int evaluation_group_status { get; set; }
    public int evaluation_group_created_by { get; set; }
    public int evaluation_group_updated_by { get; set; }
    public string filter_keyword { get; set; }

}

public class m0_evaluation_form
{
    public int m0_evaluation_f_idx { get; set; }
    public int evaluation_f_item { get; set; }
    public int m0_evaluation_group_idx_ref { get; set; }
    public string evaluation_f_name_en { get; set; }
    public string evaluation_f_name_th { get; set; }
    public string evaluation_f_remark { get; set; }
    public int evaluation_f_score { get; set; }
    public int evaluation_f_flag { get; set; }
    public int evaluation_f_status { get; set; }
    public int evaluation_f_created_by { get; set; }
    public int evaluation_f_updated_by { get; set; }

    public string evaluation_group_name_th { get; set; }

    public string filter_keyword { get; set; }


}

public class m0_target_group
{
    public int m0_target_group_idx { get; set; }
    public string target_group_name { get; set; }
    public string target_group_remark { get; set; }
    public int target_group_status { get; set; }
    public int target_group_created_by { get; set; }
    public int target_group_updated_by { get; set; }
    public string filter_keyword { get; set; }
}
public class m0_institution
{
    public int m0_institution_idx { get; set; }
    public string institution_name { get; set; }
    public string institution_remark { get; set; }
    public int institution_status { get; set; }
    public int institution_created_by { get; set; }
    public int institution_updated_by { get; set; }
    public string filter_keyword { get; set; }
    public string institution_address { get; set; }
    public string institution_tel { get; set; }
    public string institution_fax { get; set; }

}
public class m0_lecturer
{

    public int m0_lecturer_idx { get; set; }
    public int m0_institution_idx_ref { get; set; }
    public int Eduidx_ref { get; set; }
    public string lecturer_name { get; set; }
    public string lecturer_remark { get; set; }
    public int lecturer_status { get; set; }
    public int lecturer_created_by { get; set; }
    public int lecturer_updated_by { get; set; }
    public string filter_keyword { get; set; }
    public string lecturer_address { get; set; }
    public string lecturer_tel { get; set; }
    public string lecturer_email { get; set; }
    public string institution_name { get; set; }

}
public class m0_objective
{
    public int m0_objective_idx { get; set; }
    public string objective_name { get; set; }
    public string objective_remark { get; set; }
    public int objective_status { get; set; }
    public int objective_created_by { get; set; }
    public int objective_updated_by { get; set; }
    public string filter_keyword { get; set; }

}

public class m0_meeting
{
    public int m0_meeting_idx { get; set; }
    public int meeting_code { get; set; }
    public string meeting_name { get; set; }
    public string meeting_remark { get; set; }
    public string meeting_file_name { get; set; }
    public int place_idx_ref { get; set; }
    public int positon_status { get; set; }
    public int meeting_flag { get; set; }
    public int meeting_status { get; set; }
    public int meeting_created_by { get; set; }
    public int meeting_updated_by { get; set; }
    public string filter_keyword { get; set; }
    public string zplace_name { get; set; }
    public string zpositon_status_name { get; set; }
    public string meeting_date_from { get; set; }
    public string meeting_date_to { get; set; }
    public int file_flag { get; set; }
    public decimal file_duration { get; set; }

}

public class m0_iso
{
    public int m0_iso_idx { get; set; }
    public string iso_name { get; set; }
    public string iso_remark { get; set; }
    public int iso_flag { get; set; }
    public int iso_status { get; set; }
    public int iso_created_by { get; set; }
    public int iso_updated_by { get; set; }
    public string filter_keyword { get; set; }
}

#region m0_runno
[Serializable]
public class m0_runno
{
    public int m0_runno_idx { get; set; }
    public string m0_formname { get; set; }
    public string m0_prefix { get; set; }
    public string m0_year { get; set; }
    public int m0_rundocno { get; set; }
    public string m0_docno { get; set; }
    public string option_name { get; set; }
    public string num_width { get; set; }
}
#endregion m0_runno


//transection
[Serializable]
public class traning_req
{
    public string operation_status_id { get; set; }
    public int idx { get; set; }
    //U0
    public int u0_training_req_idx { get; set; }
    public string training_req_no { get; set; }
    public string u0_training_req_date { get; set; }
    public int u0_RSecID_ref { get; set; }
    public int u0_RDeptID_ref { get; set; }
    public int u0_RPosIDX_ref { get; set; }
    public int u0_EmpIDX_ref { get; set; }
    public int approve_status { get; set; }
    public int u0_idx { get; set; }
    public int node_idx { get; set; }
    public int actor_idx { get; set; }
    public string approve_remark { get; set; }

    //U1
    public int u1_training_req_idx { get; set; }
    public int u0_training_req_idx_ref { get; set; }
    public int m0_training_idx_ref { get; set; }
    public int training_req_type_flag { get; set; }
    public string training_req_needs { get; set; }
    public decimal training_req_budget { get; set; }
    public string training_req_month_study { get; set; }
    public int u1_RPosIDX_ref { get; set; }
    public int training_req_qty { get; set; }
    public int app_flag { get; set; }
    public int app_user { get; set; }
    public string app_date { get; set; }
    public string training_req_other { get; set; }
    public int training_req_item { get; set; }
    public string pos_name_th { get; set; }
    public int training_req_pos_flag { get; set; }
    public string training_req_pos_other { get; set; }
    //U2
    public int u2_training_req_idx_ref { get; set; }
    public int u1_training_req_idx_ref { get; set; }
    public int u2_RPosIDX_ref { get; set; }
    public int u2_EmpIDX_ref { get; set; }
    public int training_req_item_ref { get; set; }

    //U3
    public int u3_training_req_idx_ref { get; set; }
    public int u3_RPosIDX_ref { get; set; }

    // U0/U1/U2
    public int training_req_status { get; set; }
    public int training_req_created_by { get; set; }
    public string training_req_created_at { get; set; }
    public int training_req_updated_by { get; set; }
    public string training_req_updated_at { get; set; }

    public int training_qty { get; set; }
    public int training_emp_qty { get; set; }
    public string training_code { get; set; }
    public string training_name { get; set; }

    public string dept_name_th { get; set; }
    public string org_name_th { get; set; }
    public string employee_name { get; set; }
    public string employee_code { get; set; }

    public int EmpIDXApprove1 { get; set; }
    public string NameApprover1 { get; set; }
    public int EmpIDXApprove2 { get; set; }
    public string NameApprover2 { get; set; }

    public string node_name { get; set; }
    public string actor_name { get; set; }
    public string decision_name { get; set; }


    public string filter_keyword { get; set; }

    public int JobLevel { get; set; }
    public int zmonth { get; set; }
    public int zyear { get; set; }


}
public class course
{
    public string operation_status_id { get; set; }
    public int idx { get; set; }

    //el_u0_course
    public int u0_course_idx { get; set; }
    public string course_no { get; set; }
    public string course_date { get; set; }
    public string course_name { get; set; }
    public string course_remark { get; set; }
    public int course_type_etraining { get; set; }
    public int course_type_elearning { get; set; }
    public int course_priority { get; set; }
    public int m0_training_group_idx_ref { get; set; }
    public int m0_level_idx_ref { get; set; }
    public int course_score { get; set; }
    public int u0_idx { get; set; }
    public int node_idx { get; set; }
    public int actor_idx { get; set; }
    public int level_code { get; set; }
    public int approve_status { get; set; }
    public int app_flag { get; set; }
    public int app_user { get; set; }
    public string app_date { get; set; }
    public int TIDX_ref { get; set; }
    public string course_plan_status { get; set; }
    public int m0_training_branch_idx_ref { get; set; }
    public int trn_plan_flag { get; set; }
    public int course_assessment { get; set; }
    public string io_page_flag { get; set; }

    //el_u1_course
    public int u1_course_idx { get; set; }
    public int RSecID_ref { get; set; }
    public int RDeptID_ref { get; set; }
    public int RPosIDX_ref { get; set; }
    public int course_qty { get; set; }
    public int org_idx_ref { get; set; }


    //el_u2_course_test
    public int u2_course_idx { get; set; }
    public int course_item { get; set; }
    public string course_proposition { get; set; }
    public string course_propos_a { get; set; }
    public string course_propos_b { get; set; }
    public string course_propos_c { get; set; }
    public string course_propos_d { get; set; }
    public string course_propos_answer { get; set; }
    public string course_proposition_img { get; set; }
    public string course_propos_a_img { get; set; }
    public string course_propos_b_img { get; set; }
    public string course_propos_c_img { get; set; }
    public string course_propos_d_img { get; set; }


    //el_u3_course_evaluation
    public int u3_course_idx { get; set; }
    public int m0_evaluation_f_idx_ref { get; set; }
    public int evaluation_f_item { get; set; }
    public int m0_evaluation_group_idx_ref { get; set; }
    public string course_evaluation_f_name_th { get; set; }

    //u4
    public int u0_course_idx_ref_pass { get; set; }

    //u5
    public int TIDX_flag { get; set; }

    //el_u6_course
    public int u6_course_idx { get; set; }
    public string video_title { get; set; }
    public string video_description { get; set; }
    public int video_item { get; set; }
    public decimal score_through_per { get; set; }
    public string video_name { get; set; }
    public string video_images { get; set; }

    // All
    public int u0_course_idx_ref { get; set; }
    public int course_status { get; set; }
    public int course_created_by { get; set; }
    public string course_created_at { get; set; }
    public int course_updated_by { get; set; }
    public string course_updated_at { get; set; }

    //file Lookup
    public string node_name { get; set; }
    public string actor_name { get; set; }
    public string decision_name { get; set; }
    public string training_group_name { get; set; }
    public string training_branch_name { get; set; }
    public int u2_course_test_item { get; set; }
    public int u3_course_evaluation_item { get; set; }
    public string target_name { get; set; }
    public string org_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_th { get; set; }
    public string evaluation_group_name_th { get; set; }
    public int zvideo_item { get; set; }

    // Filter
    public string filter_keyword { get; set; }

    public int JobLevel { get; set; }
    public int zmonth { get; set; }
    public int zyear { get; set; }

    //meeting room
    public int m1_idx { get; set; }
    public int m0_meeting_idx { get; set; }
    public int meeting_code { get; set; }
    public string meeting_name { get; set; }
    public string meeting_remark { get; set; }
    public string meeting_file_name { get; set; }
    public int place_idx_ref { get; set; }
    public int positon_status { get; set; }
    public int meeting_flag { get; set; }
    public int meeting_status { get; set; }
    public int meeting_created_by { get; set; }
    public int meeting_updated_by { get; set; }

    public string zplace_name { get; set; }
    public string zpositon_status_name { get; set; }
    public string meeting_date_from { get; set; }
    public string meeting_date_to { get; set; }
    public int file_flag { get; set; }
    public decimal file_duration { get; set; }
    public int hh_qty { get; set; }
    public int nn_qty { get; set; }

}
public class training_plan
{
    public string operation_status_id { get; set; }
    public int idx { get; set; }

    //el_u0_training_plan
    public int u0_training_plan_idx { get; set; }
    public string training_plan_no { get; set; }
    public string training_plan_date { get; set; }
    public int training_plan_year { get; set; }
    public int u0_course_idx_ref { get; set; }
    public int lecturer_type { get; set; }
    public int m0_institution_idx_ref { get; set; }
    public int m0_target_group_idx_ref { get; set; }
    public int training_plan_qty { get; set; }
    public decimal training_plan_amount { get; set; }
    public int training_plan_model { get; set; }
    public int training_plan_m1 { get; set; }
    public int training_plan_m2 { get; set; }
    public int training_plan_m3 { get; set; }
    public int training_plan_m4 { get; set; }
    public int training_plan_m5 { get; set; }
    public int training_plan_m6 { get; set; }
    public int training_plan_m7 { get; set; }
    public int training_plan_m8 { get; set; }
    public int training_plan_m9 { get; set; }
    public int training_plan_m10 { get; set; }
    public int training_plan_m11 { get; set; }
    public int training_plan_m12 { get; set; }
    public decimal training_plan_budget { get; set; }
    public decimal training_plan_costperhead { get; set; }
    public string training_plan_status { get; set; }
    public int training_plan_created_by { get; set; }
    public string training_plan_created_at { get; set; }
    public int training_plan_updated_by { get; set; }
    public string training_plan_updated_at { get; set; }

    public int training_plan_mtt { get; set; }
    public int training_plan_npw { get; set; }
    public int training_plan_rjn { get; set; }
    public int trn_course_flag { get; set; }

    //u1
    public int u0_training_plan_idx_ref { get; set; }

    //node
    public int u0_idx { get; set; }
    public int node_idx { get; set; }
    public int actor_idx { get; set; }
    public int level_code { get; set; }
    public int approve_status { get; set; }
    public int app_flag { get; set; }
    public int app_user { get; set; }
    public string app_date { get; set; }
    public string approve_remark { get; set; }
    //to

    public string to_decision_name { get; set; }

    public string to_node_name { get; set; }

    public string to_actor_name { get; set; }

    public string to_node_type_name { get; set; }

    public string zprocess_name { get; set; }

    public int el_approve_status { get; set; }
    public int to_m0_node_idx { get; set; }
    public int to_m0_actor_idx { get; set; }

    //node md
    public int md_u0_idx { get; set; }
    public int md_node_idx { get; set; }
    public int md_actor_idx { get; set; }
    public int md_approve_status { get; set; }
    public int md_app_flag { get; set; }
    public int md_app_user { get; set; }
    public string md_app_date { get; set; }
    public string md_approve_remark { get; set; }

    public string course_name { get; set; }
    public string node_name { get; set; }
    public string actor_name { get; set; }
    public string decision_name { get; set; }
    public string training_group_name { get; set; }
    public string training_group_color { get; set; }
    public string training_branch_name { get; set; }
    public string target_name { get; set; }
    public string target_group_name { get; set; }
    public string institution_name { get; set; }

    public string md_node_name { get; set; }
    public string md_actor_name { get; set; }
    public string md_decision_name { get; set; }

    // Filter
    public string filter_keyword { get; set; }

    public int JobLevel { get; set; }
    public int zmonth { get; set; }
    public int zyear { get; set; }
    public string zstatus { get; set; }

}
public class u_manage
{
    public string operation_status_id { get; set; }
    public int idx { get; set; }

    public int u0_manage_idx { get; set; }
    public int RSecID_ref { get; set; }
    public int RDeptID_ref { get; set; }
    public int org_idx_ref { get; set; }
    public int rpos_idx_ref { get; set; }
    public int manage_status { get; set; }
    public int u1_manage_idx { get; set; }
    public int u0_manage_idx_ref { get; set; }
    public int m0_from_crll_idx { get; set; }
    public int crontroll_flag { get; set; }

    public string from_name { get; set; }
    public string from_crontroll_name { get; set; }
    public string from_crontroll_description { get; set; }
    public string org_name_th { get; set; }
    public string sec_name_th { get; set; }
    public string dept_name_th { get; set; }

    public string filter_keyword { get; set; }
    public string zdate { get; set; }
    public string FullNameTH { get; set; }

}

/*
public class training_course
{
    public string operation_status_id { get; set; }
    public int idx { get; set; }

    //training_course
    public int u0_training_course_idx { get; set; }
    public string training_course_no { get; set; }
    public string training_course_date { get; set; }
    public int u0_training_plan_idx_ref { get; set; }
    public int u0_course_idx_ref { get; set; }
    public string training_course_description { get; set; }
    public int training_course_type { get; set; }
    public int place_idx_ref { get; set; }
    public int lecturer_type { get; set; }
    public string training_course_date_strat { get; set; }
    public string training_course_date_end { get; set; }
    public string training_course_remark { get; set; }
    public int training_course_planbudget_type { get; set; }
    public decimal training_course_total { get; set; }
    public decimal training_course_total_avg { get; set; }
    public decimal training_course_reduce_tax { get; set; }
    public decimal training_course_net_charge { get; set; }
    public decimal training_course_net_charge_tax { get; set; }
    public int costcenter_idx_ref { get; set; }
    public int RDeptID_ref { get; set; }
    public int condition { get; set; }
    public decimal training_course_planbudget_total { get; set; }
    public decimal training_course_budget_total { get; set; }
    public decimal training_course_budget_balance { get; set; }
    public decimal training_course_budget_total_per { get; set; }
    public int training_course_status { get; set; }
    public int training_course_created_by { get; set; }
    public string training_course_created_at { get; set; }
    public int training_course_updated_by { get; set; }
    public string training_course_updated_at { get; set; }
    public string course_plan_status { get; set; }
    public string training_course_file_name { get; set; }
    public string dept_name_th { get; set; }
    public int RSecID { get; set; }
    public string summ_no { get; set; }
    public string summ_date { get; set; }
    public string io_page_flag { get; set; }
    public int place_type { get; set; }
    public int m0_target_group_idx_ref { get; set; }

    public int super_u0_idx { get; set; }
    public int super_node_idx { get; set; }
    public int super_actor_idx { get; set; }
    public int super_app_status { get; set; }
    public int super_app_user { get; set; }
    public string super_app_date { get; set; }
    public string super_app_remark { get; set; }
    public int hr_status { get; set; }
    public int hr_user { get; set; }
    public string hr_date { get; set; }
    public string hr_remark { get; set; }

    public string zdecision { get; set; }
    public int zacter_status { get; set; }

    public string qrcode_test { get; set; }
    public string qrcode_evaluationform { get; set; }
    public string StatusDoc { get; set; }

    //el_u1_training_course_lecturer
    public int u1_training_course_idx { get; set; }
    public int u0_training_course_idx_ref { get; set; }
    public int m0_institution_idx_ref { get; set; }


    //el_u2_training_course_objective
    public int u2_training_course_idx { get; set; }
    public int m0_objective_idx_ref { get; set; }


    //el_u3_training_course_employee
    public int u3_training_course_idx { get; set; }
    public int emp_idx_ref { get; set; }
    public int register_status { get; set; }
    public int register_user { get; set; }
    public string register_remark { get; set; }
    public string register_date { get; set; }
    public string zregister_date { get; set; }

    public int signup_status { get; set; }
    public string signup_date { get; set; }
    public int signup_user { get; set; }
    public string signup_remark { get; set; }
    public string zsignup_date { get; set; }

    public decimal test_scores { get; set; }
    public int test_scores_status { get; set; }
    public string test_scores_date { get; set; }
    public int test_scores_user { get; set; }
    public string test_scores_remark { get; set; }
    public string ztest_scores_date { get; set; }


    //el_u4_training_course_expenses
    public int u4_training_course_idx { get; set; }
    public string expenses_description { get; set; }
    public decimal amount { get; set; }
    public decimal vat { get; set; }
    public decimal withholding_tax { get; set; }

    //el_u5_training_course_follow
    public int u5_training_course_idx { get; set; }
    public int pass_test_flag { get; set; }
    public decimal pass_test_per { get; set; }
    public int hour_training_flag { get; set; }
    public decimal hour_training_per { get; set; }
    public int write_report_training_flag { get; set; }
    public int publish_training_flag { get; set; }
    public string publish_training_description { get; set; }
    public int course_lecturer_flag { get; set; }
    public int other_flag { get; set; }
    public string other_description { get; set; }
    public int hrd_nofollow_flag { get; set; }
    public int hrd_follow_flag { get; set; }
    public int hrd_follow_day { get; set; }

    //el_u6_training_course_summary
    public int u6_training_course_idx { get; set; }
    public string training_summary_report_remark { get; set; }
    public string training_benefits_remark { get; set; }
    public string supervisors_additional { get; set; }
    public int summ_std_user { get; set; }
    public int summ_super_user { get; set; }
    public int u3_training_course_idx_ref { get; set; }
    public string summ_std_date { get; set; }
    public string summ_super_date { get; set; }
    public int summ_std_status { get; set; }
    public int summ_super_status { get; set; }
    public int summ_hr_user { get; set; }
    public string summ_hr_remark { get; set; }

    //el_u7_training_course_resulte
    public int u7_training_course_idx { get; set; }
    public int resulte_std_user { get; set; }
    public int resulte_app_user { get; set; }
    public string resulte_std_date { get; set; }
    public string resulte_app_date { get; set; }
    public int resulte_app_status { get; set; }
    public string remark { get; set; }

    //el_u8_training_course_date
    public string u8_training_course_idx { get; set; }
    public string training_course_date_start { get; set; }
    public decimal training_course_date_qty { get; set; }


    //node
    public int u0_idx { get; set; }
    public int node_idx { get; set; }
    public int actor_idx { get; set; }
    public int level_code { get; set; }
    public int approve_status { get; set; }
    public int app_flag { get; set; }
    public int app_user { get; set; }
    public string app_date { get; set; }
    public string approve_remark { get; set; }
    public int type_request { get; set; }
    public int el_approve_status { get; set; }
    public int to_m0_node_idx { get; set; }
    public int to_m0_actor_idx { get; set; }
    public string status_name { get; set; }
    public int staidx { get; set; }

    //node md
    public int md_u0_idx { get; set; }
    public int md_node_idx { get; set; }
    public int md_actor_idx { get; set; }
    public int md_approve_status { get; set; }
    public int md_app_flag { get; set; }
    public int md_app_user { get; set; }
    public string md_app_date { get; set; }
    public string md_approve_remark { get; set; }

    public string course_name { get; set; }
    public string node_name { get; set; }
    public string actor_name { get; set; }
    public string decision_name { get; set; }
    public string training_group_name { get; set; }
    public string training_group_color { get; set; }
    public string training_branch_name { get; set; }
    public string target_name { get; set; }
    public string target_group_name { get; set; }
    public string institution_name { get; set; }
    public string objective_name { get; set; }

    public string md_node_name { get; set; }
    public string md_actor_name { get; set; }
    public string md_decision_name { get; set; }



    public string zstatus_name { get; set; }

    // Filter
    public string filter_keyword { get; set; }

    public int JobLevel { get; set; }
    public int zmonth { get; set; }
    public int zyear { get; set; }
    public string zstatus { get; set; }

    public int training_course_year { get; set; }

    public string zdate { get; set; }
    public string zdate_start { get; set; }
    public string ztime_start { get; set; }
    public string zdate_end { get; set; }
    public string ztime_end { get; set; }
    public string zpermission { get; set; }
    public int zday { get; set; }


    public int EmpIDX { get; set; }
    public int CostIDX { get; set; }
    public int RDeptID { get; set; }
    public string EmpCode { get; set; }
    public string FullNameTH { get; set; }
    public string DeptNameTH { get; set; }
    public string PosNameTH { get; set; }
    public string CostNo { get; set; }
    public string MobileNo { get; set; }
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public string m0_fileidx_ref { get; set; }
    public string file_reason { get; set; }

    public int zcount_emp { get; set; }
    public int zcount_resulte { get; set; }


    public string level_name { get; set; }
    public string level_remark { get; set; }

    public int zresulte_app_status { get; set; }
    public int zresulte_count { get; set; }

    public int course_score { get; set; }
    public decimal score_through_per { get; set; }

    public int u0_course_idx { get; set; }
    public int zcount_Lv { get; set; }

    public int grade_status { get; set; }
    public int grade_user { get; set; }
    public string grade_date { get; set; }
    public decimal grade_avg { get; set; }
    public int course_assessment { get; set; }
    public int zcourse_count { get; set; }

    public string zcourse_name { get; set; }
    public int m0_evaluation_f_idx_ref { get; set; }
    public int m0_evaluation_group_idx_ref { get; set; }
    public int course_item { get; set; }
    public string evaluation_group_name_th { get; set; }
    public string zName { get; set; }
    public int score_qty { get; set; }
    public string doc_no { get; set; }

    public string u2_course_idx { get; set; }
    public string course_proposition { get; set; }
    public string course_propos_a { get; set; }
    public string course_propos_b { get; set; }
    public string course_propos_c { get; set; }
    public string course_propos_d { get; set; }
    public string course_proposition_img { get; set; }
    public string course_propos_a_img { get; set; }
    public string course_propos_b_img { get; set; }
    public string course_propos_c_img { get; set; }
    public string course_propos_d_img { get; set; }
    public string course_propos_answer { get; set; }
    public string course_no { get; set; }
    public int result_flag { get; set; }
    public string training_course_propos_answer { get; set; }
    public decimal course_score_avg { get; set; }
    public decimal score_through_per_result { get; set; }
    public string emp_code { get; set; }
    public string emp_name_th { get; set; }

    public int open_flag { get; set; }

    public int item_count { get; set; }


    public int course_type_etraining { get; set; }

    public int course_type_elearning { get; set; }

    public string zcourse_type_etraining_name { get; set; }


    public string video_name { get; set; }
    public string video_images { get; set; }
    public int video_item { get; set; }
    public string video_title { get; set; }
    public string video_description { get; set; }
    public int u6_course_idx { get; set; }
    public int item_count_std { get; set; }
    public int zitem_count { get; set; }

    public string currenttime { get; set; }
    public string duration { get; set; }
    public int currenttime_int { get; set; }

    public int u13_training_course_idx { get; set; }
    public int item_total { get; set; }
    public int el_flag { get; set; }
    public int rpos_idx_ref { get; set; }
    public string location_name { get; set; }

    public string Approve1 { get; set; }
    public string Pos_Approve1 { get; set; }
    public string dateApprove1 { get; set; }
    public string Approve2 { get; set; }
    public string Pos_Approve2 { get; set; }
    public string dateApprove2 { get; set; }
    public string Approve3 { get; set; }
    public string Pos_Approve3 { get; set; }
    public string dateApprove3 { get; set; }
    public string Approve4 { get; set; }
    public string Pos_Approve4 { get; set; }
    public string dateApprove4 { get; set; }

    public string emp_email { get; set; }
    public string type_plan { get; set; }

}
*/

[Serializable]
public class training_course
{
    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }
    [XmlElement("idx")]
    public int idx { get; set; }

    //training_course
    [XmlElement("u0_training_course_idx")]
    public int u0_training_course_idx { get; set; }
    [XmlElement("training_course_no")]
    public string training_course_no { get; set; }
    [XmlElement("training_course_date")]
    public string training_course_date { get; set; }
    [XmlElement("u0_training_plan_idx_ref")]
    public int u0_training_plan_idx_ref { get; set; }
    [XmlElement("u0_course_idx_ref")]
    public int u0_course_idx_ref { get; set; }
    [XmlElement("training_course_description")]
    public string training_course_description { get; set; }
    [XmlElement("training_course_type")]
    public int training_course_type { get; set; }
    [XmlElement("place_idx_ref")]
    public int place_idx_ref { get; set; }
    [XmlElement("lecturer_type")]
    public int lecturer_type { get; set; }
    [XmlElement("training_course_date_strat")]
    public string training_course_date_strat { get; set; }
    [XmlElement("training_course_date_end")]
    public string training_course_date_end { get; set; }
    [XmlElement("training_course_remark")]
    public string training_course_remark { get; set; }
    [XmlElement("training_course_planbudget_type")]
    public int training_course_planbudget_type { get; set; }
    [XmlElement("training_course_total")]
    public decimal training_course_total { get; set; }
    [XmlElement("training_course_total_avg")]
    public decimal training_course_total_avg { get; set; }
    [XmlElement("training_course_reduce_tax")]
    public decimal training_course_reduce_tax { get; set; }
    [XmlElement("training_course_net_charge")]
    public decimal training_course_net_charge { get; set; }
    [XmlElement("training_course_net_charge_tax")]
    public decimal training_course_net_charge_tax { get; set; }
    [XmlElement("costcenter_idx_ref")]
    public int costcenter_idx_ref { get; set; }
    [XmlElement("RDeptID_ref")]
    public int RDeptID_ref { get; set; }
    [XmlElement("condition")]
    public int condition { get; set; }
    [XmlElement("training_course_planbudget_total")]
    public decimal training_course_planbudget_total { get; set; }
    [XmlElement("training_course_budget_total")]
    public decimal training_course_budget_total { get; set; }
    [XmlElement("training_course_budget_balance")]
    public decimal training_course_budget_balance { get; set; }
    [XmlElement("training_course_budget_total_per")]
    public decimal training_course_budget_total_per { get; set; }
    [XmlElement("training_course_status")]
    public int training_course_status { get; set; }
    [XmlElement("training_course_created_by")]
    public int training_course_created_by { get; set; }
    [XmlElement("training_course_created_at")]
    public string training_course_created_at { get; set; }
    [XmlElement("training_course_updated_by")]
    public int training_course_updated_by { get; set; }
    [XmlElement("training_course_updated_at")]
    public string training_course_updated_at { get; set; }
    [XmlElement("course_plan_status")]
    public string course_plan_status { get; set; }
    [XmlElement("training_course_file_name")]
    public string training_course_file_name { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("RSecID")]
    public int RSecID { get; set; }
    [XmlElement("summ_no")]
    public string summ_no { get; set; }
    [XmlElement("summ_date")]
    public string summ_date { get; set; }
    [XmlElement("io_page_flag")]
    public string io_page_flag { get; set; }
    [XmlElement("place_type")]
    public int place_type { get; set; }
    [XmlElement("m0_target_group_idx_ref")]
    public int m0_target_group_idx_ref { get; set; }
    [XmlElement("qrcode_test")]
    public string qrcode_test { get; set; }
    [XmlElement("qrcode_evaluationform")]
    public string qrcode_evaluationform { get; set; }
    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }


    //el_u1_training_course_lecturer
    [XmlElement("u1_training_course_idx")]
    public int u1_training_course_idx { get; set; }
    [XmlElement("u0_training_course_idx_ref")]
    public int u0_training_course_idx_ref { get; set; }
    [XmlElement("m0_institution_idx_ref")]
    public int m0_institution_idx_ref { get; set; }

    [XmlElement("super_u0_idx")]
    public int super_u0_idx { get; set; }
    [XmlElement("super_node_idx")]
    public int super_node_idx { get; set; }
    [XmlElement("super_actor_idx")]
    public int super_actor_idx { get; set; }
    [XmlElement("super_app_status")]
    public int super_app_status { get; set; }
    [XmlElement("super_app_user")]
    public int super_app_user { get; set; }
    [XmlElement("super_app_date")]
    public string super_app_date { get; set; }
    [XmlElement("super_app_remark")]
    public string super_app_remark { get; set; }
    [XmlElement("hr_status")]
    public int hr_status { get; set; }
    [XmlElement("hr_user")]
    public int hr_user { get; set; }
    [XmlElement("hr_date")]
    public string hr_date { get; set; }
    [XmlElement("hr_remark")]
    public string hr_remark { get; set; }

    [XmlElement("zdecision")]
    public string zdecision { get; set; }
    [XmlElement("zacter_status")]
    public int zacter_status { get; set; }

    //el_u2_training_course_objective
    [XmlElement("u2_training_course_idx")]
    public int u2_training_course_idx { get; set; }
    [XmlElement("m0_objective_idx_ref")]
    public int m0_objective_idx_ref { get; set; }


    //el_u3_training_course_employee
    [XmlElement("u3_training_course_idx")]
    public int u3_training_course_idx { get; set; }
    [XmlElement("emp_idx_ref")]
    public int emp_idx_ref { get; set; }
    [XmlElement("register_status")]
    public int register_status { get; set; }
    [XmlElement("register_user")]
    public int register_user { get; set; }
    [XmlElement("register_remark")]
    public string register_remark { get; set; }
    [XmlElement("register_date")]
    public string register_date { get; set; }
    [XmlElement("zregister_date")]
    public string zregister_date { get; set; }

    [XmlElement("signup_status")]
    public int signup_status { get; set; }
    [XmlElement("signup_date")]
    public string signup_date { get; set; }
    [XmlElement("signup_user")]
    public int signup_user { get; set; }
    [XmlElement("signup_remark")]
    public string signup_remark { get; set; }
    [XmlElement("zsignup_date")]
    public string zsignup_date { get; set; }

    [XmlElement("test_scores")]
    public decimal test_scores { get; set; }
    [XmlElement("test_scores_status")]
    public int test_scores_status { get; set; }
    [XmlElement("test_scores_date")]
    public string test_scores_date { get; set; }
    [XmlElement("test_scores_user")]
    public int test_scores_user { get; set; }
    [XmlElement("test_scores_remark")]
    public string test_scores_remark { get; set; }
    [XmlElement("ztest_scores_date")]
    public string ztest_scores_date { get; set; }


    //el_u4_training_course_expenses
    [XmlElement("u4_training_course_idx")]
    public int u4_training_course_idx { get; set; }
    [XmlElement("expenses_description")]
    public string expenses_description { get; set; }
    [XmlElement("amount")]
    public decimal amount { get; set; }
    [XmlElement("vat")]
    public decimal vat { get; set; }
    [XmlElement("withholding_tax")]
    public decimal withholding_tax { get; set; }

    //el_u5_training_course_follow
    [XmlElement("u5_training_course_idx")]
    public int u5_training_course_idx { get; set; }
    [XmlElement("pass_test_flag")]
    public int pass_test_flag { get; set; }
    [XmlElement("pass_test_per")]
    public decimal pass_test_per { get; set; }
    [XmlElement("hour_training_flag")]
    public int hour_training_flag { get; set; }
    [XmlElement("hour_training_per")]
    public decimal hour_training_per { get; set; }
    [XmlElement("write_report_training_flag")]
    public int write_report_training_flag { get; set; }
    [XmlElement("publish_training_flag")]
    public int publish_training_flag { get; set; }
    [XmlElement("publish_training_description")]
    public string publish_training_description { get; set; }
    [XmlElement("course_lecturer_flag")]
    public int course_lecturer_flag { get; set; }
    [XmlElement("other_flag")]
    public int other_flag { get; set; }
    [XmlElement("other_description")]
    public string other_description { get; set; }
    [XmlElement("hrd_nofollow_flag")]
    public int hrd_nofollow_flag { get; set; }
    [XmlElement("hrd_follow_flag")]
    public int hrd_follow_flag { get; set; }
    [XmlElement("hrd_follow_day")]
    public int hrd_follow_day { get; set; }

    //el_u6_training_course_summary
    [XmlElement("u6_training_course_idx")]
    public int u6_training_course_idx { get; set; }
    [XmlElement("training_summary_report_remark")]
    public string training_summary_report_remark { get; set; }
    [XmlElement("training_benefits_remark")]
    public string training_benefits_remark { get; set; }
    [XmlElement("supervisors_additional")]
    public string supervisors_additional { get; set; }
    [XmlElement("summ_std_user")]
    public int summ_std_user { get; set; }
    [XmlElement("summ_super_user")]
    public int summ_super_user { get; set; }
    [XmlElement("u3_training_course_idx_ref")]
    public int u3_training_course_idx_ref { get; set; }
    [XmlElement("summ_std_date")]
    public string summ_std_date { get; set; }
    [XmlElement("summ_super_date")]
    public string summ_super_date { get; set; }
    [XmlElement("summ_std_status")]
    public int summ_std_status { get; set; }
    [XmlElement("summ_super_status")]
    public int summ_super_status { get; set; }
    [XmlElement("summ_hr_user")]
    public int summ_hr_user { get; set; }
    [XmlElement("summ_hr_remark")]
    public string summ_hr_remark { get; set; }

    //el_u7_training_course_resulte
    [XmlElement("u7_training_course_idx")]
    public int u7_training_course_idx { get; set; }
    [XmlElement("resulte_std_user")]
    public int resulte_std_user { get; set; }
    [XmlElement("resulte_app_user")]
    public int resulte_app_user { get; set; }
    [XmlElement("resulte_std_date")]
    public string resulte_std_date { get; set; }
    [XmlElement("resulte_app_date")]
    public string resulte_app_date { get; set; }
    [XmlElement("resulte_app_status")]
    public int resulte_app_status { get; set; }
    [XmlElement("remark")]
    public string remark { get; set; }

    //el_u8_training_course_date
    [XmlElement("u8_training_course_idx")]
    public string u8_training_course_idx { get; set; }
    [XmlElement("training_course_date_start")]
    public string training_course_date_start { get; set; }
    [XmlElement("training_course_date_qty")]
    public decimal training_course_date_qty { get; set; }


    // node
    public int level_code { get; set; }
    [XmlElement("approve_status")]
    public int approve_status { get; set; }
    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }
    [XmlElement("node_idx")]
    public int node_idx { get; set; }
    [XmlElement("actor_idx")]
    public int actor_idx { get; set; }
    [XmlElement("approve_remark")]
    public string approve_remark { get; set; }
    [XmlElement("app_flag")]
    public int app_flag { get; set; }
    [XmlElement("app_user")]
    public int app_user { get; set; }
    [XmlElement("app_date")]
    public string app_date { get; set; }
    [XmlElement("type_request")]
    public int type_request { get; set; }
    [XmlElement("el_approve_status")]
    public int el_approve_status { get; set; }
    [XmlElement("to_m0_node_idx")]
    public int to_m0_node_idx { get; set; }
    [XmlElement("to_m0_actor_idx")]
    public int to_m0_actor_idx { get; set; }
    [XmlElement("status_name")]
    public string status_name { get; set; }
    [XmlElement("staidx")]
    public int staidx { get; set; }

    //node md
    [XmlElement("md_u0_idx")]
    public int md_u0_idx { get; set; }
    [XmlElement("md_node_idx")]
    public int md_node_idx { get; set; }
    [XmlElement("md_actor_idx")]
    public int md_actor_idx { get; set; }
    [XmlElement("md_approve_status")]
    public int md_approve_status { get; set; }
    [XmlElement("md_app_flag")]
    public int md_app_flag { get; set; }
    [XmlElement("md_app_user")]
    public int md_app_user { get; set; }
    [XmlElement("md_app_date")]
    public string md_app_date { get; set; }
    [XmlElement("md_approve_remark")]
    public string md_approve_remark { get; set; }

    [XmlElement("zstatus_name")]
    public string zstatus_name { get; set; }

    //lookup
    [XmlElement("course_name")]
    public string course_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("decision_name")]
    public string decision_name { get; set; }

    [XmlElement("training_group_color")]
    public string training_group_color { get; set; }

    [XmlElement("training_group_name")]
    public string training_group_name { get; set; }

    [XmlElement("training_branch_name")]
    public string training_branch_name { get; set; }

    [XmlElement("target_name")]
    public string target_name { get; set; }

    [XmlElement("target_group_name")]
    public string target_group_name { get; set; }


    [XmlElement("institution_name")]
    public string institution_name { get; set; }

    [XmlElement("objective_name")]
    public string objective_name { get; set; }

    [XmlElement("md_node_name")]
    public string md_node_name { get; set; }
    [XmlElement("md_actor_name")]
    public string md_actor_name { get; set; }
    [XmlElement("md_decision_name")]
    public string md_decision_name { get; set; }

    // Filter
    public string filter_keyword { get; set; }

    [XmlElement("JobLevel")]
    public int JobLevel { get; set; }
    [XmlElement("zmonth")]
    public int zmonth { get; set; }
    [XmlElement("zyear")]
    public int zyear { get; set; }
    [XmlElement("zstatus")]
    public string zstatus { get; set; }
    [XmlElement("zpermission")]
    public string zpermission { get; set; }

    [XmlElement("training_course_year")]
    public int training_course_year { get; set; }

    [XmlElement("zdate")]
    public string zdate { get; set; }
    [XmlElement("zdate_start")]
    public string zdate_start { get; set; }
    [XmlElement("ztime_start")]
    public string ztime_start { get; set; }
    [XmlElement("zdate_end")]
    public string zdate_end { get; set; }
    [XmlElement("ztime_end")]
    public string ztime_end { get; set; }
    [XmlElement("zday")]
    public int zday { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }
    [XmlElement("CostIDX")]
    public int CostIDX { get; set; }
    [XmlElement("RDeptID")]
    public int RDeptID { get; set; }
    [XmlElement("EmpCode")]
    public string EmpCode { get; set; }
    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }
    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }
    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }
    [XmlElement("CostNo")]
    public string CostNo { get; set; }
    [XmlElement("MobileNo")]
    public string MobileNo { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("m0_fileidx_ref")]
    public string m0_fileidx_ref { get; set; }
    [XmlElement("file_reason")]
    public string file_reason { get; set; }


    [XmlElement("zcount_emp")]
    public int zcount_emp { get; set; }
    [XmlElement("zcount_resulte")]
    public int zcount_resulte { get; set; }

    [XmlElement("level_name")]
    public string level_name { get; set; }
    [XmlElement("level_remark")]
    public string level_remark { get; set; }

    [XmlElement("zresulte_app_status")]
    public int zresulte_app_status { get; set; }
    [XmlElement("zresulte_count")]
    public int zresulte_count { get; set; }

    [XmlElement("course_score")]
    public int course_score { get; set; }
    [XmlElement("score_through_per")]
    public decimal score_through_per { get; set; }

    [XmlElement("u0_course_idx")]
    public int u0_course_idx { get; set; }
    [XmlElement("zcount_Lv")]
    public int zcount_Lv { get; set; }

    [XmlElement("grade_status")]
    public int grade_status { get; set; }
    [XmlElement("grade_user")]
    public int grade_user { get; set; }
    [XmlElement("grade_date")]
    public string grade_date { get; set; }
    [XmlElement("grade_avg")]
    public decimal grade_avg { get; set; }
    [XmlElement("course_assessment")]
    public int course_assessment { get; set; }
    [XmlElement("zcourse_count")]
    public int zcourse_count { get; set; }

    [XmlElement("zcourse_name")]
    public string zcourse_name { get; set; }
    [XmlElement("m0_evaluation_f_idx_ref")]
    public int m0_evaluation_f_idx_ref { get; set; }
    [XmlElement("m0_evaluation_group_idx_ref")]
    public int m0_evaluation_group_idx_ref { get; set; }
    [XmlElement("course_item")]
    public int course_item { get; set; }
    [XmlElement("evaluation_group_name_th")]
    public string evaluation_group_name_th { get; set; }
    [XmlElement("zName")]
    public string zName { get; set; }
    [XmlElement("score_qty")]
    public int score_qty { get; set; }
    [XmlElement("doc_no")]
    public string doc_no { get; set; }

    [XmlElement("u2_course_idx")]
    public int u2_course_idx { get; set; }
    [XmlElement("course_proposition")]
    public string course_proposition { get; set; }
    [XmlElement("course_propos_a")]
    public string course_propos_a { get; set; }
    [XmlElement("course_propos_b")]
    public string course_propos_b { get; set; }
    [XmlElement("course_propos_c")]
    public string course_propos_c { get; set; }
    [XmlElement("course_propos_d")]
    public string course_propos_d { get; set; }
    [XmlElement("course_proposition_img")]
    public string course_proposition_img { get; set; }
    [XmlElement("course_propos_a_img")]
    public string course_propos_a_img { get; set; }
    [XmlElement("course_propos_b_img")]
    public string course_propos_b_img { get; set; }
    [XmlElement("course_propos_c_img")]
    public string course_propos_c_img { get; set; }
    [XmlElement("course_propos_d_img")]
    public string course_propos_d_img { get; set; }
    [XmlElement("course_propos_answer")]
    public string course_propos_answer { get; set; }
    [XmlElement("course_no")]
    public string course_no { get; set; }
    [XmlElement("training_course_item")]
    public int training_course_item { get; set; }
    [XmlElement("result_flag")]
    public int result_flag { get; set; }
    [XmlElement("training_course_propos_answer")]
    public string training_course_propos_answer { get; set; }
    [XmlElement("course_score_avg")]
    public decimal course_score_avg { get; set; }
    [XmlElement("score_through_per_result")]
    public decimal score_through_per_result { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("open_flag")]
    public int open_flag { get; set; }
    [XmlElement("item_count")]
    public int item_count { get; set; }

    [XmlElement("course_type_etraining")]
    public int course_type_etraining { get; set; }
    [XmlElement("course_type_elearning")]
    public int course_type_elearning { get; set; }
    [XmlElement("zcourse_type_etraining_name")]
    public string zcourse_type_etraining_name { get; set; }

    [XmlElement("video_name")]
    public string video_name { get; set; }
    [XmlElement("video_images")]
    public string video_images { get; set; }
    [XmlElement("video_item")]
    public int video_item { get; set; }
    [XmlElement("video_title")]
    public string video_title { get; set; }
    [XmlElement("video_description")]
    public string video_description { get; set; }
    [XmlElement("u6_course_idx")]
    public int u6_course_idx { get; set; }
    [XmlElement("item_count_std")]
    public int item_count_std { get; set; }
    [XmlElement("zitem_count")]
    public int zitem_count { get; set; }

    [XmlElement("currenttime")]
    public string currenttime { get; set; }
    [XmlElement("duration")]
    public string duration { get; set; }
    [XmlElement("currenttime_int")]
    public int currenttime_int { get; set; }

    [XmlElement("u13_training_course_idx")]
    public int u13_training_course_idx { get; set; }
    [XmlElement("item_total")]
    public int item_total { get; set; }

    [XmlElement("el_flag")]
    public int el_flag { get; set; }

    [XmlElement("rpos_idx_ref")]
    public int rpos_idx_ref { get; set; }
    [XmlElement("location_name")]
    public string location_name { get; set; }

    [XmlElement("Approve1")]
    public string Approve1 { get; set; }
    [XmlElement("Pos_Approve1")]
    public string Pos_Approve1 { get; set; }
    [XmlElement("dateApprove1")]
    public string dateApprove1 { get; set; }
    [XmlElement("Approve2")]
    public string Approve2 { get; set; }
    [XmlElement("Pos_Approve2")]
    public string Pos_Approve2 { get; set; }
    [XmlElement("dateApprove2")]
    public string dateApprove2 { get; set; }
    [XmlElement("Approve3")]
    public string Approve3 { get; set; }
    [XmlElement("Pos_Approve3")]
    public string Pos_Approve3 { get; set; }
    [XmlElement("dateApprove3")]
    public string dateApprove3 { get; set; }
    [XmlElement("Approve4")]
    public string Approve4 { get; set; }
    [XmlElement("Pos_Approve4")]
    public string Pos_Approve4 { get; set; }
    [XmlElement("dateApprove4")]
    public string dateApprove4 { get; set; }
    [XmlElement("emp_email")]
    public string emp_email { get; set; }
    [XmlElement("type_plan")]
    public string type_plan { get; set; }
    [XmlElement("emp_email_approve")]
    public string emp_email_approve { get; set; }


}

//Lookup
[Serializable]
public class employeeM
{
    public string operation_status_id { get; set; }
    public int idx { get; set; }

    public int EmpIDX { get; set; }

    public string DocCode { get; set; }

    public string EmpCode { get; set; }

    public string FullNameTH { get; set; }

    public string EmpName { get; set; }

    public int OrgIDX { get; set; }

    public string OrgNameTH { get; set; }

    public int SysIDX { get; set; }

    public string SysName { get; set; }

    public int RDeptID { get; set; }

    public string RDeptName { get; set; }

    public string SecNameTH { get; set; }

    public int RSecID { get; set; }

    public int RPosIDX_J { get; set; }

    public string PosNameTH { get; set; }

    public string Email { get; set; }

    public string MobileNo { get; set; }

    public string CostNo { get; set; }

    public int CostIDX { get; set; }

    public int JobLevel { get; set; }

    public int JobGradeIDX { get; set; }

    public int EmpIDXApprove1 { get; set; }

    public string NameApprover1 { get; set; }

    public int EmpIDXApprove2 { get; set; }

    public string NameApprover2 { get; set; }


    public string doc_no { get; set; }

    public string doc_date { get; set; }

    public int approve_status { get; set; }

    public string zstatus { get; set; }

}
[Serializable]
public class trainingLoolup
{
    public string operation_status_id { get; set; }
    public int idx { get; set; }
    public int idx1 { get; set; }

    public int m0_training_idx { get; set; }
    public string training_code { get; set; }
    public string training_name { get; set; }

    public int m0_training_type_idx { get; set; }
    public string training_type_name { get; set; }

    //public int RDeptIDX { get; set; }
    public int DeptIDX { get; set; }
    public int OrgIDX { get; set; }

    public int rdept_idx { get; set; }
    public string dept_name_th { get; set; }
    public string org_name_th { get; set; }
    public string zFullName { get; set; }

    public int m0_training_group_idx { get; set; }
    public string training_group_name { get; set; }

    public int m0_training_branch_idx { get; set; }
    public string training_branch_name { get; set; }

    public string email_name { get; set; }

    public string doc_no { get; set; }
    public string doc_date { get; set; }
    public string employee_name { get; set; }

    public int SecIDX { get; set; }
    public string SecNameTH { get; set; }

    public int m0_level_idx { get; set; }
    public string level_code { get; set; }

    public string fil_Search { get; set; }

    public int zId { get; set; }
    public string zName { get; set; }
    public int zId_G { get; set; }
    public string zName_G { get; set; }
    public string zRemark { get; set; }

    public int score { get; set; }

    //Strat Node //
    public int u0_idx { get; set; }
    public int from_node { get; set; }
    public string from_name { get; set; }
    public int from_actor { get; set; }
    public string from_actor_name { get; set; }
    public int to_node { get; set; }
    public string to_name { get; set; }
    public int to_actor { get; set; }
    public string to_actor_name { get; set; }
    public int decision { get; set; }
    public string decision_name { get; set; }
    public int node_type { get; set; }
    public string node_typer_name { get; set; }
    public int m0_node_process_idx { get; set; }
    public string node_type_process_name { get; set; }

    public int approve_status { get; set; }

    public int zmonth { get; set; }
    public int zyear { get; set; }

    public int m0_training_group_idx_ref { get; set; }

    //end Node //

    public decimal amount { get; set; }
    public decimal qty { get; set; }
    public decimal total { get; set; }
    public int lecturer_type { get; set; }

    public int EmpIDX { get; set; }

    public int course_score { get; set; }
    public decimal score_through_per { get; set; }


}

//start Report
[Serializable]
public class training_rpt_plan
{
    public string operation_status_id { get; set; }
    public int idx { get; set; }
    public int idx1 { get; set; }

    public int m0_training_idx { get; set; }
    public int m0_training_group_idx_ref { get; set; }

    public int zId { get; set; }
    public string zName { get; set; }
    public int zId_G { get; set; }
    public string zName_G { get; set; }

    public int zmonth { get; set; }
    public int zyear { get; set; }

    public string fil_Search { get; set; }
    public string filter_keyword { get; set; }

    //Data
    public string zcode { get; set; }
    public string zdoc_no { get; set; }
    public string zbranch_name { get; set; }
    public string zmtt { get; set; }
    public string znpw { get; set; }
    public string zrjn { get; set; }
    public string zgroup_name { get; set; }
    public int zqty { get; set; }
    public int zmodel { get; set; }
    public int zm1 { get; set; }
    public int zm2 { get; set; }
    public int zm3 { get; set; }
    public int zm4 { get; set; }
    public int zm5 { get; set; }
    public int zm6 { get; set; }
    public int zm7 { get; set; }
    public int zm8 { get; set; }
    public int zm9 { get; set; }
    public int zm10 { get; set; }
    public int zm11 { get; set; }
    public int zm12 { get; set; }
    public decimal zamount { get; set; }
    public decimal zbudget { get; set; }

    //COURSE-LEARN-EMP
    public int u0_course_idx { get; set; }
    public string course_no { get; set; }
    public string course_date { get; set; }
    public string course_name { get; set; }
    public string course_priority { get; set; }
    public int course_score { get; set; }
    public int course_status { get; set; }
    public int level_code { get; set; }
    public string training_group_name { get; set; }
    public string training_branch_name { get; set; }
    public int u2_course_test_item { get; set; }
    public int u3_course_evaluation_item { get; set; }
    public string zcourse_date { get; set; }
    public string course_plan_status { get; set; }
    public string priority_name { get; set; }
    public string priority_remark { get; set; }
    public int trn_plan_flag { get; set; }
    public int course_assessment { get; set; }
    public string zcourse_type_etraining_name { get; set; }
    public int emp_idx { get; set; }
    public int course_type_etraining { get; set; }
    public int course_type_elearning { get; set; }
    public string course_remark { get; set; }
    public string level_name { get; set; }

    // report_training_plan
    public int u0_training_course_idx { get; set; }
    public string ztype { get; set; }
    public string zstatus_training_plan { get; set; }
    public string zcatergories { get; set; }
    public int zyear_plan { get; set; }
    public string ztraining_date { get; set; }
    public string zcourse_name { get; set; }
    public string ztrainer { get; set; }
    public string zinstitue { get; set; }
    public string ztarget_group { get; set; }
    public decimal zquantity_course { get; set; }
    public decimal ztarget { get; set; }
    public decimal zregister { get; set; }
    public decimal zpersen { get; set; }
    public string ztime_length { get; set; }
    public decimal zhh { get; set; }
    public decimal zhh_total { get; set; }
    public string zassessment_tool { get; set; }
    public decimal zpass { get; set; }
    public decimal zpass_persen { get; set; }
    public decimal zsatisfaction { get; set; }
    public string zcost_center { get; set; }
    public decimal zplan_amount { get; set; }
    public int training_course_created_by { get; set; }
    public string training_course_created_at { get; set; }
    public string zcreate_name { get; set; }
    public int lecturer_type { get; set; }

    public decimal u10_item { get; set; }
    public decimal u10_total_qty { get; set; }
    public decimal score_qty { get; set; }
    public decimal total_qty { get; set; }


}

//end Report

[Serializable]
public class trainingLoolup_Xml
{
    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }
    [XmlElement("idx")]
    public int idx { get; set; }


    [XmlElement("m0_training_idx")]
    public int m0_training_idx { get; set; }
    [XmlElement("training_code")]
    public string training_code { get; set; }
    [XmlElement("training_name")]
    public string training_name { get; set; }

    [XmlElement("m0_training_type_idx")]
    public int m0_training_type_idx { get; set; }
    [XmlElement("training_type_name")]
    public string training_type_name { get; set; }

    //[XmlElement("RDeptIDX")]
    //public int RDeptIDX { get; set; }
    //[XmlElement("DeptIDX")]
    public int DeptIDX { get; set; }
    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("zFullName")]
    public string zFullName { get; set; }

    [XmlElement("m0_training_group_idx")]
    public int m0_training_group_idx { get; set; }
    [XmlElement("training_group_name")]
    public string training_group_name { get; set; }

    [XmlElement("m0_training_branch_idx")]
    public int m0_training_branch_idx { get; set; }
    [XmlElement("training_branch_name")]
    public string training_branch_name { get; set; }

    [XmlElement("SecIDX")]
    public int SecIDX { get; set; }
    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }
    [XmlElement("email_name")]
    public string email_name { get; set; }
    [XmlElement("doc_no")]
    public string doc_no { get; set; }
    [XmlElement("doc_date")]
    public string doc_date { get; set; }
    [XmlElement("employee_name")]
    public string employee_name { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("emp_email")]
    public string emp_email { get; set; }
    [XmlElement("zyear")]
    public int zyear { get; set; }
    [XmlElement("qty")]
    public int qty { get; set; }
    [XmlElement("amount")]
    public decimal amount { get; set; }
    [XmlElement("model")]
    public int model { get; set; }
    [XmlElement("course_name")]
    public string course_name { get; set; }
    [XmlElement("priority_name")]
    public string priority_name { get; set; }
    [XmlElement("priority_remark")]
    public string priority_remark { get; set; }
    [XmlElement("target_group_name")]
    public string target_group_name { get; set; }
    [XmlElement("mtt")]
    public int mtt { get; set; }
    [XmlElement("npw")]
    public int npw { get; set; }
    [XmlElement("rjn")]
    public int rjn { get; set; }

    [XmlElement("approve_status")]
    public int approve_status { get; set; }
    [XmlElement("md_approve_status")]
    public int md_approve_status { get; set; }
    [XmlElement("decision_name")]
    public string decision_name { get; set; }
    [XmlElement("node_name")]
    public string node_name { get; set; }
    [XmlElement("actor_name")]
    public string actor_name { get; set; }
    [XmlElement("md_decision_name")]
    public string md_decision_name { get; set; }
    [XmlElement("md_node_name")]
    public string md_node_name { get; set; }
    [XmlElement("md_actor_name")]
    public string md_actor_name { get; set; }
    [XmlElement("approve_mode_name")]
    public string approve_mode_name { get; set; }


    [XmlElement("fil_Search")]
    public string fil_Search { get; set; }

    [XmlElement("zdate")]
    public string zdate { get; set; }
    [XmlElement("zdate_start")]
    public string zdate_start { get; set; }
    [XmlElement("ztime_start")]
    public string ztime_start { get; set; }
    [XmlElement("zdate_end")]
    public string zdate_end { get; set; }
    [XmlElement("ztime_end")]
    public string ztime_end { get; set; }
    [XmlElement("zplace_name")]
    public string zplace_name { get; set; }

    [XmlElement("link")]
    public string link { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("zdate_full")]
    public string zdate_full { get; set; }

}

[Serializable]
public class traning_req_Xml
{
    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }
    [XmlElement("idx")]
    public int idx { get; set; }

    //U0
    [XmlElement("u0_training_req_idx")]
    public int u0_training_req_idx { get; set; }
    [XmlElement("training_req_no")]
    public string training_req_no { get; set; }
    [XmlElement("u0_training_req_date")]
    public string u0_training_req_date { get; set; }
    [XmlElement("u0_RSecID_ref")]
    public int u0_RSecID_ref { get; set; }
    [XmlElement("u0_RDeptID_ref")]
    public int u0_RDeptID_ref { get; set; }
    [XmlElement("u0_RPosIDX_ref")]
    public int u0_RPosIDX_ref { get; set; }
    [XmlElement("u0_EmpIDX_ref")]
    public int u0_EmpIDX_ref { get; set; }


    //U1
    [XmlElement("u1_training_req_idx")]
    public int u1_training_req_idx { get; set; }
    [XmlElement("u0_training_req_idx_ref")]
    public int u0_training_req_idx_ref { get; set; }
    [XmlElement("m0_training_idx_ref")]
    public int m0_training_idx_ref { get; set; }
    [XmlElement("training_req_type_flag")]
    public int training_req_type_flag { get; set; }
    [XmlElement("training_req_needs")]
    public string training_req_needs { get; set; }
    [XmlElement("training_req_budget")]
    public decimal training_req_budget { get; set; }
    [XmlElement("training_req_month_study")]
    public string training_req_month_study { get; set; }
    [XmlElement("u1_RPosIDX_ref")]
    public int u1_RPosIDX_ref { get; set; }
    [XmlElement("training_req_qty")]
    public int training_req_qty { get; set; }
    [XmlElement("app_flag")]
    public int app_flag { get; set; }
    [XmlElement("app_user")]
    public int app_user { get; set; }
    [XmlElement("app_date")]
    public string app_date { get; set; }
    [XmlElement("training_req_other")]
    public string training_req_other { get; set; }
    [XmlElement("training_req_item")]
    public int training_req_item { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    //U2
    [XmlElement("u2_training_req_idx_ref")]
    public int u2_training_req_idx_ref { get; set; }
    [XmlElement("u1_training_req_idx_ref")]
    public int u1_training_req_idx_ref { get; set; }
    [XmlElement("u2_RPosIDX_ref")]
    public int u2_RPosIDX_ref { get; set; }
    [XmlElement("u2_EmpIDX_ref")]
    public int u2_EmpIDX_ref { get; set; }
    [XmlElement("training_req_item_ref")]
    public int training_req_item_ref { get; set; }


    // U0/U1/U2
    [XmlElement("training_req_status")]
    public int training_req_status { get; set; }
    [XmlElement("training_req_created_by")]
    public int training_req_created_by { get; set; }
    [XmlElement("training_req_created_at")]
    public string training_req_created_at { get; set; }
    [XmlElement("training_req_updated_by")]
    public int training_req_updated_by { get; set; }
    [XmlElement("training_req_updated_at")]
    public string training_req_updated_at { get; set; }


    [XmlElement("training_qty")]
    public int training_qty { get; set; }
    [XmlElement("training_emp_qty")]
    public int training_emp_qty { get; set; }
    [XmlElement("training_code")]
    public string training_code { get; set; }
    [XmlElement("training_name")]
    public string training_name { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("employee_name")]
    public string employee_name { get; set; }


    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("emp_email")]
    public string emp_email { get; set; }
    [XmlElement("zyear")]
    public int zyear { get; set; }
    [XmlElement("qty")]
    public int qty { get; set; }
    [XmlElement("amount")]
    public decimal amount { get; set; }
    [XmlElement("model")]
    public int model { get; set; }
    [XmlElement("course_name")]
    public string course_name { get; set; }
    [XmlElement("priority_name")]
    public string priority_name { get; set; }
    [XmlElement("priority_remark")]
    public string priority_remark { get; set; }
    [XmlElement("target_group_name")]
    public string target_group_name { get; set; }
    [XmlElement("mtt")]
    public int mtt { get; set; }
    [XmlElement("npw")]
    public int npw { get; set; }
    [XmlElement("rjn")]
    public int rjn { get; set; }
    [XmlElement("training_group_name")]
    public string training_group_name { get; set; }
    [XmlElement("training_branch_name")]
    public string training_branch_name { get; set; }
    [XmlElement("doc_no")]
    public string doc_no { get; set; }
    [XmlElement("doc_date")]
    public string doc_date { get; set; }

    [XmlElement("approve_status")]
    public int approve_status { get; set; }
    [XmlElement("md_approve_status")]
    public int md_approve_status { get; set; }
    [XmlElement("decision_name")]
    public string decision_name { get; set; }
    [XmlElement("node_name")]
    public string node_name { get; set; }
    [XmlElement("actor_name")]
    public string actor_name { get; set; }
    [XmlElement("md_decision_name")]
    public string md_decision_name { get; set; }
    [XmlElement("md_node_name")]
    public string md_node_name { get; set; }
    [XmlElement("md_actor_name")]
    public string md_actor_name { get; set; }
    [XmlElement("approve_mode_name")]
    public string approve_mode_name { get; set; }


    [XmlElement("filter_keyword")]
    public string filter_keyword { get; set; }

    [XmlElement("zdate")]
    public string zdate { get; set; }
    [XmlElement("zdate_start")]
    public string zdate_start { get; set; }
    [XmlElement("ztime_start")]
    public string ztime_start { get; set; }
    [XmlElement("zdate_end")]
    public string zdate_end { get; set; }
    [XmlElement("ztime_end")]
    public string ztime_end { get; set; }
    [XmlElement("zplace_name")]
    public string zplace_name { get; set; }

    [XmlElement("link")]
    public string link { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("zdate_full")]
    public string zdate_full { get; set; }

}

#endregion

#region Bonus
[Serializable]
public class U0_QuizDocument
{
    public int condition { get; set; }
    public int CEmpIDX { get; set; }
    public int u0_course_idx { get; set; }
    public string course_name { get; set; }
    public int m0_tqidx { get; set; }
    public string type_quiz { get; set; }
    public int u2_main_course_idx { get; set; }
    public int status { get; set; }
    public int u2_course_idx { get; set; }
    public int course_item { get; set; }

}

[Serializable]
public class M1_QuizDocument
{
    public int CEmpIDX { get; set; }
    public int u0_course_idx { get; set; }
    public string course_name { get; set; }
    public int m0_tqidx { get; set; }
    public string type_quiz { get; set; }
    public int no_choice { get; set; }
    public string quest_name { get; set; }
    public int pic_quest { get; set; }
    public string choice_a { get; set; }
    public int pic_a { get; set; }
    public string choice_b { get; set; }
    public int pic_b { get; set; }
    public string choice_c { get; set; }
    public string pic_c { get; set; }
    public string choice_d { get; set; }
    public int pic_d { get; set; }
    public string choice_ans { get; set; }
    public int pic_ans { get; set; }

    public int course_item { get; set; }
    public string course_proposition { get; set; }
    public string course_propos_a { get; set; }
    public string course_propos_b { get; set; }
    public string course_propos_c { get; set; }
    public string course_propos_d { get; set; }
    public string course_propos_answer { get; set; }
    public int u2_course_idx { get; set; }


}

[Serializable]
public class M0_QuizDocument
{
    public int CEmpIDX { get; set; }
    public int u0_course_idx { get; set; }
    public string weight_score { get; set; }
    public int m0_tqidx { get; set; }
    public int u2_main_course_idx { get; set; }
    public string type_quiz { get; set; }
    public string course_name { get; set; }

}

#endregion


#region m0_master_course / m0_expert_salary
[Serializable]
public class m0_course_cost
{
    [XmlElement("CCIDX")]
    public int CCIDX { get; set; }
    [XmlElement("cost_idx")]
    public int cost_idx { get; set; }
    [XmlElement("course_nub")]
    public string course_nub { get; set; }
    [XmlElement("course_detail")]
    public string course_detail { get; set; }
    [XmlElement("create_idx")]
    public int create_idx { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
    [XmlElement("course_status")]
    public int course_status { get; set; }
    [XmlElement("type_select_course")]
    public int type_select_course { get; set; }
    [XmlElement("cost_name")]
    public string cost_name { get; set; }

    [XmlElement("year_cost")]
    public int year_cost { get; set; }
    [XmlElement("cost_balance")]
    public string cost_balance { get; set; }
    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }
    [XmlElement("m1costidx")]
    public int m1costidx { get; set; }
    [XmlElement("cost_price")]
    public string cost_price { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("el_adminidx")]
    public int el_adminidx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("m0_fileidx")]
    public int m0_fileidx { get; set; }
    [XmlElement("file_name")]
    public string file_name { get; set; }
    [XmlElement("u0_course_idx")]
    public int u0_course_idx { get; set; }


}

[Serializable]
public class m0_expert_salary
{
    [XmlElement("EPIDX")]
    public int EPIDX { get; set; }
    [XmlElement("expert_detail")]
    public string expert_detail { get; set; }
    [XmlElement("expert_num")]
    public string expert_num { get; set; }
    [XmlElement("expert_vat")]
    public int expert_vat { get; set; }
    [XmlElement("ep_create_idx")]
    public int ep_create_idx { get; set; }
    [XmlElement("ep_createdate")]
    public string ep_createdate { get; set; }
    [XmlElement("ep_updatedate")]
    public string ep_updatedate { get; set; }
    [XmlElement("ep_status")]
    public int ep_status { get; set; }
    [XmlElement("type_select_expert")]
    public int type_select_expert { get; set; }
}

[Serializable]
public class M1_ImportCost
{
    [XmlElement("year_import")]
    public int year_import { get; set; }
    [XmlElement("cost_price")]
    public string cost_price { get; set; }
    [XmlElement("cost_name")]
    public string cost_name { get; set; }
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

}
#endregion m0_runno

#region el_m0_training_location
[Serializable]
public class m0_training_location
{
    [XmlElement("LOIDX")]
    public int LOIDX { get; set; }
    [XmlElement("location_type")]
    public int location_type { get; set; }
    [XmlElement("location_name")]
    public string location_name { get; set; }
    [XmlElement("location_address")]
    public string location_address { get; set; }
    [XmlElement("location_create_idx")]
    public int location_create_idx { get; set; }
    [XmlElement("location_status")]
    public int location_status { get; set; }
    [XmlElement("location_idx_ref")]
    public int location_idx_ref { get; set; }
    [XmlElement("type_select_location")]
    public int type_select_location { get; set; }
    [XmlElement("location_type_name")]
    public string location_type_name { get; set; }
}
#endregion

