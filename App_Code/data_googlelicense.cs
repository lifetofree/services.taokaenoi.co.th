﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_networkdevices
/// </summary>
/// 

[Serializable]
[XmlRoot("data_googlelicense")]
public class data_googlelicense
{

    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }


    public m0_ggltype_detail[] m0_ggltype_list { get; set; }
    public m0_gglsoftwarename_detail[] m0_gglsoftwarename_list { get; set; }
    public m0_gglcompany_detail[] m0_gglcompany_list { get; set; }
    public u0_gglbuy_detail[] u0_gglbuy_list { get; set; }
    public m0_ggldataset_detail[] m0_ggldataset_list { get; set; }
    public bind_gglbuy_detail[] bind_gglbuy_list { get; set; }

    public bindInsert_gglbuy_detail[] bindInsert_gglbuy_list { get; set; }
    public m0_gglholderemail_detail[] m0_gglholderemail_list { get; set; }
    public m1_gglholderemail_detail[] m1_gglholderemail_list { get; set; }
    public bind_holdergmail_detail[] bind_holdergmail_list { get; set; }
    public u0_gglrenew_detail[] u0_gglrenew_list { get; set; }
    public ggltranfer_detail[] ggltranfer_list { get; set; }
    public del_holdergmail_detail[] delete_holdergmail_list { get; set; }
    public bind_gglsearch_detail[] bind_gglsearch_list { get; set; }
    public bindsearch_report_ggl_detail[] bindsearch_report_ggl_list { get; set; }
    public bindreport_ggl_detail[] bindreport_ggl_list { get; set; }

    [XmlElement("bind_u0_document_ggl_list")]
    public bind_u0_document_ggl_detail[] bind_u0_document_ggl_list { get; set; }


    public log_u0_document_ggl_detail[] log_u0_document_ggl_list { get; set; }
    public log_delete_holder_ggl_detail[] log_delete_holder_ggl_list { get; set; }


    [XmlElement("approve_u0_document_ggl_list")]
    public approve_u0_document_ggl_detail[] approve_u0_document_ggl_list { get; set; }

    public search_u0_document_ggl_detail[] search_u0_document_ggl_list { get; set; }
    public email_adonline_ggl_detail[] email_adonline_ggl_list { get; set; }
    public m0_type_email_ggl_detail[] m0_type_email_ggl_list { get; set; }


}


public class m0_type_email_ggl_detail
{

    public int m0type_email_idx { get; set; }
    public int condition { get; set; }
    public string m0type_email_name { get; set; }
    public int m0type_status { get; set; }
    public int cemp_idx { get; set; }

}


public class email_adonline_ggl_detail
{

    public int m0_idx { get; set; }
    public int condition { get; set; }
    public string costcenter_no { get; set; }
    public int costcenter_idx { get; set; }
    public string name_license_gmail { get; set; }
    public int cemp_idx { get; set; }

}

public class m0_ggltype_detail
{ 
    public int type_idx { get; set; }   
    public string type_name { get; set; }   
    public int type_status { get; set; }
    public int cemp_idx { get; set; }
}

public class m0_gglsoftwarename_detail
{
    public int software_name_idx { get; set; }
    public string name { get; set; }
    public int status { get; set; }
    public int cemp_idx { get; set; }
}

public class m0_gglcompany_detail
{
    public int company_idx { get; set; }
    public string company_name { get; set; }
    public int company_status { get; set; }
    public int cemp_idx { get; set; }
}

public class u0_gglbuy_detail
{

    public int u0_buy_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public string lot_code { get; set; }

    public int org_idx { get; set; }

    public int rdept_idx { get; set; }

    public int rsec_idx { get; set; }

    public string costcenter_idx { get; set; }

    public int costcenter_no { get; set; }

    public int num_license { get; set; }
}

public class m0_ggldataset_detail
{
    
    public int u0_buy_idx { get; set; }
  
    public int software_name_idx { get; set; }
   
    public int company_idx { get; set; }
  
    public string company_name { get; set; }
   
    public int count_license { get; set; }
    
    public string date_purchase { get; set; }
    
    public string date_expire { get; set; }
    
    public int status { get; set; }
    
    public int cemp_idx { get; set; }
    
    public int type_idx { get; set; }
   
    public string po_code { get; set; }
   
    public string lot_code { get; set; }
   
    public int org_idx { get; set; }
    
    public int rdept_idx { get; set; }
   
    public int rsec_idx { get; set; }
  
    public int costcenter_idx { get; set; }
   
    public string costcenter_no { get; set; }

    public int num_license { get; set; }

}

public class bind_gglbuy_detail
{
    public int m0_idx { get; set; }

    public int u0_buy_idx { get; set; }
    
    public int software_name_idx { get; set; }
   
    public int company_idx { get; set; }
    
    public string company_name { get; set; }
    
    public int count_license { get; set; }
    
    public string date_purchase { get; set; }
    
    public string date_expire { get; set; }
    
    public int status { get; set; }
   
    public int cemp_idx { get; set; }
    
    public int emp_create { get; set; }
    
    public int type_idx { get; set; }
  
    public string po_code { get; set; }
   
    public string lot_codebuy { get; set; }
   
    public int lot_code { get; set; }
  
    public int org_idx { get; set; }
  
    public string org_name_th { get; set; }
   
    public int rdept_idx { get; set; }
   
    public string dept_name_th { get; set; }
  
    public int rsec_idx { get; set; }
   
    public string sec_name_th { get; set; }
   
    public string name_create { get; set; }
    
    public int costcenter_idx { get; set; }
    
    public string costcenter_no { get; set; }
   
    public int num_license { get; set; }   
    
    public int price { get; set; }

    public string OrgNameTH_inlicense { get; set; }

    public string DeptNameTH_inlicense { get; set; }

    public int status_active { get; set; }

    public string name_license_gmail { get; set; }

    public string software_name { get; set; }

    public int Count_EmpGoogleLicnese { get; set; }
    public int storage { get; set; }
    public string m0type_email_name { get; set; }
    public int m0type_email_idx { get; set; }

}

public class bindInsert_gglbuy_detail
{
    public int m0_idx { get; set; }

    public int u0_buy_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int emp_create { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public int lot_code { get; set; }

    public int org_idx { get; set; }

    public string org_name_th { get; set; }

    public int rdept_idx { get; set; }

    public string dept_name_th { get; set; }

    public int rsec_idx { get; set; }

    public string sec_name_th { get; set; }

    public string name_create { get; set; }

    public int costcenter_idx { get; set; }

    public string costcenter_no { get; set; }

    public int num_license { get; set; }

    public int price { get; set; }

    public string OrgNameTH_inlicense { get; set; }

    public string DeptNameTH_inlicense { get; set; }

    public int status_active { get; set; }

    public string name_license_gmail { get; set; }

    public string software_name { get; set; }
}

public class m0_gglholderemail_detail
{
    public int m0_idx { get; set; }

    public int m1_idx { get; set; }

    public int u0_buy_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int emp_create { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public int lot_code { get; set; }

    public int org_idx { get; set; }

    public string org_name_th { get; set; }

    public int rdept_idx { get; set; }

    public string dept_name_th { get; set; }

    public int rsec_idx { get; set; }

    public string sec_name_th { get; set; }

    public string name_create { get; set; }

    public int costcenter_idx { get; set; }

    public string costcenter_no { get; set; }

    public int num_license { get; set; }

    public int price { get; set; }

    public string OrgNameTH_inlicense { get; set; }

    public string DeptNameTH_inlicense { get; set; }

    public int status_active { get; set; }

    public string name_license_gmail { get; set; }

    public string software_name { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int emp_idx { get; set; }

    public string emp_idx_check { get; set; }

    public int storage { get; set; }
    public int m0type_email_idx { get; set; }
    public int emp_idx_holder { get; set; }
    public string name_license_gmail_old { get; set; }


}

public class m1_gglholderemail_detail
{
    public int m0_idx { get; set; }

    public int m1_idx { get; set; }

    public int u0_buy_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int emp_create { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public int lot_code { get; set; }

    public int org_idx { get; set; }

    public string org_name_th { get; set; }

    public int rdept_idx { get; set; }

    public string dept_name_th { get; set; }

    public int rsec_idx { get; set; }

    public string sec_name_th { get; set; }

    public string name_create { get; set; }

    public int costcenter_idx { get; set; }

    public string costcenter_no { get; set; }

    public int num_license { get; set; }

    public int price { get; set; }

    public string OrgNameTH_inlicense { get; set; }

    public string DeptNameTH_inlicense { get; set; }

    public int status_active { get; set; }

    public string name_license_gmail { get; set; }

    public string software_name { get; set; }

    public int emp_idx { get; set; }
}

public class bind_holdergmail_detail
{
    public int m1_idx { get; set; }

    public int m0_idx { get; set; }

    public int u0_buy_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int emp_create { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public int lot_code { get; set; }

    public int org_idx { get; set; }

    public string org_name_th { get; set; }

    public int rdept_idx { get; set; }

    public string dept_name_th { get; set; }

    public int rsec_idx { get; set; }

    public string sec_name_th { get; set; }

    public string name_create { get; set; }

    public int costcenter_idx { get; set; }

    public string costcenter_no { get; set; }

    public int num_license { get; set; }

    public int price { get; set; }

    public string OrgNameTH_inlicense { get; set; }

    public string DeptNameTH_inlicense { get; set; }

    public int status_active { get; set; }

    public string name_license_gmail { get; set; }

    public string software_name { get; set; }

    public string name_holder_email { get; set; }

    public int emp_idx { get; set; }

    public bool selected { get; set; }

    public int count_empholder { get; set; }
    public int m0type_email_idx { get; set; }
    public string m0type_email_name { get; set; }
    public int m0_actor_idx { get; set; }
    public int m0_node_idx { get; set; }
    public int doc_decision { get; set; }

}

public class u0_gglrenew_detail
{

    public int u0_buy_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public string lot_code { get; set; }

    public int org_idx { get; set; }

    public int rdept_idx { get; set; }

    public int rsec_idx { get; set; }

    public string costcenter_idx { get; set; }

    public string costcenter_no { get; set; }

    public int num_license { get; set; }

    public int price { get; set; }

    public string OrgNameTH_inlicense { get; set; }

    public string DeptNameTH_inlicense { get; set; }

    public int count_all { get; set; }

}

public class ggltranfer_detail
{

    public int u0_buy_idx { get; set; }

    public int m0_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public string lot_code { get; set; }

    public int org_idx { get; set; }

    public int rdept_idx { get; set; }

    public int rsec_idx { get; set; }

    public int costcenter_idx { get; set; }

    public string costcenter_no { get; set; }

    public int num_license { get; set; }
}

public class del_holdergmail_detail
{

    
    public int m1_idx { get; set; }

    public int m0_idx { get; set; }

    public int u0_buy_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string software_name { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int emp_create { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public int lot_code { get; set; }

    public int org_idx { get; set; }

    public string org_name_th { get; set; }

    public int rdept_idx { get; set; }

    public string dept_name_th { get; set; }

    public int rsec_idx { get; set; }

    public string sec_name_th { get; set; }

    public string name_create { get; set; }

    public int costcenter_idx { get; set; }

    public string costcenter_no { get; set; }

    public int num_license { get; set; }

    public int price { get; set; }

    public string OrgNameTH_inlicense { get; set; }

    public string DeptNameTH_inlicense { get; set; }

    public int status_active { get; set; }

    public string name_license_gmail { get; set; }

    public string name_holder_email { get; set; }

    public int emp_idx { get; set; }

    public int m0_node_idx { get; set; }

    public int m0_actor_idx { get; set; }

    public int doc_decision { get; set; }

}

public class bind_gglsearch_detail
{
    public int emp_idx { get; set; }

    public int m0_idx { get; set; }

    public int u0_buy_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int emp_create { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public int lot_code { get; set; }

    public int org_idx { get; set; }

    public string org_name_th { get; set; }

    public int rdept_idx { get; set; }

    public string dept_name_th { get; set; }

    public int rsec_idx { get; set; }

    public string sec_name_th { get; set; }

    public string name_create { get; set; }

    public int costcenter_idx { get; set; }

    public string costcenter_no { get; set; }

    public int num_license { get; set; }

    public int price { get; set; }

    public string OrgNameTH_inlicense { get; set; }

    public string DeptNameTH_inlicense { get; set; }

    public int status_active { get; set; }

    public string name_license_gmail { get; set; }

    public string software_name { get; set; }

    public int Count_EmpGoogleLicnese { get; set; }

    public string email { get; set; }

    
    public int m0type_email_idx { get; set; }

   
    public string m0type_email_name { get; set; }
}

public class bindsearch_report_ggl_detail
{
    public int emp_idx { get; set; }

    public int m0_idx { get; set; }

    public int u0_buy_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int emp_create { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public int lot_code { get; set; }

    public int org_idx { get; set; }

    public string org_name_th { get; set; }

    public int rdept_idx { get; set; }

    public string dept_name_th { get; set; }

    public int rsec_idx { get; set; }

    public string sec_name_th { get; set; }

    public string name_create { get; set; }

    public int costcenter_idx { get; set; }

    public string costcenter_no { get; set; }

    public int num_license { get; set; }

    public int price { get; set; }

    public string OrgNameTH_inlicense { get; set; }

    public string DeptNameTH_inlicense { get; set; }

    public int status_active { get; set; }

    public string name_license_gmail { get; set; }

    public string software_name { get; set; }

    public int Count_EmpGoogleLicnese { get; set; }

    public string email { get; set; }

    public int m0type_email_idx { get; set; }

    public string m0type_email_name { get; set; }
}

[Serializable]
public class bind_u0_document_ggl_detail
{

    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }

    [XmlElement("u0_buy_idx")]
    public int u0_buy_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("emp_create")]
    public int emp_create { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("po_code")]
    public string po_code { get; set; }

    [XmlElement("lot_codebuy")]
    public string lot_codebuy { get; set; }

    [XmlElement("lot_code")]
    public int lot_code { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }

    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }

    [XmlElement("num_license")]
    public int num_license { get; set; }

    [XmlElement("price")]
    public int price { get; set; }

    [XmlElement("OrgNameTH_inlicense")]
    public string OrgNameTH_inlicense { get; set; }

    [XmlElement("DeptNameTH_inlicense")]
    public string DeptNameTH_inlicense { get; set; }

    [XmlElement("status_active")]
    public int status_active { get; set; }

    [XmlElement("name_license_gmail")]
    public string name_license_gmail { get; set; }

    [XmlElement("Count_EmpGoogleLicnese")]
    public int Count_EmpGoogleLicnese { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("status_document")]
    public string status_document { get; set; }

    [XmlElement("name_holder")]
    public string name_holder { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("email_license")]
    public string email_license { get; set; }

    [XmlElement("org_idx_holder")]
    public int org_idx_holder { get; set; }

    [XmlElement("org_name_th_holder")]
    public string org_name_th_holder { get; set; }

    [XmlElement("rdept_idx_holder")]
    public int rdept_idx_holder { get; set; }

    [XmlElement("dept_name_th_holder")]
    public string dept_name_th_holder { get; set; }

    [XmlElement("rsec_idx_holder")]
    public int rsec_idx_holder { get; set; }

    [XmlElement("sec_name_th_holder")]
    public string sec_name_th_holder { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("holder_gmail")]
    public string holder_gmail { get; set; }

    [XmlElement("m0type_email_idx")]
    public int m0type_email_idx { get; set; }

    [XmlElement("m0type_email_name")]
    public string m0type_email_name { get; set; }



}

public class log_u0_document_ggl_detail
{
    public int m0_idx { get; set; }

    public int u0_buy_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int emp_create { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public int lot_code { get; set; }

    public int org_idx { get; set; }

    public string org_name_th { get; set; }

    public int rdept_idx { get; set; }

    public string dept_name_th { get; set; }

    public int rsec_idx { get; set; }

    public string sec_name_th { get; set; }

    public string name_create { get; set; }

    public int costcenter_idx { get; set; }

    public string costcenter_no { get; set; }

    public int num_license { get; set; }

    public int price { get; set; }

    public string OrgNameTH_inlicense { get; set; }

    public string DeptNameTH_inlicense { get; set; }

    public int status_active { get; set; }

    public string name_license_gmail { get; set; }

    public string software_name { get; set; }

    public int Count_EmpGoogleLicnese { get; set; }

    public int m0_node_idx { get; set; }

    public int m0_actor_idx { get; set; }

    public int doc_decision { get; set; }

    public int u0_idx { get; set; }

    public string status_document { get; set; }

    public string name_holder { get; set; }

    public string date_create { get; set; }

    public string time_create { get; set; }

    public string email_license { get; set; }

    public int emp_idx { get; set; }
   
    public int org_idx_holder { get; set; }
 
    public string org_name_th_holder { get; set; }
   
    public int rdept_idx_holder { get; set; }
    
    public string dept_name_th_holder { get; set; }
    
    public int rsec_idx_holder { get; set; }
   
    public string sec_name_th_holder { get; set; }
  
    public string create_date { get; set; }
   
    public string node_name { get; set; }
    
    public string actor_des { get; set; }
  
    public string status_name { get; set; }
 
    public string comment { get; set; }




}

public class log_delete_holder_ggl_detail
{
    public int m0_idx { get; set; }

    public int u0_buy_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int emp_create { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public int lot_code { get; set; }

    public int org_idx { get; set; }

    public string org_name_th { get; set; }

    public int rdept_idx { get; set; }

    public string dept_name_th { get; set; }

    public int rsec_idx { get; set; }

    public string sec_name_th { get; set; }

    public string name_create { get; set; }

    public int costcenter_idx { get; set; }

    public string costcenter_no { get; set; }

    public int num_license { get; set; }

    public int price { get; set; }

    public string OrgNameTH_inlicense { get; set; }

    public string DeptNameTH_inlicense { get; set; }

    public int status_active { get; set; }

    public string name_license_gmail { get; set; }

    public string software_name { get; set; }

    public int Count_EmpGoogleLicnese { get; set; }

    public int m0_node_idx { get; set; }

    public int m0_actor_idx { get; set; }

    public int doc_decision { get; set; }

    public int u0_idx { get; set; }

    public string status_document { get; set; }

    public string name_holder { get; set; }

    public string date_create { get; set; }

    public string time_create { get; set; }

    public string email_license { get; set; }

    public int emp_idx { get; set; }

    public int org_idx_holder { get; set; }

    public string org_name_th_holder { get; set; }

    public int rdept_idx_holder { get; set; }

    public string dept_name_th_holder { get; set; }

    public int rsec_idx_holder { get; set; }

    public string sec_name_th_holder { get; set; }

    public string create_date { get; set; }

    public string node_name { get; set; }

    public string actor_des { get; set; }

    public string status_name { get; set; }

    public string comment { get; set; }

    public string name_holder_delete { get; set; }




}

[Serializable]
public class approve_u0_document_ggl_detail
{

    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }

    [XmlElement("u0_buy_idx")]
    public int u0_buy_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("emp_create")]
    public int emp_create { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("po_code")]
    public string po_code { get; set; }

    [XmlElement("lot_codebuy")]
    public string lot_codebuy { get; set; }

    [XmlElement("lot_code")]
    public int lot_code { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }

    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }

    [XmlElement("num_license")]
    public int num_license { get; set; }

    [XmlElement("price")]
    public int price { get; set; }

    [XmlElement("OrgNameTH_inlicense")]
    public string OrgNameTH_inlicense { get; set; }

    [XmlElement("DeptNameTH_inlicense")]
    public string DeptNameTH_inlicense { get; set; }

    [XmlElement("status_active")]
    public int status_active { get; set; }

    [XmlElement("name_license_gmail")]
    public string name_license_gmail { get; set; }

    [XmlElement("Count_EmpGoogleLicnese")]
    public int Count_EmpGoogleLicnese { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("status_document")]
    public string status_document { get; set; }

    [XmlElement("name_holder")]
    public string name_holder { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("email_license")]
    public string email_license { get; set; }

    [XmlElement("org_idx_holder")]
    public int org_idx_holder { get; set; }

    [XmlElement("org_name_th_holder")]
    public string org_name_th_holder { get; set; }

    [XmlElement("rdept_idx_holder")]
    public int rdept_idx_holder { get; set; }

    [XmlElement("dept_name_th_holder")]
    public string dept_name_th_holder { get; set; }

    [XmlElement("rsec_idx_holder")]
    public int rsec_idx_holder { get; set; }

    [XmlElement("sec_name_th_holder")]
    public string sec_name_th_holder { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }


}

public class search_u0_document_ggl_detail
{
    public int m0_idx { get; set; }

    public int u0_buy_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int emp_create { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public int lot_code { get; set; }

    public int org_idx { get; set; }

    public string org_name_th { get; set; }

    public int rdept_idx { get; set; }

    public string dept_name_th { get; set; }

    public int rsec_idx { get; set; }

    public string sec_name_th { get; set; }

    public string name_create { get; set; }

    public int costcenter_idx { get; set; }

    public string costcenter_no { get; set; }

    public int num_license { get; set; }

    public int price { get; set; }

    public string OrgNameTH_inlicense { get; set; }

    public string DeptNameTH_inlicense { get; set; }

    public int status_active { get; set; }

    public string name_license_gmail { get; set; }

    public string software_name { get; set; }

    public int Count_EmpGoogleLicnese { get; set; }

    public int m0_node_idx { get; set; }

    public int m0_actor_idx { get; set; }

    public int doc_decision { get; set; }

    public int u0_idx { get; set; }

    public string status_document { get; set; }

    public string name_holder { get; set; }

    public string date_create { get; set; }

    public string time_create { get; set; }

    public string email_license { get; set; }

    public int emp_idx { get; set; }

    public string email { get; set; }

    public int m0type_email_idx { get; set; }

    public string m0type_email_name { get; set; }


}

public class bindreport_ggl_detail
{

    public int emp_idx { get; set; }

    public int m0_idx { get; set; }

    public int u0_buy_idx { get; set; }

    public int software_name_idx { get; set; }

    public int company_idx { get; set; }

    public string software_name { get; set; }

    public string company_name { get; set; }

    public int count_license { get; set; }

    public string date_purchase { get; set; }

    public string date_expire { get; set; }

    public int status { get; set; }

    public int cemp_idx { get; set; }

    public int emp_create { get; set; }

    public int type_idx { get; set; }

    public string po_code { get; set; }

    public string lot_codebuy { get; set; }

    public int lot_code { get; set; }

    public int org_idx { get; set; }

    public string org_name_th { get; set; }

    public int rdept_idx { get; set; }

    public string dept_name_th { get; set; }

    public int rsec_idx { get; set; }

    public string sec_name_th { get; set; }

    public string name_create { get; set; }

    public int costcenter_idx { get; set; }

    public string costcenter_no { get; set; }

    public int num_license { get; set; }

    public int price { get; set; }

    public string OrgNameTH_inlicense { get; set; }

    public string DeptNameTH_inlicense { get; set; }

    public int status_active { get; set; }

    public string name_license_gmail { get; set; }

    public int Count_EmpGoogleLicnese { get; set; }

    public string email { get; set; }

    public int m0type_email_idx { get; set; }

    public string m0type_email_name { get; set; }


}

