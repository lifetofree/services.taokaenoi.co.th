using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for api_dcc
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_dcc : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_dcc _data_dcc = new data_dcc();

    public api_dcc()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region car/par
    // get cp_m0_document_type_list
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCPM0DocTypeList(string jsonIn)
    {
        // convert to xml
        _xml_in = _funcTool.convertJsonToXml(jsonIn);
        // execute and return
        _ret_val = _serviceExec.actionExec("dccConn", "data_dcc", "service_car_par", _xml_in, 91); // return w/ json

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get cp_m1_document_type_list
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCPM1DocTypeList(string jsonIn)
    {
        // convert to xml
        _xml_in = _funcTool.convertJsonToXml(jsonIn);
        // execute and return
        _ret_val = _serviceExec.actionExec("dccConn", "data_dcc", "service_car_par", _xml_in, 81); // return w/ json

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get cp_m0_node_decision
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCPM0NodeDecisionList(string jsonIn)
    {
        // convert to xml
        _xml_in = _funcTool.convertJsonToXml(jsonIn);
        // execute and return
        _ret_val = _serviceExec.actionExec("dccConn", "data_dcc", "service_car_par", _xml_in, 71); // return w/ json

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set cp_u0_document, cp_u1_document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCPU0DocumentList(string jsonIn)
    {
        // convert to xml
        _xml_in = _funcTool.convertJsonToXml(jsonIn);
        // execute and return
        _ret_val = _serviceExec.actionExec("dccConn", "data_dcc", "service_car_par", _xml_in, 10); // return w/ json

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // get cp_u0_document, cp_u1_document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCPU0DocumentList(string jsonIn)
    {
        // convert to xml
        _xml_in = _funcTool.convertJsonToXml(jsonIn);
        // execute and return
        _ret_val = _serviceExec.actionExec("dccConn", "data_dcc", "service_car_par", _xml_in, 20); // return w/ json

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCPU0DocumentDetail(string jsonIn)
    {
        // convert to xml
        _xml_in = _funcTool.convertJsonToXml(jsonIn);
        // execute and return
        _ret_val = _serviceExec.actionExec("dccConn", "data_dcc", "service_car_par", _xml_in, 21); // return w/ json

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion car/par
}