﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("DataMachine")]
public class DataMachine
{
    public string ReturnCode { get; set; }
    public string ReturnMsg { get; set; }
    [XmlElement("BoxRepairMachine")]
    public RepairMachineDetail[] BoxRepairMachine { get; set; }
    public StatusListDetail[] BoxStatusList { get; set; }
    public TypeMachineDetail[] BoxTypeMachine { get; set; }
    public QuestionDetail[] BoxQuestionMachine { get; set; }
    public Machine_Register[] BoxMachine_Register { get; set; }
    public RepairMachine_Export[] BoxRepairMachine_Export { get; set; }

}

[Serializable]
public class RepairMachineDetail
{
    [XmlElement("RPIDX")]
    public int RPIDX { get; set; }

    [XmlElement("m0tidx")]
    public int m0tidx { get; set; }

    [XmlElement("SysIDX")]
    public int SysIDX { get; set; }

    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }

    [XmlElement("GCIDX")]
    public int GCIDX { get; set; }

    [XmlElement("SysNameTH")]
    public string SysNameTH { get; set; }

    [XmlElement("NameTypecode")]
    public string NameTypecode { get; set; }

    [XmlElement("NameGroupcode")]
    public string NameGroupcode { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }


    [XmlElement("u1idx_planning")]
    public int u1idx_planning { get; set; }

    [XmlElement("DateManage")]
    public string DateManage { get; set; }


    [XmlElement("RECode")]
    public string RECode { get; set; }

    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }

    [XmlElement("UserEmpIDX")]
    public int UserEmpIDX { get; set; }

    [XmlElement("CTime")]
    public string CTime { get; set; }

    [XmlElement("CommentAuto")]
    public string CommentAuto { get; set; }

    [XmlElement("CDate")]
    public string CDate { get; set; }


    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("CMIDX")]
    public int CMIDX { get; set; }

    [XmlElement("FileUser")]
    public int FileUser { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }


    [XmlElement("CommentUser")]
    public string CommentUser { get; set; }

    [XmlElement("DateNotify")]
    public string DateNotify { get; set; }

    [XmlElement("TimeNotify")]
    public string TimeNotify { get; set; }


    [XmlElement("AdminEmpIDX")]
    public int AdminEmpIDX { get; set; }

    [XmlElement("FileAM")]
    public int FileAM { get; set; }

    [XmlElement("AdminApproveEmpIDX")]
    public int AdminApproveEmpIDX { get; set; }

    [XmlElement("DateApprove")]
    public string DateApprove { get; set; }

    [XmlElement("TimeApprove")]
    public string TimeApprove { get; set; }


    [XmlElement("CommentApprove")]
    public string CommentApprove { get; set; }

    [XmlElement("DateReceive")]
    public string DateReceive { get; set; }

    [XmlElement("DateClose")]
    public string DateClose { get; set; }

    [XmlElement("CommentReciveAdmin")]
    public string CommentReciveAdmin { get; set; }

    [XmlElement("DategetJobSearch")]
    public string DategetJobSearch { get; set; }

    [XmlElement("DatecloseJobSearch")]
    public string DatecloseJobSearch { get; set; }

    [XmlElement("Price")]
    public int Price { get; set; }

    [XmlElement("CCAIDX")]
    public int CCAIDX { get; set; }

    [XmlElement("STAppIDX")]
    public int STAppIDX { get; set; }

    [XmlElement("RoomIDX")]
    public int RoomIDX { get; set; }

    [XmlElement("Roomname")]
    public string Roomname { get; set; }


    [XmlElement("BuildingIDX")]
    public int BuildingIDX { get; set; }

    [XmlElement("BuildingName")]
    public string BuildingName { get; set; }


    [XmlElement("AdminReciveEmpIDX")]
    public int AdminReciveEmpIDX { get; set; }

    [XmlElement("ACIDX")]
    public int ACIDX { get; set; }

    [XmlElement("IFSearch")]
    public int IFSearch { get; set; }

    [XmlElement("IFSearchbetween")]
    public int IFSearchbetween { get; set; }

    [XmlElement("nameclosejobID")]
    public int nameclosejobID { get; set; }

    [XmlElement("ClasssearchID")]
    public int ClasssearchID { get; set; }

    [XmlElement("MCCode")]
    public string MCCode { get; set; }

    [XmlElement("NameMachine")]
    public string NameMachine { get; set; }

    [XmlElement("MCIDX")]
    public int MCIDX { get; set; }

    [XmlElement("EMail")]
    public string EMail { get; set; }

    [XmlElement("NameUser")]
    public string NameUser { get; set; }

    [XmlElement("UserOrgNameTH")]
    public string UserOrgNameTH { get; set; }

    [XmlElement("UserSecNameTH")]
    public string UserSecNameTH { get; set; }

    [XmlElement("UserDeptNameTH")]
    public string UserDeptNameTH { get; set; }

    [XmlElement("UserPosNameTH")]
    public string UserPosNameTH { get; set; }

    [XmlElement("nodeidx")]
    public int nodeidx { get; set; }

    [XmlElement("actoridx")]
    public int actoridx { get; set; }

    [XmlElement("node_status")]
    public int node_status { get; set; }

    [XmlElement("status_name1")]
    public string status_name1 { get; set; }


    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("TypeNameTH")]
    public string TypeNameTH { get; set; }

    [XmlElement("MCNameTH")]
    public string MCNameTH { get; set; }

    [XmlElement("Abbreviation")]
    public string Abbreviation { get; set; }

    [XmlElement("DateAlertJob")]
    public string DateAlertJob { get; set; }

    [XmlElement("TimeAlertJob")]
    public string TimeAlertJob { get; set; }

    [XmlElement("NameAdminClose")]
    public string NameAdminClose { get; set; }

    [XmlElement("DateCloseJob")]
    public string DateCloseJob { get; set; }

    [XmlElement("TimeCloseJob")]
    public string TimeCloseJob { get; set; }

    [XmlElement("CommentAdmin")]
    public string CommentAdmin { get; set; }

    [XmlElement("NameAdminRe")]
    public string NameAdminRe { get; set; }

    [XmlElement("UserEmpCode")]
    public string UserEmpCode { get; set; }

    [XmlElement("MobileNo")]
    public string MobileNo { get; set; }

    [XmlElement("DateReciveJob")]
    public string DateReciveJob { get; set; }

    [XmlElement("TimeReciveJob")]
    public string TimeReciveJob { get; set; }

    [XmlElement("sumtime")]
    public string sumtime { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("EmpCode")]
    public string EmpCode { get; set; }

    [XmlElement("RPIDXQuest")]
    public int RPIDXQuest { get; set; }

    [XmlElement("DC_YEAR")]
    public string DC_YEAR { get; set; }

    [XmlElement("DC_Month")]
    public string DC_Month { get; set; }

    [XmlElement("zSysIDX_name")]
    public string zSysIDX_name { get; set; }

    /*public int RPIDX { get; set; }
    public string RECode { get; set; }
    public int TmcIDX { get; set; }
    public int UserEmpIDX { get; set; }
    public int FileUser { get; set; }
    public int OrgIDX { get; set; }
    public int RDeptIDX { get; set; }
    public int RSecIDX { get; set; }
    public int RPosIDX { get; set; }
    public int LocIDX { get; set; }
    public string LocName { get; set; }
    public string CommentUser { get; set; }
    public string DateNotify { get; set; }
    public string TimeNotify { get; set; }
    public int AdminEmpIDX { get; set; }
    public int FileAM { get; set; }
    public int AdminApproveEmpIDX { get; set; }
    public string DateApprove { get; set; }
    public string TimeApprove { get; set; }
    public string CommentApprove { get; set; }
    public string DateReceive { get; set; }
    public string DateClose { get; set; }
    public string CommentReciveAdmin { get; set; }
    public string DategetJobSearch { get; set; }
    public string DatecloseJobSearch { get; set; }
    public int Price { get; set; }
    public int CCAIDX { get; set; }
    public int STAppIDX { get; set; }
    public int RoomIDX { get; set; }
    public string Roomname { get; set; }
    public int BuildingIDX { get; set; }
    public string BuildingName { get; set; }
    public int AdminReciveEmpIDX { get; set; }
    public int ACIDX { get; set; }
    public int IFSearch { get; set; }
    public int IFSearchbetween { get; set; }
    public int nameclosejobID { get; set; }
    public int ClasssearchID { get; set; }
    public string MCCode { get; set; }
    public int MCIDX { get; set; }
    public string NameMachine { get; set; }
    public string EMail { get; set; }
    public string NameUser { get; set; }
    public string UserOrgNameTH { get; set; }
    public string UserDeptNameTH { get; set; }
    public string UserSecNameTH { get; set; }
    public string UserPosNameTH { get; set; }
    public int nodeidx { get; set; }
    public int actoridx { get; set; }
    public string actor_name { get; set; }
    public string node_name { get; set; }
    public string status_name { get; set; }
    public string TypeNameTH { get; set; }
    public string MCNameTH { get; set; }
    public string Abbreviation { get; set; }
    public string DateAlertJob { get; set; }
    public string TimeAlertJob { get; set; }
    public string NameAdminClose { get; set; }
    public string DateCloseJob { get; set; }
    public string TimeCloseJob { get; set; }
    public string CommentAdmin { get; set; }
    public string NameAdminRe { get; set; }
    public string UserEmpCode { get; set; }
    public string MobileNo { get; set; }
    public string DateReciveJob { get; set; }
    public string TimeReciveJob { get; set; }
    public int node_status { get; set; }
    public string sumtime { get; set; }
    public string CTime { get; set; }
    public string CommentAuto { get; set; }
    public string CDate { get; set; }
    public string FullNameTH { get; set; }
    public int CMIDX { get; set; }
    public int EmpIDX { get; set; }
    public string EmpCode { get; set; }
    */

}

[Serializable]
public class StatusListDetail
{
    public int stidx { get; set; }
    public int dtidx { get; set; }
    public int status_state { get; set; }
    public string status_name { get; set; }
    public int CEmpIDX { get; set; }
    public string downtime_name { get; set; }


}

[Serializable]
public class TypeMachineDetail
{
    public int CEmpIDX { get; set; }
    public int TmcIDX { get; set; }

    public int TStatus { get; set; }

    public string NameTH { get; set; }

    public string CodeTM { get; set; }
    public string NameEN { get; set; }

}

[Serializable]
public class QuestionDetail
{
    public int TFBIDX { get; set; }
    public int TFBIDX1 { get; set; }
    public int TFBIDX2 { get; set; }
    public int TFBIDX3 { get; set; }
    public int POIDX { get; set; }
    public int UMFB2 { get; set; }
    public int Point { get; set; }
    public string QuestionSet { get; set; }
    public string Topic { get; set; }
    public string Other { get; set; }
    public string Question { get; set; }
    public int TIDX { get; set; }
    public int SumPoint { get; set; }
    public int RPIDX { get; set; }
    public int WFIDX { get; set; }
    public int STAIDX { get; set; }
    public int U1IDX { get; set; }
    public int U0AQS { get; set; }
    public int PO_TFBIDX2 { get; set; }

}

[Serializable]
public class Machine_Register
{
    public int MCIDX { get; set; }
    public string MCCode { get; set; }
    public string Abbreviation { get; set; }
    public string MCNameTH { get; set; }
    public string MCNameEN { get; set; }
    public string MCGenerate { get; set; }
    public string MCSerial { get; set; }
    public string MCInsurance { get; set; }
    public string MCMachineNo { get; set; }
    public string AlertPurchaseDate { get; set; }
    public string AlertOutInsureDate { get; set; }
    public string TC_Name { get; set; }
    public string NameMachine { get; set; }
    public string NameTypeTH { get; set; }
    public string NameTypeEN { get; set; }
    public string ADate { get; set; }
    public string DetailEtc { get; set; }
    public string AssetsCode { get; set; }
    public string Partpic { get; set; }
    public string MCStatusDetail { get; set; }
    public string PlName { get; set; }
    public string STApprove { get; set; }
    public string RoomName { get; set; }
    public string LocName { get; set; }
    public string OrgNameTH { get; set; }
    public string RDeptName { get; set; }
    public string FullNameTH { get; set; }
    public string SecNameTH { get; set; }


    public int TCIDX { get; set; }
    public string NameTypeCode { get; set; }
    public int GCIDX { get; set; }
    public string NameGroupCode { get; set; }

    public int TmcIDX { get; set; }
    public string CodeTM { get; set; }
    public string NameTH { get; set; }
    public int TCLIDX { get; set; }

}

[Serializable]
public class RepairMachine_Export
{
    public string ระบบ { get; set; }
    public string รหัสเอกสาร { get; set; }
    public string ประเภทเครื่องจักร { get; set; }
    public string ชื่อเครื่องจักร { get; set; }
    public string รหัสใหม่ { get; set; }
    public string รหัสเก่า { get; set; }
    public string สถานที่ { get; set; }
    public string อาคาร { get; set; }
    public string ห้อง { get; set; }
    public string วันที่แจ้ง { get; set; }
    public string ชื่อผู้แจ้ง { get; set; }
    public string ฝ่ายผู้แจ้ง { get; set; }
    public string รายละเอียดรายการ { get; set; }
    public string วันที่รับงาน { get; set; }
    public string เจ้าหน้าที่รับงาน { get; set; }
    public string ความคิดเห็นเจ้าหน้าที่รับงาน { get; set; }
    public string วันที่ปิดงาน { get; set; }
    public string เจ้าหน้าที่ปิดงาน { get; set; }
    public string ความคิดเห็นเจ้าหน้าที่ปิดงาน { get; set; }
    public string เวลาในการดำเนินการ { get; set; }
    public string สถานะดำเนินการ { get; set; }
}