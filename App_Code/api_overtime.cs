﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.Configuration;

/// <summary>
/// Summary description for api_roombooking
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_overtime : System.Web.Services.WebService
{

    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_overtime _data_overtime = new data_overtime();
    service_mail _serviceMail = new service_mail();

    string _local_xml = "";
    string _mail_subject = "";
    string _mail_body = "";

    string email_cemp = "";
    string email_hr = "";
    string email_admin = "";
    string email_admin_test = "";

    string email_hrtest = "";
    string email_hr_check = "";

    string email_cemptest = "";
    string email_headtest = "";

    string email_head = "";

    string email_emp = "";
    string email_emptest = "";

    string email_admintest = "";

    string link_system = "http://mas.taokaenoi.co.th/hr-overtime";
    string link_system_shiftrotate = "http://mas.taokaenoi.co.th/hr-overtime_rotate";

    public api_overtime()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    // create ot month
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCreateOTMonth(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_overtime _data_overtime = new data_overtime();
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 110); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _local_xml);

            if (_data_overtime.return_code == 0) //user create ot month
            {
                //check e-mail null
                if (_data_overtime.ovt_u0_document_list[0].emp_email != null && _data_overtime.ovt_u0_document_list[0].emp_email != "-")
                {
                    email_cemptest = "waraporn.teoy@gmail.com";
                    email_cemp = _data_overtime.ovt_u0_document_list[0].emp_email.ToString(); //ของจริง
                }

                if (_data_overtime.ovt_u0_document_list[0].emp_email != null && _data_overtime.ovt_u0_document_list[0].emp_email_headuser != "-")
                {
                    email_headtest = "teoy_42@hotmail.com";
                    email_head = _data_overtime.ovt_u0_document_list[0].emp_email_headuser.ToString(); //ของจริง
                }


                email_hr = "webmaster@taokaenoi.co.th "; //ใช้ทดสอบ hr
                //email_hr = "hr_admin_center@taokaenoi.co.th,nuchanart.ar@taokaenoi.co.th"; //ใช้ทดสอบ 

                //string email = email_hr + "," + email_cemptest + "," + email_headtest; //ใช้ทดสอบ 
                string email = email_cemp + "," + email_head + "," + email_hr; // ของจริง
                ovt_u0_document_detail _temp_u0 = _data_overtime.ovt_u0_document_list[0];
                //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการจองห้องประชุม";
                _mail_subject = "[HR : Overtime] - แจ้งรายการขอ OT พนักงานกะคงที่";
                _mail_body = _serviceMail.SentMailCreateOTMonth(_data_overtime.ovt_u0_document_list[0], link_system);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);
            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set ot month by head user
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetOTMontHeadUser(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 111); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // create ot day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCreateGroupOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 112); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // create Set Import Employee OTDay
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetImportEmployeeOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 113); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // import group ot day by admin
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetGroupOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 120); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set insert ot by admin 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCreateOTByAdmin(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_overtime _data_overtime = new data_overtime();
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 130); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _local_xml);

            if (_data_overtime.return_code == 0) //user create ot month
            {
                int count = _data_overtime.ovt_u1_document_list.Length;
                int i = 0;
                foreach (var data in _data_overtime.ovt_u1_document_list)
                {
                    //email_hr = "waraporn.teoy@gmail.com";
                    //email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 

                    string u1_docidx = _data_overtime.ovt_u1_document_list[i].u0_doc1_idx.ToString();
                    string link_u1 = link_system;

                    //mail emp ot
                    if (_data_overtime.ovt_u1_document_list[i].emp_email != null && _data_overtime.ovt_u1_document_list[i].emp_email != "-")
                    {
                        email_emptest = "teoy_42@hotmail.com"; //ใช้ทดสอบ
                        email_emp = _data_overtime.ovt_u1_document_list[i].emp_email.ToString(); //ของจริง
                    }

                    //head user
                    if (_data_overtime.ovt_u1_document_list[i].emp_email_headuser != null && _data_overtime.ovt_u1_document_list[i].emp_email_headuser != "-")
                    {
                        email_headtest = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ
                        email_head = _data_overtime.ovt_u1_document_list[i].emp_email_headuser.ToString(); //ของจริง
                    }

                    //hr
                    if (_data_overtime.ovt_u1_document_list[i].emp_email_hr != null && _data_overtime.ovt_u1_document_list[i].emp_email_hr != "-")
                    {
                        email_hrtest = "webmaster@taokaenoi.co.th"; //ใช้ทดสอบ
                        email_hr = _data_overtime.ovt_u1_document_list[i].emp_email_hr.ToString(); //ของจริง
                    }

                    //admin
                    if (_data_overtime.ovt_u1_document_list[i].emp_email_admin != null && _data_overtime.ovt_u1_document_list[i].emp_email_admin != "-")
                    {
                        email_admintest = "webmaster@taokaenoi.co.th"; //ใช้ทดสอบ
                        email_admin = _data_overtime.ovt_u1_document_list[i].emp_email_admin.ToString(); //ของจริง
                    }


                    string email_u1 = email_emp + "," + email_head + "," + email_admintest; //ใช้ทดสอบ
                                                                                            //string email = email_cemp + "," + _email_qa; // ของจริง
                    ovt_u1_document_detail _temp_u1 = _data_overtime.ovt_u1_document_list[i];
                    _mail_subject = "[HR : Overtime ] - แจ้งรายการขอ OT (กะหมุนเวียน)";// + "" + email_emp + "" + email_head;
                    //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการถูกแทนที่การจองห้องประชุม";

                    _mail_body = _serviceMail.SentMailCreateOTDay(_data_overtime.ovt_u1_document_list[i], link_u1);

                    _serviceMail.SendHtmlFormattedEmailFull(email_u1, "", "", _mail_subject, _mail_body);

                    i++;
                }

            }
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // create ot rotate by admin
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCreateOTShiftRotate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_overtime _data_overtime = new data_overtime();
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 140); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _local_xml);

            if (_data_overtime.return_code == 0) //user create ot month
            {
                int count = _data_overtime.ovt_u1doc_rotate_list.Length;
                int i = 0;
                foreach (var data in _data_overtime.ovt_u1doc_rotate_list)
                {
                    //email_hr = "waraporn.teoy@gmail.com";
                    //email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 

                    string u1_docidx = _data_overtime.ovt_u1doc_rotate_list[i].u1_doc_idx.ToString();
                    string link_u1 = link_system_shiftrotate;

                    //mail emp userot
                    if (_data_overtime.ovt_u1doc_rotate_list[i].emp_email != null && _data_overtime.ovt_u1doc_rotate_list[i].emp_email != "-")
                    {
                        email_emptest = "teoy_42@hotmail.com"; //ใช้ทดสอบ
                        email_emp = _data_overtime.ovt_u1doc_rotate_list[i].emp_email.ToString(); //ของจริง
                    }

                    //head user 1/2
                    if (_data_overtime.ovt_u1doc_rotate_list[i].emp_email_headuser != null && _data_overtime.ovt_u1doc_rotate_list[i].emp_email_headuser != "-")
                    {
                        email_headtest = "webmaster@taokaenoi.co.th"; //ใช้ทดสอบ
                        email_head = _data_overtime.ovt_u1doc_rotate_list[i].emp_email_headuser.ToString(); //ของจริง
                    }

                    string email_u1 = email_emp + "," + email_head + "," + email_admintest; //ใช้ทดสอบ
                                                                                            //string email = email_cemp + "," + _email_qa; // ของจริง

                    ovt_u1doc_rotate_detail _temp_u1 = _data_overtime.ovt_u1doc_rotate_list[i];
                    _mail_subject = "[HR : Overtime ] - แจ้งรายการขอ OT (กะหมุนเวียน)";// + "" + email_emp + "" + email_head;

                    _mail_body = _serviceMail.SentMailCreateOTShiftRotate(_data_overtime.ovt_u1doc_rotate_list[i], link_u1);

                    _serviceMail.SendHtmlFormattedEmailFull(email_u1, "", "", _mail_subject, _mail_body);

                    i++;
                }

            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // select detail date in create ot month
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDateOTMonth(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail time scan ot month
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTimeScanOTMont(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 211); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail date scan ot month and leve online
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDateScanOTMont(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 212); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select time start in ot month
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTimeStartScanOTMont(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 213); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select edit time start and end in ot month
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetEditViewScanOTMont(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 214); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select view emp count hour wait approve
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetViewWaitApproveHourDayEmp(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 215); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail ot
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailOT(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail ot
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailOT1(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 220); // return w/ json "{\"data_overtime\": {\"return_code\": \"0\",\"return_msg\": null } ,\"ovt_u0_document_list\": [{\"u0_doc_idx\": \"264\",\"m0_group_idx\": \"0\",\"type_idx\": \"0\",\"ovt_detail\": null,\"u0_ovt_status\": \"0\",\"emp_type\": \"2\",\"cemp_idx\": \"1413\",\"create_date_sort\": \"2019-10-09T18:37:05.813\",\"create_date\": \"09/10/2019\",\"time_create_date\": \"18:37:05\",\"update_date\": \"11/10/2019\",\"m0_node_idx\": \"3\",\"m0_actor_idx\": \"3\",\"staidx\": \"4\",\"month_ot\": \"9\",\"month_ot_name\": \"กันยายน\",\"sumhours\": \"0\",\"actor_name\": \"ฝ่ายทรัพยากรบุคคล\",\"status_name\": \"รอตรวจสอบ OT\",\"node_name\": \"พิจารณาผล\",\"current_status\": \"รอตรวจสอบ OT พิจารณาผล โดย ฝ่ายทรัพยากรบุคคล\",\"emp_code\": \"58000106\",\"emp_name_en\": \"Waraporn Bumrungkit\",\"emp_name_th\": \"นางสาว วราภรณ์ บำรุงกิจ\",\"org_idx\": \"1\",\"org_name_en\": \"Taokaenoi Food & Marketing PCL.\",\"org_name_th\": \"บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด (มหาชน)\",\"rdept_idx\": \"20\",\"dept_name_en\": \"MIS\",\"dept_name_th\": \"การจัดการระบบสารสนเทศ\",\"rsec_idx\": \"210\",\"sec_name_en\": \"Application Development\",\"sec_name_th\": \"พัฒนาแอพพลิเคชั่น\",\"rpos_idx\": \"1019\",\"pos_name_th\": \"เจ้าหน้าที่พัฒนาแอพพลิเคชั่น\",\"emp_email\": \"webmaster@taokaenoi.co.th\",\"costcenter_idx\": \"1\",\"costcenter_no\": \"106100\"}]}";

            //if(_ret_val == null)
            //{
            //_ret_val = "{\"data_overtime\": {\"return_code\": \"0\",\"return_msg\": null } ,\"ovt_u0_document_list\": [{\"u0_doc_idx\": \"265\",\"m0_group_idx\": \"0\",\"type_idx\": \"0\",\"ovt_detail\": null,\"u0_ovt_status\": \"0\",\"emp_type\": \"2\",\"cemp_idx\": \"1413\",\"create_date_sort\": \"2019-10-09T18:37:05.813\",\"create_date\": \"09/10/2019\",\"time_create_date\": \"18:37:05\",\"update_date\": \"11/10/2019\",\"m0_node_idx\": \"3\",\"m0_actor_idx\": \"3\",\"staidx\": \"4\",\"month_ot\": \"9\",\"month_ot_name\": \"กันยายน\",\"sumhours\": \"0\",\"actor_name\": \"ฝ่ายทรัพยากรบุคคล\",\"status_name\": \"รอตรวจสอบ OT\",\"node_name\": \"พิจารณาผล\",\"current_status\": \"รอตรวจสอบ OT พิจารณาผล โดย ฝ่ายทรัพยากรบุคคล\",\"emp_code\": \"58000106\",\"emp_name_en\": \"Waraporn Bumrungkit\",\"emp_name_th\": \"นางสาว วราภรณ์ บำรุงกิจ\",\"org_idx\": \"1\",\"org_name_en\": \"Taokaenoi Food & Marketing PCL.\",\"org_name_th\": \"บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด (มหาชน)\",\"rdept_idx\": \"20\",\"dept_name_en\": \"MIS\",\"dept_name_th\": \"การจัดการระบบสารสนเทศ\",\"rsec_idx\": \"210\",\"sec_name_en\": \"Application Development\",\"sec_name_th\": \"พัฒนาแอพพลิเคชั่น\",\"rpos_idx\": \"1019\",\"pos_name_th\": \"เจ้าหน้าที่พัฒนาแอพพลิเคชั่น\",\"emp_email\": \"webmaster@taokaenoi.co.th\",\"costcenter_idx\": \"1\",\"costcenter_no\": \"106100\"},{\"u0_doc_idx\": \"266\",\"m0_group_idx\": \"0\",\"type_idx\": \"0\",\"ovt_detail\": null,\"u0_ovt_status\": \"0\",\"emp_type\": \"2\",\"cemp_idx\": \"1413\",\"create_date_sort\": \"2019-10-09T18:37:05.813\",\"create_date\": \"09/10/2019\",\"time_create_date\": \"18:37:05\",\"update_date\": \"11/10/2019\",\"m0_node_idx\": \"3\",\"m0_actor_idx\": \"3\",\"staidx\": \"4\",\"month_ot\": \"9\",\"month_ot_name\": \"กันยายน\",\"sumhours\": \"0\",\"actor_name\": \"ฝ่ายทรัพยากรบุคคล\",\"status_name\": \"รอตรวจสอบ OT\",\"node_name\": \"พิจารณาผล\",\"current_status\": \"รอตรวจสอบ OT พิจารณาผล โดย ฝ่ายทรัพยากรบุคคล\",\"emp_code\": \"58000106\",\"emp_name_en\": \"Waraporn Bumrungkit\",\"emp_name_th\": \"นางสาว วราภรณ์ บำรุงกิจ\",\"org_idx\": \"1\",\"org_name_en\": \"Taokaenoi Food & Marketing PCL.\",\"org_name_th\": \"บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด (มหาชน)\",\"rdept_idx\": \"20\",\"dept_name_en\": \"MIS\",\"dept_name_th\": \"การจัดการระบบสารสนเทศ\",\"rsec_idx\": \"210\",\"sec_name_en\": \"Application Development\",\"sec_name_th\": \"พัฒนาแอพพลิเคชั่น\",\"rpos_idx\": \"1019\",\"pos_name_th\": \"เจ้าหน้าที่พัฒนาแอพพลิเคชั่น\",\"emp_email\": \"webmaster@taokaenoi.co.th\",\"costcenter_idx\": \"1\",\"costcenter_no\": \"106100\"}]}";
            //}
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail ot viiew
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailOTView(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 221); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail ot viiew log
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailOTViewLog(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 222); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select ot month view
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetOTMontView(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 223); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select ot wait approve ot month
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailWaitApproveOT(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 224); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select decision approve ot
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDecisionWaitApprove(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 225); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select decision approve ot
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetHeadUserSetOTMonth(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 226); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail import employee ot day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailEmployeeImportOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 227); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select view detail import employee ot day 26-25vday
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetViewDetailEmployeeImportOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 228); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select log view detail import employee ot day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLogViewDetailEmployeeImportOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 229); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select count approve all
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCountWaitApproveOT(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 230); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select Search Report DetailEmp OTDay
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchReportDetailEmpOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 231); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select count to admincreate ot shift type 2
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCountWaitApproveAdminCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 233); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select group detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailGroupOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 240); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select view group detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetViewDetailGroupOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 241); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select Type OT GROUP Day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTypeOTGroupOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 250); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select Type day work ot import day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTypeDayWorkOT(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 251); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select Type ot import day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTypeOTImport(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 252); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select Doc status in ot month
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDocStatusOTMonth(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 253); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail ot day by admin
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailOTDayAdmin(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 260); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select timestart ot day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTimeStartOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 261); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select check permission admin create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPermissionAdminCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 262); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select admin create ot day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailAdminCreateOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 263); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select admin create ot day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetViewDetailAdminCreateOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 264); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select gv admin create ot day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetViewDetailGvAdminOT(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 265); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail to employee shift
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailToEmployeeOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 266); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select view detail to employee shift
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetViewDetailToEmployeeOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 267); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select Log Detail EmployeeOTDay
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLogDetailEmployeeOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 268); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select wait approve head ot day admin create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetWaitApproveHeadOTDayAdminCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 270); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select Detail OTCreate ShiftRotate
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailOTCreateShiftRotate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1", _xml_in, 280); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select Detail create to admin 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailCreateToAdminShiftRotate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1", _xml_in, 281); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select Detail create to user 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailCreateToUserShiftRotate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1", _xml_in, 282); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select Log Detail shifttime rotate to user 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLogDetailCreateToUserShiftRotate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1", _xml_in, 283); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get detail wait approve ot shift rotate
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailApproveOTShiftRotate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1", _xml_in, 284); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get count wait approve ot shift rotate
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCountWaitApproveOTShiftRotate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1", _xml_in, 285); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get Status Search Detail ot shift rotate
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetStatusSearchOTShiftRotate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1", _xml_in, 286); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get User Search Detail ot shift rotate
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetUserSearchOTShiftRotate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1", _xml_in, 287); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get m0type shift rotate
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetM0ShiftTypeRotate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1", _xml_in, 288); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get detail to admin edit shift rotate
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailRotateAdminEdit(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1", _xml_in, 289); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get detail to admin edit shift rotate new
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDataDetailRotateAdminEdit(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 290); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get detail ddl_start to admin edit shift rotate new
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailRotateAdminStartEdit(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 291); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get detail ddl_end to admin edit shift rotate new
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailRotateAdminEndEdit(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 292); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get detail ddl_end to admin edit shift rotate new
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailRotateAdminEndEdit1(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn); //service_ovt_overtime1
            // execute and return
            //_ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 293); // return w/ json
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1", _xml_in, 293); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get detail ddl_end to admin edit shift rotate new
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailRotateAdminStartEdit1(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn); //service_ovt_overtime1
            // execute and return
            //_ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 293); // return w/ json
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1", _xml_in, 294); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // approve head user ot month
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetHeadUserApproveOTMonth(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_overtime _data_overtime = new data_overtime();
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 310); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _local_xml);

            if (_data_overtime.return_code == 0) //user create ot month
            {
                //sent mail ot month
                if (_data_overtime.ovt_u0_document_list[0].emp_type == 2)
                {
                    //check e-mail null
                    if (_data_overtime.ovt_u0_document_list[0].emp_email != null && _data_overtime.ovt_u0_document_list[0].emp_email != "-")
                    {
                        email_cemptest = "waraporn.teoy@gmail.com";
                        email_cemp = _data_overtime.ovt_u0_document_list[0].emp_email.ToString(); //ของจริง
                    }

                    if (_data_overtime.ovt_u0_document_list[0].emp_email != null && _data_overtime.ovt_u0_document_list[0].emp_email_headuser != "-")
                    {
                        email_headtest = "teoy_42@hotmail.com";
                        email_head = _data_overtime.ovt_u0_document_list[0].emp_email_headuser.ToString(); //ของจริง
                    }

                    email_hr = "webmaster@taokaenoi.co.th "; //ใช้ทดสอบ hr
                    //email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 

                    //string email = email_hr + "," + email_cemptest + "," + email_headtest; //ใช้ทดสอบ 
                    string email = email_cemp + "," + email_head + "," + email_hr; // ของจริง
                    ovt_u0_document_detail _temp_u0 = _data_overtime.ovt_u0_document_list[0];
                    //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการจองห้องประชุม";
                    _mail_subject = "[HR : Overtime] - แจ้งพิจารณารายการขอ OT พนักงานกะคงที่";
                    _mail_body = _serviceMail.SentMailHeadApproveOTMonth(_data_overtime.ovt_u0_document_list[0], link_system);

                    _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);
                }

            }


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //user edit ot month
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUserEditOTMonth(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_overtime _data_overtime = new data_overtime();
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 311); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _local_xml);

            if (_data_overtime.return_code == 0) //user create ot month
            {
                //check e-mail null
                if (_data_overtime.ovt_u0_document_list[0].emp_email != null && _data_overtime.ovt_u0_document_list[0].emp_email != "-")
                {
                    email_cemptest = "waraporn.teoy@gmail.com";
                    email_cemp = _data_overtime.ovt_u0_document_list[0].emp_email.ToString(); //ของจริง
                }

                if (_data_overtime.ovt_u0_document_list[0].emp_email != null && _data_overtime.ovt_u0_document_list[0].emp_email_headuser != "-")
                {
                    email_headtest = "teoy_42@hotmail.com";
                    email_head = _data_overtime.ovt_u0_document_list[0].emp_email_headuser.ToString(); //ของจริง
                }


                email_hr = "webmaster@taokaenoi.co.th"; //ใช้ทดสอบ hr
                //email_hr = "hr_admin_center@taokaenoi.co.th"; //จริง 

                string email = email_hr + "," + email_cemptest + "," + email_headtest; //ใช้ทดสอบ 
                //string email = email_cemp + "," + email_head + "," + email_hr; // ของจริง
                ovt_u0_document_detail _temp_u0 = _data_overtime.ovt_u0_document_list[0];
                //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการจองห้องประชุม";
                _mail_subject = "[HR : Overtime] - แจ้งแก้ไขเอกสารขอ OT พนักงานกะคงที่";
                _mail_body = _serviceMail.SentMailEditOTMonth(_data_overtime.ovt_u0_document_list[0], link_system);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);
            }


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Search detail ot month index
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchOTMonthIndex(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 312); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Search detail waitapprove ot month
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchWaitApproveOTMonth(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 313); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //set approve admincreate ot by headuer and hr approve
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetApproveOTAdminCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_overtime _data_overtime = new data_overtime();
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 320); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _local_xml);

            if (_data_overtime.return_code == 0) //user create ot month
            {
                int count = _data_overtime.ovt_u1_document_list.Length;
                int i = 0;
                foreach (var data in _data_overtime.ovt_u1_document_list)
                {
                    //email_hr = "waraporn.teoy@gmail.com";
                    //email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 

                    string u1_docidx = _data_overtime.ovt_u1_document_list[i].u0_doc1_idx.ToString();
                    string link_u1 = link_system;

                    //mail employee ot
                    if (_data_overtime.ovt_u1_document_list[i].emp_email != null && _data_overtime.ovt_u1_document_list[i].emp_email != "-")
                    {
                        email_emptest = "teoy_42@hotmail.com"; //ใช้ทดสอบ
                        email_emp = _data_overtime.ovt_u1_document_list[i].emp_email.ToString(); //ของจริง
                    }

                    //head user
                    if (_data_overtime.ovt_u1_document_list[i].emp_email_headuser != null && _data_overtime.ovt_u1_document_list[i].emp_email_headuser != "-")
                    {
                        email_headtest = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ
                        email_head = _data_overtime.ovt_u1_document_list[i].emp_email_headuser.ToString(); //ของจริง
                    }

                    //hr
                    if (_data_overtime.ovt_u1_document_list[i].emp_email_hr != null && _data_overtime.ovt_u1_document_list[i].emp_email_hr != "-")
                    {
                        email_hrtest = "webmaster@taokaenoi.co.th"; //ใช้ทดสอบ
                        email_hr = _data_overtime.ovt_u1_document_list[i].emp_email_hr.ToString(); //ของจริง
                    }

                    //admin
                    if (_data_overtime.ovt_u1_document_list[i].emp_email_admin != null && _data_overtime.ovt_u1_document_list[i].emp_email_admin != "-")
                    {
                        email_admintest = "webmaster@taokaenoi.co.th"; //ใช้ทดสอบ
                        email_admin = _data_overtime.ovt_u1_document_list[i].emp_email_admin.ToString(); //ของจริง
                    }


                    string email_u1 = email_emp + "," + email_head + "," + email_admintest; //ใช้ทดสอบ
                                                                                            //string email = email_cemp + "," + _email_qa; // ของจริง
                    ovt_u1_document_detail _temp_u1 = _data_overtime.ovt_u1_document_list[i];
                    _mail_subject = "[HR : Overtime] - แจ้งพิจารณารายการขอ OT (กะหมุนเวียน)";// + "," + email_emp + "," + email_head + "," + email_hr;
                    //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการถูกแทนที่การจองห้องประชุม";

                    _mail_body = _serviceMail.SentMailApproveOTDay(_data_overtime.ovt_u1_document_list[i], link_u1);

                    _serviceMail.SendHtmlFormattedEmailFull(email_u1, "", "", _mail_subject, _mail_body);

                    i++;
                }

            }


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set approve ot shift rotate
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetApproveOTShiftRotate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_overtime _data_overtime = new data_overtime();
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 330); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_overtime = (data_overtime)_funcTool.convertXmlToObject(typeof(data_overtime), _local_xml);

            if (_data_overtime.return_code == 0) //user create ot month
            {
                int count = _data_overtime.ovt_u1doc_rotate_list.Length;
                int i = 0;
                foreach (var data in _data_overtime.ovt_u1doc_rotate_list)
                {
                    //email_hr = "waraporn.teoy@gmail.com";
                    //email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 

                    string u1_docidx = _data_overtime.ovt_u1doc_rotate_list[i].u1_doc_idx.ToString();
                    string link_u1 = link_system_shiftrotate;

                    int node = _data_overtime.ovt_u1doc_rotate_list[i].m0_node_idx;
                    int actor = _data_overtime.ovt_u1doc_rotate_list[i].m0_actor_idx;

                    switch (node)
                    {
                        case 11: //admin edit

                            //mail emp ot
                            if (_data_overtime.ovt_u1doc_rotate_list[i].emp_email != null && _data_overtime.ovt_u1doc_rotate_list[i].emp_email != "-")
                            {
                                email_emptest = "teoy_42@hotmail.com"; //ใช้ทดสอบ
                                email_emp = _data_overtime.ovt_u1doc_rotate_list[i].emp_email.ToString(); //ของจริง
                            }

                            //admin
                            if (_data_overtime.ovt_u1doc_rotate_list[i].emp_email_admin != null && _data_overtime.ovt_u1doc_rotate_list[i].emp_email_admin != "-")
                            {
                                email_admin_test = "webmaster@taokaenoi.co.th"; //ใช้ทดสอบ
                                email_admin = _data_overtime.ovt_u1doc_rotate_list[i].emp_email_admin.ToString(); //ของจริง

                            }

                            break;
                        case 12: //head 1/2 approve

                            //mail emp ot
                            if (_data_overtime.ovt_u1doc_rotate_list[i].emp_email != null && _data_overtime.ovt_u1doc_rotate_list[i].emp_email != "-")
                            {
                                email_emptest = "teoy_42@hotmail.com"; //ใช้ทดสอบ
                                email_emp = _data_overtime.ovt_u1doc_rotate_list[i].emp_email.ToString(); //ของจริง
                            }

                            //head user
                            if (_data_overtime.ovt_u1doc_rotate_list[i].emp_email_headuser != null && _data_overtime.ovt_u1doc_rotate_list[i].emp_email_headuser != "-")
                            {
                                email_headtest = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ
                                email_head = _data_overtime.ovt_u1doc_rotate_list[i].emp_email_headuser.ToString(); //ของจริง
                            }

                            break;

                        case 13: //hr approve

                            //mail emp ot
                            if (_data_overtime.ovt_u1doc_rotate_list[i].emp_email != null && _data_overtime.ovt_u1doc_rotate_list[i].emp_email != "-")
                            {
                                email_emptest = "teoy_42@hotmail.com"; //ใช้ทดสอบ
                                email_emp = _data_overtime.ovt_u1doc_rotate_list[i].emp_email.ToString(); //ของจริง
                            }

                            //hr
                            if (_data_overtime.ovt_u1doc_rotate_list[i].emp_email_hr != null && _data_overtime.ovt_u1doc_rotate_list[i].emp_email_hr != "-")
                            {
                                email_hrtest = "webmaster@taokaenoi.co.th"; //ใช้ทดสอบ
                                email_hr = _data_overtime.ovt_u1doc_rotate_list[i].emp_email_hr.ToString(); //ของจริง
                            }

                            break;

                    }


                    string email_u1 = email_emp + "," + email_head + "," + email_hrtest; //ใช้ทดสอบ
                                                                                         //string email = email_cemp + "," + _email_qa; // ของจริง
                    ovt_u1doc_rotate_detail _temp_u1 = _data_overtime.ovt_u1doc_rotate_list[i];
                    _mail_subject = "[HR : Overtime ] - แจ้งพิจารณารายการขอ OT (กะหมุนเวียน)";// + "," + email_emp + "," + email_head + "," + email_hr;
                    //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการถูกแทนที่การจองห้องประชุม";

                    _mail_body = _serviceMail.SentMailApproveOTShiftRotate(_data_overtime.ovt_u1doc_rotate_list[i], link_u1);

                    _serviceMail.SendHtmlFormattedEmailFull(email_u1, "", "", _mail_subject, _mail_body);

                    i++;
                }

            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Search detail ot month
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReportOTMonth(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 410); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get report overtime all shift
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReportOvertime(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 411); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get view print report overtime all shift
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetViewPrintReportOvertime(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 412); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete Group OT Day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetDelGroupOTDay(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_overtime", "service_ovt_overtime1new", _xml_in, 910); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


}
