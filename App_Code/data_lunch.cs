using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_lunch")]
public class data_lunch
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_idx")]
    public int return_idx { get; set; }

    public lunch_meal_detail[] lunch_meal_list { get; set; }
    public search_meal_detail[] search_meal_list { get; set; }

    public lunch_meal_template_detail[] lunch_meal_template_list { get; set; }
}

public class lunch_meal_detail
{
    public int u0_idx { get; set; }
    public int emp_idx { get; set; }
    public string emp_code { get; set; }
    public int res_idx { get; set; }
    public int interval_min { get; set; }
    public int plant_idx { get; set; }

    public string create_date { get; set; }
    public string emp_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_th { get; set; }

    public int count_amount { get; set; }
}

public class search_meal_detail
{
    public string s_emp_idx { get; set; }
    public string s_emp_code { get; set; }
    public string s_res_idx { get; set; }
    public string s_start_date { get; set; }
    public string s_end_date { get; set; }
    public string s_report_type { get; set; }
    public string s_plant_idx { get; set; }
}

public class lunch_meal_template_detail
{
    public int emp_idx { get; set; }
    public string emp_code { get; set; }
    public string template_index { get; set; }
    public int plant_idx { get; set; }
    public int fid { get; set; }
}