﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_nmon")]
public class data_nmon
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }

    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }

    [XmlElement("BoxTopicNmonList")]
    public MasterTopicNmonList[] BoxTopicNmonList { get; set; }

    [XmlElement("BoxTypeTopicNmonList")]
    public MasterTypeTopicNmonList[] BoxTypeTopicNmonList { get; set; }

}

[Serializable]
public class MasterTypeTopicNmonList
{
    [XmlElement("TMIDX")]
    public int TMIDX { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("TypeMaster")]
    public string TypeMaster { get; set; }

    [XmlElement("TypeStatus")]
    public int TypeStatus { get; set; }

    [XmlElement("TypeTopicStatusDetail")]
    public string TypeTopicStatusDetail { get; set; }
}

[Serializable]
public class MasterTopicNmonList
{
    [XmlElement("m0idx")]
    public int m0idx { get; set; }

    [XmlElement("m0status")]
    public int m0status { get; set; }

    [XmlElement("TopicStatusDetail")]
    public string TopicStatusDetail { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("TMIDX")]
    public int TMIDX { get; set; }

    [XmlElement("TypeMaster")]
    public string TypeMaster { get; set; }

    [XmlElement("topic")]
    public string topic { get; set; }

}

