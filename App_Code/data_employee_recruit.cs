﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_employee_recruit")]
public class data_employee_recruit
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }



    // --- master data ---//

    public employee_vdo[] employee_vdo_list { get; set; }

    public topic_data[] topic_list { get; set; }

    public type_answer_data[] type_answer_list { get; set; }

    public question_data[] question_list { get; set; }

    public requset_more_data[] requset_more_list { get; set; }

    public recruit_u0_answer_data[] recruit_u0_answer_list { get; set; }

    public recruit_u1_answer_data[] recruit_u1_answer_list { get; set; }
}


public class employee_vdo
{

    public int rq_idx { get; set; }
    public int EmpIDX { get; set; }
    public int rq_vdo { get; set; }
    public string vdo_name { get; set; }
    public int vdo_d_edit { get; set; }
    public string identity_card { get; set; }
    public string FirstNameTH { get; set; }
    public string LastNameTH { get; set; }

    public int unidx { get; set; }
    public int acidx { get; set; }
    public int doc_decision { get; set; }
}

public class requset_more_data
{

    public int rq_idx { get; set; }

    public int EmpIDX { get; set; }

    public int rq_vdo { get; set; }

    public string vdo_name { get; set; }

    public int vdo_d_edit { get; set; }

    public string identity_card { get; set; }

    public string FirstNameTH { get; set; }

    public string LastNameTH { get; set; }

    public int rq_exam { get; set; }

    public int exam_status { get; set; }

    public int unidx { get; set; }

    public int acidx { get; set; }

    public int doc_decision { get; set; }

    public int rq_resmore { get; set; }

    public int resmore_status { get; set; }
}

public class topic_data
{

    public int m0_toidx { get; set; }

    public int m0_tqidx { get; set; }

    public string topic_name { get; set; }

    public int orgidx { get; set; }

    public int rdeptidx { get; set; }

    public int rsecidx { get; set; }

    public int rposidx { get; set; }

    public int status { get; set; }

    public int CEmpIDX { get; set; }

    public int UEmpIDX { get; set; }

    public string CreateDate { get; set; }

    public string UpdateDate { get; set; }
}

public class type_answer_data
{

    public int m0_taidx { get; set; }

    public string type_asn { get; set; }

    public int UEmpstatusIDX { get; set; }

}

public class question_data
{

    public int m0_quidx { get; set; }

    public int m0_toidx { get; set; }

    public int m0_taidx { get; set; }

    public int no_choice { get; set; }

    public string quest_name { get; set; }

    public string choice_a { get; set; }

    public string choice_b { get; set; }

    public string choice_c { get; set; }

    public string choice_d { get; set; }

    public int choice_ans { get; set; }

    public int status { get; set; }

    public int CEmpIDX { get; set; }

    public int UEmpIDX { get; set; }

    public string CreateDate { get; set; }

    public string UpdateDate { get; set; }


}

public class recruit_u0_answer_data
{

    public int u0_anidx { get; set; }

    public int m0_toidx { get; set; }

    public decimal total_score { get; set; }

    public int score_choice { get; set; }

    public decimal score_comment { get; set; }

    public int unidx { get; set; }

    public int acidx { get; set; }

    public int staidx { get; set; }

    public int node_decision { get; set; }

    public string determine { get; set; }

    public int status { get; set; }

    public int CEmpIDX { get; set; }

    public string CreateDate { get; set; }

    public int UEmpIDX { get; set; }

    public string UpdateDate { get; set; }
}


public class recruit_u1_answer_data
{

    public int u1_anidx { get; set; }

    public int m0_quidx { get; set; }

    public int m0_toidx { get; set; }

    public int u0_anidx { get; set; }

    public string answer { get; set; }

    public decimal score { get; set; }

    public int status { get; set; }

    public int CEmpIDX { get; set; }

    public string CreateDate { get; set; }

    public int UEmpIDX { get; set; }

    public string UpdateDate { get; set; }
}