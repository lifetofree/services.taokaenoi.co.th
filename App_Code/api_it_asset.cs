﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class api_it_asset : System.Web.Services.WebService
{

    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = String.Empty;
    string webmaseter = "webmaster@taokaenoi.co.th";

    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    service_mail _service_mail = new service_mail();
    data_itasset _dtitseet = new data_itasset();

    int _tempInt = 0;
    string _mail_subject = "";
    string _mail_body = "";
    string _link = "http://mas.taokaenoi.co.th/it-asset";
    string _link_trn = "http://mas.taokaenoi.co.th/it-asset-trnf";
    string _urlGetits_u_document = "Getits_u_document";

    public api_it_asset()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectSystem(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 201); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    #region Master Class
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_Master_Class(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 101); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Master_Class(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 202); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delete_Master_Class(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 901); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion


    #region Master Asset And Import IO
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_Master_AssetBuy(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 102); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Master_AssetBuy(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 203); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Master_YEARAssetBuy(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 215); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Master_LocationAssetBuy(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 216); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Update_Master_AssetBuy(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 301); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delete_Master_AssetBuy(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 902); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_Master_IOBuy(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 104); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Master_IOBuy(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 213); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Master_YearIOBuy(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 214); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Update_Master_IOBuy(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 303); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    #endregion

    #region Price Reference
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectSystem_Price(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 204); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectSystem_Type(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 205); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectTypeDevices_Ref(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 206); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectSoftware_Ref(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 207); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectDetailDeviceIT_Ref(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 208); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectHROffice_Ref(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectENMachine_Ref(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 211); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectQALab_Ref(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 238); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectPurchase_Ref(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 239); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectSearchPrice_Ref(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 212); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }



    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectDetailForEdit_Ref(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 209); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectPos(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 217); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectPermissionDevices(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 218); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Selectm0type(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 219); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Selectm1type(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Selectm1typewherem0(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 221); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectDevice(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 222); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectHardware_Memo(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 223); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectSoftware_Memo(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 224); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectMonitor_Memo(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 225); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectHistory_Memo(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 226); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectHistory_MemoPrint_BuyNormal(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 227); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectHistory_MemoPrintDetail_BuyNormal(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 228); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectUser_Buyreplace_normal(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 229); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectHistory_Software(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 230); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_DevicesGV(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 231); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_History_MemoSpecial(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 232); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_History_DetailMemoSpecial(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 233); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Approve_Memo(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 234); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_ApproveDetail_Memo(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 235); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Log_Memo(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 236); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Count_Memo(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 237); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_Reference(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 103); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_TypeDevices(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 105); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_Devices(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 106); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_Memo(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 107); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtitseet = (data_itasset)_funcTool.convertXmlToObject(typeof(data_itasset), _local_xml);

            if (_dtitseet.ReturnCode.ToString() == "0")
            {
                var Email = _dtitseet.boxu0memo_reference[0].Email;
                var doccode = _dtitseet.boxu0memo_reference[0].doccode;

                _mail_subject = "[Price Reference/ระบบราคาอ้างอิง] : " + doccode;
                _mail_body = _service_mail.PriceRef_BuySpecialBody(_dtitseet.boxu0memo_reference[0]);
                _service_mail.SendHtmlFormattedEmailFull(webmaseter, "", Email, _mail_subject, _mail_body);

            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Edit_Ref(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 302); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Update_Approve_Memo(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 304); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtitseet = (data_itasset)_funcTool.convertXmlToObject(typeof(data_itasset), _local_xml);

            if (_dtitseet.ReturnCode.ToString() == "0")
            {
                var Email = _dtitseet.boxu0memo_reference[0].Email;
                var doccode = _dtitseet.boxu0memo_reference[0].doccode;

                _mail_subject = "[Price Reference/ระบบราคาอ้างอิง] : " + doccode;
                _mail_body = _service_mail.PriceRef_BuySpecialBody(_dtitseet.boxu0memo_reference[0]);
                _service_mail.SendHtmlFormattedEmailFull(webmaseter, "", Email, _mail_subject, _mail_body);

            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delete_Ref(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 903); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeletePer(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 904); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteTypeDevices(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 905); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteDevices(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 906); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    #endregion

    //teppanop

    /****** start Master Data ******/
    // select // GET: odata/its_masterit
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getits_masterit(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/its_masterit(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getits_masterit_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/its_masterit
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsits_masterit(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put its_masterit
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdits_masterit(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1230); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete its_masterit
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delits_masterit(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1290); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/its_masterit(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetErrorits_masterit(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1240); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    /****** End Master Data ******/

    // start lookup
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getits_lookup(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1001); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    //end lookup

    //start transection

    /****** Start its_u_document_action ******/
    // select // GET: 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getits_u_document(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1120); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsits_u_document(string jsonIn)
    {
        string xml_in = "";
        string ret_val = "";
        string xml_in1 = "";
        string ret_val1 = "";
        int node_idx = 0;
        int actor_idx = 0;
        int flow_item = 0;
        int sysidx_admin = 0;
        if (jsonIn != null)
        {
            // convert to xml
            xml_in1 = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            ret_val1 = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", xml_in1, 1110); // return w/ json
            /***Send Email***/
            /*
            xml_in = xml_in1;
            ret_val = ret_val1;
            //xml_in = _funcTool.convertJsonToXml(ret_val);
            data_itasset _data = new data_itasset();
            data_itasset _dataitasset = new data_itasset();
            _data = (data_itasset)_funcTool.convertXmlToObject(typeof(data_itasset), xml_in);
            if (_data.its_u_document_action != null)
            {
                var item = _data.its_u_document_action[0];
                //ตรวจสอบข้อมูลขอซื้อเพื่อออกเลขทรัพย์สิน 
                if (
                    ((item.node_idx == 17) && (item.actor_idx == 10)
                    ||
                    (item.node_idx == 21) && (item.actor_idx == 10))
                    &&
                    (item.operation_status_id == "U2")
                )
                {
                    //อนุมัติ
                    if (item.approve_status == 5)
                    {
                        flow_item = 5;
                    }
                    else
                    {
                        flow_item = 4;
                    }
                    sysidx_admin = 0;
                    if (item.approve_status == 5)
                    {
                        _dataitasset = settemplate_asset_create(item.u0idx,
                                                            flow_item,
                                                            item.org_idx,
                                                            item.rdept_idx,
                                                            item.jobgrade_level_dir,
                                                            item.zstatus,
                                                            sysidx_admin,
                                                            item.node_idx,
                                                            item.actor_idx,
                                                            item.approve_status
                                                           );
                    }
                    else
                    {
                        _dataitasset = settemplate_asset_approve(item.u0idx, flow_item);
                    }  
                    

                }
            }
            */
        }



        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(ret_val1);
    }
    // Update // put 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdits_u_document(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1130); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delits_u_document(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1190); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // send email 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmailits_u_document(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            data_itasset _dataitasset = new data_itasset();
            _dataitasset = (data_itasset)_funcTool.convertXmlToObject(typeof(data_itasset), _xml_in);
            if (_dataitasset.its_u_document_action != null)
            {
                foreach (var item in _dataitasset.its_u_document_action)
                {
                    if (item.operation_status_id == "template_asset_create")
                    {
                        _dataitasset = settemplate_asset_create(item.u0idx, item.flow_item
                            , item.org_idx, item.rdept_idx, item.jobgrade_level_dir
                            , item.zstatus, item.sysidx_admin
                            );
                    }
                    else if (item.operation_status_id == "template_asset_approve")
                    {
                        _dataitasset = settemplate_asset_approve(item.u0idx, item.flow_item);
                    }
                    else if (item.operation_status_id == "template_asset_docket_create") //สรุปรายการขอซื้อ
                    {
                        _dataitasset = settemplate_asset_docket_create(item.u0_docket_idx
                            , item.org_idx, item.rdept_idx, item.jobgrade_level_dir
                            );
                    }
                    else if (item.operation_status_id == "template_asset_docket_approve")
                    {
                        _dataitasset = settemplate_asset_docket_approve(item.u0_docket_idx
                            , item.org_idx, item.rdept_idx, item.jobgrade_level_dir
                            );
                    }
                    else if (item.operation_status_id == "template_asset_docket_approvetouser")
                    {
                        _dataitasset = settemplate_asset_docket_approvetouser(item.u0_docket_idx);
                    }
                    else if (item.operation_status_id == "template_asset_delivery")
                    {
                        _dataitasset = settemplate_asset_delivery(item.u0_docket_idx, item.flow_item);
                    }
                }
            }
            _ret_val = _funcTool.convertObjectToJson(_dtitseet);
            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }

    /****** end its_u_document_action ******/


    //end transection


    //start email

    protected data_itasset callServicePostITAsset(string _cmdUrl, data_itasset _dtitseet)
    {

        //// convert to json
        _xml_in = _funcTool.convertObjectToXml(_dtitseet);
        //text.Text =  _cmdUrl + _localJson;
        if (_cmdUrl == "Getits_u_document")
        {
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1120); // return w/ json
        }

        //// call services
        //  _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;
        _xml_in = _funcTool.convertJsonToXml(_ret_val);
        ////// convert json to object
        _dtitseet = (data_itasset)_funcTool.convertXmlToObject(typeof(data_itasset), _xml_in);

        return _dtitseet;
    }


    private string getEmail_employee(int org_idx, int rdept_idx, int jobgrade_level, int type_item, int flow_item, int place_idx, int sysidx_admin)
    {
        string str = "";

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_document = new its_u_document();
        if (type_item == 1)
        {
            obj_document.operation_status_id = "permission_send_email_dir";
        }
        else if (type_item == 2)
        {
            obj_document.operation_status_id = "permission_send_email_flow_item";
        }
        else if (type_item == 3)
        {
            obj_document.operation_status_id = "permission_send_email_flow_item_1"; //permission_send_email_flow_item_1
        }
        else if (type_item == 12) // แจ้ง it ว่า user update io เสร็จแล้ว
        {
            obj_document.operation_status_id = "permission_send_email_flow_item_1";
        }
        else if (type_item == 8) //ผู้อำนวยการฝ่าย flow = 8
        {
            obj_document.operation_status_id = "permission_send_email_dir_8";
        }

        obj_document.org_idx = org_idx;
        obj_document.rdept_idx = rdept_idx;
        obj_document.joblevel = jobgrade_level;
        obj_document.flow_item = flow_item;
        obj_document.place_idx = place_idx;
        obj_document.sysidx_admin = sysidx_admin;
        _dataitasset.its_u_document_action[0] = obj_document;
        //str = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataitasset));
        _dataitasset = callServicePostITAsset(_urlGetits_u_document, _dataitasset);
        if (_dataitasset.its_u_document_action != null)
        {
            int ic = 0;
            foreach (var item in _dataitasset.its_u_document_action)
            {
                
                if (ic == 0)
                {
                    str = item.emp_email;
                }
                else
                {
                    str = str + " , " + item.emp_email;
                }
                
                ic++;
            }
        }
        //_link = str;
        setLink(str);

        return "";
    }

    public int zStringToInt(string _string)
    {
        int iBoolean = 0;
        if ((_string == null) || _string == "")
        {

        }
        else
        {
            iBoolean = int.Parse(_string);
        }
        return iBoolean;
    }

    private data_itasset settemplate_asset_create(
        int id, 
        int flow_item, 
        int org_idx, 
        int rdept_idx, 
        int jobgrade_level_dir, 
        string status, 
        int sysidx_admin,
        int node_idx = 0,
        int actor_idx = 0,
        int approve_status = 0
        )
    {
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_document = new its_u_document();

        if (status == "edit_assetno") // กับไปแก้ไขเลขที่ Asset no 
        {
            obj_document.operation_status_id = "template_asset_approve";
            obj_document.flow_item = 5;
        }
        else
        {
            obj_document.operation_status_id = "template_asset_create";
        }

        obj_document.u0idx = id;
        _dataitasset.its_u_document_action[0] = obj_document;
        _dataitasset = callServicePostITAsset(_urlGetits_u_document, _dataitasset);
        if (_dataitasset.its_u_document_action != null)
        {
            string emailmove = "";
            string replyempmove = "";// "seniordeveloper@taokaenoi.co.th";
            string title = _dataitasset.its_u_document_action[0].SysNameTH;
            // emailmove = "it_suppport@taokaenoi.co.th , senior_support@taokaenoi.co.th,seniordeveloper@taokaenoi.co.th";
            int ic = 0;
            if (flow_item == 2) //ขออนุมัติรายการขอซื้อโดย dir ฝ่าย
            {

                _mail_subject = "[MIS/Asset] - " + "แจ้งขอซื้ออุปกรณ์ " + title.Trim();
                
                emailmove = getEmail_employee(
                 org_idx
               , rdept_idx
               , jobgrade_level_dir
               , 1
               , flow_item
               , 0
               , sysidx_admin
               );
                //  }

                ic++;
            }
            else if (flow_item == 3) //ขออนุมัติรายการขอซื้อโดย budget
            {

                _mail_subject = "[MIS/Asset] - " + "แจ้งพิจารณารายการขอซื้ออุปกรณ์ " + title.Trim();

                emailmove = getEmail_employee(
                org_idx
                , rdept_idx
                , jobgrade_level_dir
                , 2
                , flow_item
                , 0
                , sysidx_admin
                );

                ic++;
            }
            else if (flow_item == 4) //ตรวจสอบข้อมูลขอซื้อเพื่อออกเลขทรัพย์สิน โดยเจ้าหน้าที่ asset
            {

                _mail_subject = "[MIS/Asset] - " + "แจ้งตรวจสอบข้อมูลขอซื้อเพื่อออกเลขทรัพย์สิน";
                int place_idx = _dataitasset.its_u_document_action[0].place_idx;
                if ((place_idx != 4) && (place_idx != 6))
                {
                    place_idx = 0;
                }
                if (status == "edit_assetno") // กับไปแก้ไขเลขที่ Asset no 
                {

                    emailmove = getEmail_employee(
                      _dataitasset.its_u_document_action[0].org_idx_its
                      , rdept_idx
                      , jobgrade_level_dir
                      , 2
                      , flow_item
                      , place_idx
                      , sysidx_admin
                      );

                }
                else
                {

                    emailmove = getEmail_employee(
                      _dataitasset.its_u_document_action[0].org_idx_its
                    , rdept_idx
                    , jobgrade_level_dir
                    , 2
                    , flow_item
                    , place_idx
                    , sysidx_admin
                    );

                }


                ic++;
            }
            else if ((flow_item == 5)) // อนุมัติ โดย dir asset
            {

                _mail_subject = "[MIS/Asset] - " + "แจ้งพิจารณารายการขอซื้ออุปกรณ์ " + title.Trim();

                emailmove = getEmail_employee(
                                                org_idx
                                                , rdept_idx
                                                , jobgrade_level_dir
                                                , 2
                                                , flow_item
                                                , _dataitasset.its_u_document_action[0].place_idx
                                                , sysidx_admin
                                                );


                ic++;
            }
            else if ((flow_item == 50) || (flow_item == 51) || (flow_item == 52))
            {
                _mail_subject = "[MIS/Asset] - " + "แจ้งพิจารณารายการขอซื้ออุปกรณ์ " + title.Trim();

                emailmove = getEmail_employee(
                                                0
                                                , rdept_idx
                                                , jobgrade_level_dir
                                                , 2
                                                , flow_item
                                                , 0
                                                , 0
                                                );
            }
            else if (flow_item == 6) // ตรวจสอบรายการขอซื้อโดยเจ้าหน้าที่ IT/HR/EN
            {

                _mail_subject = "[MIS/Asset] - " + "แจ้งตรวจสอบรายการขอซื้ออุปกรณ์ " + title.Trim();

                if ((sysidx_admin == 9) || (sysidx_admin == 37))
                {

                    emailmove = getEmail_employee(
                                 org_idx
                                , rdept_idx
                                , jobgrade_level_dir
                                , 2
                                , flow_item
                                , 0
                                , sysidx_admin
                                );

                }
                else
                {

                    emailmove = getEmail_employee(
                                    org_idx
                                   , rdept_idx
                                   , jobgrade_level_dir
                                   , 3
                                   , flow_item
                                   , 0
                                   , sysidx_admin
                                   );

                }

                ic++;
            }
            else if (flow_item == 7) // ขออนุมัติรายการขอซื้อโดย manager it/hr/rn
            {
                _mail_subject = "[MIS/Asset] - " + "แจ้งพิจารณารายการขอซื้ออุปกรณ์ " + title.Trim();

                int _plant_idx = 0;
                if ((sysidx_admin == 11) || (sysidx_admin == 36))
                {
                    _plant_idx = _dataitasset.its_u_document_action[0].place_idx;
                }

                emailmove = getEmail_employee(
                    org_idx
                  , rdept_idx
                  , jobgrade_level_dir
                  , 2
                  , flow_item
                  , _plant_idx
                  , sysidx_admin
                  );

                ic++;
            }
            else if (flow_item == 8) // ขออนุมัติรายการขอซื้อโดย director it/hr/rn
            {
                _mail_subject = "[MIS/Asset] - " + "แจ้งพิจารณารายการขอซื้ออุปกรณ์ " + title.Trim();

                emailmove = getEmail_employee(
        org_idx
      , rdept_idx
      , jobgrade_level_dir
      , 1
      , flow_item
      , 0
      , sysidx_admin
      );

                ic++;
            }
            else if (flow_item == 9) // สรุปรายการขอซื้อเพื่อให้ md approve it/hr/rn
            {
                _mail_subject = "[MIS/Asset] - " + "แจ้งสรุปรายการขอซื้ออุปกรณ์ " + title.Trim();

                emailmove = getEmail_employee(
        org_idx
      , rdept_idx
      , jobgrade_level_dir
      , 2
      , flow_item
      , 0
      , sysidx_admin
      );

                ic++;
            }
            else if ((flow_item == 12) && (status == "create_ionew")) // แก้ไขเลขที่ IO และราคาใหม่เส็จแล้ว
            {
                _mail_subject = "[MIS/Asset] - " + "แจ้งส่งมอบอุปกรณ์ " + title.Trim();
                flow_item = 11;

                emailmove = getEmail_employee(
        org_idx
      , rdept_idx
      , jobgrade_level_dir
      , 12
      , flow_item
      , 0
      , sysidx_admin
      );

                ic++;
            }
            if (status == "edit_assetno") // กับไปแก้ไขเลขที่ Asset no 
            {
                _mail_body = approve_asset(_dataitasset.its_u_document_action[0], title, _link);
            }
            else if (status == "create_ionew") // 
            {
                _mail_body = create_itasset_io(_dataitasset.its_u_document_action[0], title, _link);
            }
            else
            {
                _mail_body = create_itasset(_dataitasset.its_u_document_action[0], title, _link);
            }

            SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

        }
        return _dataitasset;

    }
    public string create_itasset(its_u_document create_itassetlist, string title, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_asset_create.html")))
        {
            body = reader.ReadToEnd();
        }

        body = body.Replace("{title_asset}", title);
        body = body.Replace("{document_code}", create_itassetlist.doccode);
        body = body.Replace("{empcode_asset}", create_itassetlist.emp_code);
        body = body.Replace("{fullname_asset}", create_itassetlist.emp_name_th);
        body = body.Replace("{name_asset}", create_itassetlist.name_purchase_type);
        body = body.Replace("{name_equipment}", create_itassetlist.type_name);
        body = body.Replace("{details_asset}", create_itassetlist.remark);
        body = body.Replace("{link}", link);
        body = body.Replace("{location_name}", create_itassetlist.place_name);

        return body;
    }

    //e-mail approve
    private data_itasset settemplate_asset_approve(int id, int flow_item)
    {
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_document = new its_u_document();
        obj_document.operation_status_id = "template_asset_approve";
        obj_document.u0idx = id;
        obj_document.flow_item = flow_item;
        _dataitasset.its_u_document_action[0] = obj_document;
        _dataitasset = callServicePostITAsset(_urlGetits_u_document, _dataitasset);
        if (_dataitasset.its_u_document_action != null)
        {
            string emailmove = _dataitasset.its_u_document_action[0].emp_email;
            setLink(_dataitasset.its_u_document_action[0].emp_email);

            string replyempmove = "";// "seniordeveloper@taokaenoi.co.th";
            string title = _dataitasset.its_u_document_action[0].SysNameTH;

            int ic = 0;
            // emailmove = "it_suppport@taokaenoi.co.th , senior_support@taokaenoi.co.th,seniordeveloper@taokaenoi.co.th";
            _mail_subject = "[MIS/Asset] - " + "การอนุมัติรายการขอซื้อ " + title.Trim();
            _mail_body = approve_asset(_dataitasset.its_u_document_action[0], title, _link);

            SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

        }
        return _dataitasset;
    }
    public void setLink(string str)
    {
        _link = str;
    }
    public string approve_asset(its_u_document create_itassetlist, string title, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_asset_approve.html")))
        {
            body = reader.ReadToEnd();
        }

        body = body.Replace("{document_code}", create_itassetlist.doccode);
        body = body.Replace("{name_asset}", create_itassetlist.name_purchase_type);
        body = body.Replace("{name_equipment}", create_itassetlist.type_name);
        body = body.Replace("{drept_name_asset}", create_itassetlist.dept_name_th);
        body = body.Replace("{node_status_asset}", create_itassetlist.zstatus_name);
        body = body.Replace("{actor_name_asset}", create_itassetlist.from_actor_name);
        body = body.Replace("{details_asset}", create_itassetlist.zapp_remark);
        body = body.Replace("{link}", link);
        body = body.Replace("{location_name}", create_itassetlist.place_name);

        return body;
    }
    //e-mail docket to md
    private data_itasset settemplate_asset_docket_create(int id, int org_idx, int rdept_idx, int jobgrade_level_dir)
    {
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_document = new its_u_document();
        obj_document.operation_status_id = "template_asset_docket_create";
        obj_document.zstatus = "create";
        obj_document.u0_docket_idx = id;
        _dataitasset.its_u_document_action[0] = obj_document;
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataitasset));
        _dataitasset = callServicePostITAsset(_urlGetits_u_document, _dataitasset);
        if (_dataitasset.its_u_document_action != null)
        {
            string emailmove = "";
            string replyempmove = ""; // "seniordeveloper@taokaenoi.co.th";
            string title = _dataitasset.its_u_document_action[0].SysNameTH;

            int ic = 0;
            // emailmove = "it_suppport@taokaenoi.co.th , senior_support@taokaenoi.co.th,seniordeveloper@taokaenoi.co.th";
            _mail_subject = "[MIS/Asset] - " + "แจ้งพิจารณารายการอุปกรณ์ที่ขอซื้อ ";

            emailmove = getEmail_employee(
       org_idx
     , rdept_idx
     , jobgrade_level_dir
     , 2
     , 10
     , 0
     , 0
     );

            _mail_body = create_asset_docket(_dataitasset.its_u_document_action[0], title, _link);

            SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

        }
        return _dataitasset;

    }
    public string create_asset_docket(its_u_document create_itassetlist, string title, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_asset_docket_create.html")))
        {
            body = reader.ReadToEnd();
        }

        body = body.Replace("{document_code}", create_itassetlist.doccode);
        body = body.Replace("{document_date}", zsetMonthYear(create_itassetlist.zmonth, create_itassetlist.zyear));
        body = body.Replace("{round}", create_itassetlist.docket_item.ToString());
        body = body.Replace("{type_name}", create_itassetlist.SysNameTH);
        body = body.Replace("{qty}", getformatfloat(create_itassetlist.total_qty.ToString(), 0));
        body = body.Replace("{link}", link);

        return body;
    }

    //e-mail docket provre to it
    private data_itasset settemplate_asset_docket_approve(int id, int org_idx, int rdept_idx, int jobgrade_level_dir)
    {
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_document = new its_u_document();
        obj_document.operation_status_id = "template_asset_docket_create";
        obj_document.zstatus = "approve";
        obj_document.u0_docket_idx = id;
        _dataitasset.its_u_document_action[0] = obj_document;
        _dataitasset = callServicePostITAsset(_urlGetits_u_document, _dataitasset);
        if (_dataitasset.its_u_document_action != null)
        {
            string emailmove = "";
            string replyempmove = "";// "seniordeveloper@taokaenoi.co.th";
            string title = _dataitasset.its_u_document_action[0].SysNameTH;
            int ic = 0;
            // emailmove = "it_suppport@taokaenoi.co.th , senior_support@taokaenoi.co.th,seniordeveloper@taokaenoi.co.th";
            _mail_subject = "[MIS/Asset] - " + "การอนุมัติรายการขอซื้อ  ";

            emailmove = getEmail_employee(
        org_idx
      , rdept_idx
      , jobgrade_level_dir
      , 3
      , 11
      , 0
      , _dataitasset.its_u_document_action[0].sysidx
      );

            _mail_body = approve_asset_docket(_dataitasset.its_u_document_action[0], title, _link);

            SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

        }
        return _dataitasset;

    }
    public string approve_asset_docket(its_u_document create_itassetlist, string title, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_asset_docket_approve.html")))
        {
            body = reader.ReadToEnd();
        }

        body = body.Replace("{document_code}", create_itassetlist.doccode);
        body = body.Replace("{document_date}", zsetMonthYear(create_itassetlist.zmonth, create_itassetlist.zyear));
        body = body.Replace("{round}", create_itassetlist.docket_item.ToString());
        body = body.Replace("{type_name}", create_itassetlist.SysNameTH);
        body = body.Replace("{qty}", getformatfloat(create_itassetlist.total_qty.ToString(), 0));
        body = body.Replace("{node_status_asset}", create_itassetlist.zstatus_name);
        body = body.Replace("{actor_name_asset}", create_itassetlist.from_actor_name);
        body = body.Replace("{details_asset}", create_itassetlist.zapp_remark);
        body = body.Replace("{link}", link);

        return body;
    }
    //e-mail docket provre to it
    private data_itasset settemplate_asset_docket_approvetouser(int id)
    {
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_document = new its_u_document();
        obj_document.operation_status_id = "template_asset_docket_approve";
        obj_document.u0_docket_idx = id;
        _dataitasset.its_u_document_action[0] = obj_document;
        _dataitasset = callServicePostITAsset(_urlGetits_u_document, _dataitasset);
        if (_dataitasset.its_u_document_action != null)
        {
            foreach (var _value in _dataitasset.its_u_document_action)
            {
                string emailmove = _dataitasset.its_u_document_action[0].emp_email;
                string replyempmove = "";// "seniordeveloper@taokaenoi.co.th";
                string title = _value.SysNameTH;

                int ic = 0;
                // emailmove = "it_suppport@taokaenoi.co.th , senior_support@taokaenoi.co.th,seniordeveloper@taokaenoi.co.th";
                _mail_subject = "[MIS/Asset] - " + "การอนุมัติรายการขอซื้อ ";
                _mail_body = approve_asset(_value, title, _link);


                SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }

        }
        return _dataitasset;
    }

    public string create_itasset_io(its_u_document create_itassetlist, string title, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_asset_create_ionew.html")))
        {
            body = reader.ReadToEnd();
        }

        body = body.Replace("{title_asset}", title);
        body = body.Replace("{document_code}", create_itassetlist.doccode);
        body = body.Replace("{empcode_asset}", create_itassetlist.emp_code);
        body = body.Replace("{fullname_asset}", create_itassetlist.emp_name_th);
        body = body.Replace("{name_asset}", create_itassetlist.name_purchase_type);
        body = body.Replace("{name_equipment}", create_itassetlist.type_name);
        body = body.Replace("{details_asset}", create_itassetlist.remark);
        body = body.Replace("{details_remark}", create_itassetlist.user_remark);
        body = body.Replace("{link}", link);
        body = body.Replace("{location_name}", create_itassetlist.place_name);

        return body;
    }



    //email test

    public void SendHtmlFormattedEmailFull(string recepientEmailList, string recepientCcEmailList, string replyToEmailList, string subject, string body)
    {
        recepientEmailList = "seniordeveloper@taokaenoi.co.th";
        recepientCcEmailList = "";
        recepientEmailList = "seniordeveloper@taokaenoi.co.th";
        _mail_subject = "TEST EMAIL";
        _service_mail.SendHtmlFormattedEmailFull(recepientEmailList, recepientCcEmailList, replyToEmailList, subject, body);

    }

    public string zsetMonthYear(int Month, int Year)
    {
        return zMonthTH(Month) + " " + Year.ToString();
    }
    public string zMonthTH(int AMonth)
    {
        string sMonth = "";
        if (AMonth == 1) { sMonth = "มกราคม"; }
        else if (AMonth == 2) { sMonth = "กุมภาพันธ์ "; }
        else if (AMonth == 3) { sMonth = "มีนาคม"; }
        else if (AMonth == 4) { sMonth = "เมษายน"; }
        else if (AMonth == 5) { sMonth = "พฤษภาคม"; }
        else if (AMonth == 6) { sMonth = "มิถุนายน"; }
        else if (AMonth == 7) { sMonth = "กรกฎาคม"; }
        else if (AMonth == 8) { sMonth = "สิงหาคม"; }
        else if (AMonth == 9) { sMonth = "กันยายน"; }
        else if (AMonth == 10) { sMonth = "ตุลาคม"; }
        else if (AMonth == 11) { sMonth = "พฤศจิกายน"; }
        else if (AMonth == 12) { sMonth = "ธันวาคม"; }
        return sMonth;
    }
    public string getformatfloat(string qty, int iformat)
    {
        if (qty == "")
        {
            qty = "0";
        }
        return string.Format("{0:n" + iformat.ToString() + "}", float.Parse(qty));
    }

    private data_itasset settemplate_asset_delivery(int id, int flow_item) // ส่งเมลหาเจ้าหน้าที่ asset
    {

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_document = new its_u_document();
        obj_document.operation_status_id = "document_asset_delivery";
        obj_document.u0_docket_idx = id;
        obj_document.flow_item = flow_item;
        _dataitasset.its_u_document_action[0] = obj_document;
        _dataitasset = callServicePostITAsset(_urlGetits_u_document, _dataitasset);
        if (_dataitasset.its_u_document_action != null)
        {
            foreach (var item in _dataitasset.its_u_document_action)
            {

                string emailmove = "";
                string replyempmove = "";// "seniordeveloper@taokaenoi.co.th";
                string title = item.SysNameTH;

                int ic = 0;
                //emailmove = "it_suppport@taokaenoi.co.th , senior_support@taokaenoi.co.th,seniordeveloper@taokaenoi.co.th , seniordeveloper@taokaenoi.co.th";
                _mail_subject = "[MIS/Asset] - " + "การส่งมอบเครื่องให้กับผู้ใช้งาน ";// + title.Trim();

                if ((flow_item == 14) || (flow_item == 1)) //แจ้งเพิ่ม price io ,ผลการส่งหมอบเครื่อง
                {

                    emailmove = item.emp_email;

                }
                else
                {
                    //ส่งหาเจ้าหน้าที่ asset
                    emailmove = getEmail_employee(0, 0, 0, 2, 4, 0, 0);
                }

                _mail_body = asset_delivery(item, title, _link);

                SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);
            }

        }

        return _dataitasset;
    }
    public string asset_delivery(its_u_document create_itassetlist, string title, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_asset_delivery.html")))
        {
            body = reader.ReadToEnd();
        }

        body = body.Replace("{document_code}", create_itassetlist.doccode);
        body = body.Replace("{fullname_purchase}", create_itassetlist.emp_name_th);
        body = body.Replace("{name_asset}", create_itassetlist.name_purchase_type);
        body = body.Replace("{name_equipment}", create_itassetlist.type_name);
        body = body.Replace("{drept_name_asset}", create_itassetlist.dept_name_th);
        body = body.Replace("{ionum}", create_itassetlist.ionum);
        body = body.Replace("{asset_no}", create_itassetlist.asset_no);
        body = body.Replace("{actor_name_asset}", create_itassetlist.pr_name);
        body = body.Replace("{details_asset}", create_itassetlist.pr_remark);
        body = body.Replace("{link}", link);

        return body;
    }

    //end email


    /****** Start its_u_transfer_action ******/
    // select // GET: 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getits_u_transfer(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1320); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsits_u_transfer(string jsonIn)
    {
        string ret_val = "";
        string xml_in = "";
        if (jsonIn != null)
        {
            // convert to xml
            xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", xml_in, 1310); // return w/ json
            xml_in = _funcTool.convertJsonToXml(ret_val);
            data_itasset _data = new data_itasset();
            _data = (data_itasset)_funcTool.convertXmlToObject(typeof(data_itasset), xml_in);
            if (_data.its_TransferDevice_u0_action != null)
            {
                foreach (var item in _data.its_TransferDevice_u0_action)
                {

                    int u0idx = item.u0_transf_idx;
                    _data = settemplate_transfer(u0idx, item.node_idx, item.actor_idx);

                }
            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(ret_val);
    }
    // Update // put 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdits_u_transfer(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1330); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delits_u_transfer(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1390); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }



    #region send email transfer
    /*
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void send_email_transfer(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            data_itasset _data = new data_itasset();
            _data = (data_itasset)_funcTool.convertXmlToObject(typeof(data_itasset), _xml_in);
            if (_data.its_TransferDevice_u0_action != null)
            {
                foreach (var item in _data.its_TransferDevice_u0_action)
                {

                    int u0idx = item.u0_transf_idx;
                    _data = settemplate_transfer(u0idx, item.node_idx, item.actor_idx);

                }
            }
            
        }

    }
    */
    private data_itasset settemplate_transfer(int id, int node_idx, int actor_idx)
    {
        data_itasset _data = new data_itasset();
        _data.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 obj_document = new its_TransferDevice_u0();
        obj_document.u0_transf_idx = id;
        obj_document.node_idx = node_idx;
        obj_document.actor_idx = actor_idx;
        obj_document.operation_status_id = "list_datasend_email";
        _data.its_TransferDevice_u0_action[0] = obj_document;
        _data = callServicePostTransfer(_data);
        if (_data.its_TransferDevice_u0_action != null)
        {
            var v_item = _data.its_TransferDevice_u0_action[0];
            string emailmove = "";// "seniordeveloper@taokaenoi.co.th";
            string replyempmove = "seniordeveloper@taokaenoi.co.th";
            string title = "";

            if ((v_item.node_idx == 2) && (v_item.actor_idx == 3) && (v_item.doc_status == 0))//ดำเนินการ พิจารณาผล(ผู้สร้าง) โดย Manager
            {
                _mail_subject = "[โอนย้ายอุปกรณ์] ผู้สร้าง";
                emailmove = getEmail_employee_Transfer(v_item.rdept_idx, 1, 0, 0, 0, 0);
                _mail_body = databody_Transfer(_data.its_TransferDevice_u0_action[0], title, _link_trn);
                trnf_SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            else if ((v_item.node_idx == 3) && (v_item.actor_idx == 4) && (v_item.doc_status == 9))//ดำเนินการ พิจารณาผล(ผู้สร้าง) โดย Director
            {
                _mail_subject = "[โอนย้ายอุปกรณ์] ผู้สร้าง";
                emailmove = getEmail_employee_Transfer(v_item.rdept_idx, 2, 0, 0, 0, 0);
                _mail_body = databody_Transfer(_data.its_TransferDevice_u0_action[0], title, _link_trn);
                trnf_SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            else if ((v_item.node_idx == 4) && (v_item.actor_idx == 2) && (v_item.doc_status == 9))//ดำเนินการ พิจารณาผล(ผู้รับ) โดย ผู้รับโอนย้าย
            {
                _mail_subject = "[โอนย้ายอุปกรณ์] ผู้รับโอนย้าย";
                emailmove = getEmail_employee_Transfer(v_item.rdept_idx, 4, v_item.rsec_idx, 0, 0, 0);
                // _link = _data.its_TransferDevice_u0_action[0].n_emp_email;
                _mail_body = databody_Transfer(_data.its_TransferDevice_u0_action[0], title, _link_trn);
                trnf_SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            else if ((v_item.node_idx == 5) && (v_item.actor_idx == 3) && (v_item.doc_status == 9))//ดำเนินการ พิจารณาผล(ผู้รับ) โดย Manager
            {
                _mail_subject = "[โอนย้ายอุปกรณ์] ผู้รับ";
                emailmove = getEmail_employee_Transfer(v_item.n_rdept_idx, 1, 0, 0, 0, 0);
                _mail_body = databody_Transfer(_data.its_TransferDevice_u0_action[0], title, _link_trn);
                trnf_SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            else if ((v_item.node_idx == 6) && (v_item.actor_idx == 4) && (v_item.doc_status == 9))//ดำเนินการ พิจารณาผล(ผู้รับ) โดย Director
            {
                _mail_subject = "[โอนย้ายอุปกรณ์] ผู้รับ";
                emailmove = getEmail_employee_Transfer(v_item.n_rdept_idx, 2, 0, 0, 0, 0);
                _mail_body = databody_Transfer(_data.its_TransferDevice_u0_action[0], title, _link_trn);
                trnf_SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            else if (
                (((v_item.node_idx == 14) && (v_item.actor_idx == 10)) || ((v_item.node_idx == 15) && (v_item.actor_idx == 11))) &&
                (v_item.doc_status == 9)
                )//ดำเนินการ พิจารณาผล โดย เจ้าหน้าที่ EN / QA
            {
                _mail_subject = "[โอนย้ายอุปกรณ์] เจ้าหน้าที่ของต้นสังกัด";
                emailmove = getEmail_employee_Transfer(v_item.n_rdept_idx, 5, 0, 0, 7, v_item.sysidx);
                _mail_body = databody_Transfer(_data.its_TransferDevice_u0_action[0], title, _link_trn);
                trnf_SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            else if (
                (((v_item.node_idx == 14) && (v_item.actor_idx == 12)) || ((v_item.node_idx == 15) && (v_item.actor_idx == 14))) &&
                (v_item.doc_status == 9)
                )//ดำเนินการ พิจารณาผล โดย Manager EN / QA
            {
                _mail_subject = "[โอนย้ายอุปกรณ์] Manager ต้นสังกัด";
                emailmove = getEmail_employee_Transfer(v_item.n_rdept_idx, 6, 0, 0, 8, v_item.sysidx);
                _mail_body = databody_Transfer(_data.its_TransferDevice_u0_action[0], title, _link_trn);
                trnf_SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            /*
            else if (
                (((v_item.node_idx == 14) && (v_item.actor_idx == 13)) || ((v_item.node_idx == 15) && (v_item.actor_idx == 15))) &&
                (v_item.doc_status == 9)
                )//ดำเนินการ พิจารณาผล โดย Manager EN / QA
            {
                _mail_subject = "[โอนย้ายอุปกรณ์] Director ต้นสังกัด";
                // emailmove = 
                // getEmail_employee_Transfer(v_item.n_rdept_idx, 5, 0, 0, 7, v_item.sysidx);
                int isysidx = 0;
                if(v_item.sysidx == 36) // QA
                {
                    isysidx = 27;
                }
                else
                {
                    isysidx = v_item.sysidx;
                }
                getEmail_employee_Transfer(isysidx, 2, 0, 0, 0, 0);
                _mail_body = databody_Transfer(_data.its_TransferDevice_u0_action[0], title, _link);
                trnf_SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            */
            else if (((v_item.node_idx == 7) || (v_item.node_idx == 16)) && (v_item.actor_idx == 5) && (v_item.doc_status == 9))//ดำเนินการ พิจารณาผล(Asset) โดย เจ้าหน้าที่ Asset
            {
                _mail_subject = "[โอนย้ายอุปกรณ์] เจ้าหน้าที่ Asset";
                emailmove = getEmail_employee_Transfer(0, 3, 0, 0, 10, 0);
                _mail_body = databody_Transfer(_data.its_TransferDevice_u0_action[0], title, _link_trn);
                trnf_SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            else if ((v_item.node_idx == 8) && (v_item.actor_idx == 3) && (v_item.doc_status == 9))//ดำเนินการ พิจารณาผล(Asset) โดย Manager Asset
            {
                _mail_subject = "[โอนย้ายอุปกรณ์] Manager Asset";
                emailmove = getEmail_employee_Transfer(0, 3, 0, v_item.n_place_idx, 11, 0);
                _mail_body = databody_Transfer(_data.its_TransferDevice_u0_action[0], title, _link_trn);
                trnf_SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            /*
            else if ((v_item.node_idx == 10) && (v_item.actor_idx == 9) && (v_item.doc_status == 9))//ดำเนินการ พิจารณาผล(Asset) โดย Director
            {

                 _mail_subject = "[โอนย้ายอุปกรณ์] Director Asset";
                // emailmove = 
                getEmail_employee_Transfer(9, 2, 0, 0, 0, 0);
                _mail_body = databody_Transfer(_data.its_TransferDevice_u0_action[0], title, _link);
                trnf_SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);
                
            }*/
            else if ((v_item.node_idx == 9) && (v_item.actor_idx == 9) && (v_item.doc_status == 3))//ดำเนินการ พิจารณาผล(Asset) โดย Director
            {

                _mail_subject = "[โอนย้ายอุปกรณ์] ผลการดำเนินการโอนย้ายอุปกรณ์";

                //string email1 = _data.its_TransferDevice_u0_action[0].emp_email;
                emailmove = getEmail_employee_Transfer(v_item.n_rdept_idx, 2, 0, 0, 0, 0);
                //string email2 = _link;
                // _link = email1 + "," + email2;
                _mail_body = databody_Transfer(_data.its_TransferDevice_u0_action[0], title, _link_trn);
                trnf_SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }

            else if ((v_item.node_idx == 13) && (v_item.actor_idx == 1))//ดำเนินการ แก้ไข โดย ผู้สร้าง
            {
                _mail_subject = "[โอนย้ายอุปกรณ์] แก้ไขเอกสาร";
                emailmove = _data.its_TransferDevice_u0_action[0].emp_email;
                _mail_body = databody_Transfer(_data.its_TransferDevice_u0_action[0], title, _link_trn);
                trnf_SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }


        }
        return _data;

    }
    public string databody_Transfer(its_TransferDevice_u0 create_list, string title, string link, int iFlagSafety = 0)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_asset_transfer_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{title_name}", title);
        body = body.Replace("{doc_code}", create_list.doccode);
        body = body.Replace("{doc_date}", create_list.zdocdate);
        body = body.Replace("{type_name}", create_list.sys_name);
        body = body.Replace("{from_name}", create_list.emp_name_th);
        body = body.Replace("{to_name}", create_list.n_emp_name_th);
        body = body.Replace("{actor_name}", create_list.to_name);
        body = body.Replace("{link}", link);

        return body;
    }
    protected data_itasset callServicePostTransfer(data_itasset _dtitseet)
    {

        //// convert to json
        _xml_in = _funcTool.convertObjectToXml(_dtitseet);
        //text.Text =  _cmdUrl + _localJson;
        _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1320); // return w/ json

        //// call services
        //  _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;
        _xml_in = _funcTool.convertJsonToXml(_ret_val);
        ////// convert json to object
        _dtitseet = (data_itasset)_funcTool.convertXmlToObject(typeof(data_itasset), _xml_in);

        return _dtitseet;
    }
    public void trnf_SendHtmlFormattedEmailFull(string recepientEmailList, string recepientCcEmailList, string replyToEmailList, string subject, string body)
    {
        recepientEmailList = "seniordeveloper@taokaenoi.co.th";
        recepientCcEmailList = "";
        recepientEmailList = "seniordeveloper@taokaenoi.co.th";
        _mail_subject = "TEST EMAIL";
        _service_mail.SendHtmlFormattedEmailFull(recepientEmailList, recepientCcEmailList, replyToEmailList, subject, body);


    }

    private string getEmail_employee_Transfer(int _rdept_idx, int status, int _rsec_idx, int place_idx, int _flow_item, int _sysidx)
    {
        data_itasset _data = new data_itasset();
        _data.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 obj_document = new its_TransferDevice_u0();
        //obj_document.emp_idx = _empidx;
        obj_document.rdept_idx = _rdept_idx;
        obj_document.rsec_idx = _rsec_idx;

        if (status == 1)
        {
            obj_document.operation_status_id = "perss_send_email_mg";
        }
        else if (status == 2)
        {
            obj_document.operation_status_id = "perss_send_email_dir";
        }
        else if (status == 3)
        {
            obj_document.operation_status_id = "perss_send_email_dept_admin";
            obj_document.flow_item = _flow_item;
            obj_document.place_idx = place_idx;
        }
        else if (status == 4) // office
        {
            obj_document.operation_status_id = "list_datasend_email_sec";
        }
        else if (status == 5) //office Agency
        {
            obj_document.operation_status_id = "perss_send_email_dept_admin_office_agency";
            obj_document.flow_item = _flow_item;
            obj_document.sysidx_admin = _sysidx;
            obj_document.place_idx = place_idx;
        }
        else if (status == 6) //Manager Agency
        {
            obj_document.operation_status_id = "perss_send_email_dept_admin_mg_agency";
            obj_document.flow_item = _flow_item;
            obj_document.sysidx_admin = _sysidx;
            obj_document.place_idx = place_idx;
        }
        _data.its_TransferDevice_u0_action[0] = obj_document;
        string str = "";
        _xml_in = _funcTool.convertObjectToXml(_data);
        _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1320);
        _xml_in = _funcTool.convertJsonToXml(_ret_val);
        _data = (data_itasset)_funcTool.convertXmlToObject(typeof(data_itasset), _xml_in);
        if (_data.its_TransferDevice_u0_action != null)
        {
            int ic = 0;
            foreach (var item in _data.its_TransferDevice_u0_action)
            {

                if (ic == 0)
                {
                    str = item.emp_email;
                }
                else
                {
                    str = str + " , " + item.emp_email;
                }

                ic++;
            }
        }
        //_link = str;
        return str;
    }

    #endregion send email transfer


    //***SCB***//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SCBSandboxCallback(string jsonIn)
    {
        //if (jsonIn != null)
        //{
        //    // convert to xml
        //    _xml_in = _funcTool.convertJsonToXml(jsonIn);
        //    // execute and return
        //    _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 201); // return w/ json
        //}

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

}
