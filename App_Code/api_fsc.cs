using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for api_log
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_fsc : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_fsc _data_fsc = new data_fsc();

    public api_fsc()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    // search random employee list
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SearchRandomList(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_fsc", "service_fsc_random", _xml_in, 10); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set random employee list
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRandomList(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_fsc", "service_fsc_random", _xml_in, 11); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set random employee list
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRandomList(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_fsc", "service_fsc_random", _xml_in, 12); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set random survey status
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRandomSurveyStatus(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_fsc", "service_fsc_random", _xml_in, 21); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
}