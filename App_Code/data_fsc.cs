using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_fsc")]
public class data_fsc
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public fsc_random_detail[] fsc_random_list { get; set; }
    public search_fsc_detail[] search_fsc_list { get; set; }
}

[Serializable]
public class fsc_random_detail
{
    public int u0_idx { get; set; }

    public int emp_idx { get; set; }
    public string emp_code { get; set; }

    // employee name profile.
    public string prefix_th { get; set; }
    public string emp_name_en { get; set; }
    public string emp_name_th { get; set; }

    public string emp_firstname_en { get; set; }
    public string emp_lastname_en { get; set; }
    public string emp_firstname_th { get; set; }
    public string emp_lastname_th { get; set; }

    public string emp_nickname_en { get; set; }
    public string emp_nickname_th { get; set; }
    // employee name profile

    // employee organization profile
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int rpos_idx { get; set; }

    public int plant_idx { get; set; }
    public string plant_name { get; set; }

    public string org_name_en { get; set; }
    public string dept_name_en { get; set; }
    public string sec_name_en { get; set; }
    public string pos_name_en { get; set; }

    public string org_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_th { get; set; }
    public string pos_name_th { get; set; }
    // employee organization profile

    public int survey_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

[Serializable]
public class search_fsc_detail
{
    public string s_emp_type_idx { get; set; }
    public string s_plant_idx { get; set; }
    public string s_nat_idx { get; set; }
    public string s_rdept_idx { get; set; }
}