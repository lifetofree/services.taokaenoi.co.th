﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_employee")]
public class data_employee
{

    [XmlElement("return_code")]
    public string return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_value")]
    public string return_value { get; set; }
    [XmlElement("return_idx")]
    public string return_idx { get; set; }

    [XmlElement("return_ad")]
    public string return_ad { get; set; }

    [XmlElement("return_user")]
    public string return_user { get; set; }

    [XmlElement("return_datead")]
    public string return_datead { get; set; }

    [XmlElement("return_vpnsap")]
    public string return_vpnsap { get; set; }

    [XmlElement("return_datesap")]
    public string return_datesap { get; set; }

    [XmlElement("return_vpnbi")]
    public string return_vpnbi { get; set; }

    [XmlElement("return_datebi")]
    public string return_datebi { get; set; }

    [XmlElement("return_vpnexpress")]
    public string return_vpnexpress { get; set; }

    [XmlElement("return_usersap")]
    public string return_usersap { get; set; }

    [XmlElement("return_dateusersap")]
    public string return_dateusersap { get; set; }

    [XmlElement("return_dateexpress")]
    public string return_dateexpress { get; set; }

    [XmlElement("return_email")]
    public string return_email { get; set; }

    [XmlElement("return_dateemail")]
    public string return_dateemail { get; set; }


    [XmlElement("employee_list")]
    public employee_detail[] employee_list { get; set; }
    [XmlElement("employee_list_small")]
    public employee_detail_small[] employee_list_small { get; set; }

    [XmlElement("organization_list")]
    public organization_details[] organization_list { get; set; }
    [XmlElement("department_list")]
    public department_details[] department_list { get; set; }
    [XmlElement("section_list")]
    public section_details[] section_list { get; set; }
    [XmlElement("position_list")]
    public position_details[] position_list { get; set; }

    [XmlElement("search_key_emp_list")]
    public search_key_employee[] search_key_emp_list { get; set; }


    [XmlElement("version_mobile_list")]
    public Version[] version_mobile_list { get; set; }

    [XmlElement("hospital_list")]
    public Hospital[] hospital_list { get; set; }

    [XmlElement("BoxTel_list")]
    public Tel_List[] BoxTel_list { get; set; }

    [XmlElement("BoxHolder_list")]
    public Holder_List[] BoxHolder_list { get; set; }

    [XmlElement("BoxChild_list")]

    public Child_List[] BoxChild_list { get; set; }



    [XmlElement("BoxPrior_list")]

    public Prior_List[] BoxPrior_list { get; set; }



    [XmlElement("BoxEducation_list")]

    public Education_List[] BoxEducation_list { get; set; }



    [XmlElement("BoxTrain_list")]

    public Train_List[] BoxTrain_list { get; set; }



    [XmlElement("ODSPRelation_list")]

    public ODSP_List[] ODSPRelation_list { get; set; }

    [XmlElement("R0Address_list")]
    public Address_List[] R0Address_list { get; set; }

    [XmlElement("BoxReferent_list")]
    public Referent_List[] BoxReferent_list { get; set; }

    [XmlElement("BoxManage_Position_list")]
    public Manage_Position_List[] BoxManage_Position_list { get; set; }

    [XmlElement("Boxm1_personlist_topic")]
    public Manage_Position_List_Topic[] Boxm1_personlist_topic { get; set; }


    [XmlElement("BoxTypevisa_list")]
    public Typevisa_List[] BoxTypevisa_list { get; set; }

    [XmlElement("employee_list_tel")]
    public employee_detail_tel[] employee_list_tel { get; set; }

    [XmlElement("employee_list_report_time")]
    public employee_detail_time[] employee_list_report_time { get; set; }

    [XmlElement("ShiftTime_details")]
    public ShiftTime[] ShiftTime_details { get; set; }

    [XmlElement("Ipaddress_List")]
    public employee_Ipaddress[] Ipaddress_List { get; set; }
    [XmlElement("Asset_Type_List")]
    public Asset_Type_Detail[] Asset_Type_List { get; set; }

    [XmlElement("BoxPosision")]
    public BoxPosisionList[] BoxPosision { get; set; }
    [XmlElement("BoxSarary")]
    public BoxSararyList[] BoxSarary { get; set; }
    
    [XmlElement("Unit_Type_List")]
    public Unit_Type_Detail[] Unit_Type_List { get; set; }

    [XmlElement("Holder_List")]
    public Holder_Detail[] Holder_List { get; set; }
    [XmlElement("BoxBenefits")]
    public BoxBenefitsList[] BoxBenefits { get; set; }
    [XmlElement("BoxEmployeeTHlist")]
    public EmployeeTHlist[] BoxEmployeeTHlist { get; set; }
    [XmlElement("BoxEmployeeFRlist")]
    public EmployeeFRlist[] BoxEmployeeFRlist { get; set; }
    [XmlElement("BoxEmployeeHealthlist")]
    public EmployeeHealthlist[] BoxEmployeeHealthlist { get; set; }
    [XmlElement("EducationList")]
    public DetailEducation[] EducationList { get; set; }
    [XmlElement("CostCenterDetail")]
    public CostCenter[] CostCenterDetail { get; set; }
    [XmlElement("penalty_list")]
    public penalty_details[] penalty_list { get; set; }
    [XmlElement("Reference_persons")]
    public Reference_persons_list[] Reference_persons { get; set; }
    [XmlElement("select_emp")]
    public select_emp_list[] select_emp { get; set; }
    [XmlElement("box_experience_")]
    public experiencelist[] box_experience_ { get; set; }
    [XmlElement("position_change")]
    public position_changeList[] position_change { get; set; }

    [XmlElement("BoxEmployee_ProbationList")]
    public Employee_Probation[] BoxEmployee_ProbationList { get; set; }

    [XmlElement("BoxExport_ProbationList")]
    public Export_Probation[] BoxExport_ProbationList { get; set; }

    [XmlElement("BoxGurantee_list")]
    public Gurantee_list[] BoxGurantee_list { get; set; }

    [XmlElement("BoxEmployeeLeave_list")]
    public EmployeeLeave_list[] BoxEmployeeLeave_list { get; set; }

    [XmlElement("BoxEmployeeResign_list")]
    public EmployeeResign_list[] BoxEmployeeResign_list { get; set; }


    //re organization
    [XmlElement("cen_approve_u0_list")]
    public cen_approve_u0_detail[] cen_approve_u0_list { get; set; }

    [XmlElement("cen_position_emp_r0_list")]
    public cen_position_emp_r0_detail[] cen_position_emp_r0_list { get; set; }

    [XmlElement("employee_health_detail_list")]
    public employee_health_detail[] employee_health_detail_list { get; set; }

    [XmlElement("cen_employee_detail_list")]
    public cen_employee_detail[] cen_employee_detail_list { get; set; }


    //re organization

    [XmlElement("employee_list_v2")]
    public employee_detail_v2[] employee_list_v2 { get; set; }
    [XmlElement("employee_node_config")]
    public employee_node_config[] employee_node_config { get; set; }



    [XmlElement("m0_doc_decision")]
    public m0_doc_decision[] m0_doc_decision { get; set; }

    [XmlElement("requset_more_list")]
    public requset_more_in_emp[] requset_more_list { get; set; }


}


[Serializable]
public class requset_more_in_emp
{

    [XmlElement("rq_idx")]
    public int rq_idx { get; set; }
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }
    [XmlElement("rq_vdo")]
    public int rq_vdo { get; set; }
    [XmlElement("vdo_name")]
    public string vdo_name { get; set; }
    [XmlElement("vdo_d_edit")]
    public int vdo_d_edit { get; set; }
    [XmlElement("identity_card")]
    public string identity_card { get; set; }
    [XmlElement("FirstNameTH")]
    public string FirstNameTH { get; set; }
    [XmlElement("LastNameTH")]
    public string LastNameTH { get; set; }
    [XmlElement("rq_exam")]
    public int rq_exam { get; set; }
    [XmlElement("exam_status")]
    public int exam_status { get; set; }
    [XmlElement("unidx")]
    public int unidx { get; set; }
    [XmlElement("acidx")]
    public int acidx { get; set; }
    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }
    [XmlElement("rq_resmore")]
    public int rq_resmore { get; set; }
    [XmlElement("resmore_status")]
    public int resmore_status { get; set; }

}

[Serializable]
public class m0_doc_decision
{
    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("noidx")]
    public int noidx { get; set; }

    [XmlElement("decision_name")]
    public string decision_name { get; set; }

    [XmlElement("decision_desc")]
    public string decision_desc { get; set; }

    [XmlElement("decision_status")]
    public int decision_status { get; set; }

}

public class employee_node_config
{
    [XmlElement("unode_v")]
    public int unode_v { get; set; }

}

[Serializable]
public class employee_detail_v2
{

    [XmlElement("requestvdo")]
    public int requestvdo { get; set; }

    [XmlElement("basicexam")]
    public int basicexam { get; set; }

    [XmlElement("fullrequest")]
    public int fullrequest { get; set; }

}


//re organization
[Serializable]
public class cen_approve_u0_detail
{

    [XmlElement("emp_idx_approve1")]
    public int emp_idx_approve1 { get; set; }

    [XmlElement("emp_idx_approve2")]
    public int emp_idx_approve2 { get; set; }

    [XmlElement("emp_approve1")]
    public string emp_approve1 { get; set; }

    [XmlElement("emp_approve2")]
    public string emp_approve2 { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }


    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }


}

[Serializable]
public class cen_position_emp_r0_detail
{

    [XmlElement("pos_emp_idx")]
    public int pos_emp_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("pos_idx")]
    public int pos_idx { get; set; }

    [XmlElement("flag_main")]
    public int flag_main { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("pos_emp_status")]
    public int pos_emp_status { get; set; }



}

[Serializable]
public class employee_health_detail
{

    // U0_EmployeeHealth
    [XmlElement("height_s")]
    public int height_s { get; set; }
    [XmlElement("weight_s")]
    public int weight_s { get; set; }

    [XmlElement("Height")]
    public string Height { get; set; }
    [XmlElement("Weight")]
    public string Weight { get; set; }

    [XmlElement("btype_idx")]
    public int btype_idx { get; set; }
    [XmlElement("btype_name_en")]
    public string btype_name_en { get; set; }
    [XmlElement("btype_name_th")]
    public string btype_name_th { get; set; }

    [XmlElement("brh_idx")]
    public int brh_idx { get; set; }
    [XmlElement("brh_name_en")]
    public string brh_name_en { get; set; }
    [XmlElement("brh_name_th")]
    public string brh_name_th { get; set; }

    [XmlElement("allergy")]
    public string allergy { get; set; }
    [XmlElement("cdeisease")]
    public string cdeisease { get; set; }
    [XmlElement("operate")]
    public string operate { get; set; }
    [XmlElement("emer_contact")]
    public string emer_contact { get; set; }
    [XmlElement("scar_s")]
    public string scar_s { get; set; }

    public int soc_status { get; set; }
    [XmlElement("soc_no")]
    public string soc_no { get; set; }
    [XmlElement("soc_hos_idx")]
    public int soc_hos_idx { get; set; }
    [XmlElement("soc_expired_date")]
    public string soc_expired_date { get; set; }

    [XmlElement("examinationdate")]
    public string examinationdate { get; set; }
    [XmlElement("medicalcertificate_id")]
    public int medicalcertificate_id { get; set; }

    [XmlElement("resultlab_id")]
    public int resultlab_id { get; set; }
    // U0_EmployeeHealth

}

[Serializable]
public class cen_employee_detail
{

    [XmlElement("cost_idx")]
    public int cost_idx { get; set; }

    [XmlElement("empgroup_idx")]
    public int empgroup_idx { get; set; }

    [XmlElement("pos_idx")]
    public int pos_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

}

//re organization

#region Reference_persons_list 
[Serializable]
public class Reference_persons_list
{
    [XmlElement("emp_idx_reference")]
    public int emp_idx_reference { get; set; }
    [XmlElement("mobile_number")]
    public string mobile_number { get; set; }
    [XmlElement("fullname_reference")]
    public string fullname_reference { get; set; }
    [XmlElement("relationship")]
    public string relationship { get; set; }
    [XmlElement("address_reference")]
    public string address_reference { get; set; }
}

#endregion

#region experience 
[Serializable]
public class experiencelist
{

    [XmlElement("exper_idx_")]
    public int exper_idx_ { get; set; }
    [XmlElement("emp_idx_")]
    public int emp_idx_ { get; set; }
    [XmlElement("job__details")]
    public string job__details { get; set; }
    [XmlElement("name_company")]
    public string name_company { get; set; }
    [XmlElement("position_old_company")]
    public string position_old_company { get; set; }
    [XmlElement("workIn_old_company")]
    public string workIn_old_company { get; set; }
    [XmlElement("resign_old_company")]
    public string resign_old_company { get; set; }
    [XmlElement("motive_resign")]
    public string motive_resign { get; set; }
    [XmlElement("salary_old_company")]
    public string salary_old_company { get; set; }

}

#endregion

#region position_changeList 
[Serializable]
public class position_changeList
{
    [XmlElement("id_EODSPIDX")]
    public int id_EODSPIDX { get; set; }
    [XmlElement("id_emps")]
    public int id_emps { get; set; }
    [XmlElement("rpos_id_old")]
    public int rpos_id_old { get; set; }
    [XmlElement("rpos_id_new")]
    public int rpos_id_new { get; set; }
    [XmlElement("effecttive_date")]
    public string effecttive_date { get; set; }
    [XmlElement("status_EStaOut")]
    public int status_EStaOut { get; set; }

    [XmlElement("position_th_old")]
    public string position_th_old { get; set; }
    [XmlElement("position_th_new")]
    public string position_th_new { get; set; }
    [XmlElement("createdate")]
    public string createdate { get; set; }
}

#endregion


#region BoxPosisionList
[Serializable]
public class BoxPosisionList
{
    [XmlElement("EODSPIDX")]
    public int EODSPIDX { get; set; }
    [XmlElement("EmpIDX_Pos")]
    public int EmpIDX_Pos { get; set; }
    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }
    [XmlElement("EStatus")]
    public int EStatus { get; set; }
    [XmlElement("EStaOut")]
    public int EStaOut { get; set; }
}
#endregion

#region BoxSararyList
[Serializable]
public class BoxSararyList
{
    [XmlElement("emp_sarary_idx")]
    public int emp_sarary_idx { get; set; }
    [XmlElement("sarary_old")]
    public string sarary_old { get; set; }
    [XmlElement("sarary_new")]
    public string sarary_new { get; set; }
    [XmlElement("motive_of_pay")]
    public string motive_of_pay { get; set; }
    [XmlElement("date_create_sarary")]
    public string date_create_sarary { get; set; }
    [XmlElement("time_create_sarary")]
    public string time_create_sarary { get; set; }
    [XmlElement("date_approve_sarary")]
    public string date_approve_sarary { get; set; }
    [XmlElement("Benefits_new_")]
    public string Benefits_new_ { get; set; }
}
#endregion

#region BoxBenefitsList
[Serializable]
public class BoxBenefitsList
{
    [XmlElement("benefit_idx")]
    public int benefit_idx { get; set; }
    [XmlElement("name_benefit")]
    public string name_benefit { get; set; }
    [XmlElement("status_benefit")]
    public int status_benefit { get; set; }
    [XmlElement("emp_idx_benefit")]
    public int emp_idx_benefit { get; set; }
}
#endregion

#region EmployeeTHlist
[Serializable]
public class EmployeeTHlist
{
    [XmlElement("ProfileTHIDX")]
    public int ProfileTHIDX { get; set; }

    [XmlElement("emp_idx_th")]
    public int emp_idx_th { get; set; }

    [XmlElement("IdentityCard")]
    private string _IdentityCard;
    public string IdentityCard { get; set; }

    [XmlElement("IssuedAt")]
    private string _IssuedAt;
    public string IssuedAt { get; set; }

    [XmlElement("IDateExpired")]
    private string _IDateExpired;
    public string IDateExpired { get; set; }

    [XmlElement("BProvIDX")]
    public int BProvIDX { get; set; }

    [XmlElement("EmpAddress")]
    private string _EmpAddress;
    public string EmpAddress { get; set; }

    [XmlElement("DistIDX")]
    public int DistIDX { get; set; }

    [XmlElement("DistName")]
    private string _DistName;
    public string DistName { get; set; }

    [XmlElement("AmpIDX")]
    public int AmpIDX { get; set; }

    [XmlElement("AmpName")]
    private string _AmpName;
    public string AmpName { get; set; }

    [XmlElement("ProvIDX")]
    public int ProvIDX { get; set; }

    [XmlElement("ProvName")]
    private string _ProvName;
    public string ProvName { get; set; }

    [XmlElement("PostCode")]
    private string _PostCode;
    public string PostCode { get; set; }

    [XmlElement("CountryIDX")]
    public int CountryIDX { get; set; }

    [XmlElement("CountryName")]
    private string _CountryName;
    public string CountryName { get; set; }

    [XmlElement("PhoneNo")]
    private string _PhoneNo;
    public string PhoneNo { get; set; }

    [XmlElement("SocStatus")]
    public int SocStatus { get; set; }

    [XmlElement("SocNo")]
    private string _SocNo;
    public string SocNo { get; set; }

    [XmlElement("SocHosIDX")]
    public int SocHosIDX { get; set; }

    [XmlElement("SocHosName")]
    private string _SocHosName;
    public string SocHosName { get; set; }

    [XmlElement("SocDateExpired")]
    private string _SocDateExpired;
    public string SocDateExpired { get; set; }

    [XmlElement("TaxID")]
    private string _TaxID;
    public string TaxID { get; set; }

    [XmlElement("TIssuedAt")]
    private string _TIssuedAt;
    public string TIssuedAt { get; set; }

    [XmlElement("MilIDX")]
    public int MilIDX { get; set; }

    [XmlElement("MilName")]
    private string _MilName;
    public string MilName { get; set; }



}
#endregion

#region EmployeeFRlist

[Serializable]
public class EmployeeFRlist
{
    [XmlElement("ProfileOIDX")]
    public int ProfileOIDX { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("EmpAddress")]
    private string _EmpAddress;
    public string EmpAddress { get; set; }

    [XmlElement("CountryIDX")]
    public int CountryIDX { get; set; }

    [XmlElement("CountryName")]
    private string _CountryName;
    public string CountryName { get; set; }

    [XmlElement("PhoneNo")]
    private string _PhoneNo;
    public string PhoneNo { get; set; }

    [XmlElement("ContNameTH")]
    private string _ContNameTH;
    public string ContNameTH { get; set; }

    [XmlElement("ContNameEN")]
    private string _ContNameEN;
    public string ContNameEN { get; set; }

    [XmlElement("ContAddress")]
    private string _ContAddress;
    public string ContAddress { get; set; }

    [XmlElement("ContPhoneNo")]
    private string _ContPhoneNo;
    public string ContPhoneNo { get; set; }

    [XmlElement("Tr38_1No")]
    private string _Tr38_1No;
    public string Tr38_1No { get; set; }

    [XmlElement("TrDateIssued")]
    private string _TrDateIssued;
    public string TrDateIssued { get; set; }

    [XmlElement("TrExpired")]
    private string _TrExpired;
    public string TrExpired { get; set; }

    [XmlElement("WPermitNo")]
    private string _WPermitNo;
    public string WPermitNo { get; set; }

    [XmlElement("WDateIssued")]
    private string _WDateIssued;
    public string WDateIssued { get; set; }

    [XmlElement("WDateExpired")]
    private string _WDateExpired;
    public string WDateExpired { get; set; }

    [XmlElement("DDateOut")]
    private string _DDateOut;
    public string DDateOut { get; set; }

    [XmlElement("DDateIn")]
    private string _DDateIn;
    public string DDateIn { get; set; }

    [XmlElement("Employers")]
    private string _Employers;
    public string Employers { get; set; }

    [XmlElement("VisaIDX")]
    public string VisaIDX { get; set; }

    [XmlElement("VisaNameTH")]
    private string _VisaNameTH;
    public string VisaNameTH { get; set; }

    [XmlElement("Passport_Issued")]
    private string _Passport_Issued;
    public string Passport_Issued { get; set; }

    [XmlElement("WPermit_Issued ")]
    private string _WPermit_Issued;
    public string WPermit_Issued { get; set; }

}


#endregion

#region EmployeeHealthlist
[Serializable]
public class EmployeeHealthlist
{
    [XmlElement("HIDX")]
    public int HIDX { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("Height")]
    public float Height { get; set; }

    [XmlElement("Weight")]
    public float Weight { get; set; }

    [XmlElement("Scar")]
    private string _Scar;
    public string Scar { get; set; }

    [XmlElement("BTypeIDX")]
    public int BTypeIDX { get; set; }

    [XmlElement("BGName")]
    private string _BGName;
    public string BGName { get; set; }

    [XmlElement("BRHIDX")]
    public int BRHIDX { get; set; }

    [XmlElement("RHName")]
    private string _RHName;
    public string RHName { get; set; }

    [XmlElement("Allergy")]
    private string _Allergy;
    public string Allergy { get; set; }

    [XmlElement("CDisease")]
    private string _CDisease;
    public string CDisease { get; set; }

    [XmlElement("Operate")]
    private string _Operate;
    public string Operate { get; set; }

    [XmlElement("EmerContact")]
    private string _EmerContact;
    public string EmerContact { get; set; }

}
#endregion

#region DetailEducation

[Serializable]
public class DetailEducation
{

    [XmlElement("Eduidx")]
    public int Eduidx { get; set; }
    [XmlElement("emp_idx_edu")]
    public int emp_idx_edu { get; set; }
    [XmlElement("Edu_name")]
    public string Edu_name { get; set; }
    [XmlElement("Major")]
    public string Major { get; set; }
    [XmlElement("University")]
    public string University { get; set; }
    [XmlElement("Faculty")]
    public int Faculty { get; set; }
    [XmlElement("GPA")]
    public string GPA { get; set; }
    [XmlElement("exper_idx")]
    public int exper_idx { get; set; }
    [XmlElement("academic_year")]
    public string academic_year { get; set; }
}

#endregion

#region CostCenter
[Serializable]
public class CostCenter  /*  CostCenter DataBox  */
{
    [XmlElement("CostIDX")]
    public int CostIDX { get; set; }
    [XmlElement("CostNo")]
    private string _CostNo;
    public string CostNo { get; set; }
    [XmlElement("CostName")]
    private string _CostName;
    public string CostName { get; set; }
    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }
    [XmlElement("CostStatus")]
    public int CostStatus { get; set; }
    [XmlElement("CostStatusDetail")]
    private string _CostStatusDetail;
    public string CostStatusDetail { get; set; }
    [XmlElement("costcenter_name")]
    public string costcenter_name { get; set; }
}
#endregion



[Serializable]
public class select_emp_list
{
    [XmlElement("emp_idx_list")]
    public int emp_idx_list { get; set; }

}


#region penalty_details 
[Serializable]
public class penalty_details
{
    [XmlElement("penalty_idx")]
    public int penalty_idx { get; set; }
    [XmlElement("emp_idx_penalty")]
    public int emp_idx_penalty { get; set; }
    [XmlElement("warninig_number")]
    public string warninig_number { get; set; }
    [XmlElement("date_of_warning")]
    public string date_of_warning { get; set; }
    [XmlElement("penalty_number")]
    public string penalty_number { get; set; }
    [XmlElement("date_delinquent")]
    public string date_delinquent { get; set; }
    [XmlElement("details__")]
    public string details__ { get; set; }
    [XmlElement("penalty_was")]
    public string penalty_was { get; set; }
    [XmlElement("penalty_type")]
    public int penalty_type { get; set; }
    [XmlElement("work_break_idx")]
    public string work_break_idx { get; set; }
    [XmlElement("next_penalty")]
    public string next_penalty { get; set; }
    [XmlElement("name__")]
    public string name__ { get; set; }
    [XmlElement("status_penalty")]
    public int status_penalty { get; set; }
    [XmlElement("create_date_penalty")]
    public string create_date_penalty { get; set; }

    [XmlElement("qty_day")]
    public string qty_day { get; set; }
    [XmlElement("start_date_break")]
    public string start_date_break { get; set; }
    [XmlElement("end_date_break")]
    public string end_date_break { get; set; }
    [XmlElement("status_break")]
    public int status_break { get; set; }
}
#endregion



[Serializable]
public class employee_Ipaddress
{
    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("btype_idx")]
    public int btype_idx { get; set; }

    [XmlElement("emp_status")]
    public int emp_status { get; set; }
}

[Serializable]
public class employee_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_password")]
    public string emp_password { get; set; }

    [XmlElement("perm_pol")]
    public int perm_pol { get; set; }

    [XmlElement("perm_pol_name")]
    public string perm_pol_name { get; set; }

    [XmlElement("u0_perm_perm_ad")]
    public string u0_perm_perm_ad { get; set; }

    [XmlElement("u0_perm_perm_sap")]
    public string u0_perm_perm_sap { get; set; }

    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("u0_monitor")]
    public string u0_monitor { get; set; }

    [XmlElement("u0_printer")]
    public string u0_printer { get; set; }

    // employee name profile
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }
    [XmlElement("prefix_th")]
    public string prefix_th { get; set; }

    [XmlElement("emp_fullname_th")]
    public string emp_fullname_th { get; set; }
    [XmlElement("emp_end_date")]
    public string emp_end_date { get; set; }

    [XmlElement("emp_firstname_en")]
    public string emp_firstname_en { get; set; }
    [XmlElement("emp_lastname_en")]
    public string emp_lastname_en { get; set; }
    [XmlElement("emp_firstname_th")]
    public string emp_firstname_th { get; set; }
    [XmlElement("emp_lastname_th")]
    public string emp_lastname_th { get; set; }

    [XmlElement("emp_nickname_en")]
    public string emp_nickname_en { get; set; }
    [XmlElement("emp_nickname_th")]
    public string emp_nickname_th { get; set; }
    // employee name profile

    // employee general profile
    [XmlElement("sex_idx")]
    public int sex_idx { get; set; }
    [XmlElement("sex_name_en")]
    public string sex_name_en { get; set; }
    [XmlElement("sex_name_th")]
    public string sex_name_th { get; set; }

    [XmlElement("prefix_idx")]
    public int prefix_idx { get; set; }
    [XmlElement("prefix_name_en")]
    public string prefix_name_en { get; set; }
    [XmlElement("prefix_name_th")]
    public string prefix_name_th { get; set; }

    [XmlElement("emp_phone_no")]
    public string emp_phone_no { get; set; }
    [XmlElement("emp_mobile_no")]
    public string emp_mobile_no { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_birthday")]
    public string emp_birthday { get; set; }
    // employee general profile

    // employee type
    [XmlElement("emp_type_idx")]
    public int emp_type_idx { get; set; }
    [XmlElement("emp_type_name")]
    public string emp_type_name { get; set; }
    // employee type

    // employee organization profile
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("r0idx")]
    public int r0idx { get; set; }
    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }
    [XmlElement("LocName")]
    public string LocName { get; set; }
    [XmlElement("LocStatus")]
    public int LocStatus { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }
    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }
    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }
    [XmlElement("pos_name_en")]
    public string pos_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("jobgrade_idx")]
    public int jobgrade_idx { get; set; }
    [XmlElement("jobgrade_level")]
    public int jobgrade_level { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }
    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }
    [XmlElement("costcenter_name")]
    public string costcenter_name { get; set; }
    // employee organization profile

    // employee start date, probation date and status
    [XmlElement("emp_start_date")]
    public string emp_start_date { get; set; }
    [XmlElement("emp_probation_status")]
    public int emp_probation_status { get; set; }
    [XmlElement("emp_probation_date")]
    public string emp_probation_date { get; set; }
    // employee start date, probation date and status

    // employee profile date and status
    [XmlElement("emp_createdate")]
    public string emp_createdate { get; set; }
    [XmlElement("emp_updatedate")]
    public string emp_updatedate { get; set; }
    [XmlElement("emp_status")]
    public int emp_status { get; set; }
    // employee profile date and status

    // employee update password profile
    [XmlElement("new_password")]
    public string new_password { get; set; }
    [XmlElement("type_reset")]
    public int type_reset { get; set; }
    // employee update password profile

    // vacation approver profile
    [XmlElement("emp_idx_approve1")]
    public int emp_idx_approve1 { get; set; }
    [XmlElement("emp_idx_approve2")]
    public int emp_idx_approve2 { get; set; }
    [XmlElement("emp_approve1")]
    public string emp_approve1 { get; set; }
    [XmlElement("emp_approve2")]
    public string emp_approve2 { get; set; }
    // vacation approver profile

    // nat_idx, race_idx, rel_idx
    [XmlElement("nat_idx")]
    public int nat_idx { get; set; }
    [XmlElement("nat_name_en")]
    public string nat_name_en { get; set; }
    [XmlElement("nat_name_th")]
    public string nat_name_th { get; set; }

    [XmlElement("race_idx")]
    public int race_idx { get; set; }
    [XmlElement("race_name_en")]
    public string race_name_en { get; set; }
    [XmlElement("race_name_th")]
    public string race_name_th { get; set; }

    [XmlElement("rel_idx")]
    public int rel_idx { get; set; }
    [XmlElement("rel_name_en")]
    public string rel_name_en { get; set; }
    [XmlElement("rel_name_th")]
    public string rel_name_th { get; set; }
    // nat_idx, race_idx, rel_idx

    // emp_address
    [XmlElement("emp_address")]
    public string emp_address { get; set; }

    [XmlElement("dist_idx")]
    public int dist_idx { get; set; }
    [XmlElement("dist_name_en")]
    public string dist_name_en { get; set; }
    [XmlElement("dist_name_th")]
    public string dist_name_th { get; set; }

    [XmlElement("amp_idx")]
    public int amp_idx { get; set; }
    [XmlElement("amp_name_en")]
    public string amp_name_en { get; set; }
    [XmlElement("amp_name_th")]
    public string amp_name_th { get; set; }

    [XmlElement("prov_idx")]
    public int prov_idx { get; set; }
    [XmlElement("prov_name_en")]
    public string prov_name_en { get; set; }
    [XmlElement("prov_name_th")]
    public string prov_name_th { get; set; }

    [XmlElement("post_code")]
    public string post_code { get; set; }

    [XmlElement("country_idx")]
    public int country_idx { get; set; }
    [XmlElement("country_name_en")]
    public string country_name_en { get; set; }
    [XmlElement("country_name_th")]
    public string country_name_th { get; set; }
    // emp_address

    [XmlElement("married_status")]
    public int married_status { get; set; }
    [XmlElement("married_name_en")]
    public string married_name_en { get; set; }
    [XmlElement("married_name_th")]
    public string married_name_th { get; set; }

    [XmlElement("inhabit_status")]
    public int inhabit_status { get; set; }
    [XmlElement("inhabit_name_en")]
    public string inhabit_name_en { get; set; }
    [XmlElement("inhabit_name_th")]
    public string inhabit_name_th { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    // U0_EmployeeTH
    [XmlElement("identity_card")]
    public string identity_card { get; set; }
    [XmlElement("issued_at")]
    public string issued_at { get; set; }
    [XmlElement("idate_expired")]
    public string idate_expired { get; set; }

    [XmlElement("iemp_address")]
    public string iemp_address { get; set; }

    [XmlElement("idist_idx")]
    public int idist_idx { get; set; }
    [XmlElement("idist_name_en")]
    public string idist_name_en { get; set; }
    [XmlElement("idist_name_th")]
    public string idist_name_th { get; set; }

    [XmlElement("iamp_idx")]
    public int iamp_idx { get; set; }
    [XmlElement("iamp_name_en")]
    public string iamp_name_en { get; set; }
    [XmlElement("iamp_name_th")]
    public string iamp_name_th { get; set; }

    [XmlElement("iprov_idx")]
    public int iprov_idx { get; set; }
    [XmlElement("iprov_name_en")]
    public string iprov_name_en { get; set; }
    [XmlElement("iprov_name_th")]
    public string iprov_name_th { get; set; }

    [XmlElement("ipost_code")]
    public string ipost_code { get; set; }

    [XmlElement("icountry_idx")]
    public int icountry_idx { get; set; }
    [XmlElement("icountry_name_en")]
    public string icountry_name_en { get; set; }
    [XmlElement("icountry_name_th")]
    public string icountry_name_th { get; set; }

    [XmlElement("iemp_phone_no")]
    public string iemp_phone_no { get; set; }

    [XmlElement("soc_status")]
    public int soc_status { get; set; }
    [XmlElement("soc_no")]
    public string soc_no { get; set; }
    [XmlElement("soc_hos_idx")]
    public int soc_hos_idx { get; set; }
    [XmlElement("soc_expired_date")]
    public string soc_expired_date { get; set; }

    [XmlElement("tax_id")]
    public string tax_id { get; set; }
    [XmlElement("tax_issued_at")]
    public string tax_issued_at { get; set; }

    [XmlElement("mil_idx")]
    public int mil_idx { get; set; }
    [XmlElement("mil_name_en")]
    public string mil_name_en { get; set; }
    [XmlElement("mil_name_th")]
    public string mil_name_th { get; set; }
    // U0_EmployeeTH

    // U0_EmployeeFR
    /*[XmlElement("femp_address")]
    public string femp_address { get; set; }

    [XmlElement("fcountry_idx")]
    public int fcountry_idx { get; set; }
    [XmlElement("fcountry_name_en")]
    public string fcountry_name_en { get; set; }
    [XmlElement("fcountry_name_th")]
    public string fcountry_name_th { get; set; }

    [XmlElement("femp_phone_no")]
    public string femp_phone_no { get; set; }

    [XmlElement("cont_name_th")]
    public string cont_name_th { get; set; }
    [XmlElement("cont_name_en")]
    public string cont_name_en { get; set; }

    [XmlElement("cont_address")]
    public string cont_address { get; set; }
    [XmlElement("cont_phone_no")]
    public string cont_phone_no { get; set; }

    [XmlElement("tr38_no")]
    public string tr38_no { get; set; }
    [XmlElement("tr_issued_date")]
    public string tr_issued_date { get; set; }
    [XmlElement("tr_expried_date")]
    public string tr_expried_date { get; set; }

    [XmlElement("wpermit_no")]
    public string wpermit_no { get; set; }
    [XmlElement("wissued_date")]
    public string wissued_date { get; set; }
    [XmlElement("wexpired_date")]
    public string wexpired_date { get; set; }*/

    /*[XmlElement("ddate_out")]
    public string ddate_out { get; set; }
    [XmlElement("ddate_in")]
    public string ddate_in { get; set; }

    [XmlElement("employers")]
    public string employers { get; set; }

    [XmlElement("visa_idx")]
    public int visa_idx { get; set; }*/

    /*[XmlElement("fsoc_status")]
    public int fsoc_status { get; set; }
    [XmlElement("fsoc_hos_idx")]
    public int fsoc_hos_idx { get; set; }
    [XmlElement("fsoc_expired_date")]
    public string fsoc_expired_date { get; set; }
    [XmlElement("fsoc_no")]
    public string fsoc_no { get; set; }*/

    //[XmlElement("passport_issued")]
    //public string passport_issued { get; set; }
    //[XmlElement("wpermit_issued")]
    //public string wpermit_issued { get; set; }
    // U0_EmployeeFR

    // // U0_EmployeeHealth
    [XmlElement("height_s")]
    public int height_s { get; set; }
    [XmlElement("weight_s")]
    public int weight_s { get; set; }

    // [XmlElement("btype_idx")]
    // public int btype_idx { get; set; }
    // [XmlElement("btype_name_en")]
    // public string btype_name_en { get; set; }
    // [XmlElement("btype_name_th")]
    // public string btype_name_th { get; set; }

    [XmlElement("btype_idx")]
    public int btype_idx { get; set; }
    [XmlElement("btype_name_en")]
    public string btype_name_en { get; set; }
    [XmlElement("btype_name_th")]
    public string btype_name_th { get; set; }

    [XmlElement("brh_idx")]
    public int brh_idx { get; set; }
    [XmlElement("brh_name_en")]
    public string brh_name_en { get; set; }
    [XmlElement("brh_name_th")]
    public string brh_name_th { get; set; }

    // [XmlElement("allergy")]
    // public string allergy { get; set; }
    // [XmlElement("cdeisease")]
    // public string cdeisease { get; set; }
    // [XmlElement("operate")]
    // public string operate { get; set; }
    // [XmlElement("emer_contact")]
    // public string emer_contact { get; set; }
    // // U0_EmployeeHealth

    // --------------- //
    //[XmlElement("Height")]
    //public string Height { get; set; }
    //[XmlElement("Weight")]
    //public string Weight { get; set; }
    [XmlElement("Scar")]
    public string Scar { get; set; }
    [XmlElement("scar_s")]
    public string scar_s { get; set; }
    [XmlElement("BGName")]
    public string BGName { get; set; }
    [XmlElement("RHName")]
    public string RHName { get; set; }
    [XmlElement("Allergy")]
    public string Allergy { get; set; }
    [XmlElement("IdentityCard")]
    public string IdentityCard { get; set; }
    [XmlElement("country_name")]
    public string country_name { get; set; }
    [XmlElement("prov_name")]
    public string prov_name { get; set; }
    [XmlElement("amp_name")]
    public string amp_name { get; set; }
    [XmlElement("dist_name")]
    public string dist_name { get; set; }

    [XmlElement("emp_time")]
    public string emp_time { get; set; }

    //[XmlElement("perm_pol")]
    //public int perm_pol { get; set; }

    [XmlElement("PhoneNumber")]
    public string PhoneNumber { get; set; }

    [XmlElement("empstatus")]
    public string empstatus { get; set; }

    [XmlElement("m0telidx")]
    public int m0telidx { get; set; }

    [XmlElement("u0telidx")]
    public int u0telidx { get; set; }

    [XmlElement("u0_tel")]
    public string u0_tel { get; set; }

    [XmlElement("HCEmpIDX")]
    public int HCEmpIDX { get; set; }

    //[XmlElement("u0_code")]
    //public string u0_code { get; set; }

    //[XmlElement("u0_monitor")]
    //public string u0_monitor { get; set; }

    //[XmlElement("u0_printer")]
    //public string u0_printer { get; set; }

    //[XmlElement("u0_perm_perm_ad")]
    //public string u0_perm_perm_ad { get; set; }


    //[XmlElement("perm_pol_name")]
    //public string perm_pol_name { get; set; }


    //[XmlElement("u0_perm_perm_sap")]
    //public string u0_perm_perm_sap { get; set; }
    // --------------- //

    [XmlElement("military_idx")]
    public int military_idx { get; set; }

    [XmlElement("idcard")]
    public string idcard { get; set; }

    [XmlElement("expcard")]
    public string expcard { get; set; }

    [XmlElement("dvcar_idx")]
    public int dvcar_idx { get; set; }

    [XmlElement("dvmt_idx")]
    public int dvmt_idx { get; set; }

    [XmlElement("bank_idx")]
    public int bank_idx { get; set; }

    [XmlElement("bank_name")]
    public string bank_name { get; set; }

    [XmlElement("bank_no")]
    public string bank_no { get; set; }

    [XmlElement("emer_name")]
    public string emer_name { get; set; }

    [XmlElement("emer_relation")]
    public string emer_relation { get; set; }

    [XmlElement("emer_tel")]
    public string emer_tel { get; set; }

    [XmlElement("fathername")]
    public string fathername { get; set; }

    [XmlElement("telfather")]
    public string telfather { get; set; }

    [XmlElement("mothername")]
    public string mothername { get; set; }

    [XmlElement("telmother")]
    public string telmother { get; set; }

    [XmlElement("wifename")]
    public string wifename { get; set; }

    [XmlElement("telwife")]
    public string telwife { get; set; }

    [XmlElement("emp_address_permanent")]
    public string emp_address_permanent { get; set; }

    [XmlElement("country_idx_permanent")]
    public int country_idx_permanent { get; set; }

    [XmlElement("prov_idx_permanent")]
    public int prov_idx_permanent { get; set; }

    [XmlElement("amp_idx_permanent")]
    public int amp_idx_permanent { get; set; }

    [XmlElement("dist_idx_permanent")]
    public int dist_idx_permanent { get; set; }

    [XmlElement("PhoneNumber_permanent")]
    public string PhoneNumber_permanent { get; set; }

    [XmlElement("u0_tel_permanent")]
    public string u0_tel_permanent { get; set; }

    [XmlElement("HIDX")]
    public int HIDX { get; set; }

    [XmlElement("examinationdate")]
    public string examinationdate { get; set; }

    [XmlElement("security_id")]
    public string security_id { get; set; }

    [XmlElement("expsecuritydate")]
    public string expsecuritydate { get; set; }

    [XmlElement("BRHIDX")]
    public int BRHIDX { get; set; }

    [XmlElement("BTypeIDX")]
    public int BTypeIDX { get; set; }

    [XmlElement("medicalcertificate_id")]
    public int medicalcertificate_id { get; set; }

    [XmlElement("resultlab_id")]
    public int resultlab_id { get; set; }

    [XmlElement("emp_resign_date")]
    public string emp_resign_date { get; set; }

    [XmlElement("TIDX")]
    public int TIDX { get; set; }

    [XmlElement("target_name")]
    public string target_name { get; set; }

    [XmlElement("target_number")]
    public string target_number { get; set; }

    [XmlElement("Rsec_ID")]
    public string Rsec_ID { get; set; }

    [XmlElement("type_approve")]
    public int type_approve { get; set; }

    [XmlElement("type_select_emp")]
    public int type_select_emp { get; set; }

    [XmlElement("dvfork_idx")]
    public int dvfork_idx { get; set; }

    [XmlElement("dvtruck_idx")]
    public int dvtruck_idx { get; set; }

    [XmlElement("dvlicen_car")]
    public string dvlicen_car { get; set; }

    [XmlElement("dvlicen_moto")]
    public string dvlicen_moto { get; set; }

    [XmlElement("dvlicen_fork")]
    public string dvlicen_fork { get; set; }

    [XmlElement("dvlicen_truck")]
    public string dvlicen_truck { get; set; }

    [XmlElement("licen_father")]
    public string licen_father { get; set; }

    [XmlElement("licen_mother")]
    public string licen_mother { get; set; }

    [XmlElement("licen_wife")]
    public string licen_wife { get; set; }

    [XmlElement("startdate_old")]
    public string startdate_old { get; set; }

    [XmlElement("startdate_search")]
    public string startdate_search { get; set; }

    [XmlElement("enddate_search")]
    public string enddate_search { get; set; }

    [XmlElement("datetype_search")]
    public int datetype_search { get; set; }

    [XmlElement("emp_code_cus")]
    public string emp_code_cus { get; set; }

    [XmlElement("emp_status_name")]
    public string emp_status_name { get; set; }

    [XmlElement("dvcarstatus_idx")]
    public int dvcarstatus_idx { get; set; }

    [XmlElement("dvmtstatus_idx")]
    public int dvmtstatus_idx { get; set; }

    [XmlElement("Search_jobgrade1")]
    public int Search_jobgrade1 { get; set; }

    [XmlElement("Search_jobgrade2")]
    public int Search_jobgrade2 { get; set; }

    [XmlElement("EmpINID")]
    public int EmpINID { get; set; }

    [XmlElement("salary")]
    public int salary { get; set; }

    [XmlElement("JobTypeIDX")]
    public int JobTypeIDX { get; set; }

    [XmlElement("PosGrop1")]
    public int PosGrop1 { get; set; }

    [XmlElement("PosGrop2")]
    public int PosGrop2 { get; set; }

    [XmlElement("PosGrop3")]
    public int PosGrop3 { get; set; }

    [XmlElement("PosGroupNameTH")]
    public string PosGroupNameTH { get; set; }

    [XmlElement("PosGroupNameTH2")]
    public string PosGroupNameTH2 { get; set; }

    [XmlElement("PosGroupNameTH3")]
    public string PosGroupNameTH3 { get; set; }

    [XmlElement("DetailProfile")]
    public string DetailProfile { get; set; }

    [XmlElement("JName")]
    public string JName { get; set; }

    [XmlElement("WName")]
    public string WName { get; set; }

    [XmlElement("PIDX")]
    public int PIDX { get; set; }

    [XmlElement("P_Name")]
    public string P_Name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("Approve_status")]
    public int Approve_status { get; set; }

    [XmlElement("comment_approve")]
    public string comment_approve { get; set; }

    [XmlElement("u0_unidx")]
    public int u0_unidx { get; set; }

    [XmlElement("u0_acidx")]
    public int u0_acidx { get; set; }

    [XmlElement("u0_doc_decision")]
    public int u0_doc_decision { get; set; }

    [XmlElement("sridx")]
    public int sridx { get; set; }

    // VisaIDX

    [XmlElement("VisaIDX")]
    public int VisaIDX { get; set; }

    [XmlElement("id_card_visa")]
    public string id_card_visa { get; set; }

    [XmlElement("visa_startdate")]
    public string visa_startdate { get; set; }

    [XmlElement("visa_enddate")]
    public string visa_enddate { get; set; }

    [XmlElement("ckcars_search")]
    public string ckcars_search { get; set; }

    [XmlElement("ckjobs_search")]
    public string ckjobs_search { get; set; }

    [XmlElement("EDUIDX")]
    public int EDUIDX { get; set; }

    [XmlElement("passportID")]
    public string passportID { get; set; }

    [XmlElement("passport_enddate")]
    public string passport_enddate { get; set; }

    [XmlElement("workpermit_enddate")]
    public string workpermit_enddate { get; set; }

    [XmlElement("ACIDX")]
    public int ACIDX { get; set; }

    [XmlElement("name_Affiliation")]
    public string name_Affiliation { get; set; }

    [XmlElement("P_m0_toidx")]
    public string P_m0_toidx { get; set; }

    [XmlElement("determine")]
    public string determine { get; set; }

    [XmlElement("statusinterview")]
    public string statusinterview { get; set; }

    [XmlElement("passport_issuat")]
    public string passport_issuat { get; set; }
    [XmlElement("passport_start")]
    public string passport_start { get; set; }
    [XmlElement("workpermitID")]
    public string workpermitID { get; set; }
    [XmlElement("workpermit_issuat")]
    public string workpermit_issuat { get; set; }
    [XmlElement("workpermit_start")]
    public string workpermit_start { get; set; }

    [XmlElement("personal_email")]
    public string personal_email { get; set; }

}



[Serializable]
public class employee_detail_tel
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("perm_pol")]
    public int perm_pol { get; set; }

    [XmlElement("perm_pol_name")]
    public string perm_pol_name { get; set; }

    [XmlElement("u0_perm_perm_ad")]
    public string u0_perm_perm_ad { get; set; }

    [XmlElement("u0_perm_perm_sap")]
    public string u0_perm_perm_sap { get; set; }

    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("u0_monitor")]
    public string u0_monitor { get; set; }

    [XmlElement("u0_printer")]
    public string u0_printer { get; set; }


    // employee name profile
    [XmlElement("prefix_th")]
    public string prefix_th { get; set; }
    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("emp_fullname_th")]
    public string emp_fullname_th { get; set; }
    [XmlElement("emp_end_date")]
    public string emp_end_date { get; set; }


    [XmlElement("emp_firstname_en")]
    public string emp_firstname_en { get; set; }
    [XmlElement("emp_lastname_en")]
    public string emp_lastname_en { get; set; }
    [XmlElement("emp_firstname_th")]
    public string emp_firstname_th { get; set; }
    [XmlElement("emp_lastname_th")]
    public string emp_lastname_th { get; set; }

    [XmlElement("emp_nickname_en")]
    public string emp_nickname_en { get; set; }
    [XmlElement("emp_nickname_th")]
    public string emp_nickname_th { get; set; }
    // employee name profile

    // employee general profile
    [XmlElement("sex_idx")]
    public int sex_idx { get; set; }
    [XmlElement("sex_name_en")]
    public string sex_name_en { get; set; }
    [XmlElement("sex_name_th")]
    public string sex_name_th { get; set; }

    [XmlElement("prefix_idx")]
    public int prefix_idx { get; set; }
    [XmlElement("prefix_name_en")]
    public string prefix_name_en { get; set; }
    [XmlElement("prefix_name_th")]
    public string prefix_name_th { get; set; }

    [XmlElement("emp_phone_no")]
    public string emp_phone_no { get; set; }
    [XmlElement("emp_mobile_no")]
    public string emp_mobile_no { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }


    // employee organization profile
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("r0idx")]
    public int r0idx { get; set; }
    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }
    [XmlElement("LocName")]
    public string LocName { get; set; }
    [XmlElement("LocStatus")]
    public int LocStatus { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }
    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }
    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }
    [XmlElement("pos_name_en")]
    public string pos_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("emp_type_idx")]
    public int emp_type_idx { get; set; }
    [XmlElement("emp_type_name")]
    public string emp_type_name { get; set; }


    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }
    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }
    [XmlElement("costcenter_name")]
    public string costcenter_name { get; set; }
    // employee organization profile


    // employee profile date and status
    [XmlElement("emp_createdate")]
    public string emp_createdate { get; set; }
    [XmlElement("emp_updatedate")]
    public string emp_updatedate { get; set; }
    [XmlElement("emp_status")]
    public int emp_status { get; set; }
    // employee profile date and status



    [XmlElement("PhoneNumber")]
    public string PhoneNumber { get; set; }

    [XmlElement("empstatus")]
    public string empstatus { get; set; }

    [XmlElement("m0telidx")]
    public int m0telidx { get; set; }

    [XmlElement("u0telidx")]
    public int u0telidx { get; set; }

    [XmlElement("u0_tel")]
    public string u0_tel { get; set; }

    [XmlElement("HCEmpIDX")]
    public int HCEmpIDX { get; set; }


}

[Serializable]
public class employee_detail_small
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_password")]
    public string emp_password { get; set; }

    // employee name profile.
    [XmlElement("prefix_th")]
    public string prefix_th { get; set; }
    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("ckcars_search")]
    public string ckcars_search { get; set; }

    [XmlElement("ckjobs_search")]
    public string ckjobs_search { get; set; }

    [XmlElement("EDUIDX")]
    public int EDUIDX { get; set; }

    [XmlElement("emp_firstname_en")]
    public string emp_firstname_en { get; set; }
    [XmlElement("emp_lastname_en")]
    public string emp_lastname_en { get; set; }
    [XmlElement("emp_firstname_th")]
    public string emp_firstname_th { get; set; }
    [XmlElement("emp_lastname_th")]
    public string emp_lastname_th { get; set; }

    [XmlElement("emp_nickname_en")]
    public string emp_nickname_en { get; set; }
    [XmlElement("emp_nickname_th")]
    public string emp_nickname_th { get; set; }
    // employee name profile

    // employee general profile
    [XmlElement("sex_idx")]
    public int sex_idx { get; set; }
    [XmlElement("sex_name_en")]
    public string sex_name_en { get; set; }
    [XmlElement("sex_name_th")]
    public string sex_name_th { get; set; }

    [XmlElement("prefix_idx")]
    public int prefix_idx { get; set; }
    [XmlElement("prefix_name_en")]
    public string prefix_name_en { get; set; }
    [XmlElement("prefix_name_th")]
    public string prefix_name_th { get; set; }

    [XmlElement("emp_phone_no")]
    public string emp_phone_no { get; set; }
    [XmlElement("emp_mobile_no")]
    public string emp_mobile_no { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_birthday")]
    public string emp_birthday { get; set; }
    // employee general profile

    // employee type
    [XmlElement("emp_type_idx")]
    public int emp_type_idx { get; set; }
    [XmlElement("emp_type_name")]
    public string emp_type_name { get; set; }
    // employee type

    // employee organization profile
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("r0idx")]
    public int r0idx { get; set; }
    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }
    [XmlElement("LocName")]
    public string LocName { get; set; }
    [XmlElement("LocStatus")]
    public int LocStatus { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }
    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }
    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }
    [XmlElement("pos_name_en")]
    public string pos_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("jobgrade_idx")]
    public int jobgrade_idx { get; set; }
    [XmlElement("jobgrade_level")]
    public int jobgrade_level { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }
    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }
    [XmlElement("costcenter_name")]
    public string costcenter_name { get; set; }
    // employee organization profile

    // employee start date, probation date and status
    [XmlElement("emp_start_date")]
    public string emp_start_date { get; set; }
    [XmlElement("emp_probation_status")]
    public int emp_probation_status { get; set; }
    [XmlElement("emp_probation_date")]
    public string emp_probation_date { get; set; }
    // employee start date, probation date and status

    // employee profile date and status
    [XmlElement("emp_createdate")]
    public string emp_createdate { get; set; }
    [XmlElement("emp_updatedate")]
    public string emp_updatedate { get; set; }
    [XmlElement("emp_status")]
    public int emp_status { get; set; }
    // employee profile date and status

    // employee update password profile
    [XmlElement("new_password")]
    public string new_password { get; set; }
    [XmlElement("type_reset")]
    public int type_reset { get; set; }
    // employee update password profile

    // vacation approver profile
    [XmlElement("emp_idx_approve1")]
    public int emp_idx_approve1 { get; set; }
    [XmlElement("emp_idx_approve2")]
    public int emp_idx_approve2 { get; set; }
    [XmlElement("emp_approve1")]
    public string emp_approve1 { get; set; }
    [XmlElement("emp_approve2")]
    public string emp_approve2 { get; set; }
    // vacation approver profile

    [XmlElement("emp_selected")]
    public bool emp_selected { get; set; }
}

[Serializable]
public class organization_details
{
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("org_account")]
    public string org_account { get; set; }
    [XmlElement("org_address")]
    public string org_address { get; set; }
    [XmlElement("dist_idx")]
    public int dist_idx { get; set; }
    [XmlElement("amp_idx")]
    public int amp_idx { get; set; }
    [XmlElement("prov_idx")]
    public int prov_idx { get; set; }
    [XmlElement("post_code")]
    public string post_code { get; set; }

    [XmlElement("org_status")]
    public int org_status { get; set; }

    [XmlElement("dist_name")]
    public string dist_name { get; set; }
    [XmlElement("amp_name")]
    public string amp_name { get; set; }
    [XmlElement("prov_name")]
    public string prov_name { get; set; }
}

[Serializable]
public class department_details
{
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rdept_status")]
    public int rdept_status { get; set; }

    [XmlElement("dept_idx")]
    public int dept_idx { get; set; }
    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("short_dept")]
    public string short_dept { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
}

[Serializable]
public class section_details
{
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("rsec_status")]
    public int rsec_status { get; set; }

    [XmlElement("sec_idx")]
    public int sec_idx { get; set; }
    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("dept_idx")]
    public int dept_idx { get; set; }
    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
}

[Serializable]
public class position_details
{
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }
    [XmlElement("rpos_status")]
    public int rpos_status { get; set; }

    [XmlElement("pos_idx")]
    public int pos_idx { get; set; }
    [XmlElement("pos_name_en")]
    public string pos_name_en { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("sec_idx")]
    public int sec_idx { get; set; }
    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("dept_idx")]
    public int dept_idx { get; set; }
    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
}
//#endregion get organization, department, section, position

#region search key employee
[Serializable]
public class search_key_employee
{
    [XmlElement("s_emp_idx")]
    public string s_emp_idx { get; set; }
    [XmlElement("s_emp_code")]
    public string s_emp_code { get; set; }
    [XmlElement("s_emp_name")]
    public string s_emp_name { get; set; }
    [XmlElement("s_org_idx")]
    public string s_org_idx { get; set; }
    [XmlElement("s_rdept_idx")]
    public string s_rdept_idx { get; set; }
    [XmlElement("s_rsec_idx")]
    public string s_rsec_idx { get; set; }
    [XmlElement("s_cost_center")]
    public string s_cost_center { get; set; }
    [XmlElement("s_date_field")]
    public string s_date_field { get; set; }
    [XmlElement("s_month_year")]
    public string s_month_year { get; set; }
    [XmlElement("s_emp_type")]
    public string s_emp_type { get; set; }
    [XmlElement("s_emp_status")]
    public string s_emp_status { get; set; }
}
#endregion search key employee

#region Hospital
[Serializable]
public class Hospital  /*    */
{
    [XmlElement("HosIDX")]
    public int HosIDX { get; set; }
    [XmlElement("Name")]
    private string _Name;
    public string Name { get; set; }
    [XmlElement("HStatus")]
    public int HStatus { get; set; }
    [XmlElement("HStatus1")]
    private string _HStatus1;
    public string HStatus1 { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }
    [XmlElement("IpAddress")]
    private string _IpAddress;
    public string IpAddress { get; set; }
}

[Serializable]
public class Version  /*  Version Mobile  */
{
    [XmlElement("VersionCode")]
    public string VersionCode { get; set; }
    [XmlElement("VersionName")]
    public string VersionName { get; set; }
    [XmlElement("UrlUpdate")]
    public string UrlUpdate { get; set; }
    [XmlElement("Status_System")]
    public int Status_System { get; set; }
}
#endregion

#region Telephone
[Serializable]
public class Tel_List
{
    [XmlElement("m0telidx")]
    public int m0telidx { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("m0status")]
    public int m0status { get; set; }

    [XmlElement("m0_tel")]
    public string m0_tel { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("u0telidx")]
    public int u0telidx { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("u0_tel")]
    public string u0_tel { get; set; }

    [XmlElement("u0status")]
    public int u0status { get; set; }

    [XmlElement("DeptName")]
    public string DeptName { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("PhoneNumber")]
    public string PhoneNumber { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }



}
#endregion

#region Holder Devices
[Serializable]
public class Holder_List
{
    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("name_m0_typedevice")]
    public string name_m0_typedevice { get; set; }

    [XmlElement("HolderDate")]
    public string HolderDate { get; set; }

    [XmlElement("NHDEmpIDX")]
    public int NHDEmpIDX { get; set; }

    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }

    [XmlElement("u0_acc")]
    public string u0_acc { get; set; }

}
#endregion

[Serializable]
public class Child_List
{
    [XmlElement("emp_idx_chi")]
    public int emp_idx_chi { get; set; }
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("CHIDX")]
    public int CHIDX { get; set; }

    [XmlElement("Child_name")]
    public string Child_name { get; set; }

    [XmlElement("Child_num")]
    public string Child_num { get; set; }
}

[Serializable]
public class Prior_List
{
    [XmlElement("emp_idx_pri")]
    public int emp_idx_pri { get; set; }
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("ExperIDX")]
    public int ExperIDX { get; set; }

    [XmlElement("Pri_Nameorg")]
    public string Pri_Nameorg { get; set; }

    [XmlElement("Pri_pos")]
    public string Pri_pos { get; set; }

    [XmlElement("Pri_worktime")]
    public string Pri_worktime { get; set; }

    [XmlElement("Pri_resign")]
    public string Pri_resign { get; set; }

    [XmlElement("Pri_startdate")]
    public string Pri_startdate { get; set; }

    [XmlElement("Pri_resigndate")]
    public string Pri_resigndate { get; set; }

    [XmlElement("Pri_description")]
    public string Pri_description { get; set; }

    [XmlElement("Pri_salary")]
    public string Pri_salary { get; set; }
}

[Serializable]
public class Education_List
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("EDUIDX")]
    public int EDUIDX { get; set; }

    [XmlElement("Edu_qualification_ID")]
    public string Edu_qualification_ID { get; set; }

    [XmlElement("Edu_qualification")]
    public string Edu_qualification { get; set; }

    [XmlElement("Edu_name")]
    public string Edu_name { get; set; }

    [XmlElement("Edu_branch")]
    public string Edu_branch { get; set; }

    [XmlElement("Edu_start")]
    public string Edu_start { get; set; }

    [XmlElement("Edu_end")]
    public string Edu_end { get; set; }

    [XmlElement("qualification_name")]
    public string qualification_name { get; set; }
}

[Serializable]
public class Train_List
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("TNIDX")]
    public int TNIDX { get; set; }

    [XmlElement("Train_courses")]
    public string Train_courses { get; set; }

    [XmlElement("Train_date")]
    public string Train_date { get; set; }

    [XmlElement("Train_assessment")]
    public string Train_assessment { get; set; }

    [XmlElement("Train_enddate")]
    public string Train_enddate { get; set; }
}

[Serializable]
public class ODSP_List
{
    [XmlElement("OrgIDX_S")]
    public int OrgIDX_S { get; set; }
    [XmlElement("OrgNameTH_S")]
    public string OrgNameTH_S { get; set; }
    [XmlElement("OrgNameEN_S")]
    public string OrgNameEN_S { get; set; }
    [XmlElement("RDeptIDX_S")]
    public int RDeptIDX_S { get; set; }
    [XmlElement("DeptNameTH_S")]
    public string DeptNameTH_S { get; set; }
    [XmlElement("DeptNameEN_S")]
    public string DeptNameEN_S { get; set; }
    [XmlElement("RSecIDX_S")]
    public int RSecIDX_S { get; set; }
    [XmlElement("SecNameTH_S")]
    public string SecNameTH_S { get; set; }
    [XmlElement("SecNameEN_S")]
    public string SecNameEN_S { get; set; }
    [XmlElement("RPosIDX_S")]
    public int RPosIDX_S { get; set; }
    [XmlElement("PosNameTH_S")]
    public string PosNameTH_S { get; set; }
    [XmlElement("PosNameEN_S")]
    public string PosNameEN_S { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("EODSPIDX")]
    public int EODSPIDX { get; set; }
    [XmlElement("EmpIDXUser")]
    public int EmpIDXUser { get; set; }
    [XmlElement("EStatus")]
    public int EStatus { get; set; }
    [XmlElement("EStaOut")]
    public int EStaOut { get; set; }
    [XmlElement("JobLevel")]
    public int JobLevel { get; set; }
}

[Serializable]
public class Address_List
{
    [XmlElement("country_idx")]
    public int country_idx { get; set; }

    [XmlElement("country_name")]
    public string country_name { get; set; }

    [XmlElement("prov_idx")]
    public int prov_idx { get; set; }

    [XmlElement("prov_name")]
    public string prov_name { get; set; }

    [XmlElement("amp_idx")]
    public int amp_idx { get; set; }

    [XmlElement("amp_name")]
    public string amp_name { get; set; }

    [XmlElement("dist_idx")]
    public int dist_idx { get; set; }

    [XmlElement("dist_name")]
    public string dist_name { get; set; }

    [XmlElement("NatIDX")]
    public int NatIDX { get; set; }

    [XmlElement("NatName")]
    public string NatName { get; set; }

}

[Serializable]
public class Referent_List
{
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("ReIDX")]
    public int ReIDX { get; set; }

    [XmlElement("ref_fullname")]
    public string ref_fullname { get; set; }

    [XmlElement("ref_position")]
    public string ref_position { get; set; }

    [XmlElement("ref_relation")]
    public string ref_relation { get; set; }

    [XmlElement("ref_tel")]
    public string ref_tel { get; set; }
}

#region Manage_Position_List
[Serializable]
public class Manage_Position_List  /*  Manage_Position_List DataBox  */
{
    [XmlElement("PIDX")]
    public int PIDX { get; set; }

    [XmlElement("PosGroupIDX")]
    public int PosGroupIDX { get; set; }

    [XmlElement("P_Name")]
    public string P_Name { get; set; }

    [XmlElement("P_Description")]
    public string P_Description { get; set; }

    [XmlElement("P_Location")]
    public string P_Location { get; set; }

    [XmlElement("P_Property")]
    public string P_Property { get; set; }

    [XmlElement("P_Unit")]
    public int P_Unit { get; set; }

    [XmlElement("P_Salary")]
    public string P_Salary { get; set; }

    [XmlElement("P_Contract")]
    public string P_Contract { get; set; }

    [XmlElement("P_EmpCreate")]
    public int P_EmpCreate { get; set; }

    [XmlElement("P_EmpDate")]
    public string P_EmpDate { get; set; }

    [XmlElement("P_Status")]
    public int P_Status { get; set; }

    [XmlElement("P_Piority")]
    public int P_Piority { get; set; }

    [XmlElement("PosGroupNameTH")]
    public string PosGroupNameTH { get; set; }

    [XmlElement("PosGroupNameEN")]
    public string PosGroupNameEN { get; set; }

    [XmlElement("PosGroupStatus")]
    public int PosGroupStatus { get; set; }

    [XmlElement("P_EmpIDX")]
    public string P_EmpIDX { get; set; }

    [XmlElement("P_m0_toidx")]
    public string P_m0_toidx { get; set; }

    [XmlElement("m0_toidx")]
    public int m0_toidx { get; set; }

    [XmlElement("P_Choose")]
    public int P_Choose { get; set; }

}


[Serializable]
public class Manage_Position_List_Topic
{
    [XmlElement("topic_name")]
    public string topic_name { get; set; }

    [XmlElement("P_Name")]
    public string P_Name { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("PIDX")]
    public int PIDX { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("m0_toidx")]
    public int m0_toidx { get; set; }

    [XmlElement("P_EmpIDX")]
    public int P_EmpIDX { get; set; }

    [XmlElement("TPIDX")]
    public int TPIDX { get; set; }

    [XmlElement("P_Description")]
    public string P_Description { get; set; }

    [XmlElement("P_Location")]
    public string P_Location { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }


}
#endregion




#region Typevisa Devices
[Serializable]
public class Typevisa_List
{
    [XmlElement("VisaIDX")]
    public int VisaIDX { get; set; }

    [XmlElement("name_visa_th")]
    public string name_visa_th { get; set; }

    [XmlElement("name_visa_en")]
    public string name_visa_en { get; set; }

    [XmlElement("YearVisa")]
    public int YearVisa { get; set; }

    [XmlElement("id_card_visa")]
    public string id_card_visa { get; set; }

    [XmlElement("visa_startdate")]
    public string visa_startdate { get; set; }

    [XmlElement("visa_enddate")]
    public string visa_enddate { get; set; }

    [XmlElement("CEmapIDX")]
    public int CEmapIDX { get; set; }

    [XmlElement("VTStatus")]
    public int VTStatus { get; set; }

    [XmlElement("name_visa")]
    public string name_visa { get; set; }

}
#endregion

#region Report Time
[Serializable]
public class employee_detail_time
{
    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("time_all_sum")]
    public string time_all_sum { get; set; }

    [XmlElement("time_in")]
    public string time_in { get; set; }

    [XmlElement("time_in_rate")]
    public string time_in_rate { get; set; }

    [XmlElement("time_out")]
    public string time_out { get; set; }

    [XmlElement("time_leave")]
    public string time_leave { get; set; }

    [XmlElement("empshif_idx")]
    public int empshif_idx { get; set; }

    [XmlElement("empshift_name")]
    public string empshift_name { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("traget_idx")]
    public int traget_idx { get; set; }

    [XmlElement("emp_type_idx")]
    public int emp_type_idx { get; set; }

    [XmlElement("datetime_start")]
    public string datetime_start { get; set; }

    [XmlElement("datetime_end")]
    public string datetime_end { get; set; }

    [XmlElement("Select_action")]
    public int Select_action { get; set; }

}
#endregion

#region Shift Time
[Serializable]
public class ShiftTime
{
    [XmlElement("empShiftTime_idx")]
    public int empShiftTime_idx { get; set; }
    [XmlElement("shiftTime_idx")]
    public int shiftTime_idx { get; set; }
    [XmlElement("idxTypeShiftTime")]
    public int idxTypeShiftTime { get; set; }
    [XmlElement("nameType_shift")]
    public string nameType_shift { get; set; }
    [XmlElement("nameTime_shift")]
    public string nameTime_shift { get; set; }
    [XmlElement("type_shiftTime")]
    public string type_shiftTime { get; set; }

    [XmlElement("emp_shift_idx")]
    public int emp_shift_idx { get; set; }
    [XmlElement("org_idx_shiftTime")]
    public int org_idx_shiftTime { get; set; }
    [XmlElement("rdept_idx_shiftTime")]
    public int rdept_idx_shiftTime { get; set; }
    [XmlElement("rsec_idx_shiftTime")]
    public int rsec_idx_shiftTime { get; set; }
    [XmlElement("rpos_idx_shiftTime")]
    public int rpos_idx_shiftTime { get; set; }

    [XmlElement("employee_code")]
    public string employee_code { get; set; }
    [XmlElement("fullname_employee")]
    public string fullname_employee { get; set; }
    [XmlElement("org_shiftTime")]
    public string org_shiftTime { get; set; }
    [XmlElement("dept_shiftTime")]
    public string dept_shiftTime { get; set; }
    [XmlElement("sec_shiftTime")]
    public string sec_shiftTime { get; set; }
    [XmlElement("pos_shiftTime")]
    public string pos_shiftTime { get; set; }
    [XmlElement("search_shiftTime")]
    public string search_shiftTime { get; set; }

    [XmlElement("midx")]
    public int midx { get; set; }
    [XmlElement("shif_use")]
    public int shif_use { get; set; }
    [XmlElement("TypeWork")]
    public string TypeWork { get; set; }
}

#endregion

#region Asset_Type_Detail
[Serializable]
public class Asset_Type_Detail
{
    [XmlElement("asidx")]
    public int asidx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("idx_sup")]
    public int idx_sup { get; set; }

    [XmlElement("as_name")]
    public string as_name { get; set; }

    [XmlElement("as_emp_create")]
    public string as_emp_create { get; set; }

    [XmlElement("as_status")]
    public int as_status { get; set; }
}
#endregion

#region Unit_Type_Detail
[Serializable]
public class Unit_Type_Detail
{
    [XmlElement("m0_unidx")]
    public int m0_unidx { get; set; }

    [XmlElement("m0_name")]
    public string m0_name { get; set; }

    [XmlElement("m0_emp_create")]
    public int m0_emp_create { get; set; }

    [XmlElement("m0_createdate")]
    public string m0_createdate { get; set; }

    [XmlElement("m0_updatedate")]
    public string m0_updatedate { get; set; }

    [XmlElement("m0_status")]
    public int m0_status { get; set; }
}
#endregion

#region Holder_Detail
[Serializable]
public class Holder_Detail
{
    [XmlElement("u0_asidx")] //idx list
    public int u0_asidx { get; set; }

    [XmlElement("m0_asidx")]
    public int m0_asidx { get; set; }

    [XmlElement("m0_name")] //unit type name
    public string m0_name { get; set; }

    [XmlElement("u0_asset_no")] //asset no.
    public string u0_asset_no { get; set; }

    [XmlElement("u0_detail")] //detail
    public string u0_detail { get; set; }

    [XmlElement("u0_number")] //number
    public int u0_number { get; set; }

    [XmlElement("m0_unidx")]
    public int m0_unidx { get; set; }

    [XmlElement("u0_holderdate")] //holder date
    public string u0_holderdate { get; set; }

    [XmlElement("u0_emp_create")] //idx create
    public int u0_emp_create { get; set; }

    [XmlElement("u0_createdate")]
    public string u0_createdate { get; set; }

    [XmlElement("u0_updatedate")]
    public string u0_updatedate { get; set; }

    [XmlElement("u0_status")]
    public int u0_status { get; set; }

    [XmlElement("asidx")]
    public int asidx { get; set; }

    [XmlElement("idx_sup")]
    public int idx_sup { get; set; }

    [XmlElement("as_name")] //asset type
    public string as_name { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("Select_action")]
    public int Select_action { get; set; }
}
#endregion

[Serializable]
public class Employee_Probation
{
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("IFLenght")]
    public int IFLenght { get; set; }

    [XmlElement("EmpCode1")]
    public string EmpCode1 { get; set; }

    [XmlElement("EmpCode")]
    public string EmpCode { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }

    [XmlElement("EmpTypeName")]
    public string EmpTypeName { get; set; }

    [XmlElement("EmpProbation")]
    public int EmpProbation { get; set; }

    [XmlElement("EmpProbationDate")]
    public string EmpProbationDate { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }

    [XmlElement("JobGradeIDX")]
    public int JobGradeIDX { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("RDeptID")]
    public int RDeptID { get; set; }

    [XmlElement("RSecID")]
    public int RSecID { get; set; }

    [XmlElement("EmpType")]
    public int EmpType { get; set; }

    [XmlElement("EmpProbationDate_Start")]
    public string EmpProbationDate_Start { get; set; }

    [XmlElement("EmpProbationDate_End")]
    public string EmpProbationDate_End { get; set; }

    [XmlElement("IFSearchBetween")]
    public int IFSearchBetween { get; set; }

    [XmlElement("EmpIN_Ex")]
    public string EmpIN_Ex { get; set; }

    [XmlElement("Condition")]
    public int Condition { get; set; }

}

[Serializable]
public class Export_Probation
{
    [XmlElement("ประเภทพนักงาน")]
    public string ประเภทพนักงาน { get; set; }


    [XmlElement("วันที่เริ่มงาน")]
    public string วันที่เริ่มงาน { get; set; }



    [XmlElement("องค์กร")]
    public string องค์กร { get; set; }


    [XmlElement("ฝ่าย")]
    public string ฝ่าย { get; set; }


    [XmlElement("แผนก")]
    public string แผนก { get; set; }


    [XmlElement("ตำแหน่ง")]
    public string ตำแหน่ง { get; set; }


    [XmlElement("รหัสพนักงาน")]
    public string รหัสพนักงาน { get; set; }

    [XmlElement("ชื่อพนักงาน")]
    public string ชื่อพนักงาน { get; set; }


    [XmlElement("JobGrade")]
    public int JobGrade { get; set; }

    [XmlElement("ประจำสถานที่")]
    public string ประจำสถานที่ { get; set; }


    [XmlElement("สถานะพนักงาน")]
    public string สถานะพนักงาน { get; set; }


    [XmlElement("วันที่ผ่านทดลองงาน")]
    public string วันที่ผ่านทดลองงาน { get; set; }



}


[Serializable]
public class Gurantee_list
{
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("GUAIDX")]
    public int GUAIDX { get; set; }

    [XmlElement("gu_fullname")]
    public string gu_fullname { get; set; }

    [XmlElement("gu_relation")]
    public string gu_relation { get; set; }

    [XmlElement("gu_startdate")]
    public string gu_startdate { get; set; }

    [XmlElement("gu_status")]
    public string gu_status { get; set; }

    [XmlElement("gu_createdate")]
    public string gu_createdate { get; set; }

    [XmlElement("gu_tel")]
    public string gu_tel { get; set; }
}

[Serializable]
public class EmployeeLeave_list
{
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("leavetype_1")]
    public String leavetype_1 { get; set; }
    [XmlElement("leavetype_2")]
    public String leavetype_2 { get; set; }
    [XmlElement("leavetype_3")]
    public String leavetype_3 { get; set; }
    [XmlElement("leavetype_4")]
    public String leavetype_4 { get; set; }
    [XmlElement("leavetype_5")]
    public String leavetype_5 { get; set; }
    [XmlElement("leavetype_6")]
    public String leavetype_6 { get; set; }
    [XmlElement("leavetype_17")]
    public String leavetype_17 { get; set; }
    [XmlElement("leavetype_18")]
    public String leavetype_18 { get; set; }
    [XmlElement("leavetype_19")]
    public String leavetype_19 { get; set; }
    [XmlElement("leavetype_20")]
    public String leavetype_20 { get; set; }
    [XmlElement("leavetype_21")]
    public String leavetype_21 { get; set; }
}

[Serializable]
public class EmployeeResign_list
{
    [XmlElement("emp_idx_resign")]
    public int emp_idx_resign { get; set; }
    [XmlElement("RTIDX")]
    public int RTIDX { get; set; }
    [XmlElement("RT_Name")]
    public string RT_Name { get; set; }
    [XmlElement("RT_Status")]
    public int RT_Status { get; set; }
    [XmlElement("RT_CreateDate")]
    public string RT_CreateDate { get; set; }
}


//#endregion