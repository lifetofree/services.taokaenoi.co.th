﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_ecom_channel")]
public class data_ecom_channel
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }



    // --- master data ---//

    public ecom_m0_channel[] ecom_m0_channel_list { get; set; }

}


public class ecom_m0_channel
{



    public int channel_idx { get; set; }


    public string channel_name { get; set; }

   
    public int cemp_idx { get; set; }

 
    public string m0_order_type { get; set; }


    public string m0_sale_org { get; set; }

   
    public string m0_channel { get; set; }

    
    public string m0_division { get; set; }

    public string m0_sales_office { get; set; }

    public string m0_sales_group { get; set; }

    public string m0_customer_no { get; set; }

    public int channel_status { get; set; }

    public string create_date { get; set; }

    public string update_date { get; set; }

}
