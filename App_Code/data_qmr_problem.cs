﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_qmr_problem")]
public class data_qmr_problem
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }
    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }

    [XmlElement("Boxu0_ProblemDocument")]
    public U0_ProblemDocument[] Boxu0_ProblemDocument { get; set; }
    public M0_ProblemDocument[] Boxm0_ProblemDocument { get; set; }
    public U3_ProblemDocument[] Boxu3_ProblemDocument { get; set; }

}

[Serializable]
public class U0_ProblemDocument
{
    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("LocIDX_List")]
    public int LocIDX_List { get; set; }

    [XmlElement("BuildingIDX")]
    public int BuildingIDX { get; set; }

    [XmlElement("m0_tpidx")]
    public int m0_tpidx { get; set; }

    [XmlElement("problem_name")]
    public string problem_name { get; set; }

    [XmlElement("unit_name")]
    public string unit_name { get; set; }

    [XmlElement("m0_unidx")]
    public int m0_unidx { get; set; }

    [XmlElement("matname")]
    public string matname { get; set; }

    [XmlElement("matidx")]
    public int matidx { get; set; }

    [XmlElement("mat_no")]
    public int mat_no { get; set; }

    [XmlElement("count_sup")]
    public int count_sup { get; set; }


    [XmlElement("mat_no_search")]
    public string mat_no_search { get; set; }

    [XmlElement("sup_name")]
    public string sup_name { get; set; }

    [XmlElement("m0_supidx")]
    public int m0_supidx { get; set; }

    [XmlElement("m0_pridx")]
    public int m0_pridx { get; set; }

    [XmlElement("orgidx")]
    public int orgidx { get; set; }

    [XmlElement("rdeptidx")]
    public int rdeptidx { get; set; }

    [XmlElement("rsecidx")]
    public int rsecidx { get; set; }

    [XmlElement("rposidx")]
    public int rposidx { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("empcode")]
    public string empcode { get; set; }

    [XmlElement("doc_code")]
    public string doc_code { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("m0_plidx")]
    public int m0_plidx { get; set; }

    [XmlElement("batchnumber")]
    public string batchnumber { get; set; }

    [XmlElement("dategetproduct")]
    public string dategetproduct { get; set; }

    [XmlElement("qty_get")]
    public string qty_get { get; set; }

    [XmlElement("unit_get")]
    public int unit_get { get; set; }

    [XmlElement("qty_problem")]
    public string qty_problem { get; set; }

    [XmlElement("unit_problem")]
    public int unit_problem { get; set; }

    [XmlElement("qty_random")]
    public string qty_random { get; set; }

    [XmlElement("unit_random")]
    public int unit_random { get; set; }

    [XmlElement("detail_remark")]
    public string detail_remark { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("u0_docidx")]
    public int u0_docidx { get; set; }

    [XmlElement("u1_docidx")]
    public int u1_docidx { get; set; }

    [XmlElement("typeproduct_name")]
    public string typeproduct_name { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }

    [XmlElement("BuildingName")]
    public string BuildingName { get; set; }

    [XmlElement("production_name")]
    public string production_name { get; set; }

    [XmlElement("JobGradeIDX")]
    public int JobGradeIDX { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("LocIDX_ref")]
    public int LocIDX_ref { get; set; }

    [XmlElement("m0_pbidx")]
    public int m0_pbidx { get; set; }

    [XmlElement("m0_rbidx")]
    public int m0_rbidx { get; set; }

    [XmlElement("qty_rollback")]
    public string qty_rollback { get; set; }

    [XmlElement("unit_rollback")]
    public int unit_rollback { get; set; }

    [XmlElement("detail_rollback")]
    public string detail_rollback { get; set; }

    [XmlElement("type_problem_name")]
    public string type_problem_name { get; set; }

    [XmlElement("rollback_name")]
    public string rollback_name { get; set; }

    [XmlElement("userlogin")]
    public string userlogin { get; set; }

    [XmlElement("passlogin")]
    public string passlogin { get; set; }

    [XmlElement("ifsearch")]
    public int ifsearch { get; set; }

    [XmlElement("datecreate")]
    public string datecreate { get; set; }

    [XmlElement("dateend")]
    public string dateend { get; set; }

    [XmlElement("admin_name")]
    public string admin_name { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("Email_HeadUser")]
    public string Email_HeadUser { get; set; }

    [XmlElement("Email_QA")]
    public string Email_QA { get; set; }

    [XmlElement("Email_DirQA")]
    public string Email_DirQA { get; set; }


    [XmlElement("Email_HeadQA")]
    public string Email_HeadQA { get; set; }

    [XmlElement("email_sup")]
    public string email_sup { get; set; }

    [XmlElement("Email_PUR")]
    public string Email_PUR { get; set; }

    [XmlElement("comment_sup")]
    public string comment_sup { get; set; }

    [XmlElement("downtime")]
    public string downtime { get; set; }

    [XmlElement("unit_name_problem")]
    public string unit_name_problem { get; set; }

    [XmlElement("unit_name_random")]
    public string unit_name_random { get; set; }

    [XmlElement("unit_name_rollback")]
    public string unit_name_rollback { get; set; }

    [XmlElement("datemodify")]
    public string datemodify { get; set; }

    [XmlElement("dateexp")]
    public string dateexp { get; set; }

    [XmlElement("dateproblem")]
    public string dateproblem { get; set; }

    [XmlElement("po_no")]
    public string po_no { get; set; }

    [XmlElement("tv_no")]
    public string tv_no { get; set; }

    [XmlElement("percent_seaweed")]
    public string percent_seaweed { get; set; }

    [XmlElement("code_seaweed")]
    public string code_seaweed { get; set; }

    [XmlElement("u2_docidx")]
    public int u2_docidx { get; set; }

    [XmlElement("m0_mridx")]
    public int m0_mridx { get; set; }

    [XmlElement("material_risk")]
    public string material_risk { get; set; }

    [XmlElement("m1_tpidx")]
    public int m1_tpidx { get; set; }

    [XmlElement("m2_tpidx")]
    public int m2_tpidx { get; set; }

    [XmlElement("group_name")]
    public string group_name { get; set; }

    [XmlElement("typeproduct_name_m1")]
    public string typeproduct_name_m1 { get; set; }

    [XmlElement("typeproduct_name_m2")]
    public string typeproduct_name_m2 { get; set; }

    [XmlElement("m0_group_tpidx")]
    public int m0_group_tpidx { get; set; }

    [XmlElement("UpdateDate")]
    public string UpdateDate { get; set; }

    [XmlElement("count_date")]
    public int count_date { get; set; }

    [XmlElement("month_create")]
    public string month_create { get; set; }

    [XmlElement("send_pur")]
    public string send_pur { get; set; }

    [XmlElement("group_name_new")]
    public string group_name_new { get; set; }

    [XmlElement("adminidx")]
    public int adminidx { get; set; }

    [XmlElement("factory_code")]
    public string factory_code { get; set; }

    [XmlElement("qty_seaweed")]
    public string qty_seaweed { get; set; }

    [XmlElement("unit_seaweed")]
    public int unit_seaweed { get; set; }

    [XmlElement("unit_name_seaweed")]
    public string unit_name_seaweed { get; set; }

}

[Serializable]
public class M0_ProblemDocument
{
    public int l0_docidx { get; set; }
    public string status_name { get; set; }
    public int staidx { get; set; }
    public string typeproduct_name { get; set; }
    public int status { get; set; }
    public int m0_tpidx { get; set; }
    public string unit_name { get; set; }
    public int m0_unidx { get; set; }
    public int LocIDX { get; set; }
    public string LocName { get; set; }
    public int BuildingIDX { get; set; }
    public string BuildingName { get; set; }
    public string production_name { get; set; }
    public int m0_plidx { get; set; }
    public string problem_name { get; set; }
    public int m0_pridx { get; set; }
    public string matname { get; set; }
    public int matidx { get; set; }
    public int mat_no { get; set; }
    public string sup_name { get; set; }
    public int m0_supidx { get; set; }
    public string node_name { get; set; }
    public string actor_name { get; set; }
    public string createdate { get; set; }
    public string comment { get; set; }
    public string FullNameTH { get; set; }
    public int m0_pbidx { get; set; }
    public string type_problem_name { get; set; }
    public string rollback_name { get; set; }
    public int m0_rbidx { get; set; }
    public int acidx { get; set; }
    public string email { get; set; }
    public int m0_mridx { get; set; }
    public string material_risk { get; set; }
    public int m1_tpidx { get; set; }
    public int m2_tpidx { get; set; }
    public string group_name { get; set; }
    public string typeproduct_name_m1 { get; set; }
    public string typeproduct_name_m2 { get; set; }
    public int m0_group_tpidx { get; set; }
    public int status_edit { get; set; }
    public string group_name_new { get; set; }
    public int JobGradeIDX { get; set; }
    public int rsecidx { get; set; }
    public string emp_name_th { get; set; }
    public int status_approve { get; set; }
    public int status_new { get; set; }
    public string statusname_new { get; set; }
    public string statusname_old { get; set; }
    public string material_risk_new { get; set; }
    public string typeproduct_name_new { get; set; }
    public int m0_tpidx_new { get; set; }
    public string problem_name_new { get; set; }

}


[Serializable]
public class U3_ProblemDocument
{
    public int m0_mridx { get; set; }
    public string material_risk { get; set; }
    public string comment_risk { get; set; }
    public int u2_docidx { get; set; }
    public int u3_docidx { get; set; }
    public string m0_mridx_comma { get; set; }
}