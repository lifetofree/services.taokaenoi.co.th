﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
/// <summary>
/// Summary description for api_holderdevices
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_holderdevices : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = String.Empty;
    string _mail_subject = "";
    string _mail_body = "";


    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    DataHoldersDevices _dtholder = new DataHoldersDevices();
    service_mail _serviceMail = new service_mail();
    string reply = "it_support@taokaenoi.co.th";
    string link = "http://mas.taokaenoi.co.th/holder-devices";

    public api_holderdevices()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region SELECT

    // Select List

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void select_view_user7(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 211); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void select_user(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 214); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // Select Remote
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void select_view_user8and9(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 212); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Select Search List
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void select_view_user10(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 213); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // Select SYSTEMList
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void select_approvelist(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 210); // return w/ json

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // Select StatusSAP
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void HDEMPIDXANDNHDEMPIDX(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 208); // return w/ json

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Select Location
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void ddl_Approve(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 209); // return w/ json

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // Select Priority
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectDevices(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 202); // return w/ json

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Select_SAPList
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectMasterList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 201); // return w/ json

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // Select_SAPALLList
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectDetailHolder(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 203); // return w/ json

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Select_SAPStatuWaiating
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectHolder(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 204); // return w/ json

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Select_SAPStatuWaiating
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectLogHolder(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 205); // return w/ json

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Select_DetailList
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectApprove1Holder(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 207); // return w/ json

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }



    #endregion

    #region Insert&Update

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_Holder(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 101); // return w/ json
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtholder = (DataHoldersDevices)_funcTool.convertXmlToObject(typeof(DataHoldersDevices), _local_xml);

            var rtCode_mail = _dtholder.ReturnEmail;
            var rtCode_devicecode = _dtholder.ReturnDeviceCode;
            var rtCode_type = _dtholder.ReturnType;

            rtCode_type = rtCode_type.Replace("&", "&amp;");
            string name = rtCode_mail.Split('@')[1].Split('.')[0];
            if (rtCode_mail != "")
            {
                //if (name != "taokaenoi" && name != "taokaenoiland" && name != "gmail" && name != "genc" && name != "drtobi")
                //{
                    //string email = "kantida3620@gmail.com,webmaster@taokaenoi.co.th";

                    _mail_subject = "[MIS/Holder-Devices] -" + rtCode_devicecode.ToString();
                    _mail_body = _serviceMail.HoldersCreateBody(_dtholder, rtCode_type.ToString(), link);
                    _serviceMail.SendHtmlFormattedEmailFull(rtCode_mail.ToString(), "", reply, _mail_subject, _mail_body);

                //}
            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Update_Holder(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 301); // return w/ json

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Update_ApproveChangeHolder(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 303); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtholder = (DataHoldersDevices)_funcTool.convertXmlToObject(typeof(DataHoldersDevices), _local_xml);

            var rtCode_mail = _dtholder.ReturnEmail;
            var rtCode_devicecode = _dtholder.ReturnDeviceCode;
            var rtCode_type = _dtholder.ReturnType;
            var rtCodenode = _dtholder.Return_Node;
            var rtCodeactor = _dtholder.Return_Actor;
            var rtCodestatus = _dtholder.Return_Status;


            rtCode_type = rtCode_type.Replace("&", "&amp;");
            //string email = "kantida3620@gmail.com,webmaster@taokaenoi.co.th";

            _mail_subject = "[MIS/Holder-Devices] -" + rtCode_devicecode.ToString();

            if (rtCodenode.ToString() != "8" && rtCodestatus.ToString() == "1" || rtCodenode.ToString() != "8" && rtCodestatus.ToString() == "3") // Approve 
            {
                _mail_body = _serviceMail.HoldersFlowApproveChangeBody(_dtholder, rtCode_type.ToString(), link);
            }
            else if (rtCodenode.ToString() == "8" && rtCodeactor.ToString() == "2" && rtCodestatus.ToString() == "1" || rtCodenode.ToString() == "8" && rtCodeactor.ToString() == "2" && rtCodestatus.ToString() == "3") // Approve 
            {
                _mail_body = _serviceMail.HoldersApproveChangeBody(_dtholder, rtCode_type.ToString(), link);
            }
            else if (rtCodestatus.ToString() == "2" || rtCodestatus.ToString() == "4") // Non Approve and End
            {
                _mail_body = _serviceMail.HoldersNonApproveChangeBody(_dtholder, rtCode_type.ToString(), link);
            }
            _serviceMail.SendHtmlFormattedEmailFull(rtCode_mail.ToString(), "", reply, _mail_subject, _mail_body);
        }


        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Update_ChangeHolder(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 302); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtholder = (DataHoldersDevices)_funcTool.convertXmlToObject(typeof(DataHoldersDevices), _local_xml);

               var rtCode_mail = _dtholder.ReturnEmail;
                var rtCode_devicecode = _dtholder.ReturnDeviceCode;
                var rtCode_type = _dtholder.ReturnType;

                rtCode_type = rtCode_type.Replace("&", "&amp;");
             //   string email = "kantida3620@gmail.com,webmaster@taokaenoi.co.th";

                _mail_subject = "[MIS/Holder-Devices] -" + rtCode_devicecode.ToString();
                _mail_body = _serviceMail.HoldersChangeBody(_dtholder, rtCode_type.ToString(), link, "0");
                _serviceMail.SendHtmlFormattedEmailFull(rtCode_mail.ToString(), "", reply, _mail_subject, _mail_body);
            
            
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Update_LogHolder(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "DataHoldersDevices", "HoldersDevice", _xml_in, 304); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtholder = (DataHoldersDevices)_funcTool.convertXmlToObject(typeof(DataHoldersDevices), _local_xml);

            
            var rtCode_mail = _dtholder.ReturnEmail;
            var rtCode_devicecode = _dtholder.ReturnDeviceCode;
            var rtCode_type = _dtholder.ReturnType;
            var rtCodestatus = _dtholder.Return_Status;
          //  string email = "kantida3620@gmail.com,webmaster@taokaenoi.co.th";

            _mail_subject = "[MIS/Holder-Devices] -" + rtCode_devicecode;
            _mail_body = _serviceMail.HoldersChangeBody(_dtholder, rtCode_type, link, rtCodestatus);
            _serviceMail.SendHtmlFormattedEmailFull(rtCode_mail.ToString(), "", reply, _mail_subject, _mail_body);
            
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    #endregion



}
