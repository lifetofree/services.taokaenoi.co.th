﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_hr_news
/// </summary>
/// 

[Serializable]
[XmlRoot("data_hr_news")]

public class data_hr_news
{
    
    public int return_code { get; set; }
    
    public string return_msg { get; set; }
    
    public int return_idx { get; set; }

    public int news_mode { get; set; }


    public hr_news_type_detail_m0[] hr_news_type_m0 { get; set; } //type news m0

    
    public hr_news_type_detail_m1[] hr_news_type_m1 { get; set; } //title news m1

    
    public hr_news_list_detail_u0[] hr_news_list_u0 { get; set; } //list news u0

    
    public hr_news_slide_detail_u0[] hr_news_slide_u0 { get; set; } //slide news u0

    
    public hr_news_img_detail_u0[] hr_news_img_u0 { get; set; } //img news u0

    public hr_news_search_detail[] hr_news_search_list { get; set; } //news search

    public hr_news_per_detail[] hr_news_per_list { get; set; } //news permission


}
[Serializable]

public class hr_news_type_detail_m0
{
    
    public int type_idx { get; set; }
    
    public string type_name { get; set; }
    
    public int cemp_idx { get; set; }
    
    public int type_status { get; set; }
    
    public string create_date { get; set; }
    
    public string update_date { get; set; }
}
[Serializable]

public class hr_news_type_detail_m1
{
    
    public int title_idx { get; set; }
    
    public int type_idx { get; set; }
    
    public string type_name { get; set; }
    
    public string title_name { get; set; }
    
    public int title_type_file { get; set; }
    
    public int title_status { get; set; }
    
    public int cemp_idx { get; set; }
    
    public string create_date { get; set; }
    
    public string update_date { get; set; }
}
[Serializable]

public class hr_news_list_detail_u0
{
    
    public int title_idx { get; set; }
    
    public int type_idx { get; set; }
    
    public string type_name { get; set; }
    
    public string title_name { get; set; }
    
    public int title_type_file { get; set; }



    
    public int list_idx { get; set; }
    
    public string list_name { get; set; }
    
    public string list_desc { get; set; }
    
    public string list_file { get; set; }
    
    public string list_timeout_start { get; set; }
    
    public string list_timeout_end { get; set; }
    
    public int list_status { get; set; }
    
    public int cemp_idx { get; set; }
    
    public string create_date { get; set; }
    
    public string update_date { get; set; }
}
[Serializable]
public class hr_news_slide_detail_u0
{
    
    public int slide_idx { get; set; }
    
    public string slide_file { get; set; }
    
    public string slide_timeout_start { get; set; }
    
    public string slide_timeout_end { get; set; }
    
    public int cemp_idx { get; set; }
    
    public int slide_status { get; set; }

    public int slide_show { get; set; }

    public string create_date { get; set; }
    
    public string update_date { get; set; }
}
[Serializable]
public class hr_news_img_detail_u0
{
    
    public int img_idx { get; set; }
    
    public string img_file { get; set; }
    
    public string img_timeout_start { get; set; }
    
    public string img_timeout_end { get; set; }
    
    public int cemp_idx { get; set; }
    
    public int img_status { get; set; }
    
    public string create_date { get; set; }
    
    public string update_date { get; set; }
}

[Serializable]
public class hr_news_search_detail
{
    public string s_type { get; set; }

    public string s_title { get; set; }

    public string s_name { get; set; }

    public string s_status { get; set; }

    public string s_show { get; set; }

    public string s_start_time { get; set; }

    public string s_end_time { get; set; }
}

[Serializable]
public class hr_news_per_detail
{
   
    public int per_idx { get; set; }

    public string per_rpos_id { get; set; }
 
    public string per_name { get; set; }
  
    public int cemp_idx { get; set; }
 
    public int per_status { get; set; }
   
    public string create_date { get; set; }
  
    public string update_date { get; set; }

}