﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_drc
/// </summary>
/// 
[Serializable]
[XmlRoot("data_drc")]
public class data_drc
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

     public TypeTopicDetail[] BoxTypeTopicList { get; set; }
    public TypeWorkDetail[] BoxTypeWorkList { get; set; }
    public SetNameDetail[] BoxM0SetNameList { get; set; }
    public SetTopicNameDetail[] BoxTopicNameList { get; set; }
    public SetOptionDetail[] BoxOptionList { get; set; }
    public SetFormR0MasterDetail[] BoxFormR0MasterList { get; set; }
    public SetFormR1MasterDetail[] BoxFormR1MasterList { get; set; }

}

public class TypeTopicDetail
{   
    public int TYIDX { get; set; }
    public string Typename { get; set; }
    public int Type_status { get; set; }
    public int CEmpIDX { get; set; }
    public string Type_statusDetail { get; set; }
    public int EmpIDX { get; set; }
    public string FullNameTH { get; set; }
    public int EmpStatus { get; set; }
    public string topic_name { get; set; }
    public int tidx { get; set; }

}

public class TypeWorkDetail
{

    public int TWIDX { get; set; }
    public string Type_Work { get; set; }
    public int Work_Status { get; set; }
    public int CEmpIDX { get; set; }
    public string Work_StatusDetail { get; set; }

}

public class SetNameDetail
{
    public int sidx { get; set; }
    public int setroot_idx { get; set; }
    public string set_name { get; set; }
    public string set_detail { get; set; }
    public int set_status { get; set; }
    public int cempidx { get; set; }
    public string set_statusDetail { get; set; }
    public int tidx { get; set; }
    public int u0idx { get; set; }
    public int R0IDX { get; set; }
    public int value2 { get; set; }
    public int root_tidx { get; set; }
    public string value1 { get; set; }
    public string test1 { get; set; }

}

public class SetTopicNameDetail
{

    public int tidx { get; set; }
    public int sidx { get; set; }
    public string set_name { get; set; }
    public int OPIDX { get; set; }
    public string Option_name { get; set; }
    public int root_idx { get; set; }
    public string topic_name { get; set; }
    public string topic_detail { get; set; }
    public int topic_status { get; set; }
    public int cempidx { get; set; }
    public string topic_statusDetail { get; set; }
    public int R0IDX { get; set; }
    public int u0idx { get; set; }



}

public class SetOptionDetail
{

    public int OPIDX { get; set; }
    public string Option_name { get; set; }
    public int Option_Status { get; set; }
    public int CEmpIDX { get; set; }

}

public class SetFormR0MasterDetail
{

    public int R0IDX { get; set; }
    public string R0_Name { get; set; }
    public int R0_Status { get; set; }
    public int cempidx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int TWIDX { get; set; }
    public int R1IDX { get; set; }
    public int R1_Status { get; set; }
    public int tidx { get; set; }
    public string topic_name { get; set; }
    public string topicnameroot { get; set; }
    public int root_idx { get; set; }
    public int sidx { get; set; }
    public int OPIDX { get; set; }

}

public class SetFormR1MasterDetail
{

    public int R0IDX { get; set; }
    public string R0_Name { get; set; }
    public int R0_Status { get; set; }
    public int cempidx_add { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int TWIDX { get; set; }
    public int R1IDX { get; set; }
    public int R1_Status { get; set; }
    public int tidx_add { get; set; }
    public int tidx { get; set; }
    public string topic_name { get; set; }

}

