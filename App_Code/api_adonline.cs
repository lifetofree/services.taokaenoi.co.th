using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

public class api_adonline : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_adonline dataADOnline = new data_adonline();

    public api_adonline()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region Constants
    public static class Constants
    {
        public const int NUMBER_NULL = -1;
        public const int CREATE = 10;
        public const int UPDATE = 11;
        public const int UPDATE_NODE = 12;
        public const int UPDATE_STATUS = 13;
        public const int UPDATE_IT = 14;
        public const int SELECT_ALL = 20;
        public const int SELECT_WHERE = 21;
        public const int SELECT_WHERE_WAITING = 22;
        public const int SELECT_WHERE_IN = 23;
        public const int SELECT_ALL_ONLINE = 24;
        public const int SELECT_EMAIL = 26;
        public const int SELECT_WHERE_EMP_CODE = 27;
        public const int SELECT_WHERE_APPROVER = 28;
        public const int SELECT_WHERE_EMP_DATA = 29;
        public const int SELECT_ADD_MORE_DATA = 25;
        public const int BAN = 30;
        public const int UNBAN = 31;
        public const string VALID_FALSE = "101";
        public const int RADIO_PERM_PERM = 2;
        public const int RADIO_PERM_TEMP = 1;
        public const int RADIO_EMP_NEW = 1;
        public const int RADIO_EMP_OLD = 2;
        public const int RADIO_EMP_VISITOR = 3;
        public const int CREATE_U0_PERM = 10;
        public const int CREATE_U1_PERM = 11;
        public const string PREFIX_ALPHABET_PERM_PERM = "AD";
        public const string PREFIX_ALPHABET_PERM_TEMP = "GU";
        public const int ORG_TKN_IDX = 1;
        public const int ORG_TOBI_IDX = 2;
        public const int ORG_TKNL_IDX = 3;
        public const string PREFIX_CODE_ID_ORG_TKN = "01";
        public const string PREFIX_CODE_ID_ORG_TKNL = "02";
        public const string PREFIX_CODE_ID_ORG_TOBI = "03";
        public const string WITH_WHERE_YES = "YES";
        public const string WITH_WHERE_NO = "NO";
        public const string KEY_GET_FROM_PERM_TEMP = "TEMP";
        public const string KEY_GET_FROM_PERM_PERM = "PERM";
        public const string TYPE_EMAIL_PERM_TEMP_APP = "APP";
        public const string TYPE_EMAIL_PERM_TEMP_NOTAPP = "NOTAPP";
        public const string TYPE_EMAIL_PERM_TEMP_SUCCESS = "SUCCESS";
        public const int PERM_TEMP_MAX_CONTACT = 10;
        public const int NODE_PERM_TEMP_CREATE_GOTO_DIRECTOR_CREATOR = 1;
        public const int NODE_PERM_TEMP_APP_GOTO_HEADER_IT = 2;
        public const int NODE_PERM_TEMP_NOTAPP_ENDBY_DIRECTOR_CREATOR = 3;
        public const int NODE_PERM_TEMP_APP_GOTO_DIRECTOR_IT = 4;
        public const int NODE_PERM_TEMP_NOTAPP_ENDBY_HEADER_IT = 5;
        public const int NODE_PERM_TEMP_APP_GOTO_IT = 6;
        public const int NODE_PERM_TEMP_NOTAPP_ENDBY_DIRECTOR_IT = 7;
        public const int NODE_PERM_TEMP_SAVE_ENDBY_IT = 8;
        public const int U1_PERM_TEMP_STATUS_APP_OR_ONLINE = 1;
        public const int U1_PERM_TEMP_STATUS_NOTAPP = 2;
        public const int RDEPT_MIS_ID = 20;
        public const int JOBLEVEL_MIS_RDEPT = 6;
        public const int JOBLEVEL_OTHER_RDEPT = 9;
        public static int[] NODE_PERM_TEMP_APPROVE = { 1, 2, 4, 6 };
        public static int[] NODE_PERM_TEMP_NOT_APPROVE = { 3, 5, 7 };
        public static int[] NODE_PERM_TEMP_SUCCESS = { 8 };
        public static int[] JOBLEVELS_OFFICIAL = { 1, 2, 3, 4, 5 };
        public static int[] JOBLEVELS_HEADER = { 6, 7, 8 };
        public static int[] JOBLEVELS_DIRECTOR = { 9, 10, 11, 12, 13, 14 };
        public const int NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_HR = 1;
        public const int NODE_PERM_PERM_CREATE_GOTO_HEADER_EMPNEW = 2;
        public const int NODE_PERM_PERM_CREATE_GOTO_DIRECTOR_EMPNEW = 3;
        public const int NODE_PERM_PERM_DIRECTOR_HR_APP_GOTO_HEADER_IT = 4;
        public const int NODE_PERM_PERM_DIRECTOR_HR_NOTAPP_ENDBY_DIRECTOR_HR = 5;
        public const int NODE_PERM_PERM_HEADER_EMPNEW_APP_GOTO_DIRECTOR_EMPNEW = 6;
        public const int NODE_PERM_PERM_HEADER_EMPNEW_NOTAPP_ENDBY_HEADER_EMPNEW = 7;
        public const int NODE_PERM_PERM_DIRECTOR_EMPNEW_APP_GOTO_HEADER_IT = 8;
        public const int NODE_PERM_PERM_DIRECTOR_EMPNEW_NOTAPP_ENDBY_DIRECTOR_EMPNEW = 9;
        public const int NODE_PERM_PERM_HEADER_IT_APP_GOTO_DIRECTOR_IT = 10;
        public const int NODE_PERM_PERM_HEADER_IT_NOTAPP_ENDBY_HEADER_IT = 11;
        public const int NODE_PERM_PERM_DIRECTOR_IT_APP_GOTO_IT = 12;
        public const int NODE_PERM_PERM_DIRECTOR_IT_NOTAPP_ENDBY_DIRECTOR_IT = 13;
        public const int NODE_PERM_PERM_IT_APP_FINISHBY_IT = 14;
        public const int NODE_PERM_PERM_IT_NOTAPP_ENDBY_IT = 15;

        public const int SELECT_VPNMAIN = 40;
        public const int SELECT_VPNSUBMAIN = 41;
        public const int SELECT_U2VPN = 42;
        public const int INSERT_U2VPN = 18;
        public const int APPROVE_U2VPN = 19;

    }
    #endregion Constants

    #region M0 permission policy
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readM0PermPol(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adPermissionPolicyService", _xml_in, Constants.SELECT_ALL);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void createM0PermPol(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adPermissionPolicyService", _xml_in, Constants.CREATE);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void updateM0PermPol(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adPermissionPolicyService", _xml_in, Constants.UPDATE);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void deleteM0PermPol(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adPermissionPolicyService", _xml_in, Constants.BAN);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion M0 permission policy

    #region Permission Temporary
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readU0IndexPermTemp(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_ALL);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readU1IndexDetailPermTemp(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_WHERE);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void createU0PermTemp(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.CREATE_U0_PERM);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void createU1PermTemp(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.CREATE_U1_PERM);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readU0WaitingPermTemp(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_WHERE_WAITING);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readU0WaitingDetailPermTemp(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_ALL);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readU1WaitingDetailPermTemp(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_WHERE);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void approveU0WaitingPermTemp(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.UPDATE_NODE);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void approveU1WaitingPermTemp(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.UPDATE_STATUS);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void saveWaitingPermTemp(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.UPDATE_IT);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion Permission Temporary

    #region Permission Permanent
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readU0IndexPermPerm(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_ALL);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readU1IndexDetailPermPerm(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_WHERE);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void createU0PermPerm(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.CREATE_U0_PERM);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readU0WaitingPermPerm(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_WHERE_WAITING);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readU0WaitingDetailPermPerm(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_ALL);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readU1WaitingPermPerm(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_WHERE);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void approveU0WaitingPermPerm(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.UPDATE_NODE);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void saveCommentITU0WaitingPermPerm(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.UPDATE_IT);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion Permission Permanent

    #region Permission Log
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readL0Perm(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adLogOnlineService", _xml_in, Constants.SELECT_WHERE);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void createL0Perm(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adLogOnlineService", _xml_in, Constants.CREATE);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion Permission Log

    #region Others
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readEmpProfile(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_WHERE_EMP_DATA);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readEmpProfileByEmpCode(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_WHERE_EMP_CODE);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readPermType(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adPermissionTypeService", _xml_in, Constants.SELECT_ALL_ONLINE);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readNode(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adCRUDNodeService", _xml_in, Constants.SELECT_ALL);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readEmails(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_EMAIL);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void readPermPol(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adPermissionPolicyService", _xml_in, Constants.SELECT_ALL);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetActive_MenuApprove(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_WHERE_IN);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectWho_CanApprove(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_ALL_ONLINE);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_ADDMORE_DATA(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_ADD_MORE_DATA);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion

    #region VPNMAIN-SUBMAIN
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectVPNMAIN(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_VPNMAIN);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectVPNSUBMAIN(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_VPNSUBMAIN);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void InsertU2VPN(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineVpnService", _xml_in, Constants.INSERT_U2VPN);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectU2VPN(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineService", _xml_in, Constants.SELECT_U2VPN);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void ApproveU2VPN(string jsonIn)
    {
        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("misConn", "data_adonline", "adOnlineVpnService", _xml_in, Constants.APPROVE_U2VPN);
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    #endregion VPNMAIN-SUBMAIN

}
