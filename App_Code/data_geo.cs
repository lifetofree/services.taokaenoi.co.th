using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_geo
/// </summary>
[Serializable]
[XmlRoot("data_geo")]
public class data_geo
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_idx")]
    public int return_idx { get; set; }

    public geo_detail[] geo_list { get; set; }
}

[Serializable]
public class geo_detail
{
    public int idx { get; set; }
    public string code { get; set; }
    public string name_th { get; set; }
    public string name_en { get; set; }

    public int geography_idx { get; set; }
    public int province_idx { get; set; }
    public int amphure_idx { get; set; }
    public int district_idx { get; set; }

    public int zip_code { get; set; }
}