using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;


public class api_hr_payroll : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    service_mail _serviceMail = new service_mail();
    data_hr_payroll _data_hr_payroll = new data_hr_payroll();

    int _tempInt = 0;
    string _mail_subject = "";
    string _mail_body = "";

    public api_hr_payroll()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    // get slip data
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSlipData(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("hrConn", "data_hr_payroll", "service_hr_payroll_slip", _xml_in, 10); // return w/ json

            data_hr_payroll _tempIn = new data_hr_payroll();
            _tempIn = (data_hr_payroll)_funcTool.convertXmlToObject(typeof(data_hr_payroll), _xml_in);
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_hr_payroll = (data_hr_payroll)_funcTool.convertXmlToObject(typeof(data_hr_payroll), _local_xml);
            /*   if(_data_hr_payroll.return_code == 0 && _funcTool.convertToInt(_tempIn.search_slip_data_list[0].s_u0_idx) > 0)
               {
                   // create mail body
                   en_slip_data_detail_u0 _temp_u0 = _data_hr_payroll.en_slip_data_list_u0[0];
                   _mail_subject = "[HR-Slip] : รหัสผ่านใบจ่ายเงินเดือนรอบ - " + _temp_u0.payroll_date;
                   _mail_body = _serviceMail.SlipDownloadBody(_temp_u0);
                   _serviceMail.SendHtmlFormattedEmailPrivate(_temp_u0.personal_email, "", "", _mail_subject, _mail_body);
               }
               */
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get slip data
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetSlipData(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("hrConn", "data_hr_payroll", "service_hr_payroll_slip", _xml_in, 11); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get slip data
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTKNSlipData(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("hrConn", "data_hr_payroll", "service_hr_payroll_slip", _xml_in, 20); // return w/ json

            data_hr_payroll _tempIn = new data_hr_payroll();
            _tempIn = (data_hr_payroll)_funcTool.convertXmlToObject(typeof(data_hr_payroll), _xml_in);
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_hr_payroll = (data_hr_payroll)_funcTool.convertXmlToObject(typeof(data_hr_payroll), _local_xml);
            if (_data_hr_payroll.return_code == 0 && _funcTool.convertToInt(_tempIn.search_slip_data_list[0].s_u0_idx) > 0)
            {
                // create mail body
                en_slip_data_detail_u0 _temp_u0 = _data_hr_payroll.en_slip_data_list_u0[0];
                _mail_subject = "[HR-Slip] : รหัสผ่านใบจ่ายเงินเดือนรอบ - " + _temp_u0.payroll_date;
                _mail_body = _serviceMail.SlipDownloadBody(_temp_u0);
                _serviceMail.SendHtmlFormattedEmailPrivate(_temp_u0.personal_email, "", "", _mail_subject, _mail_body);
            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get slip data
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetTKNSlipData(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("hrConn", "data_hr_payroll", "service_hr_payroll_slip", _xml_in, 21); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
}