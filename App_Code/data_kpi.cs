﻿using System;
using System.Xml.Serialization;
/// <summary>
/// Summary description for data_roombooking
/// </summary>
/// 

[Serializable]
[XmlRoot("data_kpi")]
public class data_kpi
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public kpi_u0_detail[] kpi_details_u0_list { get; set; }
    public kpi_u1_detail[] kpi_details_u1_list { get; set; }
    public kpi_u2_detail[] kpi_details_u2_list { get; set; }

    public kpi_search_template_detail[] kpi_search_template_list { get; set; }

}

#region template
public class kpi_search_template_detail
{

    public int u0_detail_idx { get; set; }
    public string s_u0_detail_idx { get; set; }
    public string s_u1_detail_idx { get; set; }
    public string s_condition { get; set; }
    public string s_cemp_idx { get; set; }

}
#endregion template

public class kpi_u0_detail
{

    public int u0_detail_idx { get; set; }
    public int u1_detail_idx { get; set; }
    public string details_title { get; set; }
    public string details_desc { get; set; }
    public string unit_desc { get; set; }
    public int m0_unit_type_idx { get; set; }
    public string weight { get; set; }
    public string annual_target { get; set; }
    public int order_no { get; set; }
    public int u0_detail_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public string unit_name { get; set; }
    public int condition { get; set; }

}

public class kpi_u1_detail
{

    public int u1_detail_idx { get; set; }
    public int u0_detail_idx { get; set; }
    public string details_title { get; set; }
    public string measurement_detail { get; set; }
    public int m0_unit_type_idx { get; set; }
    public string details_desc { get; set; }
    public string unit_desc { get; set; }
    public string unit_name { get; set; }
    public string weight { get; set; }
    public string deadline { get; set; }
    public int u1_detail_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int condition { get; set; }


}

public class kpi_u2_detail
{

    public int u2_detail_idx { get; set; }
    public int u0_detail_idx { get; set; }
    public int u1_detail_idx { get; set; }
    public string target { get; set; }
    public string actual { get; set; }
    public string month { get; set; }
    public int month_idx { get; set; }
    public int u2_detail_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public string target_pre { get; set; }
    public string actual_pre { get; set; }
    public string month_pre { get; set; }
    public int condition { get; set; }



}