﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_blacklist
/// </summary>
/// 
[Serializable]
[XmlRoot("data_blacklist")]
public class data_blacklist
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public blacklist_m0_detail[] blacklist_m0_list { get; set; }

    public blacklist_u0_detail[] blacklist_u0_list { get; set; }

    public blacklist_u1_detail[] blacklist_u1_list { get; set; }

    public blacklist_log_detail[] blacklist_log_list { get; set; }

    public blacklist_log_view[] blacklist_log_listview { get; set; }

    public Mblacklist[] blacklist_m0 { get; set; }
}

public class Mblacklist
{
    public int Blacklist_idx { get; set; }

    public int condition { get; set; }

    public string Blacklist_detail { get; set; }

    public string Blacklist_status { get; set; }
}

public class blacklist_log_view
{

    public int u0_idx { get; set; }

    public string PrefixnameTH { get; set; }

    public string FirstnameTH { get; set; }

    public string LastnameTH { get; set; }

    public string create_date { get; set; }

    public string time_create { get; set; }

    public string update_date { get; set; }

    public string time_update { get; set; }

    public string emp_name_th { get; set; }

    public string Blacklist_detail { get; set; }

    public string log_status { get; set; }

}

public class blacklist_log_detail
{   

    public int u0_idx { get; set; }

    public int u1_idx { get; set; }

    public int cemp_idx { get; set; }

    public int status { get; set; }
}

public class blacklist_u1_detail
{
    public int U1_IDX { get; set; }

    public int U0_IDX { get; set; }

    public int BlacklistIDX { get; set; }

    public string Note { get; set; }

    public int cemp_idx { get; set; }

    public string Blacklist_detail { get; set; }

    public int M0_blacklistidx { get; set; }

    public string M0_Blacklistdetail { get; set; }

    public int U1_BlacklistIDX { get; set; }

    public int condition { get; set; }

    public string U1_note { get; set; }

    public int status { get; set; }

    public int U1_IDX1 { get; set; }

}


public class blacklist_m0_detail
{
    public int Blacklist_idx { get; set; }

    public int condition { get; set; }

    public string Blacklist_detail { get; set; }

    public int Blacklist_status { get; set; }

    public int Cemp_IDX { get; set; }

    public string Create_date { get; set; }

    public string Update_date { get; set; }

    public int M0_IDX { get; set; }
}

public class blacklist_u0_detail
{
    public int U0_idx { get; set; }

    public int condition { get; set; }

    public string Idcard { get; set; }

    public string Passpost { get; set; }

    public int PrefixIDXTH { get; set; }

    public string FirstnameTH { get; set; }

    public string LastnameTH { get; set; }

    public int PrefixIDXEN { get; set; }

    public string FirstnameEN { get; set; }

    public string LastnameEN { get; set; }

    public int NatIDX { get; set; }

    public int RaceIDX { get; set; }

    public string U0_note { get; set; }

    public string create_date { get; set; }

    public string update_date { get; set; }

    public int cemp_idx { get; set; }

    public int U0_status { get; set; }

    public string PrefixnameTH { get; set; }

    public string PrefixnameEN { get; set; }

    public string RaceName { get; set; }

    public string NatName { get; set; }

    public string u1_note { get; set; }

    public string Blacklist_detail { get; set; }

    public int u1BlacklistIDX { get; set; }

}