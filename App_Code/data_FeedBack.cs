using System;
using System.Xml.Serialization;

//[Serializable]
//[XmlRoot("data_FeedBack")]
public class data_FeedBack
{
    public int return_code { get; set; }
    public string return_msg { get; set; }
    public M0_FeedBack_1_detail[] M0_FeedBack_1_list { get; set; }
    public M0_System_detail[] M0_System_list { get; set; }
    public M0_FeedBack_2_detail[] M0_FeedBack_2_list { get; set; }
    public M0_FeedBack_3_detail[] M0_FeedBack_3_list { get; set; }
    public U_FeedBack_detail[] U_FeedBack_list { get; set; }

    public T0_FeedBack_1_detail[] T0_FeedBack_1_list { get; set; }
    public T0_FeedBack_2_detail[] T0_FeedBack_2_list { get; set; }
    public T0_FeedBack_3_detail[] T0_FeedBack_3_list { get; set; }
}

public class M0_FeedBack_1_detail
{
    public int MFBIDX1 { get; set; }
    public int SysIDX { get; set; }
    public string QuestionSet { get; set; }
    public int MFB1Status { get; set; }
    public int CEmpIDX { get; set; }

}
public class M0_System_detail
{
    public int SysIDX { get; set; }
    public string SysNameTH { get; set; }
    public string SysNameTEN{ get; set; }
}
public class M0_FeedBack_2_detail
{
    public int MFBIDX2 { get; set; }
    public string Topic { get; set; }
    public int MFB2Status { get; set; }
    public int CEmpIDX { get; set; }
}
public class M0_FeedBack_3_detail
{
    public int MFBIDX3 { get; set; }
    public string Question { get; set; }
    public int MFB3Status { get; set; }
    public int CEmpIDX { get; set; }
}

public class U_FeedBack_detail
{
    public int UIDX { get; set; }
    public int MFBIDX { get; set; }
    public int MFBIDX1 { get; set; }
    public int MFBIDX2 { get; set; }
    public int MFBIDX3 { get; set; }
    public int MFBStatus { get; set; }
    public int CEmpIDX { get; set; }

    //Lookup
    public string QuestionSet { get; set; }
    public string Topic { get; set; }
    public string Question { get; set; }

}

public class T0_FeedBack_1_detail
{
    public int TFBIDX1 { get; set; }
    public int SysIDX { get; set; }
    public string QuestionSet { get; set; }
    public int TFB1Status { get; set; }
    public int CEmpIDX { get; set; }

}
public class T0_FeedBack_2_detail
{
    public int TFBIDX2 { get; set; }
    public string Topic { get; set; }
    public int TFB2Status { get; set; }
    public int CEmpIDX { get; set; }
}
public class T0_FeedBack_3_detail
{
    public int TFBIDX3 { get; set; }
    public string Question { get; set; }
    public int TFB3Status { get; set; }
    public int CEmpIDX { get; set; }
}