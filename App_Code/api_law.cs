﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.Configuration;

/// <summary>
/// Summary description for api_roombooking
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_law : System.Web.Services.WebService
{


    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_law _data_law = new data_law();
    service_mail _serviceMail = new service_mail();

    string _local_xml = "";
    string _mail_subject = "";
    string _mail_body = "";

    string email_cemp = "";
    string email_hr = "";
    string email_admin = "";
    string email_admin_test = "";

    string email_hrtest = "";
    string email_hr_check = "";

    string email_cemptest = "";
    string email_headtest = "";

    string email_mglawtest = "";
    string email_mglaw = "";

    string email_lawofficertest = "";
    string email_lawofficer = "";

    string email_headusertest = "";
    string email_headuser = "";

    string email_approvetest = "";
    string email_approve = "";

    //email_approvetest = "teoy_42@hotmail.com";
    //email_approve = _data_law.tcm_u0_document_list[0].emp_email_approve.ToString(); //ของจริง


    string email_head = "";

    string email_emp = "";
    string email_emptest = "";

    string email_admintest = "";

    string link_system = "http://localhost/mas.taokaenoi.co.th/laws-trademarks-contract";
    string link_system_promise = "http://localhost/mas.taokaenoi.co.th/laws-trademarks-permise";

    public api_law()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    // set m0 doctype
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetLawM0DocType(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 110); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set m0 subdoctype
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetLawM0SubDocType(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 111); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set create document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetLawCreateDocument(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_law _data_law = new data_law();
            _data_law = (data_law)_funcTool.convertXmlToObject(typeof(data_law), _xml_in);


            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 120); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_law = (data_law)_funcTool.convertXmlToObject(typeof(data_law), _local_xml);

            if (_data_law.return_code == 0) //create law
            {
                //check e-mail null
                if (_data_law.tcm_u0_document_list[0].emp_email != null && _data_law.tcm_u0_document_list[0].emp_email != "-")
                {
                    email_cemptest = "waraporn.teoy@gmail.com";
                    email_cemp = _data_law.tcm_u0_document_list[0].emp_email.ToString(); //ของจริง
                }

                if (_data_law.tcm_u0_document_list[0].emp_email_headuser != null && _data_law.tcm_u0_document_list[0].emp_email_headuser != "-")
                {
                    email_headtest = "teoy_42@hotmail.com";
                    email_head = _data_law.tcm_u0_document_list[0].emp_email_headuser.ToString(); //ของจริง
                }


                email_hr = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ hr
                //email_hr = "hr_admin_center@taokaenoi.co.th,nuchanart.ar@taokaenoi.co.th"; //ใช้ทดสอบ 

                string email = email_hr + "," + email_cemptest + "," + email_headtest; //ใช้ทดสอบ 
                //string email = email_cemp + "," + email_head + "," + email_hr; // ของจริง
                tcm_u0_document_detail _temp_u0 = _data_law.tcm_u0_document_list[0];
                //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการจองห้องประชุม";
                _mail_subject = "[Law : Trademarks] - สร้างใบคำขอ" + " " + _data_law.tcm_u0_document_list[0].docrequest_code.ToString() + "|" + email_cemp.ToString() + "|" + email_head.ToString();
                _mail_body = _serviceMail.SentMailCreateLawTMContract(_data_law.tcm_u0_document_list[0], link_system);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);

            }


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set approve document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetLawApproveDocument(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_law _data_law = new data_law();
            _data_law = (data_law)_funcTool.convertXmlToObject(typeof(data_law), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 121); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_law = (data_law)_funcTool.convertXmlToObject(typeof(data_law), _local_xml);

            if (_data_law.return_code == 0) //create law
            {
                //check e-mail null
                if (_data_law.tcm_u0_document_list[0].emp_email != null && _data_law.tcm_u0_document_list[0].emp_email != "-")
                {
                    email_cemptest = "waraporn.teoy@gmail.com";
                    email_cemp = _data_law.tcm_u0_document_list[0].emp_email.ToString(); //ของจริง
                }

                if (_data_law.tcm_u0_document_list[0].emp_email_approve != null && _data_law.tcm_u0_document_list[0].emp_email_approve != "-")
                {
                    email_approvetest = "teoy_42@hotmail.com";
                    email_approve = _data_law.tcm_u0_document_list[0].emp_email_approve.ToString(); //ของจริง
                }

                if (_data_law.tcm_u0_document_list[0].emp_email_mglaw != null && _data_law.tcm_u0_document_list[0].emp_email_mglaw != "-")
                {
                    email_mglawtest = "teoy_42@hotmail.com";
                    email_mglaw = _data_law.tcm_u0_document_list[0].emp_email_mglaw.ToString(); //ของจริง
                }

                if (_data_law.tcm_u0_document_list[0].emp_email_lawofficer != null && _data_law.tcm_u0_document_list[0].emp_email_lawofficer != "-")
                {
                    //string email_lawofficertest = "";
                    //string email_lawofficer = "";
                    email_lawofficertest = "teoy_42@hotmail.com";
                    email_lawofficer = _data_law.tcm_u0_document_list[0].emp_email_lawofficer.ToString(); //ของจริง
                }

                if (_data_law.tcm_u0_document_list[0].emp_email_headuser != null && _data_law.tcm_u0_document_list[0].emp_email_headuser != "-")
                {
                   
                    email_headusertest = "teoy_42@hotmail.com";
                    email_headuser = _data_law.tcm_u0_document_list[0].emp_email_headuser.ToString(); //ของจริง
                }


                //email_hr = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ hr
                //email_hr = "hr_admin_center@taokaenoi.co.th,nuchanart.ar@taokaenoi.co.th"; //ใช้ทดสอบ 

                string email = email_cemptest + "," + email_headtest + "," + email_mglawtest + "," + email_lawofficertest; //ใช้ทดสอบ 
                //string email = email_cemp + "," + email_head + "," + email_hr; // ของจริง
                tcm_u0_document_detail _temp_u0 = _data_law.tcm_u0_document_list[0];
                //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการจองห้องประชุม";
                _mail_subject = "[Law : Trademarks] - Document Detail" + " " + _data_law.tcm_u0_document_list[0].docrequest_code.ToString() + "|" + email_cemp.ToString() + "|" + email_headuser.ToString() + "|" + email_mglaw.ToString() + "|" + email_lawofficer.ToString() + "|" + email_approve.ToString();
                _mail_body = _serviceMail.SentMailApproveLawTMContract(_data_law.tcm_u0_document_list[0], link_system);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);

            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set update document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetLawUpdateDocument(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 122); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set insert record sheet
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetLawRecordSheet(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 130); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set insert publication record sheet
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetLawRecordSheetPublication(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 131); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set insert deadline record sheet
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetLawRecordSheetDeadline(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 132); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set insert document promise
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetDocumentPromise(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_law _data_law = new data_law();
            _data_law = (data_law)_funcTool.convertXmlToObject(typeof(data_law), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 140); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_law = (data_law)_funcTool.convertXmlToObject(typeof(data_law), _local_xml);

            if (_data_law.return_code == 0) //create law
            {
                //check e-mail null
                if (_data_law.pm_u0_document_detail_list[0].emp_email != null && _data_law.pm_u0_document_detail_list[0].emp_email != "-")
                {
                    email_cemptest = "waraporn.teoy@gmail.com";
                    email_cemp = _data_law.pm_u0_document_detail_list[0].emp_email.ToString(); //ของจริง
                }

                if (_data_law.pm_u0_document_detail_list[0].emp_email_headuser != null && _data_law.pm_u0_document_detail_list[0].emp_email_headuser != "-")
                {
                    email_approvetest = "teoy_42@hotmail.com";
                    email_headuser = _data_law.pm_u0_document_detail_list[0].emp_email_headuser.ToString(); //ของจริง
                }

                string email = email_cemptest + "," + email_approvetest; //ใช้ทดสอบ 

                //string email = email_cemp + "," + email_headuser; // ของจริง

                pm_u0_document_detail _temp_u0 = _data_law.pm_u0_document_detail_list[0];
             
                _mail_subject = "[Law : Promise] - สร้างรายการ" + " " + _data_law.pm_u0_document_detail_list[0].document_code.ToString() + "|" + email_cemp.ToString() + "|" + email_headuser.ToString();
                _mail_body = _serviceMail.SentMailCreateLawTMPromise(_data_law.pm_u0_document_detail_list[0], link_system_promise);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);

            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set approve document promise
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetApproveDocumentPromise(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_law _data_law = new data_law();
            _data_law = (data_law)_funcTool.convertXmlToObject(typeof(data_law), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 141); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_law = (data_law)_funcTool.convertXmlToObject(typeof(data_law), _local_xml);

            if (_data_law.return_code == 0) //create law
            {
                //check e-mail null
                if (_data_law.pm_u0_document_detail_list[0].emp_email != null && _data_law.pm_u0_document_detail_list[0].emp_email != "-")
                {
                    email_cemptest = "waraporn.teoy@gmail.com";
                    email_cemp = _data_law.pm_u0_document_detail_list[0].emp_email.ToString(); //ของจริง
                }

                //check e-mail null
                if (_data_law.pm_u0_document_detail_list[0].emp_email_approve != null && _data_law.pm_u0_document_detail_list[0].emp_email_approve != "-")
                {
                    email_headtest = "waraporn.teoy@gmail.com";
                    email_approve = _data_law.pm_u0_document_detail_list[0].emp_email_approve.ToString(); //ของจริง
                }

                //check e-mail null
                if (_data_law.pm_u0_document_detail_list[0].emp_email_mglaw != null && _data_law.pm_u0_document_detail_list[0].emp_email_mglaw != "-")
                {
                    email_mglawtest = "waraporn.teoy@gmail.com";
                    email_mglaw = _data_law.pm_u0_document_detail_list[0].emp_email_mglaw.ToString(); //ของจริง
                }


                string email = email_cemptest + "," + email_headtest + "," + email_mglawtest + "," + email_lawofficertest; //ใช้ทดสอบ 
                //string email = email_cemp + "," + email_approve + "," + email_mglaw; // ของจริง


                pm_u0_document_detail _temp_u0 = _data_law.pm_u0_document_detail_list[0];
                //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการจองห้องประชุม";
                _mail_subject = "[Law : Promise] - พิจารณารายการ" + " " + _data_law.pm_u0_document_detail_list[0].document_code.ToString() + "|" + email_cemp.ToString() + "|" + email_approve.ToString() + "|" + email_mglaw.ToString() + "|" + email_lawofficer.ToString() + "|" + email_approve.ToString();
                _mail_body = _serviceMail.SentMailApproveLawTMPromise(_data_law.pm_u0_document_detail_list[0], link_system_promise);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);

            }




        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get m0law contry
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawContryDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 210); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get select m0 doctype
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawM0DocType(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 211); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get select m0 sub doctype
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawM0SubDocType(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 212); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get select m0 jobtype
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawM0JobType(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 213); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get select m0 owner
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawM0Owner(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 214); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get select m0 Incorporated in
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawM0Incorporated(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 215); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get select m0 Trademark
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawM0Trademark(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 216); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get select m0 record status
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawM0RecordStatus(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 217); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get select m0 local language
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawM0LocalLanguage(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 218); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get select m0 actions
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawM0Actions(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 219); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // get select detail document trademarks
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawDetailDocument(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 220); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get select log detail document trademarks
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawLogDetailDocument(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 221); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get select wait approve document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawWaitApproveDetailDoc(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 222); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get select view detail law officer in document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetViewDetailLawOfficer(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 223); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get count wait approve
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCountLawWaitApprove(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 224); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get search detail contract
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchDetailContract(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 225); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get status approve
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawStatusApprove(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 230); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get name law to document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSelectLawInDocument(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 231); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get master Type Of Registration
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawM0TypeOfRegistration(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 240); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get master Type Of Work
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawM0TypeOfWork(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 241); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get m0 action detail list
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawM0ActionkDetailList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 242); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get m0 Type Promise
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawPMtypepromise(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 250); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get m0 SubType Promise
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawPMSubtypepromise(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 251); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get u0 docdetail pm promise
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawPMDocDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 260); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get u0 view docdetail pm promise
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawPMViewDocDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 261); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get u0 Log view docdetail pm promise
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawPMLogViewDocDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 262); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get wait approve pm promise
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCountLawPMWaitApprove(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 263); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get wait approve detail pm promise
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawPMWaitApproveDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 264); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get law officer detail pm promise
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawPMLawOfficer(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 265); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get promise file document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawPMFileDocument(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 266); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get law document record sheet
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawRecordSheet(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 310); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get law document view record sheet
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawViewRecordSheet(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 311); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get law document view deadline record sheet
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLawViewDeadlineRecordSheet(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 312); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get ddl trademarks profile create recordsheet
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDDLTrademarksProfile(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 411); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get detail to trademark profile
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailTrademarksProfile(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 412); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get search detail to trademark profile
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchDetailTrademark(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 413); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set delete m0 doctype
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetDelLawM0DocType(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 910); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set delete m0 subdoctype
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetDelLawM0SubDocType(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 911); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set update record sheet renewal
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdateLawRenewal(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 511); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set update record sheet assignment
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdateLawAssignment(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 512); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    
    // set update record sheet changeofname
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdateLawChangeofname(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 513); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set update record sheet priorities
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdateLawPriorities(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 514); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set update record sheet Client
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdateLawClient(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 515); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set update record sheet Agent
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdateLawAgent(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 516); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set update record sheet Goods
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdateLawGoods(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 517); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set update record sheet Deadline
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdateLawDeadline(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("lawConn", "data_law", "service_law_trademarks", _xml_in, 518); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

}
