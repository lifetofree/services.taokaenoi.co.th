﻿using System;
using System.Xml.Serialization;
/// <summary>
/// Summary description for data_roombooking
/// </summary>
/// 

[Serializable]
[XmlRoot("data_carbooking")]
public class data_carbooking
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public cbk_m0_place_detail[] cbk_m0_place_list { get; set; }
    public cbk_m0_expenses_detail[] cbk_m0_expenses_list { get; set; }
    public cbk_m0_typecar_detail[] cbk_m0_typecar_list { get; set; }
    public cbk_m1_typecar_detail[] cbk_m1_typecar_list { get; set; }
    public cbk_m0_province_detail[] cbk_m0_province_list { get; set; }
    public cbk_m0_admin_detail[] cbk_m0_admin_list { get; set; }
    public cbk_m0_costcenter_detail[] cbk_m0_costcenter_list { get; set; }
    public cbk_m0_type_detail[] cbk_m0_type_list { get; set; }
    public cbk_m0_brandcar_detail[] cbk_m0_brandcar_list { get; set; }
    public cbk_m0_enginecar_detail[] cbk_m0_enginecar_list { get; set; }
    public cbk_m0_fuelcar_detail[] cbk_m0_fuelcar_list { get; set; }
    public cbk_m0_car_detail[] cbk_m0_car_list { get; set; }
    public cbk_m1_car_detail[] cbk_m1_car_list { get; set; }
    public cbk_m0_topic_ma_detail[] cbk_m0_topic_ma_list { get; set; }
    public cbk_searchreport_macar_detail[] cbk_searchreport_macar_list { get; set; }
    public cbk_m0_usecar_outplan_detail[] cbk_m0_usecar_outplan_list { get; set; }

    public cbk_type_booking_detail[] cbk_type_booking_list { get; set; }
    public cbk_type_travel_detail[] cbk_type_travel_list { get; set; }
    public cbk_m0_car_use_detail[] cbk_m0_car_use_list { get; set; }
    public cbk_m0_document_status_detail[] cbk_m0_document_status_list { get; set; }
    public cbk_u0_document_ma_detail[] cbk_u0_document_ma_list { get; set; }
    public cbk_u1_document_ma_detail[] cbk_u1_document_ma_list { get; set; }
    public cbk_u2_document_ma_detail[] cbk_u2_document_ma_list { get; set; }


    //-- document --//
    [XmlElement("cbk_u0_document_list")]
    public cbk_u0_document_detail[] cbk_u0_document_list { get; set; }

    [XmlElement("cbk_u1_document_list")]
    public cbk_u1_document_detail[] cbk_u1_document_list { get; set; }

    [XmlElement("cbk_u2_document_list")]
    public cbk_u2_document_detail[] cbk_u2_document_list { get; set; }

    [XmlElement("cbk_u3_document_list")]
    public cbk_u3_document_detail[] cbk_u3_document_list { get; set; }

    [XmlElement("cbk_u4_document_list")]
    public cbk_u4_document_detail[] cbk_u4_document_list { get; set; }

    [XmlElement("cbk_search_car_list")]
    public cbk_search_car_detail[] cbk_search_car_list { get; set; }

    [XmlElement("cbk_searchreport_car_list")]
    public cbk_searchreport_car_detail[] cbk_searchreport_car_list { get; set; }
    //-- document --//

}


public class cbk_m0_usecar_outplan_detail
{

    public int u0_document_idx { get; set; }
    public int usecar_outplan_idx { get; set; }
    public string usecar_outplan_name { get; set; }


}

public class cbk_searchreport_macar_detail
{

    public int u0_document_idx { get; set; }
    public int u1_document_idx { get; set; }
    public string u0_document_code { get; set; }
    public string date_start { get; set; }
    public string time_start { get; set; }
    public string date_end { get; set; }
    public string time_end { get; set; }
    public string time_create_date { get; set; }
    public int detailtype_car_idx { get; set; }
    public string count_travel { get; set; }
    public int type_booking_idx { get; set; }
    public int type_travel_idx { get; set; }
    public string objective_booking { get; set; }
    public string place_name_other { get; set; }
    public int place_idx { get; set; }
    public string distance { get; set; }
    public string time_length { get; set; }
    public string note_booking { get; set; }
    public int cemp_idx { get; set; }
    public int cemp_org_idx { get; set; }
    public int cemp_rdept_idx { get; set; }
    public int cemp_rsec_idx { get; set; }
    public int cemp_rpos_idx { get; set; }
    public int head_emp_idx { get; set; }
    public int head_org_idx { get; set; }
    public int head_rdept_idx { get; set; }
    public int head_rsec_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int decision { get; set; }
    public int staidx { get; set; }
    public string comment { get; set; }
    public string status_name { get; set; }
    public string actor_name { get; set; }
    public string node_name { get; set; }
    public string emp_name_en { get; set; }
    public string emp_name_th { get; set; }
    public string org_name_en { get; set; }
    public string org_name_th { get; set; }
    public string dept_name_en { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_en { get; set; }
    public string sec_name_th { get; set; }
    public int rpos_idx { get; set; }
    public string pos_name_th { get; set; }
    public string place_name { get; set; }
    public string emp_code { get; set; }
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int costcenter_idx { get; set; }
    public string costcenter_no { get; set; }
    public string emp_email { get; set; }
    public string detailtype_car_name { get; set; }
    public string type_booking_name { get; set; }
    public string type_travel_name { get; set; }
    public int cemp_joblevel { get; set; }
    public int count_waitApprove { get; set; }
    public string decision_name { get; set; }
    public int decision_idx { get; set; }
    public int m0_car_idx { get; set; }
    public string car_register { get; set; }
    public int car_use_status { get; set; }
    public int car_use_idx { get; set; }
    public string date_time_chekbutton { get; set; }
    public string decision_approve { get; set; }
    public string emp_email_sentto_headuser { get; set; }
    public string car_use_name { get; set; }
    public int condition { get; set; }
    public int IFSearchbetween { get; set; }
    public int count_carbooking { get; set; }
    public int sum_expenses { get; set; }
    public int count_booking { get; set; }
    public string DC_YEAR { get; set; }
    public string DC_Mount { get; set; }
    public string sum_expenses_decimal { get; set; }
    public int type_car_idx { get; set; }
    public int u0_document_ma_idx { get; set; }
    public string miles_start { get; set; }
    public string miles_current { get; set; }
    public string company_detail { get; set; }
    public string note_detail { get; set; }
    public string pr_code { get; set; }
    public string po_code { get; set; }
    public string date_start_repair { get; set; }
    public string date_end_repair { get; set; }
    public string price_total_repair { get; set; }
    public string discount { get; set; }
    public string vat { get; set; }
    public string price_net { get; set; }
    public string ProvName { get; set; }
    public string car_look { get; set; }
    public string emp_name_en_driver { get; set; }
    public string emp_name_th_driver { get; set; }
    public string org_name_th_driver { get; set; }
    public string dept_name_th_driver { get; set; }
    public string sec_name_th_driver { get; set; }
    public string pos_name_th_driver { get; set; }
    public int costcenter_idx_driver { get; set; }
    public string costcenter_no_driver { get; set; }
    public int emp_idx { get; set; }
    public string sum_price_total_repair_decimal { get; set; }
    public string sum_vat_decimal { get; set; }
    public string sum_price_net_decimal { get; set; }


}

public class cbk_u0_document_ma_detail
{

    public int u0_document_ma_idx { get; set; }
    public int m0_car_idx { get; set; }
    public string miles_start { get; set; }
    public string miles_current { get; set; }
    public string company_detail { get; set; }
    public string note_detail { get; set; }
    public string pr_code { get; set; }
    public string po_code { get; set; }
    public string date_start_repair { get; set; }
    public string date_end_repair { get; set; }
    public string price_total_repair { get; set; }
    public string discount { get; set; }
    public string vat { get; set; }
    public string price_net { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int staidx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int condition { get; set; }
    public int decision { get; set; }
    public string costcenter_no { get; set; }
    public string car_register { get; set; }
    public string ProvName { get; set; }
    public string car_look { get; set; }
    public string emp_name_en_driver { get; set; }
    public string emp_name_th_driver { get; set; }
    public string org_name_th_driver { get; set; }
    public string dept_name_th_driver { get; set; }
    public string sec_name_th_driver { get; set; }
    public string pos_name_th_driver { get; set; }
    public string emp_name_th { get; set; }
    public string org_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_th { get; set; }
    public string pos_name_th { get; set; }
    public string emp_email { get; set; }
    public string time_create_date { get; set; }
    public string status_name { get; set; }
    public string node_name { get; set; }
    public string actor_name { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int costcenter_idx_driver { get; set; }
    public string costcenter_no_driver { get; set; }
    public int costcenter_idx { get; set; }
    public string emp_code { get; set; }
    public int emp_idx { get; set; }
    public string sum_price_total_repair_decimal { get; set; }
    public string sum_vat_decimal { get; set; }
    public string sum_price_net_decimal { get; set; }

}

public class cbk_u1_document_ma_detail
{

    public int u0_document_ma_idx { get; set; }
    public int u1_document_ma_idx { get; set; }
    public int u0_node_idx { get; set; }
    public string miles_start { get; set; }
    public int m0_car_idx { get; set; }
    public int decision_idx { get; set; }
    public string comment { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int staidx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int condition { get; set; }
    public string time_create { get; set; }
    public string current_decision { get; set; }
    public string current_artor { get; set; }
    public string emp_name_th { get; set; }

}

public class cbk_u2_document_ma_detail
{

    public int u0_document_ma_idx { get; set; }
    public int u2_document_ma_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int condition { get; set; }
    public int topic_ma_idx { get; set; }
    public string quantity { get; set; }
    public string price_unit { get; set; }
    public string price_total { get; set; }
    public string topic_ma_name { get; set; }
    public string sum_price_total_decimal { get; set; }
    public string sum_price_unit_decimal { get; set; }
}

public class cbk_m0_topic_ma_detail
{
    
    public int topic_ma_idx { get; set; }
    public string topic_ma_name { get; set; }
    public string detail_ma_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int topic_ma_status { get; set; }
    public int condition { get; set; }

}

public class cbk_m0_document_status_detail
{

    public int staidx { get; set; }
    public string status_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int status { get; set; }
    public int condition { get; set; }

}

public class cbk_m0_car_use_detail
{

    public int car_use_idx { get; set; }
    public string car_use_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int car_use_status { get; set; }
    public int condition { get; set; }

}

public class cbk_type_booking_detail
{

    public int type_booking_idx { get; set; }
    public string type_booking_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int type_booking_status { get; set; }
    public int condition { get; set; }

}

public class cbk_type_travel_detail
{

    public int type_booking_idx { get; set; }
    public int type_travel_idx { get; set; }
    public string type_travel_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int type_travel_status { get; set; }
    public int condition { get; set; }

}

public class cbk_m0_place_detail
{
    public int place_idx { get; set; }
    public string place_name { get; set; }
    public string place_code { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int place_status { get; set; }
    public int condition { get; set; }
}

public class cbk_m0_expenses_detail
{
    
    public int expenses_idx { get; set; }
    public string expenses_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int expenses_status { get; set; }
    public int condition { get; set; }

}

public class cbk_m0_typecar_detail
{

    public int type_car_idx { get; set; }
    public string type_car_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int type_car_status { get; set; }
    public int condition { get; set; }

}

public class cbk_m1_typecar_detail
{

    public int type_car_idx { get; set; }
    public string type_car_name { get; set; }
    public int detailtype_car_idx { get; set; }
    public string detailtype_car_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int detailtype_car_status { get; set; }
    public int condition { get; set; }

}

public class cbk_m0_province_detail
{

    public int ProvIDX { get; set; }
    public string ProvName { get; set; }
    public int ProvStatus { get; set; }
    public int condition { get; set; }

}

public class cbk_m0_admin_detail
{

    public int emp_idx { get; set; }
    public string emp_code { get; set; }
    public int rpos_idx { get; set; }
    public int rsec_idx { get; set; }
    public int rdept_idx { get; set; }
    public int org_idx { get; set; }
    public int costcenter_idx { get; set; }
    public int emp_status { get; set; }
    public string emp_name_th { get; set; }


}

public class cbk_m0_costcenter_detail
{

    public int CostIDX { get; set; }
    public string CostNo { get; set; }
    public string CostName { get; set; }
    public int CostStatus { get; set; }
    public int condition { get; set; }

}

public class cbk_m0_type_detail
{

    public int type_idx { get; set; }
    public string type_name { get; set; }
    public int type_status { get; set; }
    public int condition { get; set; }

}

public class cbk_m0_brandcar_detail
{

    public int brand_car_idx { get; set; }
    public string brand_car_name { get; set; }
    public int brand_car_status { get; set; }
    public int condition { get; set; }

}

public class cbk_m0_enginecar_detail
{

    public int engine_brand_idx { get; set; }
    public string engine_brand_name { get; set; }
    public int engine_brand_status { get; set; }
    public int condition { get; set; }

}

public class cbk_m0_fuelcar_detail
{

    public int fuel_idx { get; set; }
    public string fuel_name { get; set; }
    public int fuel_status { get; set; }
    public int condition { get; set; }

}

[Serializable]
public class cbk_m0_car_detail
{
    [XmlElement("m0_car_idx")]
    public int m0_car_idx { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("detailtype_car_idx")]
    public int detailtype_car_idx { get; set; }

    [XmlElement("car_province_idx")]
    public int car_province_idx { get; set; }

    [XmlElement("car_register")]
    public string car_register { get; set; }

    [XmlElement("admin_idx")]
    public int admin_idx { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }

    [XmlElement("asset_number")]
    public string asset_number { get; set; }

    [XmlElement("car_look")]
    public string car_look { get; set; }

    [XmlElement("brand_car_idx")]
    public int brand_car_idx { get; set; }

    [XmlElement("type_car_idx")]
    public int type_car_idx { get; set; }

    [XmlElement("car_model")]
    public string car_model { get; set; }

    [XmlElement("car_genaration")]
    public string car_genaration { get; set; }

    [XmlElement("car_color")]
    public string car_color { get; set; }

    [XmlElement("car_number")]
    public string car_number { get; set; }

    [XmlElement("car_number_located")]
    public string car_number_located { get; set; }

    [XmlElement("engine_brand_idx")]
    public int engine_brand_idx { get; set; }

    [XmlElement("engine_number")]
    public string engine_number { get; set; }

    [XmlElement("engine_number_located")]
    public string engine_number_located { get; set; }

    [XmlElement("fuel_idx")]
    public int fuel_idx { get; set; }

    [XmlElement("gas_cylinder")]
    public string gas_cylinder { get; set; }

    [XmlElement("count_pump")]
    public int count_pump { get; set; }

    [XmlElement("cc")]
    public int cc { get; set; }

    [XmlElement("horse_power")]
    public int horse_power { get; set; }

    [XmlElement("car_weight")]
    public int car_weight { get; set; }

    [XmlElement("car_load")]
    public int car_load { get; set; }

    [XmlElement("total_weight")]
    public int total_weight { get; set; }

    [XmlElement("count_seat")]
    public int count_seat { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_car_status")]
    public int m0_car_status { get; set; }

    [XmlElement("m0_car_status_name")]
    public string m0_car_status_name { get; set; }

    [XmlElement("m1_car_status_name")]
    public string m1_car_status_name { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("tax_deadline")]
    public string tax_deadline { get; set; }

    [XmlElement("price_tax")]
    public string price_tax { get; set; }

    [XmlElement("date_insurance_term")]
    public string date_insurance_term { get; set; }

    [XmlElement("date_car_insurance_deadline")]
    public string date_car_insurance_deadline { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("ProvName")]
    public string ProvName { get; set; }

    [XmlElement("admin_emp_name_en")]
    public string admin_emp_name_en { get; set; }

    [XmlElement("admin_emp_name_th")]
    public string admin_emp_name_th { get; set; }

    [XmlElement("admin_org_name_en")]
    public string admin_org_name_en { get; set; }

    [XmlElement("admin_org_name_th")]
    public string admin_org_name_th { get; set; }

    [XmlElement("admin_dept_name_th")]
    public string admin_dept_name_th { get; set; }

    [XmlElement("admin_sec_name_th")]
    public string admin_sec_name_th { get; set; }

    [XmlElement("admin_pos_name_th")]
    public string admin_pos_name_th { get; set; }

    [XmlElement("CostNo")]
    public string CostNo { get; set; }

    [XmlElement("brand_car_name")]
    public string brand_car_name { get; set; }

    [XmlElement("type_car_name")]
    public string type_car_name { get; set; }

    [XmlElement("detailtype_car_name")]
    public string detailtype_car_name { get; set; }

    [XmlElement("engine_brand_name")]
    public string engine_brand_name { get; set; }

    [XmlElement("fuel_name")]
    public string fuel_name { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("admin_rdept_idx")]
    public int admin_rdept_idx { get; set; }

    [XmlElement("admin_rsec_idx")]
    public int admin_rsec_idx { get; set; }

    [XmlElement("m1_car_idx")]
    public int m1_car_idx { get; set; }

    [XmlElement("emp_name_th_update")]
    public string emp_name_th_update { get; set; }

    [XmlElement("org_name_th_update")]
    public string org_name_th_update { get; set; }

    [XmlElement("dept_name_th_update")]
    public string dept_name_th_update { get; set; }

    [XmlElement("sec_name_th_update")]
    public string sec_name_th_update { get; set; }

    [XmlElement("pos_name_th_update")]
    public string pos_name_th_update { get; set; }

    [XmlElement("cemp_update")]
    public int cemp_update { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("time_update")]
    public string time_update { get; set; }



}

[Serializable]
public class cbk_m1_car_detail
{
    [XmlElement("m0_car_idx")]
    public int m0_car_idx { get; set; }

    [XmlElement("m1_car_idx")]
    public int m1_car_idx { get; set; }

    [XmlElement("tax_deadline")]
    public string tax_deadline { get; set; }

    [XmlElement("price_tax")]
    public int price_tax { get; set; }

    [XmlElement("date_insurance_term")]
    public string date_insurance_term { get; set; }

    [XmlElement("date_car_insurance_deadline")]
    public string date_car_insurance_deadline { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m1_car_status")]
    public int m1_car_status { get; set; }

}

[Serializable]
public class cbk_u0_document_detail
{
    [XmlElement("u0_document_idx")]
    public int u0_document_idx { get; set; }

    [XmlElement("u1_document_idx")]
    public int u1_document_idx { get; set; }

    [XmlElement("u0_document_code")]
    public string u0_document_code { get; set; }

    [XmlElement("date_start")]
    public string date_start { get; set; }

    [XmlElement("time_start")]
    public string time_start { get; set; }

    [XmlElement("date_end")]
    public string date_end { get; set; }

    [XmlElement("time_end")]
    public string time_end { get; set; }

    [XmlElement("detailtype_car_idx")]
    public int detailtype_car_idx { get; set; }

    [XmlElement("count_travel")]
    public string count_travel { get; set; }

    [XmlElement("type_booking_idx")]
    public int type_booking_idx { get; set; }

    [XmlElement("type_travel_idx")]
    public int type_travel_idx { get; set; }

    [XmlElement("objective_booking")]
    public string objective_booking { get; set; }

    [XmlElement("place_name_other")]
    public string place_name_other { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("distance")]
    public string distance { get; set; }

    [XmlElement("time_length")]
    public string time_length { get; set; }

    [XmlElement("note_booking")]
    public string note_booking { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("cemp_org_idx")]
    public int cemp_org_idx { get; set; }

    [XmlElement("cemp_rdept_idx")]
    public int cemp_rdept_idx { get; set; }

    [XmlElement("cemp_rsec_idx")]
    public int cemp_rsec_idx { get; set; }

    [XmlElement("head_emp_idx")]
    public int head_emp_idx { get; set; }

    [XmlElement("head_org_idx")]
    public int head_org_idx { get; set; }

    [XmlElement("head_rdept_idx")]
    public int head_rdept_idx { get; set; }

    [XmlElement("head_rsec_idx")]
    public int head_rsec_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("time_create_date")]
    public string time_create_date { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }

    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("detailtype_car_name")]
    public string detailtype_car_name { get; set; }

    [XmlElement("type_booking_name")]
    public string type_booking_name { get; set; }

    [XmlElement("type_travel_name")]
    public string type_travel_name { get; set; }

    [XmlElement("cemp_rpos_idx")]
    public int cemp_rpos_idx { get; set; }

    [XmlElement("cemp_joblevel")]
    public int cemp_joblevel { get; set; }

    [XmlElement("count_waitApprove")]
    public int count_waitApprove { get; set; }

    [XmlElement("decision_name")]
    public string decision_name { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("m0_car_idx")]
    public int m0_car_idx { get; set; }

    [XmlElement("car_register")]
    public string car_register { get; set; }

    [XmlElement("car_use_status")]
    public int car_use_status { get; set; }

    [XmlElement("car_use_idx")]
    public int car_use_idx { get; set; }

    [XmlElement("date_time_chekbutton")]
    public string date_time_chekbutton { get; set; }

    [XmlElement("decision_approve")]
    public string decision_approve { get; set; }

    [XmlElement("emp_email_sentto_headuser")]
    public string emp_email_sentto_headuser { get; set; }

    [XmlElement("car_use_name")]
    public string car_use_name { get; set; }

    [XmlElement("car_busy")]
    public string car_busy { get; set; }

    [XmlElement("usecar_outplan")]
    public int usecar_outplan { get; set; }

}

[Serializable]
public class cbk_u1_document_detail
{
    [XmlElement("u0_document_idx")]
    public int u0_document_idx { get; set; }

    [XmlElement("u1_document_idx")]
    public int u1_document_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("current_artor")]
    public string current_artor { get; set; }

    [XmlElement("current_decision")]
    public string current_decision { get; set; }

    [XmlElement("current_status")]
    public string current_status { get; set; }

    [XmlElement("current_status_name")]
    public string current_status_name { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("date_start")]
    public string date_start { get; set; }

    [XmlElement("date_end")]
    public string date_end { get; set; }

    [XmlElement("time_start")]
    public string time_start { get; set; }

    [XmlElement("time_end")]
    public string time_end { get; set; }

    [XmlElement("type_search")]
    public int type_search { get; set; }

    [XmlElement("m0_room_idx_check")]
    public string m0_room_idx_check { get; set; }

    [XmlElement("type_booking_idx")]
    public int type_booking_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("result_use_name")]
    public string result_use_name { get; set; }

    [XmlElement("count_waitApprove")]
    public int count_waitApprove { get; set; }

    [XmlElement("status_booking")]
    public string status_booking { get; set; }

    [XmlElement("date_time_chekbutton")]
    public string date_time_chekbutton { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_email_sentto_hr")]
    public string emp_email_sentto_hr { get; set; }

    [XmlElement("decision_name_hr")]
    public string decision_name_hr { get; set; }

    

}

[Serializable]
public class cbk_u2_document_detail
{
    [XmlElement("u0_document_idx")]
    public int u0_document_idx { get; set; }

    [XmlElement("u2_document_idx")]
    public int u2_document_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("car_use_idx")]
    public int car_use_idx { get; set; }

    [XmlElement("m0_car_idx")]
    public int m0_car_idx { get; set; }

    [XmlElement("emp_idx_driver")]
    public int emp_idx_driver { get; set; }

    [XmlElement("car_use_status")]
    public int car_use_status { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("detailtype_car_name")]
    public string detailtype_car_name { get; set; }

    [XmlElement("car_look")]
    public string car_look { get; set; }

    [XmlElement("car_register")]
    public string car_register { get; set; }

    [XmlElement("miles_start")]
    public int miles_start { get; set; }

    [XmlElement("miles_end")]
    public int miles_end { get; set; }

    [XmlElement("note_detail_caruse")]
    public string note_detail_caruse { get; set; }

    [XmlElement("emp_admin_idx")]
    public int emp_admin_idx { get; set; }

    [XmlElement("car_use_name")]
    public string car_use_name { get; set; }

    [XmlElement("usecar_outplan")]
    public int usecar_outplan { get; set; }



}

[Serializable]
public class cbk_u3_document_detail
{
    [XmlElement("u0_document_idx")]
    public int u0_document_idx { get; set; }

    [XmlElement("u3_document_idx")]
    public int u3_document_idx { get; set; }

    [XmlElement("u2_document_idx")]
    public int u2_document_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("car_use_idx")]
    public int car_use_idx { get; set; }

    [XmlElement("m0_car_idx")]
    public int m0_car_idx { get; set; }

    [XmlElement("emp_idx_driver")]
    public int emp_idx_driver { get; set; }

    [XmlElement("car_use_status")]
    public int car_use_status { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("detailtype_car_name")]
    public string detailtype_car_name { get; set; }

    [XmlElement("car_look")]
    public string car_look { get; set; }

    [XmlElement("car_register")]
    public string car_register { get; set; }

    [XmlElement("miles_start")]
    public int miles_start { get; set; }

    [XmlElement("miles_end")]
    public int miles_end { get; set; }

    [XmlElement("note_detail_caruse")]
    public string note_detail_caruse { get; set; }

    [XmlElement("emp_admin_idx")]
    public int emp_admin_idx { get; set; }

    [XmlElement("expenses_idx")]
    public int expenses_idx { get; set; }

    [XmlElement("count_expenses")]
    public int count_expenses { get; set; }

    [XmlElement("expenses_name")]
    public string expenses_name { get; set; }

    [XmlElement("count_expenses_decimal")]
    public string count_expenses_decimal { get; set; }

}

[Serializable]
public class cbk_u4_document_detail
{
    [XmlElement("u0_document_idx")]
    public int u0_document_idx { get; set; }

    [XmlElement("u3_document_idx")]
    public int u3_document_idx { get; set; }

    [XmlElement("u2_document_idx")]
    public int u2_document_idx { get; set; }

    [XmlElement("u4_document_idx")]
    public int u4_document_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("distance_outplan")]
    public string distance_outplan { get; set; }

    [XmlElement("expenses_outplan")]
    public string expenses_outplan { get; set; }

    [XmlElement("detail_outplan")]
    public string detail_outplan { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

}

[Serializable]
public class cbk_search_car_detail
{
    [XmlElement("u0_document_idx")]
    public int u0_document_idx { get; set; }

    [XmlElement("u3_document_idx")]
    public int u3_document_idx { get; set; }

    [XmlElement("u2_document_idx")]
    public int u2_document_idx { get; set; }

    [XmlElement("detailtype_car_idx")]
    public int detailtype_car_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("date_start")]
    public string date_start { get; set; }

    [XmlElement("date_end")]
    public string date_end { get; set; }

    [XmlElement("time_start")]
    public string time_start { get; set; }

    [XmlElement("time_end")]
    public string time_end { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("IFSearchbetween")]
    public int IFSearchbetween { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("cemp_rdept_idx")]
    public int cemp_rdept_idx { get; set; }

    [XmlElement("cemp_rsec_idx")]
    public int cemp_rsec_idx { get; set; }

    [XmlElement("cemp_rpos_idx")]
    public int cemp_rpos_idx { get; set; }

    [XmlElement("cemp_joblevel")]
    public int cemp_joblevel { get; set; }

    [XmlElement("car_use_idx")]
    public int car_use_idx { get; set; }

    [XmlElement("car_use_name")]
    public string car_use_name { get; set; }

    [XmlElement("m0_car_idx")]
    public int m0_car_idx { get; set; }

    [XmlElement("car_register")]
    public string car_register { get; set; }

}

[Serializable]
public class cbk_searchreport_car_detail
{
    [XmlElement("u0_document_idx")]
    public int u0_document_idx { get; set; }

    [XmlElement("u1_document_idx")]
    public int u1_document_idx { get; set; }

    [XmlElement("u0_document_code")]
    public string u0_document_code { get; set; }

    [XmlElement("date_start")]
    public string date_start { get; set; }

    [XmlElement("time_start")]
    public string time_start { get; set; }

    [XmlElement("date_end")]
    public string date_end { get; set; }

    [XmlElement("time_end")]
    public string time_end { get; set; }

    [XmlElement("time_create_date")]
    public string time_create_date { get; set; }

    [XmlElement("detailtype_car_idx")]
    public int detailtype_car_idx { get; set; }

    [XmlElement("count_travel")]
    public string count_travel { get; set; }

    [XmlElement("type_booking_idx")]
    public int type_booking_idx { get; set; }

    [XmlElement("type_travel_idx")]
    public int type_travel_idx { get; set; }

    [XmlElement("objective_booking")]
    public string objective_booking { get; set; }

    [XmlElement("place_name_other")]
    public string place_name_other { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("distance")]
    public string distance { get; set; }

    [XmlElement("time_length")]
    public string time_length { get; set; }

    [XmlElement("note_booking")]
    public string note_booking { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("cemp_org_idx")]
    public int cemp_org_idx { get; set; }

    [XmlElement("cemp_rdept_idx")]
    public int cemp_rdept_idx { get; set; }

    [XmlElement("cemp_rsec_idx")]
    public int cemp_rsec_idx { get; set; }

    [XmlElement("cemp_rpos_idx")]
    public int cemp_rpos_idx { get; set; }

    [XmlElement("head_emp_idx")]
    public int head_emp_idx { get; set; }

    [XmlElement("head_org_idx")]
    public int head_org_idx { get; set; }

    [XmlElement("head_rdept_idx")]
    public int head_rdept_idx { get; set; }

    [XmlElement("head_rsec_idx")]
    public int head_rsec_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }

    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("detailtype_car_name")]
    public string detailtype_car_name { get; set; }

    [XmlElement("type_booking_name")]
    public string type_booking_name { get; set; }

    [XmlElement("type_travel_name")]
    public string type_travel_name { get; set; }

    [XmlElement("cemp_joblevel")]
    public int cemp_joblevel { get; set; }

    [XmlElement("count_waitApprove")]
    public int count_waitApprove { get; set; }

    [XmlElement("decision_name")]
    public string decision_name { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("m0_car_idx")]
    public int m0_car_idx { get; set; }

    [XmlElement("car_register")]
    public string car_register { get; set; }

    [XmlElement("car_use_status")]
    public int car_use_status { get; set; }

    [XmlElement("car_use_idx")]
    public int car_use_idx { get; set; }

    [XmlElement("date_time_chekbutton")]
    public string date_time_chekbutton { get; set; }

    [XmlElement("decision_approve")]
    public string decision_approve { get; set; }

    [XmlElement("emp_email_sentto_headuser")]
    public string emp_email_sentto_headuser { get; set; }

    [XmlElement("car_use_name")]
    public string car_use_name { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("IFSearchbetween")]
    public int IFSearchbetween { get; set; }

    [XmlElement("count_carbooking")]
    public int count_carbooking { get; set; }

    [XmlElement("sum_expenses")]
    public int sum_expenses { get; set; }

    [XmlElement("count_booking")]
    public int count_booking { get; set; }

    [XmlElement("DC_YEAR")]
    public string DC_YEAR { get; set; }

    [XmlElement("DC_Mount")]
    public string DC_Mount { get; set; }

    [XmlElement("sum_expenses_decimal")]
    public string sum_expenses_decimal { get; set; }

    [XmlElement("sum_expenses_outplan_decimal")]
    public string sum_expenses_outplan_decimal { get; set; }

    [XmlElement("usecar_outplan_idx")]
    public int usecar_outplan_idx { get; set; }


}


