﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_networkdevices
/// </summary>
public class data_networkdevices
{
    public int return_code { get; set; }
    public string return_msg { get; set; }

    public m0type_detail[] m0type_list { get; set; }
    public m0category_detail[] m0category_list { get; set; }
    public m0place_detail[] m0place_list { get; set; }
    public m0room_detail[] m0room_list { get; set; }
    public m0company_detail[] m0company_list { get; set; }
    public m0floor_detail[] m0floor_list { get; set; }
    public m0chamber_detail[] m0chamber_list { get; set; }
    public r0position_detail[] r0position_list { get; set; }
    public m0image_detail[] m0image_list { get; set; }

}

public class m0type_detail
{

    public int type_idx { get; set; }  
    public string type_name { get; set; }   
    public int type_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }

}

public class m0category_detail
{

    public int category_idx { get; set; }
    public string category_name { get; set; }
    public int category_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int type_idx { get; set; }    
    public string type_name { get; set; }

}

public class m0place_detail
{

    public int place_idx { get; set; }
    public string place_name { get; set; }
    public int place_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
   
}

public class m0room_detail
{

    public int room_idx { get; set; }
    public string room_name { get; set; }
    public int room_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int place_idx { get; set; }
    public string place_name { get; set; }

}

public class m0company_detail
{

    public int company_idx { get; set; }
    public string company_name { get; set; }
    public int company_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }

}

public class m0floor_detail
{
    public int FLIDX { get; set; }
    public string Floor_name { get; set; }
    public int FL_status { get; set; }
    public int CEmpIDX { get; set; }
}

public class m0chamber_detail
{
    public int CHIDX { get; set; }
    public string Chamber_name { get; set; }
    public int CH_status { get; set; }
    public int CEmpIDX { get; set; }
}

public class r0position_detail
{
    public int REIDX { get; set; }

    public int FLIDX { get; set; }
    public string Floor_name { get; set; }

    public int CHIDX { get; set; }
    public string Chamber_name { get; set; }

    public int LocIDX { get; set; }
    public string Locname { get; set; }
    public int RE_status { get; set; }

    public int CEmpIDX { get; set; }
    public int u0idx { get; set; }
    public string register_number { get; set; }
    public string no_regis { get; set; }

    public int row_ { get; set; }
    public int cell_ { get; set; }
    public int status_active { get; set; }
    public int imgidx { get; set; }
    public string img_name { get; set; }

    public string place_name { get; set; }
    public int place_idx { get; set; }


}

public class m0image_detail
{
    public int imgidx { get; set; }

    public int FLIDX { get; set; }
    public string img_name { get; set; }
    public int LocIDX { get; set; }

    public string place_name { get; set; }
    public int place_idx { get; set; }


    public int buidx { get; set; }
    public int CEmpIDX { get; set; }
    public int img_status { get; set; }

    public string Floor_name { get; set; }

    public string Locname { get; set; }

    public string room_name { get; set; }


}