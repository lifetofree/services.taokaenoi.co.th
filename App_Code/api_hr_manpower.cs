﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for api_hr_manpower
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_hr_manpower : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = String.Empty;
    string _mail_subject = "";
    string _mail_body = "";
    string webmaseter = "webmaster@taokaenoi.co.th";

    service_mail _serviceMail = new service_mail();
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_hr_manpower _dtman = new data_hr_manpower();

    public api_hr_manpower()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectMaster_ManPower(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_hr_manpower", "service_manpower", _xml_in, 200); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_ManPower(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_hr_manpower", "service_manpower", _xml_in, 100); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtman = (data_hr_manpower)_funcTool.convertXmlToObject(typeof(data_hr_manpower), _local_xml);

            if (_dtman.ReturnCode.ToString() == "0")
            {
                var Email = _dtman.Boxu0_DocumentDetail[0].Email.ToString();
                var doc_code = _dtman.Boxu0_DocumentDetail[0].doc_code.ToString();
                var Email_Admin = _dtman.Boxu0_DocumentDetail[0].Email_Admin.ToString();

                Email += "," + Email_Admin;

                _mail_subject = "[HR MAN POWER : ระบบขอกำลังคน] :"   + doc_code;
                _mail_body = _serviceMail.ManpowerCreateBody(_dtman.Boxu0_DocumentDetail[0]);
                _serviceMail.SendHtmlFormattedEmailFull(Email.ToString(), "", Email.ToString(), _mail_subject, _mail_body);

            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Approve_ManPower(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_hr_manpower", "service_manpower", _xml_in, 101); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtman = (data_hr_manpower)_funcTool.convertXmlToObject(typeof(data_hr_manpower), _local_xml);

            if (_dtman.ReturnCode.ToString() == "0")
            {
                var Email = _dtman.Boxu0_DocumentDetail[0].Email.ToString();
                var doc_code = _dtman.Boxu0_DocumentDetail[0].doc_code.ToString();
                var Email_Admin = _dtman.Boxu0_DocumentDetail[0].Email_Admin.ToString();

                Email += "," + Email_Admin;

                _mail_subject = "[HR MAN POWER : ระบบขอกำลังคน] :" + doc_code;
                _mail_body = _serviceMail.ManpowerCreateBody(_dtman.Boxu0_DocumentDetail[0]);
                _serviceMail.SendHtmlFormattedEmailFull(Email.ToString(), "", Email.ToString(), _mail_subject, _mail_body);

            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
}
