using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.IO;

using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using ConfidentialProtect;

/// <summary>
/// Summary description for service_execute
/// </summary>
public class service_mail
{
    function_db _funcDB = new function_db();
    CProtect _cProtect = new CProtect();

    public service_mail()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void SendHtmlFormattedEmail(string recepientEmailList, string subject, string body)
    {
        using (MailMessage mailMessage = new MailMessage())
        {
            mailMessage.From = new MailAddress("noreply@taokaenoi.co.th");
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            string[] recepientEmail = recepientEmailList.Split(',');
            foreach (string rEmail in recepientEmail)
            {
                if (rEmail != String.Empty)
                {
                    mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                }
            }
            mailMessage.Bcc.Add(new MailAddress("noreply@taokaenoi.co.th"));
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com"; // "smtp-relay.gmail.com";
            smtp.EnableSsl = true;
            System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
            NetworkCred.UserName = "noreply@taokaenoi.co.th";
            NetworkCred.Password = "knlolksosgsuakcd";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;

            SqlConnection conn = _funcDB.strDBConn("logConn");
            try
            {
                smtp.Send(mailMessage);
            }
            catch (SmtpException ex)
            {
                string _file_name = "~/logmail.txt";
                using (StreamWriter _testData = new StreamWriter(System.Web.HttpContext.Current.Server.MapPath(_file_name), true))
                {
                    _testData.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "|" + ex.StatusCode.ToString() + "\n"); // Write the file.
                }

                conn.Open();
                string sql = "INSERT INTO log_test_mail(ex_message) SELECT ex_message = '" + ex.StatusCode + "' from server .29";
                SqlCommand command = new SqlCommand(sql, conn);
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }
    }

    public void SendHtmlFormattedEmailFull(string recepientEmailList, string recepientCcEmailList, string replyToEmailList, string subject, string body)
    {
        using (MailMessage mailMessage = new MailMessage())
        {
            mailMessage.From = new MailAddress("noreply@taokaenoi.co.th");
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            // set to mail list
            string[] recepientEmail = recepientEmailList.Split(',');
            foreach (string rEmail in recepientEmail)
            {
                if (rEmail != String.Empty)
                {
                    mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                }
            }
            // set cc mail list
            string[] recepientCcEmail = recepientCcEmailList.Split(',');
            foreach (string cEmail in recepientCcEmail)
            {
                if (cEmail != String.Empty)
                {
                    mailMessage.CC.Add(new MailAddress(cEmail)); //Adding Multiple To email Id
                }
            }
            //set reply to list
            string[] replyToEmail = replyToEmailList.Split(',');
            foreach (string rtEmail in replyToEmail)
            {
                if (rtEmail != String.Empty)
                {
                    mailMessage.ReplyToList.Add(new MailAddress(rtEmail)); //Adding Multiple To email Id
                }
            }
            mailMessage.Bcc.Add(new MailAddress("noreply@taokaenoi.co.th"));
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;
            System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
            NetworkCred.UserName = "noreply@taokaenoi.co.th";
            NetworkCred.Password = "knlolksosgsuakcd";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;

            SqlConnection conn = _funcDB.strDBConn("logConn");
            try
            {
                smtp.Send(mailMessage);
            }
            catch (SmtpException ex)
            {
                string _file_name = "~/logmail.txt";
                using (StreamWriter _testData = new StreamWriter(System.Web.HttpContext.Current.Server.MapPath(_file_name), true))
                {
                    _testData.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "|" + ex.StatusCode.ToString() + "\n"); // Write the file.
                }

                conn.Open();
                string sql = "INSERT INTO log_test_mail(ex_message) SELECT ex_message = '" + ex.StatusCode + "' from server .29";
                SqlCommand command = new SqlCommand(sql, conn);
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }
    }

    public void SendHtmlFormattedEmailPrivate(string recepientEmailList, string recepientCcEmailList, string replyToEmailList, string subject, string body)
    {
        using (MailMessage mailMessage = new MailMessage())
        {
            mailMessage.From = new MailAddress("noreply@taokaenoi.co.th");
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            // set to mail list
            string[] recepientEmail = recepientEmailList.Split(',');
            foreach (string rEmail in recepientEmail)
            {
                if (rEmail != String.Empty)
                {
                    mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                }
            }
            // set cc mail list
            string[] recepientCcEmail = recepientCcEmailList.Split(',');
            foreach (string cEmail in recepientCcEmail)
            {
                if (cEmail != String.Empty)
                {
                    mailMessage.CC.Add(new MailAddress(cEmail)); //Adding Multiple To email Id
                }
            }
            //set reply to list
            string[] replyToEmail = replyToEmailList.Split(',');
            foreach (string rtEmail in replyToEmail)
            {
                if (rtEmail != String.Empty)
                {
                    mailMessage.ReplyToList.Add(new MailAddress(rtEmail)); //Adding Multiple To email Id
                }
            }
            // mailMessage.Bcc.Add(new MailAddress("noreply@taokaenoi.co.th"));
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;
            System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
            NetworkCred.UserName = "noreply@taokaenoi.co.th";
            NetworkCred.Password = "knlolksosgsuakcd";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            try
            {
                smtp.Send(mailMessage);
            }
            catch (SmtpException ex)
            {
            }
        }
    }

    public string getEncrypted(string dataIn)
    {
        return _cProtect.CProtectEncrypt(dataIn);
    }

    public string getDecrypted(string dataIn)
    {
        return _cProtect.CProtectDecrypt(dataIn);
    }

    #region api_vacation
    public string VacationCreateBody(u0_document_detail _u0_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_vacation_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{emp_code}", _u0_detail.emp_code);
        body = body.Replace("{emp_name_th}", _u0_detail.emp_name_th);
        body = body.Replace("{pos_name_th}", _u0_detail.pos_name_th);
        body = body.Replace("{u0_leave_date}", _u0_detail.u0_leave_start + " - " + _u0_detail.u0_leave_end);
        body = body.Replace("{m0_leavetype_name}", _u0_detail.m0_leavetype_name);
        body = body.Replace("{u0_emp_shift_name}", _u0_detail.u0_emp_shift_name);
        body = body.Replace("{u0_leave_comment}", _u0_detail.u0_leave_comment);
        return body;
    }

    public string VacationApproveBody(u0_document_detail _u0_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_vacation_approve.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{emp_code}", _u0_detail.emp_code);
        body = body.Replace("{emp_name_th}", _u0_detail.emp_name_th);
        body = body.Replace("{pos_name_th}", _u0_detail.pos_name_th);
        body = body.Replace("{u0_leave_date}", _u0_detail.u0_leave_start + " - " + _u0_detail.u0_leave_end);
        body = body.Replace("{m0_leavetype_name}", _u0_detail.m0_leavetype_name);
        body = body.Replace("{u0_emp_shift_name}", _u0_detail.u0_emp_shift_name);
        body = body.Replace("{u0_leave_comment}", _u0_detail.u0_leave_comment);
        body = body.Replace("{approve_status_name}", _u0_detail.u0_status_name);
        return body;
    }
    #endregion api_vacation

    #region api_softwarelicense_devices
    public string softwareLicenseCreate(u0_softwarelicense_detail _insertu0_softwarelicense_detail, string link_software_insert)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_software_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{u0_code}", _insertu0_softwarelicense_detail.u0_code);
        body = body.Replace("{software_name}", _insertu0_softwarelicense_detail.software_name);
        //body = body.Replace("{software_nameemail}", value_software_name);
        body = body.Replace("{link}", link_software_insert);

        return body;
    }

    public string SoftwareApproveCreate(u0_softwarelicense_detail _approveu0_softwarelicense_detail, string link_approve)
    {
        string body = string.Empty;


        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_softwareapprove_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{u0_code}", _approveu0_softwarelicense_detail.u0_code);
        body = body.Replace("{comment}", _approveu0_softwarelicense_detail.comment);
        body = body.Replace("{link}", link_approve);

        return body;
    }

    #endregion api_softwarelicense_devices

    #region api_software_license
    public string googleLicenseCreate(bind_u0_document_ggl_detail _bind_u0_document_ggl_detail, string link_google_insert)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_holder_google_license_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{email_license}", _bind_u0_document_ggl_detail.email_license);
        body = body.Replace("{holder_gmail}", _bind_u0_document_ggl_detail.holder_gmail);
        //body = body.Replace("{software_nameemail}", value_software_name);
        body = body.Replace("{link}", link_google_insert);

        return body;
    }

    public string googleLicenseApprove(approve_u0_document_ggl_detail _approve_u0_document_ggl_detail, string link_google_insert)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_holder_google_license_approve.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{email_license}", _approve_u0_document_ggl_detail.email_license);
        body = body.Replace("{name_holder}", _approve_u0_document_ggl_detail.name_holder);
        body = body.Replace("{comment}", _approve_u0_document_ggl_detail.comment);
        //body = body.Replace("{software_nameemail}", value_software_name);
        body = body.Replace("{link}", link_google_insert);

        return body;
    }


    #endregion api_google_license

    #region api_device
    public string deviceCreateBody(u0_device_detail _u0_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_device_create_holder.html")))
        {
            body = reader.ReadToEnd();
        }

        int tdidx = _u0_detail.m0_tdidx;
        //body = body.Replace("{Email}", "mail");//_u0_detail.Email);
        //body = body.Replace("{u0_code}", _u0_detail.u0_code);
        //body = body.Replace("{FullNameTH_HD}", "-");// _u0_detail.FullNameTH_HD);
        //body = body.Replace("{name_m0_typedevice}", _u0_detail.name_m0_typedevice);
        //body = body.Replace("{Detail_Device}", _u0_detail.ddl_band_vga);

        switch (tdidx)
        {
            case 6: //Cpu
                body = body.Replace("{Email}", _u0_detail.Email);
                body = body.Replace("{u0_code}", _u0_detail.u0_code);
                body = body.Replace("{FullNameTH_HD}", _u0_detail.FullNameTH_HD);
                body = body.Replace("{name_m0_typedevice}", _u0_detail.name_m0_typedevice);
                body = body.Replace("{Detail_Device}", _u0_detail.ddl_band_cpu);
                break;
            case 7: //Ram
                body = body.Replace("{Email}", _u0_detail.Email);
                body = body.Replace("{u0_code}", _u0_detail.u0_code);
                body = body.Replace("{FullNameTH_HD}", _u0_detail.FullNameTH_HD);
                body = body.Replace("{name_m0_typedevice}", _u0_detail.name_m0_typedevice);
                body = body.Replace("{Detail_Device}", _u0_detail.ddl_band_ram);
                break;
            case 8: //Hdd
                body = body.Replace("{Email}", _u0_detail.Email);
                body = body.Replace("{u0_code}", _u0_detail.u0_code);
                body = body.Replace("{FullNameTH_HD}", _u0_detail.FullNameTH_HD);
                body = body.Replace("{name_m0_typedevice}", _u0_detail.name_m0_typedevice);
                body = body.Replace("{Detail_Device}", _u0_detail.ddl_band_hdd);
                break;
            case 9: //Vga
                body = body.Replace("{Email}", _u0_detail.Email);
                body = body.Replace("{u0_code}", _u0_detail.u0_code);
                body = body.Replace("{FullNameTH_HD}", _u0_detail.FullNameTH_HD);
                body = body.Replace("{name_m0_typedevice}", _u0_detail.name_m0_typedevice);
                body = body.Replace("{Detail_Device}", _u0_detail.ddl_band_vga);
                break;
            case 10: //Other
                     //body = body.Replace("{ddl_band_cpu}", _u0_detail.d);
                break;
        }

        return body;
    }

    /*public string deviceApproveBody(u0_device_detail _u0_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_device_approve.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{emp_code}", _u0_detail.emp_code);
        body = body.Replace("{emp_name_th}", _u0_detail.emp_name_th);
        body = body.Replace("{pos_name_th}", _u0_detail.pos_name_th);
        body = body.Replace("{u0_leave_date}", _u0_detail.u0_leave_start + " - " + _u0_detail.u0_leave_end);
        body = body.Replace("{m0_leavetype_name}", _u0_detail.m0_leavetype_name);
        body = body.Replace("{u0_emp_shift_name}", _u0_detail.u0_emp_shift_name);
        body = body.Replace("{u0_leave_comment}", _u0_detail.u0_leave_comment);
        body = body.Replace("{approve_status_name}", _u0_detail.u0_status_name);
        return body;
    }*/
    #endregion api_vacation

    #region api_holder-devices

    public string HoldersCreateBody(DataHoldersDevices _dtholder, string rtCode_type, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_holders_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{ReturnName}", _dtholder.ReturnName);
        body = body.Replace("{ReturnDept}", _dtholder.ReturnDept);
        body = body.Replace("{ReturnDept_Accept}", _dtholder.ReturnDept_Accept);
        body = body.Replace("{ReturnDate}", _dtholder.ReturnDate);
        body = body.Replace("{ReturnDeviceCode}", _dtholder.ReturnDeviceCode);
        body = body.Replace("{ReturnAsset}", _dtholder.ReturnAsset);
        body = body.Replace("{rtCode_type}", rtCode_type);
        body = body.Replace("{ReturnName_Accept}", _dtholder.ReturnName_Accept);
        body = body.Replace("{link}", link);

        return body;
    }

    public string HoldersChangeBody(DataHoldersDevices _dtholder, string rtCode_type, string link, string status)
    {
        string body = string.Empty;

        if (status == "0")
        {
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_holders_change.html")))
            {
                body = reader.ReadToEnd();
            }
        }
        else if (status == "1")
        {
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_holders_cancel.html")))
            {
                body = reader.ReadToEnd();
            }
        }
        body = body.Replace("{ReturnEmail}", _dtholder.ReturnEmail);
        body = body.Replace("{ReturnName}", _dtholder.ReturnName);
        body = body.Replace("{ReturnDept}", _dtholder.ReturnDept);
        body = body.Replace("{ReturnDept_Accept}", _dtholder.ReturnDept_Accept);
        body = body.Replace("{ReturnDate}", _dtholder.ReturnDate);
        body = body.Replace("{ReturnDeviceCode}", _dtholder.ReturnDeviceCode);
        body = body.Replace("{ReturnAsset}", _dtholder.ReturnAsset);
        body = body.Replace("{rtCode_type}", rtCode_type);
        body = body.Replace("{ReturnName_Own}", _dtholder.ReturnName_Own);
        body = body.Replace("{ReturnName_Accept}", _dtholder.ReturnName_Accept);
        body = body.Replace("{ReturnName_Before}", _dtholder.ReturnName_Before);
        body = body.Replace("{link}", link);

        return body;
    }

    public string HoldersApproveChangeBody(DataHoldersDevices _dtholder, string rtCode_type, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_holders_approve.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{ReturnEmail}", _dtholder.ReturnEmail);
        body = body.Replace("{ReturnName}", _dtholder.ReturnName);
        body = body.Replace("{ReturnDept}", _dtholder.ReturnDept);
        body = body.Replace("{ReturnDept_Accept}", _dtholder.ReturnDept_Accept);
        body = body.Replace("{ReturnDate}", _dtholder.ReturnDate);
        body = body.Replace("{ReturnDeviceCode}", _dtholder.ReturnDeviceCode);
        body = body.Replace("{ReturnAsset}", _dtholder.ReturnAsset);
        body = body.Replace("{rtCode_type}", rtCode_type);
        body = body.Replace("{ReturnName_Own}", _dtholder.ReturnName_Own);
        body = body.Replace("{ReturnName_Accept}", _dtholder.ReturnName_Accept);
        body = body.Replace("{ReturnName_Before}", _dtholder.ReturnName_Before);
        body = body.Replace("{link}", link);

        return body;
    }

    public string HoldersFlowApproveChangeBody(DataHoldersDevices _dtholder, string rtCode_type, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_holders_flowapprove.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{ReturnEmail}", _dtholder.ReturnEmail);
        body = body.Replace("{ReturnName}", _dtholder.ReturnName);
        body = body.Replace("{ReturnDept}", _dtholder.ReturnDept);
        body = body.Replace("{ReturnDept_Accept}", _dtholder.ReturnDept_Accept);
        body = body.Replace("{ReturnDate}", _dtholder.ReturnDate);
        body = body.Replace("{ReturnDeviceCode}", _dtholder.ReturnDeviceCode);
        body = body.Replace("{ReturnAsset}", _dtholder.ReturnAsset);
        body = body.Replace("{rtCode_type}", rtCode_type);
        body = body.Replace("{ReturnName_Own}", _dtholder.ReturnName_Own);
        body = body.Replace("{ReturnName_Accept}", _dtholder.ReturnName_Accept);
        body = body.Replace("{ReturnName_Before}", _dtholder.ReturnName_Before);
        body = body.Replace("{link}", link);

        return body;
    }

    public string HoldersNonApproveChangeBody(DataHoldersDevices _dtholder, string rtCode_type, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_holders_nonapprove.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{ReturnEmail}", _dtholder.ReturnEmail);
        body = body.Replace("{ReturnName}", _dtholder.ReturnName);
        body = body.Replace("{ReturnDept}", _dtholder.ReturnDept);
        body = body.Replace("{ReturnDept_Accept}", _dtholder.ReturnDept_Accept);
        body = body.Replace("{ReturnDate}", _dtholder.ReturnDate);
        body = body.Replace("{ReturnDeviceCode}", _dtholder.ReturnDeviceCode);
        body = body.Replace("{ReturnAsset}", _dtholder.ReturnAsset);
        body = body.Replace("{rtCode_type}", rtCode_type);
        body = body.Replace("{ReturnName_Own}", _dtholder.ReturnName_Own);
        body = body.Replace("{ReturnName_Accept}", _dtholder.ReturnName_Accept);
        body = body.Replace("{ReturnName_Before}", _dtholder.ReturnName_Before);
        body = body.Replace("{link}", link);

        return body;
    }


    #endregion

    #region api_chr-approve
    public string CHR_ApproveBody(BindData _bind, string rtCode_name, string rtCode_status, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_chr_approve.html")))
        {
            body = reader.ReadToEnd();
        }
        //body = body.Replace("{rtCode_name}", rtCode_name);
        //body = body.Replace("{rtCode_status}", rtCode_status);
        //body = body.Replace("{doc_code}", _data_chr.doc);
        //body = body.Replace("{System_name}", _data_chr.BindData_U0Document[0].System_name);
        //body = body.Replace("{EmpName}", _data_chr.BindData_U0Document[0].EmpName);
        //body = body.Replace("{DeptNameTH}", _data_chr.BindData_U0Document[0].DeptNameTH);
        //body = body.Replace("{CostNo}", _data_chr.BindData_U0Document[0].CostNo);
        //body = body.Replace("{Email}", _data_chr.BindData_U0Document[0].Email);
        //body = body.Replace("{Usersap}", _data_chr.BindData_U0Document[0].Usersap);
        //body = body.Replace("{topic}", _data_chr.BindData_U0Document[0].Topic);
        //body = body.Replace("{new_sys}", _data_chr.BindData_U0Document[0].new_sys);
        //body = body.Replace("{old_sys}", _data_chr.BindData_U0Document[0].old_sys);
        //body = body.Replace("{Approve1}", _data_chr.BindData_U0Document[0].Approve1);
        //body = body.Replace("{Approve2}", _data_chr.BindData_U0Document[0].Approve2);
        //body = body.Replace("{Approve3}", _data_chr.BindData_U0Document[0].Approve3);
        body = body.Replace("{rtCode_name}", rtCode_name);
        body = body.Replace("{rtCode_status}", rtCode_status);
        body = body.Replace("{doc_code}", _bind.doc_code);
        body = body.Replace("{System_name}", _bind.System_name);
        body = body.Replace("{EmpName}", _bind.EmpName);
        body = body.Replace("{DeptNameTH}", _bind.DeptNameTH);
        body = body.Replace("{CostNo}", _bind.CostNo);
        body = body.Replace("{Email}", _bind.Email);
        body = body.Replace("{Usersap}", _bind.Usersap);
        body = body.Replace("{topic}", _bind.Topic);
        body = body.Replace("{new_sys}", _bind.new_sys);
        body = body.Replace("{old_sys}", _bind.old_sys);
        body = body.Replace("{Approve1}", _bind.Approve1);
        body = body.Replace("{Approve2}", _bind.Approve2);
        body = body.Replace("{Approve3}", _bind.Approve3);
        body = body.Replace("{link}", link);

        return body;
    }

    #endregion

    #region api_ma-online

    public string MaOnlineCreateBody(datama_online _datama, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ma-online_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{ReturnCode}", _datama.ReturnCode);
        body = body.Replace("{ReturnU0Code}", _datama.ReturnU0Code);
        body = body.Replace("{ReturnORGTH}", _datama.ReturnORGTH);
        body = body.Replace("{ReturnDepTH}", _datama.ReturnDepTH);
        body = body.Replace("{ReturnSecTH}", _datama.ReturnSecTH);
        body = body.Replace("{ReturnEmpName}", _datama.ReturnEmpName);
        body = body.Replace("{ReturnAdmin}", _datama.ReturnAdmin);
        body = body.Replace("{ReturnStatus}", _datama.ReturnStatus);
        body = body.Replace("{ReturnCreateDate}", _datama.ReturnCreateDate);
        body = body.Replace("{ReturnMsg}", _datama.ReturnMsg);
        body = body.Replace("{link}", link);

        return body;
    }

    public string MaOnlineApproveBody(datama_online _datama, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ma_online_approve.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{ReturnCode}", _datama.ReturnCode);
        body = body.Replace("{ReturnU0Code}", _datama.ReturnU0Code);
        body = body.Replace("{ReturnORGTH}", _datama.ReturnORGTH);
        body = body.Replace("{ReturnDepTH}", _datama.ReturnDepTH);
        body = body.Replace("{ReturnSecTH}", _datama.ReturnSecTH);
        body = body.Replace("{ReturnEmpName}", _datama.ReturnEmpName);
        body = body.Replace("{ReturnAdmin}", _datama.ReturnAdmin);
        body = body.Replace("{ReturnStatus}", _datama.ReturnStatus);
        body = body.Replace("{ReturnCreateDate}", _datama.ReturnCreateDate);
        body = body.Replace("{ReturnMsg}", _datama.ReturnMsg);
        body = body.Replace("{link}", link);

        return body;
    }



    #endregion

    #region api_document_online
    public string expDateEffective(Details_U0_DocumentONLine _Details_U0_DocumentONLine)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_doc_online_exp_date_effective.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{days_of_time}", _Details_U0_DocumentONLine.days_of_time);
        body = body.Replace("{doc_rev}", _Details_U0_DocumentONLine.doc_rev);
        body = body.Replace("{doc_type_iso}", _Details_U0_DocumentONLine.doc_type_iso);
        body = body.Replace("{doc_name_iso}", _Details_U0_DocumentONLine.doc_name_iso);
        body = body.Replace("{doc_no}", _Details_U0_DocumentONLine.doc_no);
        body = body.Replace("{doc_name}", _Details_U0_DocumentONLine.doc_name);
        body = body.Replace("{doc_date_effective}", _Details_U0_DocumentONLine.doc_date_effective);
        return body;
    }
    #endregion

    #region api_it_news
    public string itnews_alert(u0_it_news_details alert_news, string link_news_alert, string pathfile)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_it_news_alert.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{name_type_news}", alert_news.name_type_news);
        body = body.Replace("{title_it_news}", alert_news.title_it_news);
        //   body = body.Replace("{picture}", pathfile);
        // body = body.Replace(String.Format(@"<img src=""cid:{0}"" />" + "{ picture}"), pathfile);
        body = body.Replace("{details_news}", alert_news.details_news);
        body = body.Replace("{link}", link_news_alert);

        return body;
    }



    #endregion

    #region api qa lab : labis
    //sent mail for create document test list
    public string SentMailLABISCreate(qa_lab_u0doc_qalab u0Document, string link_system_labis)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_qa_lab_create.html")))
        {
            body = reader.ReadToEnd();
        }

        string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        string Createlist = "";
        foreach (string rCreate in _listTestDetailCreate)
        {
            if (rCreate != String.Empty)
            {
                Createlist += rCreate + "<br />";
            }
        }

        body = body.Replace("{document_code}", u0Document.document_code);
        body = body.Replace("{emp_name_th}", u0Document.emp_name_th);
        body = body.Replace("{org_name_th}", u0Document.org_name_th);
        body = body.Replace("{dept_name_th}", u0Document.dept_name_th);
        body = body.Replace("{sec_name_th}", u0Document.sec_name_th);
        body = body.Replace("{emp_email}", u0Document.emp_email);
        body = body.Replace("{test_detail_name}", Createlist);
        body = body.Replace("{create_date}", u0Document.create_date);
        body = body.Replace("{link}", link_system_labis);

        return body;
    }

    //sent mail for document test result success
    public string SentMailLABISResultSuccess(qa_lab_u0doc_qalab u0DocumentSuccess, string link_system_labis)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_qa_lab_result_success.html")))
        {
            body = reader.ReadToEnd();
        }


        string[] _listTestDetail = u0DocumentSuccess.test_detail_name.Split('|');
        string Resultlist = "";
        foreach (string rResult in _listTestDetail)
        {
            if (rResult != String.Empty)
            {
                Resultlist += rResult + "<br />";
            }
        }

        body = body.Replace("{document_code}", u0DocumentSuccess.document_code);
        body = body.Replace("{emp_name_th}", u0DocumentSuccess.emp_name_th);
        body = body.Replace("{org_name_th}", u0DocumentSuccess.org_name_th);
        body = body.Replace("{dept_name_th}", u0DocumentSuccess.dept_name_th);
        body = body.Replace("{sec_name_th}", u0DocumentSuccess.sec_name_th);
        body = body.Replace("{emp_email}", u0DocumentSuccess.emp_email);
        body = body.Replace("{test_detail_name}", Resultlist);
        body = body.Replace("{link}", link_system_labis);

        return body;
    }


    //sent mail for reject document test list
    public string SentMailLABISReject(qa_lab_u0doc_qalab u0DocumentReject, string link_system_labis)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_qa_lab_reject.html")))
        {
            body = reader.ReadToEnd();
        }

        string[] _listTestDetailReject = u0DocumentReject.test_detail_name.Split('|');
        string _rejectlist = "";
        foreach (string rejectlist in _listTestDetailReject)
        {
            if (rejectlist != String.Empty)
            {
                _rejectlist += rejectlist + "<br />";
            }
        }

        string[] _listSampleCodeReject = u0DocumentReject.sample_code.Split('|');
        string _rejectSampleCodelist = "";
        foreach (string rejectSampleCodelist in _listSampleCodeReject)
        {
            if (rejectSampleCodelist != String.Empty)
            {
                _rejectSampleCodelist += rejectSampleCodelist + "<br />";
            }
        }

        body = body.Replace("{document_code}", u0DocumentReject.document_code);
        body = body.Replace("{sample_code}", u0DocumentReject.sample_code);
        body = body.Replace("{emp_name_th}", u0DocumentReject.emp_name_th);
        body = body.Replace("{org_name_th}", u0DocumentReject.org_name_th);
        body = body.Replace("{dept_name_th}", u0DocumentReject.dept_name_th);
        body = body.Replace("{sec_name_th}", u0DocumentReject.sec_name_th);
        body = body.Replace("{emp_email}", u0DocumentReject.emp_email);
        body = body.Replace("{test_detail_name}", _rejectlist);
        body = body.Replace("{create_date}", u0DocumentReject.create_date);
        body = body.Replace("{link}", link_system_labis);

        return body;
    }


    #endregion

    #region api qa lab : cims devices
    //sent mail for create document test list
    public string SentMailTranferDevices(qa_cims_u0_document_device_detail u0_document_device, string link_system_cims_devices)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_qa_lab_cims_tranfer.html")))
        {
            body = reader.ReadToEnd();
        }

        string[] _listTestDetailCreate = u0_document_device.device_id_no.Split('|');
        string Createlist = "";
        foreach (string rCreate in _listTestDetailCreate)
        {
            if (rCreate != String.Empty)
            {
                Createlist += rCreate + "<br />";
            }
        }

        //body = body.Replace("{document_code}", u0_document_device.document_code);
        body = body.Replace("{emp_name_th_create}", u0_document_device.emp_name_th_create);
        body = body.Replace("{org_name_th_create}", u0_document_device.org_name_th_create);
        body = body.Replace("{dept_name_th_create}", u0_document_device.dept_name_th_create);
        body = body.Replace("{sec_name_th_create}", u0_document_device.sec_name_th_create);
        body = body.Replace("{emp_email_create}", u0_document_device.emp_email_create);
        //body = body.Replace("{test_detail_name}", Createlist);
        body = body.Replace("{create_date}", u0_document_device.create_date);
        body = body.Replace("{device_id_no}", Createlist);
        body = body.Replace("{org_tranfer}", u0_document_device.org_tranfer);
        body = body.Replace("{dept_tranfer}", u0_document_device.dept_tranfer);
        body = body.Replace("{sec_tranfer}", u0_document_device.sec_tranfer);
        body = body.Replace("{head_email_tranfer}", u0_document_device.head_email_tranfer);

        body = body.Replace("{link}", link_system_cims_devices);

        return body;
    }

    //sent mail for head user approve
    public string SentMailTranferHeadCreateApprove(qa_cims_u0_document_device_detail u0_document_device, string link_system_cims_devices)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_qa_lab_cims_headtranfer.html")))
        {
            body = reader.ReadToEnd();
        }

        string[] _listTestDetailCreate = u0_document_device.device_id_no.Split('|');
        string Createlist_Head = "";
        foreach (string rCreate in _listTestDetailCreate)
        {
            if (rCreate != String.Empty)
            {
                Createlist_Head += rCreate + "<br />";
            }
        }

        //body = body.Replace("{document_code}", u0_document_device.document_code);
        body = body.Replace("{emp_name_th_create}", u0_document_device.emp_name_th_create);
        body = body.Replace("{org_name_th_create}", u0_document_device.org_name_th_create);
        body = body.Replace("{dept_name_th_create}", u0_document_device.dept_name_th_create);
        body = body.Replace("{sec_name_th_create}", u0_document_device.sec_name_th_create);
        body = body.Replace("{emp_email_create}", u0_document_device.emp_email_create);
        //body = body.Replace("{test_detail_name}", Createlist);
        body = body.Replace("{create_date}", u0_document_device.create_date);
        body = body.Replace("{device_id_no}", Createlist_Head);
        body = body.Replace("{org_tranfer}", u0_document_device.org_tranfer);
        body = body.Replace("{dept_tranfer}", u0_document_device.dept_tranfer);
        body = body.Replace("{sec_tranfer}", u0_document_device.sec_tranfer);
        body = body.Replace("{email_tranfer}", u0_document_device.email_tranfer);
        body = body.Replace("{decision_status}", u0_document_device.decision_status);

        body = body.Replace("{link}", link_system_cims_devices);

        return body;
    }

    //sent mail for head user approve
    public string SentMailTranferQAApprove(qa_cims_u0_document_device_detail u0_document_device, string link_system_cims_devices)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_qa_lab_cims_qa_lab_tranfer.html")))
        {
            body = reader.ReadToEnd();
        }

        string[] _listTestDetailCreate = u0_document_device.device_id_no.Split('|');
        string Createlist_QA = "";
        foreach (string rCreate in _listTestDetailCreate)
        {
            if (rCreate != String.Empty)
            {
                Createlist_QA += rCreate + "<br />";
            }
        }

        //body = body.Replace("{document_code}", u0_document_device.document_code);
        body = body.Replace("{emp_name_th_create}", u0_document_device.emp_name_th_create);
        body = body.Replace("{org_name_th_create}", u0_document_device.org_name_th_create);
        body = body.Replace("{dept_name_th_create}", u0_document_device.dept_name_th_create);
        body = body.Replace("{sec_name_th_create}", u0_document_device.sec_name_th_create);
        body = body.Replace("{emp_email_create}", u0_document_device.emp_email_create);
        //body = body.Replace("{test_detail_name}", Createlist);
        body = body.Replace("{create_date}", u0_document_device.create_date);
        body = body.Replace("{device_id_no}", Createlist_QA);
        body = body.Replace("{org_tranfer}", u0_document_device.org_tranfer);
        body = body.Replace("{dept_tranfer}", u0_document_device.dept_tranfer);
        body = body.Replace("{sec_tranfer}", u0_document_device.sec_tranfer);

        body = body.Replace("{decision_status}", u0_document_device.decision_status);

        body = body.Replace("{headqa_email_tranfer}", u0_document_device.headqa_email_tranfer);
        body = body.Replace("{email_tranfer}", u0_document_device.email_tranfer);
        body = body.Replace("{email_create_tranfer}", u0_document_device.email_create_tranfer);

        body = body.Replace("{link}", link_system_cims_devices);

        return body;
    }

    //sent mail for head qa approve
    public string SentMailTranferHeadQA(qa_cims_u0_document_device_detail u0_document_device, string link_system_cims_devices)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_qa_lab_cims_qa_lab_headqa_tranfer.html")))
        {
            body = reader.ReadToEnd();
        }

        string[] _listTestDetailCreate = u0_document_device.device_id_no.Split('|');
        string Createlist_QA = "";
        foreach (string rCreate in _listTestDetailCreate)
        {
            if (rCreate != String.Empty)
            {
                Createlist_QA += rCreate + "<br />";
            }
        }

        //body = body.Replace("{document_code}", u0_document_device.document_code);
        body = body.Replace("{emp_name_th_create}", u0_document_device.emp_name_th_create);
        body = body.Replace("{org_name_th_create}", u0_document_device.org_name_th_create);
        body = body.Replace("{dept_name_th_create}", u0_document_device.dept_name_th_create);
        body = body.Replace("{sec_name_th_create}", u0_document_device.sec_name_th_create);
        body = body.Replace("{emp_email_create}", u0_document_device.emp_email_create);
        //body = body.Replace("{test_detail_name}", Createlist);
        body = body.Replace("{create_date}", u0_document_device.create_date);
        body = body.Replace("{device_id_no}", Createlist_QA);
        body = body.Replace("{org_tranfer}", u0_document_device.org_tranfer);
        body = body.Replace("{dept_tranfer}", u0_document_device.dept_tranfer);
        body = body.Replace("{sec_tranfer}", u0_document_device.sec_tranfer);
        body = body.Replace("{email_tranfer}", u0_document_device.email_tranfer);
        body = body.Replace("{decision_status}", u0_document_device.decision_status);
        body = body.Replace("{headqa_email_tranfer}", u0_document_device.headqa_email_tranfer);
        body = body.Replace("{email_create_tranfer}", u0_document_device.email_create_tranfer);
        body = body.Replace("{link}", link_system_cims_devices);

        return body;
    }

    //sent mail for receive tranfer approve
    public string SentMailTranferRecive(qa_cims_u0_document_device_detail u0_document_device, string link_system_cims_devices)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_qa_lab_cims_recive_tranfer.html")))
        {
            body = reader.ReadToEnd();
        }

        string[] _listTestDetailCreate = u0_document_device.device_id_no.Split('|');
        string Createlist_QA = "";
        foreach (string rCreate in _listTestDetailCreate)
        {
            if (rCreate != String.Empty)
            {
                Createlist_QA += rCreate + "<br />";
            }
        }

        //body = body.Replace("{document_code}", u0_document_device.document_code);
        body = body.Replace("{emp_name_th_create}", u0_document_device.emp_name_th_create);
        body = body.Replace("{org_name_th_create}", u0_document_device.org_name_th_create);
        body = body.Replace("{dept_name_th_create}", u0_document_device.dept_name_th_create);
        body = body.Replace("{sec_name_th_create}", u0_document_device.sec_name_th_create);
        body = body.Replace("{emp_email_create}", u0_document_device.emp_email_create);
        //body = body.Replace("{test_detail_name}", Createlist);
        body = body.Replace("{create_date}", u0_document_device.create_date);
        body = body.Replace("{device_id_no}", Createlist_QA);
        body = body.Replace("{org_tranfer}", u0_document_device.org_tranfer);
        body = body.Replace("{dept_tranfer}", u0_document_device.dept_tranfer);
        body = body.Replace("{sec_tranfer}", u0_document_device.sec_tranfer);
        body = body.Replace("{email_tranfer}", u0_document_device.email_tranfer);
        body = body.Replace("{decision_status}", u0_document_device.decision_status);
        body = body.Replace("{headqa_email_tranfer}", u0_document_device.headqa_email_tranfer);
        body = body.Replace("{email_create_tranfer}", u0_document_device.email_create_tranfer);
        body = body.Replace("{link}", link_system_cims_devices);

        return body;
    }
    #endregion

    #region api job order
    public string SentMailJob_orderCreate(job_order_overview U0jobrder, string link_system_joborder)
    {
        string body = string.Empty;
        // string bodyreq = HttpUtility.HtmlDecode(U0jobrder.require_jo).ToString();
        //   body = HttpUtility.HtmlDecode(body);
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_job_order_create.html")))
        {
            body = reader.ReadToEnd();

        }

        //  var decode = (HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(U0jobrder.require_jo).ToString()).ToString()).ToString());
        var decode = HttpUtility.HtmlDecode(U0jobrder.require_jo).ToString();

        body = body.Replace("{no_invoice}", U0jobrder.no_invoice);
        body = body.Replace("{emp_name_th}", U0jobrder.emp_name_th);
        body = body.Replace("{org_name_th}", U0jobrder.org_name_th);
        body = body.Replace("{dept_name_th}", U0jobrder.dept_name_th);
        body = body.Replace("{sec_name_th}", U0jobrder.sec_name_th);
        body = body.Replace("{emp_email}", U0jobrder.emp_email);
        body = body.Replace("{create_date}", U0jobrder.day_created);
        body = body.Replace("{statenode_name}", U0jobrder.statenode_name);
        body = body.Replace("{title_jo}", U0jobrder.title_jo);
        body = body.Replace("{require_jo}", decode);
        body = body.Replace("{actor_name}", U0jobrder.name_description);
        body = body.Replace("{link}", link_system_joborder);


        return body;
    }

    public string SentMailJob_orderend(job_order_overview U0jobrder, string link_system_joborder)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_job_order_end.html")))
        {
            body = reader.ReadToEnd();
        }

        var decode = HttpUtility.HtmlDecode(U0jobrder.require_jo).ToString();

        body = body.Replace("{no_invoice}", U0jobrder.no_invoice);
        body = body.Replace("{emp_name_th}", U0jobrder.emp_name_th);
        body = body.Replace("{org_name_th}", U0jobrder.org_name_th);
        body = body.Replace("{dept_name_th}", U0jobrder.dept_name_th);
        body = body.Replace("{sec_name_th}", U0jobrder.sec_name_th);
        body = body.Replace("{emp_email}", U0jobrder.emp_email);
        body = body.Replace("{day_created}", U0jobrder.day_created);
        body = body.Replace("{statenode_name}", U0jobrder.statenode_name);
        body = body.Replace("{title_jo}", U0jobrder.title_jo);
        body = body.Replace("{require_jo}", decode);
        body = body.Replace("{name_description}", U0jobrder.name_description);
        body = body.Replace("{link}", link_system_joborder);

        return body;
    }

    public string SentMailJob_orderCreateU1_(jo_add_datalist U1jobrder, string link_system_joborder)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_job_order_InsertU1.html")))
        {
            body = reader.ReadToEnd();
        }

        var decode = HttpUtility.HtmlDecode(U1jobrder.require_jo).ToString();

        body = body.Replace("{no_invoice}", U1jobrder.no_invoice);
        body = body.Replace("{emp_name_th}", U1jobrder.emp_name_th);
        body = body.Replace("{org_name_th}", U1jobrder.org_name_th);
        body = body.Replace("{dept_name_th}", U1jobrder.dept_name_th);
        body = body.Replace("{sec_name_th}", U1jobrder.sec_name_th);
        body = body.Replace("{emp_email}", U1jobrder.emp_email);
        body = body.Replace("{u1_title}", U1jobrder.u1_title);
        body = body.Replace("{u1_day_sent}", U1jobrder.u1_day_sent);
        body = body.Replace("{title_jo}", U1jobrder.title_jo);
        body = body.Replace("{require_jo}", decode);
        body = body.Replace("{create_date}", U1jobrder.day_created);
        body = body.Replace("{actor_name}", U1jobrder.name_description);
        body = body.Replace("{link}", link_system_joborder);

        return body;
    }


    public string SentMailJob_orderCreatesubmit(jo_add_datalist U1jobrder, string link_system_joborder)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_job_order_submitjob.html")))
        {
            body = reader.ReadToEnd();
        }

        var decode = HttpUtility.HtmlDecode(U1jobrder.require_jo).ToString();

        body = body.Replace("{title}", U1jobrder.u1_title);
        body = body.Replace("{no_invoice}", U1jobrder.no_invoice);
        body = body.Replace("{emp_name_th}", U1jobrder.emp_name_th);
        body = body.Replace("{org_name_th}", U1jobrder.org_name_th);
        body = body.Replace("{dept_name_th}", U1jobrder.dept_name_th);
        body = body.Replace("{sec_name_th}", U1jobrder.sec_name_th);
        body = body.Replace("{emp_email}", U1jobrder.emp_email);
        body = body.Replace("{title_jo}", U1jobrder.title_jo);
        body = body.Replace("{require_jo}", decode);
        body = body.Replace("{create_date}", U1jobrder.day_created);
        body = body.Replace("{statenode_name}", U1jobrder.statenode_name);
        body = body.Replace("{link}", link_system_joborder);

        return body;
    }

    public string SentMailJob_orderCreatecancel(jo_add_datalist U1jobrder, string link_system_joborder)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_job_order_canceljob.html")))
        {
            body = reader.ReadToEnd();
        }

        var decode = HttpUtility.HtmlDecode(U1jobrder.require_jo).ToString();

        body = body.Replace("{title}", U1jobrder.u1_title);
        body = body.Replace("{no_invoice}", U1jobrder.no_invoice);
        body = body.Replace("{emp_name_th}", U1jobrder.emp_name_th);
        body = body.Replace("{org_name_th}", U1jobrder.org_name_th);
        body = body.Replace("{dept_name_th}", U1jobrder.dept_name_th);
        body = body.Replace("{sec_name_th}", U1jobrder.sec_name_th);
        body = body.Replace("{emp_email}", U1jobrder.emp_email);
        body = body.Replace("{title_jo}", U1jobrder.title_jo);
        body = body.Replace("{require_jo}", decode);
        body = body.Replace("{create_date}", U1jobrder.day_created);
        body = body.Replace("{statenode_name}", U1jobrder.statenode_name);
        body = body.Replace("{link}", link_system_joborder);

        return body;
    }

    #endregion

    #region api enrepair
    public string ENRepairCreateBody(DataMachine _dtmachine)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ENRepair_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{RECode}", _dtmachine.BoxRepairMachine[0].RECode);
        body = body.Replace("{NameUser}", _dtmachine.BoxRepairMachine[0].NameUser);
        body = body.Replace("{UserDeptNameTH}", _dtmachine.BoxRepairMachine[0].UserDeptNameTH);
        body = body.Replace("{MobileNo}", _dtmachine.BoxRepairMachine[0].MobileNo);
        body = body.Replace("{EMail}", _dtmachine.BoxRepairMachine[0].EMail);
        body = body.Replace("{LocName}", _dtmachine.BoxRepairMachine[0].LocName);
        body = body.Replace("{BuildingName}", _dtmachine.BoxRepairMachine[0].BuildingName);
        body = body.Replace("{Roomname}", _dtmachine.BoxRepairMachine[0].Roomname);
        body = body.Replace("{TypeNameTH}", _dtmachine.BoxRepairMachine[0].TypeNameTH);
        body = body.Replace("{MCNameTH}", _dtmachine.BoxRepairMachine[0].MCNameTH);
        body = body.Replace("{CommentUser}", _dtmachine.BoxRepairMachine[0].CommentUser);

        return body;
    }

    public string ENRepairSenduseClosejobBody(DataMachine _dtmachine, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ENRepair_senduser.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{RECode}", _dtmachine.BoxRepairMachine[0].RECode);
        body = body.Replace("{CommentAdmin}", _dtmachine.BoxRepairMachine[0].CommentAdmin);
        body = body.Replace("{NameAdminClose}", _dtmachine.BoxRepairMachine[0].NameAdminClose);
        body = body.Replace("{status_name}", _dtmachine.BoxRepairMachine[0].status_name);
        body = body.Replace("{link}", link);

        return body;
    }

    public string ENRepairAdminClosejobBody(DataMachine _dtmachine)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ENRepair_adminclosejob.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{RECode}", _dtmachine.BoxRepairMachine[0].RECode);
        body = body.Replace("{CommentApprove}", _dtmachine.BoxRepairMachine[0].CommentApprove);
        body = body.Replace("{NameUser}", _dtmachine.BoxRepairMachine[0].NameUser);
        body = body.Replace("{status_name1}", _dtmachine.BoxRepairMachine[0].status_name1);

        return body;
    }

    #endregion

    #region api room_booking : booking
    //sent mail for create room booking
    public string SentMailRoomBookingCreate(rbk_u0_roombooking_detail u0_roombooking, string link_system_roombooking)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_rbk_roombooking_create.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u0_document_idx}", u0_roombooking.u0_document_idx.ToString());
        body = body.Replace("{emp_name_th}", u0_roombooking.emp_name_th);
        body = body.Replace("{org_name_th}", u0_roombooking.org_name_th);
        body = body.Replace("{dept_name_th}", u0_roombooking.dept_name_th);
        body = body.Replace("{sec_name_th}", u0_roombooking.sec_name_th);
        body = body.Replace("{emp_email}", u0_roombooking.emp_email);
        body = body.Replace("{place_name}", u0_roombooking.place_name);
        body = body.Replace("{room_name_th}", u0_roombooking.room_name_th);
        body = body.Replace("{create_date}", u0_roombooking.create_date);
        body = body.Replace("{date_start}", u0_roombooking.date_start);
        body = body.Replace("{time_start}", u0_roombooking.time_start);
        body = body.Replace("{date_end}", u0_roombooking.date_end);
        body = body.Replace("{time_end}", u0_roombooking.time_end);
        body = body.Replace("{emp_email_sentto_hr}", u0_roombooking.emp_email_sentto_hr);
        body = body.Replace("{type_booking_name}", u0_roombooking.type_booking_name);
        body = body.Replace("{topic_booking}", u0_roombooking.topic_booking);
        body = body.Replace("{topic_booking}", u0_roombooking.topic_booking);

        if (u0_roombooking.detail_foodbooking != "")
        {
            body = body.Replace("{detail_foodbooking}", u0_roombooking.detail_foodbooking);
            body = body.Replace("{food_detail_room}", "-");
        }
        else
        {

            string[] _list_FoodDetailRoom = u0_roombooking.food_detail_room.Split('|');
            string Createlist_RoomDetail = "";
            foreach (string rCreate_RoomDetail in _list_FoodDetailRoom)
            {
                if (rCreate_RoomDetail != String.Empty)
                {
                    Createlist_RoomDetail += rCreate_RoomDetail + "<br />";
                }
            }

            body = body.Replace("{food_detail_room}", Createlist_RoomDetail);
            body = body.Replace("{detail_foodbooking}", "-");
        }
        //body = body.Replace("{test_detail_name}", Createlist);
        //body = body.Replace("{create_date}", u0Document.create_date);
        body = body.Replace("{link}", link_system_roombooking);






        return body;
    }

    //sent mail for approve room booking
    public string SentMailRoomBookingHRApprove(rbk_u0_roombooking_detail u0_roombooking_approve, string link_system_roombooking_approve)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_rbk_roombooking_hrapprove.html")))
        {
            body = reader.ReadToEnd();
        }


        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u0_document_idx}", u0_roombooking_approve.u0_document_idx.ToString());
        body = body.Replace("{emp_name_th}", u0_roombooking_approve.emp_name_th);
        body = body.Replace("{org_name_th}", u0_roombooking_approve.org_name_th);
        body = body.Replace("{dept_name_th}", u0_roombooking_approve.dept_name_th);
        body = body.Replace("{sec_name_th}", u0_roombooking_approve.sec_name_th);
        body = body.Replace("{emp_email}", u0_roombooking_approve.emp_email);
        body = body.Replace("{place_name}", u0_roombooking_approve.place_name);
        body = body.Replace("{room_name_th}", u0_roombooking_approve.room_name_th);
        body = body.Replace("{create_date}", u0_roombooking_approve.create_date);
        body = body.Replace("{date_start}", u0_roombooking_approve.date_start);
        body = body.Replace("{time_start}", u0_roombooking_approve.time_start);
        body = body.Replace("{date_end}", u0_roombooking_approve.date_end);
        body = body.Replace("{time_end}", u0_roombooking_approve.time_end);
        body = body.Replace("{emp_email_sentto_hr}", u0_roombooking_approve.emp_email_sentto_hr);
        body = body.Replace("{type_booking_name}", u0_roombooking_approve.type_booking_name);
        body = body.Replace("{topic_booking}", u0_roombooking_approve.topic_booking);
        //body = body.Replace("{topic_booking}", u0_roombooking_approve.topic_booking);
        body = body.Replace("{decision_name_hr}", u0_roombooking_approve.decision_name_hr);
        body = body.Replace("{comment}", u0_roombooking_approve.comment);
        body = body.Replace("{link}", link_system_roombooking_approve);

        if (u0_roombooking_approve.m0_actor_idx == 2 && u0_roombooking_approve.staidx == 3)
        {
            IFormatProvider culture_ = new CultureInfo("en-US", true);
            //DateTime DateNow = DateTime.ParseExact(DateTime.Now.ToString("yyyyMMddTHHmmssZ"), "yyyyMMddTHHmmssZ", CultureInfo.InvariantCulture);
            string DateNow_start = u0_roombooking_approve.date_start + " " + u0_roombooking_approve.time_start;//"12/02/2019 10:00";
            string DateNow_end = u0_roombooking_approve.date_end + " " + u0_roombooking_approve.time_end;//"12/02/2019 10:00";

            DateTime Datestart = DateTime.ParseExact(DateNow_start, "dd/MM/yyyy HH:mm", culture_);
            DateTime Dateend = DateTime.ParseExact(DateNow_end, "dd/MM/yyyy HH:mm", culture_);
            //litDebug1.Text = TimeZoneInfo.ConvertTimeToUtc(Datestarting).ToString("yyyyMMddTHHmmssZ");

            string _url = "http://www.google.com/calendar/event";
            string TEMPLATE = "TEMPLATE";
            //string VIEW = "VIEW";
            string text = u0_roombooking_approve.topic_booking;
            string dates = TimeZoneInfo.ConvertTimeToUtc(Datestart).ToString("yyyyMMddTHHmmssZ") + "/" + TimeZoneInfo.ConvertTimeToUtc(Dateend).ToString("yyyyMMddTHHmmssZ");//"20131124T010000Z/20131124T020000Z";
            string details = u0_roombooking_approve.detail_booking;
            string location = u0_roombooking_approve.room_name_th + " " + u0_roombooking_approve.place_name;
            ////string add = "waraporn.teoy@gmail.com,msirirak4441@gmail.com ";

            string url_ = _url + "?action=" + TEMPLATE + "&text=" + text + "&dates=" + dates + "&details=" + details + "&location=" + location;
            ////string url_ = _url + "?action=" + TEMPLATE + "&text=" + text + "&dates=" + dates + "&details=" + details + "&location=" + location + "&add=" + add;
            //string url_view = _url + "?action=" + VIEW + "&text=" + text + "&dates=" + dates + "&details=" + details + "&location=" + location;


            body = body.Replace("{linkAddGoogle}", url_);
            body = body.Replace("{NameLink}", "AddGoogleCalendar");
            //body = body.Replace("{ViewGoogle}", url_view);
        }
        else
        {
            body = body.Replace("{linkAddGoogle}", "");
            body = body.Replace("{NameLink}", "no data");
        }



        return body;
    }

    //sent mail for approve room booking
    public string SentMailRoomBookingHRApproveReplace(rbk_u1_roombooking_detail u1_roombooking_approve, string link_system_roombooking_approvereplace)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_rbk_roombooking_replace.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u0_document_idx}", u1_roombooking_approve.u0_document_idx.ToString());
        body = body.Replace("{emp_name_th}", u1_roombooking_approve.emp_name_th);
        body = body.Replace("{org_name_th}", u1_roombooking_approve.org_name_th);
        body = body.Replace("{dept_name_th}", u1_roombooking_approve.dept_name_th);
        body = body.Replace("{sec_name_th}", u1_roombooking_approve.sec_name_th);
        body = body.Replace("{emp_email}", u1_roombooking_approve.emp_email);
        body = body.Replace("{place_name}", u1_roombooking_approve.place_name);
        body = body.Replace("{room_name_th}", u1_roombooking_approve.room_name_th);
        body = body.Replace("{create_date}", u1_roombooking_approve.create_date);
        body = body.Replace("{date_start}", u1_roombooking_approve.date_start);
        body = body.Replace("{time_start}", u1_roombooking_approve.time_start);
        body = body.Replace("{date_end}", u1_roombooking_approve.date_end);
        body = body.Replace("{time_end}", u1_roombooking_approve.time_end);
        body = body.Replace("{emp_email_sentto_hr}", u1_roombooking_approve.emp_email_sentto_hr);
        body = body.Replace("{type_booking_name}", u1_roombooking_approve.type_booking_name);
        body = body.Replace("{topic_booking}", u1_roombooking_approve.topic_booking);
        body = body.Replace("{topic_booking}", u1_roombooking_approve.topic_booking);
        body = body.Replace("{decision_name_hr}", u1_roombooking_approve.decision_name_hr);
        body = body.Replace("{comment}", u1_roombooking_approve.comment);

        //body = body.Replace("{test_detail_name}", Createlist);
        //body = body.Replace("{create_date}", u0Document.create_date);
        body = body.Replace("{link}", link_system_roombooking_approvereplace);

        return body;
    }

    //sent mail for edit room booking
    public string SentMailRoomBookingHREdit(rbk_u0_roombooking_detail u0_roombooking_hredit, string link_system_roombooking_hredit)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_rbk_roombooking_hredit.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u0_document_idx}", u0_roombooking_hredit.u0_document_idx.ToString());
        body = body.Replace("{emp_name_th}", u0_roombooking_hredit.emp_name_th);
        body = body.Replace("{org_name_th}", u0_roombooking_hredit.org_name_th);
        body = body.Replace("{dept_name_th}", u0_roombooking_hredit.dept_name_th);
        body = body.Replace("{sec_name_th}", u0_roombooking_hredit.sec_name_th);
        body = body.Replace("{emp_email}", u0_roombooking_hredit.emp_email);
        body = body.Replace("{place_name}", u0_roombooking_hredit.place_name);
        body = body.Replace("{room_name_th}", u0_roombooking_hredit.room_name_th);
        body = body.Replace("{create_date}", u0_roombooking_hredit.create_date);
        body = body.Replace("{date_start}", u0_roombooking_hredit.date_start);
        body = body.Replace("{time_start}", u0_roombooking_hredit.time_start);
        body = body.Replace("{date_end}", u0_roombooking_hredit.date_end);
        body = body.Replace("{time_end}", u0_roombooking_hredit.time_end);
        body = body.Replace("{emp_email_sentto_hr}", u0_roombooking_hredit.emp_email_sentto_hr);
        body = body.Replace("{type_booking_name}", u0_roombooking_hredit.type_booking_name);
        body = body.Replace("{topic_booking}", u0_roombooking_hredit.topic_booking);
        body = body.Replace("{decision_name_hr}", u0_roombooking_hredit.decision_name_hr);
        body = body.Replace("{comment}", u0_roombooking_hredit.comment);

        //body = body.Replace("{test_detail_name}", Createlist);
        //body = body.Replace("{create_date}", u0Document.create_date);
        body = body.Replace("{link}", link_system_roombooking_hredit);


        IFormatProvider culture_ = new CultureInfo("en-US", true);
        //DateTime DateNow = DateTime.ParseExact(DateTime.Now.ToString("yyyyMMddTHHmmssZ"), "yyyyMMddTHHmmssZ", CultureInfo.InvariantCulture);
        string DateNow_start = u0_roombooking_hredit.date_start + " " + u0_roombooking_hredit.time_start;//"12/02/2019 10:00";
        string DateNow_end = u0_roombooking_hredit.date_end + " " + u0_roombooking_hredit.time_end;//"12/02/2019 10:00";

        DateTime Datestart = DateTime.ParseExact(DateNow_start, "dd/MM/yyyy HH:mm", culture_);
        DateTime Dateend = DateTime.ParseExact(DateNow_end, "dd/MM/yyyy HH:mm", culture_);
        //litDebug1.Text = TimeZoneInfo.ConvertTimeToUtc(Datestarting).ToString("yyyyMMddTHHmmssZ");

        string _url = "http://www.google.com/calendar/event";
        //string TEMPLATE = "TEMPLATE";
        string VIEW = "VIEW";
        string text = u0_roombooking_hredit.topic_booking;
        string dates = TimeZoneInfo.ConvertTimeToUtc(Datestart).ToString("yyyyMMddTHHmmssZ") + "/" + TimeZoneInfo.ConvertTimeToUtc(Dateend).ToString("yyyyMMddTHHmmssZ");//"20131124T010000Z/20131124T020000Z";
        string details = u0_roombooking_hredit.detail_booking;
        string location = u0_roombooking_hredit.room_name_th + " " + u0_roombooking_hredit.place_name;

        //string url_ = _url + "?action=" + TEMPLATE + "&text=" + text + "&dates=" + dates + "&details=" + details + "&location=" + location;
        string url_view = _url + "?action=" + VIEW + "&text=" + text + "&dates=" + dates + "&details=" + details + "&location=" + location;


        //body = body.Replace("{linkAddGoogle}", url_);
        body = body.Replace("{ViewGoogle}", url_view);
        return body;
    }

    //sent mail for user edit room booking
    public string SentMailRoomBookingUserEdit(rbk_u0_roombooking_detail u0_roombooking_useredit, string link_system_roombooking_useredit)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_rbk_roombooking_useredit.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u0_document_idx}", u0_roombooking_useredit.u0_document_idx.ToString());
        body = body.Replace("{emp_name_th}", u0_roombooking_useredit.emp_name_th);
        body = body.Replace("{org_name_th}", u0_roombooking_useredit.org_name_th);
        body = body.Replace("{dept_name_th}", u0_roombooking_useredit.dept_name_th);
        body = body.Replace("{sec_name_th}", u0_roombooking_useredit.sec_name_th);
        body = body.Replace("{emp_email}", u0_roombooking_useredit.emp_email);
        body = body.Replace("{place_name}", u0_roombooking_useredit.place_name);
        body = body.Replace("{room_name_th}", u0_roombooking_useredit.room_name_th);
        body = body.Replace("{create_date}", u0_roombooking_useredit.create_date);
        body = body.Replace("{date_start}", u0_roombooking_useredit.date_start);
        body = body.Replace("{time_start}", u0_roombooking_useredit.time_start);
        body = body.Replace("{date_end}", u0_roombooking_useredit.date_end);
        body = body.Replace("{time_end}", u0_roombooking_useredit.time_end);
        body = body.Replace("{emp_email_sentto_hr}", u0_roombooking_useredit.emp_email_sentto_hr);
        body = body.Replace("{type_booking_name}", u0_roombooking_useredit.type_booking_name);
        body = body.Replace("{topic_booking}", u0_roombooking_useredit.topic_booking);
        body = body.Replace("{topic_booking}", u0_roombooking_useredit.topic_booking);

        //body = body.Replace("{test_detail_name}", Createlist);
        //body = body.Replace("{create_date}", u0Document.create_date);
        body = body.Replace("{link}", link_system_roombooking_useredit);






        return body;
    }

    //sent mail for user edit room booking
    public string SentMailRoomBookingUserCancel(rbk_u0_roombooking_detail u0_roombooking_usercancel, string link_system_roombooking_usercancal)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_rbk_roombooking_usercancel.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u0_document_idx}", u0_roombooking_usercancel.u0_document_idx.ToString());
        body = body.Replace("{emp_name_th}", u0_roombooking_usercancel.emp_name_th);
        body = body.Replace("{org_name_th}", u0_roombooking_usercancel.org_name_th);
        body = body.Replace("{dept_name_th}", u0_roombooking_usercancel.dept_name_th);
        body = body.Replace("{sec_name_th}", u0_roombooking_usercancel.sec_name_th);
        body = body.Replace("{emp_email}", u0_roombooking_usercancel.emp_email);
        body = body.Replace("{place_name}", u0_roombooking_usercancel.place_name);
        body = body.Replace("{room_name_th}", u0_roombooking_usercancel.room_name_th);
        body = body.Replace("{create_date}", u0_roombooking_usercancel.create_date);
        body = body.Replace("{date_start}", u0_roombooking_usercancel.date_start);
        body = body.Replace("{time_start}", u0_roombooking_usercancel.time_start);
        body = body.Replace("{date_end}", u0_roombooking_usercancel.date_end);
        body = body.Replace("{time_end}", u0_roombooking_usercancel.time_end);
        body = body.Replace("{emp_email_sentto_hr}", u0_roombooking_usercancel.emp_email_sentto_hr);
        body = body.Replace("{type_booking_name}", u0_roombooking_usercancel.type_booking_name);
        body = body.Replace("{topic_booking}", u0_roombooking_usercancel.topic_booking);
        body = body.Replace("{status_name}", u0_roombooking_usercancel.status_name);

        //body = body.Replace("{test_detail_name}", Createlist);
        //body = body.Replace("{create_date}", u0Document.create_date);
        body = body.Replace("{link}", link_system_roombooking_usercancal);

        IFormatProvider culture_ = new CultureInfo("en-US", true);
        //DateTime DateNow = DateTime.ParseExact(DateTime.Now.ToString("yyyyMMddTHHmmssZ"), "yyyyMMddTHHmmssZ", CultureInfo.InvariantCulture);
        string DateNow_start = u0_roombooking_usercancel.date_start + " " + u0_roombooking_usercancel.time_start;//"12/02/2019 10:00";
        string DateNow_end = u0_roombooking_usercancel.date_end + " " + u0_roombooking_usercancel.time_end;//"12/02/2019 10:00";

        DateTime Datestart = DateTime.ParseExact(DateNow_start, "dd/MM/yyyy HH:mm", culture_);
        DateTime Dateend = DateTime.ParseExact(DateNow_end, "dd/MM/yyyy HH:mm", culture_);
        //litDebug1.Text = TimeZoneInfo.ConvertTimeToUtc(Datestarting).ToString("yyyyMMddTHHmmssZ");

        string _url = "http://www.google.com/calendar/event";
        //string TEMPLATE = "TEMPLATE";
        string VIEW = "VIEW";
        string text = u0_roombooking_usercancel.topic_booking;
        string dates = TimeZoneInfo.ConvertTimeToUtc(Datestart).ToString("yyyyMMddTHHmmssZ") + "/" + TimeZoneInfo.ConvertTimeToUtc(Dateend).ToString("yyyyMMddTHHmmssZ");//"20131124T010000Z/20131124T020000Z";
        string details = u0_roombooking_usercancel.detail_booking;
        string location = u0_roombooking_usercancel.room_name_th + " " + u0_roombooking_usercancel.place_name;

        //string url_ = _url + "?action=" + TEMPLATE + "&text=" + text + "&dates=" + dates + "&details=" + details + "&location=" + location;
        string url_view = _url + "?action=" + VIEW + "&text=" + text + "&dates=" + dates + "&details=" + details + "&location=" + location;


        //body = body.Replace("{linkAddGoogle}", url_);
        body = body.Replace("{ViewGoogle}", url_view);


        return body;
    }

    #endregion

    #region api car_booking : booking
    //sent mail for create 
    public string SentMailCarBookingCreate(cbk_u0_document_detail u0_carbooking, string link_system_carbooking)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_cbk_create.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u0_document_idx}", u0_carbooking.u0_document_idx.ToString());
        body = body.Replace("{emp_name_th}", u0_carbooking.emp_name_th);
        body = body.Replace("{org_name_th}", u0_carbooking.org_name_th);
        body = body.Replace("{dept_name_th}", u0_carbooking.dept_name_th);
        body = body.Replace("{create_date}", u0_carbooking.create_date);
        body = body.Replace("{detailtype_car_name}", u0_carbooking.detailtype_car_name);
        body = body.Replace("{type_booking_name}", u0_carbooking.type_booking_name);
        body = body.Replace("{place_name}", u0_carbooking.place_name);
        body = body.Replace("{place_name_other}", u0_carbooking.place_name_other);
        body = body.Replace("{count_travel}", u0_carbooking.count_travel);
        body = body.Replace("{date_start}", u0_carbooking.date_start);
        body = body.Replace("{time_start}", u0_carbooking.time_start);
        body = body.Replace("{date_end}", u0_carbooking.date_end);
        body = body.Replace("{time_end}", u0_carbooking.time_end);
        body = body.Replace("{link}", link_system_carbooking);

        return body;
    }

    //sent mail for head user approve
    public string SentMailCarBookingHeaduserApprove(cbk_u0_document_detail u0_carbooking_headapprove, string link_system_carbooking)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_cbk_headuser_approve.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u0_document_idx}", u0_carbooking_headapprove.u0_document_idx.ToString());
        body = body.Replace("{emp_name_th}", u0_carbooking_headapprove.emp_name_th);
        body = body.Replace("{org_name_th}", u0_carbooking_headapprove.org_name_th);
        body = body.Replace("{dept_name_th}", u0_carbooking_headapprove.dept_name_th);
        body = body.Replace("{create_date}", u0_carbooking_headapprove.create_date);
        body = body.Replace("{detailtype_car_name}", u0_carbooking_headapprove.detailtype_car_name);
        body = body.Replace("{type_booking_name}", u0_carbooking_headapprove.type_booking_name);
        body = body.Replace("{place_name}", u0_carbooking_headapprove.place_name);
        body = body.Replace("{place_name_other}", u0_carbooking_headapprove.place_name_other);
        body = body.Replace("{count_travel}", u0_carbooking_headapprove.count_travel);
        body = body.Replace("{date_start}", u0_carbooking_headapprove.date_start);
        body = body.Replace("{time_start}", u0_carbooking_headapprove.time_start);
        body = body.Replace("{date_end}", u0_carbooking_headapprove.date_end);
        body = body.Replace("{time_end}", u0_carbooking_headapprove.time_end);
        body = body.Replace("{comment}", u0_carbooking_headapprove.comment);
        body = body.Replace("{decision_approve}", u0_carbooking_headapprove.decision_approve);
        body = body.Replace("{emp_email_sentto_headuser}", u0_carbooking_headapprove.emp_email_sentto_headuser);
        body = body.Replace("{link}", link_system_carbooking);

        return body;
    }

    //sent mail for hr approve
    public string SentMailCarBookingHRApprove(cbk_u0_document_detail u0_carbooking_hrapprove, string link_system_carbooking)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_cbk_hr_approve.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u0_document_idx}", u0_carbooking_hrapprove.u0_document_idx.ToString());
        body = body.Replace("{emp_name_th}", u0_carbooking_hrapprove.emp_name_th);
        body = body.Replace("{org_name_th}", u0_carbooking_hrapprove.org_name_th);
        body = body.Replace("{dept_name_th}", u0_carbooking_hrapprove.dept_name_th);
        body = body.Replace("{create_date}", u0_carbooking_hrapprove.create_date);
        body = body.Replace("{detailtype_car_name}", u0_carbooking_hrapprove.detailtype_car_name);
        body = body.Replace("{type_booking_name}", u0_carbooking_hrapprove.type_booking_name);
        body = body.Replace("{place_name}", u0_carbooking_hrapprove.place_name);
        body = body.Replace("{place_name_other}", u0_carbooking_hrapprove.place_name_other);
        body = body.Replace("{count_travel}", u0_carbooking_hrapprove.count_travel);
        body = body.Replace("{date_start}", u0_carbooking_hrapprove.date_start);
        body = body.Replace("{time_start}", u0_carbooking_hrapprove.time_start);
        body = body.Replace("{date_end}", u0_carbooking_hrapprove.date_end);
        body = body.Replace("{time_end}", u0_carbooking_hrapprove.time_end);
        body = body.Replace("{comment}", u0_carbooking_hrapprove.comment);
        body = body.Replace("{decision_approve}", u0_carbooking_hrapprove.decision_approve);
        body = body.Replace("{emp_email_sentto_headuser}", u0_carbooking_hrapprove.emp_email_sentto_headuser);
        body = body.Replace("{link}", link_system_carbooking);

        return body;
    }


    #endregion

    #region api employee register
    public string EmployeeRecruitCreateBody(data_employee _data_emp, string sum_key = "")
    {
        string body = string.Empty;
        //string env_ = "http://localhost/mas.taokaenoi.co.th";
        string env_ = "http://dev.taokaenoi.co.th";
        //string env_ = "http://mas.taokaenoi.co.th";
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_hr_employee_recruit.html")))
        {
            body = reader.ReadToEnd();
        }
        string VODstatus = "none";
        string Exstatus = "none";
        string Fulldetailstatus = "none";

        body = body.Replace("{emp_name_th}", _data_emp.employee_list[0].emp_name_th);
        body = body.Replace("{PosGroupNameTH}", _data_emp.employee_list[0].PosGroupNameTH);
        body = body.Replace("{salary}", _data_emp.employee_list[0].salary.ToString());
        body = body.Replace("{emp_mobile_no}", _data_emp.employee_list[0].emp_mobile_no);
        body = body.Replace("{emp_email}", _data_emp.employee_list[0].emp_email);
        body = body.Replace("{DetailProfile}", _data_emp.employee_list[0].DetailProfile);






        body = body.Replace("{rq_more}", env_ + "/hr-employee-register-v2?pageview=editview&idx=" + _data_emp.employee_list[0].emp_idx + "&sum=" + sum_key);



        return body;
    }


    public string EmployeeRegisterCreateBody(data_employee _data_emp)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_hr_employee_register.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{emp_name_th}", _data_emp.employee_list[0].emp_name_th);
        body = body.Replace("{PosGroupNameTH}", _data_emp.employee_list[0].PosGroupNameTH);
        body = body.Replace("{salary}", _data_emp.employee_list[0].salary.ToString());
        body = body.Replace("{emp_mobile_no}", _data_emp.employee_list[0].emp_mobile_no);
        body = body.Replace("{emp_email}", _data_emp.employee_list[0].emp_email);
        body = body.Replace("{DetailProfile}", _data_emp.employee_list[0].DetailProfile);

        return body;
    }

    public string EmployeeRegisterSendQuiz(sendemailquiz sendmail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_hr_employee_register_quiz.html")))
        {
            body = reader.ReadToEnd();
        }

        string[] _listTopic = sendmail.topic_name.Split(',');
        string _topiclist = "";
        foreach (string topic_namelist in _listTopic)
        {
            if (topic_namelist != String.Empty)
            {
                _topiclist += topic_namelist + "<br />";
            }
        }

        body = body.Replace("{emp_name_th}", sendmail.emp_name_th);
        body = body.Replace("{P_Name}", sendmail.P_Name);
        body = body.Replace("{datedoing}", sendmail.datedoing);
        body = body.Replace("{P_Location}", sendmail.P_Location);
        body = body.Replace("{P_Description}", sendmail.P_Description);
        body = body.Replace("{StatusDoc}", sendmail.StatusDoc);
        body = body.Replace("{topic_name}", _topiclist);

        return body;
    }

    public string EmployeeRegisterSendQuiz_PersonalResponsibility(sendemailquiz sendmail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_hr_employee_register_quiz_person.html")))
        {
            body = reader.ReadToEnd();
        }

        string[] _listTopic = sendmail.topic_name.Split(',');
        string _topiclist = "";
        foreach (string topic_namelist in _listTopic)
        {
            if (topic_namelist != String.Empty)
            {
                _topiclist += topic_namelist + "<br />";
            }
        }

        body = body.Replace("{emp_name_th}", sendmail.emp_name_th);
        body = body.Replace("{P_Name}", sendmail.P_Name);
        body = body.Replace("{datedoing}", sendmail.datedoing);
        body = body.Replace("{P_Location}", sendmail.P_Location);
        body = body.Replace("{P_Description}", sendmail.P_Description);
        body = body.Replace("{StatusDoc}", sendmail.StatusDoc);
        body = body.Replace("{topic_name}", _topiclist);
        body = body.Replace("{statusinterview}", sendmail.statusinterview);
        body = body.Replace("{adminname}", sendmail.adminname);
        body = body.Replace("{determine}", sendmail.determine);

        return body;
    }
    #endregion api employee register

    #region api resetpassword
    public string HRResetPasswordCreateBody(data_employee _data_emp)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_hr_resetpassword.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{identity_card}", _data_emp.employee_list[0].identity_card);
        body = body.Replace("{emp_email}", _data_emp.employee_list[0].emp_email);

        return body;
    }
    #endregion api resetpassword

    #region api over_overtime : overtime
    //sent mail for create 
    public string SentMailCreateOTMonth(ovt_u0_document_detail u0_overtime, string link_system)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ovt_create.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u0_document_idx}", u0_overtime.u0_doc_idx.ToString());
        body = body.Replace("{emp_name_th}", u0_overtime.emp_name_th);
        body = body.Replace("{org_name_th}", u0_overtime.org_name_th);
        body = body.Replace("{dept_name_th}", u0_overtime.dept_name_th);
        body = body.Replace("{month_ot_name}", u0_overtime.month_ot_name);
        body = body.Replace("{create_date}", u0_overtime.create_date);
        body = body.Replace("{time_create_date}", u0_overtime.time_create_date);
        body = body.Replace("{link}", link_system);

        return body;
    }

    public string SentMailHeadApproveOTMonth(ovt_u0_document_detail u0_overtime, string link_system)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ovt_headapprove.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u0_document_idx}", u0_overtime.u0_doc_idx.ToString());
        body = body.Replace("{emp_name_th}", u0_overtime.emp_name_th);
        body = body.Replace("{org_name_th}", u0_overtime.org_name_th);
        body = body.Replace("{dept_name_th}", u0_overtime.dept_name_th);
        body = body.Replace("{month_ot_name}", u0_overtime.month_ot_name);
        body = body.Replace("{create_date}", u0_overtime.create_date);
        body = body.Replace("{time_create_date}", u0_overtime.time_create_date);

        body = body.Replace("{emp_name_th_head}", u0_overtime.emp_name_th_head);
        body = body.Replace("{org_name_th_head}", u0_overtime.org_name_th_head);
        body = body.Replace("{dept_name_th_head}", u0_overtime.dept_name_th_head);
        body = body.Replace("{current_decision}", u0_overtime.current_decision);
        body = body.Replace("{comment_head}", u0_overtime.comment_head);
        body = body.Replace("{link}", link_system);

        return body;
    }

    public string SentMailEditOTMonth(ovt_u0_document_detail u0_overtime, string link_system)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ovt_useredit.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u0_document_idx}", u0_overtime.u0_doc_idx.ToString());
        body = body.Replace("{emp_name_th}", u0_overtime.emp_name_th);
        body = body.Replace("{org_name_th}", u0_overtime.org_name_th);
        body = body.Replace("{dept_name_th}", u0_overtime.dept_name_th);
        body = body.Replace("{month_ot_name}", u0_overtime.month_ot_name);
        body = body.Replace("{create_date}", u0_overtime.create_date);
        body = body.Replace("{time_create_date}", u0_overtime.time_create_date);
        body = body.Replace("{current_decision}", u0_overtime.current_decision);
        body = body.Replace("{link}", link_system);

        return body;
    }

    public string SentMailCreateOTDay(ovt_u1_document_detail u1_doc_overtime, string link_system)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ovt_createotday.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u1_doc_idx}", u1_doc_overtime.u1_doc_idx.ToString());
        body = body.Replace("{emp_name_th_create}", u1_doc_overtime.emp_name_th_create);
        body = body.Replace("{sec_name_th_create}", u1_doc_overtime.sec_name_th_create);
        body = body.Replace("{create_date_create}", u1_doc_overtime.create_date_create);
        body = body.Replace("{time_create_date_create}", u1_doc_overtime.time_create_date_create);

        body = body.Replace("{emp_name_th}", u1_doc_overtime.emp_name_th);
        body = body.Replace("{dept_name_th}", u1_doc_overtime.dept_name_th);
        body = body.Replace("{sec_name_th}", u1_doc_overtime.sec_name_th);
        body = body.Replace("{current_status}", u1_doc_overtime.current_status);



        body = body.Replace("{link}", link_system);

        return body;
    }

    public string SentMailApproveOTDay(ovt_u1_document_detail u1_doc_overtime, string link_system)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ovt_approveotday.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u1_doc_idx}", u1_doc_overtime.u1_doc_idx.ToString());
        body = body.Replace("{emp_name_th_approve}", u1_doc_overtime.emp_name_th_approve);
        body = body.Replace("{sec_name_th_approve}", u1_doc_overtime.sec_name_th_approve);
        body = body.Replace("{create_date_approve}", u1_doc_overtime.create_date_approve);
        body = body.Replace("{time_create_date_approve}", u1_doc_overtime.time_create_date_approve);

        body = body.Replace("{emp_name_th}", u1_doc_overtime.emp_name_th);
        body = body.Replace("{dept_name_th}", u1_doc_overtime.dept_name_th);
        body = body.Replace("{sec_name_th}", u1_doc_overtime.sec_name_th);

        body = body.Replace("{current_status}", u1_doc_overtime.current_status);

        body = body.Replace("{link}", link_system);

        return body;
    }

    public string SentMailCreateOTShiftRotate(ovt_u1doc_rotate_detail u1_doc_overtime, string link_system)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ovt_create_shiftrotate.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u1_doc_idx}", u1_doc_overtime.u1_doc_idx.ToString());
        body = body.Replace("{emp_name_th_create}", u1_doc_overtime.emp_name_th_create);
        body = body.Replace("{sec_name_th_create}", u1_doc_overtime.sec_name_th_create);
        body = body.Replace("{create_date}", u1_doc_overtime.create_date);
        body = body.Replace("{time_create_date}", u1_doc_overtime.time_create_date);
        body = body.Replace("{remark_admin}", u1_doc_overtime.remark_admin);
        body = body.Replace("{emp_name_th}", u1_doc_overtime.emp_name_th);
        body = body.Replace("{dept_name_th}", u1_doc_overtime.dept_name_th);
        body = body.Replace("{sec_name_th}", u1_doc_overtime.sec_name_th);
        body = body.Replace("{current_status}", u1_doc_overtime.current_status);



        body = body.Replace("{link}", link_system);

        return body;
    }

    public string SentMailApproveOTShiftRotate(ovt_u1doc_rotate_detail u1_doc_overtime, string link_system)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_ovt_approve_shiftrotate.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{u1_doc_idx}", u1_doc_overtime.u1_doc_idx.ToString());
        body = body.Replace("{emp_name_th_approve}", u1_doc_overtime.emp_name_th_approve);
        body = body.Replace("{sec_name_th_approve}", u1_doc_overtime.sec_name_th_approve);
        body = body.Replace("{create_date_approve}", u1_doc_overtime.create_date_approve);
        body = body.Replace("{time_approve}", u1_doc_overtime.time_approve);

        body = body.Replace("{emp_name_th}", u1_doc_overtime.emp_name_th);
        body = body.Replace("{dept_name_th}", u1_doc_overtime.dept_name_th);
        body = body.Replace("{sec_name_th}", u1_doc_overtime.sec_name_th);

        body = body.Replace("{current_status}", u1_doc_overtime.current_status);

        body = body.Replace("{link}", link_system);

        return body;
    }

    #endregion

    #region api_resignation
    public string ResignationCreateBody(M0_ReasonExit sendmail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_resignation_create.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{OrgNameTH}", sendmail.OrgNameTH);
        body = body.Replace("{DeptNameTH}", sendmail.DeptNameTH);
        body = body.Replace("{SecNameTH}", sendmail.SecNameTH);
        body = body.Replace("{StatusDoc}", sendmail.StatusDoc);
        body = body.Replace("{reason_resign}", sendmail.reason_resign);
        body = body.Replace("{comment_m0rsidx}", sendmail.comment_m0rsidx);
        body = body.Replace("{reason_exit}", sendmail.reason_exit);
        body = body.Replace("{type_menu}", sendmail.type_menu);
        body = body.Replace("{FullNameTH}", sendmail.FullNameTH);
        body = body.Replace("{date_resign}", sendmail.date_resign);
        body = body.Replace("{Email}", sendmail.Email + "," + sendmail.Email_Admin);

        return body;
    }

    public string ResignationExchangeBody(M0_ReasonExit sendmail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_resignation_exchange.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{OrgNameTH}", sendmail.OrgNameTH);
        body = body.Replace("{DeptNameTH}", sendmail.DeptNameTH);
        body = body.Replace("{SecNameTH}", sendmail.SecNameTH);
        body = body.Replace("{StatusDoc_Ex}", sendmail.StatusDoc_Ex);
        body = body.Replace("{FullNameTH}", sendmail.FullNameTH);

        return body;
    }
    #endregion

    #region api_price-reference
    public string PriceRef_BuySpecialBody(u0memo_reference sendmail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_priceref_buyspecial.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{OrgNameTH}", sendmail.OrgNameTH);
        body = body.Replace("{DeptNameTH}", sendmail.DeptNameTH);
        body = body.Replace("{SecNameTH}", sendmail.SecNameTH);
        body = body.Replace("{PosNameTH}", sendmail.PosNameTH);
        body = body.Replace("{StatusDoc}", sendmail.StatusDoc);
        body = body.Replace("{doccode}", sendmail.doccode);
        body = body.Replace("{type_memo}", sendmail.type_memo);
        body = body.Replace("{name_its_type}", sendmail.name_its_type);
        body = body.Replace("{FullNameTH}", sendmail.FullNameTH);
        body = body.Replace("{DateStart}", sendmail.DateStart);
        body = body.Replace("{Email}", sendmail.Email);

        return body;
    }

    #endregion

    #region api_hr_man_power
    public string ManpowerCreateBody(U0_DocumentDetail sendmail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_hr_manpower_create.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{OrgNameTH}", sendmail.OrgNameTH);
        body = body.Replace("{DeptNameTH}", sendmail.DeptNameTH);
        body = body.Replace("{SecNameTH}", sendmail.SecNameTH);
        body = body.Replace("{StatusDoc}", sendmail.StatusDoc);
        body = body.Replace("{EmpTypeName}", sendmail.EmpTypeName);
        body = body.Replace("{LocName}", sendmail.LocName);
        body = body.Replace("{reason_comment}", sendmail.reason_comment);
        body = body.Replace("{type_list}", sendmail.type_list);
        body = body.Replace("{FullNameTH}", sendmail.FullNameTH);
        body = body.Replace("{type_man}", sendmail.type_man);
        body = body.Replace("{createdate}", sendmail.createdate);
        body = body.Replace("{Email}", sendmail.Email + "," + sendmail.Email_Admin);

        return body;
    }

    #endregion

    #region api_hr_tpm_formresult
    public string TmpFormResultAlertBody(tpmu0_DocFormDetail sendmail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_hr_tmp_formresult_alert.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{emp_name_th}", sendmail.emp_name_th);
        body = body.Replace("{approve1}", sendmail.approve1);
        body = body.Replace("{approve2}", sendmail.approve2);
        body = body.Replace("{StatusDoc}", sendmail.StatusDoc);
        body = body.Replace("{sum_mine_competency}", sendmail.sum_mine_competency);
        body = body.Replace("{sum_mine_core_value}", sendmail.sum_mine_core_value);
        body = body.Replace("{sum_head_competency}", sendmail.sum_head_competency);
        body = body.Replace("{sum_head_core_value}", sendmail.sum_head_core_value);
        body = body.Replace("{StatusDoc}", sendmail.StatusDoc);
        body = body.Replace("{form_name}", sendmail.form_name);
        body = body.Replace("{Email}", sendmail.emp_email + "," + sendmail.email1 + "," + sendmail.email2 + "," + sendmail.email1 + "," + sendmail.email_hr);

        return body;
    }


    #endregion

    #region api law : trademarks contract
    //sent mail for create 
    public string SentMailCreateLawTMContract(tcm_u0_document_detail u0_document_tcmcreate, string link_system)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_trademarkcontract_create.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{docrequest_code}", u0_document_tcmcreate.docrequest_code.ToString());
        body = body.Replace("{emp_code}", u0_document_tcmcreate.emp_code);
        body = body.Replace("{emp_name_en}", u0_document_tcmcreate.emp_name_en);
        body = body.Replace("{org_name_en}", u0_document_tcmcreate.org_name_en);
        body = body.Replace("{dept_name_en}", u0_document_tcmcreate.dept_name_en);
        body = body.Replace("{type_name_en}", u0_document_tcmcreate.type_name_en);
        body = body.Replace("{jobtype_name_en}", u0_document_tcmcreate.jobtype_name_en);
        body = body.Replace("{current_status}", u0_document_tcmcreate.current_status);
        body = body.Replace("{emp_email_headuser}", u0_document_tcmcreate.emp_email_headuser);
        body = body.Replace("{emp_email}", u0_document_tcmcreate.emp_email);
        //body = body.Replace("{org_name_th}", u0_document_tcmcreate.org_name_th);
        //
        //body = body.Replace("{month_ot_name}", u0_document_tcmcreate.month_ot_name);
        //body = body.Replace("{create_date}", u0_document_tcmcreate.create_date);
        //body = body.Replace("{time_create_date}", u0_document_tcmcreate.time_create_date);
        body = body.Replace("{link}", link_system);

        return body;
    }

    //sent mail for approve 
    public string SentMailApproveLawTMContract(tcm_u0_document_detail u0_document_tcmapprove, string link_system)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_trademarkcontract_approve.html")))
        {
            body = reader.ReadToEnd();
        }

        //string[] _listTestDetailCreate = u0Document.test_detail_name.Split('|');
        //string Createlist = "";
        //foreach (string rCreate in _listTestDetailCreate)
        //{
        //    if (rCreate != String.Empty)
        //    {
        //        Createlist += rCreate + "<br />";
        //    }
        //}

        body = body.Replace("{docrequest_code}", u0_document_tcmapprove.docrequest_code.ToString());
        body = body.Replace("{emp_code}", u0_document_tcmapprove.emp_code);
        body = body.Replace("{emp_name_en}", u0_document_tcmapprove.emp_name_en);
        body = body.Replace("{current_status}", u0_document_tcmapprove.current_status);

        body = body.Replace("{emp_email_headuser}", u0_document_tcmapprove.emp_email_headuser);
        body = body.Replace("{emp_email}", u0_document_tcmapprove.emp_email);
        body = body.Replace("{emp_email_mglaw}", u0_document_tcmapprove.emp_email_mglaw);
        body = body.Replace("{emp_email_lawofficer}", u0_document_tcmapprove.emp_email_lawofficer);

        body = body.Replace("{emp_code_approve}", u0_document_tcmapprove.emp_code_approve);
        body = body.Replace("{emp_name_en_approve}", u0_document_tcmapprove.emp_name_en_approve);


        //body = body.Replace("{org_name_th}", u0_document_tcmcreate.org_name_th);
        //
        //body = body.Replace("{month_ot_name}", u0_document_tcmcreate.month_ot_name);
        //body = body.Replace("{create_date}", u0_document_tcmcreate.create_date);
        //body = body.Replace("{time_create_date}", u0_document_tcmcreate.time_create_date);
        body = body.Replace("{link}", link_system);

        return body;
    }
    #endregion

    #region api law : trademarks promise
    //sent mail for create 
    public string SentMailCreateLawTMPromise(pm_u0_document_detail u0_document_tcmcreate, string link_system)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_lawpromise_create.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{document_code}", u0_document_tcmcreate.document_code.ToString());
        body = body.Replace("{emp_code}", u0_document_tcmcreate.emp_code);
        body = body.Replace("{emp_name_th}", u0_document_tcmcreate.emp_name_th);
        body = body.Replace("{org_name_th}", u0_document_tcmcreate.org_name_th);
        body = body.Replace("{dept_name_th}", u0_document_tcmcreate.dept_name_th);
        body = body.Replace("{type_document_en}", u0_document_tcmcreate.type_document_en);
        body = body.Replace("{subtype_document_en}", u0_document_tcmcreate.subtype_document_en);
        body = body.Replace("{current_status}", u0_document_tcmcreate.current_status);
        body = body.Replace("{emp_email_headuser}", u0_document_tcmcreate.emp_email_headuser);
        body = body.Replace("{emp_email}", u0_document_tcmcreate.emp_email);
        //body = body.Replace("{org_name_th}", u0_document_tcmcreate.org_name_th);
        //
        //body = body.Replace("{month_ot_name}", u0_document_tcmcreate.month_ot_name);
        //body = body.Replace("{create_date}", u0_document_tcmcreate.create_date);
        //body = body.Replace("{time_create_date}", u0_document_tcmcreate.time_create_date);
        body = body.Replace("{link}", link_system);

        return body;
    }

    public string SentMailApproveLawTMPromise(pm_u0_document_detail u0_document_tpmapprove, string link_system)
    {
        string body = string.Empty;

        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_lawpromise_approve.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{document_code}", u0_document_tpmapprove.document_code.ToString());
        body = body.Replace("{emp_code_approve}", u0_document_tpmapprove.emp_code_approve);
        body = body.Replace("{emp_name_th_approve}", u0_document_tpmapprove.emp_name_th_approve);

        body = body.Replace("{type_document_en}", u0_document_tpmapprove.type_document_en);
        body = body.Replace("{subtype_document_en}", u0_document_tpmapprove.subtype_document_en);
        body = body.Replace("{current_status}", u0_document_tpmapprove.current_status);
        body = body.Replace("{emp_email_approve}", u0_document_tpmapprove.emp_email_approve);
        body = body.Replace("{emp_email_mglaw}", u0_document_tpmapprove.emp_email_mglaw);
        body = body.Replace("{emp_email}", u0_document_tpmapprove.emp_email);
        body = body.Replace("{comment}", u0_document_tpmapprove.comment);



        body = body.Replace("{link}", link_system);

        return body;
    }
    #endregion

    
    #region api qmr-problem report
    public string QMR_ProblemCreateBody(U0_ProblemDocument sendmail, int unidx, int doc_decision, string emp_email)
    {
        string path_create = "";

        if (unidx == 1 || unidx == 2 || unidx == 3 || unidx == 7 || (unidx == 9 && doc_decision == 3 || unidx == 9 && doc_decision == 6 || unidx == 9 && doc_decision == 8))
        {
            path_create = "~/template/template_qmr_problem_create.html";
        }
        else if (unidx == 4 || unidx == 9 && doc_decision == 2)
        {
            path_create = "~/template/template_qmr_problem_qa.html";
        }
        else if (unidx == 6)
        {
            path_create = "~/template/template_qmr_problem_supplier.html";
        }
        else if (unidx == 9 && doc_decision == 7 || unidx == 8)
        {
            path_create = "~/template/template_qmr_problem_complete.html";
        }


        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath(path_create)))
        {
            body = reader.ReadToEnd();
        }

        body = body.Replace("{dept_name_th}", sendmail.dept_name_th);
        body = body.Replace("{sec_name_th}", sendmail.sec_name_th);
        body = body.Replace("{emp_name_th}", sendmail.emp_name_th);
        body = body.Replace("{doc_code}", sendmail.doc_code);
        body = body.Replace("{LocName}", sendmail.LocName);
        body = body.Replace("{BuildingName}", sendmail.BuildingName);
        body = body.Replace("{production_name}", sendmail.production_name);
        body = body.Replace("{typeproduct_name}", sendmail.typeproduct_name);
        body = body.Replace("{problem_name}", sendmail.problem_name);
        body = body.Replace("{detail_remark}", sendmail.detail_remark);
        body = body.Replace("{StatusDoc}", sendmail.StatusDoc);
        body = body.Replace("{comment_sup}", sendmail.comment_sup);
        body = body.Replace("{detail_rollback}", sendmail.detail_rollback);
        body = body.Replace("{sup_name}", sendmail.sup_name);
        body = body.Replace("{user_login}", sendmail.userlogin);
        body = body.Replace("{admin_name}", sendmail.admin_name);
        body = body.Replace("{batchnumber}", sendmail.batchnumber);
        body = body.Replace("{matname}", sendmail.matname);
        body = body.Replace("{mat_no}", sendmail.mat_no.ToString());
        body = body.Replace("{dategetproduct}", sendmail.dategetproduct);
        body = body.Replace("{qty_get}", sendmail.qty_get);
        body = body.Replace("{unit_get}", sendmail.unit_name);
        body = body.Replace("{qty_rollback}", sendmail.qty_rollback.ToString());
        body = body.Replace("{unit_name_rollback}", sendmail.unit_name_rollback);
        body = body.Replace("{po_no}", sendmail.po_no.ToString());
        body = body.Replace("{tv_no}", sendmail.tv_no.ToString());
        body = body.Replace("{type_problem_name}", sendmail.type_problem_name);
        body = body.Replace("{rollback_name}", sendmail.rollback_name);



        body = body.Replace("{emp_email}", emp_email);// sendmail.emp_email + "," + sendmail.Email_HeadUser + "," + sendmail.Email_QA + "," + sendmail.Email_HeadQA + "," + sendmail.email_sup + "," + sendmail.Email_PUR);

        return body;
    }

    public string QMR_ProblemRepasswordBody(U0_ProblemDocument sendmail, string emp_email)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_qmr_problem_resetpass.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{sup_name}", sendmail.sup_name);
        body = body.Replace("{user_login}", sendmail.userlogin);
        body = body.Replace("{admin_name}", sendmail.admin_name);
        body = body.Replace("{emp_email}", emp_email);// sendmail.emp_email + "," + sendmail.Email_HeadUser + "," + sendmail.Email_QA + "," + sendmail.Email_HeadQA + "," + sendmail.email_sup + "," + sendmail.Email_PUR);

        return body;
    }

    #endregion

    public string hr_plansaleApprove(u0_plansale_detail sendmail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_plansale_approve.html")))
        {
            body = reader.ReadToEnd();
        }

        /*string[] _listTopic = sendmail.topic_name.Split(',');
        string _topiclist = "";
        foreach (string topic_namelist in _listTopic)
        {
            if (topic_namelist != String.Empty)
            {
                _topiclist += topic_namelist + "<br />";
            }
        }*/

        body = body.Replace("{emp_code}", sendmail.emp_code);
        body = body.Replace("{emp_name_th}", sendmail.emp_name_th);
        body = body.Replace("{pos_name_th}", sendmail.pos_name_th);
        body = body.Replace("{TypeWork}", sendmail.TypeWork);
        body = body.Replace("{ps_startdate}", sendmail.ps_startdate);
        body = body.Replace("{ps_enddate}", sendmail.ps_enddate);
        body = body.Replace("{ps_comment}", sendmail.ps_comment);
        body = body.Replace("{node_description}", sendmail.node_description);

        return body;
    }

    public string hr_plansaleCreate(u0_plansale_detail sendmail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_plansale_create.html")))
        {
            body = reader.ReadToEnd();
        }

        string[] _listTopic = sendmail.node_description.Split(',');
        string _topiclist = "";
        foreach (string topic_namelist in _listTopic)
        {
            if (topic_namelist != String.Empty)
            {
                _topiclist += topic_namelist + "<br />";
            }
        }

        body = body.Replace("{emp_code}", sendmail.emp_code);
        body = body.Replace("{emp_name_th}", sendmail.emp_name_th);
        body = body.Replace("{pos_name_th}", sendmail.pos_name_th);
        body = body.Replace("{TypeWork}", sendmail.TypeWork);
        body = body.Replace("{ps_startdate}", sendmail.ps_startdate);
        body = body.Replace("{ps_enddate}", sendmail.ps_enddate);
        body = body.Replace("{ps_comment}", sendmail.ps_comment);
        body = body.Replace("{node_description}", _topiclist);

        return body;
    }

    #region api_payroll
    public string SlipDownloadBody(en_slip_data_detail_u0 _u0_detail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_payroll_slip_download.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{slip_password}", getDecrypted(_u0_detail.slip_password));
        return body;
    }
    #endregion   

    #region api elearning

    public string TrainingCreateCoursePlan(training_course sendmail)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_elerning_create.html")))
        {
            body = reader.ReadToEnd();
        }

        string datestart = sendmail.zdate_start.Replace(",", "<br />");

        body = body.Replace("{DeptNameTH}", sendmail.DeptNameTH);
        body = body.Replace("{CostNo}", sendmail.CostNo);
        body = body.Replace("{emp_name_th}", sendmail.emp_name_th);
        body = body.Replace("{training_course_no}", sendmail.training_course_no);
        body = body.Replace("{type_plan}", sendmail.type_plan);
        body = body.Replace("{zyear}", sendmail.zyear.ToString());
        body = body.Replace("{course_name}", sendmail.course_name);
        body = body.Replace("{target_group_name}", sendmail.target_group_name);
        body = body.Replace("{location_name}", sendmail.location_name);
        body = body.Replace("{date_start}", datestart);// sendmail.zdate_start);
        body = body.Replace("{StatusDoc}", sendmail.StatusDoc);
        body = body.Replace("{emp_email}", sendmail.emp_email);

        return body;
    }
    #endregion
}
