using System;
using System.Xml.Serialization;

public class data_menu
{
    public int return_code { get; set; }
    public string return_msg { get; set; }
    public m0_menu_detail[] m0_menu_list { get; set; }
    public m0_system_detail[] m0_system_list { get; set; }
    public m0_permissiontype_detail[] m0_permissiontype_list { get; set; }
    public m0_permission_detail[] m0_permission_list { get; set; }
    public m0_role_detail[] m0_role_list { get; set; }
}

[Serializable]
public class m0_menu_detail
{
    public int menu_idx { get; set; }
    public string menu_name_th { get; set; }
    public string menu_name_en { get; set; }
    public string menu_url { get; set; }
    public int menu_relation { get; set; }
    public int menu_order { get; set; }
    public string menu_icon { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int menu_status { get; set; }
    public int menu_level { get; set; }

    public int system_idx { get; set; }

   
    public string menu_name_thlist { get; set; }
    public string menu_name_thtest { get; set; }

}

[Serializable]
public class m0_system_detail
{
    public int system_idx { get; set; }
    public string system_name_th { get; set; }
    public string system_name_en { get; set; }  
    public int system_order { get; set; }   
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int system_status { get; set; }

    public int rdept_idx { get; set; }

}

[Serializable]
public class m0_permissiontype_detail
{  
    public int permissiontype_idx { get; set; } 
    public string permissiontype_name { get; set; }  
    public int cemp_idx { get; set; }  
    public string create_date { get; set; }   
    public string update_date { get; set; }
    public int permissiontype_status { get; set; }
    public int system_idx { get; set; }
    public int menu_idx { get; set; }
}

[Serializable]
public class m0_permission_detail
{

    public int permission_idx { get; set; }
    public string permission_name { get; set; }
    public int permission_type { get; set; }
    public int permissiontype_idx { get; set; }
    public string permissiontype_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int permissiontype_status { get; set; }
    public int system_idx { get; set; }
    public int menu_idx { get; set; }

    
    public int role_idx { get; set; }  
    public string role_name_th { get; set; } 
    public string role_name_en { get; set; }
}

[Serializable]
public class m0_role_detail
{

    public int role_idx { get; set; }
    public string role_name_th { get; set; }
    public string role_name_en { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int role_status { get; set; }

    public int permission_idx { get; set; }
    public int permission_type { get; set; }
    public int permissiontype_idx { get; set; }
    public string permissiontype_name { get; set; }
       
    public int system_idx { get; set; }
    public int menu_idx { get; set; }
    public string menu_name_thlist { get; set; }

    public string permission_idxstring { get; set; }

}