using System;
using System.Xml.Serialization;



/// <summary>
/// Summary description for data_purchase
/// </summary>

[Serializable]
[XmlRoot("data_jo")]
public class data_jo
{

    [XmlElement("return_code")]
    public int return_code { get; set; }

    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("return_mailmis")]
    public string return_mailmis { get; set; }

    //[XmlElement("to_m0_node")]
    //public int to_m0_node { get; set; }

    //[XmlElement("to_m0_actor")]
    //public int to_m0_actor { get; set; }

    //[XmlElement("id_statework")]
    //public int id_statework { get; set; }

    //[XmlElement("m0_actor_idx")]
    //public int m0_actor_idx { get; set; }

    //[XmlElement("m0_node_idx")]
    //public int m0_node_idx { get; set; }



    [XmlElement("job_order_overview_action")]
    public job_order_overview [] job_order_overview_action { get; set; }

    [XmlElement("jo_add_datalist_action")]
    public jo_add_datalist[] jo_add_datalist_action { get; set; }

    public jo_log[] jo_log_action { get; set; }
    public job_user[] job_user_action{ get; set; }
}

#region job_user
[Serializable]
public class job_user
{
    public int emp_login { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int jobgrade_level { get; set; }
    public int org_idx { get; set; }
}
#endregion

#region jo_log
    [Serializable]
public class jo_log
{
    public int jo_idx_ref { get; set; }
    public int l0_jo_idx { get; set; }
    public int l0_jo_idx_ref { get; set; }
    public int m0_actor_idx_lo { get; set; }
    public int m0_node_idx_lo { get; set; }
    public int id_statework_lo { get; set; }
    public int oakja { get; set; }
    public string from_table { get; set; }
    public string requried_old { get; set; }
    public string day_create { get; set; }
    public string u1_day_sent_time { get; set; }
    public int idx_create { get; set; }
    public int m0_actor_ref { get; set; }
    public string day_actor { get; set; }
    public int m0_node_ref { get; set; }
    public int m0_status_ref { get; set; }
    public string from_table_l0 { get; set; }
    public int idx_create_lo { get; set; }
    public string name_emp { get; set; }
    public int l0_codeemp { get; set; }
    public string AdminMain { get; set; }
    public string statenode_name_l0 { get; set; }
    public string node_name_l0 { get; set; }
    public string name_description { get; set; }
    public int condition { get; set; }
    public string comment { get; set; }

}

#endregion

#region jo_add_datalist
[Serializable]
public class jo_add_datalist
{

    public int u1_emp_sent { get; set; }
    public int u1_jo_idx { get; set; }
   
    public int u0_jo_idx_ref { get; set; }
    public int wait { get; set; }
    public int u1_status { get; set; }
    public int u1_ov_useonly { get; set; }
    public int u1_day_loop { get; set; }
    public string u1_day_receive { get; set; }
    public int u1_emp_receive { get; set; }

    public int jolog_m0_node { get; set; }
    public int jolog_m0_actor { get; set; }
    public int jolog_m0_status { get; set; }
    public int u1_codeemp { get; set; }
    
    public int u1_emp_sent_single { get; set; }
    public string createmp { get; set; }
    public string name_emp_look { get; set; }
    public string receive_emp_name { get; set; }
    public string u1_day_receive_time { get; set; }

    [XmlElement("u1_title")]
    public string u1_title { get; set; }

    [XmlElement("u1_day_sent")]
    public string u1_day_sent { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("day_created")]
    public string day_created { get; set; }

    [XmlElement("no_invoice")]
    public string no_invoice { get; set; }

    [XmlElement("statenode_name")]
    public string statenode_name { get; set; }

    [XmlElement("id_statework")]
    public int id_statework { get; set; }

    [XmlElement("name_description")]
    public string name_description { get; set; }

    [XmlElement("end_send")]
    public string end_send { get; set; }

    [XmlElement("sec_mis")]
    public int sec_mis { get; set; }

    [XmlElement("title_jo")]
    public string title_jo { get; set; }

    [XmlElement("require_jo")]
    public string require_jo { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }
}
#endregion

#region job_order_overview_action

[Serializable]
public class job_order_overview
{
    [XmlElement("u0_day_sent_time")]
    public string u0_day_sent_time { get; set; }

    [XmlElement("emp_dep_job")]
    public int emp_dep_job { get; set; }

    [XmlElement("emp_org_job")]
    public int emp_org_job { get; set; }

    [XmlElement("name_emp_look")]
    public string name_emp_look { get; set; }

    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("u0_jo_idx")]
    public int u0_jo_idx { get; set; }

    [XmlElement("no_invoice")]
    public string no_invoice { get; set; }

    [XmlElement("day_start")]
    public string day_start { get; set; }

    [XmlElement("day_finish")]
    public string day_finish { get; set; }

    [XmlElement("day_amount")]
    public string day_amount { get; set; }

    [XmlElement("title_jo")]
    public string title_jo { get; set; }

    [XmlElement("require_jo")]
    public string require_jo { get; set; }

    [XmlElement("id_statework")]
    public int id_statework { get; set; }

    [XmlElement("emp_id_creator")]
    public int emp_id_creator { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rsec_creator")]
    public int rsec_creator { get; set; }

    [XmlElement("day_created")]
    public string day_created { get; set; }

    [XmlElement("emp_id_receive")]
    public int emp_id_receive { get; set; }

    [XmlElement("day_receive")]
    public string day_receive { get; set; }

    [XmlElement("emp_id_sent")]
    public int emp_id_sent { get; set; }

    [XmlElement("day_sent")]
    public string day_sent { get; set; }

    [XmlElement("to_m0_node")]
    public int to_m0_node { get; set; }

    [XmlElement("to_m0_actor")]
    public int to_m0_actor { get; set; }

    [XmlElement("statenode_name")]
    public string statenode_name { get; set; }

    [XmlElement("u1_jo_idx")]
    public int u1_jo_idx { get; set; }

    [XmlElement("u1_day_sent")]
    public int u1_day_sent { get; set; }

    [XmlElement("u1_title")]
    public string u1_title { get; set; }

    [XmlElement("u0_jo_idx_ref")]
    public int u0_jo_idx_ref { get; set; }

    [XmlElement("name_actor")]
    public string name_actor { get; set; }

    [XmlElement("name_description")]
    public string name_description { get; set; }

    [XmlElement("node_status")]
    public int node_status { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("fromac")]
    public string fromac { get; set; }

    [XmlElement("jo_idx_ref")]
    public int jo_idx_ref { get; set; }

    [XmlElement("statenode_state")]
    public int statenode_state { get; set; }

    [XmlElement("AdminMain")]
    public string AdminMain { get; set; }

    [XmlElement("Organize")]
    public string Organize { get; set; }

    [XmlElement("org_idx")]
    public string org_idx { get; set; }

    [XmlElement("Deptname")]
    public string Deptname { get; set; }

    [XmlElement("Secname")]
    public string Secname { get; set; }

    [XmlElement("Posname")]
    public string Posname { get; set; }

    [XmlElement("Codeemp")]
    public int Codeemp { get; set; }

    [XmlElement("rdep_num")]
    public int rdep_num { get; set; }

    [XmlElement("org_idx_cre")]
    public int org_idx_cre { get; set; }

    [XmlElement("job_grade_cre")]
    public int job_grade_cre { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("costcenter_name")]
    public string costcenter_name { get; set; }

    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }

    [XmlElement("emp_mobile_no")]
    public string emp_mobile_no { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("requried_old")]
    public string requried_old { get; set; }

    [XmlElement("noinvoice_search")]
    public string noinvoice_search { get; set; }

    [XmlElement("day_search")]
    public string day_search { get; set; }

    [XmlElement("day_searchto")]
    public string day_searchto { get; set; }

    [XmlElement("if_date")]
    public int if_date { get; set; }

    [XmlElement("Org_search")]
    public int Org_search { get; set; }

    [XmlElement("Dept_search")]
    public int Dept_search { get; set; }

    [XmlElement("Sec_search")]
    public int Sec_search { get; set; }

    [XmlElement("ep_search")]
    public string ep_search { get; set; }

    [XmlElement("from_table")]
    public string from_table { get; set; }

    [XmlElement("nameActor")]
    public string nameActor { get; set; }

    [XmlElement("end_send")]
    public string end_send { get; set; }

    [XmlElement("day_created_time_log")]
    public string day_created_time_log { get; set; }

    [XmlElement("degree_idx")]
    public int degree_idx { get; set; }

    [XmlElement("sec_mis")]
    public int sec_mis { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

   

}

#endregion
