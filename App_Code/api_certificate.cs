﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for api_certificate
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_certificate : System.Web.Services.WebService
{

    public api_certificate()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    /*
    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }
    */
}
