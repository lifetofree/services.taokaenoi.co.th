﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for api_hr_news
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_hr_news : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_hr_news _data_hr_news = new data_hr_news();

    public api_hr_news()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetHrNews(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_hr_news", "service_hr_news_action", _xml_in, 10); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetHrNews(string jsonIn)
    {
        if (jsonIn != null)
        {
               // convert to xml
               _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_hr_news", "service_hr_news_action", _xml_in, 11); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
}