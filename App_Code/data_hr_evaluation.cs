using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_hr_evaluation")]
public class data_hr_evaluation
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }


    //transection
    [XmlElement("hr_perf_evalu_emp_action")]
    public hr_perf_evalu_emp[] hr_perf_evalu_emp_action { get; set; }
    [XmlElement("hr_perf_evalu_emp_monthly_action")]
    public hr_perf_evalu_emp_monthly[] hr_perf_evalu_emp_monthly_action { get; set; }

}

//transection
[Serializable]
public class hr_perf_evalu_emp
{
    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }
    [XmlElement("filter_keyword")]
    public string filter_keyword { get; set; }
    [XmlElement("zstatus")]
    public string zstatus { get; set; }
    [XmlElement("zmonth")]
    public int zmonth { get; set; }
    [XmlElement("zyear")]
    public int zyear { get; set; }
    [XmlElement("joblevel")]
    public int joblevel { get; set; }
    [XmlElement("idx")]
    public int idx { get; set; }
    [XmlElement("sysidx_admin")]
    public int sysidx_admin { get; set; }
    [XmlElement("admin_idx")]
    public int admin_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("u0idx")]
    public int u0idx { get; set; }
    [XmlElement("doccode")]
    public string doccode { get; set; }
    [XmlElement("docdate")]
    public string docdate { get; set; }
    [XmlElement("u0_status")]
    public int u0_status { get; set; }
    [XmlElement("u0_emp_idx")]
    public int u0_emp_idx { get; set; }
    [XmlElement("u0_org_idx")]
    public int u0_org_idx { get; set; }
    [XmlElement("u0_rdept_idx")]
    public int u0_rdept_idx { get; set; }
    [XmlElement("u0_rpos_idx")]
    public int u0_rpos_idx { get; set; }
    [XmlElement("u0_rsec_idx")]
    public int u0_rsec_idx { get; set; }
    [XmlElement("remark")]
    public string remark { get; set; }
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }
    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }
    [XmlElement("UEmpIDX")]
    public int UEmpIDX { get; set; }
    [XmlElement("UpdateDate")]
    public string UpdateDate { get; set; }


    [XmlElement("work_experience")]
    public string work_experience { get; set; }
    [XmlElement("u1idx")]
    public int u1idx { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("work_experience_qty")]
    public decimal work_experience_qty { get; set; }
    [XmlElement("ownership_qty")]
    public int ownership_qty { get; set; }
    [XmlElement("improvement_continuously_qty")]
    public int improvement_continuously_qty { get; set; }
    [XmlElement("commitment_qty")]
    public int commitment_qty { get; set; }
    [XmlElement("total_score")]
    public decimal total_score { get; set; }
    [XmlElement("missing_qty")]
    public int missing_qty { get; set; }
    [XmlElement("sick_Leave_qty")]
    public int sick_Leave_qty { get; set; }
    [XmlElement("call_back_qty")]
    public int call_back_qty { get; set; }
    [XmlElement("warning_notice_qty")]
    public int warning_notice_qty { get; set; }
    [XmlElement("work_break_qty")]
    public int work_break_qty { get; set; }
    [XmlElement("total_deduction")]
    public decimal total_deduction { get; set; }
    [XmlElement("deduction_qty")]
    public decimal deduction_qty { get; set; }
    [XmlElement("score_balance")]
    public decimal score_balance { get; set; }
    [XmlElement("score_assessment_qty")]
    public decimal score_assessment_qty { get; set; }
    [XmlElement("grade")]
    public string grade { get; set; }
    [XmlElement("grade_criteria")]
    public string grade_criteria { get; set; }
    [XmlElement("labor_cost")]
    public decimal labor_cost { get; set; }
    [XmlElement("bonus")]
    public decimal bonus { get; set; }
    [XmlElement("labor_cost_new")]
    public decimal labor_cost_new { get; set; }
    [XmlElement("bonus_day")]
    public decimal bonus_day { get; set; }
    [XmlElement("u1_status")]
    public int u1_status { get; set; }
    [XmlElement("supervisor1_idx")]
    public int supervisor1_idx { get; set; }
    [XmlElement("supervisor2_idx")]
    public int supervisor2_idx { get; set; }

    [XmlElement("u0_emp_name_th")]
    public string u0_emp_name_th { get; set; }
    [XmlElement("u0_pos_name_th")]
    public string u0_pos_name_th { get; set; }
    [XmlElement("u0_sec_name_th")]
    public string u0_sec_name_th { get; set; }
    [XmlElement("u0_org_name_th")]
    public string u0_org_name_th { get; set; }
    [XmlElement("zdocdate")]
    public string zdocdate { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }
    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }
    [XmlElement("costcenter_name")]
    public string costcenter_name { get; set; }
    [XmlElement("emp_probation_date")]
    public string emp_probation_date { get; set; }

    [XmlElement("u0_emp_code")]
    public string u0_emp_code { get; set; }
    [XmlElement("u0_dept_name_th")]
    public string u0_dept_name_th { get; set; }

    [XmlElement("m0idx")]
    public int m0idx { get; set; }
    [XmlElement("grade_code")]
    public string grade_code { get; set; }
    [XmlElement("count_grade")]
    public int count_grade { get; set; }
    [XmlElement("count_grade_criteria")]
    public int count_grade_criteria { get; set; }
    [XmlElement("description")]
    public string description { get; set; }
    [XmlElement("admonish_speech")]
    public int admonish_speech { get; set; }


    [XmlElement("total_score_before")]
    public decimal total_score_before { get; set; }
    [XmlElement("total_deduction_before")]
    public decimal total_deduction_before { get; set; }
    [XmlElement("score_balance_before")]
    public decimal score_balance_before { get; set; }
    [XmlElement("score_assessment_qty_before")]
    public decimal score_assessment_qty_before { get; set; }


}

[Serializable]
public class hr_perf_evalu_emp_monthly
{
    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }
    [XmlElement("filter_keyword")]
    public string filter_keyword { get; set; }
    [XmlElement("zstatus")]
    public string zstatus { get; set; }
    [XmlElement("zmonth")]
    public int zmonth { get; set; }
    [XmlElement("zyear")]
    public int zyear { get; set; }
    [XmlElement("joblevel")]
    public int joblevel { get; set; }
    [XmlElement("idx")]
    public int idx { get; set; }
    [XmlElement("sysidx_admin")]
    public int sysidx_admin { get; set; }
    [XmlElement("admin_idx")]
    public int admin_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("u0idx")]
    public int u0idx { get; set; }
    [XmlElement("doccode")]
    public string doccode { get; set; }
    [XmlElement("docdate")]
    public string docdate { get; set; }
    [XmlElement("u0_status")]
    public int u0_status { get; set; }
    [XmlElement("u0_emp_idx")]
    public int u0_emp_idx { get; set; }
    [XmlElement("u0_org_idx")]
    public int u0_org_idx { get; set; }
    [XmlElement("u0_rdept_idx")]
    public int u0_rdept_idx { get; set; }
    [XmlElement("u0_rpos_idx")]
    public int u0_rpos_idx { get; set; }
    [XmlElement("u0_rsec_idx")]
    public int u0_rsec_idx { get; set; }
    [XmlElement("remark")]
    public string remark { get; set; }
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }
    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }
    [XmlElement("UEmpIDX")]
    public int UEmpIDX { get; set; }
    [XmlElement("UpdateDate")]
    public string UpdateDate { get; set; }

    [XmlElement("work_experience")]
    public string work_experience { get; set; }
    [XmlElement("u1idx")]
    public int u1idx { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }



    [XmlElement("level_code")]
    public int level_code { get; set; }
    [XmlElement("group_work")]
    public int group_work { get; set; }
    [XmlElement("late_mm")]
    public int late_mm { get; set; }
    [XmlElement("Back_before_mm")]
    public int Back_before_mm { get; set; }
    [XmlElement("corp_kpi")]
    public decimal corp_kpi { get; set; }
    [XmlElement("tkn_value")]
    public int tkn_value { get; set; }
    [XmlElement("taokae_d")]
    public int taokae_d { get; set; }
    [XmlElement("absence")]
    public int absence { get; set; }
    [XmlElement("punishment1")]
    public int punishment1 { get; set; }
    [XmlElement("punishment2")]
    public int punishment2 { get; set; }
    [XmlElement("ass_self_ownership_qty")]
    public int ass_self_ownership_qty { get; set; }
    [XmlElement("ass_self_improvement_continuously_qty")]
    public int ass_self_improvement_continuously_qty { get; set; }
    [XmlElement("ass_self_commitment_qty")]
    public int ass_self_commitment_qty { get; set; }
    [XmlElement("ass_self_innovation")]
    public int ass_self_innovation { get; set; }
    [XmlElement("ass_self_achieveing_result")]
    public int ass_self_achieveing_result { get; set; }
    [XmlElement("ass_self_relation")]
    public int ass_self_relation { get; set; }
    [XmlElement("ass_self_job_knowledge")]
    public int ass_self_job_knowledge { get; set; }
    [XmlElement("ass_self_art_communication")]
    public int ass_self_art_communication { get; set; }
    [XmlElement("ass_self_leadership")]
    public int ass_self_leadership { get; set; }
    [XmlElement("ass_self_decision_making_planning")]
    public int ass_self_decision_making_planning { get; set; }
    [XmlElement("leader_ownership_qty")]
    public int leader_ownership_qty { get; set; }
    [XmlElement("leader_improvement_continuously_qty")]
    public int leader_improvement_continuously_qty { get; set; }
    [XmlElement("leader_commitment_qty")]
    public int leader_commitment_qty { get; set; }
    [XmlElement("leader_innovation")]
    public int leader_innovation { get; set; }
    [XmlElement("leader_achieveing_result")]
    public int leader_achieveing_result { get; set; }
    [XmlElement("leader_relation")]
    public int leader_relation { get; set; }
    [XmlElement("leader_job_knowledge")]
    public int leader_job_knowledge { get; set; }
    [XmlElement("leader_art_communication")]
    public int leader_art_communication { get; set; }
    [XmlElement("leader_leadership")]
    public int leader_leadership { get; set; }
    [XmlElement("leader_decision_making_planning")]
    public int leader_decision_making_planning { get; set; }
    [XmlElement("group_code")]
    public string group_code { get; set; }
    [XmlElement("missing_qty")]
    public decimal missing_qty { get; set; }
    [XmlElement("sick_Leave_qty_day")]
    public decimal sick_Leave_qty_day { get; set; }
    [XmlElement("Errandleave_day")]
    public decimal Errandleave_day { get; set; }
    [XmlElement("vacation_day")]
    public decimal vacation_day { get; set; }
    [XmlElement("workbreak_day")]
    public decimal workbreak_day { get; set; }
    [XmlElement("Maternityleave_day")]
    public decimal Maternityleave_day { get; set; }


    [XmlElement("u1_status")]
    public int u1_status { get; set; }
    [XmlElement("supervisor1_idx")]
    public int supervisor1_idx { get; set; }
    [XmlElement("supervisor2_idx")]
    public int supervisor2_idx { get; set; }

    [XmlElement("u0_emp_name_th")]
    public string u0_emp_name_th { get; set; }
    [XmlElement("u0_pos_name_th")]
    public string u0_pos_name_th { get; set; }
    [XmlElement("u0_sec_name_th")]
    public string u0_sec_name_th { get; set; }
    [XmlElement("u0_org_name_th")]
    public string u0_org_name_th { get; set; }
    [XmlElement("zdocdate")]
    public string zdocdate { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }
    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }
    [XmlElement("costcenter_name")]
    public string costcenter_name { get; set; }
    [XmlElement("emp_probation_date")]
    public string emp_probation_date { get; set; }
    [XmlElement("u0_emp_code")]
    public string u0_emp_code { get; set; }
    [XmlElement("u0_dept_name_th")]
    public string u0_dept_name_th { get; set; }


    [XmlElement("m0idx")]
    public int m0idx { get; set; }
    [XmlElement("grade_code")]
    public string grade_code { get; set; }
    [XmlElement("count_grade")]
    public int count_grade { get; set; }
    [XmlElement("count_grade_criteria")]
    public int count_grade_criteria { get; set; }

    [XmlElement("performane_name")]
    public string performane_name { get; set; }
    [XmlElement("m0corp_kpi")]
    public int m0corp_kpi { get; set; }
    [XmlElement("m0taokae_d")]
    public int m0taokae_d { get; set; }
    [XmlElement("m0tkn_value")]
    public int m0tkn_value { get; set; }
    [XmlElement("m0absence")]
    public int m0absence { get; set; }
    [XmlElement("punishment")]
    public int punishment { get; set; }
    [XmlElement("total")]
    public int total { get; set; }
    [XmlElement("description")]
    public string description { get; set; }

    [XmlElement("total_leader_tkn_value")]
    public int total_leader_tkn_value { get; set; }
    [XmlElement("total_leader_taokae_d")]
    public int total_leader_taokae_d { get; set; }
    [XmlElement("total_leader_ass_qty")]
    public int total_leader_ass_qty { get; set; }
    [XmlElement("total_deduction")]
    public int total_deduction { get; set; }

    [XmlElement("grade")]
    public string grade { get; set; }
    [XmlElement("grade_criteria")]
    public string grade_criteria { get; set; }
    [XmlElement("kpi")]
    public int kpi { get; set; }
    [XmlElement("score_balance")]
    public int score_balance { get; set; }


    [XmlElement("year_1")]
    public string year_1 { get; set; }
    [XmlElement("year_2")]
    public string year_2 { get; set; }
    [XmlElement("year_3")]
    public string year_3 { get; set; }

    [XmlElement("year_name_1")]
    public int year_name_1 { get; set; }
    [XmlElement("year_name_2")]
    public int year_name_2 { get; set; }
    [XmlElement("year_name_3")]
    public int year_name_3 { get; set; }


    [XmlElement("grade_total")]
    public int grade_total { get; set; }

    [XmlElement("grade_name")]
    public string grade_name { get; set; }
    [XmlElement("data_name")]
    public string data_name { get; set; }

    [XmlElement("item_no")]
    public int item_no { get; set; }
    
    [XmlElement("pms_name")]
    public string pms_name { get; set; }

    [XmlElement("total_deduction_before")]
    public decimal total_deduction_before { get; set; }
    [XmlElement("total_leader_ass_qty_before")]
    public decimal total_leader_ass_qty_before { get; set; }
    [XmlElement("total_leader_taokae_d_before")]
    public decimal total_leader_taokae_d_before { get; set; }
    [XmlElement("total_leader_tkn_value_before")]
    public decimal total_leader_tkn_value_before { get; set; }
    [XmlElement("score_balance_before")]
    public decimal score_balance_before { get; set; }
    [XmlElement("kpi_before")]
    public decimal kpi_before { get; set; }

    [XmlElement("total_qty")]
    public decimal total_qty { get; set; }

}

