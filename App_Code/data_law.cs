﻿using System;
using System.Xml.Serialization;
/// <summary>
/// Summary description for data_roombooking
/// </summary>
/// 

[Serializable]
[XmlRoot("data_law")]
public class data_law
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    //master
    public tcm_m0_type_detail[] tcm_m0_type_list { get; set; }
    public tcm_m0_subtype_detail[] tcm_m0_subtype_list { get; set; }
    public tcm_m0_country_detail[] tcm_m0_country_list { get; set; }
    public tcm_m0_decision_detail[] tcm_m0_decision_list { get; set; }
    public tcm_m0_owner_detail[] tcm_m0_owner_list { get; set; }
    public tcm_m0_Incorporated_detail[] tcm_m0_Incorporated_list { get; set; }
    public tcm_m0_trademark_status_detail[] tcm_m0_trademark_status_list { get; set; }
    public tcm_m0_trademark_substatus_detail[] tcm_m0_trademark_substatus_list { get; set; }
    public tcm_m0_recordsheet_status_detail[] tcm_m0_recordsheet_status_list { get; set; }
    public tcm_m0_local_language_detail[] tcm_m0_local_language_list { get; set; }
    public tcm_m0_actions_detail[] tcm_m0_actions_list { get; set; }
    public tcm_m0_typeofregistration_detail[] tcm_m0_typeofregistration_list { get; set; }
    public tcm_m0_typeofwork_detail[] tcm_m0_typeofwork_list { get; set; }
    public tcm_m0_actionsrecord_detail[] tcm_m0_actions_detail_list { get; set; }



    //document
    [XmlElement("tcm_u0_document_list")]
    public tcm_u0_document_detail[] tcm_u0_document_list { get; set; }

    [XmlElement("tcm_u1_document_list")]
    public tcm_u1_document_detail[] tcm_u1_document_list { get; set; }

    [XmlElement("tcm_u2_document_list")]
    public tcm_u2_document_detail[] tcm_u2_document_list { get; set; }

    [XmlElement("tcm_u3_document_list")]
    public tcm_u3_document_detail[] tcm_u3_document_list { get; set; }

    //document

    //record sheet
    [XmlElement("tcm_record_publication_list")]
    public tcm_record_publication_detail[] tcm_record_publication_list { get; set; }

    [XmlElement("tcm_record_abstract_list")]
    public tcm_record_abstract_detail[] tcm_record_abstract_list { get; set; }

    [XmlElement("tcm_record_actions_list")]
    public tcm_record_actions_detail[] tcm_record_actions_list { get; set; }

    [XmlElement("tcm_record_additional_list")]
    public tcm_record_additional_detail[] tcm_record_additional_list { get; set; }

    [XmlElement("tcm_record_agent_list")]
    public tcm_record_agent_detail[] tcm_record_agent_list { get; set; }

    [XmlElement("tcm_record_backgroud_list")]
    public tcm_record_backgroud_detail[] tcm_record_backgroud_list { get; set; }

    [XmlElement("tcm_record_claims_list")]
    public tcm_record_claims_detail[] tcm_record_claims_list { get; set; }

    [XmlElement("tcm_record_client_list")]
    public tcm_record_client_detail[] tcm_record_client_list { get; set; }

    [XmlElement("tcm_record_dependent_list")]
    public tcm_record_dependent_detail[] tcm_record_dependent_list { get; set; }

    [XmlElement("tcm_record_goods_list")]
    public tcm_record_goods_detail[] tcm_record_goods_list { get; set; }

    [XmlElement("tcm_record_priorities_list")]
    public tcm_record_priorities_detail[] tcm_record_priorities_list { get; set; }

    [XmlElement("tcm_record_summary_list")]
    public tcm_record_summary_detail[] tcm_record_summary_list { get; set; }

    [XmlElement("tcm_record_trademarkprofile_list")]
    public tcm_record_trademarkprofile_detail[] tcm_record_trademarkprofile_list { get; set; }

    [XmlElement("tcm_record_typeofwork_list")]
    public tcm_record_typeofwork_detail[] tcm_record_typeofwork_list { get; set; }

    [XmlElement("tcm_record_renewal_list")]
    public tcm_record_renewal_detail[] tcm_record_renewal_list { get; set; }

    [XmlElement("tcm_record_deadline_list")]
    public tcm_record_deadline_detail[] tcm_record_deadline_list { get; set; }

    [XmlElement("tcm_record_assignment_list")]
    public tcm_record_assignment_detail[] tcm_record_assignment_list { get; set; }

    [XmlElement("tcm_record_changeofname_list")]
    public tcm_record_changeofname_detail[] tcm_record_changeofname_list { get; set; }

    [XmlElement("tcm_record_description_list")]
    public tcm_record_description_detail[] tcm_record_description_list { get; set; }

    //record sheet

    //promise
    public pm_m0_typepromise_detail[] pm_m0_typepromise_detail_list { get; set; }
    public pm_m0_subtypepromise_detail[] pm_m0_subtypepromise_detail_list { get; set; }

    [XmlElement("pm_u0_document_detail_list")]
    public pm_u0_document_detail[] pm_u0_document_detail_list { get; set; }

    public pm_u1_document_detail[] pm_u1_document_detail_list { get; set; }

    public pm_u2_document_detail[] pm_u2_document_detail_list { get; set; }
}


[Serializable]
public class pm_u0_document_detail
{
    [XmlElement("u0_doc_idx")]
    public int u0_doc_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("type_document_idx")]
    public int type_document_idx { get; set; }

    [XmlElement("subtype_document_idx")]
    public int subtype_document_idx { get; set; }

    [XmlElement("doc_reference")]
    public string doc_reference { get; set; }

    [XmlElement("contract_parties")]
    public string contract_parties { get; set; }

    [XmlElement("value")]
    public string value { get; set; }

    [XmlElement("topic")]
    public string topic { get; set; }

    [XmlElement("detail")]
    public string detail { get; set; }

    [XmlElement("date_start")]
    public string date_start { get; set; }

    [XmlElement("date_end")]
    public string date_end { get; set; }

    [XmlElement("contact_person")]
    public string contact_person { get; set; }

    [XmlElement("phone_number")]
    public string phone_number { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("noidx")]
    public int noidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("current_artor")]
    public string current_artor { get; set; }

    [XmlElement("current_decision")]
    public string current_decision { get; set; }

    [XmlElement("current_status")]
    public string current_status { get; set; }

    [XmlElement("current_status_name")]
    public string current_status_name { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("type_document_en")]
    public string type_document_en { get; set; }

    [XmlElement("subtype_document_en")]
    public string subtype_document_en { get; set; }

    [XmlElement("count_waitApprove")]
    public int count_waitApprove { get; set; }

    [XmlElement("emp_email_headuser")]
    public string emp_email_headuser { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_email_mglaw")]
    public string emp_email_mglaw { get; set; }

    [XmlElement("emp_email_lawofficer")]
    public string emp_email_lawofficer { get; set; }

    [XmlElement("emp_email_approve")]
    public string emp_email_approve { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("emp_code_approve")]
    public string emp_code_approve { get; set; }

    [XmlElement("emp_name_th_approve")]
    public string emp_name_th_approve { get; set; }


}

public class pm_u1_document_detail
{
   
    public int u0_doc_idx { get; set; }
    public string document_code { get; set; }
    public string create_date { get; set; }
    public string time_create { get; set; }
    public string emp_name_th { get; set; }
    public string current_artor { get; set; }
    public string current_decision { get; set; }
    public string comment { get; set; }

}

public class pm_u2_document_detail
{

    public int u2_doc_idx { get; set; }
    public int u0_doc_idx { get; set; }
    public string document_code { get; set; }
    public string create_date { get; set; }
    public int emp_idx { get; set; }
    public int cemp_idx { get; set; }
    public string emp_code { get; set; }
    public string emp_name_th { get; set; }
    public string emp_name_en { get; set; }

}

public class pm_m0_typepromise_detail
{
   
    public int type_document_idx { get; set; }
    public string type_document_en { get; set; }
    public string type_document_th { get; set; }
    public int type_document_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }

}

public class pm_m0_subtypepromise_detail
{
    
    public int subtype_document_idx { get; set; }
    public string subtype_document_en { get; set; }
    public string subtype_document_th { get; set; }
    public int subtype_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }


}

[Serializable]
public class tcm_record_description_detail
{
    [XmlElement("description_idx")]
    public int description_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("description_detail")]
    public string description_detail { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }


}

[Serializable]
public class tcm_record_changeofname_detail
{
    [XmlElement("changeofname_idx")]
    public int changeofname_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("selected_idx")]
    public int selected_idx { get; set; }

    [XmlElement("selected_name")]
    public string selected_name { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("substatus")]
    public int substatus { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("trademark_status_en")]
    public string trademark_status_en { get; set; }

    [XmlElement("trademark_substatus_en")]
    public string trademark_substatus_en { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

}

[Serializable]
public class tcm_record_assignment_detail
{
    [XmlElement("assignment_idx")]
    public int assignment_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("step_idx")]
    public int step_idx { get; set; }

    [XmlElement("step_name")]
    public string step_name { get; set; }

    [XmlElement("selected_idx")]
    public int selected_idx { get; set; }

    [XmlElement("selected_name")]
    public string selected_name { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("substatus")]
    public int substatus { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("trademark_status_en")]
    public string trademark_status_en { get; set; }

    [XmlElement("trademark_substatus_en")]
    public string trademark_substatus_en { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

}

[Serializable]
public class tcm_record_deadline_detail
{
    [XmlElement("deadline_idx")]
    public int deadline_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("action_detail_idx")]
    public int action_detail_idx { get; set; }

    [XmlElement("action_detail_idx_value")]
    public string action_detail_idx_value { get; set; }

    [XmlElement("publication_idx")]
    public int publication_idx { get; set; }

    [XmlElement("deadline_date")]
    public string deadline_date { get; set; }

    [XmlElement("deadline_staus")]
    public int deadline_staus { get; set; }

    [XmlElement("deadline_stausname")]
    public string deadline_stausname { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("deadline_statusname")]
    public string deadline_statusname { get; set; }

    [XmlElement("action_detail_name")]
    public string action_detail_name { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

}

[Serializable]
public class tcm_record_renewal_detail
{
    [XmlElement("renewal_idx")]
    public int renewal_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("NextRenewalDue_date")]
    public string NextRenewalDue_date { get; set; }

    [XmlElement("Selected_value")]
    public int Selected_value { get; set; }

    [XmlElement("Selected_name")]
    public string Selected_name { get; set; }

    [XmlElement("Renewal_Status")]
    public int Renewal_Status { get; set; }

    [XmlElement("Renewal_SubStatus")]
    public int Renewal_SubStatus { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("trademark_status_en")]
    public string trademark_status_en { get; set; }

    [XmlElement("trademark_substatus_en")]
    public string trademark_substatus_en { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

}

public class tcm_m0_actionsrecord_detail
{
    public int action_detail_idx { get; set; }
    public string action_detail_name { get; set; }
    public int action_detail_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }

}

[Serializable]
public class tcm_record_typeofwork_detail
{
    [XmlElement("typework_idx")]
    public int typework_idx { get; set; }

    [XmlElement("typeofwork_idx")]
    public int typeofwork_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class tcm_record_trademarkprofile_detail
{
    [XmlElement("trademark_profile_idx")]
    public int trademark_profile_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("doc_language_idx")]
    public int doc_language_idx { get; set; }

    [XmlElement("Profile_Owner")]
    public string Profile_Owner { get; set; }

    [XmlElement("owner_language_idx")]
    public int owner_language_idx { get; set; }

    [XmlElement("Name")]
    public string Name { get; set; }

    [XmlElement("Profile_Client")]
    public string Profile_Client { get; set; }

    [XmlElement("client_language_idx")]
    public int client_language_idx { get; set; }

    [XmlElement("Translation")]
    public string Translation { get; set; }

    [XmlElement("Transliteration")]
    public string Transliteration { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("doc_language_en")]
    public string doc_language_en { get; set; }

    [XmlElement("owner_language_en")]
    public string owner_language_en { get; set; }

    [XmlElement("client_languag_en")]
    public string client_languag_en { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("country_name_en")]
    public string country_name_en { get; set; }

    [XmlElement("country_idx")]
    public int country_idx { get; set; }

}

[Serializable]
public class tcm_record_summary_detail
{
    [XmlElement("summary_idx")]
    public int summary_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("summary_detail")]
    public string summary_detail { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class tcm_record_priorities_detail
{
    [XmlElement("typeofpriority_idx")]
    public int typeofpriority_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("Type_of_priority")]
    public string Type_of_priority { get; set; }

    [XmlElement("Priority_Date")]
    public string Priority_Date { get; set; }

    [XmlElement("Priority_No")]
    public string Priority_No { get; set; }

    [XmlElement("country_idx")]
    public int country_idx { get; set; }

    [XmlElement("status_idx")]
    public int status_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("country_name_en")]
    public string country_name_en { get; set; }

    [XmlElement("status_name_en")]
    public string status_name_en { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

}

[Serializable]
public class tcm_record_goods_detail
{
    [XmlElement("goods_idx")]
    public int goods_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("IntClass")]
    public string IntClass { get; set; }

    [XmlElement("Goods_Detail")]
    public string Goods_Detail { get; set; }

    [XmlElement("language_idx")]
    public int language_idx { get; set; }

    [XmlElement("language_en")]
    public string language_en { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

}

[Serializable]
public class tcm_record_dependent_detail
{
    [XmlElement("dependent_idx")]
    public int dependent_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("Dependent_Registration")]
    public string Dependent_Registration { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class tcm_record_client_detail
{
    [XmlElement("client_idx")]
    public int client_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("Client_Detail")]
    public string Client_Detail { get; set; }

    [XmlElement("Client_Contact")]
    public string Client_Contact { get; set; }

    [XmlElement("Client_Reference")]
    public string Client_Reference { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("status_idx")]
    public int status_idx { get; set; }

    [XmlElement("status_name_en")]
    public string status_name_en { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

}

[Serializable]
public class tcm_record_claims_detail
{
    [XmlElement("claims_idx")]
    public int claims_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("claims_detail")]
    public string claims_detail { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class tcm_record_backgroud_detail
{
    [XmlElement("background_idx")]
    public int background_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("background_detail")]
    public string background_detail { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class tcm_record_agent_detail
{
    [XmlElement("agent_idx")]
    public int agent_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("Agent_Detail")]
    public string Agent_Detail { get; set; }

    [XmlElement("Agent_Contact")]
    public string Agent_Contact { get; set; }

    [XmlElement("Agent_Reference")]
    public string Agent_Reference { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("status_idx")]
    public int status_idx { get; set; }

    [XmlElement("status_name_en")]
    public string status_name_en { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

}

[Serializable]
public class tcm_record_additional_detail
{
    [XmlElement("additional_idx")]
    public int additional_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("Additional_Details")]
    public string Additional_Details { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class tcm_record_actions_detail
{
    [XmlElement("actiondetail_idx")]
    public int actiondetail_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("action_idx")]
    public int action_idx { get; set; }

    [XmlElement("Actions_Detail")]
    public string Actions_Detail { get; set; }

    [XmlElement("Owner_Detail")]
    public string Owner_Detail { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("deadline_date")]
    public string deadline_date { get; set; }

    [XmlElement("deadline_idx")]
    public int deadline_idx { get; set; }

    [XmlElement("action_name_en")]
    public string action_name_en { get; set; }

    [XmlElement("publication_idx")]
    public int publication_idx { get; set; }

}

[Serializable]
public class tcm_record_abstract_detail
{
    [XmlElement("abstract_idx")]
    public int abstract_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("abstract_detail")]
    public string abstract_detail { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class tcm_record_publication_detail
{
    [XmlElement("publication_idx")]
    public int publication_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("Publication_Date")]
    public string Publication_Date { get; set; }

    [XmlElement("NoticeofAllowance_Date")]
    public string NoticeofAllowance_Date { get; set; }

    [XmlElement("Journal_Volume")]
    public string Journal_Volume { get; set; }

    [XmlElement("PubRegDate_Date")]
    public string PubRegDate_Date { get; set; }

    [XmlElement("Registry_Reference")]
    public string Registry_Reference { get; set; }

    [XmlElement("FinalOfficeAction_Date")]
    public string FinalOfficeAction_Date { get; set; }

    [XmlElement("OfficeAction_Date")]
    public string OfficeAction_Date { get; set; }

    [XmlElement("Date_DeclarationFiled")]
    public string Date_DeclarationFiled { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

public class tcm_m0_typeofwork_detail
{

    public int typeofwork_idx { get; set; }
    public string typeofwork_en { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int typeofwork_status { get; set; }

}

public class tcm_m0_typeofregistration_detail
{
  
    public int typeofregistration_idx { get; set; }
    public string typeofregistration_en { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int typeofregistration_status { get; set; }

}

public class tcm_m0_actions_detail
{

    public int action_idx { get; set; }
    public int action_condition { get; set; }
    public string action_name_en { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int action_status { get; set; }

}

public class tcm_m0_local_language_detail
{ 
    public int language_idx { get; set; }
    public string language_en { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int status { get; set; }

}

public class tcm_m0_recordsheet_status_detail
{

    public int status_idx { get; set; }
    public string status_name_en { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int status { get; set; }
    public string status_name { get; set; }

}

public class tcm_m0_trademark_status_detail
{

    public int trademark_status_idx { get; set; }
    public string trademark_status_th { get; set; }
    public string trademark_status_en { get; set; }
    public int trademark_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int condition { get; set; }
    public int type_idx { get; set; }
    public string formset_detail { get; set; }
    public int trademark_substatus_idx { get; set; }
    public string trademark_substatus_en { get; set; }

}

public class tcm_m0_trademark_substatus_detail
{

    public int trademark_substatus_idx { get; set; }
    public int trademark_status_idx { get; set; }
    public string trademark_substatus_en { get; set; }
    public string formset_detail { get; set; }
    public string trademark_status_th { get; set; }
    public string trademark_status_en { get; set; }
    public int trademark_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int condition { get; set; }
    public int type_idx { get; set; }

}

public class tcm_m0_Incorporated_detail
{

    public int Incorporated_in_idx { get; set; }
    public string Incorporated_in_th { get; set; }
    public string Incorporated_in_en { get; set; }
    public int Incorporated_in_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int condition { get; set; }

}

public class tcm_m0_owner_detail
{

    public int owner_idx { get; set; }
    public string owner_name_en { get; set; }
    public string owner_name_th { get; set; }
    public string owner_address_en { get; set; }
    public int owner_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int condition { get; set; }

}

public class tcm_m0_decision_detail
{

    public int decision_idx { get; set; }
    public int noidx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int decision_status { get; set; }
    public string decision_name { get; set; }
    public string decision_des { get; set; }
    public int condition { get; set; }

}

public class tcm_m0_type_detail
{

    public int type_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int type_status { get; set; }
    public string type_name_th { get; set; }
    public string type_name_en { get; set; }

}

public class tcm_m0_subtype_detail
{

    public int subtype_idx { get; set; }
    public int type_idx { get; set; }
    public string type_name_en { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int subtype_status { get; set; }
    public string subtype_name_th { get; set; }
    public string subtype_name_en { get; set; }
    public int condition { get; set; }

}

public class tcm_m0_country_detail
{

    public int country_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int country_status { get; set; }
    public string country_name_th { get; set; }
    public string country_name_en { get; set; }

}

[Serializable]
public class tcm_u0_document_detail
{
    [XmlElement("u0_doc_idx")]
    public int u0_doc_idx { get; set; }

    [XmlElement("docrequest_code")]
    public string docrequest_code { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("subtype_idx")]
    public int subtype_idx { get; set; }

    [XmlElement("jobtype_idx")]
    public int jobtype_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("topic_name")]
    public string topic_name { get; set; }

    [XmlElement("doc_detail")]
    public string doc_detail { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("emphead_idx")]
    public int emphead_idx { get; set; }

    [XmlElement("empmg_idx")]
    public int empmg_idx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("noidx")]
    public int noidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("current_status")]
    public string current_status { get; set; }

    [XmlElement("jobtype_name_en")]
    public string jobtype_name_en { get; set; }

    [XmlElement("jobtype_name_th")]
    public string jobtype_name_th { get; set; }

    [XmlElement("type_name_en")]
    public string type_name_en { get; set; }

    [XmlElement("subtype_name_en")]
    public string subtype_name_en { get; set; }

    [XmlElement("country_idx_checkdetail")]
    public string country_idx_checkdetail { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("count_waitApprove")]
    public int count_waitApprove { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_email_headuser")]
    public string emp_email_headuser { get; set; }

    [XmlElement("emp_email_law")]
    public string emp_email_law { get; set; }

    [XmlElement("emp_email_mglaw")]
    public string emp_email_mglaw { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("emp_code_headuser")]
    public string emp_code_headuser { get; set; }

    [XmlElement("emp_name_th_headuser")]
    public string emp_name_th_headuser { get; set; }

    [XmlElement("dept_name_th_headuser")]
    public string dept_name_th_headuser { get; set; }

    [XmlElement("org_name_en_headuser")]
    public string org_name_en_headuser { get; set; }

    [XmlElement("dept_name_en_headuser")]
    public string dept_name_en_headuser { get; set; }

    [XmlElement("emp_name_en_headuser")]
    public string emp_name_en_headuser { get; set; }

    [XmlElement("emp_email_lawofficer")]
    public string emp_email_lawofficer { get; set; }

    [XmlElement("emp_code_approve")]
    public string emp_code_approve { get; set; }

    [XmlElement("emp_name_en_approve")]
    public string emp_name_en_approve { get; set; }

    [XmlElement("emp_email_approve")]
    public string emp_email_approve { get; set; }


}

[Serializable]
public class tcm_u1_document_detail
{
    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("u0_doc_idx")]
    public int u0_doc_idx { get; set; }

    [XmlElement("trademark_substatus_idx")]
    public int trademark_substatus_idx { get; set; }

    [XmlElement("doc_code")]
    public string doc_code { get; set; }

    [XmlElement("docrequest_code")]
    public string docrequest_code { get; set; }

    [XmlElement("country_idx")]
    public int country_idx { get; set; }

    [XmlElement("owner_idx")]
    public int owner_idx { get; set; }

    [XmlElement("incorporated_in_idx")]
    public int incorporated_in_idx { get; set; }

    [XmlElement("application_no")]
    public string application_no { get; set; }

    [XmlElement("application_date")]
    public string application_date { get; set; }

    [XmlElement("registration_no")]
    public string registration_no { get; set; }

    [XmlElement("registration_date")]
    public string registration_date { get; set; }

    [XmlElement("tradmark_status")]
    public int tradmark_status { get; set; }

    [XmlElement("trademark_status_en")]
    public string trademark_status_en { get; set; }

    [XmlElement("tradmark_substatus")]
    public int tradmark_substatus { get; set; }

    [XmlElement("trademark_substatus_en")]
    public string trademark_substatus_en { get; set; }

    [XmlElement("file_reference")]
    public string file_reference { get; set; }

    [XmlElement("recode_referense")]
    public string recode_referense { get; set; }

    [XmlElement("next_renewal_due")]
    public string next_renewal_due { get; set; }

    [XmlElement("type_of_registration_idx")]
    public int type_of_registration_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("subtype_idx")]
    public int subtype_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("noidx")]
    public int noidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("adminlaw_idx")]
    public int adminlaw_idx { get; set; }

    [XmlElement("dependent_registrations")]
    public string dependent_registrations { get; set; }

    [XmlElement("country_name_en")]
    public string country_name_en { get; set; }

    [XmlElement("type_name_en")]
    public string type_name_en { get; set; }

    [XmlElement("subtype_name_en")]
    public string subtype_name_en { get; set; }

    [XmlElement("current_status")]
    public string current_status { get; set; }

    [XmlElement("create_date_u0")]
    public string create_date_u0 { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("jobtype_name_en")]
    public string jobtype_name_en { get; set; }

    [XmlElement("jobtype_name_th")]
    public string jobtype_name_th { get; set; }

    [XmlElement("country_idx_checkdetail")]
    public string country_idx_checkdetail { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("count_waitApprove")]
    public int count_waitApprove { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("formview_set")]
    public string formview_set { get; set; }

    [XmlElement("dependent_registration")]
    public string dependent_registration { get; set; }

    [XmlElement("owner_name_en")]
    public string owner_name_en { get; set; }

    [XmlElement("owner_address_en")]
    public string owner_address_en { get; set; }

    [XmlElement("emp_name_en_head")]
    public string emp_name_en_head { get; set; }

    [XmlElement("emp_name_th_head")]
    public string emp_name_th_head { get; set; }

    [XmlElement("emp_name_en_mgemp")]
    public string emp_name_en_mgemp { get; set; }

    [XmlElement("emp_name_th_mgemp")]
    public string emp_name_th_mgemp { get; set; }


}

[Serializable]
public class tcm_u2_document_detail
{
    [XmlElement("u2_doc_idx")]
    public int u2_doc_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("u0_doc_idx")]
    public int u0_doc_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("current_artor")]
    public string current_artor { get; set; }

    [XmlElement("current_decision")]
    public string current_decision { get; set; }

    [XmlElement("current_status")]
    public string current_status { get; set; }

    [XmlElement("current_status_name")]
    public string current_status_name { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("date_start")]
    public string date_start { get; set; }

    [XmlElement("date_end")]
    public string date_end { get; set; }

    [XmlElement("time_start")]
    public string time_start { get; set; }

    [XmlElement("time_end")]
    public string time_end { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("count_waitApprove")]
    public int count_waitApprove { get; set; }

    [XmlElement("status_booking")]
    public string status_booking { get; set; }

    [XmlElement("date_time_chekbutton")]
    public string date_time_chekbutton { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_email_sentto_hr")]
    public string emp_email_sentto_hr { get; set; }

    [XmlElement("decision_name_hr")]
    public string decision_name_hr { get; set; }

}

[Serializable]
public class tcm_u3_document_detail
{
    [XmlElement("u3_doc_idx")]
    public int u3_doc_idx { get; set; }

    [XmlElement("u2_doc_idx")]
    public int u2_doc_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("u0_doc_idx")]
    public int u0_doc_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("pos_name_en")]
    public string pos_name_en { get; set; }

    [XmlElement("count_waitApprove")]
    public int count_waitApprove { get; set; }

    [XmlElement("status_booking")]
    public string status_booking { get; set; }

    [XmlElement("date_time_chekbutton")]
    public string date_time_chekbutton { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

}