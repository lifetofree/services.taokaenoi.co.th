using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_conference")]
public class data_conference
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public string conf_mode { get; set; }

    public conf_user_detail_u0[] conf_user_list_u0 { get; set; }
    public search_conf_data_detail[] search_conf_data_list { get; set; }
}

[Serializable]
public class conf_user_detail_u0
{
    public int u0_idx { get; set; }
    public string user_name { get; set; }
    public string user_firstname { get; set; }
    public string user_lastname { get; set; }
    public string user_mail { get; set; }
    public string user_company { get; set; }
    public string user_pass_real { get; set; }
    public string user_pass { get; set; }
    public int conf_type { get; set; }
    public int conf_round { get; set; }
    public int user_flag { get; set; }
    public int user_status { get; set; }
    public string stamp_date { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

[Serializable]
public class search_conf_data_detail
{
    public string s_u0_idx { get; set; }
    public string s_user_name { get; set; }
    public string s_user_firstname { get; set; }
    public string s_user_lastname { get; set; }
    public string s_user_mail { get; set; }
    public string s_user_company { get; set; }
    public string s_user_pass_real { get; set; }
    public string s_user_pass { get; set; }
    public string s_conf_type { get; set; }
    public string s_conf_round { get; set; }
    public string s_user_flag { get; set; }
    public string s_user_status { get; set; }
    public string s_stamp_date { get; set; }
}