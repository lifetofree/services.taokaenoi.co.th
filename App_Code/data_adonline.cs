using System;
using System.Xml.Serialization;

public class data_adonline
{
    public int return_code { get; set; }
    public string return_msg { get; set; }
    public string return_perm { get; set; }
    public string return_temp { get; set; }
    public string on_table_u1 { get; set; }
    public string return_ad { get; set; }
    public string return_user { get; set; }
    public string return_typeemail { get; set; }
    public string return_mailprivate { get; set; }
    public string return_mailcenter { get; set; }
    public string return_vpnsap { get; set; }
    public string return_vpnbi { get; set; }
    public string return_vpnexpress { get; set; }
    public string return_mailreplace { get; set; }
    public permission_policy[] ad_m0_permission_policy_action { get; set; }
    public ad_online[] ad_online_action { get; set; }
    public ad_online_perm_temp_insert_u1[] ad_online_perm_temp_insert_u1_action { get; set; }
    public ad_log_online[] ad_log_online_action { get; set; }
    public ad_u2vpn_online[] ad_online_u2vpn_action { get; set; }
    public ad_u3vpn_online[] ad_online_u3vpn_action { get; set; }
}

#region permission_policy
public class permission_policy
{
    public int m0_perm_pol_idx { get; set; }
    public string perm_pol_name { get; set; }
    public string perm_pol_desc { get; set; }
    public int perm_pol_status { get; set; }
}
#endregion permission_policy

#region ad_online
[Serializable]
public class ad_online
{
    public int emp_idx { get; set; }
    public string emp_code { get; set; }
    public string emp_firstname_th { get; set; }
    public string emp_lastname_th { get; set; }
    public string emp_name_th { get; set; }
    public string emp_name_en { get; set; }
    public int org_idx { get; set; }
    public string org_name_th { get; set; }
    public int rdept_idx { get; set; }
    public string dept_name_th { get; set; }
    public int rsec_idx { get; set; }
    public string sec_name_th { get; set; }
    public string pos_name_th { get; set; }
    public int jobgrade_level { get; set; }
    public int costcenter_no { get; set; }

    public int jobgrade_idx { get; set; }
    public int job_level { get; set; }
    public string EmpCode { get; set; }
    public int JobLevel { get; set; }
    public int EmpIDX { get; set; }
    public string FullNameTH { get; set; }

    public string on_table { get; set; }
    public string with_where { get; set; }
    public string emails_for_send { get; set; }

    public string from_table { get; set; }
    public int u0_pk_perm_idx_ref { get; set; }

    public string status_from_1 { get; set; }
    public string status_from_2 { get; set; }
    public string status_to_1 { get; set; }
    public string status_to_2 { get; set; }
    public int u0_node_idx_ref { get; set; }

    /** START Permission Temporary **/
    public string perm_temp_alphabet { get; set; }
    public string perm_temp_org { get; set; }

    public string u1_perm_temp_contact { get; set; }
    public string u1_perm_temp_contact_amount { get; set; }

    public int u0_perm_temp_idx { get; set; }
    public int m0_emp_type_idx_ref { get; set; }
    public string u0_perm_temp_code_id { get; set; }
    public string u0_perm_temp_comp_name { get; set; }
    public string u0_perm_temp_detail { get; set; }
    public string u0_cm_director_creator { get; set; }
    public string u0_cm_head_it { get; set; }
    public string u0_cm_director_it { get; set; }
    public int u0_perm_temp_status { get; set; }
    public string u0_perm_temp_created_at { get; set; }
    public int u0_perm_temp_created_by { get; set; }
    public string u0_perm_temp_updated_at { get; set; }
    public int u0_perm_temp_updated_by { get; set; }

    public int u1_perm_temp_idx { get; set; }
    public int u0_perm_temp_idx_ref { get; set; }
    public string u1_perm_temp_idcard { get; set; }
    public string u1_perm_temp_fullname { get; set; }
    public string u1_perm_temp_start { get; set; }
    public string u1_perm_temp_end { get; set; }
    public string u1_perm_temp_username { get; set; }
    public string u1_perm_temp_password { get; set; }
    public int u1_perm_temp_status { get; set; }
    public string u1_perm_temp_updated_at { get; set; }
    public int u1_perm_temp_updated_by { get; set; }

    public string filter_u0_perm_temp_code_id { get; set; }
    public string filter_u0_perm_temp_creator { get; set; }
    public string filter_u0_perm_temp_comp_name { get; set; }
    public string filter_u0_perm_temp_org { get; set; }
    public string filter_u0_perm_temp_dept { get; set; }
    public string filter_u0_perm_temp_node { get; set; }
    public string filter_u1_perm_temp_contact { get; set; }
    public string filter_u0_perm_temp_condition_date { get; set; }
    public string filter_u0_perm_temp_date_only { get; set; }
    public string filter_u0_perm_temp_date_from { get; set; }
    public string filter_u0_perm_temp_date_to { get; set; }
    /** END Permission Temporary **/

    /** START Permission Permanent **/
    public string perm_perm_alphabet { get; set; }
    public string perm_perm_org { get; set; }
    public int u0_node_idx_ref_current { get; set; }

    public int m0_perm_pol_idx { get; set; }
    public string perm_pol_name { get; set; }
    public string perm_pol_desc { get; set; }
    public int perm_pol_status { get; set; }

    public int u0_perm_perm_idx { get; set; }
    public int m0_perm_pol_idx_ref { get; set; }
    public int u0_perm_perm_emp_idx_ref { get; set; }
    public string u0_perm_perm_code_id { get; set; }
    public string u0_perm_perm_sap { get; set; }
    public string u0_perm_perm_mail_center { get; set; }
    public string u0_perm_perm_mail_priv { get; set; }
    public string u0_perm_perm_ad { get; set; }
    public string u0_perm_perm_cm_head_emp { get; set; }
    public string u0_perm_perm_cm_director_emp { get; set; }
    public string u0_perm_perm_cm_head_it { get; set; }
    public string u0_perm_perm_cm_director_it { get; set; }
    public string u0_perm_perm_cm_it { get; set; }
    public int u0_perm_perm_status { get; set; }
    public string u0_perm_perm_created_at { get; set; }
    public int u0_perm_perm_created_by { get; set; }
    public string u0_perm_perm_updated_at { get; set; }
    public int u0_perm_perm_updated_by { get; set; }

    public int u1_perm_perm_idx { get; set; }
    public int u0_perm_perm_idx_ref { get; set; }
    public string u1_perm_perm_cm_it { get; set; }
    public string u1_perm_perm_created_at { get; set; }
    public string u1_perm_perm_created_at_date { get; set; }
    public string u1_perm_perm_created_at_time { get; set; }
    public int u1_perm_perm_created_by { get; set; }

    public int emp_idx_creator { get; set; }
    public string emp_code_creator { get; set; }
    public string emp_name_th_creator { get; set; }

    public string filter_u0_perm_perm_code_id { get; set; }
    public string filter_u0_perm_perm_empcode_fullnameth_applicant { get; set; }
    public string filter_u0_perm_perm_org { get; set; }
    public string filter_u0_perm_perm_dept { get; set; }
    public string filter_u0_perm_perm_sec { get; set; }
    public string filter_u0_perm_perm_pos { get; set; }
    public string filter_u0_perm_perm_node { get; set; }
    public string filter_u0_perm_perm_condition_date { get; set; }
    public string filter_u0_perm_perm_date_only { get; set; }
    public string filter_u0_perm_perm_date_from { get; set; }
    public string filter_u0_perm_perm_date_to { get; set; }
    /** END Permission Permanent **/

    public string u0_perm_perm_date_use { get; set; }
    public string u0_perm_perm_mail_replace { get; set; }
    public int u0_perm_perm_type_mail { get; set; }
    public string u0_perm_perm_reason { get; set; }

    public int u0_perm_perm_vpn_sap { get; set; }
    public string u0_perm_perm_vpn_sap_reason { get; set; }

    public int u0_perm_perm_vpn_bi { get; set; }
    public string u0_perm_perm_vpn_bi_reason { get; set; }

    public int u0_perm_perm_vpn_express { get; set; }
    public string u0_perm_perm_vpn_express_reason { get; set; }
    public string u0_perm_perm_cm_it_before { get; set; }
    public int u2_perm_perm_idx { get; set; }
    public int vpn1idx { get; set; }
    public int vpnidx { get; set; }
    public string vpn_name { get; set; }
    public string vpn1_name { get; set; }
    public string remark { get; set; }
    public int condition_addmore { get; set; }



}
#endregion ad_online

#region ad_u2vpn_online

public class ad_u2vpn_online
{

    public int emp_idx { get; set; }
    public int condition { get; set; }
    public int u0_perm_temp_idx { get; set; }

    /** use vpn **/
    public int vpnidx { get; set; }
    public string vpn_name { get; set; }
    public int vpn1idx { get; set; }
    public string vpn1_name { get; set; }
    public string remark { get; set; }
    public int cemp_idx { get; set; }
    public int uemp_idx { get; set; }
    public string table_vpn { get; set; }
    public string on_table { get; set; }
    public int u2_perm_perm_status { get; set; }


}
#endregion ad_online

#region ad_u3vpn_online

public class ad_u3vpn_online
{

    public int emp_idx { get; set; }
    public int cemp_idx { get; set; }
    public int uemp_idx { get; set; }
    public int condition { get; set; }
    public int u0_perm_temp_idx { get; set; }

    /** use vpn **/
    public int vpnidx { get; set; }
    public string vpn_name { get; set; }
    public int vpn1idx { get; set; }
    public string vpn1_name { get; set; }
    public string remark { get; set; }
    public string table_vpn { get; set; }
    public string on_table { get; set; }

}

#endregion ad_online

#region ad_online_perm_temp_insert_u1
[Serializable]
public class ad_online_perm_temp_insert_u1
{
    public int u0_perm_temp_idx_ref { get; set; }
    public string u1_perm_temp_idcard { get; set; }
    public string u1_perm_temp_fullname { get; set; }
    public string u1_perm_temp_start { get; set; }
    public string u1_perm_temp_end { get; set; }
    public string u1_perm_temp_username { get; set; }
    public string u1_perm_temp_password { get; set; }
    public int u1_perm_temp_status { get; set; }
    public string u1_perm_temp_updated_at { get; set; }
    public int u1_perm_temp_updated_by { get; set; }
}
#endregion ad_online_insert_u1

#region ad_log_online
[Serializable]
public class ad_log_online
{
    public int l0_perm_log_idx { get; set; }
    public string from_table { get; set; }
    public int u0_pk_perm_idx_ref { get; set; }
    public int u0_node_idx_ref { get; set; }
    public string perm_log_created_at { get; set; }
    public int perm_log_created_by { get; set; }

    public string perm_log_created_at_date { get; set; }
    public string perm_log_created_at_time { get; set; }
    public string node_name_1 { get; set; }
    public string status_from_1 { get; set; }
    public string status_from_2 { get; set; }
    public string emp_name_th { get; set; }
}
#endregion ad_log_online
