﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_qa
/// </summary>
[Serializable]
[XmlRoot("data_qa_cims")]
public class data_qa_cims
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
   

    // master data //
    public qa_cims_m0_unit_detail[] qa_cims_m0_unit_list { get; set; }
    public qa_cims_m0_equipment_type_detail[] qa_cims_m0_equipment_type_list { get; set; }
    public qa_cims_m0_place_detail[] qa_cims_m0_place_list { get; set; }
    public qa_cims_m0_cal_type_detail[] qa_cims_m0_cal_type_list { get; set; }
    public qa_cims_m0_equipment_result_detail[] qa_cims_m0_equipment_result_list { get; set; }
    public qa_cims_m0_brand_detail[] qa_cims_m0_brand_list { get; set; }
    public qa_cims_m0_equipment_name_detail[] qa_cims_m0_equipment_name_list { get; set; }
    public qa_cims_m0_investigate_detail[] qa_cims_m0_investigate_list { get; set; }
    public qa_cims_m0_setname_detail[] qa_cims_m0_setname_list { get; set; }
    public qa_cims_m0_form_detail_detail[] qa_cims_m0_form_detail_list { get; set; }
    public qa_cims_m0_lab_detail[] qa_cims_m0_lab_list { get; set; }
    public qa_cims_m1_lab_detail[] qa_cims_m1_lab_list { get; set; }
    public qa_cims_r0_form_create_detail[] qa_cims_r0_form_create_list { get; set; }
    public qa_cims_r1_form_create_detail[] qa_cims_r1_form_create_list { get; set; }
    public qa_cims_log_registration_device_detail[] qa_cims_log_registration_device_list { get; set; }
    public qa_cims_m0_resulution_detail[] qa_cims_m0_resolution_list { get; set; }
    public qa_cims_m0_frequency_detail[] qa_cims_m0_frequency_list { get; set; }
    public qa_cims_m0_rangeuse_detail[] qa_cims_m0_rangeuse_list { get; set; }
    public qa_cims_m0_measuring_detail[] qa_cims_m0_measuring_list { get; set; }
    public qa_cims_m0_acceptance_detail[] qa_cims_m0_acceptance_list { get; set; }
    public qa_cims_m0_location_detail[] qa_cims_m0_location_list { get; set; }
    public qa_cims_m0_statusdevices_detail[] qa_cims_m0_statusdevices_list { get; set; }

    //master form name :: New !
    public qa_cims_m0_form_calculate_details[] qa_cims_m0_form_calculate_list { get; set; }
    public qa_cims_m1_form_calculate_details[] qa_cims_m1_form_calculate_list { get; set; }
    public qa_cims_m2_form_calculate_details[] qa_cims_m2_form_calculate_list { get; set; }

    // master data //


    // master data registration device //

    public qa_cims_m0_registration_device[] qa_cims_m0_registration_device_list { get; set; }
    public qa_cims_m1_registration_device[] qa_cims_m1_registration_device_list { get; set; }
    public qa_cims_m2_registration_device[] qa_cims_m2_registration_device_list { get; set; }
    public qa_cims_m3_registration_device[] qa_cims_m3_registration_device_list { get; set; }
    public qa_cims_m4_registration_device[] qa_cims_m4_registration_device_list { get; set; }
    public qa_cims_m5_registration_device[] qa_cims_m5_registration_device_list { get; set; }
    public qa_cims_m6_registration_device[] qa_cims_m6_registration_device_list { get; set; }


    // document //  
    public qa_cims_bindnode_decision_detail[] qa_cims_bindnode_decision_list { get; set; }

    [XmlElement("qa_cims_u0_document_device_list")]
    public qa_cims_u0_document_device_detail[] qa_cims_u0_document_device_list { get; set; }

    public qa_cims_u1_document_device_detail[] qa_cims_u1_document_device_list { get; set; }
    public qa_cims_u2_document_device_detail[] qa_cims_u2_document_device_list { get; set; }
    public qa_cims_u1_search_document_device_detail[] qa_cims_u1_search_document_device_list { get; set; }

    // document //

    //document calibation 
    public qa_cims_u0_calibration_document_details[] qa_cims_u0_calibration_document_list { get; set; }
    public qa_cims_u1_calibration_document_details[] qa_cims_u1_calibration_document_list { get; set; }
    public qa_cims_u2_calibration_document_details[] qa_cims_u2_calibration_document_list { get; set; }
    public qa_cims_m0_calibration_registration_details[] qa_cims_m0_calibration_registration_list { get; set; }
    public qa_cims_calibration_document_details[] qa_cims_calibration_document_list { get; set; }

    // search master data registration device//
    public qa_cims_search_registration_device[] qa_cims_search_registration_device_list { get; set; }
    public qa_cims_bind_topics_form_details[] qa_cims_bind_topics_form_list { get; set; }

    //record result to form by form || record by value
    public qa_cims_record_topics_details[] qa_cims_record_topics_list { get; set; }
    public qa_cims_record_result_by_form_details[] qa_cims_record_result_by_form_list { get; set; }

    public qa_cims_m0_per_management_detail[] qa_cims_m0_per_management_list { get; set; }

    //report devices
    qa_cims_reportdevices_detail[] qa_cims_reportdevices_list { get; set; }

    //per qa
    public qa_cims_m0_management_detail[] qa_cims_m0_management_list { get; set; } 
    public qa_cims_m1_management_detail[] qa_cims_m1_management_list { get; set; }    
    public qa_cims_m2_management_detail[] qa_cims_m2_management_list { get; set; }

}

[Serializable]
public class qa_cims_m0_management_detail
{

    public int m0_management_idx { get; set; }
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int m0_management_status { get; set; }
    public int rpos_idx { get; set; }  
    public string pos_name_th_per { get; set; } 
    public string sec_name_th_per { get; set; }   
    public string dept_name_th_per { get; set; }   
    public string org_name_th_per { get; set; }
    public int condition { get; set; }
    public string place_idx_check_insertregis { get; set; }
    public int place_idx { get; set; }


}

public class qa_cims_m1_management_detail
{

    public int m0_management_idx { get; set; }
    public int m1_management_idx { get; set; }
    public int place_idx { get; set; }  
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int m1_management_status { get; set; }
    public string place_name { get; set; }
}


public class qa_cims_m2_management_detail
{

    public int m0_management_idx { get; set; }
    public int m0_per_management_idx { get; set; }
    public int m2_management_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int m2_management_status { get; set; }
    public string per_management_name { get; set; }
    public int condition { get; set; }

}

public class qa_cims_m0_per_management_detail
{
    
    public int m0_per_management_idx { get; set; }   
    public string per_management_name { get; set; } 
    public int cemp_idx { get; set; }
    public int per_management_status { get; set; }
    public int condition { get; set; }

}

public class qa_cims_reportdevices_detail
{
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int m0_device_idx { get; set; }
    public int m1_device_idx { get; set; }
    public string device_serial { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int device_org_idx { get; set; }
    public int device_rdept_idx { get; set; }
    public int device_rsec_idx { get; set; }
    public string device_cal_date { get; set; }
    public string device_due_date { get; set; }
    public int device_status { get; set; }
    public int condition { get; set; }
    public int staidx { get; set; }
    public int emp_idx_create { get; set; }
    public int rdept_idx_create { get; set; }
    public int org_idx_create { get; set; }
    public int rsec_idx_create { get; set; }
    public int brand_idx { get; set; }
    public int equipment_idx { get; set; }
    public string device_id_no { get; set; }
    public int range_of_use_min { get; set; }
    public int range_of_use_max { get; set; }
    public int unit_idx_range_of_use { get; set; }
    public int equipment_type_idx { get; set; }
    public int resolution_detail { get; set; }
    public int unit_idx_resolution { get; set; }
    public int measuring_range_min { get; set; }
    public int measuring_range_max { get; set; }
    public int unit_idx_measuring_range { get; set; }
    public int acceptance_criteria { get; set; }
    public int calibration_frequency { get; set; }
    public int calibration_frequency_unit { get; set; }
    public string equipment_name { get; set; }
    public string equipment_type_name { get; set; }
    public string status_name { get; set; }
    public string device_rsec { get; set; }
    public string device_rdept { get; set; }
    public string device_org { get; set; }
    public int acceptance_criteria_unit_idx { get; set; }
    public string device_details { get; set; }
    public int cal_type_idx { get; set; }
    public string calibration_point { get; set; }
    public string brand_name { get; set; }
    public string unit_name_ROU { get; set; }
    public string unit_name_Res { get; set; }
    public string unit_name_MR { get; set; }
    public string unit_name_AC { get; set; }
    public string unit_name_CP { get; set; }
    public int device_place_idx { get; set; }
    public string place_name { get; set; }
    public int place_idx { get; set; }
    public string list_m0_device { get; set; }
    public string resolution_name_value { get; set; }
    public string detail_frequency { get; set; }
    public string range_value { get; set; }
    public string measuring_value { get; set; }
    public string acceptance_value { get; set; }
    public int equipment_frequency_idx { get; set; }
    public int equipment_resolution_idx { get; set; }
    public int m3_device_frequency_idx { get; set; }
    public string receive_devices { get; set; }
    public int certificate { get; set; }
    public int m0_location_idx { get; set; }
    public int m0_status_idx { get; set; }
    public int m1_location_idx { get; set; }
    public string location_name { get; set; }
    public string location_zone { get; set; }
    public string certificate_detail { get; set; }
    public int count_devices { get; set; }

}

public class qa_cims_m0_statusdevices_detail
{
    public int m0_status_idx { get; set; }
    public string status_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int status { get; set; }
    public int condition { get; set; }

}

public class qa_cims_m0_acceptance_detail
{

    public int m6_device_acceptance_idx { get; set; }
    public int m0_device_idx { get; set; }
    public string acceptance_criteria { get; set; }
    public int unit_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int status { get; set; }
    public int condition { get; set; }
    public string unit_symbol_en { get; set; }

}

public class qa_cims_m0_resulution_detail
{
    
    public int equipment_resolution_idx { get; set; }    
    public string resolution_name { get; set; }
    public int unit_idx { get; set; }   
    public int cemp_idx { get; set; }   
    public string create_date { get; set; }  
    public string update_date { get; set; }   
    public int resolution_status { get; set; }
    public string unit_symbol_en { get; set; }
}

public class qa_cims_m0_frequency_detail
{
    public int equipment_frequency_idx { get; set; }  
    public string frequency_count { get; set; }
    public int frequency_unit { get; set; }
    public int cemp_idx { get; set; } 
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int frequency_status { get; set; }
    public int condition { get; set; }
    public string detail_frequency { get; set; }
    public string unit_frequency { get; set; }
    
}

public class qa_cims_m0_rangeuse_detail
{
    
    public int m0_device_idx { get; set; }    
    public int m4_device_range_idx { get; set; }   
    public int cemp_idx { get; set; }   
    public string create_date { get; set; }   
    public string update_date { get; set; }   
    public string range_start { get; set; } 
    public string range_end { get; set; }   
    public int condition { get; set; }  
    public int unit_idx { get; set; }    
    public string unit_symbol_en { get; set; }


}

public class qa_cims_m0_measuring_detail
{
  
    public int m0_device_idx { get; set; }
    public int m5_device_measuring_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public string measuring_start { get; set; }
    public string measuring_end { get; set; }
    public int condition { get; set; }
    public int unit_idx { get; set; }
    public string unit_symbol_en { get; set; }


}

public class qa_cims_m0_unit_detail
{
    public int unit_idx { get; set; }
    public string unit_name_th { get; set; }
    public string unit_name_en { get; set; }
    public string unit_symbol_th { get; set; }
    public string unit_symbol_en { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int unit_status { get; set; }
    public int condition { get; set; }
}

public class qa_cims_m0_equipment_type_detail
{
    public int equipment_type_idx { get; set; }
    public string equipment_type_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int equipment_type_status { get; set; }
}

public class qa_cims_m0_place_detail
{
    public int place_idx { get; set; }
    public string place_name { get; set; }
    public string place_code { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int place_status { get; set; }
    public int condition { get; set; }
    public string place_idx_check_insert_regis { get; set; }
}

public class qa_cims_m0_cal_type_detail
{
    public int cal_type_idx { get; set; }
    public string cal_type_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int cal_type_status { get; set; }
}

public class qa_cims_m0_equipment_result_detail
{
    public int equipment_result_idx { get; set; }
    public string equipment_result { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int equipment_result_status { get; set; }
}

public class qa_cims_m0_brand_detail
{
    public int brand_idx { get; set; }
    public string brand_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int brand_status { get; set; }
}

public class qa_cims_m0_equipment_name_detail
{
    public int equipment_idx { get; set; }
    public string equipment_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int equipment_status { get; set; }
}

public class qa_cims_m0_investigate_detail
{
    public int investigate_idx { get; set; }
    public string investigate_detail { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int investigate_status { get; set; }
}

public class qa_cims_m0_setname_detail
{
    public int setname_idx { get; set; }
    public string set_name { get; set; }
    public string set_detail { get; set; }
    public int setroot_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int setname_status { get; set; }
}

public class qa_cims_m0_form_detail_detail
{
    public int form_detail_idx { get; set; }
    public string form_detail_name { get; set; }
    public string form_detail { get; set; }
    public int form_detail_root_idx { get; set; }
    public int set_idx { get; set; }
    public string set_name { get; set; }
    public int option_idx { get; set; }
    public string option_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int form_detail_status { get; set; }
    public int condition { get; set; }
}

public class qa_cims_m0_lab_detail
{
    public int m0_lab_idx { get; set; }
    public string lab_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int lab_status { get; set; }
    public int cal_type_idx { get; set; }
    public string cal_type_name { get; set; }
    public int place_idx { get; set; }
    public string place_name { get; set; }
    public int condition { get; set; }
    public int decision { get; set; }

}

public class qa_cims_m1_lab_detail
{
    public int m1_lab_idx { get; set; }
    public int m0_lab_idx { get; set; }
    public int equipment_idx { get; set; }
    public int certificate { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public string status { get; set; }
    public string equipment_name { get; set; }
    public string lab_name { get; set; }

}

public class qa_cims_r0_form_create_detail
{
    public int r0_form_create_idx { get; set; }
    public string r0_form_create_name { get; set; }
    public int equipment_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int cemp_idx { get; set; }
    public int condition { get; set; }
    public int r0_form_create_status { get; set; }
    public string form_detail_name { get; set; }
    public string equipment_name { get; set; }
}

public class qa_cims_r1_form_create_detail
{
    public int r1_form_create_idx { get; set; }
    public int r0_form_create_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int cemp_idx { get; set; }
    public int condition { get; set; }
    public int form_detail_idx { get; set; }
    public int r1_form_create_status { get; set; }
    public string form_detail_name { get; set; }
    public string r0_form_create_name { get; set; }
    public int r1_form_root_idx { get; set; }
    public string text_name_setform { get; set; }

}

public class qa_cims_m0_registration_device
{
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int m0_device_idx { get; set; }
    public int m1_device_idx { get; set; }
    public string device_serial { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int device_org_idx { get; set; }
    public int device_rdept_idx { get; set; }
    public int device_rsec_idx { get; set; }
    public string device_cal_date { get; set; }
    public string device_due_date { get; set; }
    public int device_status { get; set; }
    public int condition { get; set; }
    public int staidx { get; set; }
    public int emp_idx_create { get; set; }
    public int rdept_idx_create { get; set; }
    public int org_idx_create { get; set; }
    public int rsec_idx_create { get; set; }
    public int brand_idx { get; set; }
    public int equipment_idx { get; set; }
    public string device_id_no { get; set; }
    public int range_of_use_min { get; set; }
    public int range_of_use_max { get; set; }
    public int unit_idx_range_of_use { get; set; }
    public int equipment_type_idx { get; set; }
    public int resolution_detail { get; set; }
    public int unit_idx_resolution { get; set; }
    public int measuring_range_min { get; set; }
    public int measuring_range_max { get; set; }
    public int unit_idx_measuring_range { get; set; }
    public int acceptance_criteria { get; set; }
    public int calibration_frequency { get; set; }
    public int calibration_frequency_unit { get; set; }
    public string equipment_name { get; set; }
    public string equipment_type_name { get; set; }
    public string status_name { get; set; }
    public string device_rsec { get; set; }
    public string device_rdept { get; set; }
    public string device_org { get; set; }
    public int acceptance_criteria_unit_idx { get; set; }
    public string device_details { get; set; }
    public int cal_type_idx { get; set; }
    public string calibration_point { get; set; }
    public string brand_name { get; set; }
    public string unit_name_ROU { get; set; }
    public string unit_name_Res { get; set; }
    public string unit_name_MR { get; set; }
    public string unit_name_AC { get; set; }
    public string unit_name_CP { get; set; }
    public int device_place_idx { get; set; }
    public string place_name { get; set; }
    public int place_idx { get; set; }    
    public string list_m0_device { get; set; }  
    public string resolution_name_value { get; set; }
    public string detail_frequency { get; set; }  
    public string range_value { get; set; }   
    public string measuring_value { get; set; }   
    public string acceptance_value { get; set; }    
    public int equipment_frequency_idx { get; set; }    
    public int equipment_resolution_idx { get; set; }
    public int m3_device_frequency_idx { get; set; }
    public string receive_devices { get; set; }
    public int certificate { get; set; }  
    public int m0_location_idx { get; set; }   
    public int m0_status_idx { get; set; }
    public int m1_location_idx { get; set; }
    public string location_name { get; set; }
    public string location_zone { get; set; }
    public string certificate_detail { get; set; }

}

public class qa_cims_m1_registration_device
{
    public int m1_device_idx { get; set; }
    public int m0_device_idx { get; set; }
    public int calibration_point { get; set; }
    public string update_date { get; set; }
    public int unit_idx { get; set; }
    public string unit_symbol_en { get; set; }
    public int cemp_idx { get; set; }
    public int status { get; set; }
}

[Serializable]
public class qa_cims_m2_registration_device
{

    public int equipment_resolution_idx { get; set; }
    public int m0_device_idx { get; set; }
    public string update_date { get; set; }
    public int unit_idx { get; set; }
    public int cemp_idx { get; set; }
    public int status { get; set; }

}

[Serializable]
public class qa_cims_m3_registration_device
{

    public int equipment_frequency_idx { get; set; }
    public int m0_device_idx { get; set; }
    public string update_date { get; set; }
    public int unit_idx { get; set; }
    public int cemp_idx { get; set; }
    public int status { get; set; }

}

[Serializable]
public class qa_cims_m4_registration_device
{
    public int m4_device_range_idx { get; set; }
    public int m0_device_idx { get; set; }
    public string update_date { get; set; }
    public string range_start { get; set; }
    public string range_end { get; set; }
    public int cemp_idx { get; set; }
    public int unit_idx { get; set; }
    public int status { get; set; }

}

[Serializable]
public class qa_cims_m5_registration_device
{

    public int m0_device_idx { get; set; }
    public int m5_device_measduring_ix { get; set; }
    public string update_date { get; set; }
    public string measuring_start { get; set; }
    public string measuring_end { get; set; }
    public int cemp_idx { get; set; }
    public int unit_idx { get; set; }
    public int status { get; set; }

}

[Serializable]
public class qa_cims_m6_registration_device
{
    public int m6_device_acceptance_idx { get; set; }
    public int m0_device_idx { get; set; }
    public string update_date { get; set; }
    public string acceptance_criteria { get; set; }
    public int cemp_idx { get; set; }
    public int unit_idx { get; set; }
    public int status { get; set; }

}

public class qa_cims_bindnode_decision_detail
{
    public int decision_idx { get; set; }
    public string decision_name { get; set; }
    public int decision_status { get; set; }
    public int nodidx { get; set; }

}

[Serializable]
public class qa_cims_u0_document_device_detail
{
    [XmlElement("u0_device_idx")]
    public int u0_device_idx { get; set; }

    [XmlElement("document_type_idx")]
    public int document_type_idx { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("emp_idx_create")]
    public int emp_idx_create { get; set; }

    [XmlElement("org_idx_create")]
    public int org_idx_create { get; set; }

    [XmlElement("rdept_idx_create")]
    public int rdept_idx_create { get; set; }

    [XmlElement("rsec_idx_create")]
    public int rsec_idx_create { get; set; }

    [XmlElement("received_date")]
    public string received_date { get; set; }

    [XmlElement("org_idx_tranfer")]
    public int org_idx_tranfer { get; set; }

    [XmlElement("rdept_idx_tranfer")]
    public int rdept_idx_tranfer { get; set; }

    [XmlElement("rsec_idx_tranfer")]
    public int rsec_idx_tranfer { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("u0_cal_idx")]
    public int u0_cal_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("document_type_name")]
    public string document_type_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("device_rsec_idx")]
    public int device_rsec_idx { get; set; }

    [XmlElement("emp_email_create")]
    public string emp_email_create { get; set; }

    [XmlElement("emp_name_th_create")]
    public string emp_name_th_create { get; set; }

    [XmlElement("org_name_th_create")]
    public string org_name_th_create { get; set; }

    [XmlElement("dept_name_th_create")]
    public string dept_name_th_create { get; set; }

    [XmlElement("sec_name_th_create")]
    public string sec_name_th_create { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("org_tranfer")]
    public string org_tranfer { get; set; }

    [XmlElement("dept_tranfer")]
    public string dept_tranfer { get; set; }

    [XmlElement("sec_tranfer")]
    public string sec_tranfer { get; set; }

    [XmlElement("head_email_tranfer")]
    public string head_email_tranfer { get; set; }

    [XmlElement("email_tranfer")]
    public string email_tranfer { get; set; }

    [XmlElement("decision_status")]
    public string decision_status { get; set; }

    [XmlElement("headqa_email_tranfer")]
    public string headqa_email_tranfer { get; set; }

    [XmlElement("email_create_tranfer")]
    public string email_create_tranfer { get; set; }

    [XmlElement("place_idx_tranfer")]
    public int place_idx_tranfer { get; set; }

    [XmlElement("place_name_tranfer")]
    public string place_name_tranfer { get; set; }

}


public class qa_cims_u1_document_device_detail
{
    public int u1_device_idx { get; set; }
    public int u0_device_idx { get; set; }
    public int m0_device_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int staidx { get; set; }
    public int cemp_idx { get; set; }
    public int condition { get; set; }
    public string device_id_no { get; set; }
    public string equipment_name { get; set; }
    public string device_serial { get; set; }
    public string sec_name_th { get; set; }
    public int status { get; set; }
    public int m0_actor_idx { get; set; }
    public int m0_node_idx { get; set; }
    public string comment { get; set; }
    public int decision_idx { get; set; }
    public string tranfer_name { get; set; }
    public int device_rsec_idx { get; set; }
    public bool selected { get; set; }
    public string place_name { get; set; }
    public int device_status { get; set; }
    public int place_idx { get; set; }
    public string status_name { get; set; }
    public int rsec_idx_create { get; set; }
    public int cal_type_idx { get; set; }   
    public string range_of_use { get; set; }  
    public string point_of_use { get; set; }   
    public int place_idx_tranfer { get; set; } 
    public string place_name_tranfer { get; set; }
}

public class qa_cims_u1_search_document_device_detail
{
    public int u0_device_idx { get; set; }
    public int document_type_idx { get; set; }
    public string document_code { get; set; }
    public int emp_idx_create { get; set; }
    public int org_idx_create { get; set; }
    public int rdept_idx_create { get; set; }
    public int rsec_idx_create { get; set; }
    public string received_date { get; set; }
    public int org_idx_tranfer { get; set; }
    public int rdept_idx_tranfer { get; set; }
    public int rsec_idx_tranfer { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int staidx { get; set; }
    public int m0_actor_idx { get; set; }
    public int m0_node_idx { get; set; }
    public int u0_cal_idx { get; set; }
    public string comment { get; set; }
    public string document_type_name { get; set; }
    public string status_name { get; set; }
    public string status_desc { get; set; }
    public int decision_idx { get; set; }
    public string emp_code { get; set; }
    public string emp_name_th { get; set; }
    public string org_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_th { get; set; }
    public string pos_name_th { get; set; }   
    public int place_idx { get; set; }  
    public string place_name { get; set; }  
    public string actor_name { get; set; }
    public string device_id_no { get; set; }
    public int cal_type_idx { get; set; }
    public string device_rsec_idx { get; set; }

}


public class qa_cims_log_registration_device_detail
{
    public int log_device_idx { get; set; }
    public string log_detalis { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string create_time { get; set; }
    public int action_type { get; set; }
    public string action_name { get; set; }
    public string ip_address { get; set; }
    public string emp_name_th { get; set; }
    public int m0_device_idx { get; set; }
}


public class qa_cims_u2_document_device_detail
{
    public int u2_device_idx { get; set; }
    public int u0_device_idx { get; set; }
    public int u1_device_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int m0_node_idx { get; set; }
    public string create_date { get; set; }
    public int cemp_idx { get; set; }
    public string comment { get; set; }
    public int decision_idx { get; set; }
    public string cemp_name { get; set; }
    public string actor_name { get; set; }
    public string decision_desc { get; set; }
    public string create_time { get; set; }
    public string device_id_no { get; set; }
}

public class qa_cims_u0_calibration_document_details
{
    public int u0_cal_idx { get; set; }
    public int place_idx { get; set; }
    public int cemp_idx { get; set; }
    public int cemp_org_idx { get; set; }
    public int cemp_rdept_idx { get; set; }
    public int cemp_rsec_idx { get; set; }
    public string received_date { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int u0_cal_doc_status { get; set; }
    public string document_code { get; set; }
    public int admin_emp_idx { get; set; }
    public int admin_org_idx { get; set; }
    public int admin_rdept_idx { get; set; }
    public int admin_rsec_idx { get; set; }
    public int staidx { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int equipment_type { get; set; }
    public int document_reference { get; set; }

}

public class qa_cims_u1_calibration_document_details
{
    public int u1_cal_idx { get; set; }
    public int u0_cal_idx { get; set; }
    public int u1_cal_doc_status { get; set; }
    public int certificate { get; set; }
    public int staidx { get; set; }
    public string other_details { get; set; }
    public string update_date { get; set; }
    public string received_calibration_date { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int m0_lab_idx { get; set; }
    public int m0_device_idx { get; set; }
    public string equipment_name { get; set; }
    public string device_id_no { get; set; }
    public string device_serial { get; set; }
    public string lab_name { get; set; }
    public int equipment_type { get; set; }
    public int decision_idx { get; set; }
    public int cemp_idx { get; set; }
    public string comment { get; set; }
    public string equipment_type_name { get; set; }
    public int place_idx { get; set; }
    public string place_name { get; set; }
    public string form_detail_name { get; set; }
    public int equipment_idx { get; set; }
    public int r0_form_create_idx { get; set; }
    public string r0_form_create_name { get; set; }
    public int r1_form_create_idx { get; set; }
    public int r1_form_root_idx { get; set; }
    public int form_detail_idx { get; set; }
    public string brand_name { get; set; }
}

public class qa_cims_u2_calibration_document_details
{
    public int u2_cal_idx { get; set; }
    public int u1_cal_idx { get; set; }
    public int u0_cal_idx { get; set; }
    public int cemp_idx { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public string comment { get; set; }
    public string create_date { get; set; }
    public int decision_idx { get; set; }

}


public class qa_cims_m0_calibration_registration_details
{
    public int m0_device_idx { get; set; }
    public int m1_device_idx { get; set; }
    public string device_serial { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int device_org_idx { get; set; }
    public int device_rdept_idx { get; set; }
    public int device_rsec_idx { get; set; }
    public string device_cal_date { get; set; }
    public string device_due_date { get; set; }
    public int device_status { get; set; }
    public int condition { get; set; }
    public int staidx { get; set; }
    public int emp_idx_create { get; set; }
    public int rdept_idx_create { get; set; }
    public int org_idx_create { get; set; }
    public int rsec_idx_create { get; set; }
    public int brand_idx { get; set; }
    public int equipment_idx { get; set; }
    public string device_id_no { get; set; }
    public decimal range_of_use_min { get; set; }
    public decimal range_of_use_max { get; set; }
    public int unit_idx_range_of_use { get; set; }
    public int equipment_type_idx { get; set; }
    public decimal resolution_detail { get; set; }
    public int unit_idx_resolution { get; set; }
    public decimal measuring_range_min { get; set; }
    public decimal measuring_range_max { get; set; }
    public int unit_idx_measuring_range { get; set; }
    public decimal acceptance_criteria { get; set; }
    public decimal calibration_frequency { get; set; }
    public int calibration_frequency_unit { get; set; }
    public string equipment_name { get; set; }
    public string equipment_type_name { get; set; }
    public string status_name { get; set; }
    public string device_rsec { get; set; }
    public int acceptance_criteria_unit_idx { get; set; }
    public string device_details { get; set; }
    public int cal_type_idx { get; set; }
    public bool selected { get; set; }
    public string device_org { get; set; }
    public string device_rdept { get; set; }
    public string brand_name { get; set; }
    public string unit_name_resolution { get; set; }
    public string unit_name_range_of_use { get; set; }
    public string unit_name_calibration_frequency { get; set; }
    public string unit_name_measuring { get; set; }
    public string unit_name_calibration_point { get; set; }
    public string unit_name_acceptance_criteria { get; set; }
    public decimal calibration_point { get; set; }
    public string calibration_point_list { get; set; }
    public string calibration_point_idx_list { get; set; }
    public int certificate { get; set; }
    public int count_point_list { get; set; }
    public int count_point_unit_idx { get; set; }


}

public class qa_cims_calibration_document_details
{
    public int u0_cal_idx { get; set; }
    public int u1_cal_idx { get; set; }
    public int place_idx { get; set; }
    public int cemp_idx { get; set; }
    public int cemp_org_idx { get; set; }
    public int cemp_rdept_idx { get; set; }
    public int cemp_rsec_idx { get; set; }
    public string received_date { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int u0_cal_doc_status { get; set; }
    public string document_code { get; set; }
    public int admin_emp_idx { get; set; }
    public int admin_org_idx { get; set; }
    public int admin_rdept_idx { get; set; }
    public int admin_rsec_idx { get; set; }
    public int staidx { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int equipment_type { get; set; }
    public int document_reference { get; set; }
    public int decision_idx { get; set; }
    public string equipment_type_name { get; set; }
    public string status_name { get; set; }
    public string emp_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_th { get; set; }
    public string pos_name_th { get; set; }

}
public class qa_cims_search_registration_device
{
    //public string equipment_name_search { get; set; }
    //public string Device_id_no_search { get; set; }
    //public string Device_serial_search { get; set; }
    //public string Device_rsec_search { get; set; }
    //public string device_due_date_search { get; set; }
    //public string device_cal_date_search { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int condition { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int m0_device_idx { get; set; }
    public int m1_device_idx { get; set; }
    public string device_serial { get; set; }
    public int device_org_idx { get; set; }
    public int device_rdept_idx { get; set; }
    public int device_rsec_idx { get; set; }
    public string device_cal_date { get; set; }
    public string device_due_date { get; set; }
    public int device_status { get; set; }
    public int staidx { get; set; }
    public int emp_idx_create { get; set; }
    public int rdept_idx_create { get; set; }
    public int org_idx_create { get; set; }
    public int rsec_idx_create { get; set; }
    public int brand_idx { get; set; }
    public int equipment_idx { get; set; }
    public string device_id_no { get; set; }
    public int range_of_use_min { get; set; }
    public int range_of_use_max { get; set; }
    public int unit_idx_range_of_use { get; set; }
    public int equipment_type_idx { get; set; }
    public int resolution_detail { get; set; }
    public int unit_idx_resolution { get; set; }
    public int measuring_range_min { get; set; }
    public int measuring_range_max { get; set; }
    public int unit_idx_measuring_range { get; set; }
    public int acceptance_criteria { get; set; }
    public int calibration_frequency { get; set; }
    public int calibration_frequency_unit { get; set; }
    public string equipment_name { get; set; }
    public string equipment_type_name { get; set; }
    public string status_name { get; set; }
    public string device_rsec { get; set; }
    public string device_rdept { get; set; }
    public string device_org { get; set; }
    public int acceptance_criteria_unit_idx { get; set; }
    public string device_details { get; set; }
    public int cal_type_idx { get; set; }
    public string calibration_point { get; set; }
    public string brand_name { get; set; }
    public string unit_name_ROU { get; set; }
    public string unit_name_Res { get; set; }
    public string unit_name_MR { get; set; }
    public string unit_name_AC { get; set; }
    public string unit_name_CP { get; set; }
    public int device_place_idx { get; set; }
    public string place_name { get; set; }
    public int place_idx { get; set; }
    public int m0_status_idx { get; set; }

}


public class qa_cims_bind_topics_form_details
{
    public int topic_form_idx { get; set; }
    public string topic_form_name { get; set; }
    public string create_date { get; set; }
    public int m0_formcal_idx { get; set; }
    public int place_idx { get; set; }
    public string device_id_no { get; set; }
    public string value { get; set; }

}

public class qa_cims_record_topics_details
{

    public int topic_form_idx { get; set; }
    public string topic_form_name { get; set; }
    public string create_date { get; set; }
    public int m0_formcal_idx { get; set; }
    public int place_idx { get; set; }
    public string device_id_no { get; set; }
    public int u1_cal_idx { get; set; }
    public int decision_idx { get; set; }
    public int certificate { get; set; }
    public int m0_actor_idx { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_device_idx { get; set; }
    public int m0_lab_idx { get; set; }
    public string value { get; set; }
    public string u1_val1 { get; set; }
    public string u1_val2 { get; set; }
    public string u1_val3 { get; set; }
    public string u1_val4 { get; set; }
    public string u1_val5 { get; set; }
    public string u1_val6 { get; set; }
    public string u1_val7 { get; set; }
    public int condition { get; set; }

}

public class qa_cims_record_result_by_form_details
{
    public string create_date { get; set; }
    public int m0_formcal_idx { get; set; }
    public int place_idx { get; set; }
    public int u1_cal_idx { get; set; }
    public int condition { get; set; }
    public int cemp_idx { get; set; }
    public string device_id_no { get; set; }
    public string value { get; set; }
    public string val1 { get; set; }
    public string val2 { get; set; }
    public string val3 { get; set; }
    public string val4 { get; set; }
    public string val5 { get; set; }
    public string val6 { get; set; }
    public string val7 { get; set; }
    public string val8 { get; set; }
    public string val9 { get; set; }
    public string val10 { get; set; }
    public string val11 { get; set; }
    public string val12 { get; set; }
    public string val13 { get; set; }
    public string val14 { get; set; }
    public string val15 { get; set; }
    public string val16 { get; set; }
    public string val17 { get; set; }
    public string val18 { get; set; }
    public string val19 { get; set; }
    public string val20 { get; set; }
    public string val21 { get; set; }

}

public class qa_cims_m0_form_calculate_details
{
    public int m0_formcal_idx { get; set; }
    public string m0_formcal_name { get; set; }
    public int m0_form_status { get; set; }
    public int cemp_idx { get; set; }
}

public class qa_cims_m1_form_calculate_details
{
    public int m1_formcal_idx { get; set; }
    public int m0_formcal_idx { get; set; }
    public string table_result_name { get; set; }
    public string table_decription { get; set; }
    public int status { get; set; }
    public int cemp_idx { get; set; }
}

public class qa_cims_m0_location_detail
{  
    public int m0_location_idx { get; set; }   
    public int m1_location_idx { get; set; }   
    public string location_name { get; set; }  
    public int location_status { get; set; } 
    public int cemp_idx { get; set; }  
    public int zone_status { get; set; }  
    public string location_zone { get; set; }
    public int condition { get; set; }
}

public class qa_cims_m2_form_calculate_details
{

    public int m2_formcal_idx { get; set; }
    public int m0_formcal_idx { get; set; }
    public string m2_signature_name { get; set; }
    public string m2_position_signature { get; set; }
    public string m2_datetime_signature { get; set; }
    public int m2_select_detail { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int m2_formcal_status { get; set; }
    public int cemp_idx { get; set; }
}






