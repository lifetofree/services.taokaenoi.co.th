﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_resignation")]
public class data_resignation
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }
    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }
    [XmlElement("ReturnExit")]
    public string ReturnExit { get; set; }

    public M0_ExitInterview[] Boxm0_ExitInterview { get; set; }
    public M0_Question[] Boxm0_Question { get; set; }
    [XmlElement("Boxm0_ReasonExit")]
    public M0_ReasonExit[] Boxm0_ReasonExit { get; set; }
    public M0_ASSET[] Boxm0_ASSET { get; set; }
    public M1_ReasonExit[] Boxm1_ReasonExit { get; set; }
    public U2_Exchange_Response[] Boxu2_Exchange { get; set; }

}

[Serializable]
public class M0_ExitInterview
{
    public string topic_name { get; set; }
    public int CEmpIDX { get; set; }
    public int m0_toidx { get; set; }
    public string type_ans { get; set; }
    public int m0_taidx { get; set; }
    public int m0_chidx { get; set; }
    public string type_choice { get; set; }
    public int m0_tqidx { get; set; }
    public string type_quest { get; set; }
    public int no_choice { get; set; }
    public string quest_name { get; set; }
    public int orgidx { get; set; }
    public int condition { get; set; }
    public string OrgNameTH { get; set; }

}

[Serializable]
public class M0_Question
{
    public string quest_name { get; set; }
    public int CEmpIDX { get; set; }
    public int m0_toidx { get; set; }
    public int m0_taidx { get; set; }
    public string choice_name { get; set; }
    public int no_choice { get; set; }
    public int m0_tqidx { get; set; }
    public int m0_chidx { get; set; }
    public string type_ans { get; set; }
    public string type_quest { get; set; }
    public int m0_quidx { get; set; }
    public int m1_quidx { get; set; }
    public string value { get; set; }
    public string remark { get; set; }
    public string answer { get; set; }
    public int u1_anidx { get; set; }
    public int u0_docidx { get; set; }

}

[Serializable]
public class M0_ReasonExit
{
    [XmlElement("reason_resign")]
    public string reason_resign { get; set; }

    [XmlElement("reason_exit")]
    public string reason_exit { get; set; }

    [XmlElement("m0_rsidx")]
    public int m0_rsidx { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("EmpIN_Ex")]
    public string EmpIN_Ex { get; set; }


    [XmlElement("EmpIN")]
    public string EmpIN { get; set; }


    [XmlElement("dateexp")]
    public string dateexp { get; set; }

    [XmlElement("m0_tidx")]
    public int m0_tidx { get; set; }

    [XmlElement("date_resign")]
    public string date_resign { get; set; }

    [XmlElement("orgidx")]
    public int orgidx { get; set; }

    [XmlElement("rdeptidx")]
    public int rdeptidx { get; set; }

    [XmlElement("rsecidx")]
    public int rsecidx { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("jobgradeidx")]
    public int jobgradeidx { get; set; }

    [XmlElement("comment_m0rsidx")]
    public string comment_m0rsidx { get; set; }

    [XmlElement("u0_docidx")]
    public int u0_docidx { get; set; }

    [XmlElement("peridx")]
    public int peridx { get; set; }

    [XmlElement("type_menu")]
    public string type_menu { get; set; }

    [XmlElement("createdate")]
    public string createdate { get; set; }

    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("rdeptidx_create")]
    public int rdeptidx_create { get; set; }

    [XmlElement("rsecidx_create")]
    public int rsecidx_create { get; set; }

    [XmlElement("rposidx_create")]
    public int rposidx_create { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("type_jobgrade_flow")]
    public int type_jobgrade_flow { get; set; }

    [XmlElement("comment_approver")]
    public string comment_approver { get; set; }

    [XmlElement("l0_docidx")]
    public int l0_docidx { get; set; }

    [XmlElement("statue_name")]
    public string statue_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("coment")]
    public string coment { get; set; }

    [XmlElement("rposidx")]
    public int rposidx { get; set; }

    [XmlElement("unidx_ex")]
    public int unidx_ex { get; set; }

    [XmlElement("acidx_ex")]
    public int acidx_ex { get; set; }

    [XmlElement("staidx_ex")]
    public int staidx_ex { get; set; }

    [XmlElement("StatusDoc_Ex")]
    public string StatusDoc_Ex { get; set; }

    [XmlElement("Email")]
    public string Email { get; set; }

    [XmlElement("u0_docidx_exit")]
    public int u0_docidx_exit { get; set; }

    [XmlElement("docidx_ref")]
    public int docidx_ref { get; set; }

    [XmlElement("Rposidx_HR")]
    public string Rposidx_HR { get; set; }

    [XmlElement("m0_toidx")]
    public int m0_toidx { get; set; }

    [XmlElement("Email_Admin")]
    public string Email_Admin { get; set; }

}

[Serializable]
public class M1_ReasonExit
{
    public string remark { get; set; }
    public int u0_anidx { get; set; }
    public int m0_quidx { get; set; }
    public int CEmpIDX { get; set; }
    public int m1_quidx { get; set; }
    public string answer { get; set; }

}

[Serializable]
public class M0_ASSET
{
    public string u0_asset_no { get; set; }
    public int emp_idx { get; set; }
    public int u0_number { get; set; }
    public string u0_detail { get; set; }
    public string as_name { get; set; }
    public int u0_asidx { get; set; }
    public int type_asset { get; set; }
    public int u1_docidx { get; set; }
    public int u0_didx { get; set; }
    public string name_m0_typedevice { get; set; }
    public string u0_acc { get; set; }
    public string HolderDate { get; set; }

}

[Serializable]
public class U2_Exchange_Response
{
    public int u2_docidx { get; set; }
    public int u0_docidx { get; set; }
    public int orgidx { get; set; }
    public int rdeptidx { get; set; }
    public int rsecidx { get; set; }
    public string date_exchange { get; set; }
    public string detail_exchange { get; set; }
    public int unidx { get; set; }
    public int acidx { get; set; }
    public int staidx { get; set; }
    public int status { get; set; }
    public int CEmpIDX { get; set; }
    public string OrgNameTH { get; set; }
    public string DeptNameTH { get; set; }
    public string SecNameTH { get; set; }
    public string StatusDoc_Ex { get; set; }
    public int unidx_ex { get; set; }

}
