﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

/// <summary>
/// Summary description for FunctionToolEcom
/// </summary>
public class FunctionToolEcom
{
    public FunctionToolEcom()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region convert custom array object
    public string ConvertObjectToXml(Object objData)
    {
        try
        {
            var xmlDoc = new XmlDocument(); //Represents an XML document, 
                                            // Initializes a new instance of the XmlDocument class.          
            var xmlSerializer = new XmlSerializer(objData.GetType());
            // Create empty namespace
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);
            // Creates a stream whose backing store is memory. 
            using (var xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, objData, namespaces);
                xmlStream.Position = 0;
                //Loads the XML document from the specified string.
                xmlDoc.Load(xmlStream);
                foreach (XmlNode node in xmlDoc)
                {
                    if (node.NodeType == XmlNodeType.XmlDeclaration)
                    {
                        xmlDoc.RemoveChild(node);
                    }
                }
                return xmlDoc.InnerXml;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }

    public Object ConvertXmlToObject(Type dataName, string xmlText)
    {
        try
        {
            var deserializer = new XmlSerializer(dataName);
            TextReader reader = new StringReader(xmlText);
            Object retData = deserializer.Deserialize(reader);
            reader.Close();

            return retData;
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    #endregion

    #region XML to JSON and JSON to XML
    public string ConvertXmlToJson(string dataXml)
    {
        try
        {
            var doc = new XmlDocument();
            doc.LoadXml(dataXml);
            return JsonConvert.SerializeXmlNode(doc);
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public string ConvertJsonToXml(string dataJson)
    {
        try
        {
            XNode node = JsonConvert.DeserializeXNode(dataJson);
            return node.ToString();
        }
        catch (Exception)
        {
            throw;
        }
    }
    #endregion
}