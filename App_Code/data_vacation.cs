using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_vacation")]
public class data_vacation
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("u0_document_list")]
    public u0_document_detail[] u0_document_list { get; set; }
    public u1_document_detail[] u1_document_list { get; set; }
    public vacation_stat_detail[] vacation_stat_list { get; set; }
    public vacation_process_detail[] vacation_process_list { get; set; }
    public vacation_leavetype[] vacation_leavetype_list { get; set; }
    public vacation_shifttime[] vacation_shifttime_list { get; set; }
}

[Serializable]
public class u0_document_detail
{
    [XmlElement("u0_document_idx")]
    public int u0_document_idx { get; set; }
    [XmlElement("m0_leavetype_idx")]
    public int m0_leavetype_idx { get; set; }
    [XmlElement("m0_leavetype_name")]
    public string m0_leavetype_name {get; set; }
    [XmlElement("u0_leave_start")]
    public string u0_leave_start { get; set; }
    [XmlElement("u0_leave_end")]
    public string u0_leave_end { get; set; }
    [XmlElement("u0_leave_calc")]
    public float u0_leave_calc { get; set; }
    [XmlElement("u0_leave_comment")]
    public string u0_leave_comment { get; set; }
    [XmlElement("u0_emp_shift")]
    public int u0_emp_shift { get; set; }
    [XmlElement("u0_emp_shift_name")]
    public string u0_emp_shift_name { get; set; }
    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }
    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }
    [XmlElement("u0_status")]
    public int u0_status { get; set; }
    [XmlElement("u0_status_name")]
    public string u0_status_name { get; set; }
    [XmlElement("type_list")]
    public int type_list { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("emp_idx_approve1")]
    public int emp_idx_approve1 { get; set; }
    [XmlElement("emp_idx_approve2")]
    public int emp_idx_approve2 { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement("emp_name_approve1")]
    public string emp_name_approve1 { get; set; }
    [XmlElement("emp_name_approve2")]
    public string emp_name_approve2 { get; set; }

    [XmlElement("emp_mail_list")]
    public string emp_mail_list { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }
}

public class u1_document_detail
{
    public int u1_document_idx { get; set; }
    public int u0_document_idx { get; set; }
    public int action_emp_idx { get; set; }
    public int action_node_idx { get; set; }
    public int u1_approve_status { get; set; }
    public string u1_approve_remark { get; set; }
}

public class vacation_stat_detail
{
  public int m0_leavetype_idx { get; set; }
  public string m0_leavetype_name { get; set; }
  public int stat_min { get; set; }
}

public class vacation_process_detail
{
  public int waiting { get; set; }
  public int completed { get; set; }
}

public class vacation_leavetype
{
    public int midx { get; set; }
    public string type_name { get; set; }
}

public class vacation_shifttime
{
    public int midx { get; set; }
    public string TypeWork { get; set; }
}
