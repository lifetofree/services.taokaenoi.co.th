﻿using System;
using System.Xml.Serialization;
/// <summary>
/// Summary description for data_roombooking
/// </summary>
/// 
[Serializable]
[XmlRoot("data_roombooking")]
public class data_roombooking
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public rbk_m0_place_detail[] rbk_m0_place_list { get; set; }
    public rbk_m0_equiment_detail[] rbk_m0_equiment_list { get; set; }
    public rbk_m0_result_use_detail[] rbk_m0_result_use_list { get; set; }
    public rbk_m0_food_detail[] rbk_m0_food_list { get; set; }
    public rbk_m0_room_detail[] rbk_m0_room_list { get; set; }
    public rbk_m1_room_detail[] rbk_m1_room_list { get; set; }
    public rbk_search_room_detail[] rbk_search_room_list { get; set; }
    public rbk_check_room_detail[] rbk_check_room_list { get; set; }
    [XmlElement("rbk_u0_roombooking_list")]
    public rbk_u0_roombooking_detail[] rbk_u0_roombooking_list { get; set; }
    [XmlElement("rbk_u1_roombooking_list")]
    public rbk_u1_roombooking_detail[] rbk_u1_roombooking_list { get; set; }
    public rbk_decision_detail[] rbk_decision_list { get; set; }
    [XmlElement("rbk_u2_roombooking_list")]
    public rbk_u2_roombooking_detail[] rbk_u2_roombooking_list { get; set; }
    
    public rbk_search_report_room_detail[] rbk_search_report_room_list { get; set; }
    public rbk_m0_conference_detail[] rbk_m0_conference_list { get; set; }

}


public class rbk_m0_place_detail
{
    public int place_idx { get; set; }
    public string place_name { get; set; }
    public string place_code { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int place_status { get; set; }
    public int condition { get; set; }
}

public class rbk_m0_equiment_detail
{
    
    public int equiment_idx { get; set; }
    public string equiment_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int equiment_status { get; set; }
    public int condition { get; set; }

}

public class rbk_m0_conference_detail
{
    public int m0_conidx { get; set; }
    public string people_desc { get; set; }
    public string conference_name { get; set; }
    public int pic_check { get; set; }
    public int sound_check { get; set; }
    public int qty_mic { get; set; }
    public int constatus { get; set; }
    public int cemp_idx { get; set; }
    public int condition { get; set; }
}


public class rbk_m0_result_use_detail
{

    public int result_use_idx { get; set; }
    public string result_use_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int result_use_status { get; set; }
    public int condition { get; set; }

}

public class rbk_m0_food_detail
{
   
    public int food_idx { get; set; }
    public string food_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int food_status { get; set; }
    public int condition { get; set; }

}

public class rbk_m0_room_detail
{
    
    public int m0_room_idx { get; set; }
    public string room_name_th { get; set; }
    public string room_name_en { get; set; }
    public string room_detail { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int m0_room_status { get; set; }
    public int condition { get; set; }
    public int place_idx { get; set; }
    public int emp_idx { get; set; }
    public int capacity_people { get; set; }

}

public class rbk_m1_room_detail
{

    public int m1_room_idx { get; set; }
    public int m0_room_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int m1_room_status { get; set; }
    public int condition { get; set; }
    public int equiment_idx { get; set; }
    public string equiment_name { get; set; }

}

public class rbk_search_room_detail
{

    public int m0_room_idx { get; set; }
    public int m1_room_idx { get; set; }
    public string room_name_th { get; set; }
    public string room_name_en { get; set; }
    public string room_detail { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int m0_room_status { get; set; }
    public int condition { get; set; }
    public int place_idx { get; set; }
    public string place_name { get; set; }
    public int emp_idx { get; set; }
    public int capacity_people { get; set; }
    public string date_start { get; set; }
    public string date_end { get; set; }
    public string time_start { get; set; }
    public string time_end { get; set; }
    public int type_booking_idx { get; set; }
    public string type_booking_name { get; set; }
    public string m0_room_idx_check { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int staidx { get; set; }
    public int IFSearchbetween { get; set; }
    public int rpos_idx { get; set; }

}

public class rbk_check_room_detail
{

    public int m0_room_idx { get; set; }
    public int m1_room_idx { get; set; }
    public string room_name_th { get; set; }
    public string room_name_en { get; set; }
    public string room_detail { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int m0_room_status { get; set; }
    public int condition { get; set; }
    public int place_idx { get; set; }
    public string place_name { get; set; }
    public int emp_idx { get; set; }
    public int capacity_people { get; set; }
    public string date_end { get; set; }
    public string time_start { get; set; }
    public string time_end { get; set; }
    public int type_search { get; set; }
    public string m0_room_idx_check { get; set; }

}

[Serializable]
public class rbk_u0_roombooking_detail
{

    [XmlElement("m0_room_idx")]
    public int m0_room_idx { get; set; }

    [XmlElement("m1_room_idx")]
    public int m1_room_idx { get; set; }

    [XmlElement("room_name_th")]
    public string room_name_th { get; set; }

    [XmlElement("room_name_en")]
    public string room_name_en { get; set; }

    [XmlElement("room_detail")]
    public string room_detail { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_room_status")]
    public int m0_room_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("capacity_people")]
    public int capacity_people { get; set; }

    [XmlElement("date_start")]
    public string date_start { get; set; }

    [XmlElement("date_end")]
    public string date_end { get; set; }

    [XmlElement("time_start")]
    public string time_start { get; set; }

    [XmlElement("time_end")]
    public string time_end { get; set; }

    [XmlElement("type_search")]
    public int type_search { get; set; }

    [XmlElement("m0_room_idx_check")]
    public string m0_room_idx_check { get; set; }

    [XmlElement("type_booking_idx")]
    public int type_booking_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("people_capacity")]
    public int people_capacity { get; set; }

    [XmlElement("detail_booking")]
    public string detail_booking { get; set; }

    [XmlElement("topic_booking")]
    public string topic_booking { get; set; }

    [XmlElement("result_use_idx")]
    public int result_use_idx { get; set; }

    [XmlElement("count_people")]
    public int count_people { get; set; }

    [XmlElement("current_status")]
    public string current_status { get; set; }

    [XmlElement("type_booking_name")]
    public string type_booking_name { get; set; }

    [XmlElement("u0_document_idx")]
    public int u0_document_idx { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("result_use_name")]
    public string result_use_name { get; set; }

    [XmlElement("Count_WaitApprove")]
    public int Count_WaitApprove { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("status_booking")]
    public string status_booking { get; set; }

    [XmlElement("date_time_chekbutton")]
    public string date_time_chekbutton { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_email_sentto_hr")]
    public string emp_email_sentto_hr { get; set; }

    [XmlElement("decision_name_hr")]
    public string decision_name_hr { get; set; }

    [XmlElement("detail_foodbooking")]
    public string detail_foodbooking { get; set; }

    [XmlElement("food_detail_room")]
    public string food_detail_room { get; set; }

    [XmlElement("admin_idx")]
    public int admin_idx { get; set; }

    [XmlElement("time_create_date")]
    public string time_create_date { get; set; }

    [XmlElement("type_room")]
    public int type_room { get; set; }

    [XmlElement("emp_mobile_no")]
    public int emp_mobile_no { get; set; }

    [XmlElement("m0_conidx")]
    public int m0_conidx { get; set; }

}

public class rbk_u1_roombooking_detail
{
    [XmlElement("u0_document_idx")]
    public int u0_document_idx { get; set; }

    [XmlElement("u1_document_idx")]
    public int u1_document_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("current_artor")]
    public string current_artor { get; set; }

    [XmlElement("current_decision")]
    public string current_decision { get; set; }

    [XmlElement("current_status")]
    public string current_status { get; set; }

    [XmlElement("current_status_name")]
    public string current_status_name { get; set; }

    [XmlElement("m0_room_idx")]
    public int m0_room_idx { get; set; }

    [XmlElement("m1_room_idx")]
    public int m1_room_idx { get; set; }

    [XmlElement("room_name_th")]
    public string room_name_th { get; set; }

    [XmlElement("room_name_en")]
    public string room_name_en { get; set; }

    [XmlElement("room_detail")]
    public string room_detail { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_room_status")]
    public int m0_room_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("capacity_people")]
    public int capacity_people { get; set; }

    [XmlElement("date_start")]
    public string date_start { get; set; }

    [XmlElement("date_end")]
    public string date_end { get; set; }

    [XmlElement("time_start")]
    public string time_start { get; set; }

    [XmlElement("time_end")]
    public string time_end { get; set; }

    [XmlElement("type_search")]
    public int type_search { get; set; }

    [XmlElement("m0_room_idx_check")]
    public string m0_room_idx_check { get; set; }

    [XmlElement("type_booking_idx")]
    public int type_booking_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("people_capacity")]
    public int people_capacity { get; set; }

    [XmlElement("detail_booking")]
    public string detail_booking { get; set; }

    [XmlElement("topic_booking")]
    public string topic_booking { get; set; }

    [XmlElement("result_use_idx")]
    public int result_use_idx { get; set; }

    [XmlElement("count_people")]
    public int count_people { get; set; }

    [XmlElement("type_booking_name")]
    public string type_booking_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("result_use_name")]
    public string result_use_name { get; set; }

    [XmlElement("Count_WaitApprove")]
    public int Count_WaitApprove { get; set; }

    [XmlElement("status_booking")]
    public string status_booking { get; set; }

    [XmlElement("date_time_chekbutton")]
    public string date_time_chekbutton { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_email_sentto_hr")]
    public string emp_email_sentto_hr { get; set; }

    [XmlElement("decision_name_hr")]
    public string decision_name_hr { get; set; }

}

public class rbk_decision_detail
{

    public int decision_idx { get; set; }
    public int noidx { get; set; }
    public string decision_name { get; set; }
    public string decision_desc { get; set; }
    public int decision_status { get; set; }
    public int staidx { get; set; }
    public string status_name { get; set; }

}

[Serializable]
public class rbk_u2_roombooking_detail
{

    [XmlElement("u0_document_idx")]
    public int u0_document_idx { get; set; }

    [XmlElement("u1_document_idx")]
    public int u1_document_idx { get; set; }

    [XmlElement("u2_document_idx")]
    public int u2_document_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("food_idx")]
    public int food_idx { get; set; }

    [XmlElement("food_name")]
    public string food_name { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("u2_status")]
    public int u2_status { get; set; }


}

public class rbk_search_report_room_detail
{

    public int m0_room_idx { get; set; }
    public int m1_room_idx { get; set; }
    public string room_name_th { get; set; }
    public string room_name_en { get; set; }
    public string room_detail { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int m0_room_status { get; set; }
    public int condition { get; set; }
    public int place_idx { get; set; }
    public string place_name { get; set; }
    public int emp_idx { get; set; }
    public int capacity_people { get; set; }
    public string date_start { get; set; }
    public string date_end { get; set; }
    public string time_start { get; set; }
    public string time_end { get; set; }
    public int type_booking_idx { get; set; }
    public string type_booking_name { get; set; }
    public string m0_room_idx_check { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int staidx { get; set; }
    public int IFSearchbetween { get; set; }
    public int rpos_idx { get; set; }
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int count_booking { get; set; }
    public string dept_name_th { get; set; }
    public string DC_YEAR { get; set; }
    public string DC_Mount { get; set; }

}

