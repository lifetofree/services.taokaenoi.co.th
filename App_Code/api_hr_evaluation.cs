﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;


public class api_hr_evaluation : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    service_mail _service_mail = new service_mail();
    data_hr_evaluation _dtitseet = new data_hr_evaluation();

    int _tempInt = 0;
    string _mail_subject = "";
    string _mail_body = "";
    string _link_daily = "http://mas.taokaenoi.co.th/hr-perf-evalu-employee";
    string _link_monthly = "http://mas.taokaenoi.co.th/hr-perf-evalu-employee-monthly";
    string _urlGetits_u_document = "";

    public api_hr_evaluation()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    

    /****** start Master Data ******/
  
    /****** End Master Data ******/

    // start lookup
    /*
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getits_lookup(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_itasset", "service_itasset", _xml_in, 1001); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    */
    //end lookup

    //start transection

    /****** Start its_u_document_action ******/
    // select // GET: 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getits_u_hr_evaluation(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_hr_evaluation", "service_hr_perf_evalu_emp", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsits_u_hr_evaluation(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_hr_evaluation", "service_hr_perf_evalu_emp", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // Update // put 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdits_u_hr_evaluation(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_hr_evaluation", "service_hr_perf_evalu_emp", _xml_in, 230); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delits_u_hr_evaluation(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_hr_evaluation", "service_hr_perf_evalu_emp", _xml_in, 290); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //_monthly

    // select // GET: 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getits_u_hr_evaluation_monthly(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_hr_evaluation", "service_hr_perf_evalu_emp", _xml_in, 320); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsits_u_hr_evaluation_monthly(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_hr_evaluation", "service_hr_perf_evalu_emp", _xml_in, 310); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // Update // put 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdits_u_hr_evaluation_monthly(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_hr_evaluation", "service_hr_perf_evalu_emp", _xml_in, 330); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delits_u_hr_evaluation_monthly(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_hr_evaluation", "service_hr_perf_evalu_emp", _xml_in, 390); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // send email 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmailhr_evaluation(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            data_hr_evaluation _data = new data_hr_evaluation();
            _data = (data_hr_evaluation)_funcTool.convertXmlToObject(typeof(data_hr_evaluation), _xml_in);
            if (_data.hr_perf_evalu_emp_action != null)
            {
                foreach (var item in _data.hr_perf_evalu_emp_action)
                {
                    if (item.operation_status_id == "daily")
                    {
                        int u0idx = item.u0idx;
                        settemplate_hr_daily(u0idx);
                        _data = settemplate_hr_daily_toleader(u0idx);
                    }
                    else if (item.operation_status_id == "daily_unconfirm")
                    {
                        int u0idx = item.u0idx;
                        _data = settemplate_hr_daily_toleader_unconfirm(u0idx);
                    }
                    else if (item.operation_status_id == "monthly")
                    {
                        int u0idx = item.u0idx;
                        settemplate_hr_monthly(u0idx);
                        _data = settemplate_hr_monthly_toleader(u0idx);
                    }
                    else if (item.operation_status_id == "monthly_unconfirm")
                    {
                        int u0idx = item.u0idx;
                        _data = settemplate_hr_monthly_toleader_unconfirm(u0idx);
                    }

                }
            }
            _ret_val = _funcTool.convertObjectToJson(_data);
            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }

    //start email

    protected data_hr_evaluation callServicePostPerfEvaluEmp_daily( data_hr_evaluation _dt)
    {
        //// convert to json
        _xml_in = _funcTool.convertObjectToXml(_dt);

        _ret_val = _serviceExec.actionExec("masConn", "data_hr_evaluation", "service_hr_perf_evalu_emp", _xml_in, 220); // return w/ json

        _xml_in = _funcTool.convertJsonToXml(_ret_val);
        ////// convert json to object
        _dt = (data_hr_evaluation)_funcTool.convertXmlToObject(typeof(data_hr_evaluation), _xml_in);

        return _dt;
    }

    private data_hr_evaluation settemplate_hr_daily(int id)
    {
        data_hr_evaluation _data = new data_hr_evaluation();
        _data.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        hr_perf_evalu_emp obj_document = new hr_perf_evalu_emp();
        obj_document.operation_status_id = "list_th_send_email";
        obj_document.u0idx = id;
        _data.hr_perf_evalu_emp_action[0] = obj_document;
        _data = callServicePostPerfEvaluEmp_daily(_data);
        if (_data.hr_perf_evalu_emp_action != null)
        {
            string emailmove = "";
            string replyempmove = "seniordeveloper@taokaenoi.co.th";
            string title = "ประเมินผลการปฏิบัติงานระดับพนักงานรายวัน";
            _mail_subject = "[HR:ประเมินผลการปฏิบัติงานระดับพนักงานรายวัน]";
            emailmove =  getEmail_employee(id,1);
            _mail_body = databody_daily(_data.hr_perf_evalu_emp_action[0], title, _link_daily);
            SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);
        }
        return _data;

    }

    private data_hr_evaluation settemplate_hr_daily_toleader(int id)
    {
        data_hr_evaluation _data = new data_hr_evaluation();
        _data.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        hr_perf_evalu_emp obj_document = new hr_perf_evalu_emp();
        obj_document.operation_status_id = "list_th_send_email";
        obj_document.u0idx = id;
        _data.hr_perf_evalu_emp_action[0] = obj_document;
        _data = callServicePostPerfEvaluEmp_daily(_data);
        if (_data.hr_perf_evalu_emp_action != null)
        {
            string emailmove = "";
            string replyempmove = "seniordeveloper@taokaenoi.co.th";
            string title = "ประเมินผลการปฏิบัติงานระดับพนักงานรายวัน";
            _mail_subject = "[HR:ประเมินผลการปฏิบัติงานระดับพนักงานรายวัน]";
            string remark = "ข้อมูลส่ง HR แล้วไม่สามารถแก้ไขข้อมูลได้";
            emailmove =  getEmail_employee(id,2);
            _mail_body = databody_daily_toleader(_data.hr_perf_evalu_emp_action[0], title, _link_daily, remark);
            SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);
        }
        return _data;

    }

    private data_hr_evaluation settemplate_hr_daily_toleader_unconfirm(int id)
    {
        data_hr_evaluation _data = new data_hr_evaluation();
        _data.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        hr_perf_evalu_emp obj_document = new hr_perf_evalu_emp();
        obj_document.operation_status_id = "list_th_send_email_unconfirm";
        obj_document.u0idx = id;
        _data.hr_perf_evalu_emp_action[0] = obj_document;
        _data = callServicePostPerfEvaluEmp_daily(_data);
        if (_data.hr_perf_evalu_emp_action != null)
        {
            string emailmove = "";
            string replyempmove = "seniordeveloper@taokaenoi.co.th";
            string title = "ประเมินผลการปฏิบัติงานระดับพนักงานรายวัน";
            _mail_subject = "[HR:ประเมินผลการปฏิบัติงานระดับพนักงานรายวัน]";
            string remark = "HR ได้ยกเลิกการส่งข้อมูลแล้วสามารถแก้ไขข้อมูลได้";
            emailmove = getEmail_employee(id, 2);
            _mail_body = databody_daily_toleader(_data.hr_perf_evalu_emp_action[0], title, _link_daily, remark);
            SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);
        }
        return _data;

    }


    protected data_hr_evaluation callServicePostPerfEvaluEmp_monthly(data_hr_evaluation _dt)
    {
        //// convert to json
        _xml_in = _funcTool.convertObjectToXml(_dt);

        _ret_val = _serviceExec.actionExec("masConn", "data_hr_evaluation", "service_hr_perf_evalu_emp", _xml_in, 320); // return w/ json

        _xml_in = _funcTool.convertJsonToXml(_ret_val);
        ////// convert json to object
        _dt = (data_hr_evaluation)_funcTool.convertXmlToObject(typeof(data_hr_evaluation), _xml_in);

        return _dt;
    }

    private data_hr_evaluation settemplate_hr_monthly(int id)
    {
        data_hr_evaluation _data = new data_hr_evaluation();
        _data.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        hr_perf_evalu_emp_monthly obj_document = new hr_perf_evalu_emp_monthly();
        obj_document.operation_status_id = "list_th_send_email";
        obj_document.u0idx = id;
        _data.hr_perf_evalu_emp_monthly_action[0] = obj_document;
        _data = callServicePostPerfEvaluEmp_monthly(_data);
        if (_data.hr_perf_evalu_emp_monthly_action != null)
        {
            string emailmove = "";
            string replyempmove = "seniordeveloper@taokaenoi.co.th";
            string title = "ประเมินผลการปฏิบัติงานระดับพนักงานรายเดือน";
            _mail_subject = "[HR:ประเมินผลการปฏิบัติงานระดับพนักงานรายเดือน]";
            emailmove =  getEmail_employee(id,1);
            _mail_body = databody_monthly(_data.hr_perf_evalu_emp_monthly_action[0], title, _link_monthly);
            SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);
        }
        return _data;

    }

    private data_hr_evaluation settemplate_hr_monthly_toleader(int id)
    {
        data_hr_evaluation _data = new data_hr_evaluation();
        _data.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        hr_perf_evalu_emp_monthly obj_document = new hr_perf_evalu_emp_monthly();
        obj_document.operation_status_id = "list_th_send_email";
        obj_document.u0idx = id;
        _data.hr_perf_evalu_emp_monthly_action[0] = obj_document;
        _data = callServicePostPerfEvaluEmp_monthly(_data);
        if (_data.hr_perf_evalu_emp_monthly_action != null)
        {
            string emailmove = "";
            string replyempmove = "seniordeveloper@taokaenoi.co.th";
            string title = "ประเมินผลการปฏิบัติงานระดับพนักงานรายเดือน";
            _mail_subject = "[HR:ประเมินผลการปฏิบัติงานระดับพนักงานรายเดือน]";
            string remark = "ข้อมูลส่ง HR แล้วไม่สามารถแก้ไขข้อมูลได้";
            emailmove =   getEmail_employee(id,3);
            _mail_body = databody_monthly_toleader(_data.hr_perf_evalu_emp_monthly_action[0], title, _link_monthly, remark);
            SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);
        }
        return _data;

    }

    private data_hr_evaluation settemplate_hr_monthly_toleader_unconfirm(int id)
    {
        data_hr_evaluation _data = new data_hr_evaluation();
        _data.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        hr_perf_evalu_emp_monthly obj_document = new hr_perf_evalu_emp_monthly();
        obj_document.operation_status_id = "list_th_send_email_unconfirm";
        obj_document.u0idx = id;
        _data.hr_perf_evalu_emp_monthly_action[0] = obj_document;
        _data = callServicePostPerfEvaluEmp_monthly(_data);
        if (_data.hr_perf_evalu_emp_monthly_action != null)
        {
            string emailmove = "";
            string replyempmove = "seniordeveloper@taokaenoi.co.th";
            string title = "ประเมินผลการปฏิบัติงานระดับพนักงานรายเดือน";
            _mail_subject = "[HR:ประเมินผลการปฏิบัติงานระดับพนักงานรายเดือน]";
            string remark = "HR ได้ยกเลิกการส่งข้อมูลแล้วสามารถแก้ไขข้อมูลได้";
            emailmove =  getEmail_employee(id, 3);
            _mail_body = databody_monthly_toleader(_data.hr_perf_evalu_emp_monthly_action[0], title, _link_monthly, remark);
            SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);
        }
        return _data;

    }

    public string databody_monthly(hr_perf_evalu_emp_monthly create_list, string title, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_hr_perf_evalu_employee.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{title_name}", title);
        body = body.Replace("{document_code}", create_list.doccode);
        body = body.Replace("{document_date}", create_list.zdocdate);
        body = body.Replace("{emp_name_actor}", create_list.u0_emp_name_th);
        body = body.Replace("{dept_name}", create_list.u0_dept_name_th);
        body = body.Replace("{node_status}", getStatus(create_list.u0_status));
        body = body.Replace("{details_name}", create_list.remark);
        body = body.Replace("{link}", link);

        return body;
    }

    public string databody_monthly_toleader(hr_perf_evalu_emp_monthly create_list, string title, string link, string remark)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_hr_perf_evalu_employee_toleader.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{title_name}", title);
        body = body.Replace("{document_code}", create_list.doccode);
        body = body.Replace("{document_date}", create_list.zdocdate);
        body = body.Replace("{emp_name_actor}", create_list.u0_emp_name_th);
        body = body.Replace("{dept_name}", create_list.u0_dept_name_th);
        body = body.Replace("{node_status}", getStatus(create_list.u0_status));
        body = body.Replace("{details_name}", remark);
        body = body.Replace("{link}", link);

        return body;
    }

    public string databody_daily(hr_perf_evalu_emp create_list, string title, string link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_hr_perf_evalu_employee.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{title_name}", title);
        body = body.Replace("{document_code}", create_list.doccode);
        body = body.Replace("{document_date}", create_list.zdocdate);
        body = body.Replace("{emp_name_actor}", create_list.u0_emp_name_th);
        body = body.Replace("{dept_name}", create_list.u0_dept_name_th);
        body = body.Replace("{node_status}", getStatus(create_list.u0_status));
        body = body.Replace("{details_name}", create_list.remark);
        body = body.Replace("{link}", link);

        return body;
    }
    public string databody_daily_toleader(hr_perf_evalu_emp create_list, string title, string link,string remark)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_hr_perf_evalu_employee_toleader.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{title_name}", title);
        body = body.Replace("{document_code}", create_list.doccode);
        body = body.Replace("{document_date}", create_list.zdocdate);
        body = body.Replace("{emp_name_actor}", create_list.u0_emp_name_th);
        body = body.Replace("{dept_name}", create_list.u0_dept_name_th);
        body = body.Replace("{node_status}", getStatus(create_list.u0_status));
        body = body.Replace("{details_name}", remark);
        body = body.Replace("{link}", link);

        return body;
    }
    protected string getStatus(int status)
    {

        if (status == 2)
        {
            // return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
            return "จบการดำเนินการและส่งข้อมูลให้ HR";
        }
        {

            // return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
            return "ดำเนินการสร้าง / แก้ไข";
        }

    }
    private string getEmail_employee(int id,int istatus)
    {
        string str = "";

        //if (istatus == 2)
        //{
        //    data_hr_evaluation _data = new data_hr_evaluation();
        //    _data.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        //    hr_perf_evalu_emp obj_document = new hr_perf_evalu_emp();
        //    obj_document.operation_status_id = "hr_send_email_leader";
        //    obj_document.u0idx = id;
        //    _data.hr_perf_evalu_emp_action[0] = obj_document;
        //    _data = callServicePostPerfEvaluEmp_daily(_data);
        //    if (_data.hr_perf_evalu_emp_action != null)
        //    {
        //        int ic = 0;
        //        foreach (var item in _data.hr_perf_evalu_emp_action)
        //        {

        //            if (ic == 0)
        //            {
        //                str = item.emp_email;
        //            }
        //            else
        //            {
        //                str = str + " , " + item.emp_email;
        //            }

        //            ic++;
        //        }
        //    }
        //}
        //if (istatus == 3)
        //{
        //    data_hr_evaluation _data = new data_hr_evaluation();
        //    _data.hr_perf_evalu_emp_monthly_action = new hr_perf_evalu_emp_monthly[1];
        //    hr_perf_evalu_emp_monthly obj_document = new hr_perf_evalu_emp_monthly();
        //    obj_document.operation_status_id = "hr_send_email_leader";
        //    obj_document.u0idx = id;
        //    _data.hr_perf_evalu_emp_monthly_action[0] = obj_document;
        //    _data = callServicePostPerfEvaluEmp_monthly(_data);
        //    if (_data.hr_perf_evalu_emp_monthly_action != null)
        //    {
        //        int ic = 0;
        //        foreach (var item in _data.hr_perf_evalu_emp_monthly_action)
        //        {

        //            if (ic == 0)
        //            {
        //                str = item.emp_email;
        //            }
        //            else
        //            {
        //                str = str + " , " + item.emp_email;
        //            }

        //            ic++;
        //        }
        //    }
        //}
        //else
        //{
        //    data_hr_evaluation _data = new data_hr_evaluation();
        //    _data.hr_perf_evalu_emp_action = new hr_perf_evalu_emp[1];
        //    hr_perf_evalu_emp obj_document = new hr_perf_evalu_emp();
        //    obj_document.operation_status_id = "hr_send_email";
        //    obj_document.u0idx = id;
        //    _data.hr_perf_evalu_emp_action[0] = obj_document;
        //    _data = callServicePostPerfEvaluEmp_daily(_data);
        //    if (_data.hr_perf_evalu_emp_action != null)
        //    {
        //        int ic = 0;
        //        foreach (var item in _data.hr_perf_evalu_emp_action)
        //        {

        //            if (ic == 0)
        //            {
        //                str = item.emp_email;
        //            }
        //            else
        //            {
        //                str = str + " , " + item.emp_email;
        //            }

        //            ic++;
        //        }
        //    }

        //}

        return "seniordeveloper@taokaenoi.co.th";
    }

    public void SendHtmlFormattedEmailFull(string recepientEmailList, string recepientCcEmailList, string replyToEmailList, string subject, string body)
    {
        using (MailMessage mailMessage = new MailMessage())
        {
            mailMessage.From = new MailAddress("noreply@taokaenoi.co.th");
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            // set to mail list
            string[] recepientEmail = recepientEmailList.Split(',');
            foreach (string rEmail in recepientEmail)
            {
                if (rEmail != String.Empty)
                {
                    mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                }
            }
            // set cc mail list
            string[] recepientCcEmail = recepientCcEmailList.Split(',');
            foreach (string cEmail in recepientCcEmail)
            {
                if (cEmail != String.Empty)
                {
                    mailMessage.CC.Add(new MailAddress(cEmail)); //Adding Multiple To email Id
                }
            }
            //set reply to list
            string[] replyToEmail = replyToEmailList.Split(',');
            foreach (string rtEmail in replyToEmail)
            {
                if (rtEmail != String.Empty)
                {
                    mailMessage.ReplyToList.Add(new MailAddress(rtEmail)); //Adding Multiple To email Id
                }
            }
            mailMessage.Bcc.Add(new MailAddress("noreply@taokaenoi.co.th"));
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;
            System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
            NetworkCred.UserName = "noreply@taokaenoi.co.th";
            NetworkCred.Password = "knlolksosgsuakcd";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mailMessage);

        }
    }


}
