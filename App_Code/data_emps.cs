﻿using System;
using System.Xml.Serialization;

#region data emps

[Serializable]
[XmlRoot("data_employee")]
public class data_emps
{
   public int return_code { get; set; }
   public string return_msg { get; set; }
   public service_announce[] emps_service_announce_action { get; set; }
   [XmlElement("emps_empshift_action")]
   public empshift[] emps_empshift_action { get; set; }
}
#endregion data emps

#region empshift
[Serializable]
public class empshift
{
    [XmlElement("u0_empshift_idx")]
    public int u0_empshift_idx { get; set; }
    [XmlElement("m0_parttime_idx_ref")]
    public int m0_parttime_idx_ref { get; set; }
    [XmlElement("emp_idx_ref")]
    public int emp_idx_ref { get; set; }
    [XmlElement("empshift_status")]
    public int empshift_status { get; set; }
    [XmlElement("empshift_created_at")]
    public string empshift_created_at { get; set; }
    [XmlElement("empshift_created_by")]
    public int empshift_created_by { get; set; }
    [XmlElement("empshift_updated_at")]
    public string empshift_updated_at { get; set; }

    [XmlElement("m0_parttime_idx")]
    public int m0_parttime_idx { get; set; }
    [XmlElement("m0_reward_idx_ref")]
    public int m0_reward_idx_ref { get; set; }
    [XmlElement("parttime_code")]
    public string parttime_code { get; set; }
    [XmlElement("parttime_name_th")]
    public string parttime_name_th { get; set; }
    [XmlElement("parttime_name_eng")]
    public string parttime_name_eng { get; set; }
    [XmlElement("parttime_note")]
    public string parttime_note { get; set; }
    [XmlElement("parttime_bg_color")]
    public string parttime_bg_color { get; set; }
    [XmlElement("parttime_name_and_color")]
    public string parttime_name_and_color { get; set; }
    [XmlElement("parttime_start_datetime")]
    public string parttime_start_datetime { get; set; }
    [XmlElement("parttime_end_datetime")]
    public string parttime_end_datetime { get; set; }
    [XmlElement("parttime_btw_start_datetime")]
    public string parttime_btw_start_datetime { get; set; }
    [XmlElement("parttime_btw_end_datetime")]
    public string parttime_btw_end_datetime { get; set; }
    [XmlElement("parttime_break_start_datetime")]
    public string parttime_break_start_datetime { get; set; }
    [XmlElement("parttime_break_end_datetime")]
    public string parttime_break_end_datetime { get; set; }
    [XmlElement("parttime_amount_scan_card")]
    public int parttime_amount_scan_card { get; set; }
    [XmlElement("parttime_time_stand")]
    public int parttime_time_stand { get; set; }
    [XmlElement("parttime_amount_work_hours")]
    public float parttime_amount_work_hours { get; set; }
    [XmlElement("parttime_amount_break_hours")]
    public float parttime_amount_break_hours { get; set; }
    [XmlElement("parttime_status")]
    public int parttime_status { get; set; }
    [XmlElement("parttime_created_at")]
    public string parttime_created_at { get; set; }
    [XmlElement("parttime_created_by")]
    public int parttime_created_by { get; set; }
    [XmlElement("parttime_updated_at")]
    public string parttime_updated_at { get; set; }

    /** Start convert Date Time to Show */
    [XmlElement("reward_name")]
    public string reward_name { get; set; }
    [XmlElement("pt_start_date_only")]
    public string pt_start_date_only { get; set; }
    [XmlElement("pt_end_date_only")]
    public string pt_end_date_only { get; set; }
    [XmlElement("pt_start_date")]
    public string pt_start_date { get; set; }
    [XmlElement("pt_start_time")]
    public string pt_start_time { get; set; }
    [XmlElement("pt_end_date")]
    public string pt_end_date { get; set; }
    [XmlElement("pt_end_time")]
    public string pt_end_time { get; set; }
    [XmlElement("pt_btw_start_date")]
    public string pt_btw_start_date { get; set; }
    [XmlElement("pt_btw_start_time")]
    public string pt_btw_start_time { get; set; }
    [XmlElement("pt_btw_end_date")]
    public string pt_btw_end_date { get; set; }
    [XmlElement("pt_btw_end_time")]
    public string pt_btw_end_time { get; set; }
    [XmlElement("pt_break_start_date")]
    public string pt_break_start_date { get; set; }
    [XmlElement("pt_break_start_time")]
    public string pt_break_start_time { get; set; }
    [XmlElement("pt_break_end_date")]
    public string pt_break_end_date { get; set; }
    [XmlElement("pt_break_end_time")]
    public string pt_break_end_time { get; set; }
    [XmlElement("status_time_stand")]
    public string status_time_stand { get; set; }

    [XmlElement("announce_diary_date_start")]
    public string announce_diary_date_start { get; set; }
    [XmlElement("announce_diary_date_end")]
    public string announce_diary_date_end { get; set; }
    [XmlElement("m0_group_rsec_idx_ref")]
    public string m0_group_rsec_idx_ref { get; set; }
    /** End convert Date Time to Show */

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("Approve_status")]
    public int Approve_status { get; set; }

    [XmlElement("comment_approve")]
    public string comment_approve { get; set; }

    [XmlElement("u0_unidx")]
    public int u0_unidx { get; set; }

    [XmlElement("u0_acidx")]
    public int u0_acidx { get; set; }

    [XmlElement("u0_doc_decision")]
    public int u0_doc_decision { get; set; }

    [XmlElement("type_action")]
    public int type_action { get; set; }

    [XmlElement("emp_createdate")]
    public string emp_createdate { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("m0_group_diary_idx")]
    public int m0_group_diary_idx { get; set; }
    [XmlElement("group_diary_name")]
    public string group_diary_name { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("countdown")]
    public int countdown { get; set; }
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("holiday")]
    public string holiday { get; set; }
    [XmlElement("day_1")]
    public string day_1 { get; set; }
    [XmlElement("day_2")]
    public string day_2 { get; set; }
    [XmlElement("day_3")]
    public string day_3 { get; set; }
    [XmlElement("day_4")]
    public string day_4 { get; set; }
    [XmlElement("day_5")]
    public string day_5 { get; set; }
    [XmlElement("day_6")]
    public string day_6 { get; set; }
    [XmlElement("day_7")]
    public string day_7 { get; set; }
    [XmlElement("day_8")]
    public string day_8 { get; set; }
    [XmlElement("day_9")]
    public string day_9 { get; set; }
    [XmlElement("day_10")]
    public string day_10 { get; set; }
    [XmlElement("day_11")]
    public string day_11 { get; set; }
    [XmlElement("day_12")]
    public string day_12 { get; set; }
    [XmlElement("day_13")]
    public string day_13 { get; set; }
    [XmlElement("day_14")]
    public string day_14 { get; set; }
    [XmlElement("day_15")]
    public string day_15 { get; set; }
    [XmlElement("day_16")]
    public string day_16 { get; set; }
    [XmlElement("day_17")]
    public string day_17 { get; set; }
    [XmlElement("day_18")]
    public string day_18 { get; set; }
    [XmlElement("day_19")]
    public string day_19 { get; set; }
    [XmlElement("day_20")]
    public string day_20 { get; set; }
    [XmlElement("day_21")]
    public string day_21 { get; set; }
    [XmlElement("day_22")]
    public string day_22 { get; set; }
    [XmlElement("day_23")]
    public string day_23 { get; set; }
    [XmlElement("day_24")]
    public string day_24 { get; set; }
    [XmlElement("day_25")]
    public string day_25 { get; set; }
    [XmlElement("day_26")]
    public string day_26 { get; set; }
    [XmlElement("day_27")]
    public string day_27 { get; set; }
    [XmlElement("day_28")]
    public string day_28 { get; set; }
    [XmlElement("day_29")]
    public string day_29 { get; set; }
    [XmlElement("day_30")]
    public string day_30 { get; set; }
    [XmlElement("day_31")]
    public string day_31 { get; set; }

    [XmlElement("check_1")]
    public string check_1 { get; set; }
    [XmlElement("check_2")]
    public string check_2 { get; set; }
    [XmlElement("check_3")]
    public string check_3 { get; set; }
    [XmlElement("check_4")]
    public string check_4 { get; set; }
    [XmlElement("check_5")]
    public string check_5 { get; set; }
    [XmlElement("check_6")]
    public string check_6 { get; set; }
    [XmlElement("check_7")]
    public string check_7 { get; set; }
    [XmlElement("check_8")]
    public string check_8 { get; set; }
    [XmlElement("check_9")]
    public string check_9 { get; set; }
    [XmlElement("check_10")]
    public string check_10 { get; set; }
    [XmlElement("check_11")]
    public string check_11 { get; set; }
    [XmlElement("check_12")]
    public string check_12 { get; set; }
    [XmlElement("check_13")]
    public string check_13 { get; set; }
    [XmlElement("check_14")]
    public string check_14 { get; set; }
    [XmlElement("check_15")]
    public string check_15 { get; set; }
    [XmlElement("check_16")]
    public string check_16 { get; set; }
    [XmlElement("check_17")]
    public string check_17 { get; set; }
    [XmlElement("check_18")]
    public string check_18 { get; set; }
    [XmlElement("check_19")]
    public string check_19 { get; set; }
    [XmlElement("check_20")]
    public string check_20 { get; set; }
    [XmlElement("check_21")]
    public string check_21 { get; set; }
    [XmlElement("check_22")]
    public string check_22 { get; set; }
    [XmlElement("check_23")]
    public string check_23 { get; set; }
    [XmlElement("check_24")]
    public string check_24 { get; set; }
    [XmlElement("check_25")]
    public string check_25 { get; set; }
    [XmlElement("check_26")]
    public string check_26 { get; set; }
    [XmlElement("check_27")]
    public string check_27 { get; set; }
    [XmlElement("check_28")]
    public string check_28 { get; set; }
    [XmlElement("check_29")]
    public string check_29 { get; set; }
    [XmlElement("check_30")]
    public string check_30 { get; set; }
    [XmlElement("check_31")]
    public string check_31 { get; set; }

    [XmlElement("month")]
    public string month { get; set; }
    [XmlElement("year")]
    public string year { get; set; }

}
#endregion empshift


#region service_announce
public class service_announce
{
   public int organization_idx { get; set; }
   public int department_idx { get; set; }
   public int u0_announce_idx { get; set; }
   public int u0_node_idx_ref { get; set; }
   public string announce_start { get; set; }
   public string announce_end { get; set; }
   public string announce_created_at { get; set; }
   public string announce_updated_at { get; set; }
   public int announce_updated_by { get; set; }
   public string count_emp { get; set; }
   public int emp_idx_ref { get; set; }
   public string EmpCode { get; set; }
   public string JobLevel { get; set; }
   public string FullNameTH { get; set; }
   public string OrgNameTH { get; set; }
   public string PosNameTH { get; set; }
   public string SecNameTH { get; set; }
   public string DeptNameTH { get; set; }
   public string NameApprover1 { get; set; }
   public string NameApprover2 { get; set; }
   public string announce_created_at_convert { get; set; }
   public string from_node_name { get; set; }
   public string to_node_name { get; set; }
   public string from_actor_name { get; set; }
   public string to_actor_name { get; set; }
   public string node_decision_statnode { get; set; }
   public string m0_statnode_statnode_ref { get; set; }
}
#endregion service_announce
