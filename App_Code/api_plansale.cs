﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

/// <summary>
/// Summary description for api_employee
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_plansale : System.Web.Services.WebService
{
   string _xml_in = "";
   string _ret_val = "";
   string _local_xml = String.Empty;
   string _mail_subject = "";
   string _mail_body = "";
   function_tool _funcTool = new function_tool();
   service_execute _serviceExec = new service_execute();
   data_plansale dataPlansale = new data_plansale();
   service_mail _serviceMail = new service_mail();

    public api_plansale()
   {
      //Uncomment the following line if using designed components
      //InitializeComponent();
   }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetInsertDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_plansale", "service_plansale", _xml_in, 10); // return w/ json

            /*_local_xml = _funcTool.convertJsonToXml(_ret_val);
            dataPlansale = (data_plansale)_funcTool.convertXmlToObject(typeof(data_plansale), _local_xml);
            if (dataPlansale.return_code.ToString() == "0")
            {
                var Email = "mongkonl.d@taokaenoi.co.th"; //dataPlansale.u0_plansale_list[0].email;
                var username = dataPlansale.u0_plansale_list[0].emp_name_th;
                ////var position_name = dataPlansale.u0_plansale_list[0].pos_name_th;
                ////var typeWork = dataPlansale.u0_plansale_list[0].TypeWork;
                ////var ps_startdate = dataPlansale.u0_plansale_list[0].ps_startdate;
                ////var ps_enddate = dataPlansale.u0_plansale_list[0].ps_enddate;
                ////var ps_comment = dataPlansale.u0_plansale_list[0].ps_comment;
                ////var node_description = dataPlansale.u0_plansale_list[0].node_description;

                //_mail_subject = "[HR PlanSale] : " + username;
                //_mail_body = _serviceMail.hr_plansaleCreate(dataPlansale.u0_plansale_list[0]);
                //_serviceMail.SendHtmlFormattedEmailFull(Email, "", Email, _mail_subject, _mail_body);

            }*/
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSelectDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_plansale", "service_plansale", _xml_in, 20); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetUpdateDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_plansale", "service_plansale", _xml_in, 30); // return w/ json

            /*_local_xml = _funcTool.convertJsonToXml(_ret_val);
            dataPlansale = (data_plansale)_funcTool.convertXmlToObject(typeof(data_plansale), _local_xml);
            if (dataPlansale.return_code.ToString() == "0")
            {
                var Email = "mongkonl.d@taokaenoi.co.th"; //dataPlansale.u0_plansale_list[0].email;
                var username = dataPlansale.u0_plansale_list[0].emp_name_th;
                //var position_name = dataPlansale.u0_plansale_list[0].pos_name_th;
                //var typeWork = dataPlansale.u0_plansale_list[0].TypeWork;
                //var ps_startdate = dataPlansale.u0_plansale_list[0].ps_startdate;
                //var ps_enddate = dataPlansale.u0_plansale_list[0].ps_enddate;
                //var ps_comment = dataPlansale.u0_plansale_list[0].ps_comment;
                //var node_description = dataPlansale.u0_plansale_list[0].node_description;

                _mail_subject = "[HR PlanSale] : " + username;
                _mail_body = _serviceMail.hr_plansaleApprove(dataPlansale.u0_plansale_list[0]);
                _serviceMail.SendHtmlFormattedEmailFull(Email, "", Email, _mail_subject, _mail_body);

            }*/
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetUpdateDetailList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_plansale", "service_plansale", _xml_in, 31); // return w/ json

            /*_local_xml = _funcTool.convertJsonToXml(_ret_val);
            dataPlansale = (data_plansale)_funcTool.convertXmlToObject(typeof(data_plansale), _local_xml);
            if (dataPlansale.return_code.ToString() == "0")
            {
                var Email = "mongkonl.d@taokaenoi.co.th"; //dataPlansale.u0_plansale_list[0].email;
                var username = dataPlansale.u0_plansale_list[0].emp_name_th;
                //var position_name = dataPlansale.u0_plansale_list[0].pos_name_th;
                //var typeWork = dataPlansale.u0_plansale_list[0].TypeWork;
                //var ps_startdate = dataPlansale.u0_plansale_list[0].ps_startdate;
                //var ps_enddate = dataPlansale.u0_plansale_list[0].ps_enddate;
                //var ps_comment = dataPlansale.u0_plansale_list[0].ps_comment;
                //var node_description = dataPlansale.u0_plansale_list[0].node_description;

                _mail_subject = "[HR PlanSale] : " + username;
                _mail_body = _serviceMail.hr_plansaleApprove(dataPlansale.u0_plansale_list[0]);
                _serviceMail.SendHtmlFormattedEmailFull(Email, "", Email, _mail_subject, _mail_body);

            }*/
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

}
