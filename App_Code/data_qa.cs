﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_qa
/// </summary>
/// 
[Serializable]
[XmlRoot("data_qa")]
public class data_qa
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_typemat")]
    public string return_typemat { get; set; }

    // master data //
    public qa_m0_place_detail[] qa_m0_place_list { get; set; }
    public qa_m0_setname_detail[] qa_m0_setname_list { get; set; }
    public qa_m0_datesample_detail[] qa_m0_datesample_list { get; set; }
    public qa_m0_material_detail[] qa_m0_material_list { get; set; }
    public qa_m0_test_detail[] qa_m0_test_detail_list { get; set; }
    public qa_r0_set_test_detail[] qa_r0_set_test_detail_list { get; set; }
    // master data //

    // document //  
    public qa_lab_u0_qalab[] qa_lab_u0_qalab_list { get; set; }
    public qa_lab_u1_qalab[] qa_lab_u1_qalab_list { get; set; }
    public qa_lab_u2_qalab[] qa_lab_u2_qalab_list { get; set; }
    public qa_lab_u3_qalab[] qa_lab_u3_qalab_list { get; set; }
    public qa_lab_u4_qalab[] qa_lab_u4_qalab_list { get; set; }
    public qa_lab_u5_qalab[] qa_lab_u5_qalab_list { get; set; }

    [XmlElement("qa_lab_u0doc_qalab_list")]
    public qa_lab_u0doc_qalab[] qa_lab_u0doc_qalab_list { get; set; }

    public qa_lab_vtestdetail[] qa_lab_vtestdetail_list { get; set; }
    public bindqa_m0_test_detail[] bind_qa_m0_test_detail_list { get; set; }
    public qa_m0_history_doc_detail[] qa_m0_history_doc_list { get; set; }
    public qa_m0_history_sample_detail[] qa_m0_history_sample_list { get; set; }
    public bind_qa_node_decision_detail[] bind_qa_node_decision_list { get; set; }
    public bindlab_m0_test_detail[] bindlab_qa_m0_testdetail_list { get; set; }
    public bind_qa_lab_form_detail[] bind_qa_lab_form_list { get; set; }
    public qa_m0_option_detail[] qa_m0_option_list { get; set; }
    public bind_qa_lab_result_detail[] bind_qa_lab_result_list { get; set; }
    public seach_document_detail[] search_document_list { get; set; }
    public seach_document_inlab_detail[] search_document_inlab_list { get; set; }
    // document //

}

public class qa_m0_place_detail
{

    public int place_idx { get; set; }
    public string place_name { get; set; }
    public string place_code { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int place_status { get; set; }
    public int condition { get; set; }

}

public class qa_m0_setname_detail
{
    public int set_idx { get; set; }
    public string set_name { get; set; }
    public string set_detail { get; set; }
    public int setroot_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int set_name_status { get; set; }

}

public class qa_m0_datesample_detail
{
    public int date_sample_idx { get; set; }
    public string date_sample_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int status { get; set; }

}

public class qa_m0_material_detail
{
    public int material_idx { get; set; }
    public string material_code { get; set; }
    public string material_name { get; set; }

}

public class qa_m0_test_detail
{
    public int test_detail_idx { get; set; }
    public string test_detail_name { get; set; }
    public int test_detail_time { get; set; }
    public int test_type_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int test_detail_status { get; set; }
    public int condition { get; set; }
    public int root_detail { get; set; }

}

public class qa_r0_set_test_detail
{
    public int set_test_detail_root_idx { get; set; }
    public string set_test_detail_name { get; set; }
    public int test_detail_time { get; set; }
    public int set_test_detail_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int set_test_detail_status { get; set; }
    public int condition { get; set; }

}

public class qa_lab_u0_qalab
{
    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("admin_emp_idx")]
    public int admin_emp_idx { get; set; }

    [XmlElement("admin_org_idx")]
    public int admin_org_idx { get; set; }

    [XmlElement("admin_rdept_idx")]
    public int admin_rdept_idx { get; set; }

    [XmlElement("admin_rsec_idx")]
    public int admin_rsec_idx { get; set; }

    [XmlElement("simple_form")]
    public int simple_form { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("cemp_org_idx")]
    public int cemp_org_idx { get; set; }

    [XmlElement("cemp_rdept_idx")]
    public int cemp_rdept_idx { get; set; }

    [XmlElement("cemp_rsec_idx")]
    public int cemp_rsec_idx { get; set; }

    [XmlElement("received_date")]
    public string received_date { get; set; }

    [XmlElement("closed_date")]
    public string closed_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("set_test_detail_idx")]
    public int set_test_detail_idx { get; set; }

    [XmlElement("sent_labout")]
    public int sent_labout { get; set; }


}

public class qa_lab_u1_qalab
{
    public int u1_qalab_idx { get; set; }
    public int u0_qalab_idx { get; set; }
    public int u2_qalab_idx { get; set; }
    public string sample_name { get; set; }
    public string sample_code { get; set; }
    public int date_sample1_idx { get; set; }
    public string data_sample1 { get; set; }
    public int date_sample2_idx { get; set; }
    public string data_sample2 { get; set; }
    public string batch { get; set; }
    public string cabinet { get; set; }
    public string shift_time { get; set; }
    public string time { get; set; }
    public string customer_name { get; set; }
    public string other_details { get; set; }
    public string material_code { get; set; }
    public string material_name { get; set; }
    public int certificate { get; set; }
    public int staidx { get; set; }
    public int condition { get; set; }
    public string received_date { get; set; }
    public string received_sample_date { get; set; }
    public string test_date { get; set; }
    public string status_name { get; set; }
    public int decision { get; set; }
    public int cemp_idx { get; set; }
    public int m0_lab_idx { get; set; }
    public string lab_name { get; set; }
    public int Count_All { get; set; }
    public int Count_Success { get; set; }
    public int Count_Place { get; set; }
    public int r1_form_create_idx { get; set; }
    public string form_detail_name { get; set; }
    public string r0_form_create_name { get; set; }
    public string detail_tested { get; set; }
    public string set_test_name { get; set; }  
    public string test_detailidx_insert { get; set; }   
    public int count_test_detail_idx { get; set; }    
    public string materail_type { get; set; }   
    public int org_idx_production { get; set; }    
    public int rdept_idx_production { get; set; }   
    public int rsec_idx_production { get; set; }
    public string org_idx_production_name { get; set; }
    public string rdept_idx_production_name { get; set; }
    public string rsec_idx_production_name { get; set; }
    public int settest_idx { get; set; }  
    public string org_name_th { get; set; }   
    public string dept_name_th { get; set; }   
    public string sec_name_th { get; set; }
    public string EXP_MFD { get; set; }


}

public class qa_lab_u2_qalab
{
    public int u0_qalab_idx { get; set; }
    public int u2_qalab_idx { get; set; }
    public int u1_qalab_idx { get; set; }
    public string update_date { get; set; }
    public string received_sample_date { get; set; }
    public string test_date { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int staidx { get; set; }
    public int form_detail_idx { get; set; }
    public int lab_type_idx { get; set; }
    public int place_idx { get; set; }
    public int test_detail_idx { get; set; }
    public int m0_lab_idx { get; set; }
    public int condition { get; set; }
    public int count_chktest { get; set; }
    public string material_code { get; set; }
    public string comment { get; set; }
    public int decision { get; set; }
    public int admin_emp_idx { get; set; }
    public int admin_org_idx { get; set; }
    public int admin_rdept_idx { get; set; }
    public int admin_rsec_idx { get; set; }
    public int cemp_idx { get; set; }

}

public class qa_lab_u3_qalab
{
    public int u3_qalab_idx { get; set; }
    public int u0_qalab_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int cemp_idx { get; set; }
}

public class qa_lab_u4_qalab
{
    public int u4_qalab_idx { get; set; }
    public int u0_qalab_idx { get; set; }
    public int u1_qalab_idx { get; set; }
    public int u2_qalab_idx { get; set; }
    public string create_date { get; set; }
    public int u0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public string comment { get; set; }
    public int decision_idx { get; set; }
    public int cemp_idx { get; set; }
    public int count_detail_tested { get; set; }
    public int count_sample_tested { get; set; }
    public int form_detail_idx { get; set; }
    public int r1_form_create_status { get; set; }
}

public class qa_lab_u5_qalab
{
    public int u4_qalab_idx { get; set; }
    public int u0_qalab_idx { get; set; }
    public int u1_qalab_idx { get; set; }
    public int u2_qalab_idx { get; set; }
    public string create_date { get; set; }
    public int u0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public string comment { get; set; }
    public int decision_idx { get; set; }
    public int cemp_idx { get; set; }
    public int count_detail_tested { get; set; }
    public int count_sample_tested { get; set; }
    public int form_detail_idx { get; set; }
    public int r1_form_create_status { get; set; }   
    public int r1_form_create_idx { get; set; }
    public string detail_tested { get; set; }

}

[Serializable]
public class qa_lab_u0doc_qalab
{
    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("u0_qalab_idx")]
    public int u0_qalab_idx { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("admin_emp_idx")]
    public int admin_emp_idx { get; set; }

    [XmlElement("admin_org_idx")]
    public int admin_org_idx { get; set; }

    [XmlElement("admin_rdept_idx")]
    public int admin_rdept_idx { get; set; }

    [XmlElement("admin_rsec_idx")]
    public int admin_rsec_idx { get; set; }

    [XmlElement("simple_form")]
    public int simple_form { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("cemp_org_idx")]
    public int cemp_org_idx { get; set; }

    [XmlElement("cemp_rdept_idx")]
    public int cemp_rdept_idx { get; set; }

    [XmlElement("cemp_rsec_idx")]
    public int cemp_rsec_idx { get; set; }

    [XmlElement("received_date")]
    public string received_date { get; set; }

    [XmlElement("closed_date")]
    public string closed_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("cemp_rsec")]
    public string cemp_rsec { get; set; }

    [XmlElement("test_detail_name")]
    public string test_detail_name { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_email_sec")]
    public string emp_email_sec { get; set; }

    [XmlElement("sample_code")]
    public string sample_code { get; set; }

    [XmlElement("lab_out")]
    public string lab_out { get; set; }


}

public class qa_lab_vtestdetail
{
    public int u0_qalab_idx { get; set; }
    public int place_idx { get; set; }
    public string document_code { get; set; }
    public int admin_emp_idx { get; set; }
    public int admin_org_idx { get; set; }
    public int admin_rdept_idx { get; set; }
    public int admin_rsec_idx { get; set; }
    public int simple_form { get; set; }
    public int cemp_idx { get; set; }
    public int cemp_org_idx { get; set; }
    public int cemp_rdept_idx { get; set; }
    public int cemp_rsec_idx { get; set; }
    public string received_date { get; set; }
    public string closed_date { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int staidx { get; set; }
    public int condition { get; set; }
    public string status_name { get; set; }
    public string emp_name_th { get; set; }
    public string org_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_th { get; set; }
    public string pos_name_th { get; set; }
    public int test_detail_idx { get; set; }
    public string test_detail_name { get; set; }
   

}

public class bindqa_m0_test_detail
{
    public int test_detail_idx { get; set; }
    public string test_detail_name { get; set; }
    public int test_detail_time { get; set; }
    public int test_type_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int test_detail_status { get; set; }
    public int condition { get; set; }
    public int root_detail { get; set; }
    public int u0_qalab_idx { get; set; }
    public int u1_qalab_idx { get; set; }
    public int u2_qalab_idx { get; set; }

}

public class qa_m0_history_doc_detail
{
    public int u4_qalab_idx { get; set; }
    public int u0_qalab_idx { get; set; }
    public int u1_qalab_idx { get; set; }
    public string sample_code { get; set; }
    public int u2_qalab_idx { get; set; }
    public string create_date { get; set; }
    public int u0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public string comment { get; set; }
    public int decision_idx { get; set; }
    public int cemp_idx { get; set; }

    public string current_artor { get; set; }
    public string current_decision { get; set; }
    public string emp_name_th { get; set; }

}

public class qa_m0_history_sample_detail
{
    public int u4_qalab_idx { get; set; }
    public int u0_qalab_idx { get; set; }
    public int u1_qalab_idx { get; set; }
    public int u2_qalab_idx { get; set; }
    public string create_date { get; set; }
    public int u0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public string comment { get; set; }
    public int decision_idx { get; set; }
    public int cemp_idx { get; set; }

    public string current_artor { get; set; }
    public string current_decision { get; set; }
    public string emp_name_th { get; set; }

}

public class bind_qa_node_decision_detail
{
    public int u4_qalab_idx { get; set; }
    public int u0_qalab_idx { get; set; }
    public int u1_qalab_idx { get; set; }
    public int u2_qalab_idx { get; set; }
    public string create_date { get; set; }
    public int u0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public string comment { get; set; }
    public int cemp_idx { get; set; }

    public string current_artor { get; set; }
    public string current_decision { get; set; }
    public string emp_name_th { get; set; }
    public int noidx { get; set; }
    public int decision_idx { get; set; }
    public string decision_name { get; set; }

}

public class bindlab_m0_test_detail
{
    public int test_detail_idx { get; set; }
    public string test_detail_name { get; set; }
    public int test_detail_time { get; set; }
    public int test_type_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int test_detail_status { get; set; }
    public int condition { get; set; }
    public int root_detail { get; set; }
    public int u0_qalab_idx { get; set; }
    public int u1_qalab_idx { get; set; }
    public int u2_qalab_idx { get; set; }

}

public class qa_m0_lab
{
    public int m0_lab_idx { get; set; }
    public string lab_name { get; set; }
    public int lab_type_idx { get; set; }
    public string lab_type_name { get; set; }
    public int place_idx { get; set; }
    public string place_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int lab_status { get; set; }
}

public class qa_m1_lab
{
    public int m1_lab_idx { get; set; }
    public int m0_lab_idx { get; set; }
    public string lab_name { get; set; }
    public int test_detail_idx { get; set; }
    public int certificate { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int status { get; set; }
    public string test_detail_name { get; set; }
}

public class bind_qa_lab_form_detail
{
    public int u2_qalab_idx { get; set; }
    public int r1_form_create_idx { get; set; }
    public int r0_form_create_idx { get; set; }
    public string r0_form_create_name { get; set; }
    public int certificate { get; set; }
    public int test_detail_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public string sample_code { get; set; }
    public string test_detail_name { get; set; }
    public string form_detail_name { get; set; }
    public int form_detail_idx { get; set; }
    public int u0_qalab_idx { get; set; }
    public int option_idx { get; set; }
    public int form_detail_root_idx { get; set; }
    public int set_idx { get; set; }
    public int r1_form_detail_root_idx { get; set; }
    public string text_name_setform { get; set; }
    public int u1_qalab_idx { get; set; }
    public int staidx { get; set; }
    public int lab_type_idx { get; set; }
    public int m0_lab_idx { get; set; }
}

public class qa_m0_option_detail
{
    public int option_idx { get; set; }
    public string option_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int option_status { get; set; }
    public int condition { get; set; }

}

public class bind_qa_lab_result_detail
{
    public int u1_qalab_idx { get; set; }
    public int u2_qalab_idx { get; set; }
    public int u5_qalab_idx { get; set; }
    public string sample_code { get; set; }
    public string text_name_setform { get; set; }
    public int staidx { get; set; }
    public string test_detail_name { get; set; }
    public int condition { get; set; }
    public string create_date { get; set; }
    public string detail_tested { get; set; }
    public string result_list { get; set; }
    public int test_detail_idx { get; set; }
    public int m0_lab { get; set; }

}

public class seach_document_detail
{    
    public int u1_qalab_idx { get; set; }   
    public int u0_qalab_idx { get; set; }   
    public int u2_qalab_idx { get; set; }   
    public string document_code { get; set; }    
    public string sample_code { get; set; } 
    public int date_sample1_idx { get; set; }  
    public string data_sample1 { get; set; }    
    public int date_sample2_idx { get; set; }    
    public string data_sample2 { get; set; }    
    public string batch { get; set; }    
    public string cabinet { get; set; }    
    public string shift_time { get; set; }   
    public string time { get; set; }    
    public string customer_name { get; set; }   
    public string other_details { get; set; }  
    public string material_code { get; set; }   
    public string material_name { get; set; }   
    public int certificate { get; set; }  
    public int staidx { get; set; }
    public int condition { get; set; }   
    public int count_testu1 { get; set; }   
    public string received_date { get; set; }   
    public string received_sample_date { get; set; }
    public string test_date { get; set; }   
    public string status_name { get; set; } 
    public int decision { get; set; }   
    public int cemp_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int m0_lab_idx { get; set; }   
    public string lab_name { get; set; }   
    public int Count_All { get; set; }  
    public int Count_Success { get; set; }  
    public int r1_form_create_idx { get; set; }   
    public string form_detail_name { get; set; }    
    public string r0_form_create_name { get; set; }  
    public string detail_tested { get; set; }   
    public string create_date { get; set; }
    public int place_idx { get; set; }

}

public class seach_document_inlab_detail
{
    public int u1_qalab_idx { get; set; }
    public int u0_qalab_idx { get; set; }
    public int u2_qalab_idx { get; set; }
    public string document_code { get; set; }
    public string sample_code { get; set; }
    public int date_sample1_idx { get; set; }
    public string data_sample1 { get; set; }
    public int date_sample2_idx { get; set; }
    public string data_sample2 { get; set; }
    public string batch { get; set; }
    public string cabinet { get; set; }
    public string shift_time { get; set; }
    public string time { get; set; }
    public string customer_name { get; set; }
    public string other_details { get; set; }
    public string material_code { get; set; }
    public string material_name { get; set; }
    public int certificate { get; set; }
    public int staidx { get; set; }
    public int condition { get; set; }
    public int count_testu1 { get; set; }
    public string received_date { get; set; }
    public string received_sample_date { get; set; }
    public string test_date { get; set; }
    public string status_name { get; set; }
    public int decision { get; set; }
    public int cemp_idx { get; set; }
    public int m0_lab_idx { get; set; }
    public string lab_name { get; set; }
    public int Count_All { get; set; }
    public int Count_Success { get; set; }
    public int r1_form_create_idx { get; set; }
    public string form_detail_name { get; set; }
    public string r0_form_create_name { get; set; }
    public string detail_tested { get; set; }
    public string create_date { get; set; }

}
