﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_device")]
public class data_device
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("u0_device_list")]
    public u0_device_detail[] u0_device_list { get; set; }
    public m0_typedevice_detail[] m0_typedevice_list { get; set; }
    public m0_band_detail[] m0_band_list { get; set; }
    public m0_moniter_detail[] m0_moniter_list { get; set; }
    public m0_level_detail[] m0_level_list { get; set; }
    public m0_status_detail[] m0_status_list { get; set; }
    public m0_insurance_detail[] m0_insurance_list { get; set; }
    public m0_hdd_detail[] m0_hdd_list { get; set; }
    public m0_ram_detail[] m0_ram_list { get; set; }
    public m0_vga_detail[] m0_vga_list { get; set; }
    public m0_cpu_detail[] m0_cpu_list { get; set; }
    public m0_printer_detail[] m0_printer_list { get; set; }
}

[Serializable]
public class u0_device_detail
{
    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }
    [XmlElement("u0_code")]
    public string u0_code { get; set; }
    [XmlElement("u0_acc")]
    public string u0_acc { get; set; }
    [XmlElement("u0_po")]
    public string u0_po { get; set; }
    [XmlElement("m0_iridx")]
    public int m0_iridx { get; set; }
    [XmlElement("u0_purchase")]
    public string u0_purchase { get; set; }
    [XmlElement("u0_expDate")]
    public string u0_expDate { get; set; }
    [XmlElement("m0_bidx")]
    public int m0_bidx { get; set; }
    [XmlElement("m0_cpidx")]
    public int m0_cpidx { get; set; }
    [XmlElement("m0_sidx")]
    public int m0_sidx { get; set; }
    [XmlElement("m0_lvidx")]
    public int m0_lvidx { get; set; }
    [XmlElement("u0_serial")]
    public string u0_serial { get; set; }
    [XmlElement("m0_tdidx")]
    public int m0_tdidx { get; set; }
    [XmlElement("u0_empcreate")]
    public int u0_empcreate { get; set; }
    [XmlElement("u0_createdate")]
    public string u0_createdate { get; set; }
    [XmlElement("m0_orgidx")]
    public int m0_orgidx { get; set; }
    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }
    [XmlElement("u0_relation_didx")]
    public int u0_relation_didx { get; set; }

    [XmlElement("u0_dvdidx")]
    public int u0_dvdidx { get; set; }
    [XmlElement("m0_hidx")]
    public int m0_hidx { get; set; }
    [XmlElement("m0_ridx")]
    public int m0_ridx { get; set; }
    [XmlElement("m0_vidx")]
    public int m0_vidx { get; set; }
    [XmlElement("m0_cidx")]
    public int m0_cidx { get; set; }
    [XmlElement("m0_pidx")]
    public int m0_pidx { get; set; }
    [XmlElement("m0_midx")]
    public int m0_midx { get; set; }

    [XmlElement("name_m0_typedevice")]
    public string name_m0_typedevice { get; set; }
    [XmlElement("name_m0_band")]
    public string name_m0_band { get; set; }
    [XmlElement("detail_m0_band")]
    public string detail_m0_band { get; set; }

    [XmlElement("band_ram")]
    public string band_ram { get; set; }
    [XmlElement("type_ram")]
    public string type_ram { get; set; }
    [XmlElement("capacity_ram")]
    public string capacity_ram { get; set; }
    [XmlElement("ddl_band_ram")]
    public string ddl_band_ram { get; set; }

    [XmlElement("band_hdd")]
    public string band_hdd { get; set; }
    [XmlElement("type_hdd")]
    public string type_hdd { get; set; }
    [XmlElement("capacity_hdd")]
    public string capacity_hdd { get; set; }
    [XmlElement("ddl_band_hdd")]
    public string ddl_band_hdd { get; set; }

    [XmlElement("type_vga")]
    public string type_vga { get; set; }
    [XmlElement("generation_vga")]
    public string generation_vga { get; set; }
    [XmlElement("ddl_band_vga")]
    public string ddl_band_vga { get; set; }

    [XmlElement("band_cpu")]
    public string band_cpu { get; set; }
    [XmlElement("generation_cpu")]
    public string generation_cpu { get; set; }
    [XmlElement("ddl_band_cpu")]
    public string ddl_band_cpu { get; set; }

    [XmlElement("band_moniter")]
    public string band_moniter { get; set; }
    [XmlElement("size_moniter")]
    public string size_moniter { get; set; }

    [XmlElement("band_print")]
    public string band_print { get; set; }
    [XmlElement("type_print")]
    public string type_print { get; set; }
    [XmlElement("type_ink")]
    public string type_ink { get; set; }
    [XmlElement("generation_print")]
    public string generation_print { get; set; }

    [XmlElement("m0_Master")]
    public int m0_Master { get; set; }
    [XmlElement("u0_unit")]
    public int u0_unit { get; set; }
    [XmlElement("name_m0_status")]
    public string name_m0_status { get; set; }
    [XmlElement("u0_relation_List")]
    public string u0_relation_List { get; set; }

    [XmlElement("u0_unidx")]
    public int u0_unidx { get; set; }
    [XmlElement("u0_acidx")]
    public int u0_acidx { get; set; }
    [XmlElement("u0_doc_decision")]
    public int u0_doc_decision { get; set; }

    [XmlElement("u1_unidx")]
    public int u1_unidx { get; set; }
    [XmlElement("u1_acidx")]
    public int u1_acidx { get; set; }
    [XmlElement("u1_doc_decision")]
    public int u1_doc_decision { get; set; }
    [XmlElement("doc_status")]
    public string doc_status { get; set; }
    [XmlElement("u0_referent")]
    public string u0_referent { get; set; }
    [XmlElement("name_doc_status")]
    public string name_doc_status { get; set; }

    [XmlElement("u0_didx_reference")]
    public int u0_didx_reference { get; set; }
    [XmlElement("u0_referent_code")]
    public string u0_referent_code { get; set; }
    [XmlElement("u0_status_approve")]
    public int u0_status_approve { get; set; }
    [XmlElement("comment")]
    public string comment { get; set; }
    [XmlElement("u0_didx_reference_transfer")]
    public int u0_didx_reference_transfer { get; set; }
    [XmlElement("u0_slotram")]
    public int u0_slotram { get; set; }
    [XmlElement("Approve_stauts")]
    public int Approve_stauts { get; set; }

    [XmlElement("EmpName")]
    public string EmpName { get; set; }
    [XmlElement("actor_des")]
    public string actor_des { get; set; }
    [XmlElement("node_name")]
    public string node_name { get; set; }
    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("software_idx")]
    public int software_idx { get; set; }
    [XmlElement("software_type")]
    public string software_type { get; set; }
    [XmlElement("name_software")]
    public string name_software { get; set; }
    [XmlElement("u0_swidx")]
    public int u0_swidx { get; set; }
    [XmlElement("type_name")]
    public string type_name { get; set; }
    [XmlElement("u0_software_licen")]
    public string u0_software_licen { get; set; }
    [XmlElement("u0_software_free")]
    public string u0_software_free { get; set; }

    [XmlElement("HDEmpIDX")]
    public int HDEmpIDX { get; set; }
    [XmlElement("R0_depidx")]
    public int R0_depidx { get; set; }
    [XmlElement("R0_secidx")]
    public int R0_secidx { get; set; }
    [XmlElement("u0_node_dec")]
    public int u0_node_dec { get; set; }

    [XmlElement("FullNameTH_HD")]
    public string FullNameTH_HD { get; set; }
    [XmlElement("Email")]
    public string Email { get; set; }
    [XmlElement("Detail_Device")]
    public string Detail_Device { get; set; }

    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }
    [XmlElement("U1_Doc_status")]
    public int U1_Doc_status { get; set; }

    [XmlElement("name_m0_insurance")]
    public string name_m0_insurance { get; set; }
    [XmlElement("name_m0_level")]
    public string name_m0_level { get; set; }
    [XmlElement("u0_code_cutoff")]
    public string u0_code_cutoff { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("u0_code_old")]
    public string u0_code_old { get; set; }

    [XmlElement("HD_R0_depidx")]
    public int HD_R0_depidx { get; set; }
    [XmlElement("HD_R0_secidx")]
    public int HD_R0_secidx { get; set; }
    [XmlElement("HD_Level")]
    public int HD_Level { get; set; }
    [XmlElement("u0_code_main")]
    public string u0_code_main { get; set; }
    [XmlElement("u0_typedeviceetc_idx")]
    public int u0_typedeviceetc_idx { get; set; }

}

public class m0_typedevice_detail
{
    public int m0_tdidx { get; set; }
    public string name_m0_typedevice { get; set; }
    public int status_m0_typedevice { get; set; }
}

public class m0_band_detail
{
    public int m0_bidx { get; set; }
    public string name_m0_band { get; set; }
    public string detail_m0_band { get; set; }
    public int status_m0_band { get; set; }
}

public class m0_moniter_detail
{
    public int m0_midx { get; set; }
    public string band_moniter { get; set; }
    public string size_moniter { get; set; }
    public int status_m0_moniter { get; set; }
    public string name_band_moniter { get; set; }  
}

public class m0_level_detail
{
    public int m0_lvidx { get; set; }
    public string name_m0_level { get; set; }
    public string detail_m0_level { get; set; }
    public int status_m0_level { get; set; }
}

public class m0_status_detail
{
    public int m0_sidx { get; set; }
    public string name_m0_status { get; set; }
    public string detail_m0_status { get; set; }
    public int status_m0_status { get; set; }
}

public class m0_insurance_detail
{
    public int m0_iridx { get; set; }
    public string name_m0_insurance { get; set; }
    public string detail_m0_insurance { get; set; }
    public int status_m0_insurance { get; set; }
}

public class m0_hdd_detail
{
    public int m0_hidx { get; set; }
    public string band_hdd { get; set; }
    public string type_hdd { get; set; }
    public string capacity_hdd { get; set; }
    public int status_m0_hdd { get; set; }
    public string name_band_hdd { get; set; }
}

public class m0_ram_detail
{
    public int m0_ridx { get; set; }
    public string band_ram { get; set; }
    public string type_ram { get; set; }
    public string capacity_ram { get; set; }
    public string ddl_ram { get; set; }
    public int status_m0_ram { get; set; }
}

public class m0_vga_detail
{
    public int m0_vidx { get; set; }
    public string type_vga { get; set; }
    public string generation_vga { get; set; }
    public int status_m0_vga { get; set; }
    public string name_type_vga { get; set; } 
}

public class m0_cpu_detail
{
    public int m0_cidx { get; set; }
    public string band_cpu { get; set; }
    public string generation_cpu { get; set; }
    public string ddl_band_cpu { get; set; }
    public int status_m0_cpu { get; set; }
}

public class m0_printer_detail
{
    public int m0_pidx { get; set; }
    public string band_print { get; set; }
    public string type_print { get; set; }
    public string type_ink { get; set; }
    public string generation_print { get; set; }
    public int status_m0_printer { get; set; }
    public string name_band_print { get; set; }
}