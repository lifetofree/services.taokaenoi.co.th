﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_ecom_promotion")]
public class data_ecom_promotion
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }





    // --- master data ---//

    public ecom_m0_promotion[] ecom_m0_promotion_list { get; set; }

}


public class ecom_m0_promotion
{

    public int pro_idx { get; set; }
    public string PromotionDesc { get; set; }

    public string PromotionID { get; set; }

    public int cemp_idx { get; set; }
 
    public int emp_idx_update { get; set; }

    public int pro_status { get; set; }

    public string create_date { get; set; }
   
    public string update_date { get; set; }

    public string type_promotion { get; set; }
    
    public float amount { get; set; }

    public string sap_and_desc { get; set; }
    

}
