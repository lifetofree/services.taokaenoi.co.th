﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_pms : System.Web.Services.WebService
{

    service_mail _serviceMail = new service_mail();
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_pms _data_pms = new data_pms();

    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = String.Empty;
    string _mail_subject = "";
    string _mail_body = "";
    string webmaseter = "webmaster@taokaenoi.co.th";

    public api_pms()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void PMSInsertMaster_FormResult(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_pms", "service_pms_action", _xml_in, 10); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void PMSInsertSystem_FormResult(string jsonIn)
    {

        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("masConn", "data_pms", "service_pms_action", _xml_in, 11); // return w/ json

            //_local_xml = _funcTool.convertJsonToXml(_ret_val);
            //_dtpmform = (data_tpm_form)_funcTool.convertXmlToObject(typeof(data_tpm_form), _local_xml);

            //if (_dtpmform.ReturnCode.ToString() == "0")
            //{
            //    var unidx = _dtpmform.Boxtpmu0_DocFormDetail[0].unidx;
            //    if (unidx.ToString() == "5")
            //    {
            //        var emp_email = _dtpmform.Boxtpmu0_DocFormDetail[0].emp_email;
            //        var email1 = _dtpmform.Boxtpmu0_DocFormDetail[0].email1;
            //        var email2 = _dtpmform.Boxtpmu0_DocFormDetail[0].email2;
            //        var emp_name_th = _dtpmform.Boxtpmu0_DocFormDetail[0].emp_name_th;
            //        var email_hr = _dtpmform.Boxtpmu0_DocFormDetail[0].email_hr;

            //        emp_email += "," + email1 + "," + email2 + "," + email_hr;

            //        _mail_subject = "[HR - TPM/ระบบประเมินประจำปี] : " + emp_name_th;
            //        _mail_body = _serviceMail.TmpFormResultAlertBody(_dtpmform.Boxtpmu0_DocFormDetail[0]);
            //        _serviceMail.SendHtmlFormattedEmailFull("webmaster@taokaenoi.co.th", "", emp_email, _mail_subject, _mail_body);
            //    }
            //}
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void PMSApproveAllSystem(string jsonIn)
    {

        if (jsonIn != null)
        {
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("masConn", "data_pms", "service_pms_action", _xml_in, 12); // return w/ json

           
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void PMSSelectMaster_FormResult(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_pms", "service_pms_action", _xml_in, 20); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void PMSSelectSystem_FormResult(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_pms", "service_pms_action", _xml_in, 21); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPmsManageData(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_pms", "service_pms_manage_action", _xml_in, 10); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetPmsManageData(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_pms", "service_pms_manage_action", _xml_in, 11); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
}
