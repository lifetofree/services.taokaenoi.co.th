using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Data.SqlClient;

/// <summary>
/// Summary description for api_log
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_visitors : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_visitors _data_visitors = new data_visitors();
    service_mail _servicemail = new service_mail();
    function_db _funcDB = new function_db();

    int _tempInt = 0;
    string _mail_subject = "";
    string _mail_body = "";
    string _link = "http://mas.taokaenoi.co.th/vm-visitors";
    string _url = "";

    public api_visitors()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region master data
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetVisitorType(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 90); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion master data

    #region document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetTransaction(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 10); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTransaction(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 20); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion

    #region send email
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void send_email_visitors(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            data_visitors _data = new data_visitors();
            _data = (data_visitors)_funcTool.convertXmlToObject(typeof(data_visitors), _xml_in);
            if (_data.vm_visitors_sendemail_list != null)
            {
                foreach (var item in _data.vm_visitors_sendemail_list)
                {
                    //if ((item.node_idx == 2) && (item.actor_idx == 3))
                    //{
                    //    int u0idx = item.u0_idx;
                    //    _data = settemplate_hr_daily_toleader(u0idx);
                    //}
                    int u0idx = item.u0_idx;
                    _data = settemplate_visitors(u0idx, item.node_idx, item.actor_idx);

                }
            }
            _ret_val = _funcTool.convertObjectToJson(_data);
            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }

    private data_visitors settemplate_visitors(int id,int node_idx,int actor_idx)
    {
        data_visitors _data = new data_visitors();
        _data.vm_visitors_sendemail_list = new vm_visitors_sendemail_detail[1];
        vm_visitors_sendemail_detail obj_document = new vm_visitors_sendemail_detail();
        obj_document.u0_idx = id;
        obj_document.node_idx = node_idx;
        obj_document.actor_idx = actor_idx;
        _data.vm_visitors_sendemail_list[0] = obj_document;
        _data = callServicePostVisitors(_data);
        if (_data.vm_visitors_sendemail_list != null)
        {
            string emailmove = "";
            string replyempmove = "";// "seniordeveloper@taokaenoi.co.th";
            string title = "";

            if ((node_idx == 2) && (actor_idx == 3))//apporved
            {
                _mail_subject = "[Visitors] ขออนุมัติการเข้าพบ";
                if(_data.vm_visitors_sendemail_list[0].flag_walkin == 1)
                {
                    emailmove = _data.vm_visitors_sendemail_list[0].visit_emp_email;
                }
                //else
                //{
                    var item = _data.vm_visitors_sendemail_list[0];
                    if(item.emp_approve_email1 != "")
                    {
                        string sEmail = item.emp_approve_email1;
                        if (emailmove == "")
                        {
                            emailmove = sEmail;
                        }
                        else
                        {
                            emailmove = emailmove + ","+sEmail;
                        }
                    }
                    if (item.emp_approve_email2 != "")
                    {
                        string sEmail = item.emp_approve_email2;
                        if (emailmove == "")
                        {
                            emailmove = sEmail;
                        }
                        else
                        {
                            emailmove = emailmove + "," + sEmail;
                        }
                    }
                    
                //}
                _mail_body = databody_visitors(_data.vm_visitors_sendemail_list[0], title, _link);
                SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            else if (( (node_idx == 3) || (node_idx == 12) ) && (actor_idx == 4))//zone
            {
                _mail_subject = "[Visitors] ขออนุมัติโซน";
                emailmove = getEmail_employee(id, node_idx, actor_idx,1);
                _mail_body = databody_visitors(_data.vm_visitors_sendemail_list[0], title, _link);
                SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            /*
            else if ((node_idx == 4) && (actor_idx == 5)) //͹��ѵԵ��  vm_visitors_setperms_admin_m0
            {
                _mail_subject = "[Visitors]";
                getEmail_employee(id, node_idx, actor_idx, 2);
                _mail_body = databody_visitors(_data.vm_visitors_sendemail_list[0], title, _link);
                SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

                //���ͼ�����ͧ�����Ҿ�
                _mail_subject = "[Visitors]";
                _link = _data.vm_visitors_sendemail_list[0].visit_emp_email;
                _mail_body = databody_visitors(_data.vm_visitors_sendemail_list[0], title, _link);
                SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }*/
            else if (
                ((node_idx == 5) && (actor_idx == 6)) //operator
                 //||
                 //((node_idx == 7) && (actor_idx == 1))
                )//͹��ѵԵ��  vm_visitors_setperms_admin_m0
            {
                _mail_subject = "[Visitors] ขออนุมัติจากโอเปอเรเตอร์";
                emailmove = getEmail_employee(id, node_idx, actor_idx,2);
                if(_data.vm_visitors_sendemail_list[0].flag_safety == 1) // safety
                {
                    string sEmail = "sarayut_env@yahoo.com";
                    if (emailmove == "")
                    {
                        emailmove = sEmail;
                    }
                    else
                    {
                        emailmove = emailmove + "," + sEmail;
                    }
                }
                _mail_body = databody_visitors(_data.vm_visitors_sendemail_list[0], title, _link);
                SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            else if (
                   ((node_idx == 7) && (actor_idx == 1))
                )
            {
                _mail_subject = "[Visitors] ขออนุมัติจากต้นสังกัด";
                emailmove = _data.vm_visitors_sendemail_list[0].visit_emp_email;
                _mail_body = databody_visitors(_data.vm_visitors_sendemail_list[0], title, _link);
                SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

            }
            else if (
                   ((node_idx == 8) && (actor_idx == 1))
                )
            {
                _mail_subject = "[Visitors] ยืนยันจบงานต้นสังกัด";
                emailmove = _data.vm_visitors_sendemail_list[0].visit_emp_email;
                _mail_body = databody_visitors(_data.vm_visitors_sendemail_list[0], title, _link);
                SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);
            }
            else if(_data.vm_visitors_sendemail_list[0].doc_status == 3) //Rejected
            {
                
                _mail_subject = "[Visitors] ไม่อนุมัติ";
                emailmove = _data.vm_visitors_sendemail_list[0].visit_emp_email;
                _mail_body = databody_visitors(_data.vm_visitors_sendemail_list[0], title, _link);
                SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);
            }
        }
        return _data;

    }

    public string databody_visitors(vm_visitors_sendemail_detail create_list, string title, string link ,int iFlagSafety = 0)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_visitor_create.html")))
        {
            body = reader.ReadToEnd();
        }
        body = body.Replace("{title_name}", title);
        body = body.Replace("{doc_date}", create_list.create_date);
        body = body.Replace("{visit_date}", create_list.visit_date);
        body = body.Replace("{emp_name}", create_list.visit_emp_name_th);
        body = body.Replace("{visitor_type}", create_list.visitor_type_name);
        body = body.Replace("{actor_name}", create_list.zstatus);
        body = body.Replace("{link}", link);

        return body;
    }
    protected data_visitors callServicePostVisitors(data_visitors _dt)
    {
        // convert to json
        _xml_in = _funcTool.convertObjectToXml(_dt);

        _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 21); // return w/ json

        _xml_in = _funcTool.convertJsonToXml(_ret_val);
        // convert json to object
        _dt = (data_visitors)_funcTool.convertXmlToObject(typeof(data_visitors), _xml_in);

        return _dt;
    }
    public void SendHtmlFormattedEmailFull(string recepientEmailList, string recepientCcEmailList, string replyToEmailList, string subject, string body)
    {
        _servicemail.SendHtmlFormattedEmailFull(recepientEmailList, recepientCcEmailList, replyToEmailList, subject, body);
    }

    private string getEmail_employee(int id, int node_idx, int actor_idx,int istatus)
    {
        data_visitors _data = new data_visitors();
        _data.vm_visitors_sendemail_list = new vm_visitors_sendemail_detail[1];
        vm_visitors_sendemail_detail obj_document = new vm_visitors_sendemail_detail();
        obj_document.u0_idx = id;
        obj_document.node_idx = node_idx;
        obj_document.actor_idx = actor_idx;
        _data.vm_visitors_sendemail_list[0] = obj_document;
        string str = "";
        _xml_in = _funcTool.convertObjectToXml(_data);
        if(istatus == 1)
        {
            _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 22);
        }
        else if (istatus == 2)
        {
            _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 23);
        }
        _xml_in = _funcTool.convertJsonToXml(_ret_val);
        _data = (data_visitors)_funcTool.convertXmlToObject(typeof(data_visitors), _xml_in);
        if (_data.vm_visitors_sendemail_list != null)
        {
            int ic = 0;
            foreach (var item in _data.vm_visitors_sendemail_list)
            {

                if (ic == 0)
                {
                    str = item.emp_email;
                }
                else
                {
                    str = str + " , " + item.emp_email;
                }

                ic++;
            }
        }
       // _link = str;
        return str;
    }

    #endregion send email

    #region vehicle special
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetVehicleSpecial(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 81); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetVehicleSpecial(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 80); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion

    #region notification
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetNoti(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 24); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion notification

    #region security
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSecutityProfile(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 70); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion

    #region Security Board
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSecutityBoard(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 25); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion Security Board

    #region reports
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReports(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 26); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReportsPermission(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 28); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion reports

    #region top visit employee
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetVisitEmployeeTop(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_visitors", "service_vm_visitors", _xml_in, 27); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion top visit employee
}