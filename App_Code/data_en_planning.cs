﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_en_planning")]
public class data_en_planning
{
    [XmlElement("return_code")]
    public string return_code { get; set; }

    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("BoxEN_TypeItemList")]
    public TypeItem_Detail[] BoxEN_TypeItemList { get; set; }

    [XmlElement("BoxEN_StandardList")]
    public Standard_Detail[] BoxEN_StandardList { get; set; }

    [XmlElement("BoxEN_ContentList")]
    public Content_Detail[] BoxEN_ContentList { get; set; }

    [XmlElement("BoxEN_MethodList")]
    public Method_Detail[] BoxEN_MethodList { get; set; }


    [XmlElement("BoxEN_ToolingList")]
    public Tooling_Detail[] BoxEN_ToolingList { get; set; }

    [XmlElement("BoxEN_TimeList")]
    public Timing_Detail[] BoxEN_TimeList { get; set; }

    [XmlElement("BoxEN_TypeCodeList")]
    public TypeCode_Detail[] BoxEN_TypeCodeList { get; set; }

    [XmlElement("BoxEN_TypeMachineList")]
    public TypeMachine_Detail[] BoxEN_TypeMachineList { get; set; }

    [XmlElement("BoxEN_GroupCodeList")]
    public GroupCode_Detail[] BoxEN_GroupCodeList { get; set; }

    [XmlElement("BoxEN_m1formList")]
    public m1form_Detail[] BoxEN_m1formList { get; set; }

    [XmlElement("BoxEN_m2formList")]
    public m2form_Detail[] BoxEN_m2formList { get; set; }

    [XmlElement("BoxEN_m3formList")]
    public m3form_Detail[] BoxEN_m3formList { get; set; }

    [XmlElement("BoxEN_u0formList")]
    public u0form_Detail[] BoxEN_u0formList { get; set; }

    [XmlElement("BoxEN_u0docform")]
    public u0docform_Detail[] BoxEN_u0docform { get; set; }

    [XmlElement("BoxEN_u2docform")]
    public u2docform_Detail[] BoxEN_u2docform { get; set; }

    [XmlElement("en_report_action")]
    public en_report[] en_report_action { get; set; }

    [XmlElement("en_lookup_action")]
    public en_lookup[] en_lookup_action { get; set; }
}

#region Master Data M0
[Serializable]
public class TypeItem_Detail
{
    public int m0tyidx { get; set; }
    public string type_name_th { get; set; }
    public string type_name_en { get; set; }
    public int type_status { get; set; }
    public int CEmpIDX { get; set; }
    public string TypeStatusDetail { get; set; }
    public string NameEN { get; set; }
    public int TmcIDX { get; set; }
    public int GCIDX { get; set; }
    public int TCIDX { get; set; }
    public string NameTypecode { get; set; }
    public string NameGroupcode { get; set; }


}

[Serializable]
public class Standard_Detail
{
    public int m0stidx { get; set; }
    public int m0tyidx { get; set; }
    public string standard_name_th { get; set; }
    public string standard_name_en { get; set; }
    public int standard_status { get; set; }
    public int CEmpIDX { get; set; }
    public int UEmpIDX { get; set; }
    public string StandardStatusDetail { get; set; }
    public string type_name_th { get; set; }
    public int TmcIDX { get; set; }
    public string NameEN { get; set; }
    public int GCIDX { get; set; }
    public int TCIDX { get; set; }
    public string NameTypecode { get; set; }
    public string NameGroupcode { get; set; }
    public string contentth { get; set; }
    public int m0coidx { get; set; }

}

[Serializable]
public class Content_Detail
{
    public int m0coidx { get; set; }
    public string contentth { get; set; }
    public string contenten { get; set; }
    public int content_status { get; set; }
    public int CEmpIDX { get; set; }
    public int UEmpIDX { get; set; }
    public string ContentStatusDetail { get; set; }
    public int m0tyidx { get; set; }
    public string type_name_th { get; set; }
    public string NameEN { get; set; }
    public int TmcIDX { get; set; }
    public int GCIDX { get; set; }
    public int TCIDX { get; set; }
    public string NameTypecode { get; set; }
    public string NameGroupcode { get; set; }

}

[Serializable]
public class Method_Detail
{
    public int m0meidx { get; set; }
    public string method_name_th { get; set; }
    public string method_name_en { get; set; }
    public int method_status { get; set; }
    public int CEmpIDX { get; set; }
    public int UEmpIDX { get; set; }
    public string MethodStatusDetail { get; set; }
    public int m0stidx { get; set; }
    public int m0tyidx { get; set; }
    public int m0coidx { get; set; }
    public string standard_name_th { get; set; }
    public string type_name_th { get; set; }
    public int TmcIDX { get; set; }
    public string NameEN { get; set; }
    public int GCIDX { get; set; }
    public int TCIDX { get; set; }
    public string NameTypecode { get; set; }
    public string NameGroupcode { get; set; }
    public string contentth { get; set; }

}

[Serializable]
public class Tooling_Detail
{
    public int m0toidx { get; set; }
    public string tooling_name_th { get; set; }
    public string tooling_name_en { get; set; }
    public int tooling_status { get; set; }
    public int CEmpIDX { get; set; }
    public int UEmpIDX { get; set; }
    public string ToolingStatusDetail { get; set; }
    public int mat_number { get; set; }
    public int TmcIDX { get; set; }
    public string NameEN { get; set; }
    public int GCIDX { get; set; }
    public int TCIDX { get; set; }
    public string NameTypecode { get; set; }
    public string NameGroupcode { get; set; }
    public int choose_type { get; set; }


}

[Serializable]
public class Timing_Detail
{
    public int m0tidx { get; set; }
    public string time_desc { get; set; }
    public string time_short { get; set; }
    public int qty { get; set; }
    public string unit { get; set; }
    public int count_doing { get; set; }
    public string unit_doing { get; set; }
    public int count_director { get; set; }
    public string unit_director { get; set; }
    public int time_status { get; set; }
    public int CEmpIDX { get; set; }
    public string TimingStatusDetail { get; set; }
    public int count_coo { get; set; }
    public string unit_coo { get; set; }

    public string alertdoing { get; set; }
    public string alertdirector { get; set; }
    public string alertcoo { get; set; }

}

[Serializable]
public class TypeCode_Detail
{
    public int TCIDX { get; set; }
    public string NameTypecode { get; set; }
    public int TmcIDX { get; set; }
    public string TypeCode { get; set; }

}

[Serializable]
public class GroupCode_Detail
{
    public int GCIDX { get; set; }
    public int TCIDX { get; set; }
    public string NameTypecode { get; set; }
    public int GroupCode { get; set; }

}

[Serializable]
public class TypeMachine_Detail
{
    public int TypeIDX { get; set; }
    public string TypeName { get; set; }
}

#endregion

#region Master Data Form Result

[Serializable]
public class m1form_Detail
{
    public int m1idx { get; set; }
    public int TCIDX { get; set; }
    public int m1status { get; set; }
    public int CEmpIDX { get; set; }
    public int GCIDX { get; set; }
    public int LocIDX { get; set; }

}

[Serializable]
public class m2form_Detail
{
    public int m2idx { get; set; }
    public int m1idx { get; set; }
    public int m2status { get; set; }
    public int CEmpIDX { get; set; }
    public int m0tyidx { get; set; }
    public int m0coidx { get; set; }
    public int m0stidx { get; set; }
    public int m0meidx { get; set; }
    public string time { get; set; }
    public int TCIDX2 { get; set; }
    public int GCIDX2 { get; set; }
    public string m0toidx_comma { get; set; }


}

[Serializable]
public class m3form_Detail
{
    public int m3idx { get; set; }
    public int m2idx { get; set; }
    public int m3status { get; set; }
    public int m0toidx { get; set; }
    public int CEmpIDX { get; set; }
    public string m0toidx_comma { get; set; }
    public int TCIDX3 { get; set; }
    public int GCIDX3 { get; set; }
    public int m0toidx3 { get; set; }
}

[Serializable]
public class u0form_Detail
{
    public int m1idx { get; set; }
    public int m2idx { get; set; }
    public int m3idx { get; set; }
    public int TCIDX { get; set; }
    public int CEmpIDX { get; set; }
    public int GCIDX { get; set; }
    public string NameGroupcode { get; set; }
    public string NameTypecode { get; set; }
    public string NameEN { get; set; }
    public string datecerate { get; set; }
    public string type_name_th { get; set; }
    public string contentth { get; set; }
    public string method_name_th { get; set; }
    public string standard_name_th { get; set; }
    public string tooling_name_th { get; set; }
    public int m0coidx { get; set; }
    public int m0tyidx { get; set; }
    public int m0stidx { get; set; }
    public int m0meidx { get; set; }
    public string time { get; set; }
    public int m0toidx { get; set; }
    public int TmcIDX { get; set; }
    public string m0toidx_comma { get; set; }
    public int LocIDX { get; set; }
    public string LocName { get; set; }
    public string status_name { get; set; }
    public string result_comment { get; set; }

}
#endregion


#region PMMS System
[Serializable]
public class u0docform_Detail
{
    public int TmcIDX { get; set; }
    public int TCIDX { get; set; }
    public int GCIDX { get; set; }
    public int condition { get; set; }
    public int m0tidx { get; set; }
    public string customplan { get; set; }
    public int LocIDX { get; set; }
    public int m1idx { get; set; }
    public string MCCode { get; set; }
    public int countplan { get; set; }
    public string dateplanstart { get; set; }
    public int BuildingIDX { get; set; }
    public int RoomIDX { get; set; }
    public int CEmpIDX { get; set; }
    public int RdeptIDX { get; set; }
    public int RSecIDX { get; set; }
    public int MCIDX { get; set; }
    public string LocName { get; set; }
    public string MCCode_comma { get; set; }
    public string code { get; set; }
    public int u1idx_planning { get; set; }
    public string datestart { get; set; }
    public int RPIDX { get; set; }
    public string status_name { get; set; }
    public int stidx { get; set; }
    public string TypeNameTH { get; set; }
    public string MCNameTH { get; set; }
    public string BuildingName { get; set; }
    public string Roomname { get; set; }
    public string time_desc { get; set; }
    public string DateManage { get; set; }
    public int m2idx { get; set; }
    public int u0idx { get; set; }
    public string NameGroupCode { get; set; }
    public string NameTypecode { get; set; }
    public string yearplan { get; set; }
    public int Year { get; set; }
    public string RECode { get; set; }
    public string dateplanend { get; set; }
    public string CreateDate { get; set; }
    public string FullNameTH { get; set; }
    public string DeptNameTH { get; set; }
    public string SecNameTH { get; set; }
    public string PosNameTH { get; set; }
    public string MobileNo { get; set; }
    public string Email { get; set; }
    public string EmpCode { get; set; }
    public string OrgNameTH { get; set; }
    public int nodeidx { get; set; }
    public int actoridx { get; set; }
    public int STAppIDX { get; set; }
    public string MCIDX_Comma { get; set; }


}

[Serializable]
public class u2docform_Detail
{
    public int m2idx { get; set; }
    public int reult_value { get; set; }
    public string result_comment { get; set; }

}

// start teppanop
#region EN Report 
[Serializable]
public class en_report
{
    public string operation_status_id { get; set; }
    public int idx { get; set; }
    public int idx1 { get; set; }

    public int zId { get; set; }
    public string zName { get; set; }
    public int zId_G { get; set; }
    public string zName_G { get; set; }

    public int zmonth { get; set; }
    public int zyear { get; set; }

    public string fil_Search { get; set; }
    public string filter_keyword { get; set; }

    public int mcidx { get; set; }
    public string MCCode { get; set; }
    public string MCNameTH { get; set; }
    public string zMCCode { get; set; }

    public int LocIDX { get; set; }
    public int TmcIDX { get; set; }
    public int TCIDX { get; set; }
    public int BuildingIDX { get; set; }
    public int MCIDX { get; set; }
    public int zday { get; set; }
    public int zweek { get; set; }
    public int countplan { get; set; }
    public int zCount { get; set; }

}
[Serializable]
public class en_lookup
{
    public string operation_status_id { get; set; }
    public int idx { get; set; }
    public int idx1 { get; set; }

    public int zId { get; set; }
    public string zName { get; set; }
    public int zId_G { get; set; }
    public string zName_G { get; set; }

    public int zmonth { get; set; }
    public int zyear { get; set; }

    public string fil_Search { get; set; }
    public string filter_keyword { get; set; }

    public int LocIDX { get; set; }
    public int TmcIDX { get; set; }
    public int TCIDX { get; set; }
    public int BuildingIDX { get; set; }

}

#endregion
// end teppanop
#endregion

