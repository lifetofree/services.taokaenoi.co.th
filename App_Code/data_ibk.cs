﻿using System;
using System.Xml.Serialization;

public class data_ibk
{
   public int return_code { get; set; }
   public string return_msg { get; set; }
   public topic_score[] ibk_m0_topic_score_action { get; set; }
   public room_score[] ibk_u0_room_score_action { get; set; }
}

#region topic_score
[Serializable]
public class topic_score
{
   public int u0_room_score_idx { get; set; }
   public int u0_room_idx_ref { get; set; }
   public int m0_topic_score_idx_ref { get; set; }
   public int room_score_value { get; set; }
   public int room_score_amount { get; set; }
   public string room_score_created_at { get; set; }
   public int room_score_created_by { get; set; }
}
#endregion topic_score

#region room_score
[Serializable]
public class room_score
{
   public int u0_room_score_idx { get; set; }
   public int u0_room_idx_ref { get; set; }
   public int m0_topic_score_idx_ref { get; set; }
   public int room_score_value { get; set; }
   public int room_score_amount { get; set; }
   public string room_score_created_at { get; set; }
   public int room_score_created_by { get; set; }

   public int rating_score { get; set; }
   public int number_score { get; set; }
}
#endregion room_score
