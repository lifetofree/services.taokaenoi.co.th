﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_hr_dashboard")]
public class data_hr_dashboard
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }
    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }
    [XmlElement("BoxReport_Dashboard")]
    public Report_DashBoardDetail[] BoxReport_Dashboard { get; set; }
}

[Serializable]
public class Report_DashBoardDetail
{
    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("orgidx")]
    public int orgidx { get; set; }

    [XmlElement("date_now")]
    public string date_now { get; set; }

    [XmlElement("countTIDX")]
    public int countTIDX { get; set; }

    [XmlElement("pos_name")]
    public string pos_name { get; set; }

    [XmlElement("JobLevel")]
    public string JobLevel { get; set; }

    [XmlElement("count_jobgrade_idx")]
    public int count_jobgrade_idx { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }

    [XmlElement("count_plant_idx")]
    public int count_plant_idx { get; set; }

    [XmlElement("male")]
    public string male { get; set; }

    [XmlElement("female")]
    public string female { get; set; }

    [XmlElement("plant_idx")]
    public int plant_idx { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("DC_YEAR")]
    public string DC_YEAR { get; set; }

    [XmlElement("DC_Month")]
    public string DC_Month { get; set; }

    [XmlElement("emp_type_idx")]
    public int emp_type_idx { get; set; }

    [XmlElement("Plant_name")]
    public string Plant_name { get; set; }

}