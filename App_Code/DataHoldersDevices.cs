﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("DataHoldersDevices")]
public class DataHoldersDevices
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }

    [XmlElement("ReturnName")]
    public string ReturnName { get; set; }

    [XmlElement("ReturnDept")]
    public string ReturnDept { get; set; }

    [XmlElement("Return_Node")]
    public string Return_Node { get; set; }

    [XmlElement("Return_Actor")]
    public string Return_Actor { get; set; }

    [XmlElement("Return_Status")]
    public string Return_Status { get; set; }


    [XmlElement("ReturnDept_Accept")]
    public string ReturnDept_Accept { get; set; }

    [XmlElement("ReturnDate")]
    public string ReturnDate { get; set; }

    [XmlElement("ReturnDeviceCode")]
    public string ReturnDeviceCode { get; set; }

    [XmlElement("ReturnAsset")]
    public string ReturnAsset { get; set; }

    [XmlElement("ReturnType")]
    public string ReturnType { get; set; }

    [XmlElement("ReturnName_Own")]
    public string ReturnName_Own { get; set; }

    [XmlElement("ReturnName_Accept")]
    public string ReturnName_Accept { get; set; }

    [XmlElement("ReturnName_Before")]
    public string ReturnName_Before { get; set; }

    [XmlElement("ReturnIDX")]
    public string ReturnIDX { get; set; }

    [XmlElement("ReturnCount")]
    public string ReturnCount { get; set; }

    [XmlElement("return_code")]
    public string return_code { get; set; }

    [XmlElement("ReturnEmpNode")]
    public string ReturnEmpNode { get; set; }

    [XmlElement("ReturnEmail")]
    public string ReturnEmail { get; set; }

    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }

    [XmlElement("CountRows")]
    public string CountRows { get; set; }

    public HoldersDevices[] BoxHoldersDevices { get; set; }
    public AddHoldersDevices[] BoxAddHoldersDevices { get; set; }

}

public class HoldersDevices
{
    public int RDeptIDX { get; set; }
    public int CEmpIDX { get; set; }
    public int JobGradeIDX { get; set; }
    public int staidx { get; set; }
    public int NRDeptIDX { get; set; }
    public int OrgIDX { get; set; }
    public int DeviceIDX { get; set; }
    public int RSecIDX { get; set; }
    public int EqtIDX { get; set; }
    public int EmpIDX { get; set; }
    public int HolderIDX { get; set; }
    public int HDEmpIDX { get; set; }

    public int NOrgIDX { get; set; }
    public int NRSecIDX { get; set; }
    public int NHDEmpIDX { get; set; }
    public int acidx { get; set; }
    public int unidx { get; set; }
    public int noidx { get; set; }
    public int status { get; set; }
    public int status_approve { get; set; }
    public int m0_tdidx { get; set; }
   
    public string detail { get; set; }
    public string Search { get; set; }
    public string DeviceCode { get; set; }
    public string POCode { get; set; }
    public string AccessCode { get; set; }
    public string InsuranceName { get; set; }
    public string SerialNumber { get; set; }
    public string StatusDoc { get; set; }
    public string InsuranceExp { get; set; }
    public string PurchaseDate { get; set; }
    public string Organization { get; set; }
    public string Dept { get; set; }
    public string HDEmpFullName { get; set; }
    public string EqtName { get; set; }
    public string PosNameTH { get; set; }
    public string SecNameTH { get; set; }
    public string HolderStatus_ { get; set; }

    public string HolderStatus { get; set; }
    public string Remark { get; set; }
    public string EmpName { get; set; }
    public string createdate { get; set; }
    public string actor_des { get; set; }
    public string node_name { get; set; }
    public string status_name { get; set; }
    public string comment { get; set; }
    public string FullNameTH { get; set; }
    public string approver1 { get; set; }
    public string approver2 { get; set; }

}


public class AddHoldersDevices
{
    public int NOrgIDX_add { get; set; }
    public int RDeptIDX_add { get; set; }
    public int NRDeptIDX_add { get; set; }
    public int RSecIDX_add { get; set; }
    public int NRSecIDX_add { get; set; }
    public int PosIDX_add { get; set; }
    public int NPosIDX_add { get; set; }
    public int EqtIDX_add { get; set; }
    public int EmpIDX_add { get; set; }
    public int DeviceIDX_add { get; set; }
    public int HDEmpIDX_add { get; set; }
    public int NHDEmpIDX_add { get; set; }
    public int HolderIDX { get; set; }
    public int acidx { get; set; }
    public int unidx { get; set; }
    public int noidx { get; set; }
    public int status { get; set; }
    public int NEmpIDX1 { get; set; }
    public int NEmpIDX2 { get; set; }
    public string Remark { get; set; }

}