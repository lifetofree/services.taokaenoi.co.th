using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Text;



/// <summary>
/// Summary description for api_job_order
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_job_order : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";

    //sent e - mail
    string _local_xml = "";
    string _mail_subject = "";
    string _mail_body = "";

    string no_invoice_mail = "";
    string test_details_mail = "";
    string details_mail = "";
    string create_date_mail = "";
    string link_system_joborder = "http://demo.taokaenoi.co.th/job-order";


    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_jo _data_jo = new data_jo();
    service_mail _serviceMail = new service_mail();


    public api_job_order()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectDatajoborder_mishen(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 28); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void InsertDatajoborder(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 10); // return w/ json

            data_jo _data_jo_sendmail = new data_jo();
            _data_jo_sendmail = (data_jo)_funcTool.convertXmlToObject(typeof(data_jo), _xml_in);

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_jo_sendmail = (data_jo)_funcTool.convertXmlToObject(typeof(data_jo), _local_xml);

            var email_mis = _data_jo_sendmail.return_mailmis;

            if (_data_jo_sendmail.return_code == 0)
            {
                string email_sendTo = "";

                if (_data_jo_sendmail.job_order_overview_action[0].emp_email != null)
                {
                    email_sendTo = _data_jo_sendmail.job_order_overview_action[0].emp_email.ToString();
                    
                }
                 string replyempcreate = "mancheep@gmail.com";
                 string email =  "mancheep@gmail.com"; //เทส
                //string email = email_sendTo + ","+ email_mis; // เอาจริง

                job_order_overview _temp_u0 = _data_jo_sendmail.job_order_overview_action[0];
                _mail_subject = "[MIS : Job Order] - มีรายการร้องขอความต้องการเพื่อให้หัวหน้าผู้ใช้งานดำเนินการ";

                _mail_body = _serviceMail.SentMailJob_orderCreate(_data_jo_sendmail.job_order_overview_action[0], link_system_joborder);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);

            }
            else
            {

            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void UpdateDatajoborder(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 11); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void UpdateDatajobordersta(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 12); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectDatajoborder_headlook(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 21); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insertu1(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 50); // return w/ json


            //data_jo _data_jo_sendmail = new data_jo();
            //_data_jo_sendmail = (data_jo)_funcTool.convertXmlToObject(typeof(data_jo), _xml_in);

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_jo = (data_jo)_funcTool.convertXmlToObject(typeof(data_jo), _local_xml);

            var id_satus = _data_jo.jo_add_datalist_action[0].id_statework;
            var end_send = _data_jo.jo_add_datalist_action[0].end_send;

            var email_mis = _data_jo.return_mailmis;

            if (_data_jo.return_code == 0)
            {
                string email_sendTo = "";

                if (_data_jo.jo_add_datalist_action[0].emp_email != null)
                {
                  email_sendTo = _data_jo.jo_add_datalist_action[0].emp_email.ToString();
                }

                    string replyempcreate = "mancheep@gmail.com";
                    string email = "mancheep@gmail.com"; //เทส

                    // string email = email_sendTo +","+ email_mis; // จริง

                    jo_add_datalist _temp_u0 = _data_jo.jo_add_datalist_action[0];
                    _mail_subject = "[MIS : Job Order] - มีการส่งมอบงานที่ร้องขอความต้องการจากฝ่าย MIS";

                    _mail_body = _serviceMail.SentMailJob_orderCreateU1_(_data_jo.jo_add_datalist_action[0], link_system_joborder);

                    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);   
            }
            else
            {

            }
        }
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectU1ov(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 24); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Selectheadseehome(string jsonIn)       // ��͹ ��� mis �����ҧ��ǹ
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 23); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Updateu1end(string jsonIn)       // �Ѿ�൵��
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 51); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Updateu1naiend(string jsonIn)       // �Դ ��� ����Ǣ���ش���� �ѧ�� ��͡�������
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 52); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Updatestau0(string jsonIn)       // �Ѿ�൵�� �ͧ u0 head ͹��ѵ�
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 13); // return w/ json

          //  data_jo _data_jo_sendmail = new data_jo();
           // _data_jo_sendmail = (data_jo)_funcTool.convertXmlToObject(typeof(data_jo), _xml_in);

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_jo = (data_jo)_funcTool.convertXmlToObject(typeof(data_jo), _local_xml);

            /// job_order_overview _temp_u0_1 = _data_jo_sendmail.job_order_overview_action[0];

            var node_idx = _data_jo.job_order_overview_action[0].m0_node_idx;
            var actor = _data_jo.job_order_overview_action[0].m0_actor_idx;

            var status = _data_jo.job_order_overview_action[0].id_statework;

            var to_node = _data_jo.job_order_overview_action[0].to_m0_node;
            var to_actor = _data_jo.job_order_overview_action[0].to_m0_actor;

            var email_mis = _data_jo.return_mailmis;

            if (_data_jo.return_code == 0)
           {
                string email_sendTo = "";
               
                if (_data_jo.job_order_overview_action[0].emp_email != null)
                {
                    email_sendTo = _data_jo.job_order_overview_action[0].emp_email.ToString();
                }

                string replyempcreate = "mancheep@gmail.com";
                string email = "mancheep@gmail.com"; //เทส
                //  string email = email_sendTo + "," + email_mis; // จริง

                if (to_node.ToString() == "1" && to_actor.ToString() == "1" && status.ToString() == "3")
            {  
              //  job_order_overview _temp_u0 = _data_jo.job_order_overview_action[0];
                _mail_subject = "[MIS : Job Order] - มีการเเก้ไขรายการร้องขอความต้องการ";

                    _mail_body = _serviceMail.SentMailJob_orderCreate(_data_jo.job_order_overview_action[0], link_system_joborder);

                    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            }
            else if (to_node.ToString() == "3" && to_actor.ToString() == "3" && status.ToString() == "2")
            {   
              //  job_order_overview _temp_u0 = _data_jo.job_order_overview_action[0];
                _mail_subject = "[MIS : Job Order] - มีรายการร้องขอความต้องการเพื่อให้หัวหน้าฝ่าย MIS ดำเนินงาน";

                _mail_body = _serviceMail.SentMailJob_orderCreate(_data_jo.job_order_overview_action[0], link_system_joborder);
                    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            }
            else if (to_node.ToString() == "2" && to_actor.ToString() == "2" && status.ToString() == "1")
            { 
              //  job_order_overview _temp_u0 = _data_jo.job_order_overview_action[0];
                _mail_subject = "[MIS : Job Order] - มีรายการร้องขอความต้องการเพื่อให้หัวหน้าผู้ใช้งานดำเนินการ";

                _mail_body = _serviceMail.SentMailJob_orderCreate(_data_jo.job_order_overview_action[0], link_system_joborder);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            }
            else if (to_node.ToString() == "4" && to_actor.ToString() == "1" && status.ToString() == "2")
            {
               // job_order_overview _temp_u0 = _data_jo.job_order_overview_action[0];
                _mail_subject = "[MIS : Job Order] - เเจ้งระยะเวลาที่ใช้ในการดำเนินงานของรายการที่ร้องขอ";

                _mail_body = _serviceMail.SentMailJob_orderCreate(_data_jo.job_order_overview_action[0], link_system_joborder);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            }
            else if (to_node.ToString() == "3" && to_actor.ToString() == "3" && status.ToString() == "3")
            {
                //job_order_overview _temp_u0 = _data_jo.job_order_overview_action[0];
                _mail_subject = "[MIS : Job Order] -  มีการร้องขอให้กำหนดระยะเวลาในการดำเนินงานอีกครั้ง";

                _mail_body = _serviceMail.SentMailJob_orderCreate(_data_jo.job_order_overview_action[0], link_system_joborder);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            }
            else if (to_node.ToString() == "5" && to_actor.ToString() == "4" && status.ToString() == "5")
            {
                //job_order_overview _temp_u0 = _data_jo.job_order_overview_action[0];
                _mail_subject = "[MIS : Job Order] - รอการดำเนินงานจากพนักงานภายในฝ่าย MIS";

                _mail_body = _serviceMail.SentMailJob_orderCreate(_data_jo.job_order_overview_action[0], link_system_joborder);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            }
          
            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Updatestau1_ov(string jsonIn)       // �Ѿ�൵�� �ͧ u1
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 14); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectDatajoborder_listheadsee(string jsonIn)       // �Դ ��� ����Ǣ���ش���� �ѧ�� ��͡�������
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 25); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectMIS_share(string jsonIn)       // �Դ ��� ����Ǣ���ش���� �ѧ�� ��͡�������
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 26); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Updat_statusdeleate(string jsonIn)       // �Դ ��� ����Ǣ���ش���� �ѧ�� ��͡�������
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 16); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_jo = (data_jo)_funcTool.convertXmlToObject(typeof(data_jo), _local_xml);

            var email_mis = _data_jo.return_mailmis;

            if (_data_jo.return_code == 0)
            {
                string email_sendTo = "";

                if (_data_jo.jo_add_datalist_action[0].emp_email != null)
                {
                    email_sendTo = _data_jo.jo_add_datalist_action[0].emp_email.ToString();
                }

                string replyempcreate = "mancheep@gmail.com";
                string email = "mancheep@gmail.com"; //เทส
                //string email = email_sendTo + "," + email_mis; จริง
                jo_add_datalist _temp_u0 = _data_jo.jo_add_datalist_action[0];
                _mail_subject = "[MIS : Job Order] - งานที่ส่งมอบถูกลบโดยพนักงานภายในฝ่าย MIS";

                _mail_body = _serviceMail.SentMailJob_orderCreatesubmit(_data_jo.jo_add_datalist_action[0], link_system_joborder);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);

            }
            else
            {

            }


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

   
//    /////////////////////////////peach/////////////////////////////////
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void SelectDatajoborder(string jsonIn)
        {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in,20); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
        }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteDatajoborder(string jsonIn)
    {

       if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 12); // return w/ json
        }

       Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectDatajoborder_log(string jsonIn)
    {

       if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 22); // return w/ json
        }

       Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Update_daystartfinal(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 15); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_daystartfinal(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 27); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_mishead(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 29); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
// ////////////////////////////////////////////////////////////////
   
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insertloggaritum(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 17); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    } 

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectU1ov_v2(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 30); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Updateu1_userok(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 18); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_jo = (data_jo)_funcTool.convertXmlToObject(typeof(data_jo), _local_xml);

             var email_mis = _data_jo.return_mailmis;

            if (_data_jo.return_code == 0)
            {
                string email_sendTo = "";

                if (_data_jo.jo_add_datalist_action[0].emp_email != null)
                {
                    email_sendTo = _data_jo.jo_add_datalist_action[0].emp_email.ToString();
                }

                string replyempcreate = "mancheep@gmail.com";
                string email = "mancheep@gmail.com"; //เทส
                //string email = email_sendTo + ","+ email_mis; // จริง

                jo_add_datalist _temp_u0 = _data_jo.jo_add_datalist_action[0];
                _mail_subject = "[MIS : Job Order] - ยอมรับงานที่ดำเนินการส่งมอบโดยฝ่าย MIS";

                _mail_body = _serviceMail.SentMailJob_orderCreatesubmit(_data_jo.jo_add_datalist_action[0], link_system_joborder);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);

            }
            else
            {

            }


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Updateu1_userno(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 19); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_jo = (data_jo)_funcTool.convertXmlToObject(typeof(data_jo), _local_xml);

            var email_mis = _data_jo.return_mailmis;

            if (_data_jo.return_code == 0)
            {
                string email_sendTo = "";

                if (_data_jo.jo_add_datalist_action[0].emp_email != null)
                {
                    email_sendTo = _data_jo.jo_add_datalist_action[0].emp_email.ToString();
                }

                string replyempcreate = "mancheep@gmail.com";
                string email = "mancheep@gmail.com"; //เทส
                //string email = email_sendTo+ ","+email_mis;// จริง

                jo_add_datalist _temp_u0 = _data_jo.jo_add_datalist_action[0];
                _mail_subject = "[MIS : Job Order] - ปฏิเสธงานที่ดำเนินการส่งมอบโดยฝ่าย MIS";

                _mail_body = _serviceMail.SentMailJob_orderCreatecancel(_data_jo.jo_add_datalist_action[0], link_system_joborder);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);

            }
            else
            {

            }


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Updateu1misreturn_editsucc(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 54); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_jo = (data_jo)_funcTool.convertXmlToObject(typeof(data_jo), _local_xml);

            var email_mis = _data_jo.return_mailmis;

            if (_data_jo.return_code == 0)
            {
                string email_sendTo = "";

                if (_data_jo.jo_add_datalist_action[0].emp_email != null)
                {
                    email_sendTo = _data_jo.jo_add_datalist_action[0].emp_email.ToString();
                }

                string replyempcreate = "mancheep@gmail.com";
                string email = "mancheep@gmail.com"; //เทส
                //string email = email_sendTo + ","+ email_mis ;// จริง

                jo_add_datalist _temp_u0 = _data_jo.jo_add_datalist_action[0];
                _mail_subject = "[MIS : Job Order] - ส่งมอบงานที่ดำเนินการเเก้ไขโดยฝ่าย MIS";

                _mail_body = _serviceMail.SentMailJob_orderCreateU1_(_data_jo.jo_add_datalist_action[0], link_system_joborder);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);

            }
            else
            {

            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Up_END(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 55); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insertlog_Vaddtitle(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 56); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Updatelastuser(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 57); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_jo = (data_jo)_funcTool.convertXmlToObject(typeof(data_jo), _local_xml);

            var email_mis = _data_jo.return_mailmis;

            if (_data_jo.return_code == 99)
            {
                string email_sendTo = "";

                if (_data_jo.job_order_overview_action[0].emp_email != null)
                {
                    email_sendTo = _data_jo.job_order_overview_action[0].emp_email.ToString();
                }
                string replyempcreate = "mancheep@gmail.com";
                string email = "mancheep@gmail.com"; //เทส
                 // string email = email_sendTo+","+ email_mis; //จริง
                job_order_overview _temp_u0 = _data_jo.job_order_overview_action[0];

                _mail_subject = "[MIS : Job Order] - จบการดำเนินงานของรายการร้องขอความต้องการต่อฝ่าย MIS";

                _mail_body = _serviceMail.SentMailJob_orderend(_data_jo.job_order_overview_action[0], link_system_joborder);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);

            }
            else
            {

            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Selectlogu1(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 41); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_jo = (data_jo)_funcTool.convertXmlToObject(typeof(data_jo), _local_xml);

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectWhologin(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 42); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Selectanywork(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 43); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    ////////////////

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Selecttoclose(string jsonIn)       // ?? ??? ????????????? ???? ??????????
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 14); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectDatajoborder_mhead(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 21); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SearchJoborder(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 40); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectDatajoborder_mishen_late(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_jo", "service_jo", _xml_in, 44); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
}