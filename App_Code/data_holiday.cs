﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_qa
/// </summary>
[Serializable]
[XmlRoot("data_holiday")]
public class data_holiday
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public emps_u0_holiday_detail[] emps_u0_holiday_list { get; set; }
    public emps_u0_holiday_manage_detail[] emps_u0_holiday_manage_list { get; set; }
    public emps_u1_holiday_manage_detail[] emps_u1_holiday_manage_list { get; set; }

}

public class emps_u0_holiday_detail
{
    
    public int holiday_idx { get; set; }
    public int date_idx { get; set; }
    public string holiday_name { get; set; }
    public string holiday_date { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int cemp_idx { get; set; }
    public int holiday_status { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int decision { get; set; }
    public int condition { get; set; }
    public int year_idx { get; set; }
    public string year_holiday { get; set; }


}

public class emps_u0_holiday_manage_detail
{
 
    public int u0_manage_idx { get; set; }
    public int org_idx { get; set; }
    public int u0_manage_status { get; set; }
    public int cemp_idx_update { get; set; }
    public int holiday_idx { get; set; }
    public int date_idx { get; set; }
    public string holiday_name { get; set; }
    public string holiday_date { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int cemp_idx { get; set; }
    public int holiday_status { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int decision { get; set; }
    public int condition { get; set; }
    public int year_idx { get; set; }
    public string year_holiday { get; set; }
    public string org_name_th { get; set; }

}

public class emps_u1_holiday_manage_detail
{

    public int u0_manage_idx { get; set; }
    public int u1_manage_idx { get; set; }
    public int u1_manage_status { get; set; }
    public int org_idx { get; set; }
    public int cemp_idx_update { get; set; }
    public int u0_manage_status { get; set; }
    public int holiday_idx { get; set; }
    public int date_idx { get; set; }
    public string holiday_name { get; set; }
    public string holiday_date { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int cemp_idx { get; set; }
    public int holiday_status { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int decision { get; set; }
    public int condition { get; set; }
    public int year_idx { get; set; }
    public string year_holiday { get; set; }

}
