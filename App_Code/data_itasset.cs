﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
/// <summary>
/// Summary description for data_itasset
/// </summary>
[Serializable]
[XmlRoot("data_itasset")]
public class data_itasset
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }

    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }

    [XmlElement("ReturnDocCode")]
    public string ReturnDocCode { get; set; }

    [XmlElement("boxdata_u0document_itasset")]
    public u0_document_itasset[] boxdata_u0document_itasset { get; set; }

    [XmlElement("boxmaster_class")]
    public class_itasset[] boxmaster_class { get; set; }

    [XmlElement("boxmaster_asset")]
    public asset_itasset[] boxmaster_asset { get; set; }

    [XmlElement("boxmaster_io")]
    public io_itasset[] boxmaster_io { get; set; }


    [XmlElement("boxprice_reference")]
    public price_reference[] boxprice_reference { get; set; }

    [XmlElement("boxm1price_reference")]
    public m1price_reference[] boxm1price_reference { get; set; }

    [XmlElement("boxdevices_reference")]
    public devices_reference[] boxdevices_reference { get; set; }

    [XmlElement("boxu0memo_reference")]
    public u0memo_reference[] boxu0memo_reference { get; set; }

    [XmlElement("boxu1memo_n_reference")]
    public u1memo_n_reference[] boxu1memo_n_reference { get; set; }

    [XmlElement("boxu1memo_s_reference")]
    public u1memo_s_reference[] boxu1memo_s_reference { get; set; }

    [XmlElement("boxu2memo_s_reference")]
    public u2memo_s_reference[] boxu2memo_s_reference { get; set; }

    //teppanop

    public its_lookup[] its_lookup_action { get; set; }
    // public its_u_document[] its_u_document_action { get; set; }
    public its_masterit[] its_masterit_action { get; set; }

    [XmlElement("its_u_document_action")]
    public its_u_document[] its_u_document_action { get; set; }

    [XmlElement("its_TransferDevice_u0_action")]
    public its_TransferDevice_u0[] its_TransferDevice_u0_action { get; set; }

    [XmlElement("its_TransferDevice_u1_action")]
    public its_TransferDevice_u1[] its_TransferDevice_u1_action { get; set; }

    [XmlElement("its_TransferData_action")]
    public its_TransferData[] its_TransferData_action { get; set; }

    [XmlElement("its_TransferDevice_status_l0_action")]
    public its_TransferDevice_status_l0[] its_TransferDevice_status_l0_action { get; set; }

}

[Serializable]
public class u0_document_itasset
{
    [XmlElement("SysIDX")]
    public int SysIDX { get; set; }

    [XmlElement("SysNameEN")]
    public string SysNameEN { get; set; }


}

[Serializable]
public class devices_reference
{
    [XmlElement("m0_naidx")]
    public int m0_naidx { get; set; }

    [XmlElement("name_gen")]
    public string name_gen { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("m0_tyidx")]
    public int m0_tyidx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("m2_tyidx")]
    public int m2_tyidx { get; set; }

    [XmlElement("device_name")]
    public string device_name { get; set; }

}

[Serializable]
public class u0memo_reference
{
    [XmlElement("u0_meidx")]
    public int u0_meidx { get; set; }

    [XmlElement("doccode")]
    public string doccode { get; set; }

    [XmlElement("m0_type_idx")]
    public int m0_type_idx { get; set; }

    [XmlElement("m0_tmidx")]
    public int m0_tmidx { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("doc_status")]
    public int doc_status { get; set; }

    [XmlElement("CempIDX")]
    public int CempIDX { get; set; }

    [XmlElement("orgidx")]
    public int orgidx { get; set; }

    [XmlElement("rdeptidx")]
    public int rdeptidx { get; set; }

    [XmlElement("rsecidx")]
    public int rsecidx { get; set; }

    [XmlElement("rposidx")]
    public int rposidx { get; set; }

    [XmlElement("type_memo")]
    public string type_memo { get; set; }

    [XmlElement("name_its_type")]
    public string name_its_type { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("DateStart")]
    public string DateStart { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("Email")]
    public string Email { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("comment_headerit")]
    public string comment_headerit { get; set; }

    [XmlElement("comment_it")]
    public string comment_it { get; set; }

    [XmlElement("comment_dir")]
    public string comment_dir { get; set; }


}

[Serializable]
public class u1memo_n_reference
{
    [XmlElement("u1_meidx_n")]
    public int u1_meidx_n { get; set; }

    [XmlElement("u0_meidx")]
    public int u0_meidx { get; set; }

    [XmlElement("orgidx")]
    public int orgidx { get; set; }

    [XmlElement("rdeptidx")]
    public int rdeptidx { get; set; }

    [XmlElement("rsecidx")]
    public int rsecidx { get; set; }

    [XmlElement("posidx")]
    public int posidx { get; set; }

    [XmlElement("empidx")]
    public int empidx { get; set; }

    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }

    [XmlElement("hdidx")]
    public int hdidx { get; set; }

    [XmlElement("mtidx")]
    public int mtidx { get; set; }

    [XmlElement("CempIDX")]
    public int CempIDX { get; set; }

    [XmlElement("swidx_comma")]
    public string swidx_comma { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("pos_name")]
    public string pos_name { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("leveldevice")]
    public string leveldevice { get; set; }

    [XmlElement("detail")]
    public string detail { get; set; }

    [XmlElement("typedevices")]
    public string typedevices { get; set; }

    [XmlElement("price")]
    public string price { get; set; }

    [XmlElement("typedevices_monitor")]
    public string typedevices_monitor { get; set; }

    [XmlElement("Softwarename")]
    public string Softwarename { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("name_m0_typedevice")]
    public string name_m0_typedevice { get; set; }

    [XmlElement("Softwarename_rec")]
    public string Softwarename_rec { get; set; }

}

[Serializable]
public class u1memo_s_reference
{
    [XmlElement("u1_meidx_s")]
    public int u1_meidx_s { get; set; }

    [XmlElement("u0_meidx")]
    public int u0_meidx { get; set; }

    [XmlElement("orgidx")]
    public int orgidx { get; set; }

    [XmlElement("rdeptidx")]
    public int rdeptidx { get; set; }

    [XmlElement("rsecidx")]
    public int rsecidx { get; set; }

    [XmlElement("posidx")]
    public int posidx { get; set; }

    [XmlElement("empidx")]
    public int empidx { get; set; }

    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }

    [XmlElement("m2_tyidx")]
    public int m2_tyidx { get; set; }

    [XmlElement("m2_tyidx_comma")]
    public string m2_tyidx_comma { get; set; }

    [XmlElement("m0_tyidx_comma")]
    public string m0_tyidx_comma { get; set; }

    [XmlElement("device_name")]
    public string device_name { get; set; }

    [XmlElement("CempIDX")]
    public int CempIDX { get; set; }

    [XmlElement("swidx_comma")]
    public string swidx_comma { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("pos_name")]
    public string pos_name { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("name_gen")]
    public string name_gen { get; set; }


    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("leveldevice")]
    public string leveldevice { get; set; }

    [XmlElement("detail")]
    public string detail { get; set; }

    [XmlElement("remark")]
    public string remark { get; set; }


    [XmlElement("typedevices")]
    public string typedevices { get; set; }

    [XmlElement("price")]
    public string price { get; set; }

    [XmlElement("typedevices_monitor")]
    public string typedevices_monitor { get; set; }

    [XmlElement("Softwarename")]
    public string Softwarename { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("name_m0_typedevice")]
    public string name_m0_typedevice { get; set; }

    [XmlElement("Softwarename_rec")]
    public string Softwarename_rec { get; set; }

    [XmlElement("m0_tyidx")]
    public int m0_tyidx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("m0_naidx")]
    public int m0_naidx { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("staidx_doing")]
    public int staidx_doing { get; set; }

    [XmlElement("comment_doing")]
    public string comment_doing { get; set; }

    [XmlElement("staidx_dir")]
    public int staidx_dir { get; set; }

    [XmlElement("status_dir")]
    public string status_dir { get; set; }

    [XmlElement("comment_dir")]
    public string comment_dir { get; set; }

    [XmlElement("staidx_it")]
    public int staidx_it { get; set; }

    [XmlElement("status_it")]
    public string status_it { get; set; }

    [XmlElement("comment_it")]
    public string comment_it { get; set; }

    [XmlElement("staidx_headerit")]
    public int staidx_headerit { get; set; }

    [XmlElement("status_headerit")]
    public string status_headerit { get; set; }

    [XmlElement("comment_headerit")]
    public string comment_headerit { get; set; }



}

[Serializable]
public class u2memo_s_reference
{
    [XmlElement("u2_meidx_s")]
    public int u2_meidx_s { get; set; }

    [XmlElement("u1_meidx_s")]
    public int u1_meidx_s { get; set; }

    [XmlElement("m2_tyidx")]
    public int m2_tyidx { get; set; }

    [XmlElement("CempIDX")]
    public int CempIDX { get; set; }

}

[Serializable]
public class class_itasset
{
    [XmlElement("clidx")]
    public int clidx { get; set; }

    [XmlElement("code_class")]
    public string code_class { get; set; }

    [XmlElement("code_desc")]
    public string code_desc { get; set; }

    [XmlElement("code_status")]
    public int code_status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("Class_status")]
    public string Class_status { get; set; }

}

[Serializable]
public class io_itasset
{
    public int ioidx { get; set; }
    public string costcenter_io { get; set; }
    public string department { get; set; }
    public string description { get; set; }
    public decimal cost { get; set; }
    public int unit { get; set; }
    public decimal amountio { get; set; }
    public decimal budget { get; set; }
    public decimal variance { get; set; }
    public string iono { get; set; }
    public int newio { get; set; }
    public int replaceio { get; set; }
    public string no_cost { get; set; }
    public string expenses_year { get; set; }
    public string asset_type { get; set; }
    public string no_order { get; set; }
    public int status { get; set; }
    public int CEmpIDX { get; set; }
    public int usestatus { get; set; }
    public int balance { get; set; }
    public int year_io { get; set; }
    public int CostIDX { get; set; }


}

[Serializable]
public class asset_itasset
{
    [XmlElement("asidx")]
    public int asidx { get; set; }

    [XmlElement("Asset")]
    public string Asset { get; set; }

    [XmlElement("SNo")]
    public string SNo { get; set; }

    [XmlElement("CostIDX")]
    public int CostIDX { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("CapDate")]
    public string CapDate { get; set; }

    [XmlElement("Asset_Description")]
    public string Asset_Description { get; set; }

    [XmlElement("AcquisVal")]
    public string AcquisVal { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("Asset_Status")]
    public int Asset_Status { get; set; }

    [XmlElement("CostCenter")]
    public string CostCenter { get; set; }

    [XmlElement("AssetDetailStatus")]
    public string AssetDetailStatus { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }



}

[Serializable]
public class price_reference
{
    public int SysIDX { get; set; }
    public string SysNameEN { get; set; }
    public int typidx { get; set; }

    public string leveldevice { get; set; }
    public int m0idx { get; set; }

    public int tdidx { get; set; }

    public int sfidx { get; set; }
    public int u0_didx { get; set; }
    public int joblevel { get; set; }

    public string detail { get; set; }

    public string use_year { get; set; }

    public string price { get; set; }
    public string ma_price { get; set; }
    public string remark { get; set; }

    public int m0status { get; set; }

    public int CEmpIDX { get; set; }

    public string type_name { get; set; }

    public string typedevices { get; set; }
    public int m0_tdidx { get; set; }
    public int software_name_idx { get; set; }
    public int checksearch { get; set; }
    public int posidx { get; set; }
    public string pos_name { get; set; }
    public int m1idx { get; set; }
    public int orgidx { get; set; }
    public int rdeptidx { get; set; }
    public int m1status { get; set; }
    public int m2idx { get; set; }
    public int rsecidx { get; set; }
    public int m2status { get; set; }
    public string OrgNameTH { get; set; }
    public string DeptNameTH { get; set; }
    public string SecNameTH { get; set; }
    public string PosNameTH { get; set; }
    public string emp_name_th { get; set; }

    public string emp_code { get; set; }
    public int TIDX { get; set; }

}

[Serializable]
public class m1price_reference
{
    public int m1idx { get; set; }
    public int m0idx { get; set; }
    public int orgidx { get; set; }
    public int rdeptidx { get; set; }
    public int m1status { get; set; }
    public int CEmpIDX { get; set; }
    public int m2idx { get; set; }
    public int rsecidx { get; set; }
    public int m2status { get; set; }
    public int posidx { get; set; }

}

[Serializable]
public class its_lookup
{
    public int idx { get; set; }
    public string operation_status_id { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int sysidx { get; set; }
    public int typidx { get; set; }
    public string leveldevice { get; set; }
    public decimal price { get; set; }
    public int jobgrade_level { get; set; }
    public int emp_idx { get; set; }
    public int rpos_idx { get; set; }

    public string zname { get; set; }
    public string zplace_idx { get; set; }
    public int item_count { get; set; }

    public int zmonth { get; set; }
    public int zyear { get; set; }
    public string detail { get; set; }


}
[Serializable]
public class its_masterit
{
    //search
    public string filter_keyword { get; set; }
    public int idx { get; set; }
    public string operation_status_id { get; set; }

    //its_m0_specit
    public int m0_spec_idx { get; set; }
    public string spec_name { get; set; }
    public string spec_remark { get; set; }
    public int spec_flag { get; set; }
    public int spec_status { get; set; }
    public int spec_created_by { get; set; }
    public int spec_updated_by { get; set; }

    //its_m0_device_pr_status
    public int m0_pr_idx { get; set; }
    public string pr_name { get; set; }
    public string pr_remark { get; set; }
    public int pr_flag { get; set; }
    public int pr_status { get; set; }
    public int pr_created_by { get; set; }
    public int pr_updated_by { get; set; }

    //its_m0_io_budget
    public int ioidx { get; set; }
    public string costcenter_io { get; set; }
    public string department { get; set; }
    public string description { get; set; }
    public decimal cost { get; set; }
    public int unit { get; set; }
    public decimal amountio { get; set; }
    public decimal budget { get; set; }
    public decimal variance { get; set; }
    public string iono { get; set; }
    public int newio { get; set; }
    public int replaceio { get; set; }
    public string no_cost { get; set; }
    public string expenses_year { get; set; }
    public string asset_type { get; set; }
    public string no_order { get; set; }
    public int status { get; set; }
    public int CEmpIDX { get; set; }
    public int usestatus { get; set; }
    public int out_qty { get; set; }
    public int balance_qty { get; set; }
    public int pr_type_idx { get; set; }
    public int u0idx { get; set; }
    public int costcenter_idx { get; set; }

}

/*
[Serializable]
public class its_u_document
{
    //search
    public int idx { get; set; }
    public string operation_status_id { get; set; }
    public string filter_keyword { get; set; }
    public string zstatus { get; set; }
    public int zmonth { get; set; }
    public int zyear { get; set; }
    public int joblevel { get; set; }
    public int condition_date_type { get; set; }
    public string start_date { get; set; }
    public string end_date { get; set; }
    public int rsec_idx { get; set; }
    public string zsql { get; set; }
    public string zu0idx { get; set; }
    public int rdept_idx { get; set; }
    public string zu1idx { get; set; }

    //approver / node
    public int u0_idx { get; set; }
    public int node_idx { get; set; }
    public int actor_idx { get; set; }
    public int approve_status { get; set; }
    public string approve_remark { get; set; }
    public string zdate { get; set; }

    public int app_flag { get; set; }
    public int app_user { get; set; }
    public string app_date { get; set; }

    public int flow_item { get; set; }
    public int director_emp_idx { get; set; }
    public int director_approve_status { get; set; }
    public string director_approve_remark { get; set; }
    public int budget_emp_idx { get; set; }
    public int budget_approve_status { get; set; }
    public string budget_approve_remark { get; set; }
    public int docket_item { get; set; }

    //create / update user
    public int CEmpIDX { get; set; }
    public string CreateDate { get; set; }
    public int UEmpIDX { get; set; }
    public string UpdateDate { get; set; }
    
    //lookup
    public string emp_code { get; set; }
    public string emp_name_th { get; set; }
    public string org_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_th { get; set; }
    public string pos_name_th { get; set; }
    public string name_purchase_type { get; set; }
    public string zdocdate { get; set; }
    public string zdevices_name { get; set; }
    public string status_name { get; set; }
    public string node_name { get; set; }
    public string actor_name { get; set; }
    public string emp_email { get; set; }
    public string emp_mobile_no { get; set; }
    public string type_name { get; set; }
    public string currency_th { get; set; }
    public string NameTH { get; set; }
    public string CostNo { get; set; }
    public string place_name { get; set; }
    public string SysNameTH { get; set; }
    public string zplace_idx { get; set; }
    public int item_count { get; set; }
    public int zselected { get; set; }
    public string spec_name { get; set; }

    //its_u0_document
    public int u0idx { get; set; }
    public string doccode { get; set; }
    public string docdate { get; set; }
    public int moidx { get; set; }
    public string datailUser { get; set; }
    public string remark { get; set; }
    public string dococde_ref { get; set; }
    public int fileuser { get; set; }
    public int doc_status { get; set; }
    public int emp_idx_its { get; set; }
    public int pr_type_idx { get; set; }
    public string file_memo { get; set; }
    public string file_organization { get; set; }
    public string file_cutoff { get; set; }
    public int org_idx_its { get; set; }
    public int rdept_idx_its { get; set; }
    public int rpos_idx_its { get; set; }
    public int rsec_idx_its { get; set; }
    public int its_status { get; set; }
    public int emp_idx_director { get; set; }

    public int asset_emp_idx { get; set; }
    public string asset_date { get; set; }
    public int asset_approve_status { get; set; }
    public string asset_remark { get; set; }

    public int asset_dir_emp_idx { get; set; }
    public string asset_dir_approve_date { get; set; }
    public int asset_dir_approve_status { get; set; }
    public string asset_dir_approve_remark { get; set; }

    public int itsp_emp_idx { get; set; }
    public string itsp_date { get; set; }
    public int itsp_status { get; set; }
    public string itsp_remark { get; set; }

    public int itsp_mg_emp_idx { get; set; }
    public string itsp_mg_date { get; set; }
    public int itsp_mg_status { get; set; }
    public string itsp_mg_remark { get; set; }

    public int itsp_dir_emp_idx { get; set; }
    public string itsp_dir_date { get; set; }
    public int itsp_dir_status { get; set; }
    public string itsp_dir_remark { get; set; }
    
    public int itsp_summ_emp_idx { get; set; }
    public string itsp_summ_date { get; set; }
    public int itsp_summ_status { get; set; }
    public string itsp_summ_remark { get; set; }

    public int md_emp_idx { get; set; }
    public string md_date { get; set; }
    public int md_status { get; set; }
    public string md_remark { get; set; }

    //its_u1_document
    public int u1idx { get; set; }
    public int clidx { get; set; }
    public int asidx { get; set; }
    public int sysidx { get; set; }
    public int typidx { get; set; }
    public int tdidx { get; set; }
    public string ionum { get; set; }
    public string ref_itnum { get; set; }
    public int rpos_idx { get; set; }
    public string list_menu { get; set; }
    public decimal price { get; set; }
    public int currency_idx { get; set; }
    public int qty { get; set; }
    public int unidx { get; set; }
    public int costidx { get; set; }
    public decimal budget_set { get; set; }
    public int place_idx { get; set; }
    public int acquls_val { get; set; }
    public int book_val { get; set; }
    public string action { get; set; }
    public int u1_status { get; set; }
    public int spec_idx { get; set; }
    public string name_spec { get; set; }
    public string leveldevice { get; set; }
    public decimal total_amount { get; set; }
    public string spec_remark { get; set; }

    //its_document_assetno_u2 
    public int asset_item { get; set; }
    public int u2_status { get; set; }
    public string asset_no { get; set; }
    public int u2idx { get; set; }
    public int emp_idx { get; set; }
    public int u1_docket_flag { get; set; }

    //its_docket_u0
    public int u0_docket_idx { get; set; }
    public int u0_status { get; set; }

    //its_docket_u1
    public int u1_docket_idx { get; set; }

    //its_docket_u2
    public int u2_docket_idx { get; set; }

    public int total_qty { get; set; }
    public decimal amount { get; set; }
    public int balance_qty { get; set; }
    public int total_out_qty { get; set; }
    public int total_balance_qty { get; set; }
    public int out_qty { get; set; }
    public int pr_idx { get; set; }
    public int pr_emp_idx { get; set; }
    
    public int org_idx { get; set; }

    public string docket_remark { get; set; }
    public string zu0_docket_idx { get; set; }
    public string pr_remark { get; set; }

    public int rdept_idx_admin { get; set; }
    public int rdept_idx_search { get; set; }

    public int zapp_status { get; set; }
    public string zapp_remark { get; set; }
    public int zapp_emp_idx { get; set; }
    public string zappdate { get; set; }
    public string zapp_date { get; set; }
    public string zstatus_name { get; set; }
    public string from_actor_name { get; set; }

}
*/
[Serializable]
public class its_u_document
{
    //search
    [XmlElement("idx")]
    public int idx { get; set; }
    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }
    [XmlElement("filter_keyword")]
    public string filter_keyword { get; set; }
    [XmlElement("zstatus")]
    public string zstatus { get; set; }
    [XmlElement("zmonth")]
    public int zmonth { get; set; }
    [XmlElement("zyear")]
    public int zyear { get; set; }
    [XmlElement("joblevel")]
    public int joblevel { get; set; }
    [XmlElement("condition_date_type")]
    public int condition_date_type { get; set; }
    [XmlElement("start_date")]
    public string start_date { get; set; }
    [XmlElement("end_date")]
    public string end_date { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("zsql")]
    public string zsql { get; set; }
    [XmlElement("zu0idx")]
    public string zu0idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("zu1idx")]
    public string zu1idx { get; set; }

    //approver / node
    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }
    [XmlElement("node_idx")]
    public int node_idx { get; set; }
    [XmlElement("actor_idx")]
    public int actor_idx { get; set; }
    [XmlElement("approve_status")]
    public int approve_status { get; set; }
    [XmlElement("approve_remark")]
    public string approve_remark { get; set; }
    [XmlElement("zdate")]
    public string zdate { get; set; }

    [XmlElement("app_flag")]
    public int app_flag { get; set; }
    [XmlElement("app_user")]
    public int app_user { get; set; }
    [XmlElement("app_date")]
    public string app_date { get; set; }

    [XmlElement("flow_item")]
    public int flow_item { get; set; }
    [XmlElement("director_emp_idx")]
    public int director_emp_idx { get; set; }
    [XmlElement("director_approve_status")]
    public int director_approve_status { get; set; }
    [XmlElement("director_approve_remark")]
    public string director_approve_remark { get; set; }
    [XmlElement("budget_emp_idx")]
    public int budget_emp_idx { get; set; }
    [XmlElement("budget_approve_status")]
    public int budget_approve_status { get; set; }
    [XmlElement("budget_approve_remark")]
    public string budget_approve_remark { get; set; }
    [XmlElement("docket_item")]
    public int docket_item { get; set; }

    //create / update user
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }
    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }
    [XmlElement("UEmpIDX")]
    public int UEmpIDX { get; set; }
    [XmlElement("UpdateDate")]
    public string UpdateDate { get; set; }

    //lookup
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("tax_no")]
    public string tax_no { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement("name_purchase_type")]
    public string name_purchase_type { get; set; }
    [XmlElement("zdocdate")]
    public string zdocdate { get; set; }
    [XmlElement("zdevices_name")]
    public string zdevices_name { get; set; }
    [XmlElement("status_name")]
    public string status_name { get; set; }
    [XmlElement("node_name")]
    public string node_name { get; set; }
    [XmlElement("actor_name")]
    public string actor_name { get; set; }
    [XmlElement("emp_email")]
    public string emp_email { get; set; }
    [XmlElement("emp_mobile_no")]
    public string emp_mobile_no { get; set; }
    [XmlElement("type_name")]
    public string type_name { get; set; }
    [XmlElement("currency_th")]
    public string currency_th { get; set; }
    [XmlElement("NameTH")]
    public string NameTH { get; set; }
    [XmlElement("CostNo")]
    public string CostNo { get; set; }
    [XmlElement("place_name")]
    public string place_name { get; set; }
    [XmlElement("SysNameTH")]
    public string SysNameTH { get; set; }
    [XmlElement("zplace_idx")]
    public string zplace_idx { get; set; }
    [XmlElement("item_count")]
    public int item_count { get; set; }
    [XmlElement("zselected")]
    public int zselected { get; set; }
    [XmlElement("spec_name")]
    public string spec_name { get; set; }
    [XmlElement("selected")]
    public Boolean selected { get; set; }
    [XmlElement("spec_remark")]
    public string spec_remark { get; set; }
    [XmlElement("pr_name")]
    public string pr_name { get; set; }

    //its_u0_document
    [XmlElement("u0idx")]
    public int u0idx { get; set; }
    [XmlElement("doccode")]
    public string doccode { get; set; }
    [XmlElement("docdate")]
    public string docdate { get; set; }
    [XmlElement("moidx")]
    public int moidx { get; set; }
    [XmlElement("datailUser")]
    public string datailUser { get; set; }
    [XmlElement("remark")]
    public string remark { get; set; }
    [XmlElement("dococde_ref")]
    public string dococde_ref { get; set; }
    [XmlElement("fileuser")]
    public int fileuser { get; set; }
    [XmlElement("doc_status")]
    public int doc_status { get; set; }
    [XmlElement("emp_idx_its")]
    public int emp_idx_its { get; set; }
    [XmlElement("pr_type_idx")]
    public int pr_type_idx { get; set; }
    [XmlElement("file_memo")]
    public string file_memo { get; set; }
    [XmlElement("file_organization")]
    public string file_organization { get; set; }
    [XmlElement("file_cutoff")]
    public string file_cutoff { get; set; }
    [XmlElement("org_idx_its")]
    public int org_idx_its { get; set; }
    [XmlElement("rdept_idx_its")]
    public int rdept_idx_its { get; set; }
    [XmlElement("rpos_idx_its")]
    public int rpos_idx_its { get; set; }
    [XmlElement("rsec_idx_its")]
    public int rsec_idx_its { get; set; }
    [XmlElement("its_status")]
    public int its_status { get; set; }
    [XmlElement("emp_idx_director")]
    public int emp_idx_director { get; set; }
    [XmlElement("asset_emp_idx")]
    public int asset_emp_idx { get; set; }
    [XmlElement("asset_date")]
    public string asset_date { get; set; }
    [XmlElement("asset_approve_status")]
    public int asset_approve_status { get; set; }
    [XmlElement("asset_remark")]
    public string asset_remark { get; set; }
    [XmlElement("asset_dir_emp_idx")]
    public int asset_dir_emp_idx { get; set; }
    [XmlElement("asset_dir_approve_date")]
    public string asset_dir_approve_date { get; set; }
    [XmlElement("asset_dir_approve_status")]
    public int asset_dir_approve_status { get; set; }
    [XmlElement("asset_dir_approve_remark")]
    public string asset_dir_approve_remark { get; set; }

    [XmlElement("itsp_emp_idx")]
    public int itsp_emp_idx { get; set; }
    [XmlElement("itsp_date")]
    public string itsp_date { get; set; }
    [XmlElement("itsp_status")]
    public int itsp_status { get; set; }
    [XmlElement("itsp_remark")]
    public string itsp_remark { get; set; }

    [XmlElement("itsp_mg_emp_idx")]
    public int itsp_mg_emp_idx { get; set; }
    [XmlElement("itsp_mg_date")]
    public string itsp_mg_date { get; set; }
    [XmlElement("itsp_mg_status")]
    public int itsp_mg_status { get; set; }
    [XmlElement("itsp_mg_remark")]
    public string itsp_mg_remark { get; set; }

    [XmlElement("itsp_dir_emp_idx")]
    public int itsp_dir_emp_idx { get; set; }
    [XmlElement("itsp_dir_date")]
    public string itsp_dir_date { get; set; }
    [XmlElement("itsp_dir_status")]
    public int itsp_dir_status { get; set; }
    [XmlElement("itsp_dir_remark")]
    public string itsp_dir_remark { get; set; }

    [XmlElement("itsp_summ_emp_idx")]
    public int itsp_summ_emp_idx { get; set; }
    [XmlElement("itsp_summ_date")]
    public string itsp_summ_date { get; set; }
    [XmlElement("itsp_summ_status")]
    public int itsp_summ_status { get; set; }
    [XmlElement("itsp_summ_remark")]
    public string itsp_summ_remark { get; set; }

    [XmlElement("md_emp_idx")]
    public int md_emp_idx { get; set; }
    [XmlElement("md_date")]
    public string md_date { get; set; }
    [XmlElement("md_status")]
    public int md_status { get; set; }
    [XmlElement("md_remark")]
    public string md_remark { get; set; }

    //its_u1_document
    [XmlElement("u1idx")]
    public int u1idx { get; set; }
    [XmlElement("clidx")]
    public int clidx { get; set; }
    [XmlElement("asidx")]
    public int asidx { get; set; }
    [XmlElement("sysidx")]
    public int sysidx { get; set; }
    [XmlElement("typidx")]
    public int typidx { get; set; }
    [XmlElement("tdidx")]
    public int tdidx { get; set; }
    [XmlElement("ionum")]
    public string ionum { get; set; }
    [XmlElement("ref_itnum")]
    public string ref_itnum { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }
    [XmlElement("list_menu")]
    public string list_menu { get; set; }
    [XmlElement("price")]
    public decimal price { get; set; }
    [XmlElement("currency_idx")]
    public int currency_idx { get; set; }
    [XmlElement("qty")]
    public int qty { get; set; }
    [XmlElement("unidx")]
    public int unidx { get; set; }
    [XmlElement("costidx")]
    public int costidx { get; set; }
    [XmlElement("budget_set")]
    public decimal budget_set { get; set; }
    [XmlElement("place_idx")]
    public int place_idx { get; set; }
    [XmlElement("acquls_val")]
    public int acquls_val { get; set; }
    [XmlElement("book_val")]
    public int book_val { get; set; }
    [XmlElement("action")]
    public string action { get; set; }
    [XmlElement("u1_status")]
    public int u1_status { get; set; }
    [XmlElement("spec_idx")]
    public int spec_idx { get; set; }
    [XmlElement("name_spec")]
    public string name_spec { get; set; }
    [XmlElement("leveldevice")]
    public string leveldevice { get; set; }
    [XmlElement("total_amount")]
    public decimal total_amount { get; set; }
    [XmlElement("special_flag")]
    public int special_flag { get; set; }

    //its_document_assetno_u2
    [XmlElement("asset_item")]
    public int asset_item { get; set; }
    [XmlElement("u2_status")]
    public int u2_status { get; set; }
    [XmlElement("asset_no")]
    public string asset_no { get; set; }
    [XmlElement("u2idx")]
    public int u2idx { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("u1_docket_flag")]
    public int u1_docket_flag { get; set; }

    //its_docket_u0
    [XmlElement("u0_docket_idx")]
    public int u0_docket_idx { get; set; }
    [XmlElement("u0_status")]
    public int u0_status { get; set; }

    //its_docket_u1
    [XmlElement("u1_docket_idx")]
    public int u1_docket_idx { get; set; }

    //its_docket_u2
    [XmlElement("u2_docket_idx")]
    public int u2_docket_idx { get; set; }
    [XmlElement("total_qty")]
    public int total_qty { get; set; }
    [XmlElement("amount")]
    public decimal amount { get; set; }
    [XmlElement("balance_qty")]
    public int balance_qty { get; set; }
    [XmlElement("total_out_qty")]
    public int total_out_qty { get; set; }
    [XmlElement("total_balance_qty")]
    public int total_balance_qty { get; set; }
    [XmlElement("out_qty")]
    public int out_qty { get; set; }
    [XmlElement("pr_idx")]
    public int pr_idx { get; set; }
    [XmlElement("pr_emp_idx")]
    public int pr_emp_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("docket_remark")]
    public string docket_remark { get; set; }
    [XmlElement("zu0_docket_idx")]
    public string zu0_docket_idx { get; set; }
    [XmlElement("pr_remark")]
    public string pr_remark { get; set; }
    [XmlElement("rdept_idx_admin")]
    public int rdept_idx_admin { get; set; }
    [XmlElement("rdept_idx_search")]
    public int rdept_idx_search { get; set; }

    [XmlElement("zapp_status")]
    public int zapp_status { get; set; }
    [XmlElement("zapp_remark")]
    public string zapp_remark { get; set; }
    [XmlElement("zapp_emp_idx")]
    public int zapp_emp_idx { get; set; }
    [XmlElement("zappdate")]
    public string zappdate { get; set; }
    [XmlElement("zapp_date")]
    public string zapp_date { get; set; }
    [XmlElement("zstatus_name")]
    public string zstatus_name { get; set; }
    [XmlElement("from_actor_name")]
    public string from_actor_name { get; set; }
    [XmlElement("org_address")]
    public string org_address { get; set; }
    [XmlElement("to_actor_name")]
    public string to_actor_name { get; set; }

    [XmlElement("menu_seq")]
    public int menu_seq { get; set; }
    [XmlElement("menu")]
    public string menu { get; set; }
    [XmlElement("menu_list_seq")]
    public int menu_list_seq { get; set; }
    [XmlElement("menu_list")]
    public string menu_list { get; set; }
    [XmlElement("zCount")]
    public int zCount { get; set; }
    [XmlElement("jobgrade_level_dir")]
    public int jobgrade_level_dir { get; set; }

    [XmlElement("user_status")]
    public int user_status { get; set; }
    [XmlElement("user_remark")]
    public string user_remark { get; set; }

    [XmlElement("sysidx_admin")]
    public int sysidx_admin { get; set; }

    [XmlElement("locidx")]
    public int locidx { get; set; }

    [XmlElement("posidx")]
    public int posidx { get; set; }
    [XmlElement("detail")]
    public string detail { get; set; }
    [XmlElement("m1_ref_idx")]
    public int m1_ref_idx { get; set; }
    [XmlElement("pos_name")]
    public string pos_name { get; set; }
    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }
    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("department")]
    public string department { get; set; }
    [XmlElement("description")]
    public string description { get; set; }
    [XmlElement("cost")]
    public decimal cost { get; set; }
    [XmlElement("unit")]
    public decimal unit { get; set; }
    [XmlElement("amountio")]
    public decimal amountio { get; set; }
    [XmlElement("budget")]
    public decimal budget { get; set; }
    [XmlElement("variance")]
    public decimal variance { get; set; }

}

[Serializable]
public class its_TransferDevice_u0
{
    [XmlElement("zselected")]
    public int zselected { get; set; }
    [XmlElement("idx")]
    public int idx { get; set; }
    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }

    [XmlElement("filter_keyword")]
    public string filter_keyword { get; set; }
    [XmlElement("zstatus")]
    public string zstatus { get; set; }
    [XmlElement("zmonth")]
    public int zmonth { get; set; }
    [XmlElement("zyear")]
    public int zyear { get; set; }
    [XmlElement("joblevel")]
    public int joblevel { get; set; }

    [XmlElement("zsql")]
    public string zsql { get; set; }
    [XmlElement("zu0idx")]
    public int zu0idx { get; set; }
    [XmlElement("zu1idx")]
    public int zu1idx { get; set; }
    [XmlElement("zname")]
    public string zname { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }
    [XmlElement("posidx")]
    public int posidx { get; set; }
    [XmlElement("flow_item")]
    public int flow_item { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("cost_idx")]
    public int cost_idx { get; set; }
    [XmlElement("n_cost_idx")]
    public int n_cost_idx { get; set; }
    [XmlElement("transfer_flag")]
    public int transfer_flag { get; set; }

    //approver / node
    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }
    [XmlElement("node_idx")]
    public int node_idx { get; set; }
    [XmlElement("actor_idx")]
    public int actor_idx { get; set; }
    [XmlElement("approve_status")]
    public int approve_status { get; set; }
    [XmlElement("approve_remark")]
    public string approve_remark { get; set; }
    [XmlElement("zdate")]
    public string zdate { get; set; }

    //create / update user
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }
    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }
    [XmlElement("UEmpIDX")]
    public int UEmpIDX { get; set; }
    [XmlElement("UpdateDate")]
    public string UpdateDate { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("locidx")]
    public int locidx { get; set; }
    [XmlElement("u0_status")]
    public int u0_status { get; set; }
    [XmlElement("u0idx")]
    public int u0idx { get; set; }
    [XmlElement("u1idx")]
    public int u1idx { get; set; }
    [XmlElement("doc_status")]
    public int doc_status { get; set; }
    [XmlElement("doccode")]
    public string doccode { get; set; }

    //////////////
    [XmlElement("sysidx")]
    public int sysidx { get; set; }

    [XmlElement("mg_emp_idx")]
    public int mg_emp_idx { get; set; }
    [XmlElement("dir_emp_idx")]
    public int dir_emp_idx { get; set; }
    [XmlElement("n_org_idx")]
    public int n_org_idx { get; set; }
    [XmlElement("n_rdept_idx")]
    public int n_rdept_idx { get; set; }
    [XmlElement("n_rsec_idx")]
    public int n_rsec_idx { get; set; }
    [XmlElement("n_rpos_idx")]
    public int n_rpos_idx { get; set; }
    [XmlElement("n_emp_idx")]
    public int n_emp_idx { get; set; }
    [XmlElement("n_mg_emp_idx")]
    public int n_mg_emp_idx { get; set; }
    [XmlElement("n_dir_emp_idx")]
    public int n_dir_emp_idx { get; set; }
    [XmlElement("u1_transf_idx")]
    public int u1_transf_idx { get; set; }
    [XmlElement("app_emp_idx")]
    public int app_emp_idx { get; set; }
    [XmlElement("app_date")]
    public string app_date { get; set; }
    [XmlElement("app_status")]
    public int app_status { get; set; }
    [XmlElement("app_remark")]
    public string app_remark { get; set; }

    [XmlElement("tdidx")]
    public int tdidx { get; set; }

    [XmlElement("DeviceIDX")]
    public int DeviceIDX { get; set; }
    [XmlElement("DeviceCode")]
    public string DeviceCode { get; set; }
    [XmlElement("SerialNumber")]
    public string SerialNumber { get; set; }
    [XmlElement("EqtName")]
    public string EqtName { get; set; }
    [XmlElement("HDEmpFullName")]
    public string HDEmpFullName { get; set; }
    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("name_m0_typedevice")]
    public string name_m0_typedevice { get; set; }
    [XmlElement("u0_acc")]
    public string u0_acc { get; set; }
    [XmlElement("device_idx")]
    public int device_idx { get; set; }

    [XmlElement("asidx")]
    public int asidx { get; set; }
    [XmlElement("acquis_val")]
    public decimal acquis_val { get; set; }
    [XmlElement("desc_name")]
    public string desc_name { get; set; }
    [XmlElement("doc_u2idx")]
    public int doc_u2idx { get; set; }
    [XmlElement("ref_itnum")]
    public string ref_itnum { get; set; }
    [XmlElement("ionum")]
    public string ionum { get; set; }

    [XmlElement("n_place_idx")]
    public int n_place_idx { get; set; }
    [XmlElement("place_idx")]
    public int place_idx { get; set; }
    [XmlElement("remark")]
    public string remark { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("tax_no")]
    public string tax_no { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement("zdocdate")]
    public string zdocdate { get; set; }
    [XmlElement("u0_transf_idx")]
    public int u0_transf_idx { get; set; }

    [XmlElement("n_org_name_th")]
    public string n_org_name_th { get; set; }
    [XmlElement("n_dept_name_th")]
    public string n_dept_name_th { get; set; }
    [XmlElement("n_sec_name_th")]
    public string n_sec_name_th { get; set; }
    [XmlElement("n_pos_name_th")]
    public string n_pos_name_th { get; set; }
    [XmlElement("sys_name")]
    public string sys_name { get; set; }
    [XmlElement("location_name")]
    public string location_name { get; set; }
    [XmlElement("from_name")]
    public string from_name { get; set; }
    [XmlElement("to_name")]
    public string to_name { get; set; }

    [XmlElement("asset_no")]
    public string asset_no { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }
    [XmlElement("n_emp_name_th")]
    public string n_emp_name_th { get; set; }
    [XmlElement("n_emp_email")]
    public string n_emp_email { get; set; }


    [XmlElement("BuildingIDX")]
    public int BuildingIDX { get; set; }
    [XmlElement("RoomIDX")]
    public int RoomIDX { get; set; }
    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }
    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("n_BuildingIDX")]
    public int n_BuildingIDX { get; set; }
    [XmlElement("n_RoomIDX")]
    public int n_RoomIDX { get; set; }
    [XmlElement("sysidx_admin")]
    public int sysidx_admin { get; set; }

    [XmlElement("menu_seq")]
    public int menu_seq { get; set; }
    [XmlElement("menu")]
    public string menu { get; set; }
    [XmlElement("menu_list_seq")]
    public int menu_list_seq { get; set; }
    [XmlElement("menu_list")]
    public string menu_list { get; set; }
    [XmlElement("zCount")]
    public int zCount { get; set; }
    [XmlElement("jobgrade_level_dir")]
    public int jobgrade_level_dir { get; set; }

    //search
    [XmlElement("condition_date_type")]
    public int condition_date_type { get; set; }
    [XmlElement("start_date")]
    public string start_date { get; set; }
    [XmlElement("end_date")]
    public string end_date { get; set; }
    [XmlElement("org_idx_search")]
    public int org_idx_search { get; set; }
    [XmlElement("rdept_idx_search")]
    public int rdept_idx_search { get; set; }
    [XmlElement("locidx_search")]
    public int locidx_search { get; set; }
    [XmlElement("sysidx_search")]
    public int sysidx_search { get; set; }
    [XmlElement("rdept_idx_admin")]
    public int rdept_idx_admin { get; set; }


}

[Serializable]
public class its_TransferDevice_u1
{
    [XmlElement("u1_transf_idx")]
    public int u1_transf_idx { get; set; }
    [XmlElement("u0_transf_idx")]
    public int u0_transf_idx { get; set; }
    [XmlElement("u1_status")]
    public int u1_status { get; set; }
    [XmlElement("device_idx")]
    public int device_idx { get; set; }
    [XmlElement("asidx")]
    public int asidx { get; set; }
    [XmlElement("asset_no")]
    public string asset_no { get; set; }
    [XmlElement("qty")]
    public int qty { get; set; }
    [XmlElement("acquis_val")]
    public decimal acquis_val { get; set; }
    [XmlElement("book_val")]
    public decimal book_val { get; set; }
    [XmlElement("price")]
    public decimal price { get; set; }
    [XmlElement("ref_itnum")]
    public string ref_itnum { get; set; }
    [XmlElement("ionum")]
    public string ionum { get; set; }
    [XmlElement("UEmpIDX")]
    public int UEmpIDX { get; set; }
    [XmlElement("UpdateDate")]
    public string UpdateDate { get; set; }
    [XmlElement("doc_u2idx")]
    public int doc_u2idx { get; set; }
    [XmlElement("tdidx")]
    public int tdidx { get; set; }
    [XmlElement("desc_name")]
    public string desc_name { get; set; }
    [XmlElement("name_m0_typedevice")]
    public string name_m0_typedevice { get; set; }

    [XmlElement("o_org_idx")]
    public int o_org_idx { get; set; }
    [XmlElement("o_rdept_idx")]
    public int o_rdept_idx { get; set; }
    [XmlElement("o_rsec_idx")]
    public int o_rsec_idx { get; set; }
    [XmlElement("o_rpos_idx")]
    public int o_rpos_idx { get; set; }
    [XmlElement("o_emp_idx")]
    public int o_emp_idx { get; set; }

    [XmlElement("o_LocIDX")]
    public int o_LocIDX { get; set; }
    [XmlElement("o_BuildingIDX")]
    public int o_BuildingIDX { get; set; }
    [XmlElement("o_RoomIDX")]
    public int o_RoomIDX { get; set; }

    [XmlElement("subno_flag")]
    public int subno_flag { get; set; }

}

[Serializable]
public class its_TransferData
{
    [XmlElement("u0_transf_idx")]
    public int u0_transf_idx { get; set; }
    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }
    [XmlElement("node_idx")]
    public int node_idx { get; set; }
    [XmlElement("actor_idx")]
    public int actor_idx { get; set; }
}

[Serializable]
public class its_TransferDevice_status_l0
{
    [XmlElement("zdate")]
    public string zdate { get; set; }
    [XmlElement("actor_name")]
    public string actor_name { get; set; }
    [XmlElement("node_name")]
    public string node_name { get; set; }
    [XmlElement("status_name")]
    public string status_name { get; set; }
    [XmlElement("app_remark")]
    public string app_remark { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
}