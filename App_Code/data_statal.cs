﻿using System;
using System.Xml.Serialization;


public class data_statal
{
    public int return_code { get; set; }
    public string return_msg { get; set; }
    public statal_service_m0_ipaddress_action[] statal_service_m0_ipaddress_action { get; set; }
}

public class statal_service_m0_ipaddress_action
{
    public int m0_ipaddress_idx { get; set; }
    public string ipaddress_name { get; set; }
    public int ipaddress_status { get; set; }
}
