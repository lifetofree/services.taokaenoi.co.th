﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;


/// <summary>
/// Summary description for api_softwarelicense_devices
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_softwarelicense_devices : System.Web.Services.WebService
{

    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = "";
    string _mail_subject = "";
    string _mail_body = "";

    string sent_mailsoftware = "";

    string email_empcreate = "";
    int check_emp_sentmail = 0;

    string u0_code_mailsoftware = "";
    string comment_mailsoftware = "";

    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    service_mail _serviceMail = new service_mail();
    data_softwarelicense_devices _data_softwarelicense_devices = new data_softwarelicense_devices();
    


    public api_softwarelicense_devices()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    // Set u0_softwarelicense
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetSoftwareLicense(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_softwarelicense_devices _data_softwarelicense_devices2 = new data_softwarelicense_devices();
            _data_softwarelicense_devices2 = (data_softwarelicense_devices)_funcTool.convertXmlToObject(typeof(data_softwarelicense_devices), _xml_in);


            sent_mailsoftware = HttpUtility.UrlDecode(_data_softwarelicense_devices2.u0_softwarelicense_list[0].software_name_sentemail);

            check_emp_sentmail = _data_softwarelicense_devices2.u0_softwarelicense_list[0].emp_idx;

            email_empcreate = _data_softwarelicense_devices2.u0_softwarelicense_list[0].email;

            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_softwarelicense_devices", "service_softwarelicense_devices", _xml_in, 100); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_softwarelicense_devices = (data_softwarelicense_devices)_funcTool.convertXmlToObject(typeof(data_softwarelicense_devices), _local_xml);

            //_data_softwarelicense_devices.u0_softwarelicense_list[0].software_name_sentemail.ToString();
            //email_empcreate = _data_softwarelicense_devices.u0_softwarelicense_list[0].email;
            if (_data_softwarelicense_devices.return_code == 0)
            {

                if(check_emp_sentmail == 0)
                {

                }
                else
                {
                    // create mail body
                    
                    //string email = "teoy_42@hotmail.com,waraporn.teoy@gmail.com";
                 
                    string email = email_empcreate;

                    //string email = txtemail_holderinsert.Text; เมลผู้ถือครอง
                    ///string replyempcreate = _txtemailcreate.Text;
                    string replyempcreate = "it_support@taokaenoi.co.th";
                    //string link_software = "http://demo.taokaenoi.co.th/software-license";

                    string link_software = "http://mas.taokaenoi.co.th/software-license";


                    u0_softwarelicense_detail _temp_u0 = _data_softwarelicense_devices.u0_softwarelicense_list[0];
                    _mail_subject = "[MIS/Software : Software License ] - แจ้งรายการโปรแกรมลิขสิทธิ์";

                    _mail_body = _serviceMail.softwareLicenseCreate(_data_softwarelicense_devices.u0_softwarelicense_list[0], link_software);

                    //_mail_body = _serviceMail.softwareLicenseCreate(_data_softwarelicense_devices.u0_softwarelicense_list[0], sent_mailsoftware, link_software);

                    //_mail_body = _serviceMail.softwareLicenseCreate(_temp_u0);
                    //_serviceMail.SendHtmlFormattedEmail(_temp_u0.emp_mail_list, _mail_subject, _mail_body);

                    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
                }
               
            }
            else
            {

            }



        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Set u0_softwarelicense
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSoftwareLicense(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_softwarelicense_devices", "service_softwarelicense_devices", _xml_in, 200); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get u0_softwarelicense
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCountLicenseDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_softwarelicense_devices", "service_softwarelicense_devices", _xml_in, 201); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get  Detail u0_softwarelicense where
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSoftwareLicenseView(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_softwarelicense_devices", "service_softwarelicense_devices", _xml_in, 202); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get  Detail Software name u0_softwarelicense where
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSoftwareLicenseName(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_softwarelicense_devices", "service_softwarelicense_devices", _xml_in, 203); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get Log  Detail Software name u0_softwarelicense where
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLogSoftwareLicense(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_softwarelicense_devices", "service_softwarelicense_devices", _xml_in, 204); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Set Approve License 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetApproveLicense(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

                
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_softwarelicense_devices", "service_softwarelicense_devices", _xml_in, 300); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_softwarelicense_devices = (data_softwarelicense_devices)_funcTool.convertXmlToObject(typeof(data_softwarelicense_devices), _local_xml);


            if (_data_softwarelicense_devices.return_code == 0)
            {
                //string tomail = "waraporn.teoy@gmail.com,teoy_42@hotmail.com";
                string tomail = "it_support@taokaenoi.co.th";

                //string replyempcreate = ViewState["emp_email"].ToString();
                //string replyempcreate = "waraporn.teoy@gmail.com";//ViewState["emp_email"].ToString();
                string replyempcreate = "";//ViewState["emp_email"].ToString();
                //string link_approve = "http://demo.taokaenoi.co.th/software-license";

                string link_approve = "http://mas.taokaenoi.co.th/software-license";


                u0_softwarelicense_detail _temp_u0 = _data_softwarelicense_devices.u0_softwarelicense_list[0];
                _mail_subject = "[MIS/Software : Software License] - รับทราบโปรแกรมลิขสิทธิ์";

                //_mail_body = _serviceMail.SoftwareApproveCreate(u0_code_mailsoftware, comment_mailsoftware, link_approve);

                _mail_body = _serviceMail.SoftwareApproveCreate(_data_softwarelicense_devices.u0_softwarelicense_list[0], link_approve);

                //_mail_body = _serviceMail.softwareLicenseCreate(_temp_u0);
                //_serviceMail.SendHtmlFormattedEmail(_temp_u0.emp_mail_list, _mail_subject, _mail_body);

                _serviceMail.SendHtmlFormattedEmailFull(tomail, "", replyempcreate, _mail_subject, _mail_body);
            }
            else
            {

            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // Get search in index
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchIndex(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_softwarelicense_devices", "service_softwarelicense_devices", _xml_in, 205); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get search Report
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchReportSoftware(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_softwarelicense_devices", "service_softwarelicense_devices", _xml_in, 206); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get search Report
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetBindReportSoftware(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_softwarelicense_devices", "service_softwarelicense_devices", _xml_in, 207); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Set hoder devices
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetHolderSoftware(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_softwarelicense_devices", "service_softwarelicense_devices", _xml_in, 301); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // GetSoftwareShowHolder hoder devices
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSoftwareShowHolder(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_softwarelicense_devices", "service_softwarelicense_devices", _xml_in, 208); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailSoftwareProfile(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_softwarelicense_devices", "service_softwarelicense_devices", _xml_in, 209); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }



}
