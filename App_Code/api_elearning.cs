﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.IO;
using System.Net.Mail;

/// <summary>
/// Summary description for api_FeedBack
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_elearning : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = String.Empty;
    string _mail_subject = "";
    string _mail_body = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    service_mail _service_mail = new service_mail();
    data_elearning _dtlerning = new data_elearning();

    public api_elearning()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    //*** start tran ******//

    /****** Start m0_training_req ******/
    // select // GET: odata/el_m0_training_req

    #region  P'Cake
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_u0_training_req(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 120); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_u1_training_req(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 121); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_u1_training_req_Dept(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 122); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_u2_training_req(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 122); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDel_el__training_req(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 123); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_training_req(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_training_req_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 110); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_training_req
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_u0_training_req(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 110); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_u1_training_req(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 111); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_u2_training_req(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 112); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_u3_training_req(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 113); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_training_req
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_u0_training_req(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 130); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_training_req
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_u0_training_req(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 190); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_u1u2_training_req(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 113); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_training_req(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetErrorel_m0_training_req(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 140); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    /****** Start el_u0_course ******/
    // select // GET: odata/el_u0_course
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_u_course(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_u0_course
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_u_course(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // Update // put el_m0_training_req
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_u_course(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 230); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_training_req
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_u_course(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 290); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    /****** Start el_u0_training_plan ******/
    // select // GET: odata/el_u0_training_plan
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_u_plan(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 320); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_u0_training_plan
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_u_plan(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 310); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // Update // put el_u0_training_plan
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_u_plan(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 330); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_u0_training_plan
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_u_plan(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 390); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    /****** Start el_u0_training_plan_course ******/
    // select // GET: odata/el_u0_training_plan_course
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_u_plan_course(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 420); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_u0_training_plan_course
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_u_plan_course(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 410); // return w/ json
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtlerning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

            if (_dtlerning.return_msg.ToString() == "UpdateApprove" || _dtlerning.return_msg.ToString() == "U8")
            {
                var emp_email = _dtlerning.el_training_course_action[0].emp_email.ToString();

                if (emp_email != "" && emp_email != null)
                {
                    _mail_subject = "[TRAINING COURSE] : " + _dtlerning.el_training_course_action[0].training_course_no.ToString() + "-" + _dtlerning.el_training_course_action[0].course_name.ToString();
                    _mail_body = _service_mail.TrainingCreateCoursePlan(_dtlerning.el_training_course_action[0]);
                    _service_mail.SendHtmlFormattedEmailFull("webmaster@taokaenoi.co.th", "", "webmaster@taokaenoi.co.th", _mail_subject, _mail_body);

                }
            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // Update // put el_u0_training_plan_course
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_u_plan_course(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 430); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectListPlan(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 400); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_u0_training_plan_course
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_u_plan_course(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 490); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //start manage

    /****** Start u_manage ******/
    // select // GET: odata/el_u_manage
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_u_manage(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 420); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_u_manage(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_u_manage_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 410); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_u_manage
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_u_manage(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 410); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_u_manage
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_u_manage(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 430); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_u_manage
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_u_manage(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 490); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_u_manage(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetErrorel_u_manage(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_transection", _xml_in, 440); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //end manage


    //**** end tran 

    /****** Start Master Data ******/
    /****** Start m0_training ******/
    // select // GET: odata/el_m0_training
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_training(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 120); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_training(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_training_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 110); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_training
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_training(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 110); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_training
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_training(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 130); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_training
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_training(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 190); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_training(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetErrorel_m0_training(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 140); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    /****** Start m0_training_type ******/
    // select // GET: odata/el_m0_training_type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_training_type(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_training_type(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_training_type_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_training_type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_training_type(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_training_type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_training_type(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 230); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_training
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_training_type(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 290); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    /****** Start m0_training_group ******/
    // select // GET: odata/el_m0_training_group
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_training_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 320); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_training_group(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_training_group_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 310); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_training_group
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_training_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 310); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_training_group
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_training_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 330); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail // GET: odata/el_m0_training_req(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetErrorel_m0_training_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 340); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_training
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_training_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 390); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    /****** Start m0_training_branch ******/
    // select // GET: odata/el_m0_training_branch
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_training_branch(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 420); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_training_branch(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_training_branch_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 410); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_training_branch
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_training_branch(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 410); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_training_branch
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_training_branch(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 430); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_training
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_training_branch(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 490); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    /****** Start m0_training_organ ******/
    // select // GET: odata/el_m0_training_organ
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_training_organ(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 520); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_training_organ(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_training_organ_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 510); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_training_organ
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_training_organ(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 510); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_training_organ
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_training_organ(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 530); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_training_organ
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_training_organ(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 590); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_training_organ(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetErrorel_m0_training_organ(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 540); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    /****** Start m0_email ******/
    // select // GET: odata/el_m0_email
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_email(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 720); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_email(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_email_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 710); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_email
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_email(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 710); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_email
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_email(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 730); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_training
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_email(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 790); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }



    /****** Start el_m0_runno ******/
    // select // GET: odata/el_m0_runno
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_runno(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 920); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    /****** Start m0_sche_trn_day ******/
    // select // GET: odata/el_m0_sche_trn_day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_sche_trn_day(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 620); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_sche_trn_dayL(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 621); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_sche_trn_day(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_sche_trn_day_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 610); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_sche_trn_day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_sche_trn_day(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 610); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_sche_trn_day
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_sche_trn_day(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 630); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_training
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_sche_trn_day(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 690); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select // GET: odata/el_m0_level
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_level(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 820); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_level(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_level_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 810); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_level
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_level(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 810); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_level
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_level(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 830); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_level
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_level(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 890); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_level(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetErrorel_m0_level(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 840); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select // GET: odata/el_m0_evaluation_group
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_evaluation_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1020); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_evaluation_group(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_evaluation_group_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1010); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_evaluation_group
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_evaluation_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1010); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_evaluation_group
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_evaluation_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1030); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_evaluation_group
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_evaluation_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1090); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_evaluation_group(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetErrorel_m0_evaluation_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1040); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // select // GET: odata/el_m0_evaluation_form
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_evaluation_form(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1120); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_evaluation_form(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_evaluation_form_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1110); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_evaluation_form
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_evaluation_form(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1110); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_evaluation_form
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_evaluation_form(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1130); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_evaluation_form
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_evaluation_form(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1190); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_evaluation_form(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetErrorel_m0_evaluation_form(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1140); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    /****** Start m0_target_group ******/
    // select // GET: odata/el_m0_target_group
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_target_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_target_group(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_target_group_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_target_group
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_target_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_target_group
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_target_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1230); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_training
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_target_group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1290); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    /****** Start m0_institution ******/
    // select // GET: odata/el_m0_institution
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_institution(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1320); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_institution(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_institution_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1310); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_institution
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_institution(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1310); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_institution
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_institution(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1330); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_training
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_institution(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1390); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // ****** Start m0_lecturer ******/
    // select // GET: odata/el_m0_lecturer
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_lecturer(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1420); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_lecturer(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_lecturer_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1410); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_lecturer
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_lecturer(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1410); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_lecturer
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_lecturer(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1430); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_training
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_lecturer(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1490); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    /****** Start m0_objective ******/
    // select // GET: odata/el_m0_objective
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_objective(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1520); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_objective(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_objective_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1510); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_objective
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_objective(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1510); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_objective
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_objective(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1530); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_training
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_objective(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1590); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select // GET: odata/el_m0_meeting
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_meeting(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1620); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_meeting(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_meeting_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1610); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_meeting
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_meeting(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1610); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_meeting
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_meeting(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1630); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_meeting
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_meeting(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1690); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_meeting(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetErrorel_m0_meeting(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1640); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // select // GET: odata/el_m0_iso
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_iso(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1720); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_iso(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_m0_iso_Detail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1710); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert // POST: odata/el_m0_iso
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetInsel_m0_iso(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1710); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Update // put el_m0_iso
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetUpdel_m0_iso(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1730); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Delete el_m0_iso
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delel_m0_iso(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1790); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    // select detail // GET: odata/el_m0_iso(5)
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetErrorel_m0_iso(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_master", _xml_in, 1740); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    /****** End Master Data ******/

    /****** Start Report ******/
    // select // GET: Report_plan
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_Report_Plan(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_report", _xml_in, 110); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    /****** End Report ******/


    /****** Start Lookup ******/
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_employee(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 120); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_lu_position(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 121); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getel_lu_TraningAll(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    /****** End Lookup ******/


    //********** start send E-mail **************//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmail_Traning(string jsonIn)
    {
        int id = 0;
        if (jsonIn != null)
        {

            // function_tool _funcTool = new function_tool();
            data_elearning _data_elearning = new data_elearning();
            // function_dmu _func_dmu = new function_dmu();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _xml_in = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
            _xml_in = _funcTool.convertJsonToXml(_xml_in);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _xml_in);
            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    id = item.idx;
                }
            }

            if (id > 0)
            {
                int _tempInt = 0;
                string _mail_subject = "";
                string _mail_body = "";
                string _link = ""; //http ://mas.taokaenoi.co.th/it-purchase
                string emailmove = "";
                string replyempmove = "";
                string _local_xml = "";


                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL";
                obj_trainingLoolup.idx = id;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);


                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    int i = 0;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        if (i == 0)
                        {
                            emailmove = item.email_name;
                        }
                        else
                        {
                            emailmove = emailmove + "," + item.email_name;
                        }
                        i++;
                    }
                }

                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-LIST";
                obj_trainingLoolup.idx = id;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                    data_elearning _data_sentmail = new data_elearning();
                    _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                    traning_req_Xml create_senmail = new traning_req_Xml();

                    create_senmail.training_req_no = obj_trainingLoolup_Xml.doc_no;
                    create_senmail.u0_training_req_date = obj_trainingLoolup_Xml.doc_date;
                    create_senmail.employee_name = obj_trainingLoolup_Xml.employee_name;
                    create_senmail.dept_name_th = obj_trainingLoolup_Xml.dept_name_th;

                    int i = 0;
                    string sName = string.Empty;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        if (i == 0)
                        {
                            sName = item.training_name;
                        }
                        else
                        {
                            sName = sName + " , " + item.training_name;
                        }
                        i++;
                    }

                    create_senmail.training_name = sName;
                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    _mail_subject = "[TRAINING NEEDS]";

                    _mail_body = create_trainingneeds(_data_sentmail.el_traning_req_Xml_action[0], 1, "");

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    replyempmove = "";// "seniordeveloper@taokaenoi.co.th";
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                    _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

                }

            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }
    }

    #region api e-mail create training
    public string create_trainingneeds(traning_req_Xml create_list, int id, string _link)
    {
        string body = string.Empty;
        if (id == 1)
        {
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_needs_survey.html")))
            {
                body = reader.ReadToEnd();
            }
        }
        else if (id == 2)
        {
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_needs_survey_open.html")))
            {
                body = reader.ReadToEnd();
            }
        }


        body = body.Replace("{document_code}", create_list.training_req_no);
        body = body.Replace("{document_date}", create_list.u0_training_req_date);
        body = body.Replace("{name_request}", create_list.employee_name);
        body = body.Replace("{name_department}", create_list.dept_name_th);
        body = body.Replace("{name_training}", create_list.training_name);
        // body = body.Replace("{details_purchase}", create_list.details_purchase);
        body = body.Replace("{link}", _link);

        return body;
    }


    #endregion


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmail_OpenTraning(string jsonIn)
    {
        int id = 0;
        if (jsonIn != null)
        {

            // function_tool _funcTool = new function_tool();
            data_elearning _data_elearning = new data_elearning();
            // function_dmu _func_dmu = new function_dmu();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            int _tempInt = 0;
            string _mail_subject = "";
            string _mail_body = "";
            string _link = "http://mas.taokaenoi.co.th/TrainingNeedsSurvey";
            string emailmove = "";
            string replyempmove = "";
            string _local_xml = "";


            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_trainingLoolup.operation_status_id = "E-MAIL-OPEN";
            _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
            jsonIn = _funcTool.convertObjectToJson(_data_elearning);
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);


            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                int i = 0;
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    if (i == 0)
                    {
                        emailmove = item.email_name;
                    }
                    else
                    {
                        emailmove = emailmove + "," + item.email_name;
                    }
                    i++;
                }
            }

            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_trainingLoolup.operation_status_id = "SEND_MAIL_OPEN_TRN";
            obj_trainingLoolup.idx = id;
            _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
            jsonIn = _funcTool.convertObjectToJson(_data_elearning);
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                data_elearning _data_sentmail = new data_elearning();
                _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                traning_req_Xml create_senmail = new traning_req_Xml();

                create_senmail.training_req_no = "";//obj_trainingLoolup_Xml.zFullName;
                create_senmail.u0_training_req_date = "";// obj_trainingLoolup_Xml.doc_date;
                create_senmail.employee_name = "";//obj_trainingLoolup_Xml.employee_name;
                create_senmail.dept_name_th = "";//obj_trainingLoolup_Xml.dept_name_th;

                int i = 0;
                string sName = string.Empty;
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    if (i == 0)
                    {
                        sName = item.zFullName;
                    }
                    else
                    {
                        sName = sName + " , " + item.zFullName;
                    }
                    i++;
                }

                create_senmail.training_req_no = sName;
                _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                _mail_subject = "[TRAINING NEEDS]";

                _mail_body = create_trainingneeds(_data_sentmail.el_traning_req_Xml_action[0], 2, _link);

                _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                replyempmove = "seniordeveloper@taokaenoi.co.th";
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }

    //****** START HR TO HRD *****//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmail_Plan_hrtohrd(string jsonIn)
    {
        int id = 0;
        if (jsonIn != null)
        {
            data_elearning _data_elearning = new data_elearning();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _xml_in = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
            _xml_in = _funcTool.convertJsonToXml(_xml_in);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _xml_in);
            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    id = item.idx;
                }
            }

            if (id > 0)
            {
                int _tempInt = 0;
                string _mail_subject = "";
                string _mail_body = "";
                string _link = "";
                string emailmove = "";
                string replyempmove = "";
                string _local_xml = "";
                string spos_name_th = "", semp_name_th = "";

                // ËÒ E-Mail ·Õè¨ÐÊè§
                //_data_elearning.trainingLoolup_action = new trainingLoolup[1];
                //obj_trainingLoolup.operation_status_id = "E-MAIL-HR-TO-HRD";
                //_data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                //jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                //_xml_in = _funcTool.convertJsonToXml(jsonIn);
                //_ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                //// check return value then send mail
                //_local_xml = _funcTool.convertJsonToXml(_ret_val);
                //_data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);


                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    int i = 0;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        spos_name_th = item.pos_name_th;
                        semp_name_th = item.emp_name_th;
                        if (i == 0)
                        {
                            emailmove = item.emp_email;
                        }
                        else
                        {
                            emailmove = emailmove + "," + item.emp_email;
                        }
                        i++;
                    }
                }

                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-HR-TO-HRD-DATA";
                obj_trainingLoolup.idx = id;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                    data_elearning _data_sentmail = new data_elearning();
                    _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                    traning_req_Xml create_senmail = new traning_req_Xml();

                    create_senmail.zyear = obj_trainingLoolup_Xml.zyear;
                    create_senmail.doc_no = obj_trainingLoolup_Xml.doc_no;
                    create_senmail.doc_date = obj_trainingLoolup_Xml.doc_date;
                    create_senmail.employee_name = semp_name_th;
                    create_senmail.pos_name_th = spos_name_th;
                    create_senmail.course_name = obj_trainingLoolup_Xml.course_name;
                    create_senmail.priority_name = obj_trainingLoolup_Xml.priority_name;
                    create_senmail.priority_remark = obj_trainingLoolup_Xml.priority_remark;
                    create_senmail.training_group_name = obj_trainingLoolup_Xml.training_group_name;
                    create_senmail.training_branch_name = obj_trainingLoolup_Xml.training_branch_name;

                    int i = 0;
                    string sName = string.Empty;
                    //foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    //{
                    //    if (i == 0)
                    //    {
                    //        sName = item.course_name;
                    //    }
                    //    else
                    //    {
                    //        sName = sName + " , " + item.course_name;
                    //    }
                    //    i++;
                    //}

                    //create_senmail.training_name = sName;
                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    _mail_subject = "[TRAINING PLAN]";

                    _mail_body = create_trainingplan_hrtohrd(_data_sentmail.el_traning_req_Xml_action[0], 1, "");

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    replyempmove = "seniordeveloper@taokaenoi.co.th";
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                    _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

                }

            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }
    public string create_trainingplan_hrtohrd(traning_req_Xml create_list, int id, string _link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_plan_hrtohrd.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{FullNameTH_HRD}", create_list.pos_name_th);
        body = body.Replace("{doc_no}", create_list.doc_no);
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{zyear}", create_list.zyear.ToString());
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{training_group_name}", create_list.training_group_name);
        body = body.Replace("{training_branch_name}", create_list.training_branch_name);
        body = body.Replace("{priority_name}", create_list.priority_name);
        body = body.Replace("{priority_remark}", create_list.priority_remark);

        return body;
    }
    //****** END HR TO HRD *****//

    //****** START HRD TO MD *****//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmail_Plan_hrdtomd(string jsonIn)
    {
        int id = 0;
        if (jsonIn != null)
        {
            data_elearning _data_elearning = new data_elearning();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _xml_in = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
            _xml_in = _funcTool.convertJsonToXml(_xml_in);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _xml_in);
            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    id = item.idx;
                }
            }

            if (id > 0)
            {
                int _tempInt = 0;
                string _mail_subject = "";
                string _mail_body = "";
                string _link = "";
                string emailmove = "";
                string replyempmove = "";
                string _local_xml = "";
                string spos_name_th = "", semp_name_th = "";

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    int i = 0;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        spos_name_th = item.pos_name_th;
                        semp_name_th = item.emp_name_th;
                        if (i == 0)
                        {
                            emailmove = item.emp_email;
                        }
                        else
                        {
                            emailmove = emailmove + "," + item.emp_email;
                        }
                        i++;
                    }
                }

                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-HRD-TO-MD-DATA";
                obj_trainingLoolup.idx = id;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                    data_elearning _data_sentmail = new data_elearning();
                    _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                    traning_req_Xml create_senmail = new traning_req_Xml();

                    create_senmail.zyear = obj_trainingLoolup_Xml.zyear;
                    create_senmail.doc_no = obj_trainingLoolup_Xml.doc_no;
                    create_senmail.doc_date = obj_trainingLoolup_Xml.doc_date;
                    create_senmail.employee_name = semp_name_th;
                    create_senmail.pos_name_th = spos_name_th;
                    create_senmail.course_name = obj_trainingLoolup_Xml.course_name;
                    create_senmail.priority_name = obj_trainingLoolup_Xml.priority_name;
                    create_senmail.priority_remark = obj_trainingLoolup_Xml.priority_remark;
                    create_senmail.training_group_name = obj_trainingLoolup_Xml.training_group_name;
                    create_senmail.training_branch_name = obj_trainingLoolup_Xml.training_branch_name;

                    int i = 0;
                    string sName = string.Empty;
                    //foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    //{
                    //    if (i == 0)
                    //    {
                    //        sName = item.course_name;
                    //    }
                    //    else
                    //    {
                    //        sName = sName + " , " + item.course_name;
                    //    }
                    //    i++;
                    //}

                    //create_senmail.training_name = sName;
                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    _mail_subject = "[TRAINING PLAN]";

                    _mail_body = create_trainingplan_hrdtomd(_data_sentmail.el_traning_req_Xml_action[0], 1, "");

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    replyempmove = "seniordeveloper@taokaenoi.co.th";
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                    _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

                }

            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }
    public string create_trainingplan_hrdtomd(traning_req_Xml create_list, int id, string _link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_plan_hrdtomd.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{FullNameTH_HRD}", create_list.pos_name_th);
        body = body.Replace("{doc_no}", create_list.doc_no);
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{zyear}", create_list.zyear.ToString());
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{training_group_name}", create_list.training_group_name);
        body = body.Replace("{training_branch_name}", create_list.training_branch_name);
        body = body.Replace("{priority_name}", create_list.priority_name);
        body = body.Replace("{priority_remark}", create_list.priority_remark);

        return body;
    }
    //****** END HRD TO MD *****//

    //****** START HRD TO  HR ALL *****//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmail_Plan_hrdtohr_all(string jsonIn)
    {
        int id = 0;
        if (jsonIn != null)
        {
            data_elearning _data_elearning = new data_elearning();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _xml_in = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
            _xml_in = _funcTool.convertJsonToXml(_xml_in);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _xml_in);
            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    id = item.idx;
                }
            }

            if (id > 0)
            {
                int _tempInt = 0;
                string _mail_subject = "";
                string _mail_body = "";
                string _link = "";
                string emailmove = "";
                string replyempmove = "";
                string _local_xml = "";
                string spos_name_th = "", semp_name_th = "";

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    int i = 0;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        spos_name_th = item.pos_name_th;
                        semp_name_th = item.emp_name_th;
                        if (i == 0)
                        {
                            emailmove = item.emp_email;
                        }
                        else
                        {
                            emailmove = emailmove + "," + item.emp_email;
                        }
                        i++;
                    }
                }

                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-HRD-TO-MD-DATA";
                obj_trainingLoolup.idx = id;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                    data_elearning _data_sentmail = new data_elearning();
                    _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                    traning_req_Xml create_senmail = new traning_req_Xml();

                    create_senmail.zyear = obj_trainingLoolup_Xml.zyear;
                    create_senmail.doc_no = obj_trainingLoolup_Xml.doc_no;
                    create_senmail.doc_date = obj_trainingLoolup_Xml.doc_date;
                    create_senmail.employee_name = semp_name_th;
                    create_senmail.pos_name_th = spos_name_th;
                    create_senmail.course_name = obj_trainingLoolup_Xml.course_name;
                    create_senmail.priority_name = obj_trainingLoolup_Xml.priority_name;
                    create_senmail.priority_remark = obj_trainingLoolup_Xml.priority_remark;
                    create_senmail.training_group_name = obj_trainingLoolup_Xml.training_group_name;
                    create_senmail.training_branch_name = obj_trainingLoolup_Xml.training_branch_name;
                    create_senmail.approve_mode_name = getTextDoc(
                                                       obj_trainingLoolup_Xml.approve_status,
                                                       obj_trainingLoolup_Xml.md_approve_status,
                                                       obj_trainingLoolup_Xml.decision_name,
                                                       obj_trainingLoolup_Xml.md_decision_name,
                                                       "HR"
                                                       );

                    int i = 0;
                    string sName = string.Empty;

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    _mail_subject = "[TRAINING PLAN]";

                    _mail_body = create_trainingplan_hrdtohr_all(_data_sentmail.el_traning_req_Xml_action[0], 1, "");

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    replyempmove = "seniordeveloper@taokaenoi.co.th";
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                    _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

                }

            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }
    public string create_trainingplan_hrdtohr_all(traning_req_Xml create_list, int id, string _link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_plan_hrdtohr_all.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{FullNameTH_HRD}", create_list.pos_name_th);
        body = body.Replace("{doc_no}", create_list.doc_no);
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{zyear}", create_list.zyear.ToString());
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{training_group_name}", create_list.training_group_name);
        body = body.Replace("{training_branch_name}", create_list.training_branch_name);
        body = body.Replace("{priority_name}", create_list.priority_name);
        body = body.Replace("{priority_remark}", create_list.priority_remark);
        body = body.Replace("{approve_mode_name}", create_list.approve_mode_name);

        return body;
    }
    //****** END HRD TO HR ALL *****//

    //****** START MD TO  HRD ALL *****//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmail_Plan_mdtohrd_all(string jsonIn)
    {
        int id = 0;
        if (jsonIn != null)
        {
            data_elearning _data_elearning = new data_elearning();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _xml_in = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
            _xml_in = _funcTool.convertJsonToXml(_xml_in);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _xml_in);
            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    id = item.idx;
                }
            }

            if (id > 0)
            {
                int _tempInt = 0;
                string _mail_subject = "";
                string _mail_body = "";
                string _link = "";
                string emailmove = "";
                string replyempmove = "";
                string _local_xml = "";
                string spos_name_th = "", semp_name_th = "";

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    int i = 0;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        spos_name_th = item.pos_name_th;
                        semp_name_th = item.emp_name_th;
                        if (i == 0)
                        {
                            emailmove = item.emp_email;
                        }
                        else
                        {
                            emailmove = emailmove + "," + item.emp_email;
                        }
                        i++;
                    }
                }

                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-HRD-TO-MD-DATA";
                obj_trainingLoolup.idx = id;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                    data_elearning _data_sentmail = new data_elearning();
                    _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                    traning_req_Xml create_senmail = new traning_req_Xml();

                    create_senmail.zyear = obj_trainingLoolup_Xml.zyear;
                    create_senmail.doc_no = obj_trainingLoolup_Xml.doc_no;
                    create_senmail.doc_date = obj_trainingLoolup_Xml.doc_date;
                    create_senmail.employee_name = semp_name_th;
                    create_senmail.pos_name_th = spos_name_th;
                    create_senmail.course_name = obj_trainingLoolup_Xml.course_name;
                    create_senmail.priority_name = obj_trainingLoolup_Xml.priority_name;
                    create_senmail.priority_remark = obj_trainingLoolup_Xml.priority_remark;
                    create_senmail.training_group_name = obj_trainingLoolup_Xml.training_group_name;
                    create_senmail.training_branch_name = obj_trainingLoolup_Xml.training_branch_name;
                    create_senmail.approve_mode_name = getTextDoc(
                                                       obj_trainingLoolup_Xml.approve_status,
                                                       obj_trainingLoolup_Xml.md_approve_status,
                                                       obj_trainingLoolup_Xml.decision_name,
                                                       obj_trainingLoolup_Xml.md_decision_name,
                                                       "MD"
                                                       );

                    int i = 0;
                    string sName = string.Empty;
                    //foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    //{
                    //    if (i == 0)
                    //    {
                    //        sName = item.course_name;
                    //    }
                    //    else
                    //    {
                    //        sName = sName + " , " + item.course_name;
                    //    }
                    //    i++;
                    //}

                    //create_senmail.training_name = sName;
                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    _mail_subject = "[TRAINING PLAN]";

                    _mail_body = create_trainingplan_mdtohrd_all(_data_sentmail.el_traning_req_Xml_action[0], 1, "");

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    replyempmove = "seniordeveloper@taokaenoi.co.th";
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                    _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

                }

            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }
    public string create_trainingplan_mdtohrd_all(traning_req_Xml create_list, int id, string _link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_plan_mdtohrd_all.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{FullNameTH_HRD}", create_list.pos_name_th);
        body = body.Replace("{doc_no}", create_list.doc_no);
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{zyear}", create_list.zyear.ToString());
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{training_group_name}", create_list.training_group_name);
        body = body.Replace("{training_branch_name}", create_list.training_branch_name);
        body = body.Replace("{priority_name}", create_list.priority_name);
        body = body.Replace("{priority_remark}", create_list.priority_remark);
        body = body.Replace("{approve_mode_name}", create_list.approve_mode_name);

        return body;
    }
    //****** END MD TO HRD ALL *****//

    //****** START TRN-COURSE HR TO HRD *****//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmail_plan_course_hrtohrd(string jsonIn)
    {
        int id = 0;
        if (jsonIn != null)
        {
            data_elearning _data_elearning = new data_elearning();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _xml_in = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
            _xml_in = _funcTool.convertJsonToXml(_xml_in);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _xml_in);
            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    id = item.idx;
                }
            }

            if (id > 0)
            {
                int _tempInt = 0;
                string _mail_subject = "";
                string _mail_body = "";
                string _link = "";
                string emailmove = "";
                string replyempmove = "";
                string _local_xml = "";
                string spos_name_th = "", semp_name_th = "";

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    int i = 0;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        spos_name_th = item.pos_name_th;
                        semp_name_th = item.emp_name_th;
                        if (i == 0)
                        {
                            emailmove = item.emp_email;
                        }
                        else
                        {
                            emailmove = emailmove + "," + item.emp_email;
                        }
                        i++;
                    }
                }

                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-HR-TO-HRD-DATA-COURSE";
                obj_trainingLoolup.idx = id;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);

                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                    data_elearning _data_sentmail = new data_elearning();
                    _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                    traning_req_Xml create_senmail = new traning_req_Xml();

                    create_senmail.zyear = obj_trainingLoolup_Xml.zyear;
                    create_senmail.doc_no = obj_trainingLoolup_Xml.doc_no;
                    create_senmail.doc_date = obj_trainingLoolup_Xml.doc_date;
                    create_senmail.employee_name = semp_name_th;
                    create_senmail.pos_name_th = spos_name_th;
                    create_senmail.course_name = obj_trainingLoolup_Xml.course_name;
                    create_senmail.priority_name = obj_trainingLoolup_Xml.priority_name;
                    create_senmail.priority_remark = obj_trainingLoolup_Xml.priority_remark;
                    create_senmail.training_group_name = obj_trainingLoolup_Xml.training_group_name;
                    create_senmail.training_branch_name = obj_trainingLoolup_Xml.training_branch_name;
                    //create_senmail.zdate_start = obj_trainingLoolup_Xml.zdate_start+" "+ obj_trainingLoolup_Xml.ztime_start;
                    //create_senmail.zdate_end = obj_trainingLoolup_Xml.zdate_end + " " + obj_trainingLoolup_Xml.ztime_end;

                    create_senmail.zplace_name = obj_trainingLoolup_Xml.zplace_name;

                    _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                    obj_trainingLoolup.operation_status_id = "TRN-PLAN-COURSE-U8";
                    obj_trainingLoolup.idx = id;
                    _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                    jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                    _xml_in = _funcTool.convertJsonToXml(jsonIn);
                    _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                    _local_xml = _funcTool.convertJsonToXml(_ret_val);
                    _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                    if (_data_elearning.trainingLoolup_Xml_action != null)
                    {
                        int icount = 0;

                        foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                        {
                            icount++;
                            if (icount == 1)
                            {
                                create_senmail.zdate_start = item.zdate_full;
                            }
                            else
                            {
                                create_senmail.zdate_start = create_senmail.zdate_start + " , " + item.zdate_full;
                            }
                        }
                    }

                    int i = 0;
                    string sName = string.Empty;

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    _mail_subject = "[TRAINING COURSE]";

                    _mail_body = create_trainingplan_course_hrtohrd(_data_sentmail.el_traning_req_Xml_action[0], 1, "");

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    replyempmove = "seniordeveloper@taokaenoi.co.th";
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                    _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

                }

            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }
    public string create_trainingplan_course_hrtohrd(traning_req_Xml create_list, int id, string _link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_plan_course_hrtohrd.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{FullNameTH_HRD}", create_list.pos_name_th);
        body = body.Replace("{doc_no}", create_list.doc_no);
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{zyear}", create_list.zyear.ToString());
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{training_group_name}", create_list.training_group_name);
        body = body.Replace("{training_branch_name}", create_list.training_branch_name);
        body = body.Replace("{priority_name}", create_list.priority_name);
        body = body.Replace("{priority_remark}", create_list.priority_remark);
        body = body.Replace("{place_name}", create_list.zplace_name);
        body = body.Replace("{date_start}", create_list.zdate_start);
        body = body.Replace("{date_end}", create_list.zdate_end);

        return body;
    }
    //****** END TRN-COURSE HR TO HRD *****//

    //****** START TRN-COURSE HRD TO MD *****//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmail_plan_course_hrdtomd(string jsonIn)
    {
        int id = 0;
        if (jsonIn != null)
        {
            data_elearning _data_elearning = new data_elearning();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _xml_in = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
            _xml_in = _funcTool.convertJsonToXml(_xml_in);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _xml_in);
            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    id = item.idx;
                }
            }

            if (id > 0)
            {
                int _tempInt = 0;
                string _mail_subject = "";
                string _mail_body = "";
                string _link = "";
                string emailmove = "";
                string replyempmove = "";
                string _local_xml = "";
                string spos_name_th = "", semp_name_th = "";

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    int i = 0;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        spos_name_th = item.pos_name_th;
                        semp_name_th = item.emp_name_th;
                        if (i == 0)
                        {
                            emailmove = item.emp_email;
                        }
                        else
                        {
                            emailmove = emailmove + "," + item.emp_email;
                        }
                        i++;
                    }
                }

                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-HRD-TO-MD-DATA-COURSE";
                obj_trainingLoolup.idx = id;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                    data_elearning _data_sentmail = new data_elearning();
                    _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                    traning_req_Xml create_senmail = new traning_req_Xml();

                    create_senmail.zyear = obj_trainingLoolup_Xml.zyear;
                    create_senmail.doc_no = obj_trainingLoolup_Xml.doc_no;
                    create_senmail.doc_date = obj_trainingLoolup_Xml.doc_date;
                    create_senmail.employee_name = semp_name_th;
                    create_senmail.pos_name_th = spos_name_th;
                    create_senmail.course_name = obj_trainingLoolup_Xml.course_name;
                    create_senmail.priority_name = obj_trainingLoolup_Xml.priority_name;
                    create_senmail.priority_remark = obj_trainingLoolup_Xml.priority_remark;
                    create_senmail.training_group_name = obj_trainingLoolup_Xml.training_group_name;
                    create_senmail.training_branch_name = obj_trainingLoolup_Xml.training_branch_name;
                    //create_senmail.zdate_start = obj_trainingLoolup_Xml.zdate_start + " " + obj_trainingLoolup_Xml.ztime_start;
                    //create_senmail.zdate_end = obj_trainingLoolup_Xml.zdate_end + " " + obj_trainingLoolup_Xml.ztime_end;

                    create_senmail.zplace_name = obj_trainingLoolup_Xml.zplace_name;
                    _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                    obj_trainingLoolup.operation_status_id = "TRN-PLAN-COURSE-U8";
                    obj_trainingLoolup.idx = id;
                    _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                    jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                    _xml_in = _funcTool.convertJsonToXml(jsonIn);
                    _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                    _local_xml = _funcTool.convertJsonToXml(_ret_val);
                    _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                    if (_data_elearning.trainingLoolup_Xml_action != null)
                    {
                        int icount = 0;

                        foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                        {
                            icount++;
                            if (icount == 1)
                            {
                                create_senmail.zdate_start = item.zdate_full;
                            }
                            else
                            {
                                create_senmail.zdate_start = create_senmail.zdate_start + " , " + item.zdate_full;
                            }
                        }
                    }

                    int i = 0;
                    string sName = string.Empty;

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    _mail_subject = "[TRAINING COURSE]";

                    _mail_body = create_trainingplan_course_hrdtomd(_data_sentmail.el_traning_req_Xml_action[0], 1, "");

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    replyempmove = "seniordeveloper@taokaenoi.co.th";
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                    _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

                }

            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }
    public string create_trainingplan_course_hrdtomd(traning_req_Xml create_list, int id, string _link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_plan_course_hrdtomd.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{FullNameTH_HRD}", create_list.pos_name_th);
        body = body.Replace("{doc_no}", create_list.doc_no);
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{zyear}", create_list.zyear.ToString());
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{training_group_name}", create_list.training_group_name);
        body = body.Replace("{training_branch_name}", create_list.training_branch_name);
        body = body.Replace("{priority_name}", create_list.priority_name);
        body = body.Replace("{priority_remark}", create_list.priority_remark);
        body = body.Replace("{place_name}", create_list.zplace_name);
        body = body.Replace("{date_start}", create_list.zdate_start);
        body = body.Replace("{date_end}", create_list.zdate_end);

        return body;
    }
    //****** END TRN-COURSE HRD TO MD *****//

    //****** START TRN-COURSE MD TO  HR *****//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmail_plan_course_mdtohrd_all(string jsonIn)
    {
        int id = 0;
        if (jsonIn != null)
        {
            data_elearning _data_elearning = new data_elearning();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _xml_in = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
            _xml_in = _funcTool.convertJsonToXml(_xml_in);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _xml_in);
            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    id = item.idx;
                }
            }

            if (id > 0)
            {
                int _tempInt = 0, imd_approve_status = 0;
                string _mail_subject = "";
                string _mail_body = "";
                string _link = "http://mas.taokaenoi.co.th/trainingcourse-register";
                string emailmove = "";
                string replyempmove = "";
                string _local_xml = "";
                string spos_name_th = "", semp_name_th = "";

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    int i = 0;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        spos_name_th = item.pos_name_th;
                        semp_name_th = item.emp_name_th;
                        if (i == 0)
                        {
                            emailmove = item.emp_email;
                        }
                        else
                        {
                            emailmove = emailmove + "," + item.emp_email;
                        }
                        i++;
                    }
                }

                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-HRD-TO-MD-DATA-COURSE";
                obj_trainingLoolup.idx = id;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                    data_elearning _data_sentmail = new data_elearning();
                    _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                    traning_req_Xml create_senmail = new traning_req_Xml();

                    create_senmail.zyear = obj_trainingLoolup_Xml.zyear;
                    create_senmail.doc_no = obj_trainingLoolup_Xml.doc_no;
                    create_senmail.doc_date = obj_trainingLoolup_Xml.doc_date;
                    create_senmail.employee_name = semp_name_th;
                    create_senmail.pos_name_th = spos_name_th;
                    create_senmail.course_name = obj_trainingLoolup_Xml.course_name;
                    create_senmail.priority_name = obj_trainingLoolup_Xml.priority_name;
                    create_senmail.priority_remark = obj_trainingLoolup_Xml.priority_remark;
                    create_senmail.training_group_name = obj_trainingLoolup_Xml.training_group_name;
                    create_senmail.training_branch_name = obj_trainingLoolup_Xml.training_branch_name;
                    //create_senmail.zdate_start = obj_trainingLoolup_Xml.zdate_start + " " + obj_trainingLoolup_Xml.ztime_start;
                    //create_senmail.zdate_end = obj_trainingLoolup_Xml.zdate_end + " " + obj_trainingLoolup_Xml.ztime_end;

                    create_senmail.zplace_name = obj_trainingLoolup_Xml.zplace_name;
                    create_senmail.link = _link;
                    imd_approve_status = obj_trainingLoolup_Xml.md_approve_status;
                    create_senmail.approve_mode_name = getTextDoc(
                                                       obj_trainingLoolup_Xml.approve_status,
                                                       obj_trainingLoolup_Xml.md_approve_status,
                                                       obj_trainingLoolup_Xml.decision_name,
                                                       obj_trainingLoolup_Xml.md_decision_name,
                                                       "MD"
                                                       );
                    _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                    obj_trainingLoolup.operation_status_id = "TRN-PLAN-COURSE-U8";
                    obj_trainingLoolup.idx = id;
                    _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                    jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                    _xml_in = _funcTool.convertJsonToXml(jsonIn);
                    _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                    _local_xml = _funcTool.convertJsonToXml(_ret_val);
                    _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                    if (_data_elearning.trainingLoolup_Xml_action != null)
                    {
                        int icount = 0;

                        foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                        {
                            icount++;
                            if (icount == 1)
                            {
                                create_senmail.zdate_start = item.zdate_full;
                            }
                            else
                            {
                                create_senmail.zdate_start = create_senmail.zdate_start + " , " + item.zdate_full;
                            }
                        }
                    }

                    int i = 0;
                    string sName = string.Empty;

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    _mail_subject = "[TRAINING COURSE]";

                    _mail_body = create_trainingplan_course_mdtohrd_all(_data_sentmail.el_traning_req_Xml_action[0], imd_approve_status, "");

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    replyempmove = "seniordeveloper@taokaenoi.co.th";
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                    _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

                }

            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }
    public string create_trainingplan_course_mdtohrd_all(traning_req_Xml create_list, int id, string _link)
    {
        string body = string.Empty;
        if (id == 4)
        {
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_plan_course_mdtoemp_all.html")))
            {
                body = reader.ReadToEnd();
            }
        }
        else
        {
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_plan_course_mdtohrd_all.html")))
            {
                body = reader.ReadToEnd();
            }
        }


        body = body.Replace("{FullNameTH_HRD}", create_list.pos_name_th);
        body = body.Replace("{doc_no}", create_list.doc_no);
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{zyear}", create_list.zyear.ToString());
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{training_group_name}", create_list.training_group_name);
        body = body.Replace("{training_branch_name}", create_list.training_branch_name);
        body = body.Replace("{priority_name}", create_list.priority_name);
        body = body.Replace("{priority_remark}", create_list.priority_remark);
        body = body.Replace("{place_name}", create_list.zplace_name);
        body = body.Replace("{date_start}", create_list.zdate_start);
        body = body.Replace("{date_end}", create_list.zdate_end);
        body = body.Replace("{approve_mode_name}", create_list.approve_mode_name);
        body = body.Replace("{link}", create_list.link);

        return body;
    }
    //****** END TRN-COURSE MD TO HR *****//



    public string getTextDoc(int hr_approve,
                             int md_approve,
                             string hr_decision_name,
                             string md_decision_name,
                             string sMode
        )
    {
        string text = string.Empty;
        int id = 0;
        if (sMode == "MD")
        {
            if (md_approve == 4)
            {
                text = "<span style='color:#26A65B;'>" + md_decision_name + "</span>";
            }
            else
            {
                id = md_approve;
                if (id == 5) // 5   ¡ÅÑºä»á¡éä¢
                {
                    text = "<span style='color:#F89406;'>" + md_decision_name + "</span>";
                }
                else
                {
                    text = "<span style='color:#F03434;'>" + md_decision_name + "</span>";
                }
            }

        }
        else
        {
            if (hr_approve == 4)
            {
                text = "<span style='color:#26A65B;'>" + hr_decision_name + "</span>";
            }
            else
            {
                id = hr_approve;
                if (id == 5) // 5   ¡ÅÑºä»á¡éä¢
                {
                    text = "<span style='color:#F89406;'>" + hr_decision_name + "</span>";
                }
                else
                {
                    text = "<span style='color:#F03434;'>" + hr_decision_name + "</span>";
                }
            }

        }

        return text;
    }
    //****** START TRN-COURSE user TO  header *****//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmail_traningregister(string jsonIn)
    {
        int id = 0, iEmpIDX = 0;
        if (jsonIn != null)
        {

            // function_tool _funcTool = new function_tool();
            data_elearning _data_elearning = new data_elearning();
            // function_dmu _func_dmu = new function_dmu();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _xml_in = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
            _xml_in = _funcTool.convertJsonToXml(_xml_in);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _xml_in);
            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    id = item.idx;
                    iEmpIDX = item.EmpIDX;
                }
            }

            if (id > 0)
            {
                int _tempInt = 0;
                string _mail_subject = "";
                string _mail_body = "";
                string _link = "http://demo.taokaenoi.co.th/trainingcourse-register";
                string emailmove = "";
                string replyempmove = "";
                string _local_xml = "";


                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-RESULTS";
                obj_trainingLoolup.idx = id;
                obj_trainingLoolup.EmpIDX = iEmpIDX;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);


                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    int i = 0;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        if (i == 0)
                        {
                            emailmove = item.email_name;
                        }
                        else
                        {
                            emailmove = emailmove + "," + item.email_name;
                        }
                        i++;
                    }
                }

                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-LIST-RESULTS";
                obj_trainingLoolup.idx = id;
                obj_trainingLoolup.EmpIDX = iEmpIDX;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                    data_elearning _data_sentmail = new data_elearning();
                    _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                    traning_req_Xml create_senmail = new traning_req_Xml();

                    create_senmail.training_req_no = obj_trainingLoolup_Xml.doc_no;
                    create_senmail.u0_training_req_date = obj_trainingLoolup_Xml.doc_date;
                    create_senmail.employee_name = obj_trainingLoolup_Xml.employee_name;
                    create_senmail.dept_name_th = obj_trainingLoolup_Xml.dept_name_th;
                    create_senmail.SecNameTH = obj_trainingLoolup_Xml.SecNameTH;

                    int i = 0;
                    string sName = string.Empty;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        if (i == 0)
                        {
                            sName = item.training_name;
                        }
                        else
                        {
                            sName = sName + " , " + item.training_name;
                        }
                        i++;
                    }

                    create_senmail.training_name = sName;
                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    _mail_subject = "[TRAINING RESULTS]";

                    _mail_body = create_trainingregister(_data_sentmail.el_traning_req_Xml_action[0], 1, _link);

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    replyempmove = "seniordeveloper@taokaenoi.co.th";
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                    _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

                }

            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }
    }

    public string create_trainingregister(traning_req_Xml create_list, int id, string _link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_register_rsults.html")))
        {
            body = reader.ReadToEnd();
        }

        body = body.Replace("{document_code}", create_list.training_req_no);
        body = body.Replace("{document_date}", create_list.u0_training_req_date);
        body = body.Replace("{name_request}", create_list.employee_name);
        body = body.Replace("{name_department}", create_list.dept_name_th);
        body = body.Replace("{name_training}", create_list.training_name);
        body = body.Replace("{name_sec}", create_list.SecNameTH);
        body = body.Replace("{link}", _link);

        return body;
    }
    //****** end TRN-COURSE user TO  header *****//

    //****** START TRN-COURSE OUT PLAN USER TO LEADER *****//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmail_outplan_course_usertoleader(string jsonIn)
    {
        int id = 0;
        if (jsonIn != null)
        {
            data_elearning _data_elearning = new data_elearning();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _xml_in = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
            _xml_in = _funcTool.convertJsonToXml(_xml_in);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _xml_in);
            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    id = item.idx;
                }
            }

            if (id > 0)
            {
                int _tempInt = 0;
                string _mail_subject = "";
                string _mail_body = "";
                string _link = "";
                string emailmove = "";
                string replyempmove = "";
                string _local_xml = "";
                string spos_name_th = "", semp_name_th = "";

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    int i = 0;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        spos_name_th = item.pos_name_th;
                        semp_name_th = item.emp_name_th;
                        if (i == 0)
                        {
                            emailmove = item.emp_email;
                        }
                        else
                        {
                            emailmove = emailmove + "," + item.emp_email;
                        }
                        i++;
                    }
                }

                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-USER-TO-LEADER-DATA-COURSE-OP";
                obj_trainingLoolup.idx = id;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                    data_elearning _data_sentmail = new data_elearning();
                    _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                    traning_req_Xml create_senmail = new traning_req_Xml();

                    create_senmail.zyear = obj_trainingLoolup_Xml.zyear;
                    create_senmail.doc_no = obj_trainingLoolup_Xml.doc_no;
                    create_senmail.doc_date = obj_trainingLoolup_Xml.doc_date;
                    create_senmail.employee_name = semp_name_th;
                    create_senmail.pos_name_th = spos_name_th;
                    create_senmail.course_name = obj_trainingLoolup_Xml.course_name;
                    create_senmail.priority_name = obj_trainingLoolup_Xml.priority_name;
                    create_senmail.priority_remark = obj_trainingLoolup_Xml.priority_remark;
                    create_senmail.training_group_name = obj_trainingLoolup_Xml.training_group_name;
                    create_senmail.training_branch_name = obj_trainingLoolup_Xml.training_branch_name;
                    //create_senmail.zdate_start = obj_trainingLoolup_Xml.zdate_start + " " + obj_trainingLoolup_Xml.ztime_start;
                    //create_senmail.zdate_end = obj_trainingLoolup_Xml.zdate_end + " " + obj_trainingLoolup_Xml.ztime_end;

                    create_senmail.zplace_name = obj_trainingLoolup_Xml.zplace_name;
                    _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                    obj_trainingLoolup.operation_status_id = "TRN-PLAN-COURSE-U8";
                    obj_trainingLoolup.idx = id;
                    _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                    jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                    _xml_in = _funcTool.convertJsonToXml(jsonIn);
                    _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                    _local_xml = _funcTool.convertJsonToXml(_ret_val);
                    _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                    if (_data_elearning.trainingLoolup_Xml_action != null)
                    {
                        int icount = 0;

                        foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                        {
                            icount++;
                            if (icount == 1)
                            {
                                create_senmail.zdate_start = item.zdate_full;
                            }
                            else
                            {
                                create_senmail.zdate_start = create_senmail.zdate_start + " , " + item.zdate_full;
                            }
                        }
                    }

                    int i = 0;
                    string sName = string.Empty;

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    _mail_subject = "[TRAINING COURSE]";

                    _mail_body = create_trainingoutplan_course_usertoleader(_data_sentmail.el_traning_req_Xml_action[0], 1, "");

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    replyempmove = "seniordeveloper@taokaenoi.co.th";
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                    _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

                }

            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }
    public string create_trainingoutplan_course_usertoleader(traning_req_Xml create_list, int id, string _link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_outplan_course_usertoleader.html")))
        {
            body = reader.ReadToEnd();
        }

        body = body.Replace("{FullNameTH_HRD}", create_list.pos_name_th);
        body = body.Replace("{doc_no}", create_list.doc_no);
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{zyear}", create_list.zyear.ToString());
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{training_group_name}", create_list.training_group_name);
        body = body.Replace("{training_branch_name}", create_list.training_branch_name);
        body = body.Replace("{priority_name}", create_list.priority_name);
        body = body.Replace("{priority_remark}", create_list.priority_remark);
        body = body.Replace("{place_name}", create_list.zplace_name);
        body = body.Replace("{date_start}", create_list.zdate_start);
        body = body.Replace("{date_end}", create_list.zdate_end);

        return body;
    }
    //****** START TRN-COURSE OUT PLAN USER TO LEADER *****//

    //****** START LEADER TO HR *****//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendemail_outplan_leadertohr(string jsonIn)
    {
        int id = 0;
        if (jsonIn != null)
        {
            data_elearning _data_elearning = new data_elearning();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _xml_in = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
            _xml_in = _funcTool.convertJsonToXml(_xml_in);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _xml_in);
            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    id = item.idx;
                }
            }

            if (id > 0)
            {
                int _tempInt = 0;
                string _mail_subject = "";
                string _mail_body = "";
                string _link = "";
                string emailmove = "";
                string replyempmove = "";
                string _local_xml = "";
                string spos_name_th = "", semp_name_th = "";

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    int i = 0;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        spos_name_th = item.pos_name_th;
                        semp_name_th = item.emp_name_th;
                        if (i == 0)
                        {
                            emailmove = item.emp_email;
                        }
                        else
                        {
                            emailmove = emailmove + "," + item.emp_email;
                        }
                        i++;
                    }
                }
                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-USER-TO-LEADER-DATA-COURSE-OP";
                obj_trainingLoolup.idx = id;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                    data_elearning _data_sentmail = new data_elearning();
                    _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                    traning_req_Xml create_senmail = new traning_req_Xml();

                    create_senmail.zyear = obj_trainingLoolup_Xml.zyear;
                    create_senmail.doc_no = obj_trainingLoolup_Xml.doc_no;
                    create_senmail.doc_date = obj_trainingLoolup_Xml.doc_date;
                    create_senmail.employee_name = semp_name_th;
                    create_senmail.pos_name_th = spos_name_th;
                    create_senmail.course_name = obj_trainingLoolup_Xml.course_name;
                    create_senmail.priority_name = obj_trainingLoolup_Xml.priority_name;
                    create_senmail.priority_remark = obj_trainingLoolup_Xml.priority_remark;
                    create_senmail.training_group_name = obj_trainingLoolup_Xml.training_group_name;
                    create_senmail.training_branch_name = obj_trainingLoolup_Xml.training_branch_name;
                    //create_senmail.zdate_start = obj_trainingLoolup_Xml.zdate_start + " " + obj_trainingLoolup_Xml.ztime_start;
                    //create_senmail.zdate_end = obj_trainingLoolup_Xml.zdate_end + " " + obj_trainingLoolup_Xml.ztime_end;

                    create_senmail.zplace_name = obj_trainingLoolup_Xml.zplace_name;
                    create_senmail.approve_mode_name = obj_trainingLoolup_Xml.approve_mode_name;
                    _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                    obj_trainingLoolup.operation_status_id = "TRN-PLAN-COURSE-U8";
                    obj_trainingLoolup.idx = id;
                    _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                    jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                    _xml_in = _funcTool.convertJsonToXml(jsonIn);
                    _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                    _local_xml = _funcTool.convertJsonToXml(_ret_val);
                    _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                    if (_data_elearning.trainingLoolup_Xml_action != null)
                    {
                        int icount = 0;

                        foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                        {
                            icount++;
                            if (icount == 1)
                            {
                                create_senmail.zdate_start = item.zdate_full;
                            }
                            else
                            {
                                create_senmail.zdate_start = create_senmail.zdate_start + " , " + item.zdate_full;
                            }
                        }
                    }

                    int i = 0;
                    string sName = string.Empty;

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    _mail_subject = "[TRAINING COURSE]";

                    _mail_body = create_trainingoutplan_leadertohr(_data_sentmail.el_traning_req_Xml_action[0], 1, "");

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    replyempmove = "seniordeveloper@taokaenoi.co.th";
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                    _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

                }


            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }
    public string create_trainingoutplan_leadertohr(traning_req_Xml create_list, int id, string _link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_outplan_course_leadertohr.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{FullNameTH_HRD}", create_list.pos_name_th);
        body = body.Replace("{doc_no}", create_list.doc_no);
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{zyear}", create_list.zyear.ToString());
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{training_group_name}", create_list.training_group_name);
        body = body.Replace("{training_branch_name}", create_list.training_branch_name);
        body = body.Replace("{priority_name}", create_list.priority_name);
        body = body.Replace("{priority_remark}", create_list.priority_remark);
        body = body.Replace("{approve_mode_name}", create_list.approve_mode_name);
        body = body.Replace("{place_name}", create_list.zplace_name);
        body = body.Replace("{date_start}", create_list.zdate_start);
        body = body.Replace("{date_end}", create_list.zdate_end);

        return body;
    }
    //****** END LEADER TO HR *****//

    //****** START LEADER TO HR *****//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendemail_outplan_leadertouser_all(string jsonIn)
    {
        int id = 0;
        if (jsonIn != null)
        {
            data_elearning _data_elearning = new data_elearning();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _xml_in = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
            _xml_in = _funcTool.convertJsonToXml(_xml_in);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _xml_in);
            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    id = item.idx;
                }
            }

            if (id > 0)
            {
                int _tempInt = 0;
                string _mail_subject = "";
                string _mail_body = "";
                string _link = "";
                string emailmove = "";
                string replyempmove = "";
                string _local_xml = "";
                string spos_name_th = "", semp_name_th = "";

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    int i = 0;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        spos_name_th = item.pos_name_th;
                        semp_name_th = item.emp_name_th;
                        if (i == 0)
                        {
                            emailmove = item.emp_email;
                        }
                        else
                        {
                            emailmove = emailmove + "," + item.emp_email;
                        }
                        i++;
                    }
                }
                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-USER-TO-LEADER-DATA-COURSE-OP";
                obj_trainingLoolup.idx = id;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                    data_elearning _data_sentmail = new data_elearning();
                    _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                    traning_req_Xml create_senmail = new traning_req_Xml();

                    create_senmail.zyear = obj_trainingLoolup_Xml.zyear;
                    create_senmail.doc_no = obj_trainingLoolup_Xml.doc_no;
                    create_senmail.doc_date = obj_trainingLoolup_Xml.doc_date;
                    create_senmail.employee_name = semp_name_th;
                    create_senmail.pos_name_th = spos_name_th;
                    create_senmail.course_name = obj_trainingLoolup_Xml.course_name;
                    create_senmail.priority_name = obj_trainingLoolup_Xml.priority_name;
                    create_senmail.priority_remark = obj_trainingLoolup_Xml.priority_remark;
                    create_senmail.training_group_name = obj_trainingLoolup_Xml.training_group_name;
                    create_senmail.training_branch_name = obj_trainingLoolup_Xml.training_branch_name;
                    //create_senmail.zdate_start = obj_trainingLoolup_Xml.zdate_start + " " + obj_trainingLoolup_Xml.ztime_start;
                    //create_senmail.zdate_end = obj_trainingLoolup_Xml.zdate_end + " " + obj_trainingLoolup_Xml.ztime_end;

                    create_senmail.zplace_name = obj_trainingLoolup_Xml.zplace_name;
                    create_senmail.approve_mode_name = obj_trainingLoolup_Xml.approve_mode_name;
                    _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                    obj_trainingLoolup.operation_status_id = "TRN-PLAN-COURSE-U8";
                    obj_trainingLoolup.idx = id;
                    _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                    jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                    _xml_in = _funcTool.convertJsonToXml(jsonIn);
                    _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                    _local_xml = _funcTool.convertJsonToXml(_ret_val);
                    _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                    if (_data_elearning.trainingLoolup_Xml_action != null)
                    {
                        int icount = 0;

                        foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                        {
                            icount++;
                            if (icount == 1)
                            {
                                create_senmail.zdate_start = item.zdate_full;
                            }
                            else
                            {
                                create_senmail.zdate_start = create_senmail.zdate_start + " , " + item.zdate_full;
                            }
                        }
                    }

                    int i = 0;
                    string sName = string.Empty;

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    _mail_subject = "[TRAINING COURSE]";

                    _mail_body = create_trainingoutplan_leadertouser_all(_data_sentmail.el_traning_req_Xml_action[0], 1, "");

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    replyempmove = "seniordeveloper@taokaenoi.co.th";
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                    _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

                }


            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }

    }
    public string create_trainingoutplan_leadertouser_all(traning_req_Xml create_list, int id, string _link)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_outplan_course_leadertouser_all.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{FullNameTH_HRD}", create_list.pos_name_th);
        body = body.Replace("{doc_no}", create_list.doc_no);
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{zyear}", create_list.zyear.ToString());
        body = body.Replace("{course_name}", create_list.course_name);
        body = body.Replace("{training_group_name}", create_list.training_group_name);
        body = body.Replace("{training_branch_name}", create_list.training_branch_name);
        body = body.Replace("{priority_name}", create_list.priority_name);
        body = body.Replace("{priority_remark}", create_list.priority_remark);
        body = body.Replace("{approve_mode_name}", create_list.approve_mode_name);
        body = body.Replace("{place_name}", create_list.zplace_name);
        body = body.Replace("{date_start}", create_list.zdate_start);
        body = body.Replace("{date_end}", create_list.zdate_end);

        return body;
    }
    //****** END LEADER TO HR *****//

    //****** START ÃÒÂ§Ò¹¼Å¡ÒÃ½Ö¡ÍºÃÁ leader TO  hr *****//
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void sendEmail_trn_summaryleadertohr(string jsonIn)
    {
        int id = 0, iEmpIDX = 0;
        if (jsonIn != null)
        {

            // function_tool _funcTool = new function_tool();
            data_elearning _data_elearning = new data_elearning();
            // function_dmu _func_dmu = new function_dmu();
            service_mail servicemail = new service_mail();
            trainingLoolup_Xml obj_trainingLoolup_Xml = new trainingLoolup_Xml();
            trainingLoolup obj_trainingLoolup = new trainingLoolup();

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _xml_in = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122); // return w/ json
            _xml_in = _funcTool.convertJsonToXml(_xml_in);
            _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _xml_in);
            if (_data_elearning.trainingLoolup_Xml_action != null)
            {
                foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                {
                    id = item.idx;
                    iEmpIDX = item.EmpIDX;
                }
            }

            if (id > 0)
            {
                int _tempInt = 0;
                string _mail_subject = "";
                string _mail_body = "";
                string _link = "http://demo.taokaenoi.co.th/trainingcourse";
                string emailmove = "";
                string replyempmove = "";
                string _local_xml = "";


                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-LEADER-TO-HR";
                obj_trainingLoolup.idx = id;
                obj_trainingLoolup.EmpIDX = iEmpIDX;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);


                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    int i = 0;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        if (i == 0)
                        {
                            emailmove = item.email_name;
                        }
                        else
                        {
                            emailmove = emailmove + "," + item.email_name;
                        }
                        i++;
                    }
                }

                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj_trainingLoolup.operation_status_id = "E-MAIL-LIST-LEADER-TO-HR";
                obj_trainingLoolup.idx = id;
                obj_trainingLoolup.EmpIDX = iEmpIDX;
                _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
                jsonIn = _funcTool.convertObjectToJson(_data_elearning);
                _xml_in = _funcTool.convertJsonToXml(jsonIn);
                _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_lookup", _xml_in, 122);
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_elearning = (data_elearning)_funcTool.convertXmlToObject(typeof(data_elearning), _local_xml);

                if (_data_elearning.trainingLoolup_Xml_action != null)
                {
                    obj_trainingLoolup_Xml = _data_elearning.trainingLoolup_Xml_action[0];
                    data_elearning _data_sentmail = new data_elearning();
                    _data_sentmail.el_traning_req_Xml_action = new traning_req_Xml[1];
                    traning_req_Xml create_senmail = new traning_req_Xml();

                    create_senmail.training_req_no = obj_trainingLoolup_Xml.doc_no;
                    create_senmail.u0_training_req_date = obj_trainingLoolup_Xml.doc_date;
                    create_senmail.employee_name = obj_trainingLoolup_Xml.employee_name;
                    create_senmail.dept_name_th = obj_trainingLoolup_Xml.dept_name_th;
                    create_senmail.SecNameTH = obj_trainingLoolup_Xml.SecNameTH;

                    int i = 0;
                    string sName = string.Empty;
                    foreach (var item in _data_elearning.trainingLoolup_Xml_action)
                    {
                        if (i == 0)
                        {
                            sName = item.training_name;
                        }
                        else
                        {
                            sName = sName + " , " + item.training_name;
                        }
                        i++;
                    }

                    create_senmail.training_name = sName;
                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    _mail_subject = "[TRAINING RESULTS]";

                    _mail_body = create_trn_summaryleadertohr(_data_sentmail.el_traning_req_Xml_action[0], 1, _link);

                    _data_sentmail.el_traning_req_Xml_action[0] = create_senmail;

                    replyempmove = "seniordeveloper@taokaenoi.co.th";
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
                    _service_mail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail µÍ¹ à·ÊÃÐºº

                }

            }
            //Retrun Complete
            _ret_val = _funcTool.convertObjectToJson(_data_elearning);

            Context.Response.Clear();
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(_ret_val);
        }
    }

    public string create_trn_summaryleadertohr(traning_req_Xml create_list, int id, string _link)
    {
        string body = string.Empty;  //trn_summaryleadertohr
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_register_rsults_leadertohr.html")))
        {
            body = reader.ReadToEnd();
        }

        body = body.Replace("{document_code}", create_list.training_req_no);
        body = body.Replace("{document_date}", create_list.u0_training_req_date);
        body = body.Replace("{name_request}", create_list.employee_name);
        body = body.Replace("{name_department}", create_list.dept_name_th);
        body = body.Replace("{name_training}", create_list.training_name);
        body = body.Replace("{name_sec}", create_list.SecNameTH);
        body = body.Replace("{link}", _link);

        return body;
    }
    //****** end ÃÒÂ§Ò¹¼Å¡ÒÃ½Ö¡ÍºÃÁ leader TO  hr *****//


    //********** start send E-mail **************//

    //email test

    public void SendHtmlFormattedEmailFull_testxx(string recepientEmailList, string recepientCcEmailList, string replyToEmailList, string subject, string body)
    {
        using (MailMessage mailMessage = new MailMessage())
        {
            mailMessage.From = new MailAddress("noreply@taokaenoi.co.th");
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            // set to mail list
            string[] recepientEmail = recepientEmailList.Split(',');
            foreach (string rEmail in recepientEmail)
            {
                if (rEmail != String.Empty)
                {
                    mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                }
            }
            // set cc mail list
            string[] recepientCcEmail = recepientCcEmailList.Split(',');
            foreach (string cEmail in recepientCcEmail)
            {
                if (cEmail != String.Empty)
                {
                    mailMessage.CC.Add(new MailAddress(cEmail)); //Adding Multiple To email Id
                }
            }
            //set reply to list
            string[] replyToEmail = replyToEmailList.Split(',');
            foreach (string rtEmail in replyToEmail)
            {
                if (rtEmail != String.Empty)
                {
                    mailMessage.ReplyToList.Add(new MailAddress(rtEmail)); //Adding Multiple To email Id
                }
            }
            mailMessage.Bcc.Add(new MailAddress("noreply@taokaenoi.co.th"));
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;
            System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
            NetworkCred.UserName = "noreply@taokaenoi.co.th";
            NetworkCred.Password = "knlolksosgsuakcd";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            // smtp.Port = 587;
            //smtp.Send(mailMessage);

        }
    }
    //elearning toei

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetELU0Course(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_action", _xml_in, 110); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetELM0Group(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_action", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetELM0branch(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_action", _xml_in, 211); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetELM0Level(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_action", _xml_in, 212); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetELPassCouse(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_action", _xml_in, 213); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetELGetDetailCouse(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_action", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetELGetDetailCousePerSec(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_action", _xml_in, 221); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetDelELU0Course(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_action", _xml_in, 910); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    #endregion


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void get_course_costcenter(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_el_m0_course", _xml_in, 10); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void get_expert_salary(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_el_m0_expert", _xml_in, 10); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    #region Bonus
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void InsertMaster_Quiz(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_quiz", _xml_in, 100); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectMaster_Quiz(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_quiz", _xml_in, 200); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void UpdateMaster_Quiz(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_quiz", _xml_in, 300); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteMaster_Quiz(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_elearning_quiz", _xml_in, 900); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    #endregion

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void get_training_location(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_elearning", "service_el_m0_training_location", _xml_in, 10); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //elearning
}