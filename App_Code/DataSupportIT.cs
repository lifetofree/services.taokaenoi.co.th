﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("DataSupportIT")]
public class DataSupportIT
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }
    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }
    public string Code { get; set; }
    public string ReturnIDX { get; set; }
    public string ReturnEmail { get; set; }
    public string ReturnCount { get; set; }


    public StatusSAP[] BoxStatusSAP { get; set; }
    public PrioritySAP[] BoxStatusPriority { get; set; }
    [XmlElement("BoxUserRequest")]
    public UserRequestList[] BoxUserRequest { get; set; }
    public SystemtList[] BoxSystem { get; set; }
    public FeedBackList[] BoxFeedBack { get; set; }
    public DowntimePOS[] BoxDowntimePOS { get; set; }
    [XmlElement("BoxPOSList")]
    public POSList[] BoxPOSList { get; set; }
    public ExportDataPOS[] BoxExportPOSList { get; set; }
    public ExportDataSAP[] BoxExportSAPList { get; set; }
    public ReportSAP[] BoxReportSAPList { get; set; }
    public ExportDataIT[] BoxExportITList { get; set; }
    public ShortDepartment[] BoxDepartmentList { get; set; }
    [XmlElement("request_list")]
    public Request[] request_list { get; set; }
    public CaseITlist[] CaseIT_Detail { get; set; }
    public MSAPlist[] BoxMSAPlist { get; set; }
    public RESList[] BoxRESList { get; set; }
    public ExportDataRES[] BoxExportRESList { get; set; }


}

[Serializable]
public class RESList
{
    public int RES1IDX { get; set; }
    public string RES1_Name { get; set; }
    public int RES1Status { get; set; }
    public int CEmpIDX { get; set; }
    public int RES2IDX { get; set; }
    public string RES2_Name { get; set; }
    public int RES2Status { get; set; }
    public int RES3IDX { get; set; }
    public string RES3_Name { get; set; }
    public int RES3Status { get; set; }
    public int RES4IDX { get; set; }
    public string RES4_Name { get; set; }
    public int RES4Status { get; set; }

}

public class MSAPlist
{

    public int SysIDX { get; set; }
    public string StateTime { get; set; }
    public string SysNameTH { get; set; }
    public int MS1IDX { get; set; }
    public int MS2IDX { get; set; }
    public int MS3IDX { get; set; }
    public int MS4IDX { get; set; }
    public int MS5IDX { get; set; }
    public string MS1_Name { get; set; }
    public string MS2_Name { get; set; }
    public string MS3_Name { get; set; }
    public string MS4_Name { get; set; }
    public string MS5_Name { get; set; }
    public string MS1_Code { get; set; }
    public string MS2_Code { get; set; }
    public string MS3_Code { get; set; }
    public string MS4_Code { get; set; }
    public string MS5_Code { get; set; }
    public int CEmpIDX { get; set; }
    public string CreateDate { get; set; }
    public string UpdateDate { get; set; }
    public int MS1Status { get; set; }
    public int MS2Status { get; set; }
    public int MS3Status { get; set; }
    public int MS4Status { get; set; }
    public int MS5Status { get; set; }
    public string MS1StatusDetail { get; set; }
    public string MS2StatusDetail { get; set; }
    public string MS3StatusDetail { get; set; }
    public string MS4StatusDetail { get; set; }
    public string MS5StatusDetail { get; set; }
    public string Name_Code1 { get; set; }
    public string Name_Code2 { get; set; }
    public string Name_Code3 { get; set; }
    public string Name_Code4 { get; set; }
    public string Name_Code5 { get; set; }

}

public class StatusSAP
{
    public int dtidx { get; set; }
    public int stidx { get; set; }
    public int status_state { get; set; }
    public int CEmpIDX { get; set; }
    public string status_name { get; set; }
    public string status_desc { get; set; }
    public string StatusDetail { get; set; }
    public string downtime_name { get; set; }
}

public class PrioritySAP
{
    public int PIDX { get; set; }
    public string Priority_name { get; set; }
    public int Priority_status { get; set; }
    public int CEmpIDX_PIDX { get; set; }
    public string StatusDetail { get; set; }
    public string baseline { get; set; }

}

[Serializable]
public class UserRequestList
{
    [XmlElement("EmailIDX")]
    public int EmailIDX { get; set; }
    [XmlElement("URQIDX")]
    public int URQIDX { get; set; }
    [XmlElement("MS1IDX")]
    public int MS1IDX { get; set; }
    [XmlElement("MS2IDX")]
    public int MS2IDX { get; set; }
    [XmlElement("RecieveIDX")]
    public int RecieveIDX { get; set; }
    [XmlElement("NCEmpIDX")]
    public int NCEmpIDX { get; set; }
    [XmlElement("MS3IDX")]
    public int MS3IDX { get; set; }
    [XmlElement("MS4IDX")]
    public int MS4IDX { get; set; }
    [XmlElement("MS5IDX")]
    public int MS5IDX { get; set; }
    [XmlElement("unidx")]
    public int unidx { get; set; }
    [XmlElement("acidx")]
    public int acidx { get; set; }
    [XmlElement("Priority_name")]
    public string Priority_name { get; set; }
    [XmlElement("PIDX")]
    public int PIDX { get; set; }
    [XmlElement("CStatus")]
    public int CStatus { get; set; }
    [XmlElement("IDXCMSAP")]
    public int IDXCMSAP { get; set; }
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }
    [XmlElement("EmpIDX_add")]
    public int EmpIDX_add { get; set; }
    [XmlElement("SysIDX_add")]
    public int SysIDX_add { get; set; }

    [XmlElement("CIT1IDX")]
    public int CIT1IDX { get; set; }
    [XmlElement("CIT2IDX")]
    public int CIT2IDX { get; set; }
    [XmlElement("CIT3IDX")]
    public int CIT3IDX { get; set; }
    [XmlElement("CIT4IDX")]
    public int CIT4IDX { get; set; }
    [XmlElement("Link_IT")]
    public string Link_IT { get; set; }
    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("POS1IDX")]
    public int POS1IDX { get; set; }
    [XmlElement("POS2IDX")]
    public int POS2IDX { get; set; }
    [XmlElement("POS3IDX")]
    public int POS3IDX { get; set; }
    [XmlElement("POS4IDX")]
    public int POS4IDX { get; set; }

    [XmlElement("Saplink")]
    public string Saplink { get; set; }
    [XmlElement("ITlink")]
    public string ITlink { get; set; }

    [XmlElement("Name_Code1")]
    public string Name_Code1 { get; set; }
    [XmlElement("Name_Code2")]
    public string Name_Code2 { get; set; }
    [XmlElement("Name_Code3")]
    public string Name_Code3 { get; set; }
    [XmlElement("Name_Code5")]
    public string Name_Code5 { get; set; }
    [XmlElement("Name_Code4")]
    public string Name_Code4 { get; set; }
    [XmlElement("Name_Code6")]
    public string Name_Code6 { get; set; }
    [XmlElement("Name_Code7")]
    public string Name_Code7 { get; set; }
    [XmlElement("Name_Code8")]
    public string Name_Code8 { get; set; }

    [XmlElement("ChkPlace")]
    public string ChkPlace { get; set; }
    [XmlElement("ChkAdaccept")]
    public string ChkAdaccept { get; set; }
    [XmlElement("ChkAddoing")]
    public string ChkAddoing { get; set; }

    [XmlElement("DC_YEAR")]
    public string DC_YEAR { get; set; }
    [XmlElement("DC_Mount")]
    public string DC_Mount { get; set; }


    [XmlElement("PIDX_Add")]
    public int PIDX_Add { get; set; }
    [XmlElement("DocCode")]
    public string DocCode { get; set; }
    [XmlElement("EmpCode")]
    public string EmpCode { get; set; }
    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }
    [XmlElement("Statusans")]
    public string Statusans { get; set; }
    [XmlElement("NameFull")]
    public string NameFull { get; set; }
    [XmlElement("NameCut")]
    public string NameCut { get; set; }
    [XmlElement("CommentAuto")]
    public string CommentAuto { get; set; }
    [XmlElement("CodeCut")]
    public string CodeCut { get; set; }
    [XmlElement("OLIDX")]
    public int OLIDX { get; set; }
    [XmlElement("RSecID")]
    public int RSecID { get; set; }
    [XmlElement("IFDate")]
    public int IFDate { get; set; }
    [XmlElement("r0idx")]
    public int r0idx { get; set; }
    [XmlElement("sumtime")]
    public string sumtime { get; set; }
    [XmlElement("nonkpi")]
    public string nonkpi { get; set; }
    [XmlElement("alltime")]
    public string alltime { get; set; }
    [XmlElement("EmpName")]
    public string EmpName { get; set; }
    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }
    [XmlElement("RDeptName")]
    public string RDeptName { get; set; }
    [XmlElement("NameEqDeviceETC")]
    public string NameEqDeviceETC { get; set; }
    [XmlElement("RPosIDX_J")]
    public int RPosIDX_J { get; set; }
    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }
    [XmlElement("RDeptID")]
    public int RDeptID { get; set; }
    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }
    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }
    [XmlElement("LocName")]
    public string LocName { get; set; }
    [XmlElement("IPAddress")]
    public string IPAddress { get; set; }
    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }
    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }
    [XmlElement("SysIDX")]
    public int SysIDX { get; set; }
    [XmlElement("SysName")]
    public string SysName { get; set; }
    [XmlElement("SysName1")]
    public string SysName1 { get; set; }
    [XmlElement("Email")]
    public string Email { get; set; }
    [XmlElement("MobileNo")]
    public string MobileNo { get; set; }
    [XmlElement("TelETC")]
    public string TelETC { get; set; }
    [XmlElement("EmailETC")]
    public string EmailETC { get; set; }
    [XmlElement("CostIDX")]
    public int CostIDX { get; set; }
    [XmlElement("CostNo")]
    public string CostNo { get; set; }
    [XmlElement("CostName")]
    public string CostName { get; set; }
    [XmlElement("CheckRemote")]
    public int CheckRemote { get; set; }
    [XmlElement("RemoteIDX")]
    public int RemoteIDX { get; set; }
    [XmlElement("RemoteName")]
    public string RemoteName { get; set; }
    [XmlElement("UserIDRemote")]
    public string UserIDRemote { get; set; }
    [XmlElement("PasswordRemote")]
    public string PasswordRemote { get; set; }
    [XmlElement("AdminIDX")]
    public int AdminIDX { get; set; }
    [XmlElement("AdminName")]
    public string AdminName { get; set; }
    [XmlElement("TimegetJob")]
    public string TimegetJob { get; set; }
    [XmlElement("DategetJob")]
    public string DategetJob { get; set; }
    [XmlElement("DategetJobSearch")]
    public string DategetJobSearch { get; set; }
    [XmlElement("TimecloseJob")]
    public string TimecloseJob { get; set; }
    [XmlElement("CreateDateUser")]
    public string CreateDateUser { get; set; }
    [XmlElement("TimeCreateJob")]
    public string TimeCreateJob { get; set; }
    [XmlElement("OtherName")]
    public string OtherName { get; set; }
    [XmlElement("DatecloseJob1")]
    public string DatecloseJob1 { get; set; }
    [XmlElement("DatecloseJob")]
    public string DatecloseJob { get; set; }
    [XmlElement("DatecloseJobSearch")]
    public string DatecloseJobSearch { get; set; }
    [XmlElement("DateReciveJobFirst")]
    public string DateReciveJobFirst { get; set; }
    [XmlElement("TimeReciveJobFirst")]
    public string TimeReciveJobFirst { get; set; }
    [XmlElement("AllContactAdmin")]
    public string AllContactAdmin { get; set; }
    [XmlElement("Comment")]
    public string Comment { get; set; }
    [XmlElement("AdminDoingIDX")]
    public int AdminDoingIDX { get; set; }
    [XmlElement("AdminDoingName")]
    public string AdminDoingName { get; set; }
    [XmlElement("CommentAMDoing")]
    public string CommentAMDoing { get; set; }
    [XmlElement("FileAMDoing")]
    public int FileAMDoing { get; set; }
    [XmlElement("DateCloseJob")]
    public string DateCloseJob { get; set; }
    [XmlElement("CCAIDX")]
    public int CCAIDX { get; set; }
    [XmlElement("SysIDXNoti")]
    public int SysIDXNoti { get; set; }
    [XmlElement("AddSort")]
    public string AddSort { get; set; }
    [XmlElement("CaseMean")]
    public string CaseMean { get; set; }
    [XmlElement("Abbreviation")]
    public string Abbreviation { get; set; }
    [XmlElement("addPlus")]
    public string addPlus { get; set; }
    [XmlElement("NameSys")]
    public string NameSys { get; set; }
    [XmlElement("StaIDX")]
    public int StaIDX { get; set; }
    [XmlElement("StaIDXComma")]
    public string StaIDXComma { get; set; }

    [XmlElement("amount")]
    public int amount { get; set; }
    [XmlElement("CountStatus")]
    public int CountStatus { get; set; }
    [XmlElement("StaName")]
    public string StaName { get; set; }
    [XmlElement("FileUser")]
    public int FileUser { get; set; }
    [XmlElement("detailUser")]
    public string detailUser { get; set; }
    [XmlElement("UserName")]
    public string UserName { get; set; }
    [XmlElement("UserLogonName")]
    public string UserLogonName { get; set; }
    [XmlElement("TransactionCode")]
    public string TransactionCode { get; set; }
    [XmlElement("DeviceIDX")]
    public int DeviceIDX { get; set; }
    [XmlElement("DeviceIDX_IT")]
    public int DeviceIDX_IT { get; set; }
    [XmlElement("DeviceCode_IT")]
    public string DeviceCode_IT { get; set; }
    [XmlElement("DeviceETC_IT")]
    public string DeviceETC_IT { get; set; }
    [XmlElement("EmailIDX_LN")]
    public string EmailIDX_LN { get; set; }
    [XmlElement("EmailNameETC_LN")]
    public string EmailNameETC_LN { get; set; }
    [XmlElement("RepairEmail")]
    public string RepairEmail { get; set; }
    [XmlElement("DeviceIDX_LN")]
    public int DeviceIDX_LN { get; set; }
    [XmlElement("DeviceCode_LN")]
    public string DeviceCode_LN { get; set; }
    [XmlElement("DeviceETC_LN")]
    public string DeviceETC_LN { get; set; }
    [XmlElement("TypeDeviceIDX")]
    public int TypeDeviceIDX { get; set; }
    [XmlElement("TypeNameEquip_IT")]
    public string TypeNameEquip_IT { get; set; }
    [XmlElement("EndWork")]
    public string EndWork { get; set; }
    [XmlElement("SearchSystem")]
    public int SearchSystem { get; set; }
    [XmlElement("SearchLocation")]
    public int SearchLocation { get; set; }
    [XmlElement("SearchFirstName")]
    public string SearchFirstName { get; set; }
    [XmlElement("SearchLastName")]
    public string SearchLastName { get; set; }
    [XmlElement("SearchOrganize")]
    public int SearchOrganize { get; set; }
    [XmlElement("SearchDept")]
    public int SearchDept { get; set; }
    [XmlElement("SearchSatatus")]
    public int SearchSatatus { get; set; }
    [XmlElement("IFSearch")]
    public int IFSearch { get; set; }
    [XmlElement("IFSearchragbetween")]
    public int IFSearchragbetween { get; set; }
    [XmlElement("SearchDate")]
    public string SearchDate { get; set; }
    [XmlElement("Code")]
    public string Code { get; set; }
    [XmlElement("CountCase")]
    public int CountCase { get; set; }
    [XmlElement("CountEmail")]
    public int CountEmail { get; set; }
    [XmlElement("CountSAP")]
    public int CountSAP { get; set; }
    [XmlElement("CountIT")]
    public int CountIT { get; set; }
    [XmlElement("CountComputer")]
    public int CountComputer { get; set; }
    [XmlElement("CountMonitor")]
    public int CountMonitor { get; set; }
    [XmlElement("CountNotebook")]
    public int CountNotebook { get; set; }
    [XmlElement("CountPrinter")]
    public int CountPrinter { get; set; }
    [XmlElement("CountInternet")]
    public int CountInternet { get; set; }
    [XmlElement("CountSoftware")]
    public int CountSoftware { get; set; }
    [XmlElement("CountOther")]
    public int CountOther { get; set; }

    [XmlElement("ManHours")]
    public int ManHours { get; set; }
    [XmlElement("Link")]
    public string Link { get; set; }
    [XmlElement("SapMsg")]
    public string SapMsg { get; set; }
    [XmlElement("CMIDX")]
    public int CMIDX { get; set; }
    [XmlElement("node_status")]
    public int node_status { get; set; }
    [XmlElement("Name1")]
    public string Name1 { get; set; }
    [XmlElement("Name2")]
    public string Name2 { get; set; }
    [XmlElement("Name3")]
    public string Name3 { get; set; }
    [XmlElement("Name4")]
    public string Name4 { get; set; }
    [XmlElement("Name5")]
    public string Name5 { get; set; }
    [XmlElement("Name6")]
    public string Name6 { get; set; }
    [XmlElement("Name7")]
    public string Name7 { get; set; }
    [XmlElement("Name8")]
    public string Name8 { get; set; }
    [XmlElement("Name9")]
    public string Name9 { get; set; }
    [XmlElement("Name10")]
    public string Name10 { get; set; }
    [XmlElement("Name11")]
    public string Name11 { get; set; }
    [XmlElement("Name12")]
    public string Name12 { get; set; }
    [XmlElement("Name13")]
    public string Name13 { get; set; }
    [XmlElement("CommentAutoIT")]
    public string CommentAutoIT { get; set; }
    [XmlElement("CDate")]
    public string CDate { get; set; }
    [XmlElement("CTime")]
    public string CTime { get; set; }
    [XmlElement("ISO_User")]
    public string ISO_User { get; set; }
    [XmlElement("ChkOrg")]
    public string ChkOrg { get; set; }
    [XmlElement("ChkOrgGM")]
    public string ChkOrgGM { get; set; }
    [XmlElement("ChkSys")]
    public string ChkSys { get; set; }
    [XmlElement("SysIDXGM")]
    public int SysIDXGM { get; set; }

    [XmlElement("Downtime_KPI")]
    public string Downtime_KPI { get; set; }
    [XmlElement("Downtim_NONKPI")]
    public string Downtim_NONKPI { get; set; }
    [XmlElement("ALLTIME")]
    public string ALLTIME { get; set; }

    [XmlElement("countit1id")]
    public string countit1id { get; set; }
    [XmlElement("countit2id")]
    public string countit2id { get; set; }
    [XmlElement("countit3id")]
    public string countit3id { get; set; }
    [XmlElement("countit4id")]
    public string countit4id { get; set; }
    [XmlElement("Name")]
    public string Name { get; set; }
    [XmlElement("cc")]
    public int cc { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("baseline")]
    public string baseline { get; set; }
    [XmlElement("countlist_downtime")]
    public string countlist_downtime { get; set; }

    [XmlElement("total_overtime")]
    public string total_overtime { get; set; }

    [XmlElement("overtime")]
    public string overtime { get; set; }


    [XmlElement("res_devicesidx")]
    public int res_devicesidx { get; set; }

    [XmlElement("devices_name")]
    public string devices_name { get; set; }

    [XmlElement("enidx")]
    public int enidx { get; set; }

    [XmlElement("en_name")]
    public string en_name { get; set; }

    [XmlElement("RES1IDX")]
    public int RES1IDX { get; set; }

    [XmlElement("RES2IDX")]
    public int RES2IDX { get; set; }

    [XmlElement("RES3IDX")]
    public int RES3IDX { get; set; }

    [XmlElement("RES4IDX")]
    public int RES4IDX { get; set; }

    [XmlElement("price")]
    public string price { get; set; }

    [XmlElement("RES1_Name")]
    public string RES1_Name { get; set; }

    [XmlElement("RES2_Name")]
    public string RES2_Name { get; set; }

    [XmlElement("RES3_Name")]
    public string RES3_Name { get; set; }

    [XmlElement("RES4_Name")]
    public string RES4_Name { get; set; }


}

public class FeedBackList
{
    public int URQIDX_Q { get; set; }
    public int UIDX { get; set; }
    public int SumPoint { get; set; }
    public int Name_Code1 { get; set; }
    public int WFIDX { get; set; }
    public int AQSIDX { get; set; }
    public int PO_MFBIDX2 { get; set; }
    public int U1IDX { get; set; }
    public int MFBIDX { get; set; }
    public int U0AQS { get; set; }
    public int POIDX { get; set; }
    public string Other { get; set; }
    public string Topic { get; set; }
    public string Question { get; set; }

}

public class SystemtList
{
    public int SysIDX { get; set; }
    public string SysNameTH { get; set; }
    public int stidx { get; set; }
    public string status_name { get; set; }
}

public class DowntimePOS
{
    public int r0lidx { get; set; }
    public int r0_Status { get; set; }
    public int CEmpIDX_dt { get; set; }
    public int r0didx { get; set; }
    public string StateTime { get; set; }
    public string EndTime { get; set; }

}

[Serializable]
public class POSList
{
    [XmlElement("POS1IDX")]
    public int POS1IDX { get; set; }
    [XmlElement("POS2IDX")]
    public int POS2IDX { get; set; }
    [XmlElement("POS3IDX")]
    public int POS3IDX { get; set; }
    [XmlElement("POS4IDX")]
    public int POS4IDX { get; set; }

    [XmlElement("POS1_Name")]
    public string POS1_Name { get; set; }
    [XmlElement("POS2_Name")]
    public string POS2_Name { get; set; }
    [XmlElement("POS3_Name")]
    public string POS3_Name { get; set; }
    [XmlElement("POS4_Name")]
    public string POS4_Name { get; set; }

    [XmlElement("POS1_Code")]
    public string POS1_Code { get; set; }
    [XmlElement("POS2_Code")]
    public string POS2_Code { get; set; }
    [XmlElement("POS3_Code")]
    public string POS3_Code { get; set; }
    [XmlElement("POS4_Code")]
    public string POS4_Code { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("POS1Status")]
    public int POS1Status { get; set; }
    [XmlElement("POS2Status")]
    public int POS2Status { get; set; }
    [XmlElement("POS3Status")]
    public int POS3Status { get; set; }
    [XmlElement("POS4Status")]
    public int POS4Status { get; set; }

    [XmlElement("POSStatusDetail")]
    public string POSStatusDetail { get; set; }

    [XmlElement("POSIDX")]
    public int POSIDX { get; set; }
    [XmlElement("POSStatus")]
    public int POSStatus { get; set; }

    [XmlElement("Name_Code1")]
    public string Name_Code1 { get; set; }
    [XmlElement("Name_Code2")]
    public string Name_Code2 { get; set; }
    [XmlElement("Name_Code3")]
    public string Name_Code3 { get; set; }
    [XmlElement("Name_Code4")]
    public string Name_Code4 { get; set; }


}

public class ExportDataPOS
{
    public string รหัสเอกสาร { get; set; }
    public string ระบบ { get; set; }
    public string ชื่อผู้แจ้ง { get; set; }

    public string ฝ่าย { get; set; }
    public string สถานที่ { get; set; }
    public string หมายเหตุแจ้งซ่อม { get; set; }
    public string วันที่แจ้งซ่อม { get; set; }

    public string สถานะ { get; set; }

    public string ชื่อผู้รับงาน { get; set; }
    public string วันที่รับงาน { get; set; }
    public string ชื่อผู้ปิดงาน { get; set; }
    public string วันที่ปิดงาน { get; set; }

    public string ความเห็นผู้ดูแล { get; set; }
    public string เคส { get; set; }
    public string อาการ { get; set; }
    public string แก้ไข { get; set; }
    public string Downtime_KPI { get; set; }
    public string Downtim_NONKPI { get; set; }
    public string ALLTIME { get; set; }

}

public class ExportDataSAP
{
    public string หมายเลขเอกสาร { get; set; }
    public string รหัสผู้สร้าง { get; set; }
    public string ชื่อผู้สร้าง { get; set; }

    public string Email { get; set; }
    public string ชื่อแผนกผู้สร้าง { get; set; }
    public string ชื่อฝ่ายผู้สร้าง { get; set; }
    public string ตัวย่อฝ่าย { get; set; }

    public int รหัสสถานที่ { get; set; }

    public string สถานที่ { get; set; }
    public int รหัสองค์กร { get; set; }

    public string ชื่อองค์กร { get; set; }
    public int รหัสระบบ { get; set; }

    public string ระบบ { get; set; }
    public int รหัสCostCenter { get; set; }

    public string CostCenter { get; set; }

    public string รหัสผู้รับงาน { get; set; }
    public string ชื่อผู้รับงาน { get; set; }
    public string วันที่รับงาน { get; set; }
    public string เวลาที่รับงาน { get; set; }

    public string รหัสผู้ปิดงาน { get; set; }
    public string ชื่อผู้ปิดงาน { get; set; }
    public string วันที่ปิดงาน { get; set; }
    public string เวลาที่ปิดงาน { get; set; }

    public string สถานะเอกสาร { get; set; }
    public string SAP_Log_ON { get; set; }
    public string TCode { get; set; }
    public string ปิดงานLV1 { get; set; }

    public string ปิดงานLV2 { get; set; }
    public string ปิดงานLV3 { get; set; }
    public string ปิดงานLV4 { get; set; }
    public string ปิดงานLV5 { get; set; }

    public int ManHours { get; set; }
    public string Link { get; set; }

    public string SapMsg { get; set; }
    public string คอมเม้นท์ผู้ปิดงาน { get; set; }
    public string คอมเม้นท์ผู้สร้าง { get; set; }
    public string วันที่แจ้ง { get; set; }

    public int รหัสความสำคัญ { get; set; }

    public string ความสำคัญ { get; set; }
    public string DowntimeHour { get; set; }
    public string DowntimeMin { get; set; }
}

public class ReportSAP
{
    public string Downtime { get; set; }
    public int URQIDX { get; set; }
    public string DocCode { get; set; }
    public int AdminDoingIDX { get; set; }
    public int EmpIDX { get; set; }
    public int AdminIDX { get; set; }
    public string EmpCode { get; set; }
    public string EmpName { get; set; }
    public string AdminName { get; set; }
    public string AdminDoingName { get; set; }
    public string RPosName { get; set; }
    public string RDeptName { get; set; }
    public string alltime { get; set; }
    public string CostCenter { get; set; }
    public string Name_Code1 { get; set; }
    public string Name_Code2 { get; set; }
    public string Name_Code3 { get; set; }
    public string Name_Code4 { get; set; }
    public string Name_Code5 { get; set; }
    public string DG_Mount { get; set; }
    public string DG_Year { get; set; }


}

public class ExportDataIT
{
    public string ประเภท { get; set; }

    public string เวลา { get; set; }
    public string รหัสเอกสาร { get; set; }
    public string สถานที่แจ้งซ่อม { get; set; }
    public string วันที่แจ้งซ่อม { get; set; }

    public string ชื่อผู้แจ้ง { get; set; }
    public string ฝ่าย { get; set; }
    public string หมายเหตุแจ้งซ่อม { get; set; }

    public string สถานะ { get; set; }
    public string ชื่อผู้รับงาน { get; set; }
    public string วันที่รับงาน { get; set; }
    public string ชื่อผู้ปิดงาน { get; set; }
    public string วันที่ปิดงาน { get; set; }
    public string หมายเหตุ { get; set; }
    public string ปิดงาน { get; set; }
    public string เคส { get; set; }
    public string อาการ { get; set; }
    public string แก้ไข { get; set; }
    public string สาเหตุ { get; set; }
    public string รวมเวลา { get; set; }
}

public class ShortDepartment
{
    public string DeptNameTH { get; set; }
    public string DeptNameEN { get; set; }
    public string ShortDept { get; set; }
    public string OrgNameTH { get; set; }
    public int DeptIDX { get; set; }
    public int OrgIDX { get; set; }
    public int CEmpIDX { get; set; }


}

[Serializable]
public class ExportDataRES
{

    public string ประเภท { get; set; }
    public string เวลา { get; set; }
    public string ช่วงเวลาแจ้งซ่อมถึงช่วงรับงาน { get; set; }
    public string ช่วงเวลารับงานถึงช่วงปิดงาน { get; set; }
    public string รหัสเอกสาร { get; set; }
    public string สถานที่แจ้งซ่อม { get; set; }
    public string วันที่แจ้งซ่อม { get; set; }
    public string ชื่อผู้แจ้ง { get; set; }
    public string ฝ่าย { get; set; }
    public string อุปกรณ์ { get; set; }
    public string หมายเหตุแจ้งซ่อม { get; set; }
    public string สถานะ { get; set; }
    public string ชื่อผู้รับงาน { get; set; }
    public string วันที่รับงาน { get; set; }
    public string ช่างดำเนินการ { get; set; }
    public string ชื่อผู้ปิดงาน { get; set; }
    public string วันที่ปิดงาน { get; set; }
    public string รวมเวลา { get; set; }
    public string หมายเหตุ { get; set; }
    public string หัวข้อ { get; set; }
    public string รายการ { get; set; }
    public string วิธีแก้ไข { get; set; }
    public string คำแนะนำ { get; set; }
    public string price { get; set; }

}


[Serializable]
public class Request  /*  Request Mobile  */
{
    [XmlElement("RQIDX")]
    public int RQIDX { get; set; }
    [XmlElement("RCode")]
    public string RCode { get; set; }
    [XmlElement("Request_type")]
    public int Request_type { get; set; }
    [XmlElement("Comment")]
    public string Comment { get; set; }
    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }
    [XmlElement("uq_status")]
    public int uq_status { get; set; }
    [XmlElement("deptname")]
    public string deptname { get; set; }
    [XmlElement("fullname")]
    public string fullname { get; set; }
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

}
public class CaseITlist
{
    public int CIT1IDX { get; set; }
    public int CIT2IDX { get; set; }
    public int CIT3IDX { get; set; }
    public int CIT4IDX { get; set; }
    public int CEmpIDX { get; set; }


    public int CIT1Status { get; set; }
    public int CIT2Status { get; set; }
    public int CIT3Status { get; set; }
    public int CIT4Status { get; set; }

    public string CIT1_Name { get; set; }
    public string CIT2_Name { get; set; }
    public string CIT3_Name { get; set; }

    public string CIT4_Name { get; set; }
    public string CIT1_Code { get; set; }
    public string CIT2_Code { get; set; }
    public string CIT3_Code { get; set; }
    public string CIT4_Code { get; set; }

    public string CIT1StatusDetail { get; set; }
    public string CIT2StatusDetail { get; set; }
    public string CIT3StatusDetail { get; set; }
    public string CIT4StatusDetail { get; set; }
}
