using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for service_execute
/// </summary>
public class service_execute {
    #region initial function/data
    function_db _funcDb = new function_db ();
    function_tool _funcTool = new function_tool ();

    string _spName = String.Empty;
    string _localXml = String.Empty;
    string _localJson = String.Empty;
    string _resultCode = String.Empty;
    string _resultMsg = String.Empty;

    #endregion  initial function/data

    public service_execute () {
        //
        // TODO: Add constructor logic here
        //
    }

    public string actionExec (string connName, string dataName, string cmdName, string xmlIn, int actionType) {
        // switch stored procedure name
        switch (cmdName) {
            case "service_log":
                _spName = "sp_service_log";
                break;
            case "service_employee":
                _spName = "sp_service_employee";
                break;
            case "service_vacation":
                _spName = "sp_service_vacation_u0_leavedocument_action";
                break;
            case "service_vacation_list":
                _spName = "sp_service_vacation_list_action";
                break;
            case "service_menu":
                _spName = "sp_service_control_m0_menu";
                break;
            case "service_permission":
                _spName = "sp_service_control_m0_permission";
                break;
            case "service_system":
                _spName = "sp_service_control_m0_system";
                break;
            case "service_networkdevices":
                _spName = "sp_service_nwd_m0_masterdata";
                break;

            case "service_datanetwork":
                _spName = "sp_service_nwd_u0_document";
                break;

            case "service_emps_announce":
                _spName = "emps_service_announce_action";
                break;

            case "service_emps_probation":
                _spName = "sp_service_u0_emps_probation";
                break;

            case "service_news":
                _spName = "sp_service_news_u0_news_action";
                break;

            case "ITSupport":
                _spName = "ITSupportAction";
                break;

            case "service_u0_device":
                _spName = "sp_devicesregis_action";
                break;

            case "service_m0_typedevice":
                _spName = "sp_m0_typedevice_action";
                break;

            case "service_m0_moniter":
                _spName = "sp_m0_moniter_action";
                break;

            case "service_m0_band":
                _spName = "sp_m0_band_action";
                break;

            case "service_m0_level":
                _spName = "sp_m0_leve_action";
                break;

            case "service_m0_status":
                _spName = "sp_m0_status_action";
                break;

            case "service_m0_insurance":
                _spName = "sp_m0_insurance_action";
                break;

            case "service_m0_hdd":
                _spName = "sp_m0_hdd_action";
                break;

            case "service_m0_ram":
                _spName = "sp_m0_ram_action";
                break;

            case "service_m0_vga":
                _spName = "sp_m0_vga_action";
                break;

            case "service_m0_cpu":
                _spName = "sp_m0_cpu_action";
                break;

            case "service_m0_printer":
                _spName = "sp_m0_printer_action";
                break;

            case "service_statal_ipaddress":
                _spName = "statal_service_m0_ipaddress_action";
                break;

            case "service_softwarelicense":
                _spName = "sp_service_swl_softwarelicense";
                break;

            case "service_softwarelicense_devices":
                _spName = "sp_service_swl_u0_softwarelicense";
                break;

            case "service_CenterCentralize":
                _spName = "CenterCentralize";
                break;

            case "service_CasePOSAction":
                _spName = "CasePOSAction";
                break;

            case "service_purchase":
                _spName = "sp_service_purchase_u0_document";
                break;

            case "service_purchase_master":
                _spName = "sp_service_purchase_m0_masterdata";
                break;

            case "service_purchase_report":
                _spName = "sp_service_purchase_report_action";
                break;

            case "service_CHR":
                _spName = "chr_u0_reportchr";
                break;

            case "adPermissionPolicyService":
                _spName = "ad_permission_policy_action";
                break;

            case "adPermissionTypeService":
                _spName = "ad_permission_type_action";
                break;

            case "adOnlineService":
                _spName = "ad_online_action";
                break;

            case "adOnlineVpnService":
                _spName = "ad_online_vpn_action1";
                break;

            case "adLogOnlineService":
                _spName = "ad_log_online_action";
                break;

            case "adCRUDNodeService":
                _spName = "ad_crud_node_action";
                break;

            case "service_jo":
                _spName = "jo_overview_action";
                break;

            case "service_car_par":
                _spName = "sp_service_car_par_action";
                break;

            case "HoldersDevice":
                _spName = "HolderDevicesAction";
                break;

            case "service_googlelicense":
                _spName = "sp_service_googlelicense_action";
                break;

            case "service_master_itnews":
                _spName = "sp_service_itnews_m0_action";
                break;

            case "service_itnews":
                _spName = "sp_service_itnews_u0_action";
                break;

            case "service_google_mailalert":
                _spName = "sp_service_google_mailalert_action";
                break;

            case "service_master_qa_lab":
                _spName = "sp_service_qa_lab_master_data_action";
                break;

            case "service_qa_lab":
                _spName = "sp_service_qa_lab_action";
                break;

            case "service_document_management":
                _spName = "TEST_DAR_Management_Action";
                break;
            case "m0_master_ma":
                _spName = "ma_m0_master_data";
                break;

            case "service_document_online":
                _spName = "DoccumentOnline_Action";
                break;
            case "services_networklayout":
                _spName = "sp_service_networklayout";
                break;
            case "ma_online_master":
                _spName = "ma_m0_master_action";
                break;

            case "service_mis_employee":
                _spName = "sp_service_mis_emp_action";
                break;
            case "serviceIBKRoomScore":
                _spName = "ibk_u0_room_score_action";
                break;
            case "serviceIBKTopicScore":
                _spName = "ibk_m0_topic_score_action";
                break;
            case "services_ma":
                _spName = "sp_service_ma_online";
                break;
            case "service_qa_cims_master":
                _spName = "sp_service_qa_cims_master";
                break;
            case "service_qa_cims":
                _spName = "sp_service_qa_cims_document";
                break;
            case "service_cen_member":
                _spName = "sp_Cen_Member_Action";
                break;

            case "service_enrepair":
                _spName = "EN_u0_ENRepair";
                break;

            case "service_mis_profile":
                _spName = "service_mis-profile";
                break;
            case "service_hr_healthcheck":
                _spName = "hr_hc_u0_healthcheck_action";
                break;
            case "service_MSAP1Action":
                _spName = "MSAP1Action";
                break;
            case "service_hr_costcenter":
                _spName = "sp_m0_costcenter";
                break;
            case "service_chr_online":
                _spName = "service_changerequest_online_action";
                break;
            case "service_itasset":
                _spName = "service_itasset_action"; //service_itasset_action_new
                break;
            case "service_itasset1":
                _spName = "service_itasset_action_test";
                break;
            case "service_nmon":
                _spName = "service_nmon_action";
                break;
            case "service_drc":
                _spName = "sp_service_drc_action";
                break;
            case "service_roombooking":
                _spName = "rbk_roombooking_action";
                break;
            case "service_carbooking":
                _spName = "cbk_carbooking_action";
                break;
            case "service_employee_profile":
                _spName = "sp_employee_profile_action";
                break;

            case "service_planning":
                _spName = "service_enp_planning";
                break;

            case "service_hr_Hospital":
                _spName = "sp_m0_hospital";
                break;

            case "service_version":
                _spName = "sp_m0_versionmobile";
                break;

            case "service_request":
                _spName = "sp_u0_request";
                break;

            case "service_CHR_ListCEO":
                _spName = "chr_u0_mandays_action";
                break;

            case "service_U0_CHR":
                _spName = "chr_u0_document_action";
                break;

            case "service_r0_Address":
                _spName = "sp_r0_Address";
                break;

            case "service_r0_Odsp":
                _spName = "sp_r0_odsp";
                break;

            case "service_FeedBack":
                _spName = "MFeedBack_action";
                break;

            case "service_product":
                _spName = "MProduct_Action";
                break;

            case "service_FeedBackT":
                _spName = "TFeedBack_action";
                break;

            case "service_elearning_master":
                _spName = "el_master_action";
                break;

            case "service_elearning_lookup":
                _spName = "el_lookup_action";
                break;

            case "service_elearning_transection":
                _spName = "el_transection_action";
                break;

            case "service_employee_profile_recruiter":
                //_spName = "sp_employee_profile_action_recruiter";
                //_spName = "sp_employee_profile_recruiter";
                _spName = "sp_employee_profile_recruiter_new";
                break;
            case "service_employee_recruiter_v2":

                _spName = "sp_employee_profile_recruiter_v2";
                break;
            case "service_recruit_VDO":
                _spName = "sp_employee_vdo_recruit";
                break;

            case "service_elearning_report":
                _spName = "el_report_action";
                break;

            case "service_employee_manage_position":
                _spName = "sp_m0_manage_position";
                break;

            case "service_telephone":
                _spName = "sp_service_telephone";
                break;

            case "service_tm_finger_template":
                _spName = "sp_tm_finger_template";
                break;
            case "service_tm_finger_machine":
                _spName = "sp_tm_finger_machine";
                break;
            case "service_tm_finger_time":
                _spName = "sp_tm_finger_time";
                break;
            case "service_hr_visatype":
                _spName = "sp_m0_visatype";
                break;
            case "service_emps_group_daily":
                _spName = "emps_mo_group_daily_action";
                break;
            case "service_emps_parttime":
                _spName = "emps_parttime_action";
                break;
            case "service_emps_empshift":
                _spName = "emps_empshift_action";
                break;
            case "service_emps_holiday":
                _spName = "emps_holiday_action";
                break;
            case "service_lunch_project":
                _spName = "sp_lm_finger_lunch";
                break;
            case "service_hr_perf_evalu_emp":
                _spName = "hr_perf_evalu_emp_action";
                break;
            case "service_ovt_overtime1new":
                _spName = "ovt_overtime_action_1new";
                break;
            case "service_ovt_overtime1":
                _spName = "ovt_overtime_action_new";
                break;
            case "service_ovt_overtime":
                _spName = "ovt_overtime_action";
                break;
            case "service_recruit_quest":
                _spName = "service_recruitment_question";
                break;
            case "service_recruit_quest_v2":
                _spName = "service_recruitment_question_v2";
                break;
            case "service_vm_visitors":
                _spName = "sp_vm_visitors_action";
                break;
            case "service_geo":
                _spName = "sp_geo_list_action";
                break;
            case "service_report_time":
                _spName = "sp_hr_u0_reporttime";
                break;
            case "service_asset_type":
                _spName = "sp_m0_asset_type";
                break;
            case "service_unit_type":
                _spName = "sp_m0_unit_type";
                break;
            case "service_resignation":
                _spName = "rst_service_exit_interview";
                break;
            case "service_permission_set":
                _spName = "sp_m0_permission";
                break;
            case "service_law_trademarks":
                _spName = "law_trademarks_contract_action";
                break;
            case "service_blacklist":
                _spName = "blacklist_action";
                break;
            case "service_manpower":
                _spName = "hr_service_man_power";
                break;
            case "service_qa_lab_report":
                _spName = "sp_service_qa_lab_report_action";
                break;
            case "service_corporate_kpi":
                _spName = "sp_service_corporate_kpi_action";
                break;
            case "service_tpm_form":
                _spName = "sp_tpm_formresult";
                break;
            case "service_qmr_problem":
                _spName = "qmr_service_problem";
                break;
            case "service_fsc_random":
                _spName = "s_fsc_random_action";
                break;
            case "service_report_iam":
                _spName = "iam_sp_center_report";
                break;
            case "service_tpm_report":
                _spName = "s_tpm_report_action";
                break;
            case "service_plansale":
                _spName = "ps_sp_u0_document_action";
                break;
            case "service_hr_payroll_slip":
                _spName = "s_hr_payroll_slip_action";
                break;
            case "service_sap_attach":
                _spName = "s_sap_attach_action";
                break;
            case "service_el_m0_expert":
                _spName = "sp_el_m0_expert_salary";
                break;
            case "service_el_m0_course":
                _spName = "sp_el_m0_course_costcenter";
                break;
            case "service_elearning_action":
                _spName = "el_elearning_action";
                break;
            case "service_elearning_quiz":
                _spName = "el_quiz_action";
                break;
            case "service_el_m0_training_location":
                _spName = "sp_el_m0_training_location";
                break;
            case "service_hr_checkin":
                _spName = "sp_hr_checkin";
                break;
            case "service_ecom_attach":
                _spName = "s_ecom_attach_action";
                break;
            case "service_it_notification":
                _spName = "sp_it_notification";
                break;
            case "sevice_hr_dashboard":
                _spName = "hr_report_dashboard";
                break;
            case "service_conference":
                _spName = "s_conference_action";
                break;
            case "service_epn_certificate":
                _spName = "epn_certificate_master";
                break;
            case "service_pms_action":
                _spName = "sp_pms_action";
                break;
            case "service_ecom_filinterface":
                _spName = "ecom_fileinterface";
                break;
            case "service_cen_master":
                _spName = "s_cen_master_action";
                break;
            case "service_ecom_saleunit":
                _spName = "ecom_saleunit_m0_sp";
                break;
            case "service_ecom_promotion":
                _spName = "ecom_promotion_sp";
                break;
            case "service_ecom_material":
                _spName = "ecom_material_sp";
                break;
            case "service_cen_employee":
                _spName = "s_cen_employee_action";
                break;
            case "service_ecom_channel":
                _spName = "ecom_channel_sp";
                break;
            case "service_ecom_BOM":
                _spName = "ecom_BOM_sp";
                break;
            case "service_pms_manage_action":
                _spName = "s_pms_manage_action";
                break;
            case "service_hr_news_action":
                _spName = "s_hr_news_action";
                break;
            case "service_hr_import_emp_action":
                _spName = "s_hr_import_emp_action";
                break;
            case "service_tkn_checkin":
                _spName = "s_tkn_checkin_action";
                break;
            default:
                _spName = "";
                break;
        }

        // exec  stored procedure
        if (_spName != null) {
            // execute
            _localXml = _funcDb.execSPXml (connName, _spName, xmlIn, actionType);
            // convert to json
            _localJson = _funcTool.convertXmlToJson (_localXml);
        } else {
            // set for null _spName
            _resultCode = "\"return_code\": 000";
            _resultMsg = "\"return_msg\": \"stored procedure is null\"";

            _localJson = "{\"" + dataName + "\": " + "{" + _resultCode + "," + _resultMsg + "}";
        }

        return _localJson;
    }
}