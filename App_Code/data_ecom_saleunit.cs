﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_ecom_saleunit")]
public class data_ecom_saleunit
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }





    // --- master data ---//

    public ecom_m0_saleunit[] ecom_m0_saleunit_list { get; set; }

}


public class ecom_m0_saleunit
{
    public int un_idx { get; set; }
    public string unit_name { get; set; }
    public string material { get; set; }
    public int cemp_idx { get; set; } 
    public int emp_idx_update { get; set; }
    public int un_status { get; set; }  
    public string create_date { get; set; }
    public string update_date { get; set; }

}
