﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for api_googlelicense
/// </summary>
/// 

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_googlelicense : System.Web.Services.WebService
{

    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = "";
    string _mail_subject = "";
    string _mail_body = "";


    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    service_mail _serviceMail = new service_mail();
    data_googlelicense _data_googlelicense = new data_googlelicense();


    public api_googlelicense()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


 
    // Get type Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTypeBuyggl(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 200); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get Software Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSoftwarenameggl(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 201); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get Company Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCompanyggl(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 202); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Set Buy Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetGooglelicense(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 100); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get Google License in Buy
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetGooglelicense(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 203); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get Google License in Buy
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetGooglelicenseDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 204); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get Google License in Buy
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSetGooglelicenseHD(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 205); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Set Google License in Holder Email
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetGooglelicenseHDEmail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 101); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_googlelicense = (data_googlelicense)_funcTool.convertXmlToObject(typeof(data_googlelicense), _local_xml);

            if (_data_googlelicense.return_code == 0)
            {

                string email = "waraporn.teoy@gmail.com";
                string email_create = string.Empty;
                string email_create_sent = string.Empty; 
                //string replyempcreate = _txtemailcreate.Text;
                string replyempcreate = "";

                //string link_google = "http://dev.taokaenoi.co.th/holder-google-license";

                //string link_google = "http://demo.taokaenoi.co.th/holder-google-license";

                string link_google = "http://mas.taokaenoi.co.th/holder-google-license";


                bind_u0_document_ggl_detail _temp_u0 = _data_googlelicense.bind_u0_document_ggl_list[0];

                email_create = _temp_u0.email_license;
                email_create_sent = email + ',' + email_create;

                _mail_subject = "[MIS/Google License : แจ้งรายการ] - แจ้งรายการผู้ถือครอง Google License";

                _mail_body = _serviceMail.googleLicenseCreate(_data_googlelicense.bind_u0_document_ggl_list[0], link_google);

                //_mail_body = _serviceMail.softwareLicenseCreate(_data_softwarelicense_devices.u0_softwarelicense_list[0], sent_mailsoftware, link_software);

                //_mail_body = _serviceMail.softwareLicenseCreate(_temp_u0);
                //_serviceMail.SendHtmlFormattedEmail(_temp_u0.emp_mail_list, _mail_subject, _mail_body);

                _serviceMail.SendHtmlFormattedEmailFull(email_create, "", replyempcreate, _mail_subject, _mail_body);
                //_temp_u0.email_license


            }
            else
            {

            }


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get Holder Google License in Holder Email
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetHolderGmailLicense(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 206); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get DDL Lot In expire
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetddlLotcode(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 207); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // GetDetailInLot In expire
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailInLot(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 209); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Set Tranfer Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetTranferGoogleLicense(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 102); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Set Delete Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetDeleteEmpggllicense(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 103); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get Search Index Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchIndexGooglelicense(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // data before Search Report Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetBindReportGooglelicense(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 211); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get Emp Holder In Repot Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetEmpInReportGGl(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 212); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get data Seach Report Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDataSearchReportGGL(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 213); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get data Seach Report Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetU0GoogleLicense(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 214); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get view u0_document where
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetGoogleLicenseView(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 215); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get Log Google License u0_document 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLogGoogleLicense(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 216); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get Log Google License u0_document 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLogDeleteHolderGGL(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 217); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Set Approve Holder Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetApproveGGLHolder(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 104); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_googlelicense = (data_googlelicense)_funcTool.convertXmlToObject(typeof(data_googlelicense), _local_xml);

            if (_data_googlelicense.return_code == 0)
            {

                //string email = "waraporn.teoy@gmail.com";

                string email = "it_support@taokaenoi.co.th,basis@taokaenoi.co.th";

                string email_holder = string.Empty;
                string email_sent = string.Empty;
                //string email = "it_support@taokaenoi.co.th,basis@taokaenoi.co.th";

                //string email = txtemail_holderinsert.Text; เมลผู้ถือครอง
                ///string replyempcreate = _txtemailcreate.Text;
                string replyempcreate = "";

                //string link_google = "http://dev.taokaenoi.co.th/holder-google-license";

                //string link_google = "http://demo.taokaenoi.co.th/holder-google-license";

                string link_google = "http://mas.taokaenoi.co.th/holder-google-license";


                approve_u0_document_ggl_detail _temp_approve = _data_googlelicense.approve_u0_document_ggl_list[0];

                email_holder = _temp_approve.email_license;

                email_sent = email + ',' + email_holder;

                _mail_subject = "[MIS/Google License : รับทราบรายการ] - รับทราบการถือครอง Google License";

                _mail_body = _serviceMail.googleLicenseApprove(_data_googlelicense.approve_u0_document_ggl_list[0], link_google);

               
                /////email_sent = email + ',' + email_holder;

                _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
                //_temp_u0.email_license
            }
            else
            {

            }


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // GetSearchHolderGGL Holder Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchHolderGGL(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 218); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // GetSelectMailAlert90 Holder Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSelectMailAlert90(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 219); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Get Select TypeEmail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSelectTypeEmail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // GetSelectMail Check Ad Online
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSelectMailCheckAd(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 310); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Set edit name email
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetGooglEditEmail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 105); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Set Google License in Holder Email
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetHolderEmailFormAdOnline(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);


            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_googlelicense", "service_googlelicense", _xml_in, 410); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_googlelicense = (data_googlelicense)_funcTool.convertXmlToObject(typeof(data_googlelicense), _local_xml);

            if (_data_googlelicense.return_code == 0)
            {

                //string email = "waraporn.teoy@gmail.com";
                //string email = email_empcreate;
                //string email = txtemail_holderinsert.Text; เมลผู้ถือครอง
                //string replyempcreate = _txtemailcreate.Text;

                //string email = "it_support@taokaenoi.co.th,basis@taokaenoi.co.th";
                string email = string.Empty;
                string email_holder = string.Empty;
                string email_sent = string.Empty;
                string replyempcreate = "waraporn.teoy@gmail.com";

                //string link_google = "http://dev.taokaenoi.co.th/holder-google-license";

                //string link_google = "http://demo.taokaenoi.co.th/holder-google-license";

                string link_google = "http://mas.taokaenoi.co.th/holder-google-license";


                bind_u0_document_ggl_detail _temp_u0 = _data_googlelicense.bind_u0_document_ggl_list[0];

                email = _temp_u0.email_license;
                //email_sent = email + ',' + email_holder;

                _mail_subject = "[MIS/Google License : แจ้งรายการ] - แจ้งรายการผู้ถือครอง Google License";

                _mail_body = _serviceMail.googleLicenseCreate(_data_googlelicense.bind_u0_document_ggl_list[0], link_google);

                //_mail_body = _serviceMail.softwareLicenseCreate(_data_softwarelicense_devices.u0_softwarelicense_list[0], sent_mailsoftware, link_software);

                //_mail_body = _serviceMail.softwareLicenseCreate(_temp_u0);
                //_serviceMail.SendHtmlFormattedEmail(_temp_u0.emp_mail_list, _mail_subject, _mail_body);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
                //_temp_u0.email_license
            }
            else
            {

            }


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

}
