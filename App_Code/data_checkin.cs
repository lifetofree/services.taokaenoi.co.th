﻿using System;
using System.Xml.Serialization;


public class data_checkin
{
    public int return_code { get; set; }
    public string return_msg { get; set; }

    public checkin_detail[] checkin_list { get; set; }

}

public class checkin_detail
{
    public string latitude { get; set; }
    public string longtitude { get; set; }
    public string cal_latitude { get; set; }
    public string location_name { get; set; }
    public string status_detail { get; set; }   
    public int type_select { get; set; }
    public int emp_idx { get; set; }
}


