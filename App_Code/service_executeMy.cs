using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for service_execute
/// </summary>
public class service_executeMy
{
    #region initial function/data
    function_dbMy _funcDb = new function_dbMy();
    function_tool _funcTool = new function_tool();

    string _spName = String.Empty;
    string _localXml = String.Empty;
    string _localJson = String.Empty;
    string _resultCode = String.Empty;
    string _resultMsg = String.Empty;

    #endregion  initial function/data

    public service_executeMy()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string actionExec(string connName, string dataName, string cmdName, string xmlIn, int actionType)
    {
        // switch stored procedure name
        switch (cmdName)
        {
            case "sp_fslog_data":
                _spName = "sp_fslog_data";
                break;
            case "sp_commerce_data_2":
                _spName = "sp_commerce_data_2";
                break;
            case "sp_commerce_data_3":
                _spName = "sp_commerce_data_3";
                break;
            case "sp_commerce_data_4":
                _spName = "sp_commerce_data_4";
                break;
            case "sp_commerce_data_6":
                _spName = "sp_commerce_data_6";
                break;
            case "sp_commerce_data_7":
                _spName = "sp_commerce_data_7";
                break;
            case "sp_commerce_data_8":
                _spName = "sp_commerce_data_8";
                break;
            case "sp_commerce_data_4_eye":
                _spName = "sp_commerce_data_4_eye";
                break;
            case "service_ecom_fileinterface":
                _spName = "sp_commerce_fileinterface";
                break;
        }

        // exec  stored procedure
        if (_spName != null)
        {
            // execute
           // _localXml = _funcTool.convertXmlToJson(xmlIn);
            _localXml = _funcDb.execMyJson(connName, _spName, _funcTool.convertXmlToJson(xmlIn), actionType);
            // convert to json
            //_localJson = _funcTool.convertXmlToJson(_localXml);
            _localJson = _localXml;
        }
        else
        {
            // set for null _spName
            _resultCode = "\"return_code\": 000";
            _resultMsg = "\"return_msg\": \"stored procedure is null\"";

            _localJson = "{\"" + dataName + "\": " + "{" + _resultCode + "," + _resultMsg + "}";
        }

        return _localJson;
    }
}
