using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_news")]
public class data_news
{
    public int return_code { get; set; }
    public string return_msg { get; set; }
    public u0_news_detail[] u0_news_list { get; set; }
}

[Serializable]
public class u0_news_detail
{
    public int uidx { get; set; }
    public string news_title { get; set; }
    public string news_detail { get; set; }
    public string news_author { get; set; }
    public int news_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}