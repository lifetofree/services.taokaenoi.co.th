using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Web.Configuration;
using System.Data;
using System.Configuration;

/// <summary>
/// Summary description for function_db
/// </summary>
public class function_dbMy
{
    private MySqlConnection connection;
    private string server;
    private string database;
    private string uid;
    private string password;
    private string connStr;

    //Constructor
    public function_dbMy()
    {
        Initialize();
    }

    //Initialize values
    private void Initialize()
    {
        server = "172.16.11.28"; //ConfigurationManager.AppSettings["serverip"];//"10.211.55.2";
        database = "shopdemo_tkn"; //ConfigurationManager.AppSettings["dbname"];//"fslog";
        uid = "shoptkn"; //ConfigurationManager.AppSettings["userid"];//"fsdemo";
        password = "$F3PdE"; //ConfigurationManager.AppSettings["userpassword"];//"123456";

        string connectionString;
        connectionString = "SERVER=" + server + ";" + "DATABASE=" +
        database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
        /*connectionString = "SERVER=" + "172.16.11.5" + ";" + "DATABASE=" +
        "fslog" + ";" + "UID=" + "fsdemo" + ";" + "PASSWORD=" + "123456" + ";";*/


        //string cnString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;

        //connection = new MySqlConnection(cnString);
        connection = new MySqlConnection(connectionString);
    }

    //open connection to database
    private bool ConnOpen()
    {
        try
        {

            connection.Open();
            return true;
        }
        catch (MySqlException ex)
        {
            //When handling errors, you can your application's response based 
            //on the error number.
            //The two most common error numbers when connecting are as follows:
            //0: Cannot connect to server.
            //1045: Invalid user name and/or password.
            //switch (ex.Number)
            //{
            //    case 0:
            //        MessageBox.Show("Cannot connect to server.  Contact administrator");
            //        break;

            //    case 1045:
            //        MessageBox.Show("Invalid username/password, please try again");
            //        break;
            //}
            connection.Close();
            return false;
        }
    }

    //Close connection
    private bool ConnClose()
    {
        connection.Close();
        return true;
    }

    //Execute query
    public bool ConnCmd(string cmd)
    {
        try
        {
            this.ConnOpen();
            MySqlCommand _cmd = new MySqlCommand();
            _cmd.CommandText = cmd;
            _cmd.Connection = connection;

            _cmd.ExecuteNonQuery();
            this.ConnClose();

            return true;
        }
        catch (MySqlException ex)
        {
            this.ConnClose();
            return false;
        }
    }

    //Execute Stored Procedure in JSON format
    public string execMyJson(string data_name, string sp_name, string json_in, int action_type)
    {
        var retJson = "";
        //var recordObject = "";
        //var lastRecord = "";

        //MySqlCommand cmd = new MySqlCommand();
        //cmd.Connection = connection;
        //cmd.CommandType = CommandType.Text;

        //cmdText = "CALL " + "`fslog`.`"+sp_name + "`('" + json_in + "', " + action_type + ", " + "@json_out); SELECT @json_out;";

        string constring = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
        MySqlConnection con = new MySqlConnection(constring);
        //MySqlCommand cmd = new MySqlCommand("sp_fslog_data", con);
        MySqlCommand cmd = new MySqlCommand(sp_name, con);

        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@json_in", json_in);
        cmd.Parameters.AddWithValue("@action_type", action_type);
        cmd.Parameters.Add("@json_out", MySqlDbType.Text);
        cmd.Parameters["@json_out"].Direction = ParameterDirection.Output;

        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            retJson = cmd.Parameters["@json_out"].Value.ToString();
            //retJson = JObject.Parse(cmd.Parameters["@json_out"].Value.ToString())["Datafslog"].Last().ToString();
            //retJson = JObject.Parse(retJson).ToString();
        }
        catch (Exception ex)
        {
            string _result_code = "\"result_code\": 100";
            string _result_msg = "\"result_msg\": " + ex.Message;

            retJson = "{\"" + data_name + "\": " + "{" + _result_code + "," + _result_msg + "}";
            //retJson = "{\"" + data_name + "\": " + "{" + _result_code + "}";
        }
        finally
        {
            this.ConnClose();
        }

        return retJson;
    }

    /*public SqlConnection strDBConn(string connName)
    {
        string strConn = WebConfigurationManager.ConnectionStrings[connName].ConnectionString; ;
        SqlConnection retDBConn = new SqlConnection(strConn);

        return retDBConn;
    }*/

    /*public string execSPXml(string connName, string spName, string xmlIn, int actionType)
    {
        var retXml = "";

        SqlConnection conn = strDBConn(connName);
        SqlCommand cmd = new SqlCommand(spName, conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@xml_in", SqlDbType.Xml).Value = xmlIn;
        cmd.Parameters.Add("@action_type", SqlDbType.Int).Value = actionType;
        cmd.Parameters.Add("@xml_out", SqlDbType.Xml).Direction = ParameterDirection.Output;

        try
        {
            conn.Open();
            cmd.ExecuteNonQuery();
            retXml = cmd.Parameters["@xml_out"].Value.ToString();
        }
        catch (Exception ex)
        {
            retXml = ex.Message;
        }
        finally
        {
            conn.Close();
        }

        return retXml.ToString();
    }*/
}