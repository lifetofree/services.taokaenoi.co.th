﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ServiceExec
/// </summary>
public class ServiceExec
{


    #region initial data
    FunctionToolEcom _funcTool = new FunctionToolEcom();
    DBConn _dbConn = new DBConn();

    string ret_data = "";
    string sp_name = "";
    #endregion

    public ServiceExec()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string action_exec(string data_name, string action_name, string json_in, int action_type)
    {
        switch (action_name)
        {
            case "e_commerce_action":
                sp_name = "sp_commerce_data_4";
                break;
        }

        if (sp_name != "")
        {
            ret_data = _dbConn.execMyJson(data_name, sp_name, json_in, action_type);
            // convert to xml
            ret_data = _funcTool.ConvertJsonToXml(ret_data);
        }

        return ret_data;
    }

}