﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_networkdevices
/// </summary>
/// 

public class data_softwarelicense
{

    public int return_code { get; set; }
    public string return_msg { get; set; }

    public m0software_detail[] m0software_list { get; set; }
    public m0software_dataset_detail[] m0software_dataset_list { get; set; }
    public bind_m0software_detail[] bind_m0software_list { get; set; }
    public log_m0software_detail[] log_m0software_list { get; set; }

    public m0_itswl_company_detail[] m0_itswl_company_list { get; set; }
    public organization_detail[] organization_list { get; set; }
    public softwarename_detail[] softwarename_list { get; set; }

    public bindm0_typedevice_detail[] bindm0_typedevice_list { get; set; }
    public bindu0_device_detail[] bindu0_device_list { get; set; }

}

public class softwarename_detail
{

    public int emp_idx { get; set; }
    public int software_name_idx { get; set; }

    public string type_name { get; set; }
    public string name { get; set; }

    public string software_name { get; set; }
    public string version { get; set; }
    public int software_type { get; set; }
    public int status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }

    public int company_idx { get; set; }
    public int license_idx { get; set; }
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }

    public int type_idx { get; set; }
   
    public int num_license { get; set; }

    public int check_checkbox { get; set; }

    public string asset_code { get; set; }
    public string po_code { get; set; }

    public int count_software_license { get; set; }

    public int software_use { get; set; }
    public int software_licenseuseasset { get; set; }
    public int software_ghost { get; set; }



}

public class m0software_detail
{
    
    public int emp_idx { get; set; }  
    public int software_idx { get; set; }   
    public string software_name { get; set; }   
    public string software_version { get; set; }    
    public string software_license { get; set; }  
    public string date_purchase { get; set; }   
    public string date_expire { get; set; }  
    public string company_name { get; set; }  
    public string place_name { get; set; }
    public string mobile_no { get; set; }   
    public int software_status { get; set; }
    public int cemp_idx { get; set; }   
    public string create_date { get; set; }
    public string update_date { get; set; }

    public int company_idx { get; set; }
    public int license_idx { get; set; }
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }

    public int type_idx { get; set; }
    public int num_license { get; set; }

    public int check_checkbox { get; set; }

    public string asset_code { get; set; }
    public string po_code { get; set; }

    public int software_price { get; set; }
    public int software_name_idx { get; set; }

}

public class m0software_dataset_detail
{

    public int emp_idx { get; set; }
    public int software_idx { get; set; }
    public string software_name { get; set; }
    public string software_version { get; set; }
    public string software_license { get; set; }
    public string date_purchase { get; set; }
    public string date_expire { get; set; }
    public string company_name { get; set; }
    public string place_name { get; set; }
    public string mobile_no { get; set; }
    public int software_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }

    public int company_idx { get; set; }
    public int license_idx { get; set; }
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }

    public int type_idx { get; set; }
    public int num_license { get; set; }
    public string asset_code { get; set; }

}

public class bind_m0software_detail
{

    public int emp_idx { get; set; }

    public int software_name_idx { get; set; }

    public int software_idx { get; set; }
    public string software_name { get; set; }
    public string software_version { get; set; }
    public string software_license { get; set; }
    public string date_purchase { get; set; }
    public string date_expire { get; set; }
    public string company_name { get; set; }
    public string place_name { get; set; }
    public string mobile_no { get; set; }
    public int software_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }

    public int company_idx { get; set; }
    public int license_idx { get; set; }
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }

    public int type_idx { get; set; }
    public string type_name { get; set; }
    public int num_license { get; set; }
    public int count_license { get; set; }

    public string asset_code { get; set; }
    public string po_code { get; set; }

    public int software_price { get; set; }

}

public class log_m0software_detail
{

    public int emp_idx { get; set; }
    public int software_idx { get; set; }
    public string software_name { get; set; }
    public string software_version { get; set; }
    public string software_license { get; set; }
    public string date_purchase { get; set; }
    public string date_expire { get; set; }
    public string company_name { get; set; }
    public string place_name { get; set; }
    public string mobile_no { get; set; }
    public int software_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }

    public int company_idx { get; set; }
    public int license_idx { get; set; }
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }

    public int type_idx { get; set; }
    public int num_license { get; set; }
    public int count_license { get; set; }

    public int license_org_idx { get; set; }
    public string OrgNameTH { get; set; }
    public string DeptNameTH { get; set; }
    public int num_license_rdept_idx { get; set; }

    public int license_rdept_idx { get; set; }

    public int count_asset { get; set; }
    public string asset_code { get; set; }

}

public class m0_itswl_company_detail
{

    public int emp_idx { get; set; }
    public int company_idx { get; set; }
    public string company_name { get; set; }
    public string place_name { get; set; }
    public string mobile_no { get; set; }
    public int company_status { get; set; }
    public string email { get; set; }


    public int software_idx { get; set; }
    public string software_name { get; set; }
    public string software_version { get; set; }
    public string software_license { get; set; }
    public string date_purchase { get; set; }
    public string date_expire { get; set; }
      
    public int software_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }

}

public class organization_detail
{
  
    public int emp_idx { get; set; }
    public string emp_name_th { get; set; }


    public int OrgIDX { get; set; }
    public string OrgNameTH { get; set; }
    public string OrgNameEN { get; set; }
    public int RDeptIDX { get; set; }
    public int DeptIDX { get; set; }
    public string DeptNameTH { get; set; }
    public string DeptNameEN { get; set; }

    public int RSecIDX { get; set; }
    public int SecIDX { get; set; }
    public string SecNameTH { get; set; }
    public string SecNameEN { get; set; }
    public int RPosIDX { get; set; }
    public int PosIDX { get; set; }
    public string PosNameTH { get; set; }
    public string PosNameEN { get; set; }
    public int EODSPIDX { get; set; }
    //public int EmpIDX { get; set; } 
    public int EmpIDXUser { get; set; }
    public int EStatus { get; set; }
    public string CreateDate { get; set; }
    public int EStaOut { get; set; }
    public int JobGradeIDX1 { get; set; }
    public int JobGradeIDX2 { get; set; }
    public string FullNameTH { get; set; }

    public bool selected { get; set; }

   

}

public class bindm0_typedevice_detail
{
    public int m0_tdidx { get; set; }
    public string name_m0_typedevice { get; set; }
    public int status_m0_typedevice { get; set; }
}

public class bindu0_device_detail
{
    public int u0_didx { get; set; }
    public string u0_code { get; set; }
    public string u0_acc { get; set; }
    public string u0_po { get; set; }
    public int m0_iridx { get; set; }
    public string u0_purchase { get; set; }
    public string u0_expDate { get; set; }
    public int m0_bidx { get; set; }
    public int m0_cpidx { get; set; }
    public int m0_sidx { get; set; }
    public int m0_lvidx { get; set; }
    public int u0_serial { get; set; }
    public int m0_tdidx { get; set; }
    public int u0_empcreate { get; set; }
    public string u0_createdate { get; set; }
    public int m0_orgidx { get; set; }
    
    public int u0_relation_didx { get; set; }

    public int u0_dvdidx { get; set; }
    public int m0_hidx { get; set; }
    public int m0_ridx { get; set; }
    public int m0_vidx { get; set; }
    public int m0_cidx { get; set; }
    public int m0_pidx { get; set; }
    public int m0_midx { get; set; }

    public string name_m0_typedevice { get; set; }
    public string name_m0_band { get; set; }
    public string detail_m0_band { get; set; }

    public string band_ram { get; set; }
    public string type_ram { get; set; }
    public string capacity_ram { get; set; }

    public string band_hdd { get; set; }
    public string type_hdd { get; set; }
    public string capacity_hdd { get; set; }

    public string type_vga { get; set; }
    public string generation_vga { get; set; }

    public string band_cpu { get; set; }
    public string generation_cpu { get; set; }
    public string ddl_band_cpu { get; set; }

    public string band_moniter { get; set; }
    public string size_moniter { get; set; }

    public string band_print { get; set; }
    public string type_print { get; set; }
    public string type_ink { get; set; }
    public string generation_print { get; set; }

    public int m0_Master { get; set; }
    public int u0_unit { get; set; }
    public string name_m0_status { get; set; }
    public string u0_relation_List { get; set; }

    public int u0_unidx { get; set; }
    public int u0_acidx { get; set; }
    public int u0_doc_decision { get; set; }

    public int u1_unidx { get; set; }
    public int u1_acidx { get; set; }
    public int u1_doc_decision { get; set; }
    public string doc_status { get; set; }
    public string u0_referent { get; set; }
    public string name_doc_status { get; set; }

    public int u0_didx_reference { get; set; }
    public string u0_referent_code { get; set; }
    public int u0_status_approve { get; set; }
    public string comment { get; set; }
    public int u0_didx_reference_transfer { get; set; }
    public int u0_slotram { get; set; }

    public int software_name_idx { get; set; }
    public int count_indept { get; set; }
    

    public int OrgIDX { get; set; }
    public string OrgNameTH { get; set; }
    
    public int RDeptIDX { get; set; }
    public string DeptNameTH { get; set; }
    public int RSecIDX { get; set; }
    public string SecNameTH { get; set; }

    public int emp_holder { get; set; }
    public string emp_name_th_holder { get; set; }
    public int u0_use { get; set; }
    public string email_holder { get; set; }
    public int u0_ghost { get; set; }
    public int u0_uselicense { get; set; }
    public int software_count_all { get; set; }



}