﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for api_qa_lab
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

public class api_qa_lab : System.Web.Services.WebService
{

    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_qa _data_qa = new data_qa();
    service_mail _serviceMail = new service_mail();

    //sent e - mail
    string _local_xml = "";
    string _mail_subject = "";
    string _mail_body = "";

    string document_code_mail = "";
    string test_details_mail = "";
    string details_mail = "";
    string create_date_mail = "";
    string _email_qa = "qa_lab@taokaenoi.co.th";
    string link_system_labis = "http://demo.taokaenoi.co.th/labis";

    public api_qa_lab()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    //master data place
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetplace(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 110); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert master data set name
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetNameList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 120); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data place
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetplace(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data set name
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetNameList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master datesample
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDateSampleList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 230); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master datesample where create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDateSampleListWhere(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 231); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master set test detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetSetTestDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 240); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master test detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetTestDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 241); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master material
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetMaterialList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 250); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Set M0 Material Impport
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetMaterialImport(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 150); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get M0 Material Import
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetMaterialImport(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 251); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Document //
    //Set Document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetDocument(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_qa _data_qa_sentmail = new data_qa();
            _data_qa_sentmail = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 110); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_qa_sentmail = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _local_xml);

            ////if (_data_qa_sentmail.return_code == 0)
            ////{

            ////    string email_cemp = "";
            ////    string email_cemptest = "";
            ////    email_cemptest = "waraporn.teoy@gmail.com";
            ////    //check e-mail null
            ////    if (_data_qa_sentmail.qa_lab_u0doc_qalab_list[0].emp_email != null)
            ////    {
            ////        email_cemp = "msirirak4441@gmail.com,sirinyamod@hotmail.com"; //ใช้ทดสอบ
            ////        //email_cemp = _data_qa_sentmail.qa_lab_u0doc_qalab_list[0].emp_email.ToString(); //ของจริง
            ////    }

            ////    string replyempcreate = "sirinyamod@hotmail.com";
            ////    string email = email_cemp + "," + email_cemptest; //ใช้ทดสอบ
            ////    //string email = email_cemp + "," + _email_qa; // ของจริง
            ////    qa_lab_u0doc_qalab _temp_u0 = _data_qa_sentmail.qa_lab_u0doc_qalab_list[0];
            ////    _mail_subject = "[QA LAB : LABIS ] - แจ้งการส่งตรวจวิเคราะห์ตัวอย่าง";

            ////    _mail_body = _serviceMail.SentMailLABISCreate(_data_qa_sentmail.qa_lab_u0doc_qalab_list[0], link_system_labis);

            ////    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            ////}
            ////else
            ////{

            ////}
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //Get U0 Document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetU0Document(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get View Detail U0 Document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDetailU0Document(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 211); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get View Test Detail U0 Document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetTestDetailU0Document(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 212); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get View Sample Test Detail U1 Document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetSampleView(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 213); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Test Detail In Sample
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetTestSample(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 214); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get History In Document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetHistoryDoc(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 215); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Decision Node2 receive and edit
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDecision2(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 310); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //approve receive node2 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetAppoveNode2(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            string decision_sentmail = "";

            data_qa _dataqa_reject_sentmail = new data_qa();
            _dataqa_reject_sentmail = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _xml_in);

            //  decision_sentmail = HttpUtility.UrlDecode(_dataqa_reject_sentmail.qa_lab_u2_qalab_list[0].decision.ToString());
            // sent_contentnews = _data_itnews.u0_it_news_list[0].details_news;

            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 111); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dataqa_reject_sentmail = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _local_xml);

            if (_dataqa_reject_sentmail.return_code == 1)
            {

                string email_cemp = "";
                // string email_cemp_ = "";
                //check e-mail null
                if (_dataqa_reject_sentmail.qa_lab_u0doc_qalab_list[0].emp_email != null)
                {
                    //email_cemp = "msirirak4441@gmail.com,waraporn.teoy@gmail.com";
                    email_cemp = _dataqa_reject_sentmail.qa_lab_u0doc_qalab_list[0].emp_email.ToString(); //ของจริง
                }

                string replyempcreate = "";
                string email = email_cemp;

                qa_lab_u0doc_qalab _temp_u01 = _dataqa_reject_sentmail.qa_lab_u0doc_qalab_list[0];

                _mail_subject = _dataqa_reject_sentmail.return_msg.ToString();//"[QA LAB : LABIS ] - แจ้งเพื่อดำเนินการแก้ไขเอกสารรายการตรวจวิเคราะห์";

                _mail_body = _serviceMail.SentMailLABISCreate(_dataqa_reject_sentmail.qa_lab_u0doc_qalab_list[0], link_system_labis);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            }
            else
            {

            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Document Actor 4 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDocActor4(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 410); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Testdetail in sample In Tab Lab
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetTestDetailLab(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 411); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Decision Node4 receive and noreceive Lab IN
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDecision4(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 312); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Sample Detail View Lab
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetSampleInLab(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 412); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Set Approve Node4
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetApproveNode4(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_qa _dataqa_reject_sentmail_node4 = new data_qa();
            _dataqa_reject_sentmail_node4 = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 112); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dataqa_reject_sentmail_node4 = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _local_xml);

            ////if (_dataqa_reject_sentmail_node4.return_code == 0)
            ////{
            ////    string email_cemp = "";
            ////    string replyempcreate = "sirinyamod@hotmail.com";
            ////    string email_cemptest = "waraporn.teoy@gmail.com";
            ////    //string email = "waraporn.teoy@gmail.com,msirirak4441@gmail.com,sirinyamod@hotmail.com";

            ////    if (_dataqa_reject_sentmail_node4.qa_lab_u0doc_qalab_list[0].emp_email != null)
            ////    {
            ////        email_cemp = "msirirak4441@gmail.com,sirinyamod@hotmail.com"; //ใช้ทดสอบ
            ////        //email_cemp = _data_qa_sentmail.qa_lab_u0doc_qalab_list[0].emp_email.ToString(); //ของจริง
            ////    }

            ////    string email = email_cemp + "," + email_cemptest; //ใช้ทดสอบ
            ////    //string email = email_cemp + "," + _email_qa; // ของจริง
            ////    qa_lab_u0doc_qalab _temp_u0_external_lab = _dataqa_reject_sentmail_node4.qa_lab_u0doc_qalab_list[0];
            ////    _mail_subject = "[QA LAB : LABIS ] - Lab(ภายใน) ดำเนินการ Reject ตัวอย่าง";

            ////    _mail_body = _serviceMail.SentMailLABISReject(_dataqa_reject_sentmail_node4.qa_lab_u0doc_qalab_list[0], link_system_labis);

            ////    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            ////}
            ////else
            ////{

            ////}
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get History In Sample Code
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetHistorySample(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 216); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Decision Node7 Approve and No Approve
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDecision7(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 313); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Decision Node7 Approve and No Approve
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetApproveNode7(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_qa _dataqa_reject_sentmail_node7 = new data_qa();
            _dataqa_reject_sentmail_node7 = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 114); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dataqa_reject_sentmail_node7 = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _local_xml);

            ////if (_dataqa_reject_sentmail_node7.return_code == 0)
            ////{

            ////    string replyempcreate = "sirinyamod@hotmail.com";
            ////    string email_cemptest = "waraporn.teoy@gmail.com";
            ////    string email_cemp = "";
            ////    if (_dataqa_reject_sentmail_node7.qa_lab_u0doc_qalab_list[0].emp_email != null)
            ////    {
            ////        email_cemp = "msirirak4441@gmail.com,sirinyamod@hotmail.com"; //ใช้ทดสอบ
            ////        //email_cemp = _data_qa_sentmail.qa_lab_u0doc_qalab_list[0].emp_email.ToString(); //ของจริง
            ////    }

            ////    string email = email_cemp + "," + email_cemptest; //ใช้ทดสอบ
            ////    //string email = email_cemp + "," + _email_qa; // ของจริง


            ////    qa_lab_u0doc_qalab _temp_u0_external_lab = _dataqa_reject_sentmail_node7.qa_lab_u0doc_qalab_list[0];
            ////    _mail_subject = "[QA LAB : LABIS ] - ผู้สร้างไม่ยืนยันการส่งตรวจไปยัง Lab(ภายนอก)";

            ////    _mail_body = _serviceMail.SentMailLABISReject(_dataqa_reject_sentmail_node7.qa_lab_u0doc_qalab_list[0], link_system_labis);

            ////    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            ////}
            ////else
            ////{

            ////}

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Decision Node8  
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDecision8(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 314); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Set Approve Node 8
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetApproveNode8(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 115); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Set Approve Node 11
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetApproveNode11(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_qa _data_qa_sentmail = new data_qa();
            _data_qa_sentmail = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 116); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_qa_sentmail = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _local_xml);

            ////if (_data_qa_sentmail.return_code == 0)
            ////{
            ////    string email_rsec = "";
            ////    string email_cemp = "";
            ////    //check e-mail null
            ////    if (_data_qa_sentmail.qa_lab_u0doc_qalab_list[0].emp_email_sec != null)
            ////    {
            ////        email_rsec = "waraporn.teoy@gmail.com,sirinyamod@hotmail.com";
            ////        //email_rsec = _data_qa_sentmail.qa_lab_u0doc_qalab_list[0].emp_email_sec.ToString(); //ของจริง
            ////    }
            ////    if (_data_qa_sentmail.qa_lab_u0doc_qalab_list[0].emp_email != null)
            ////    {
            ////        email_cemp = "msirirak4441@gmail.com";
            ////        //email_cemp = _data_qa_sentmail.qa_lab_u0doc_qalab_list[0].emp_email.ToString(); //ของจริง
            ////    }

            ////    string replyempcreate = "sirinyamod@hotmail.com";
            ////    string email = email_cemp + "," + email_rsec;//"waraporn.teoy@gmail.com,msirirak4441@gmail.com,sirinyamod@hotmail.com";

            ////    qa_lab_u0doc_qalab _temp_u0 = _data_qa_sentmail.qa_lab_u0doc_qalab_list[0];
            ////    _mail_subject = "[QA LAB : LABIS ] - แจ้งผลการตรวจวิเคราะห์ตัวอย่าง";

            ////    _mail_body = _serviceMail.SentMailLABISResultSuccess(_data_qa_sentmail.qa_lab_u0doc_qalab_list[0], link_system_labis);

            ////    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            ////}
            ////else
            ////{

            ////}

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Result Detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDetailResult(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 413); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // search sample code for record result test
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetSearchSampleCode(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 217); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select form for record result test
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetSelectForm(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 218); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select form for record result test
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetRecordResultTest(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 117); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Test Sup
    //[WebMethod]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public void QaGetResultSup(string jsonIn)
    //{
    //    if (jsonIn != null)
    //    {
    //        //convert to xml
    //        _xml_in = _funcTool.convertJsonToXml(jsonIn);
    //        // execute and return
    //        _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 510); // return w/ json
    //    }

    //    Context.Response.Clear();
    //    Context.Response.ContentType = "application/json; charset=utf-8";
    //    Context.Response.Write(_ret_val);
    //}

    // Test Sup Mod
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetRecordResultTest(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 219); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Import Excel Swab, Water, 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetImportExcel(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_qa _data_qa_sentmail = new data_qa();
            _data_qa_sentmail = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 119); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_qa_sentmail = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _local_xml);

            ////if (_data_qa_sentmail.return_code == 0)
            ////{

            ////    string replyempcreate = "sirinyamod@hotmail.com";
            ////    string email = "waraporn.teoy@gmail.com,msirirak4441@gmail.com,sirinyamod@hotmail.com";

            ////    qa_lab_u0doc_qalab _temp_u0 = _data_qa_sentmail.qa_lab_u0doc_qalab_list[0];
            ////    _mail_subject = "[QA LAB : LABIS ] - แจ้งการส่งตรวจวิเคราะห์ตัวอย่าง";

            ////    _mail_body = _serviceMail.SentMailLABISCreate(_data_qa_sentmail.qa_lab_u0doc_qalab_list[0], link_system_labis);

            ////    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            ////}
            ////else
            ////{

            ////}
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);

    }

    // Search Detail Index
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSearchDetailIndex(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 510); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Search Detail In Lab
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSearchDetailInLab(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 511); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // Search Detail In Lab New
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSearchDetailInLabNew(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 512); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    ///////////////////////////////////////////////////////////////////

    //insert master data set type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSettype(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 130); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //insert master data form detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetFormDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 140); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //insert master data r0 form 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetR0FormCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 141); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //insert master data r0 form 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetM0TestDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 142); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //insert master data r0 form 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetR0SetTestDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 143); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert master data lab type 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSet_labtype(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 160); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert master data lab 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSet_lab(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 170); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master form detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetFormDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 242); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master option
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetOption(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 243); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select r0 form create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetR0FormCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 244); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select r0 form create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetR1FormCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 245); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select lab type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Qagettype(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 260); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select r0 form create

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetM0Lab(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 270); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //delete master data place
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaDeleteplace(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 910); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data set name
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaDelSetName(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 920); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master lab type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaDeletetype(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 930); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master test detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaDelFormDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 940); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data r0 form
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaDelR0FormCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 941); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data test detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaDelM0TestDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 942); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data lab type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaDelete_labtype(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 960); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data lab type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaDelSetTestDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 980); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert master data r0 form 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetR1FormCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 144); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Decision Node3 receive and edit
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDecision3(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 311); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //approve receive node3 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetAppoveNode3(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_qa _datareject_sentmail = new data_qa();
            _datareject_sentmail = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 113); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _datareject_sentmail = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _local_xml);

            //if (_datareject_sentmail.return_code == 1)
            //{


            //    string replyempcreate = "sirinyamod@hotmail.com";
            //    string email = "msirirak4441@gmail.com,sirinyamod@hotmail.com";

            //    qa_lab_u0doc_qalab _temp_u0_external_lab = _datareject_sentmail.qa_lab_u0doc_qalab_list[0];
            //    // _mail_subject = "[QA LAB : LABIS ] - แจ้งการส่งตรวจวิเคราะห์ตัวอย่าง";
            //    _mail_subject = "[QA LAB : LABIS ] - แจ้งรายการที่จะต้องดำเนินการอนุมัติรายการเพื่อส่งตรวจแลปภายนอก";

            //    _mail_body = _serviceMail.SentMailLABISReject(_datareject_sentmail.qa_lab_u0doc_qalab_list[0], link_system_labis);

            //    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            //}
            //else
            //{

            //}
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    #region master data lab 
    #region select
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Qaget_lab(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 280); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion

    #region delete 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaDelete_lab(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 970); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion

    #endregion master data lab 

    #region master data lab M1 

    #region insert&Update
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSet_labM1(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 180); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion

    #region select
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Qaget_labM1(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 290); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion

    #region delete 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaDelete_labM1(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 990); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion

    #endregion master data lab 

    #region master data lab type

    #region select
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Qaget_labtype(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 270); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion
    #endregion


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetLabTypeOnSelectNode3(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_master_qa_lab", _xml_in, 211); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }



    // Test Get Value sent to ajax
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void getSupervisorApprove(string id, string emp, string dec, string place)
    {
        if (id != "" && emp != "" && dec != "" && place != "")
        {
            //string jsonIn = "{\"data_qa\":{\"return_code\":\"0\",\"qa_lab_u1_qalab_list\":{\"sample_code\":\"" + id + "\",\"cemp_idx\":\"" + emp + "\",\"u0_qalab_idx\":\"0\",\"decision\":\"" + dec + "\",\"u1_qalab_idx\":\"0\"}}}";

            string jsonIn = "{\"data_qa\":{\"return_code\":\"0\",\"qa_lab_u1_qalab_list\":{\"sample_code\":\"" + id + "\",\"cemp_idx\":\"" + emp + "\",\"u0_qalab_idx\":\"0\",\"decision\":\"" + dec + "\",\"m0_lab_idx\":\"" + place + "\",\"u1_qalab_idx\":\"0\"}}}";


            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_qa _dataqa_reject_sentmail_supervisor = new data_qa();
            _dataqa_reject_sentmail_supervisor = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _xml_in);

            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 118);

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dataqa_reject_sentmail_supervisor = (data_qa)_funcTool.convertXmlToObject(typeof(data_qa), _local_xml);

            ////if (_dataqa_reject_sentmail_supervisor.return_code == 0)
            ////{

            ////    string email_cemp = "";

            ////    //check e-mail null
            ////    if (_dataqa_reject_sentmail_supervisor.qa_lab_u0doc_qalab_list[0].emp_email != null)
            ////    {
            ////        email_cemp = "waraporn.teoy@gmail.com,msirirak4441@gmail.com,sirinyamod@hotmail.com";
            ////        //email_cemp = _data_qa_sentmail.qa_lab_u0doc_qalab_list[0].emp_email.ToString(); //ของจริง
            ////    }

            ////    string replyempcreate = "sirinyamod@hotmail.com";

            ////    string email = email_cemp; // ของปลอมนะ
            ////    // string email = email_cemp + "," + _email_qa; // ของจริง


            ////    qa_lab_u0doc_qalab _temp_u0 = _dataqa_reject_sentmail_supervisor.qa_lab_u0doc_qalab_list[0];
            ////    _mail_subject = "[QA LAB : LABIS ] - Supervisor ไม่อนุมัติรายการตรวจสอบตัวอย่าง";

            ////    _mail_body = _serviceMail.SentMailLABISReject(_dataqa_reject_sentmail_supervisor.qa_lab_u0doc_qalab_list[0], link_system_labis);

            ////    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            ////}
            ////else
            ////{

            ////}



        }
        Context.Response.AppendHeader("Access-Control-Allow-Origin", "*");
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select rsec view doc Reference
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetViewRsecReference(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select rsec view doc Reference
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetReportAnalysis(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 610); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetTestReportAnalysis(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 611); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetExportExcel(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 612); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // export details sample
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDetailExportExcel(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 613); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // export details result
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDetailResultExportExcel(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 614); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select details for update
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDetailForUpdateDocument(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 710); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set details for update
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetDetailForUpdateDocument(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 711); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set update document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaSetUpdateDocument(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 712); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // set export document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetDataExportExcel(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 615); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get place in page lab
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetSelectPlaceLab(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 513); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get search in page print
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetSearchDateForPrint(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 514); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // get search in page print
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetU1DocDateForPrint(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 515); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // set export document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void QaGetSearchDataExportExcel(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa", "service_qa_lab", _xml_in, 616); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
}
