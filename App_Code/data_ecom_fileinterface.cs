﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_ecom_fileinterface")]
public class data_ecom_fileinterface
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public ecom_fileinterface_data[] ecom_fileinterface_list { get; set; }

    public ecom_m0_data[] ecom_m0_data_list { get; set; }

    public ecom_u0_data[] ecom_u0_data_list { get; set; }

    public ecom_u1_data[] ecom_u1_data_list { get; set; }

    public ecom_m0_data_bom[] ecom_m0_data_bom_list { get; set; }
    public ecom_m1_data_bom[] ecom_m1_data_bom_list { get; set; }
    public ecom_m0_material[] ecom_m0_material_list { get; set; }

    public ecom_m0_m1_bom[] ecom_m0_m1_bom_list { get; set; }
}

public class ecom_m0_m1_bom
{

    public string bom_name { get; set; }

    public string start_date { get; set; }

    public string end_date { get; set; }

    public string product_name { get; set; }

    public string material { get; set; }

    public string unit_name { get; set; }

    public int amount { get; set; }

    public float price { get; set; }

    public float discount { get; set; }

    public string PromotionID { get; set; }

    public string PromotionDesc { get; set; }

    public string SAP_code { get; set; }

    public int type_promotion { get; set; }
    public decimal VAT { get; set; }


}

public class ecom_m0_data_bom
{
    public int bom_m0_idx { get; set; }
    public string bom_name { get; set; }
    public int cemp_idx { get; set; }
    public int emp_idx_update { get; set; }
    public int bom_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public string start_date { get; set; }
    public string end_date { get; set; }
}
public class ecom_m1_data_bom
{
    public int bom_m1_idx { get; set; }

    public int bom_m0_idx { get; set; }

    public string product_name { get; set; }

    public string material { get; set; }

    public string unit_name { get; set; }

    public decimal price { get; set; }

    public decimal discount { get; set; }

    public string PromotionID { get; set; }

    public string PromotionDesc { get; set; }
    public string SAP_code { get; set; }
    public int amount { get; set; }
    public int cemp_idx { get; set; }

    public int emp_idx_update { get; set; }

    public int bom_m1_status { get; set; }

    public string create_date { get; set; }

    public string update_date { get; set; }

    public int type_promotion { get; set; }
}
public class ecom_fileinterface_data
{

    public int qty { get; set; }
    public string city { get; set; }
    public string alias { get; set; }
    public string cname { get; set; }

    public string phone { get; set; }
    public string address { get; set; }

    public string currency { get; set; }
    public string postcode { get; set; }
    public float net_price { get; set; }
    public string reference { get; set; }
    public string tmaterial { get; set; }
    public float price_unit { get; set; }
    public float vat_amount { get; set; }
    public string county_name { get; set; }
    public string delivery_date { get; set; }
    public string id_order_state { get; set; }
    public string date_add { get; set; }
    public string product_name { get; set; }

}

public class ecom_m0_data
{
    public int channel_idx { get; set; }
    public string channel_name { get; set; }
    public int cemp_idx { get; set; }
    public string m0_order_type { get; set; }
    public string m0_sale_org { get; set; }
    public string m0_channel { get; set; }
    public string m0_division { get; set; }
    public string m0_sales_office { get; set; }
    public string m0_sales_group { get; set; }
    public string m0_customer_no { get; set; }
    public int channel_status { get; set; }

}
public class ecom_u0_data
{
    public int u0_fileinterface_idx { get; set; }
    public string file_name_import { get; set; }
    public string file_name_real { get; set; }
    public int user_emp_idx { get; set; }
    public int export_sap_status { get; set; }
    public int export_sap_emp_idx { get; set; }
    public int u0_status { get; set; }
    public string emp_idx_update { get; set; }
    public string create_date { get; set; }
    public int channel_idx { get; set; }
    public string channel_name { get; set; }
    public string emp_name_import { get; set; }
    public string emp_name_exsap { get; set; }

}

public class ecom_u1_data
{

    public string u1_fileinterface_idx { get; set; }

    public string u0_fileinterface_idx { get; set; }
 
    public string u1_order_type { get; set; }

    public string u1_sale_org { get; set; }

    public string u1_channel { get; set; }

    public string u1_division { get; set; }

    public string u1_sales_office { get; set; }

    public string u1_sales_group { get; set; }

    public string u1_customer_no { get; set; }

    public string u1_name { get; set; }

    public string u1_address { get; set; }

    public string u1_honse_number { get; set; }

    public string u1_postal_code { get; set; }

    public string u1_district { get; set; }

    public string u1_city { get; set; }

    public string u1_country { get; set; }

    public string u1_tel { get; set; }

    public string u1_tax_number { get; set; }

    public string u1_po_number { get; set; }

    public string u1_delivery_date { get; set; }

    public decimal u1_discount_1 { get; set; }

    public string u1_item { get; set; }

    public string u1_material { get; set; }

    public string u1_quantity { get; set; }

    public string u1_un_sale_unit { get; set; }

    public string u1_plant { get; set; }

    public float u1_price_unit { get; set; }
 
    public string u1_currency { get; set; }

    public string u1_discounttype { get; set; }

    public string u1_discountAmount { get; set; }

    public string u1_promotionID { get; set; }

    public string u1_promotionDesc { get; set; }

    public float u1_netPriceAmount { get; set; }

    public float u1_VATamount { get; set; }

    public string u1_item_cate { get; set; }

    public int export_sap_status { get; set; }

    public int export_sap_emp_idx { get; set; }
}

public class ecom_m0_material
{
    public int material_id { get; set; }
    public string material_name { get; set; }
    public string material { get; set; }
    public string name_and_material { get; set; }
    public int mat_status { get; set; }
    public int cemp_idx { get; set; }
}





