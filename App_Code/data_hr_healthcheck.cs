﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_qa
/// </summary>
/// 

[Serializable]
[XmlRoot("data_hr_healthcheck")]
public class data_hr_healthcheck
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    //history employee
    public hr_healthcheck_historyemployee_detail[] healthcheck_historyemployee_list { get; set; }

    //history type check form
    public hr_healthcheck_historytype_detail[] healthcheck_historytype_list { get; set; }
    public hr_healthcheck_history_topic_detail[] healthcheck_history_topic_list { get; set; }

    //form type history check
    public hr_healthcheck_historyform_detail[] healthcheck_historyform_list { get; set; }

    // selected detail type health check//
    public hr_healthcheck_typecheck_list_detail[] healthcheck_typecheck_list { get; set; }

    // master data doctor
    public hr_healthcheck_m0_province_detail[] healthcheck_m0_province_list { get; set; }
    public hr_healthcheck_m0_amphur_detail[] healthcheck_m0_amphur_list { get; set; }
    public hr_healthcheck_m0_district_detail[] healthcheck_m0_district_list { get; set; }
    public hr_healthcheck_m0_doctor_detail[] healthcheck_m0_doctor_list { get; set; }
    public hr_healthcheck_m0_detailtype_detail[] healthcheck_m0_detailtype_list { get; set; }
    //master data doctor

    //history work check
    public hr_healthcheck_u0_history_work_detail[] healthcheck_u0_history_work_list { get; set; }
    public hr_healthcheck_u1_history_work_detail[] healthcheck_u1_history_work_list { get; set; }
    public hr_healthcheck_u2_history_work_detail[] healthcheck_u2_history_work_list { get; set; }

    //history injury check
    public hr_healthcheck_u0_history_injury_detail[] healthcheck_u0_history_injury_list { get; set; } 
    public hr_healthcheck_u1_history_injury_detail[] healthcheck_u1_history_injury_list { get; set; }
    public hr_healthcheck_u2_history_injury_detail[] healthcheck_u2_history_injury_list { get; set; }

    //history health check
    public hr_healthcheck_u0_history_health_detail[] healthcheck_u0_history_health_list { get; set; }
    public hr_healthcheck_u1_history_health_detail[] healthcheck_u1_history_health_list { get; set; }
    public hr_healthcheck_u2_history_health_detail[] healthcheck_u2_history_health_list { get; set; }
    public hr_healthcheck_u3_history_health_detail[] healthcheck_u3_history_health_list { get; set; }

    //history health sick
    public hr_healthcheck_u0_history_sick_detail[] healthcheck_u0_history_sick_list { get; set; }
    public hr_healthcheck_u1_history_sick_detail[] healthcheck_u1_history_sick_list { get; set; }
    public hr_healthcheck_u2_history_sick_detail[] healthcheck_u2_history_sick_list { get; set; }
    public hr_healthcheck_u3_history_sick_detail[] healthcheck_u3_history_sick_list { get; set; }
    public hr_healthcheck_u4_history_sick_detail[] healthcheck_u4_history_sick_list { get; set; }

}

public class hr_healthcheck_historyemployee_detail
{

    public int EmpIDX { get; set; }
    public int TypeReset { get; set; }
    public int EmpReset { get; set; }
    public string EmpCode { get; set; }
    public string EmpCodeX { get; set; }
    public string EmpPassword { get; set; }
    public int EmpType { get; set; }
    public string EmpTypeName { get; set; }
    public int EmpProbation { get; set; }
    public string EmpProbationDate { get; set; }
    public int RPosIDX { get; set; }
    public int PosIDX { get; set; }
    public string PosNameTH { get; set; }
    public int SecIDX { get; set; }
    public int RSecIDX { get; set; }
    private string SecNameTH { get; set; }
    public int RDeptIDX { get; set; }
    public int DeptIDX { get; set; }
    public string DeptNameTH { get; set; }
    public int OrgIDX { get; set; }
    public string OrgNameTH { get; set; }
    public int CostIDX { get; set; }
    public string CostName { get; set; }
    public string CostNo { get; set; }
    public int SexIDX { get; set; }
    public int RSecID { get; set; }
    public int RSec_Position { get; set; }
    public string SexNameTH { get; set; }
    public int PrefixIDX { get; set; }
    public string FullNameEmpCode { get; set; }
    public string FirstNameTH { get; set; }
    public string LastNameTH { get; set; }
    public string NickNameTH { get; set; }
    public string FullNameTH { get; set; }
    public string FirstNameEN { get; set; }
    public string LastNameEN { get; set; }
    public string NickNameEN { get; set; }
    public string FullNameEN { get; set; }
    public string PhoneNo { get; set; }
    public string MobileNo { get; set; }
    public string Email { get; set; }
    public string Birthday { get; set; }
    public int NatIDX { get; set; }
    public string NatName { get; set; }
    public int RaceIDX { get; set; }
    public string RaceName { get; set; }
    public int RelIDX { get; set; }
    public string RelNameTH { get; set; }
    public string EmpAddr { get; set; }
    public int DistIDX { get; set; }
    public string DistName { get; set; }
    public int AmpIDX { get; set; }
    public string AmpName { get; set; }
    public int ProvIDX { get; set; }
    public string ProvName { get; set; }
    public int CountryIDX { get; set; }
    public string CountryName { get; set; }
    public int MarriedStatus { get; set; }
    public string PerNameTH { get; set; }
    public int InhabitStatus { get; set; }
    public string LiveNameTH { get; set; }
    public string EmpIN { get; set; }
    public string EmpOUT { get; set; }    
    public int EmpStatus { get; set; }
    public string EmpCreate { get; set; }
    public string EmpUpdate { get; set; }
    public int JobGradeIDX { get; set; }  
    public int JobLevel { get; set; }
    public string JobGradeName { get; set; } 
    public int MenuIDX { get; set; }
    public string IPAddress { get; set; }
    public string Search { get; set; }
    public string NameApprover1 { get; set; }
    public string NameApprover2 { get; set; }
    public int Experience_Month { get; set; }
    public int Experience_cut_day { get; set; }
    public int Experience_cut_Month { get; set; }  
    public int Experience_cut_Year_In { get; set; }
    public string EmpStatusName { get; set; }  
    public string IdentityCard { get; set; }
    public string MilName { get; set; }
    public string EmpProbation_Ex { get; set; }
    public string EmpProbationDate_Ex { get; set; }
    public string EmpIN_Ex { get; set; }
    public string Birthday_Ex { get; set; }  
    public string EmpStatus_Ex { get; set; }
    public string EmpOUT_Ex { get; set; }  
    public string AddStartdate { get; set; }  
    public string AddEndDate { get; set; }   
    public string EmpStatusOut_Ex { get; set; }
    public string PostCode { get; set; }

}

public class hr_healthcheck_historytype_detail
{

    public int m0_type_idx { get; set; }
    public string type_name { get; set; }
    public int type_status { get; set; }   
    public int cemp_idx { get; set; }

}

public class hr_healthcheck_historyform_detail
{
   
    public int r0_form_idx { get; set; }
    public int r1_form_idx { get; set; }    
    public int m0_type_idx { get; set; }  
    public string form_name { get; set; } 
    public int form_status { get; set; }  
    public int m0_topic_idx { get; set; }  
    public string topic_name { get; set; }
    public int option_idx { get; set; }
    public int root_idx { get; set; }
    public int set_idx { get; set; }
    public int setroot_idx { get; set; }
    public string set_name { get; set; }

}

public class hr_healthcheck_typecheck_list_detail
{
   
    public int m0_detail_typecheck_idx { get; set; }
    public string form_ndetail_typecheckame { get; set; }
    public int status { get; set; }

}

public class hr_healthcheck_history_topic_detail
{
    public int m0_history_topic_idx { get; set; }
    public int m0_type_idx { get; set; }
    public string history_topic_name { get; set; }
    public string type_name { get; set; }

}

public class hr_healthcheck_m0_province_detail
{
    public int ProvIDX { get; set; }
    public int ProvCode { get; set; }
    public string ProvName { get; set; }
}

public class hr_healthcheck_m0_amphur_detail
{
    public int ProvIDX { get; set; }
    public int AmpIDX { get; set; }
    public string AmpName { get; set; }

}

public class hr_healthcheck_m0_district_detail 
{
    public int ProvIDX { get; set; }
    public int AmpIDX { get; set; }
    public int DistIDX { get; set; }
    public string DistName { get; set; }
}

public class hr_healthcheck_m0_doctor_detail
{

    public int m0_doctor_idx { get; set; }
    public int ProvIDX { get; set; }
    public string ProvName { get; set; }
    public int AmpIDX { get; set; }
    public string AmpName { get; set; }
    public int DistIDX { get; set; }
    public string DistName { get; set; }
    public string doctor_name { get; set; }
    public string card_number { get; set; }
    public string health_authority_name { get; set; }
    public string location_name { get; set; }
    public string village_no { get; set; }
    public string road_name { get; set; }
    public string phone_number { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public int doctor_status { get; set; }

}

public class hr_healthcheck_m0_detailtype_detail
{
    public int m0_detail_typecheck_idx { get; set; }
    public string detail_typecheck { get; set; }
    public int cemp_idx { get; set; }
    public int status { get; set; }
   
}

public class hr_healthcheck_u0_history_work_detail
{
    public int u0_historywork_idx { get; set; }
    public int m0_type_idx { get; set; }
    public int emp_idx { get; set; }
    public int cemp_idx { get; set; }
    public int u0_historywork_status { get; set; }
    public string create_date { get; set; }
}

public class hr_healthcheck_u1_history_work_detail
{
    public int u1_historywork_idx { get; set; }
    public int u0_historywork_idx { get; set; }
    public int emp_idx { get; set; }
    public int cemp_idx { get; set; }
    public int u0_historywork_status { get; set; }
    public int u1_historywork_status { get; set; }
    public string create_date { get; set; }
}

public class hr_healthcheck_u2_history_work_detail
{

    public int u2_historywork_idx { get; set; }
    public int u1_historywork_idx { get; set; }
    public int u0_historywork_idx { get; set; }
    public int emp_idx { get; set; }
    public int cemp_idx { get; set; }
    public int u0_historywork_status { get; set; }
    public int u2_historywork_status { get; set; }
    public string create_date { get; set; }
    public string company_name { get; set; }
    public string department_name { get; set; }
    public string business_type { get; set; }
    public string job_description { get; set; }
    public string job_startdate { get; set; }
    public string job_enddate { get; set; }
    public string risk_health { get; set; }
    public string protection_equipment { get; set; }

}

public class hr_healthcheck_u0_history_injury_detail
{

    public int u0_historyinjury_idx { get; set; }
    public int emp_idx { get; set; }
    public int m0_type_idx { get; set; }
    public int cemp_idx { get; set; }
    public int u0_historyinjury_status { get; set; }
    public string create_date { get; set; }

}

public class hr_healthcheck_u1_history_injury_detail
{
    public int u1_historyinjury_idx { get; set; }
    public int u0_historyinjury_idx { get; set; }
    public int emp_idx { get; set; }
    public int cemp_idx { get; set; }
    public int u0_historyinjury_status { get; set; }
    public int u1_historyinjury_status { get; set; }
    public string create_date { get; set; }

}

public class hr_healthcheck_u2_history_injury_detail
{
    public int u2_historyinjury_idx { get; set; }
    public int u0_historyinjury_idx { get; set; }
    public int emp_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string date_injuly { get; set; }
    public string injury_detail { get; set; }
    public string cause_Injury { get; set; }
    public string disability { get; set; }
    public string lost_organ { get; set; }
    public int not_working { get; set; }
    public int u2_historyinjury_status { get; set; }
    public string update_date { get; set; }
    public string detail_not_working { get; set; }

}

public class hr_healthcheck_u0_history_health_detail
{

    public int u0_historyhealth_idx { get; set; }
    public int u2_historyhealth_idx { get; set; }
    public int emp_idx { get; set; }
    public int m0_type_idx { get; set; }
    public int cemp_idx { get; set; }
    public int u0_historyhealth_status { get; set; }
    public string create_date { get; set; }
}

public class hr_healthcheck_u1_history_health_detail
{
    public int u1_historyhealth_idx { get; set; }
    public int u0_historyhealth_idx { get; set; }
    public int emp_idx { get; set; }
    public int cemp_idx { get; set; }
    public int u1_historyhealth_status { get; set; }
    public string create_date { get; set; }
}

public class hr_healthcheck_u2_history_health_detail
{
    public int u2_historyhealth_idx { get; set; }
    public int condition { get; set; }
    public int u0_historyhealth_idx { get; set; }
    public int m0_detail_typecheck_idx { get; set; }
    public int m0_doctor_idx { get; set; }
    public int emp_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string date_check { get; set; }
    public string num_check { get; set; }
    public string weight { get; set; }
    public string height { get; set; }
    public string body_mass { get; set; }
    public string bloodpressure { get; set; }
    public string specify_resultbody { get; set; }
    public string resultlab { get; set; }
    public int u2_historyhealth_status { get; set; }
 
    public string pulse { get; set; }


    public string detail_typecheck { get; set; }

    public string doctor_name { get; set; }

    public string card_number { get; set; }


    public string health_authority_name { get; set; }

    public string location_name { get; set; }


    public string village_no { get; set; }

    public string road_name { get; set; }


}

public class hr_healthcheck_u3_history_health_detail
{
    public int u3_historyhealth_idx { get; set; }
    public int u0_historyhealth_idx { get; set; }
    public int emp_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string rike_health { get; set; }
    public string result_health { get; set; }
    public int u3_historyhealth_status { get; set; }
    public int u2_historyhealth_idx { get; set; }
}

public class hr_healthcheck_u0_history_sick_detail
{

    public int u0_historysick_idx { get; set; }
    public int emp_idx { get; set; }
    public int m0_type_idx { get; set; }
    public int cemp_idx { get; set; }
    public int u0_historysick_status { get; set; }
    public string create_date { get; set; }

}

public class hr_healthcheck_u1_history_sick_detail
{
    public int u1_historysick_idx { get; set; }
    public int u0_historysick_idx { get; set; }
    public int emp_idx { get; set; }
    public int cemp_idx { get; set; }
    public int u1_historysick_status { get; set; }
    public string create_date { get; set; }

}

public class hr_healthcheck_u2_history_sick_detail
{

    public int u2_historysick_idx { get; set; }
    public int u0_historysick_idx { get; set; }
    public int emp_idx { get; set; }
    public string havedisease { get; set; }
    public string surgery { get; set; }
    public string immune { get; set; }
    public string drugs_eat { get; set; }
    public string allergy_history { get; set; }
    public int smoking { get; set; }
    public string smoking_value1 { get; set; }
    public string smoking_value2 { get; set; }
    public string smoking_value3 { get; set; }
    public string smoking_value4 { get; set; }
    public int alcohol { get; set; }
    public string alcohol_value1 { get; set; }
    public string alcohol_value2 { get; set; }
    public string addict_drug { get; set; }
    public string datahealth { get; set; }
    public int cemp_idx { get; set; }

}

public class hr_healthcheck_u3_history_sick_detail
{

    public int u3_historysick_idx { get; set; }
    public int u0_historysick_idx { get; set; }
    public string eversick { get; set; }
    public string sick_year { get; set; }
    public int emp_idx { get; set; }
    public int cemp_idx { get; set; }
    public int u3_historysick_status { get; set; }
    public string create_date { get; set; }

}

public class hr_healthcheck_u4_history_sick_detail
{

    public int u4_historysick_idx { get; set; }
    public int u0_historysick_idx { get; set; }
    public string relation_family { get; set; }
    public string disease_family { get; set; }
    public int emp_idx { get; set; }
    public int cemp_idx { get; set; }
    public int u4_historysick_status { get; set; }
    public string create_date { get; set; }

}
