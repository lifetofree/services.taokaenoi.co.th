using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for api_employee
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
// [System.Web.Script.Services.ScriptService]
public class api_vacation : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = "";
    string _mail_subject = "";
    string _mail_body = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    service_mail _serviceMail = new service_mail();
    data_vacation _data_vacation = new data_vacation();

    public api_vacation()
    {
        //Uncomment the following line if using designed components
        //InitializeComponent();
    }

    #region master data
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLeaveTypeList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_vacation", "service_vacation_list", _xml_in, 21); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetShfitTimeList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_vacation", "service_vacation_list", _xml_in, 22); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion master data

    #region for employee
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetAllList(int emp_idx)
    {
        u0_document_detail _u0_doc = new u0_document_detail();
        // set data
        _u0_doc.emp_idx = emp_idx;
        _data_vacation.u0_document_list = new u0_document_detail[1];
        _data_vacation.u0_document_list[0] = _u0_doc;
        // convert to xml
        _xml_in = _funcTool.convertJsonToXml(_funcTool.convertObjectToJson(_data_vacation));
        // execute and return
        _ret_val = _serviceExec.actionExec("masConn", "data_vacation", "service_vacation_list", _xml_in, 20); // return w/ json

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetListStatus(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_vacation", "service_vacation_list", _xml_in, 30); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSaveList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_vacation", "service_vacation_list", _xml_in, 10); // return w/ json
            
            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_vacation = (data_vacation)_funcTool.convertXmlToObject(typeof(data_vacation), _local_xml);
            if(_data_vacation.return_code == 0)
            {
                // create mail body
                u0_document_detail _temp_u0 = _data_vacation.u0_document_list[0];
                _mail_subject = "[HRS] : สร้าง/แก้ไขเอกสาร - " + _temp_u0.emp_name_th;
                _mail_body = _serviceMail.VacationCreateBody(_temp_u0);
                _serviceMail.SendHtmlFormattedEmail(_temp_u0.emp_mail_list, _mail_subject, _mail_body);
            }
        }
        //var model = JsonConvert.DeserializeObject<List<MatrixModel.RootObject>>(json);
        //var contact = JSON.parse(jsonIn);

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion for employee

    #region for approver
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetAllApprove(int emp_idx)
    {
        u0_document_detail _u0_doc = new u0_document_detail();
        // set data
        _u0_doc.emp_idx = emp_idx;
        _data_vacation.u0_document_list = new u0_document_detail[1];
        _data_vacation.u0_document_list[0] = _u0_doc;
        // convert to xml
        _xml_in = _funcTool.convertJsonToXml(_funcTool.convertObjectToJson(_data_vacation));
        // execute and return
        _ret_val = _serviceExec.actionExec("masConn", "data_vacation", "service_vacation", _xml_in, 21); // return w/ json

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetApproveStatus(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_vacation", "service_vacation", _xml_in, 12); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_vacation = (data_vacation)_funcTool.convertXmlToObject(typeof(data_vacation), _local_xml);
            if(_data_vacation.return_code == 0)
            {
                // create mail body
                u0_document_detail _temp_u0 = _data_vacation.u0_document_list[0];
                _mail_subject = "[HRS] : ผลการอนุมัติเอกสาร - " + _temp_u0.emp_name_th;
                _mail_body = _serviceMail.VacationApproveBody(_temp_u0);
                _serviceMail.SendHtmlFormattedEmail(_temp_u0.emp_mail_list, _mail_subject, _mail_body);
            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetApprove_Cancel(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_vacation", "service_vacation", _xml_in, 13); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_vacation = (data_vacation)_funcTool.convertXmlToObject(typeof(data_vacation), _local_xml);
            if (_data_vacation.return_code == 0)
            {
                // create mail body
                u0_document_detail _temp_u0 = _data_vacation.u0_document_list[0];
                _mail_subject = "[HRS] : ผลการอนุมัติเอกสาร - " + _temp_u0.emp_name_th;
                _mail_body = _serviceMail.VacationApproveBody(_temp_u0);
                _serviceMail.SendHtmlFormattedEmail(_temp_u0.emp_mail_list, _mail_subject, _mail_body);
            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion for approver

    #region employee stat
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetMyStat(int emp_idx)
    {
        u0_document_detail _u0_doc = new u0_document_detail();
        // set data
        _u0_doc.emp_idx = emp_idx;
        _data_vacation.u0_document_list = new u0_document_detail[1];
        _data_vacation.u0_document_list[0] = _u0_doc;
        // convert to xml
        _xml_in = _funcTool.convertJsonToXml(_funcTool.convertObjectToJson(_data_vacation));
        // execute and return
        _ret_val = _serviceExec.actionExec("masConn", "data_vacation", "service_vacation", _xml_in, 30); // return w/ json

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion employee stat


    #region employee stat mobile
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetMyStat_Mobile(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_vacation", "service_vacation", _xml_in, 31); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_vacation = (data_vacation)_funcTool.convertXmlToObject(typeof(data_vacation), _local_xml);
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    #endregion employee stat mobile

}
