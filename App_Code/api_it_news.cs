﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for api_it_news
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_it_news : System.Web.Services.WebService
{

    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = "";
    string _mail_subject = "";
    string _mail_body = "";



    string sent_mailitnews = "";
    string sent_contentnews = "";
    string sent_typenews = "";
    string sent_titlenews = "";
    string _hasfile = "";
    string _check_sentmail = "";
    int check_emp_sentmail = 0;

    string u0_code_mailsoftware = "";
    string comment_mailsoftware = "";


    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    service_mail _serviceMail = new service_mail();
    data_it_news _data_it_news = new data_it_news();



    public api_it_news()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    #region INSERT DATA

    //insert master data type News
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SettypeNews(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_it_news", "service_master_itnews", _xml_in, 10); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert  data  News
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetITNews(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_it_news _data_itnews = new data_it_news();
            _data_itnews = (data_it_news)_funcTool.convertXmlToObject(typeof(data_it_news), _xml_in);

            string file_default = "";

            sent_titlenews = HttpUtility.UrlDecode(_data_itnews.u0_it_news_list[0].title_it_news);
            sent_contentnews = _data_itnews.u0_it_news_list[0].details_news;
            sent_typenews = _data_itnews.u0_it_news_list[0].name_type_news;
            _hasfile = _data_itnews.u0_it_news_list[0].hasfile;
            _check_sentmail = _data_itnews.u0_it_news_list[0].sentmail;
            file_default = _data_itnews.u0_it_news_list[0].pathfilepic;

            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_it_news", "service_itnews", _xml_in, 10); // return w/ json


            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_itnews = (data_it_news)_funcTool.convertXmlToObject(typeof(data_it_news), _local_xml);

            if (_data_itnews.return_code == "0")
            {

                // :: check ว่าผู้สร้างข่าวต้องการส่งเมล์ไหม??? ::::
                if (_check_sentmail == "1")
                {
                    // :: check ว่าผู้สร้างข่าวแนบไฟล์ภาพมาไหม??? ::::
                    if (_hasfile == "1")
                    {

                        string email = "msirirak4441@gmail.com";

                        string replyempcreate = "sirinyamod@hotmail.com";
                        string link_news_alert = "http://demo.taokaenoi.co.th/it-news";
                        string pathfile = "";

                        //string link_news_alert = "http://mas.taokaenoi.co.th/it-news";


                        u0_it_news_details _temp_u0 = _data_itnews.u0_it_news_list[0];
                        _mail_subject = "[MIS : IT News ] - แจ้งประชาสัมพันธ์ข่าวไอที";

                        _mail_body = _serviceMail.itnews_alert(_data_itnews.u0_it_news_list[0], link_news_alert, pathfile);

                        //_mail_body = _serviceMail.softwareLicenseCreate(_data_softwarelicense_devices.u0_softwarelicense_list[0], sent_mailsoftware, link_software);

                        //_mail_body = _serviceMail.softwareLicenseCreate(_temp_u0);
                        //_serviceMail.SendHtmlFormattedEmail(_temp_u0.emp_mail_list, _mail_subject, _mail_body);

                        _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);


                    }
                    // :: กรณีไม่มีไฟล์ภาพ
                    else

                    {


                        string email = "msirirak4441@gmail.com";

                        string replyempcreate = "sirinyamod@hotmail.com";
                        string link_news_alert = "http://demo.taokaenoi.co.th/it-news";
                        string pathfile = file_default;

                        //string link_news_alert = "http://mas.taokaenoi.co.th/it-news";


                        u0_it_news_details _temp_u0 = _data_itnews.u0_it_news_list[0];
                        _mail_subject = "[MIS : IT News ] - แจ้งประชาสัมพันธ์ข่าวไอที";

                        _mail_body = _serviceMail.itnews_alert(_data_itnews.u0_it_news_list[0], link_news_alert, pathfile);

                        //_mail_body = _serviceMail.softwareLicenseCreate(_data_softwarelicense_devices.u0_softwarelicense_list[0], sent_mailsoftware, link_software);

                        //_mail_body = _serviceMail.softwareLicenseCreate(_temp_u0);
                        //_serviceMail.SendHtmlFormattedEmail(_temp_u0.emp_mail_list, _mail_subject, _mail_body);

                        _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);


                    }



                }


            }


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }




    #endregion

    #region SELECT DATA

    //select master data type News
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GettypeNews(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_it_news", "service_master_itnews", _xml_in, 20); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data type News On dropdownlist
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GettypeNews_Ondropdownlist(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_it_news", "service_master_itnews", _xml_in, 21); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //select  data  News
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetITNews(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_it_news", "service_itnews", _xml_in, 20); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select  data  News
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetShowITNews(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_it_news", "service_itnews", _xml_in, 21); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

   

    //select  search  News
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchITNews(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_it_news", "service_itnews", _xml_in, 22); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //select  news default
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Getnewsdefault(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_it_news", "service_itnews", _xml_in, 23); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //select  data  News tab menu
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetShowITNewsMenu(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_it_news", "service_itnews", _xml_in, 24); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    #endregion

    #region DELETE DATA

    //delete master data type News
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Setdelete_typeNews(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_it_news", "service_master_itnews", _xml_in, 90); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete  data  News
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetdeleteNews(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_it_news", "service_itnews", _xml_in, 90); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    #endregion
}
