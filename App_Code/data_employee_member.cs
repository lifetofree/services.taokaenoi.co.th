﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_employee_member
/// </summary>
[Serializable]
[XmlRoot("data_employee_member")]
public class data_employee_member
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }
    [XmlElement("BoxEmp_member")]
    public Emp_member[] BoxEmp_member { get; set; }
}

[Serializable]
public class Emp_member
{
    [XmlElement("Member_EmpUser")]
    public string Member_EmpUser { get; set; }
    [XmlElement("Member_EmpPassword")]
    public string Member_EmpPassword { get; set; }
    [XmlElement("Member_EmpEmail")]
    public string Member_EmpEmail { get; set; }

}