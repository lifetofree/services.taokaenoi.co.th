﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for datama_online
/// </summary>
public class datama_online
{

    public string ReturnCode { get; set; }
    public string ReturnMsg { get; set; }
    public string ReturnU0Code { get; set; }
    public string ReturnORGTH { get; set; }
    public string ReturnDepTH { get; set; }
    public string ReturnSecTH { get; set; }
    public string ReturnEmpName { get; set; }
    public string ReturnAdmin { get; set; }
    public string ReturnStatus { get; set; }
    public string ReturnCreateDate { get; set; }
    public string ReturnEmail { get; set; }

    public M0_maList[] BoxM0_MaList { get; set; }
    public Ma_Online_List[] BoxMa_Online_List { get; set; }
    public Ma1_Online_List[] BoxMa1_Online_List { get; set; }
    public Ma2_Online_List[] BoxMa2_Online_List { get; set; }
    public Export_Ma_Online_List[] Export_BoxMa_Online_List { get; set; }
}

public class M0_maList
{
    public int m0idx { get; set; }
    public int typeidx { get; set; }
    public int CEmpIDX { get; set; }
    public int m0status { get; set; }
    public string ma_name { get; set; }
    public string detail { get; set; }
}

public class Ma_Online_List
{
    //ViewDevices
    public int u0_didx { get; set; }
    public string u0_code { get; set; }
    public string Search { get; set; }
    public string u0_acc { get; set; }
    public string EmpName { get; set; }
    public string name_m0_typedevice { get; set; }
    public string ma_name { get; set; }
    public int m0_tdidx { get; set; }

    //U0_Document
    public int u0idx { get; set; }
    public int deviceidx { get; set; }
    public int CEmpIDX { get; set; }
    public int unidx { get; set; }
    public int acidx { get; set; }
    public int staidx { get; set; }
    public int doc_status { get; set; }
    public int orgidx { get; set; }
    public int RdeptIDX { get; set; }
    public int RsecIDX { get; set; }
    public int AEmpIDX { get; set; }
    public string comment_it { get; set; }
    public string DocCode { get; set; }
    public string EmpCode { get; set; }
    public string Email { get; set; }
    public string PosNameTH { get; set; }
    public string MobileNo { get; set; }

    public int NOrgIDX { get; set; }
    public int NRDeptIDX { get; set; }
    public int NRSecIDX { get; set; }

    public string StatusDoc { get; set; }
    public string AdminName { get; set; }
    public string CreateDate { get; set; }
    public int m0idx { get; set; }
    public string comment_user { get; set; }
    public string status_name { get; set; }
    public string node_name { get; set; }
    public string actor_name { get; set; }
    public string comment { get; set; }
    public int u2idx { get; set; }
    public int nodidx { get; set; }
    public string node_desc { get; set; }
    public string EndDate { get; set; }
    public int IFSearchbetween { get; set; }

    public string OrgNameTH { get; set; }
    public string DeptNameTH { get; set; }
    public string SecNameTH { get; set; }

}

public class Ma1_Online_List
{
    public int u1idx { get; set; }
    public int u1status { get; set; }
    public int m0idx { get; set; }
    public int u0idx { get; set; }

}

public class Ma2_Online_List
{
    public int u2idx { get; set; }
    public int approveidx { get; set; }
    public int u2status { get; set; }
    public string comment_user { get; set; }

}

public class Export_Ma_Online_List
{
    public string รหัสเอกสาร { get; set; }
    public string เลขทะเบียนอุปกรณ์ { get; set; }
    public string วันที่สร้าง { get; set; }
    public string ผู้สร้าง { get; set; }
    public string ความคิดเห็นเจ้าหน้าที่ { get; set; }
    public string บริษัทผู้ถือครอง { get; set; }
    public string ฝ่ายผู้ถือครอง { get; set; }
    public string แผนกผู้ถือครอง { get; set; }
    public string ผู้ถือครอง { get; set; }
}