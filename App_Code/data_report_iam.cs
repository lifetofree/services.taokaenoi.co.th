﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_report_iam
/// </summary>
[Serializable]
[XmlRoot("data_report_iam")]
public class data_report_iam
{
    public int return_code { get; set; }
    public string return_msg { get; set; }
    [XmlElement("u0_report_iam_action")]
    public u0_report_iam[] u0_report_iam_action { get; set; }
}

#region u0_report_iam
[Serializable]
public class u0_report_iam
{
    [XmlElement("late_idx")]
    public int late_idx { get; set; }
    [XmlElement("return_idx")]
    public int return_idx { get; set; }
    [XmlElement("sick_idx")]
    public int sick_idx { get; set; }
    [XmlElement("sick_non_idx")]
    public int sick_non_idx { get; set; }
    [XmlElement("holi_idx")]
    public int holi_idx { get; set; }
    [XmlElement("birt_idx")]
    public int birt_idx { get; set; }
    [XmlElement("pro_idx")]
    public int pro_idx { get; set; }
    [XmlElement("take_idx")]
    public int take_idx { get; set; }
    [XmlElement("absen_idx")]
    public int absen_idx { get; set; }
    [XmlElement("absen_re_idx")]
    public int absen_re_idx { get; set; }
    [XmlElement("absen_late_idx")]
    public int absen_late_idx { get; set; }
    [XmlElement("absen_all_idx")]
    public int absen_all_idx { get; set; }
    [XmlElement("pb_absen_idx")]
    public int pb_absen_idx { get; set; }
    [XmlElement("at_over_idx")]
    public int at_over_idx { get; set; }
    [XmlElement("mat_befor_idx")]
    public int mat_befor_idx { get; set; }
    [XmlElement("mat_after_idx")]
    public int mat_after_idx { get; set; }
    [XmlElement("etc_idx")]
    public int etc_idx { get; set; }
    [XmlElement("time_idx")]
    public int time_idx { get; set; }


    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("pay_date")]
    public string pay_date { get; set; }
    [XmlElement("money_type")]
    public string money_type { get; set; }
    [XmlElement("money")]
    public string money { get; set; }
    [XmlElement("num")]
    public float num { get; set; }
    [XmlElement("unit")]
    public string unit { get; set; }
    [XmlElement("capacity")]
    public string capacity { get; set; }
    [XmlElement("etc")]
    public string etc { get; set; }
    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("temp_idx")]
    public int temp_idx { get; set; }
    [XmlElement("temp_name")]
    public int temp_name { get; set; }
    [XmlElement("month")]
    public int month { get; set; }
    [XmlElement("year")]
    public string year { get; set; }
    [XmlElement("type_action")]
    public int type_action { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("location_idx")]
    public int location_idx { get; set; }
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("resec_idx")]
    public int resec_idx { get; set; }
    [XmlElement("emptype_idx")]
    public int emptype_idx { get; set; }
    [XmlElement("emp_time")]
    public string emp_time { get; set; }

}
#endregion u0_report_iam