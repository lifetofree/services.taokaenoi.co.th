﻿using System;
using System.Xml.Serialization;


public class data_notification_mobile
{
    public int return_code { get; set; }
    public string return_msg { get; set; }

    public notification_detail[] notification_list { get; set; }

}

public class notification_detail
{
    public string emp_idx { get; set; }
    public string emp_code { get; set; }
    public string sum_itrepair { get; set; }
    public string sun_saprepair { get; set; }
    public string sum_gmailrepair { get; set; }
    public string sum_posrepair { get; set; }
    public string sum_leave { get; set; }
    public string sum_birepair { get; set; }
    public string sum_bprepair { get; set; }
    public string sum_roombook { get; set; }
    public string sum_carbook { get; set; }
    public string sum_ad { get; set; }
    public string sum_changerequest { get; set; }
    public string sum_visitor { get; set; }
    public string sum_temp1 { get; set; }
    public string sum_temp2 { get; set; }

    public int type_system { get; set; }
    public int type_select { get; set; }
}


