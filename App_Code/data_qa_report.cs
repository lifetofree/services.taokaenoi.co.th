﻿using System;
using System.Xml.Serialization;
/// <summary>
/// Summary description for data_roombooking
/// </summary>
/// 
[Serializable]
[XmlRoot("data_qa_report")]
public class data_qa_report
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public qa_report_m0_testdetail_detail[] qa_report_m0_testdetail_list { get; set; }

}

public class qa_report_m0_testdetail_detail
{

    public int test_detail_idx { get; set; }
    public string test_detail_name { get; set; }
    public int test_detail_status { get; set; }

}