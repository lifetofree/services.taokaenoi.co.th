﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

public class api_document_online : System.Web.Services.WebService
{
   string _xml_in = string.Empty;
   string _ret_val = string.Empty;
   string _local_xml = string.Empty;
   string _mail_subject = string.Empty;
   string _mail_body = string.Empty;
   function_tool _funcTool = new function_tool();
   service_execute _serviceExec = new service_execute();
   service_mail _serviceMail = new service_mail();
   Data_DocumentONLine dataDocOnline = new Data_DocumentONLine();

   public api_document_online()
   {
      //Uncomment the following line if using designed components
      //InitializeComponent();
   }

   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public void getDocumentExpDateEffective(string jsonIn)
   {
      if (jsonIn != null)
      {
         _xml_in = _funcTool.convertJsonToXml(jsonIn);
         _ret_val = _serviceExec.actionExec("masConn", "Data_DocumentONLine", "service_document_management", _xml_in, 299);
         _local_xml = _funcTool.convertJsonToXml(_ret_val);
         dataDocOnline = (Data_DocumentONLine)_funcTool.convertXmlToObject(typeof(Data_DocumentONLine), _local_xml);
         if (dataDocOnline.ReturnCode == "1")
         {
            for (int i = 0; i < dataDocOnline.BoxU0_DocumentONLine.Length; i++)
            {
               _mail_subject = "[DCC/Document Online]";
               _mail_body = _serviceMail.expDateEffective(dataDocOnline.BoxU0_DocumentONLine[i]);
               //_serviceMail.SendHtmlFormattedEmailFull(dataDocOnline.BoxU0_DocumentONLine[i].emp_email, "", "", _mail_subject, _mail_body);
            }
         }
      }
      Context.Response.Clear();
      Context.Response.ContentType = "application/json; charset=utf-8";
      Context.Response.Write(_ret_val);
   }

   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public void removeDocumentDateEffectiveTwoYear(string jsonIn)
   {
      if (jsonIn != null)
      {
         _xml_in = _funcTool.convertJsonToXml(jsonIn);
         _ret_val = _serviceExec.actionExec("masConn", "Data_DocumentONLine", "service_document_management", _xml_in, 298);
      }
      Context.Response.Clear();
      Context.Response.ContentType = "application/json; charset=utf-8";
      Context.Response.Write(_ret_val);
   }
}