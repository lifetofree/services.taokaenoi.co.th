﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_purchase
/// </summary>
public class data_purchase
{
    public string return_code { get; set; }
    public string return_msg { get; set; }
    public string return_email { get; set; }
    public string return_doccode { get; set; }

    public m0_type_purchase_equipment_detail[] m0_type_purchase_equipment_list { get; set; }
    public u0_purchase_equipment_details[] u0_purchase_equipment_list { get; set; }
    public u1_purchase_equipment_details[] u1_purchase_equipment_list { get; set; }
    public u2_purchase_equipment_details[] u2_purchase_equipment_list { get; set; }
    public summary_purchase_requisition_details[] summary_purchase_requisition_list { get; set; }
    public search_date_details[] search_date_list { get; set; }
    public master_spec_details[] master_spec_list { get; set; }
}


#region m0_type_purchase_equipment_detail

[Serializable]
public class m0_type_purchase_equipment_detail
{
    public int m0_equipment_type_idx { get; set; }
    public string name_equipment_type { get; set; }
    public int status_equipment { get; set; }
    public string name_purchase_type { get; set; }
    public int m0_purchase_type_idx { get; set; }
    public int status_purchase { get; set; }
    public int m0_unit_idx { get; set; }
 
}

#endregion

#region u0_purchase_equipment_details

[Serializable]
public class u0_purchase_equipment_details
{
    public int emp_idx_purchase { get; set; }
    public int u0_purchase_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int m0_node_idx { get; set; }
    public int quantity_equipment { get; set; }
    public int status_purchase { get; set; }
    public int m0_purchase_idx { get; set; }
    public int m0_equipment_idx { get; set; }
    public int org_idx_purchase { get; set; }
    public int sec_idx_purchase { get; set; }
    public int dept_idx_purchase { get; set; }
    public int pos_idx_purchase { get; set; }
    public int action_type { get; set; }
    public int costcenter_idx { get; set; }
    public int m0_price_idx { get; set; }
    public int m0_location_idx { get; set; }
    public string email_purchase { get; set; }
    public string details_purchase { get; set; }
    public string org_name_purchase { get; set; }
    public string sec_name_purchase { get; set; }
    public string drept_name_purchase { get; set; }
    public string pos_name_purchase { get; set; }
    public string fullname_purchase { get; set; }
    public string empcode_purchase { get; set; }
    public string name_equipment { get; set; }
    public string name_purchase { get; set; }
    public string node_name_purchase { get; set; }
    public string node_status_purchase { get; set; }
    public string actor_name_purchase { get; set; }
    public string time_purchasequipment { get; set; }
    public string date_purchasequipment { get; set; }
    public string document_code { get; set; }
    public string name_location { get; set; }
    public string number_io { get; set; }
    public string number_asset { get; set; }
    public string yearly { get; set; }
    public string mountly { get; set; }
    public string price_purchase { get; set; }
    public string costcenter_no { get; set; }
    public string numberphone { get; set; }
    public int numrow { get; set; }
    public int emp_idx_director { get; set; }
    public bool _selected { get; set; }
    public string _expenditure { get; set; }



}

#endregion

#region u1_purchase_equipment_details

[Serializable]
public class u1_purchase_equipment_details
{
    public int u1_purchase_idx { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int status_node { get; set; }
    public int emp_idx_purchase { get; set; }
    public int emp_idx_approve { get; set; }
    public string comment_details { get; set; }
}

#endregion

#region summary_purchase_requisition_details

[Serializable]
public class summary_purchase_requisition_details
{
    public int u0_purchase_idx { get; set; }
    public int u1_purchase_idx { get; set; }
    public int u0_actor_idx { get; set; }
    public int u0_node_idx { get; set; }
    public int u0_node_status { get; set; }
    public int u1_actor_idx { get; set; }
    public int u1_node_idx { get; set; }
    public int u1_node_status { get; set; }
    public int m0_price_idx { get; set; }
    public int emp_approve { get; set; }

}


#endregion

#region search_date_details

[Serializable]
public class search_date_details
{
    public string single_date { get; set; }
    public string over_date { get; set; }
    public string less_date { get; set; }
    public string start_date { get; set; }
    public string end_date { get; set; }
    public int condition_date_type { get; set; }

}

#endregion

#region master_spec_details

[Serializable]
public class master_spec_details
{
    public int m0_spec_idx { get; set; }
    public string name_spec { get; set; }
    public string details_spec { get; set; }
    public int m0_equipment_idx { get; set; }
    public int status_spec { get; set; }
    public int action { get; set; }

}

#endregion

#region u2_purchase_equipment_details

[Serializable]
public class u2_purchase_equipment_details
{
    public int quantity_purchase { get; set; }
    public int spec_type_idx { get; set; }
    public int location_idx { get; set; }
    public int rpos_idx_purchase { get; set; }
    public int equipment_type { get; set; }
    public string details_spec { get; set; }
    public string expenditure { get; set; }
}

#endregion