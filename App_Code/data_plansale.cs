﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_plansale
/// </summary>
[Serializable]
[XmlRoot("data_plansale")]
public class data_plansale
{
    public int return_code { get; set; }
    public string return_msg { get; set; }
    [XmlElement("u0_plansale_list")]
    public u0_plansale_detail[] u0_plansale_list { get; set; }
    [XmlElement("u1_plansale_list")]
    public u1_plansale_detail[] u1_plansale_list { get; set; }
    [XmlElement("m0_leave_list")]
    public m0_leave_detail[] m0_leave_list { get; set; }
}

#region Plansale
[Serializable]
public class u0_plansale_detail
{
    [XmlElement("ps_u0_idx")]
    public int ps_u0_idx { get; set; }
    [XmlElement("m0_typelist_idx")]
    public int m0_typelist_idx { get; set; }
    [XmlElement("uidx")]
    public int uidx { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("ps_comment")]
    public string ps_comment { get; set; }
    [XmlElement("ps_work_detail")]
    public string ps_work_detail { get; set; }
    [XmlElement("ps_startdate")]
    public string ps_startdate { get; set; }
    [XmlElement("ps_enddate")]
    public string ps_enddate { get; set; }
    [XmlElement("u0_unidx")]
    public int u0_unidx { get; set; }
    [XmlElement("u0_acidx")]
    public int u0_acidx { get; set; }
    [XmlElement("ps_status")]
    public int ps_status { get; set; }
    [XmlElement("emp_create")]
    public int emp_create { get; set; }
    [XmlElement("m0_leavetype_idx")]
    public int m0_leavetype_idx { get; set; }
    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }
    [XmlElement("doc_status")]
    public int doc_status { get; set; }



    [XmlElement("ps_u1_idx")]
    public int ps_u1_idx { get; set; }
    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }
    [XmlElement("approve_status")]
    public int approve_status { get; set; }
    [XmlElement("approve_remark")]
    public string approve_remark { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }
    [XmlElement("actor_description")]
    public string actor_description { get; set; }
    [XmlElement("actor_status")]
    public int actor_status { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }
    [XmlElement("node_description")]
    public string node_description { get; set; }
    [XmlElement("node_status")]
    public int node_status { get; set; }

    [XmlElement("m0_status_idx")]
    public int m0_status_idx { get; set; }
    [XmlElement("status_name")]
    public string status_name { get; set; }
    [XmlElement("status_description")]
    public string status_description { get; set; }
    [XmlElement("status_node")]
    public int status_node { get; set; }

    [XmlElement("from_m0_noidx")]
    public int from_m0_noidx { get; set; }
    [XmlElement("node_dec")]
    public string node_dec { get; set; }
    [XmlElement("to_m0_noidx")]
    public int to_m0_noidx { get; set; }
    [XmlElement("from_m0_acidx")]
    public int from_m0_acidx { get; set; }
    [XmlElement("to_m0_acidx")]
    public int to_m0_acidx { get; set; }
    [XmlElement("node_m0_staidx")]
    public int node_m0_staidx { get; set; }

    [XmlElement("M0LeaveTypeIDX")]
    public int M0LeaveTypeIDX { get; set; }
    [XmlElement("LeaveStart")]
    public string LeaveStart { get; set; }
    [XmlElement("LeaveEnd")]
    public string LeaveEnd { get; set; }
    [XmlElement("LeaveComment")]
    public string LeaveComment { get; set; }
    [XmlElement("emp_shift")]
    public int emp_shift { get; set; }
    [XmlElement("M0LeaveTypeName")]
    public string M0LeaveTypeName { get; set; }
    [XmlElement("TypeWork")]
    public string TypeWork { get; set; }

    [XmlElement("type_action")]
    public int type_action { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("year")]
    public int year { get; set; }
    [XmlElement("month")]
    public int month { get; set; }
    [XmlElement("tel")]
    public string tel { get; set; }
    [XmlElement("email")]
    public string email { get; set; }
    [XmlElement("day_off")]
    public string day_off { get; set; }
    [XmlElement("day_off_shift")]
    public string day_off_shift { get; set; }
}

[Serializable]
public class u1_plansale_detail
{
    [XmlElement("ps_u1_idx")]
    public int ps_u1_idx { get; set; }
    [XmlElement("ps_u0_idx")]
    public int ps_u0_idx { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }
    [XmlElement("approve_status")]
    public int approve_status { get; set; }
    [XmlElement("approve_remark")]
    public string approve_remark { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("actor_name")]
    public string actor_name { get; set; }
    [XmlElement("node_name")]
    public string node_name { get; set; }
    [XmlElement("status_name")]
    public string status_name { get; set; }
    [XmlElement("comment_approve")]
    public string comment_approve { get; set; }

    [XmlElement("_uidx")]
    public int _uidx { get; set; }
    [XmlElement("_approve")]
    public int _approve { get; set; }
    [XmlElement("_approve_no")]
    public int _approve_no { get; set; }
    [XmlElement("_approve_edit")]
    public int _approve_edit { get; set; }
    [XmlElement("_EmpIDX")]
    public int _EmpIDX { get; set; }
    [XmlElement("_EmpIDX_user")]
    public int _EmpIDX_user { get; set; }
    [XmlElement("_Empcode_user")]
    public int _Empcode_user { get; set; }
    [XmlElement("u0_unidx")]
    public int u0_unidx { get; set; }
    [XmlElement("u0_acidx")]
    public int u0_acidx { get; set; }
}

[Serializable]
public class m0_leave_detail
{
    [XmlElement("type_midx")]
    public int type_midx { get; set; }
    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("shift_midx")]
    public int shift_midx { get; set; }
    [XmlElement("shift_name")]
    public string shift_name { get; set; }
}

#endregion Plansale