﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Net.NetworkInformation;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for api_employee
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_statal : System.Web.Services.WebService
{
   string _xml_in = "";
   string _ret_val = "";
   function_tool _funcTool = new function_tool();
   service_execute _serviceExec = new service_execute();
   data_statal _data_statal = new data_statal();

   public api_statal()
   {
      //Uncomment the following line if using designed components 
      //InitializeComponent(); 
   }

   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public void getAllIPAddresses()
   {
      string jsonIn = "{'data_statal':{'statal_service_m0_ipaddress_action':{'m0_ipaddress_idx':'0','ipaddress_name':'0','ipaddress_status':'0'}}}";
      _xml_in = _funcTool.convertJsonToXml(jsonIn);
      _ret_val = _serviceExec.actionExec("misConn", "data_statal", "service_statal_ipaddress", _xml_in, 20);
      Context.Response.Clear();
      Context.Response.ContentType = "application/json; charset=utf-8";
      Context.Response.Write(_ret_val);
   }

   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public void pingAllIPAddresses()
   {
      string jsonIn = "{'data_statal':{'statal_service_m0_ipaddress_action':{'m0_ipaddress_idx':'0','ipaddress_name':'0','ipaddress_status':'0'}}}";
      _xml_in = _funcTool.convertJsonToXml(jsonIn);
      _ret_val = _serviceExec.actionExec("misConn", "data_statal", "service_statal_ipaddress", _xml_in, 20);
      var ipAddresses = String.Empty;
      JObject json = JObject.Parse(_ret_val);
      List<object> objLists = new List<object>();
      if (json["data_statal"]["statal_service_m0_ipaddress_action"].Count() == 1)
      {
         ipAddresses = json["data_statal"]["statal_service_m0_ipaddress_action"]["ipaddress_name"].ToString();
         Ping ping = new Ping();
         PingReply reply = ping.Send(ipAddresses, 5000);
         if (reply.Status != IPStatus.Success)
         {
            objLists.Add(new
            {
               address = ipAddresses.ToString(),
               status = reply.Status.ToString()
            });
         }
      }
      else
      {
         var j = 1;
         for (var i = 0; i < json["data_statal"]["statal_service_m0_ipaddress_action"].Count(); i++)
         {
            if (j == json["data_statal"]["statal_service_m0_ipaddress_action"].Count())
            {
               ipAddresses += json["data_statal"]["statal_service_m0_ipaddress_action"][i]["ipaddress_name"];
            }
            else
            {
               ipAddresses += json["data_statal"]["statal_service_m0_ipaddress_action"][i]["ipaddress_name"] + ",";
            }
            j++;
         }
         string[] ipAddressSplit = ipAddresses.Split(',');
         Ping ping = new Ping();
         for (var k = 0; k < ipAddressSplit.Length; k++)
         {
            PingReply reply = ping.Send(ipAddressSplit[k].ToString(), 5000);
            if (reply.Status != IPStatus.Success)
            {
               objLists.Add(new
               {
                  address = ipAddressSplit[k].ToString(),
                  status = reply.Status.ToString()
               });
            }
         }

      }
      var jsonSerialiser = new JavaScriptSerializer();
      var jsons = jsonSerialiser.Serialize(objLists);
      Context.Response.Clear();
      Context.Response.ContentType = "application/json; charset=utf-8";
      Context.Response.Write(jsons);
   }

   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public void pingSomeIPAddress(string ipaddress)
   {
      if (ipaddress != null)
      {
         string jsonIn = "{'data_statal':{'statal_service_m0_ipaddress_action':{'m0_ipaddress_idx':'0','ipaddress_name':'" + ipaddress + "','ipaddress_status':'0'}}}";
         _xml_in = _funcTool.convertJsonToXml(jsonIn);
         _ret_val = _serviceExec.actionExec("misConn", "data_statal", "service_statal_ipaddress", _xml_in, 21);
         JObject json = JObject.Parse(_ret_val);
         List<object> objLists = new List<object>();
         if (json["data_statal"]["return_msg"].ToString() != "null")
         {
            var ipAddresses = json["data_statal"]["statal_service_m0_ipaddress_action"]["ipaddress_name"].ToString();
            Ping ping = new Ping();
            PingReply reply = ping.Send(ipAddresses, 5000);
            objLists.Add(new
            {
               address = ipAddresses.ToString(),
               status = reply.Status.ToString()
            });
         }
         else
         {
            objLists.Add(new
            {
               address = ipaddress,
               status = "Address is not found in database."
            });
         }
         var jsonSerialiser = new JavaScriptSerializer();
         var jsons = jsonSerialiser.Serialize(objLists);
         Context.Response.Clear();
         Context.Response.ContentType = "application/json; charset=utf-8";
         Context.Response.Write(jsons);
      }
   }

   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public void pingAllIPAddressesNotify()
   {
      string jsonIn = "{'data_statal':{'statal_service_m0_ipaddress_action':{'m0_ipaddress_idx':'0','ipaddress_name':'0','ipaddress_status':'0'}}}";
      _xml_in = _funcTool.convertJsonToXml(jsonIn);
      _ret_val = _serviceExec.actionExec("misConn", "data_statal", "service_statal_ipaddress", _xml_in, 20);
      var ipAddresses = String.Empty;
      JObject json = JObject.Parse(_ret_val);
      List<object> objLists = new List<object>();
      var texts = String.Empty;
      if (json["data_statal"]["statal_service_m0_ipaddress_action"].Count() == 1)
      {
         ipAddresses = json["data_statal"]["statal_service_m0_ipaddress_action"]["ipaddress_name"].ToString();
         Ping ping = new Ping();
         PingReply reply = ping.Send(ipAddresses, 5000);
         if (reply.Status != IPStatus.Success)
         {
            texts += "\n";
            texts += "Address: " + ipAddresses.ToString();
            texts += "\n";
            texts += "Status: " + reply.Status.ToString();
         }
      }
      else
      {
         var j = 1;
         for (var i = 0; i < json["data_statal"]["statal_service_m0_ipaddress_action"].Count(); i++)
         {
            if (j == json["data_statal"]["statal_service_m0_ipaddress_action"].Count())
            {
               ipAddresses += json["data_statal"]["statal_service_m0_ipaddress_action"][i]["ipaddress_name"];
            }
            else
            {
               ipAddresses += json["data_statal"]["statal_service_m0_ipaddress_action"][i]["ipaddress_name"] + ",";
            }
            j++;
         }
         string[] ipAddressSplit = ipAddresses.Split(',');
         Ping ping = new Ping();
         for (var k = 0; k < ipAddressSplit.Length; k++)
         {
            PingReply reply = ping.Send(ipAddressSplit[k].ToString(), 5000);
            if (reply.Status != IPStatus.Success)
            {
               texts += "\n";
               texts += "Address: " + ipAddressSplit[k].ToString();
               texts += "\n";
               texts += "Status: " + reply.Status.ToString();
               texts += "\n";
               texts += "----------";
            }
         }

      }
      Context.Response.Clear();
      Context.Response.ContentType = "text/html; charset=utf-8";
      Context.Response.Write(texts);
   }

}