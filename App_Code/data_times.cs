using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_times")]
public class data_times
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_idx")]
    public int return_idx { get; set; }

    public finger_template_detail[] finger_template_list { get; set; }
    public search_template_detail[] search_template_list { get; set; }

    public finger_machine_detail[] finger_machine_list { get; set; }
    public search_machine_detail[] search_machine_list { get; set; }

    public tm_place_detail[] tm_place_list { get; set; }
    public search_place_detail[] search_place_list { get; set; }

    public tm_location_detail[] tm_location_list { get; set; }
    public search_location_detail[] search_location_list { get; set; }

    public tm_zone_detail[] tm_zone_list { get; set; }
    public search_zone_detail[] search_zone_list { get; set; }

    public tm_group_detail[] tm_group_list { get; set; }
    public search_group_detail[] search_group_list { get; set; }

    public tm_group_detail_m2[] tm_group_list_m2 { get; set; }
    public search_group_detail_m2[] search_group_list_m2 { get; set; }

    public tm_machine_admin_detail[] tm_machine_admin_list { get; set; }

    public tm_finger_time_detail[] tm_finger_time_list { get; set; }
    public search_finger_time_detail[] search_finger_time_list { get; set; } 

    public tm_plant_detail[] tm_plant_list { get; set; }
    public search_tm_plant_detail[] search_tm_plant_list { get; set; }
}

#region template
public class finger_template_detail
{
    public int m0_idx { get; set; }
    public int emp_idx { get; set; }
    public string emp_code { get; set; }
    public string index0 { get; set; }
    public string index1 { get; set; }
    public string index2 { get; set; }
    public string index3 { get; set; }
    public string index4 { get; set; }
    public string index5 { get; set; }
    public string index6 { get; set; }
    public string index7 { get; set; }
    public string index8 { get; set; }
    public string index9 { get; set; }
    public int template_status { get; set; }
    public string createdate { get; set; }
    public string updatedate { get; set; }

    public int machine_idx { get; set; }
    public string ip_host { get; set; }

    public int emp_idx_action { get; set; }
}

public class search_template_detail
{
    public string s_m0_idx { get; set; }
    public string s_emp_idx { get; set; }
    public string s_emp_code { get; set; }
    public string s_org_idx { get; set; }
    public string s_rdept_idx { get; set; }
    public string s_rsec_idx { get; set; }
    public string s_plant_idx { get; set; }
}
#endregion template

#region machine
public class finger_machine_detail
{
    public int m0_idx { get; set; }
    public string ip_host { get; set; }
    public string machine_name { get; set; }
    public int zone_idx { get; set; }
    public int place_idx { get; set; }
    public int loc_idx { get; set; }
    public int rsec_idx { get; set; }
    public int flag_attendance { get; set; }
    public int machine_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }

    public string zone_name { get; set; }
    public string place_name { get; set; }
    public string loc_name { get; set; }
    public string sec_name_th { get; set; }
    public string dept_name_th { get; set; }
    public int rdept_idx { get; set; }
    public string org_name_th { get; set; }
    public int org_idx { get; set; }

    public int template_idx { get; set; }
}

public class search_machine_detail
{
    public string s_m0_idx { get; set; }
    public string s_ip_host { get; set; }
    public string s_machine_name { get; set; }
    public string s_zone_idx { get; set; }
    public string s_place_idx { get; set; }
    public string s_loc_idx { get; set; }
    public string s_org_idx { get; set; }
    public string s_rdept_idx { get; set; }
    public string s_rsec_idx { get; set; }
    public string s_machine_status { get; set; }
}
#endregion machine

#region place
public class tm_place_detail
{
    public int m0_idx { get; set; }
    public string place_name { get; set; }
    public int place_status { get; set; }
}

public class search_place_detail
{
    public string s_m0_idx { get; set; }
    public string s_place_name { get; set; }
    public string s_place_status { get; set; }
}
#endregion place

#region location
public class tm_location_detail
{
    public int m0_idx { get; set; }
    public string location_name { get; set; }
    public int location_status { get; set; }
}

public class search_location_detail
{
    public string s_m0_idx { get; set; }
    public string s_location_name { get; set; }
    public string s_location_status { get; set; }
}
#endregion location

#region zone
public class tm_zone_detail
{
    public int m0_idx { get; set; }
    public string zone_name { get; set; }
    public int zone_status { get; set; }
}

public class search_zone_detail
{
    public string s_m0_idx { get; set; }
    public string s_zone_name { get; set; }
    public string s_zone_status { get; set; }
}
#endregion zone

#region group
public class tm_group_detail
{
    public int m0_idx { get; set; }
    public string group_name { get; set; }
    public int plant_idx { get; set; }
    public int group_status { get; set; }

    public string plant_name { get; set; }
}

public class search_group_detail
{
    public string s_m0_idx { get; set; }
    public string s_group_name { get; set; }
    public string s_plant_idx { get; set; }
    public string s_plant_name { get; set; }
    public string s_group_status { get; set; }
}

public class tm_group_detail_m2
{
    public int m2_idx { get; set; }
    public int m0_idx { get; set; }
    public int rpos_idx { get; set; }
    public int flag_priority { get; set; }
    public int approve_status { get; set; }

    public string pos_name_th { get; set; }
    public string sec_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string org_name_th { get; set; }
}

public class search_group_detail_m2
{
    public string s_m0_idx { get; set; }
    public int s_flag_priority { get; set; }
    public string s_approve_status { get; set; }
}
#endregion group

#region machine administrator
public class tm_machine_admin_detail
{
    public int m1_idx { get; set; }
    public int m0_idx { get; set; }
    public string ip_host { get; set; }
    public int emp_idx { get; set; }
    public int privilege_level { get; set; }
    public int admin_status { get; set; }
}
#endregion machine administrator

#region finger time
public class tm_finger_time_detail
{
    public int emp_idx { get; set; }
    public string emp_code { get; set; }
    public string finger_time { get; set; }
    public string machine_name { get; set; }
}

public class search_finger_time_detail
{
    public string s_emp_idx { get; set; }
    public string s_emp_code { get; set; }
    public string s_start_date { get; set; }
    public string s_end_date { get; set; }
    public string s_machine_idx { get; set; }
    public string s_machine_ip { get; set; }
}
#endregion finger time

#region plant list
public class tm_plant_detail
{
    public int plant_idx { get; set; }
    public string plant_name { get; set; }
    public int flag_visitor { get; set; }
    public int plant_status { get; set; }
}

public class search_tm_plant_detail
{
    public string s_plant_idx { get; set; }
    public string s_plant_name { get; set; }
    public string s_flag_visitor { get; set; }
    public string s_plant_status { get; set; }
}
#endregion plant list