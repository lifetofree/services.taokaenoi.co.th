﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for data_networklayout
/// </summary>
public class data_networklayout
{
    public string ReturnCode { get; set; }
    public string ReturnMsg { get; set; }
    public NWLList[] BoxnwlList { get; set; }
    public ExportNWLList[] Export_BoxNWL { get; set; }

}

public class NWLList
{
    public int REIDX { get; set; }
    public int u0idx { get; set; }

    public int row_ { get; set; }
    public int cell_ { get; set; }
    public int status_active { get; set; }
    public int category_idx { get; set; }
    public int type_idx { get; set; }
    public int countdown_npw { get; set; }
    public int countdown_rjn { get; set; }
    public int countdown_mtt { get; set; }
    public int countdown_idc { get; set; }
    public int place_idx { get; set; }


    public string register_number { get; set; }
    public string no_regis { get; set; }
    public string CreateDate { get; set; }
    public string DateDown { get; set; }
    public string down_desciption { get; set; }
    public string TimeDown { get; set; }

    public string img_name { get; set; }
    public string ip_address { get; set; }

    public string serial_number { get; set; }
    public string category_name { get; set; }
    public string brand_name { get; set; }
    public string generation_name { get; set; }
    public string type_name { get; set; }
    public string name_npw { get; set; }
    public string name_rjn { get; set; }
    public string name_idc { get; set; }
    public string name_mtt { get; set; }
    public string place_name { get; set; }

    public int FLIDX { get; set; }
    public int LocIDX { get; set; }
    public int BUIDX { get; set; }

    public string Floor_name { get; set; }
    public string Locname { get; set; }
    public string room_name { get; set; }
    public string Search { get; set; }

    public string create_log { get; set; }
    public string end_log { get; set; }
    public int IFSearchbetween { get; set; }

}

public class ExportNWLList
{
    public string รหัสทะเบียนอุปกรณ์ { get; set; }
    public string สถานที่ { get; set; }
    public string ชนิดอุปกรณ์ { get; set; }
    public string วันที่Down { get; set; }
    public string เวลาที่Down { get; set; }
    public string IPAddress { get; set; }
    public string ชื่ออุปกรณ์ { get; set; }
    public string SerialNumber { get; set; }

}