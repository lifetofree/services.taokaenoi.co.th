﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.Configuration;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

public class api_carbookingtest : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    public api_carbookingtest()
    {
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0Place(string jsonIn)
    {

        if (jsonIn != null)
        {
            //    // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            //    // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbookingtest", "service_carbookingtest", _xml_in, 210); // return w/ json

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteCbkm0Place(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbookingtest", "service_carbookingtest", _xml_in, 910); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AddCbkm0Place(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbookingtest", "service_carbookingtest", _xml_in, 110); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void UpdateCbkm0Place(string jsonIn)
    {
    }
}
