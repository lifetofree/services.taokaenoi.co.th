﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for api_qmr_problem
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_qmr_problem : System.Web.Services.WebService
{

    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = String.Empty;
    string _mail_subject = "";
    string _mail_body = "";
    string webmaseter = "webmaster@taokaenoi.co.th";
    string email_store = "siriporn.a@taokaenoi.co.th,st_rjn@taokaenoi.co.th,rm_tkn@taokaenoi.co.th,supakit.t@taokaenoi.co.th";
    string email_planning = "planning_npw@taokaenoi.co.th,planning_rjn@taokaenoi.co.th,thanakorn.o@taokaenoi.co.th";
    string email_wwh = "nitipong.d@taokaenoi.co.th,st_namai@taokaenoi.co.th,intertradegroup@taokaenoi.co.th";


    service_mail _serviceMail = new service_mail();
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_qmr_problem _dtproblem = new data_qmr_problem();

    public api_qmr_problem()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void InsertMaster_Problem(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qmr_problem", "service_qmr_problem", _xml_in, 100); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void InsertSystem_Problem(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qmr_problem", "service_qmr_problem", _xml_in, 101); // return w/ json
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtproblem = (data_qmr_problem)_funcTool.convertXmlToObject(typeof(data_qmr_problem), _local_xml);

            if (_dtproblem.ReturnCode.ToString() == "0")
            {
                var emp_email = "";
                var unidx = _dtproblem.Boxu0_ProblemDocument[0].unidx;
                var doc_decision = _dtproblem.Boxu0_ProblemDocument[0].doc_decision;

                emp_email = _dtproblem.Boxu0_ProblemDocument[0].emp_email;


                if (unidx.ToString() == "6") // ส่งหา Supplier
                {
                    var email_sup = _dtproblem.Boxu0_ProblemDocument[0].email_sup;
                    var Email_QA = _dtproblem.Boxu0_ProblemDocument[0].Email_QA;

                    emp_email += "," + email_sup + "," + Email_QA + "," + email_store + "," + email_planning;

                }
                else if (unidx.ToString() == "4")
                {
                    emp_email += "," + email_wwh;
                }
                else if (unidx.ToString() == "9" && doc_decision.ToString() == "7") // เจ้าหน้าที่ QA Approve หลังจาก Sup ดำเนินการเสร็จ
                {
                    var email_sup = _dtproblem.Boxu0_ProblemDocument[0].email_sup;
                    var Email_HeadUser = _dtproblem.Boxu0_ProblemDocument[0].Email_HeadUser;
                    var Email_QA = _dtproblem.Boxu0_ProblemDocument[0].Email_QA;
                    var Email_HeadQA = _dtproblem.Boxu0_ProblemDocument[0].Email_HeadQA;
                    var Email_PUR = _dtproblem.Boxu0_ProblemDocument[0].Email_PUR;

                    emp_email += "," + email_sup + "," + Email_HeadUser + "," + Email_QA + "," + Email_HeadQA + "," + Email_PUR + "," + email_store + "," + email_planning;

                }
                else if (unidx.ToString() == "8") // Sup ส่งหาเจ้าหน้าที่ QA
                {
                    var email_sup = _dtproblem.Boxu0_ProblemDocument[0].email_sup;
                    var Email_QA = _dtproblem.Boxu0_ProblemDocument[0].Email_QA;
                    var Email_HeadQA = _dtproblem.Boxu0_ProblemDocument[0].Email_HeadQA;
                    var Email_PUR = _dtproblem.Boxu0_ProblemDocument[0].Email_PUR;

                    emp_email += "," + email_sup + "," + Email_QA + "," + Email_HeadQA + "," + Email_PUR + "," + email_store + "," + email_planning;

                }
                else if (unidx.ToString() == "9" && doc_decision.ToString() == "2") // Approve จบการดำเนินการ
                {

                    var Email_QA = _dtproblem.Boxu0_ProblemDocument[0].Email_QA;
                    var Email_HeadQA = _dtproblem.Boxu0_ProblemDocument[0].Email_HeadQA;
                    var Email_DirQA = _dtproblem.Boxu0_ProblemDocument[0].Email_DirQA;
                    var Email_PUR = _dtproblem.Boxu0_ProblemDocument[0].Email_PUR;

                    emp_email += "," + Email_QA + "," + Email_PUR + "," + Email_DirQA + "," + email_store + "," + email_planning;
                }
                
                if (emp_email.ToString() != "" && emp_email.ToString() != null)
                {
                    if (unidx.ToString() == "9" && (doc_decision.ToString() == "3" || doc_decision.ToString() == "8"))
                    {
                        _mail_subject = "[SDP-ระบบแจ้งปัญหาวัตถุดิบ Cancel] : " + _dtproblem.Boxu0_ProblemDocument[0].doc_code.ToString() + "-" + _dtproblem.Boxu0_ProblemDocument[0].detail_remark.ToString();
                    }
                    else if (unidx.ToString() == "9" && doc_decision.ToString() == "7")
                    {
                        _mail_subject = "[SDP-ระบบแจ้งปัญหาวัตถุดิบ Closed] : " + _dtproblem.Boxu0_ProblemDocument[0].doc_code.ToString() + "-" + _dtproblem.Boxu0_ProblemDocument[0].detail_remark.ToString();
                    }
                    else
                    {
                        _mail_subject = "[SDP-ระบบแจ้งปัญหาวัตถุดิบ] : " + _dtproblem.Boxu0_ProblemDocument[0].doc_code.ToString() + "-" + _dtproblem.Boxu0_ProblemDocument[0].detail_remark.ToString();
                    }
                    _mail_body = _serviceMail.QMR_ProblemCreateBody(_dtproblem.Boxu0_ProblemDocument[0], int.Parse(unidx.ToString()), int.Parse(doc_decision.ToString()), emp_email.ToString());
                    _serviceMail.SendHtmlFormattedEmailFull(webmaseter, "", emp_email.ToString(), _mail_subject, _mail_body);
                }
            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectMaster_Problem(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qmr_problem", "service_qmr_problem", _xml_in, 200); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SelectSystem_Problem(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qmr_problem", "service_qmr_problem", _xml_in, 201); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteMaster_Problem(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qmr_problem", "service_qmr_problem", _xml_in, 900); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtproblem = (data_qmr_problem)_funcTool.convertXmlToObject(typeof(data_qmr_problem), _local_xml);

            if (_dtproblem.ReturnMsg.ToString() == "0")
            {
                var emp_email = "";
                var email_sup = _dtproblem.Boxu0_ProblemDocument[0].email_sup;
                var Email_PUR = _dtproblem.Boxu0_ProblemDocument[0].Email_PUR;

                emp_email = email_sup + "," + Email_PUR;

                _mail_subject = "[SDP-ระบบแจ้งปัญหาวัตถุดิบ] : Reset Password Supplier";
                _mail_body = _serviceMail.QMR_ProblemRepasswordBody(_dtproblem.Boxu0_ProblemDocument[0], emp_email.ToString());
                _serviceMail.SendHtmlFormattedEmailFull(emp_email.ToString(), "", emp_email, _mail_subject, _mail_body);


            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

}
