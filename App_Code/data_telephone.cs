using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_telephone")]
public class data_telephone
{
    [XmlElement("return_code")]
    public string return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("u0_telephone_list")]
    public u0_telephone_detail[] u0_telephone_list { get; set; }

    [XmlElement("u1_telephone_list")]
    public u1_telephone_detail[] u1_telephone_list { get; set; }

    [XmlElement("u2_telephone_list")]
    public u2_telephone_detail[] u2_telephone_list { get; set; }
}

[Serializable]
public class u0_telephone_detail
{
    public int u0_idx { get; set; }
    public string site_name { get; set; }
    public string site_phone { get; set; }
    public int site_status { get; set; }
}

[Serializable]
public class u1_telephone_detail
{
    public int u1_idx { get; set; }
    public int u0_idx { get; set; }
    public string dept_name { get; set; }
    public int dept_status { get; set; }
}

[Serializable]
public class u2_telephone_detail
{
    public int u2_idx { get; set; }
    public int u1_idx { get; set; }
    public string telephone_name { get; set; }
    public string telephone_number { get; set; }
    public int telephone_status { get; set; }
}