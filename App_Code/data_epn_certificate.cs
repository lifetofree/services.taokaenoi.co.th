﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_sf_certificate
/// </summary>
[Serializable]
[XmlRoot("data_certificate_doc")]
public class data_certificate_doc
{
    [XmlElement("ReturnCode")]
    public int ReturnCode { get; set; }
    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }


    public certificate_doc_detail[] certificate_doc_list { get; set;}

    public certificate_doc_detail_u1[] certificate_doc_list_u1 { get; set; }


    public certificate_doc_detail_u2[] certificate_doc_list_u2 { get; set; }


    public certificate_doc_detail_notification_m1[] certificate_doc_list_notification_m1 { get; set; }


    public certificate_doc_detail_frequency_m0[] certificate_doc_list_frequency_m0 { get; set; }

    public certificate_doc_detail_permission[] certificate_doc_list_permission { get; set; }
}


#region u0
public class certificate_doc_detail
{
    public int cemp_idx { get; set; }
    public int uemp_idx { get; set; }
    public int condition { get; set; }
    public string update_date { get; set; }
    public string create_date { get; set; }
    public int emp_idx { get; set; }
    public int type_idx { get; set; }
    public string type_name { get; set; }
    public int premission_idx { get; set; }
    public int type_status { get; set; }
    public string status_type_Name { get; set; }
    public int m0_supidx { get; set; }
    public string sup_name { get; set; }
    public string email { get; set; }
    public int type_system { get; set; }
    public int LocIDX { get; set; }
    public string LocName { get; set; }
    public int LocStatus { get; set; }
    public int permission_idx { get; set; }
    public string permission_name { get; set; }
    public int frequency_idx { get; set; }
    public string frequency_name { get; set; }
    public int frequency_status { get; set; }
    public int month_idx { get; set; }
    public string month_name_th { get; set; }
    public int month_condition { get; set; }
    public int m0_notification_idx { get; set; }
    public string m0_notification_name { get; set; }
    public int notification_status { get; set; }
    public int m1_notification_idx { get; set; }
    public string m1_notification_name { get; set; }
    public int m1_notification_status { get; set; }
    public int u0doc_idx { get; set; }
    public int date_condition { get; set; }
    public string certificate_name { get; set; }
    public string certificate_detail { get; set; }
    public string contact { get; set; }
    public string phone_no { get; set; }
    public string remark { get; set; }
    public string date_start { get; set; }
    public string date_expire { get; set; }
    public int notification_idx { get; set; }
    public int u0_status { get; set; }
    public int u2doc_idx { get; set; }
    public int u2_status { get; set; }
    public int u1_status { get; set; }
    public string status_name { get; set; }
    public int org_idx { get; set; }
    public string emp_name_th { get; set; }
    public string sec_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string org_name_th { get; set; }
    public int rsec_idx { get; set; }
    public int rel_idx { get; set; }
   


}
#endregion


#region u1
[Serializable]
public class certificate_doc_detail_u1
{

    
    public int u1doc_idx { get; set; }
    public int u0doc_idx { get; set; }
    public int month_idx { get; set; }
    public int m1_notification_idx { get; set; }
    public int u1_status { get; set; }
    public string update_date { get; set; }
    public string create_date { get; set; }
    public int cemp_idx { get; set; }
    public int condition { get; set; }
    public string month_name_th { get; set; }


}
# endregion

#region u2
[Serializable]
public class certificate_doc_detail_u2
{
  
    public int u2doc_idx { get; set; }
    public int u0doc_idx { get; set; }
    public int date_condition { get; set; }
    public int u2_status { get; set; }
    public string update_date { get; set; }
    public string create_date { get; set; }
    public int cemp_idx { get; set; }



}

#endregion


#region m1
public class certificate_doc_detail_notification_m1
{

    public int m1_notification_idx { get; set; }
    public string m1_notification_name { get; set; }
    public int m0_notification_idx { get; set; }
    public int m1_notification_status { get; set; }
    public string update_date { get; set; }
    public string create_date { get; set; }
    public int cemp_idx { get; set; }
}

#endregion


#region frequency_m0

public class certificate_doc_detail_frequency_m0
{


    public int frequency_idx { get; set; }
    
    public string frequency_name { get; set; }
    
    public int frequency_status { get; set; }

    public string update_date { get; set; }
    
    public string create_date { get; set; }
    
    public int cemp_idx { get; set; }

}
#endregion


#region  permission
public class certificate_doc_detail_permission
{

   
    public int permission_idx { get; set; }
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int rpos_idx { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string permission_name { get; set; }
    public string update_date { get; set; }
    public int permission_status { get; set; }
    public int admin_idx { get; set; }
    public int condition { get; set; }



}
#endregion
