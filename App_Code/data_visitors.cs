using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_visitors")]
public class data_visitors
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_idx")]
    public int return_idx { get; set; }
    [XmlElement("return_node_idx")]
    public int return_node_idx { get; set; }
    [XmlElement("return_actor_idx")]
    public int return_actor_idx { get; set; }

    public vm_visitors_type_detail_m0[] vm_visitors_type_list_m0 { get; set; }
    public search_visitor_type_detail[] search_visitor_type_list { get; set; }
    public vm_vehicle_type_detail_m0[] vm_vehicle_type_list_m0 { get; set; }
    public vm_visitor_card_detail_m0[] vm_visitor_card_list_m0 { get; set; }

    public vm_visitors_doc_detail_u0[] vm_visitors_doc_list_u0 { get; set; }
    public vm_visitors_doc_detail_u1[] vm_visitors_doc_list_u1 { get; set; }
    public vm_visitors_doc_detail_u2[] vm_visitors_doc_list_u2 { get; set; }
    public vm_visitors_doc_detail_u3[] vm_visitors_doc_list_u3 { get; set; }
    public vm_visitors_doc_detail_l0[] vm_visitors_doc_list_l0 { get; set; }

    public view_vm_visitors_doc_detail_u3[] view_vm_visitors_doc_list_u3 { get; set; }
    public vm_visitors_setperms_admin_detail_m0[] vm_visitors_setperms_admin_list_m0 { get; set; }
    public vm_visitors_menu_detail_m0[] vm_visitors_menu_list_m0 { get; set; }

    [XmlElement("vm_visitors_sendemail_list")]
    public vm_visitors_sendemail_detail[] vm_visitors_sendemail_list { get; set; }
    public vm_visitor_vehicle_special_detail_m0[] vm_visitor_vehicle_special_list_m0 { get; set; }
    public vm_visitor_vehicle_special_detail_l0[] vm_visitor_vehicle_special_list_l0 { get; set; }

    public vm_visitors_security_detail_m0[] vm_visitors_security_list_m0 { get; set; }
    public vm_visitors_board_detail[] vm_visitors_board_list { get; set; }

    public vm_visitors_report_search_detail[] vm_visitors_report_search_list { get; set; }
}

#region master data
public class vm_visitors_type_detail_m0
{
    public int m0_idx { get; set; }
    public string visitor_type_name { get; set; }
    public int flag_safety { get; set; }
    public int flag_loading { get; set; }
    public int type_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

public class search_visitor_type_detail
{
    public string s_m0_idx { get; set; }
    public string s_visitor_type_name { get; set; }
    public string s_type_status { get; set; }
}

public class vm_vehicle_type_detail_m0
{
    public int m0_idx { get; set; }
    public string vehicle_type_name { get; set; }
    public int type_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

public class vm_visitor_card_detail_m0
{
    public int m0_idx { get; set; }
    public string visit_card_no { get; set; }
    public string visit_card_serial { get; set; }
    public int visit_card_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

public class vm_visitor_vehicle_special_detail_m0
{
    public int m0_idx { get; set; }
    public string vehicle_plate_no { get; set; }
    public int vehicle_plate_province { get; set; }
    public int vehicle_state { get; set; }
    public int vehicle_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }

    public string vehicle_plate_province_name { get; set; }
    public int se_idx { get; set; }
}
#endregion master data

#region doc_u0
public class vm_visitors_doc_detail_u0
{
    public int u0_idx { get; set; }
    public int emp_idx { get; set; }
    public int visitor_type_idx { get; set; }
    public string visit_date { get; set; }
    public int visit_emp_idx { get; set; }
    public string visit_title { get; set; }
    public string visit_detail { get; set; }
    public int flag_safety { get; set; }
    public string safety_comment { get; set; }
    public int flag_vehicle { get; set; }
    public int flag_walkin { get; set; }
    public int node_idx { get; set; }
    public int actor_idx { get; set; }
    public int doc_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }

    public string emp_name_th { get; set; }
    public string visit_emp_name_th { get; set; }
    public int decision_idx { get; set; }
    public string visitor_type_name { get; set; }
    public string vehicle_type_name { get; set; }
    public string node_name { get; set; }
    public string actor_name { get; set; }
    public string doc_status_name { get; set; }
    public string mail_list { get; set; }   
    public int emp_idx_approve1 { get; set; }
    public string emp_approve1 { get; set; }
    public int emp_idx_approve2 { get; set; }
    public string emp_approve2 { get; set; }

    //start teppanop
    public string zmode { get; set; }
    public int rpos_idx { get; set; }
    public int zcount { get; set; }
    public int flag_loading { get; set; }
    public string s_idcard { get; set; }
    public string s_filter_keyword { get; set; }
    public int flag_arrived { get; set; }
    public string s_visit_name_th { get; set; }
    //end teppanop

    public int u1_idx { get; set; }
    public string visit_prefix { get; set; }    
    public string visit_firstname { get; set; }
    public string visit_lastname { get; set; }
    public string visit_identify_no { get; set; }
    public string visit_phone_no { get; set; }
    public string visit_card_no { get; set; }
    public string visit_card_ex_no { get; set; }

    public int u2_idx { get; set; }
    public int vehicle_type_idx { get; set; }
    public string vehicle_plate_no { get; set; }
    public int vehicle_plate_province { get; set; }
    public string vehicle_plate_province_name { get; set; }

    public int u3_idx { get; set; }
    public int group_m0_idx { get; set; }
    public string group_name { get; set; }
    public int approve_status { get; set; }
    public int group_status { get; set; }
}
#endregion doc_u0

#region  doc_u1
public class vm_visitors_doc_detail_u1
{
    public int u1_idx { get; set; }
    public int u0_idx { get; set; }
    public string visit_prefix { get; set; }
    public string visit_firstname { get; set; }
    public string visit_lastname { get; set; }
    public string visit_prefix_en { get; set; }
    public string visit_firstname_en { get; set; }
    public string visit_lastname_en { get; set; }
    public string visit_house_no { get; set; }
    public string visit_village_no { get; set; }
    public string visit_lane { get; set; }
    public string visit_road { get; set; }
    public string visit_district { get; set; }
    public string visit_amphure { get; set; }
    public string visit_province { get; set; }
    public int visit_identify_idx { get; set; }
    public string visit_identify_no { get; set; }
    public string visit_phone_no { get; set; }
    public string visit_card_no { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public string visit_card_ex_no { get; set; }
    public int flag_photo { get; set; }
}
#endregion doc_u1

#region  doc_u2
public class vm_visitors_doc_detail_u2
{
    public int u2_idx { get; set; }
    public int u0_idx { get; set; }
    public int vehicle_type_idx { get; set; }
    public string vehicle_plate_no { get; set; }
    public int vehicle_plate_province { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }

    public string vehicle_plate_province_name { get; set; }
}
#endregion doc_u2

#region  doc_u3
public class vm_visitors_doc_detail_u3
{
    public int u3_idx { get; set; }
    public int u0_idx { get; set; }
    public int group_m0_idx { get; set; }
    public int approve_status { get; set; }
    public int group_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}
#endregion doc_u3

#region doc_l0
public class vm_visitors_doc_detail_l0
{
    public int l0_idx { get; set; }
    public int u0_doc_idx { get; set; }
    public int emp_idx { get; set; }
    public int u0_node_idx { get; set; }
    public int u0_actor_idx { get; set; }
    public int m1_node_idx { get; set; }
    public string remark { get; set; }
    public string create_date { get; set; }

    public string emp_name_th { get; set; }
    public string node_name { get; set; }
    public string actor_name { get; set; }
    public string decision_name { get; set; }
}
#endregion doc_l0

#region  view_doc_u3
public class view_vm_visitors_doc_detail_u3
{
    public int u2_idx { get; set; }
    public int u0_idx { get; set; }
    public int group_m0_idx { get; set; }
    public int approve_status { get; set; }
    public int group_status { get; set; }
    public int rpos_idx { get; set; }
}
#endregion view_doc_u3

#region  vm_visitors_setperms_admin_m0
public class vm_visitors_setperms_admin_detail_m0
{
    public int permission_idx { get; set; }
    public int node_idx { get; set; }
    public int actor_idx { get; set; }
    public string discrption { get; set; }
    public int rpos_idx { get; set; }
    public int permission_status { get; set; }
    public string remark { get; set; }
    public int place_idx { get; set; }
}
#endregion vm_visitors_setperms_admin_m0

#region  vm_visitors_menu_m0
public class vm_visitors_menu_detail_m0
{
    public int m0_idx { get; set; }
    public string lb_menu { get; set; }
    public string menu { get; set; }
    public int zcount { get; set; }
}
#endregion vm_visitors_menu_m0

#region vm_visitor_vehicle_special_l0
public class vm_visitor_vehicle_special_detail_l0
{
    public int l0_idx { get; set; }
    public int vehicle_m0_idx { get; set; }
    public int se_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}
#endregion vm_visitor_vehicle_special_l0
#region sendemail
[Serializable]
public class vm_visitors_sendemail_detail
{
    [XmlElement("operation_status_id")]
    public string operation_status_id { get; set; }

    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("visitor_type_idx")]
    public int visitor_type_idx { get; set; }
    [XmlElement("visit_date")]
    public string visit_date { get; set; }
    [XmlElement("visit_emp_idx")]
    public int visit_emp_idx { get; set; }
    [XmlElement("flag_safety")]
    public int flag_safety { get; set; }
    [XmlElement("safety_comment")]
    public string safety_comment { get; set; }
    [XmlElement("flag_vehicle")]
    public int flag_vehicle { get; set; }
    [XmlElement("node_idx")]
    public int node_idx { get; set; }
    [XmlElement("actor_idx")]
    public int actor_idx { get; set; }
    [XmlElement("doc_status")]
    public int doc_status { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("visit_emp_name_th")]
    public string visit_emp_name_th { get; set; }
    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }
    [XmlElement("visitor_type_name")]
    public string visitor_type_name { get; set; }
    [XmlElement("vehicle_type_name")]
    public string vehicle_type_name { get; set; }
    [XmlElement("node_name")]
    public string node_name { get; set; }
    [XmlElement("actor_name")]
    public string actor_name { get; set; }
    [XmlElement("doc_status_name")]
    public string doc_status_name { get; set; }
    [XmlElement("mail_list")]
    public string mail_list { get; set; }
    [XmlElement("emp_idx_approve1")]
    public int emp_idx_approve1 { get; set; }
    [XmlElement("emp_approve1")]
    public string emp_approve1 { get; set; }
    [XmlElement("emp_idx_approve2")]
    public int emp_idx_approve2 { get; set; }
    [XmlElement("emp_approve2")]
    public string emp_approve2 { get; set; }

    //start teppanop

    [XmlElement("zmode")]
    public string zmode { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }
    [XmlElement("emp_approve_email1")]
    public string emp_approve_email1 { get; set; }
    [XmlElement("emp_approve_email2")]
    public string emp_approve_email2 { get; set; }
    [XmlElement("zstatus")]
    public string zstatus { get; set; }
    [XmlElement("emp_email")]
    public string emp_email { get; set; }
    [XmlElement("visit_emp_email")]
    public string visit_emp_email { get; set; }
    [XmlElement("flag_walkin")]
    public int flag_walkin { get; set; }
    //end teppanop  
}
#endregion sendemail

#region vm_visitors_security_detail_m0
public class vm_visitors_security_detail_m0
{
    public int u0_idx { get; set; }
    public string username { get; set; }
    public string password { get; set; }
    public string firstname { get; set; }
    public string lastname { get; set; }
    public int user_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}
#endregion vm_visitors_security_detail_m0

#region  board
public class vm_visitors_board_detail
{
    
    public int u2_idx { get; set; }
   
    public int u0_idx { get; set; }
    
    public string vehicle_plate_no { get; set; }
    
    public string visit_date { get; set; }
    
    public string zvisit_date { get; set; }
   
    public string zvisit_time { get; set; }
    
    public string emp_name_th { get; set; }
    
    public string visit_title { get; set; }
    
    public string visitor_type_name { get; set; }

    public string zstatus { get; set; }

    public string status_flag { get; set; }

}
#endregion board

#region report search
public class vm_visitors_report_search_detail
{
    public string s_date_type { get; set; }
    public string s_report_type { get; set; }
    public string s_start_date { get; set; }
    public string s_end_date { get; set; }
    public string s_report_org { get; set; }
    public string s_report_dept { get; set; }
    public string s_report_sec { get; set; }
    public string s_report_emp_idx { get; set; }
    public string s_visitor_type { get; set; }
    public string s_group_idx { get; set; }
    public string s_group_by { get; set; }
    public string s_order_by { get; set; }
}
#endregion report search