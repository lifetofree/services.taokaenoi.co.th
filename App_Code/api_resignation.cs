﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for api_resignation
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_resignation : System.Web.Services.WebService
{

    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = String.Empty;
    string _mail_subject = "";
    string _mail_body = "";
    string webmaseter = "webmaster@taokaenoi.co.th";

    service_mail _serviceMail = new service_mail();
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_resignation _dtresign = new data_resignation();

    public api_resignation()
    {

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_Topic(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 101); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_Question_Resignation(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 102); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_Question_ForWrite_Resignation(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 103); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_Resignation(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 104); // return w/ json
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtresign = (data_resignation)_funcTool.convertXmlToObject(typeof(data_resignation), _local_xml);

            if (_dtresign.ReturnCode.ToString() == "0")
            {
                var Email = _dtresign.Boxm0_ReasonExit[0].Email;
                var username = _dtresign.Boxm0_ReasonExit[0].FullNameTH;
                var Email_Admin = _dtresign.Boxm0_ReasonExit[0].Email_Admin;

                Email += "," + Email_Admin;

                _mail_subject = "[HR RESIGNATION/ระบบลาออก] : " + username;
                _mail_body = _serviceMail.ResignationCreateBody(_dtresign.Boxm0_ReasonExit[0]);
                _serviceMail.SendHtmlFormattedEmailFull(Email, "", Email, _mail_subject, _mail_body);

            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_Data_ExitInterview(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 105); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Insert_Exchange(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 106); // return w/ json
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtresign = (data_resignation)_funcTool.convertXmlToObject(typeof(data_resignation), _local_xml);

            if (_dtresign.ReturnCode.ToString() == "0")
            {
                var Email = _dtresign.Boxm0_ReasonExit[0].Email;
                var username = _dtresign.Boxm0_ReasonExit[0].FullNameTH;

                _mail_subject = "[HR RESIGNATION/ระบบลาออก/ส่งมอบงาน] : " + username;
                _mail_body = _serviceMail.ResignationExchangeBody(_dtresign.Boxm0_ReasonExit[0]);
                _serviceMail.SendHtmlFormattedEmailFull(Email, "", Email, _mail_subject, _mail_body);

            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Topic(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 201); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_TypeAnswer(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 203); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_TypeChoice(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 204); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_TypeQuestion(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 205); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_TypeAnswer_Resignation(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 206); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Question_Resignation(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 207); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_SubQuestion_Resignation(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 208); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_EditSubQuestion_Resignation(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 209); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_ReasonExit(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_DetailProfile(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 211); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_ASSETProfile(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 212); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_ShowDataList(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 213); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_ShowData_Asset(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 214); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_ShowData_Holder(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 215); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_ShowData_Detail(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 216); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Index_Approve(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 218); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Log(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 219); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Detail_Edit(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Detail_Answer(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 221); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Detail_Exchange(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 222); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Select_Sumlist(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 223); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Update_Resignation(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 301); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Update_Topic_Resignation(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 302); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Update_Approver(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 303); // return w/ json

            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtresign = (data_resignation)_funcTool.convertXmlToObject(typeof(data_resignation), _local_xml);

            if (_dtresign.ReturnCode.ToString() == "0")
            {
                var Email = _dtresign.Boxm0_ReasonExit[0].Email;
                var username = _dtresign.Boxm0_ReasonExit[0].FullNameTH;
                var Email_Admin = _dtresign.Boxm0_ReasonExit[0].Email_Admin;

                Email += "," + Email_Admin;


                _mail_subject = "[HR RESIGNATION/ระบบลาออก] : " + username;
                _mail_body = _serviceMail.ResignationCreateBody(_dtresign.Boxm0_ReasonExit[0]);
                _serviceMail.SendHtmlFormattedEmailFull(Email, "", Email, _mail_subject, _mail_body);

            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Update_Approver_Exchange(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 304); // return w/ json
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _dtresign = (data_resignation)_funcTool.convertXmlToObject(typeof(data_resignation), _local_xml);

            if (_dtresign.ReturnCode.ToString() == "0")
            {
                var Email = _dtresign.Boxm0_ReasonExit[0].Email;
                var username = _dtresign.Boxm0_ReasonExit[0].FullNameTH;

                _mail_subject = "[HR RESIGNATION/ระบบลาออก/ส่งมอบงาน] : " + username;
                _mail_body = _serviceMail.ResignationExchangeBody(_dtresign.Boxm0_ReasonExit[0]);
                _serviceMail.SendHtmlFormattedEmailFull(Email, "", Email, _mail_subject, _mail_body);

            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delete_Topic_Resignation(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 901); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void Delete_Subquestion_Resignation(string jsonIn)
    {

        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_resignation", "service_resignation", _xml_in, 902); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

   
}
