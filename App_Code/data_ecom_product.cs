﻿using System;
using System.Xml.Serialization;


public class data_ecom_product
{
    public int return_code { get; set; }
    public string return_msg { get; set; }


    public product_detail[] product_list { get; set; }
    public en_cart_detail[] en_cart_list { get; set; }
    public customer_detail[] customer_list { get; set; }
    public en_order_detail[] en_order_list { get; set; }
    //public en_ordertemp_detail[] en_ordertemp_list { get; set; }

}

public class product_detail
{

    public int id_product { get; set; }
    public int type_selection { get; set; }
    public string name_product { get; set; }
    public string description { get; set; }
    public string description_short { get; set; }
    public int id_supplier { get; set; }
    public int id_manufacturer { get; set; }
    public int id_category { get; set; }
    public string name_group_category { get; set; }
    public string price { get; set; }
    public double rate { get; set; }
    public string name_rate { get; set; }
    public int id_lang { get; set; }
    public string discount { get; set; }
    public string discount_type { get; set; }
    public string currency { get; set; }
    public int on_sale { get; set; }
    public string code { get; set; }
    public int id_psblog_blog { get; set; }
    public string count_down { get; set; }
    public int totalqty { get; set; }
    public int quantity { get; set; }
    public string img_subbanner_left { get; set; }
    public string img_subbanner_right { get; set; }
    public string price_excl { get; set; }
    public string price_incl { get; set; }
    public string priceproduct_total { get; set; }




}

public class en_cart_detail : product_detail
{
    public int id_cart { get; set; }
    public int id_cart_rule { get; set; }
    public int id_shop_group { get; set; }
    public int id_shop { get; set; }
    public int id_carrier { get; set; }
    public string delivery_option { get; set; }
    public int id_address_delivery { get; set; }
    public int id_address_invoice { get; set; }
    public int id_currency { get; set; }
    public int id_customer { get; set; }
    public int id_guest { get; set; }
    public string secure_key { get; set; }
    public int recyclable { get; set; }
    public int gift { get; set; }
    public string gift_message { get; set; }
    public int mobile_theme { get; set; }
    public int allow_seperated_package { get; set; }
    public string date_add { get; set; }
    public string date_upd { get; set; }
    public string checkout_session_data { get; set; }
    public int id_product_attribute { get; set; }
    public int id_customization { get; set; }
    //public int quantity { get; set; }
    public int quantity_total { get; set; }
    public int price_product_total { get; set; }
    public int price_product_total_tax { get; set; }
    public int price_shipping { get; set; }
    public string price_shipping_tax { get; set; }
    public string product_url_cart { get; set; }
    public string product_url { get; set; }
    public string price_discount { get; set; }
    public string price_discount_tax { get; set; }


    //promo discount in cart
    public string date_from { get; set; }
    public string date_to { get; set; }
    public string name_promo_discounts { get; set; }

    public int zamount {get;set;}
    public int gift_product {get;set;}
    public int minimum_amount {get;set;}
    public int totalqtydiscount {get;set;}
    public string type_name {get;set;} 
    public int priority {get;set;}
    public int promotionid {get;set;}
    public string promotionname {get;set;}
    public int pricedis {get;set;}
    //public int price{get;set;}
    //public double rate {get;set;}
    public int netsaletotal{get;set;}
    public int cart_subtotal_products_qty { get; set; }
    public double cart_subtotal_products_amount { get; set; }
    public double cart_subtotal_discount { get; set; }
    public double cart_subtotal_shipping_price { get; set; }
    public string cart_subtotal_shipping_name { get; set; }
    public double cart_summary_totals { get; set; }
    public int gift_product_flag { get; set; }
    public int item { get; set; }

    public string sp_price { get; set; }



}

public class customer_detail : en_cart_detail
{
    public int id_operating_system { get; set; }
    public int id_web_browser { get; set; }
    public string accept_language { get; set; }
    public int id_connections { get; set; }
    public int id_page { get; set; }
    public int ip_address { get; set; }
    public int id_gender { get; set; }
    public int id_default_group { get; set; }
    public int id_risk { get; set; }
    public string firstname { get; set; }
    public string lastname { get; set; }
    public string email { get; set; }
    public string passwd { get; set; }
    public string birthday { get; set; }
    public int newsletter { get; set; }
    public int optin { get; set; }
    public int active { get; set; }
    public int is_guest { get; set; }
    public int id_group { get; set; }

    public int id_address { get; set; }
    public int id_country { get; set; }
    public int id_state { get; set; }
    public int id_warehouse { get; set; }
    public string alias { get; set; }
    public string company { get; set; }
    public string address1 { get; set; }
    public string address2 { get; set; }
    public string postcode { get; set; }
    public string city { get; set; }
    public string phone_mobile { get; set; }
    public string vat_number { get; set; }
    public int type_action { get; set; }
}

public class en_order_detail : customer_detail
{

    //st_orders
    public int id_order { get; set; }
    public string reference { get; set; }
    public int current_state { get; set; }
    public string payment { get; set; }
    public int conversion_rate { get; set; }
    public string module { get; set; }
    public string shipping_number { get; set; }
    public string total_discounts { get; set; }
    public string total_discounts_tax_incl { get; set; }
    public string total_discounts_tax_excl { get; set; }
    public string total_paid { get; set; }
    public string total_paid_tax_incl { get; set; }
    public string total_paid_tax_excl { get; set; }
    public string total_paid_real { get; set; }
    public string total_products { get; set; }
    public string total_products_wt { get; set; }
    public string total_shipping { get; set; }
    public string total_shipping_tax_incl { get; set; }
    public string total_shipping_tax_excl { get; set; }
    public string carrier_tax_rate { get; set; }
    public string total_wrapping { get; set; }
    public string total_wrapping_tax_incl { get; set; }
    public string total_wrapping_tax_excl { get; set; }
    public int round_mode { get; set; }
    public int round_type { get; set; }
    public int invoice_number { get; set; }
    public int delivery_number { get; set; }
    public int valid { get; set; }
    //st_orders

    //st_order_history
    public int id_order_history { get; set; }
    public int id_employee { get; set; }
    public int id_order_state { get; set; }
    //st_order_history

    //st_order_payment
    public int id_order_payment { get; set; }
    public int amount { get; set; }
    public int transaction_id { get; set; }
    public string card_number { get; set; }
    public string card_brand { get; set; }
    public string card_expiration { get; set; }
    public string card_holder { get; set; }

    //st_order_payment


    //st_order_invoice
    public int id_order_invoice { get; set; }
    public int number { get; set; }
    public int shipping_tax_computation_method { get; set; }
    public string shop_address { get; set; }
    public string note { get; set; }

    //st_order_invoice

    //st_order_carrier
    public int id_order_carrier { get; set; }

    //st_order_carrier

    //st_order_detail
    public int id_order_detail { get; set; }
    public int product_id { get; set; }

    //st_order_detail
    public int product_quantity { get; set; }
    public string product_price { get; set; }
    public string reduction_percent { get; set; }
    public string product_quantity_discount { get; set; }

    public string total_price_tax_incl { get; set; }
    public string total_price_tax_excl { get; set; }
    public string unit_price_tax_incl { get; set; }
    public string unit_price_tax_excl { get; set; }
    public string value_tax_excl { get; set; }
    public string free_shipping { get; set; }

    //payment method
    public string paymentmethod { get; set; }

}



