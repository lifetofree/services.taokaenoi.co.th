using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for api_employee
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_menu : System.Web.Services.WebService
{
    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_menu _data_menu = new data_menu();

    public api_menu()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    // insert or update menu
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetMenu(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 10); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select all menu
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetMenu(string jsonIn)
    {
        if(jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 20); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // delect menu
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteMenu(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 93); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert system
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetSystem(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 11); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select all system
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSystem(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 21); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // delete system
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteSystem(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 92); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select permision type dropdownlist
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPermissionType(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 22); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select permision type where system and menu
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPermisstion(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 23); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select permision type where system and menu
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetPermission(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 12); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select permision type where system and menu
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPermissionMenu(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 24); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select permision type where system and menu
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeletePermission(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 94); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select permision view
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPermissionView(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 25); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // SET role
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRole(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 13); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // GET role
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRole(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 26); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    // GET role
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteRole(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 95); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // EDit role
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetEditRole(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 27); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // EDit role DDL Checkbox
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetEditRoleCheckBox(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("cenConn", "data_menu", "service_menu", _xml_in, 28); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

}
