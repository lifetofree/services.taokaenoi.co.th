using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot ("data_cen_employee")]
public class data_cen_employee {
    public int return_code { get; set; }
    public string return_msg { get; set; }
    public int return_idx { get; set; }

    public string employee_mode { get; set; }

    public cen_employee_detail_u0[] cen_employee_list_u0 { get; set; }

    public search_cen_employee_detail[] search_cen_employee_list { get; set; }
}

[Serializable]
public class cen_employee_detail_u0 {
    public int emp_idx { get; set; }
    public string emp_code { get; set; }
    public string emp_password { get; set; }

    public int emp_type_idx { get; set; }
    public string emp_type_name { get; set; }

    public int prefix_idx { get; set; }
    public string prefix_name_th { get; set; }
    public string prefix_name_en { get; set; }

    public string emp_firstname_th { get; set; }
    public string emp_lastname_th { get; set; }
    public string emp_nickname_th { get; set; }
    public string emp_name_th { get; set; }
    public string emp_firstname_en { get; set; }
    public string emp_lastname_en { get; set; }
    public string emp_nickname_en { get; set; }
    public string emp_name_en { get; set; }

    public int aff_idx { get; set; }
    public string aff_name_th { get; set; }
    public string aff_name_en { get; set; }
    public int jobgrade_idx { get; set; }
    public string jobgrade_name { get; set; }
    public int joblevel_idx { get; set; }
    public string joblevel_name { get; set; }
    public int cost_idx { get; set; }
    public string cost_no { get; set; }
    public string cost_name { get; set; }

    public int org_idx { get; set; }
    public string org_name_th { get; set; }
    public string org_name_en { get; set; }
    public int wg_idx { get; set; }
    public string wg_name_th { get; set; }
    public string wg_name_en { get; set; }
    public int lw_idx { get; set; }
    public string lw_name_th { get; set; }
    public string lw_name_en { get; set; }
    public int dept_idx { get; set; }
    public string dept_name_th { get; set; }
    public string dept_name_en { get; set; }
    public int sec_idx { get; set; }
    public string sec_name_th { get; set; }
    public string sec_name_en { get; set; }
    public int pos_idx { get; set; }
    public string pos_name_th { get; set; }
    public string pos_name_en { get; set; }

    public int empgroup_idx { get; set; }
    public string empgroup_name_th { get; set; }
    public string empgroup_name_en { get; set; }

    public string emp_start_date { get; set; }
    public string emp_resign_date { get; set; }

    public string emp_org_mail { get; set; }
    public string emp_personal_mail { get; set; }

    public int emp_idx_approve1 { get; set; }
    public string emp_approve1 { get; set; }
    public int emp_idx_approve2 { get; set; }
    public string emp_approve2 { get; set; }

    public int emp_status { get; set; }

    //-- for PMS --//
    public string emp_name_solid { get; set; }
    public string emp_name_dotted { get; set; }
    //-- for PMS --//
}

[Serializable]
public class search_cen_employee_detail {
    public string s_emp_idx { get; set; }
    public string s_emp_code { get; set; }
    public string s_emp_password { get; set; }
    public string s_emp_name { get; set; }
    public string s_org_idx { get; set; }
    public string s_wg_idx { get; set; }
    public string s_lw_idx { get; set; }
    public string s_dept_idx { get; set; }
    public string s_sec_idx { get; set; }
    public string s_pos_idx { get; set; }
    public string s_emp_status { get; set; }
}