﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

public class api_ibk : System.Web.Services.WebService
{
   string _xml_in = "";
   string _ret_val = "";
   function_tool _funcTool = new function_tool();
   service_execute _serviceExec = new service_execute();
   data_ibk dataIBK = new data_ibk();

   public api_ibk()
   {
      //Uncomment the following line if using designed components 
      //InitializeComponent(); 
   }

   #region Constants
   public static class Constants
   {
      public const int NUMBER_NULL = -1;
      public const int CREATE = 10;
      public const int UPDATE = 11;
      public const int SELECT_ALL = 20;
      public const int SELECT_WHERE_ROOM = 20;
      public const int SELECT_WHERE_TOPIC = 21;
      public const int SELECT_WHERE_CREATOR = 22;
      public const int SELECT_WHERE_AMOUNT_ROOM = 24;
   }
   #endregion Constants

   #region Room score
   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public void readU0RoomScoreAmountByRoom(int roomId)
   {
      if (roomId != 0)
      {
         string jsonIn = "{\"data_ibk\":{\"return_code\":\"0\",\"ibk_u0_room_score_action\":{\"u0_room_score_idx\":\"0\",\"u0_room_idx_ref\":\"" + roomId + "\",\"m0_topic_score_idx_ref\":\"0\",\"room_score_value\":\"0\",\"room_score_created_by\":\"0\"}}}";
         _xml_in = _funcTool.convertJsonToXml(jsonIn);
         _ret_val = _serviceExec.actionExec("ibkConn", "data_ibk", "serviceIBKRoomScore", _xml_in, Constants.SELECT_WHERE_AMOUNT_ROOM);
      }
      Context.Response.AppendHeader("Access-Control-Allow-Origin", "*");
      Context.Response.Clear();
      Context.Response.ContentType = "application/json; charset=utf-8";
      Context.Response.Write(_ret_val);
   }

   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public void readU0RoomScoreByRoom(int roomId)
   {
      if (roomId != 0)
      {
         string jsonIn = "{\"data_ibk\":{\"return_code\":\"0\",\"ibk_u0_room_score_action\":{\"u0_room_score_idx\":\"0\",\"u0_room_idx_ref\":\"" + roomId + "\",\"m0_topic_score_idx_ref\":\"0\",\"room_score_value\":\"0\",\"room_score_created_by\":\"0\"}}}";
         _xml_in = _funcTool.convertJsonToXml(jsonIn);
         _ret_val = _serviceExec.actionExec("ibkConn", "data_ibk", "serviceIBKRoomScore", _xml_in, Constants.SELECT_WHERE_ROOM);
      }
      Context.Response.AppendHeader("Access-Control-Allow-Origin", "*");
      Context.Response.Clear();
      Context.Response.ContentType = "application/json; charset=utf-8";
      Context.Response.Write(_ret_val);
   }

   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public void readU0RoomScoreByRoomAndTopic(int roomId, int topicScoreId)
   {
      if (roomId != 0 && topicScoreId != 0)
      {
         string jsonIn = "{\"data_ibk\":{\"return_code\":\"0\",\"ibk_u0_room_score_action\":{\"u0_room_score_idx\":\"0\",\"u0_room_idx_ref\":\"" + roomId + "\",\"m0_topic_score_idx_ref\":\"" + topicScoreId + "\",\"room_score_value\":\"0\",\"room_score_created_by\":\"0\"}}}";
         _xml_in = _funcTool.convertJsonToXml(jsonIn);
         _ret_val = _serviceExec.actionExec("ibkConn", "data_ibk", "serviceIBKRoomScore", _xml_in, Constants.SELECT_WHERE_TOPIC);
      }
      Context.Response.AppendHeader("Access-Control-Allow-Origin", "*");
      Context.Response.Clear();
      Context.Response.ContentType = "application/json; charset=utf-8";
      Context.Response.Write(_ret_val);
   }

   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public void readU0RoomScoreByRoomAndTopicAndCreator(int roomId, int topicScoreId, int creator)
   {
      if (roomId != 0 && topicScoreId != 0 && creator != 0)
      {
         string jsonIn = "{\"data_ibk\":{\"return_code\":\"0\",\"ibk_u0_room_score_action\":{\"u0_room_score_idx\":\"0\",\"u0_room_idx_ref\":\"" + roomId + "\",\"m0_topic_score_idx_ref\":\"" + topicScoreId + "\",\"room_score_value\":\"0\",\"room_score_created_by\":\"" + creator + "\"}}}";
         _xml_in = _funcTool.convertJsonToXml(jsonIn);
         _ret_val = _serviceExec.actionExec("ibkConn", "data_ibk", "serviceIBKRoomScore", _xml_in, Constants.SELECT_WHERE_CREATOR);
      }
      Context.Response.AppendHeader("Access-Control-Allow-Origin", "*");
      Context.Response.Clear();
      Context.Response.ContentType = "application/json; charset=utf-8";
      Context.Response.Write(_ret_val);
   }

   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public void createU0RoomScore(int roomId, int topicScoreId, int scoreValue, int creator)
   {
      if (roomId != 0 && topicScoreId != 0 && creator != 0)
      {
         string jsonIn = "{\"data_ibk\":{\"return_code\":\"0\",\"ibk_u0_room_score_action\":{\"u0_room_score_idx\":\"0\",\"u0_room_idx_ref\":\"" + roomId + "\",\"m0_topic_score_idx_ref\":\"" + topicScoreId + "\",\"room_score_value\":\"" + scoreValue + "\",\"room_score_created_by\":\"" + creator + "\"}}}";
         _xml_in = _funcTool.convertJsonToXml(jsonIn);
         _ret_val = _serviceExec.actionExec("ibkConn", "data_ibk", "serviceIBKRoomScore", _xml_in, Constants.CREATE);
      }
      Context.Response.AppendHeader("Access-Control-Allow-Origin", "*");
      Context.Response.Clear();
      Context.Response.ContentType = "application/json; charset=utf-8";
      Context.Response.Write(_ret_val);
   }
   #endregion Room score

   #region Topic score
   [WebMethod]
   [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
   public void readM0TopicScore()
   {
      string jsonIn = "{\"data_ibk\":{\"return_code\":\"0\",\"ibk_m0_topic_score_action\":{\"m0_topic_score_idx\":\"0\",\"topic_score_name_th\":\"0\",\"topic_score_name_eng\":\"0\",\"topic_score_status\":\"1\"}}}";
      _xml_in = _funcTool.convertJsonToXml(jsonIn);
      _ret_val = _serviceExec.actionExec("ibkConn", "data_ibk", "serviceIBKTopicScore", _xml_in, Constants.SELECT_ALL);
      Context.Response.AppendHeader("Access-Control-Allow-Origin", "*");
      Context.Response.Clear();
      Context.Response.ContentType = "application/json; charset=utf-8";
      Context.Response.Write(_ret_val);
   }
   #endregion Topic score
}