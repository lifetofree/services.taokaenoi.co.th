using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot ("data_tkn_checkin")]
public class data_tkn_checkin {
    public int return_code { get; set; }
    public string return_msg { get; set; }
    public int return_idx { get; set; }

    public string checkin_mode { get; set; }

    public tkn_checkin_detail[] tkn_checkin_list { get; set; }
    public tkn_checkin_setting_detail_m0[] tkn_checkin_setting_list_m0 { get; set; }
    public search_tkn_checkin_detail[] search_tkn_checkin_list { get; set; }
}

[Serializable]
public class tkn_checkin_detail {
    public int emp_idx { get; set; }
    public string latitude { get; set; }
    public string longitude { get; set; }
    public string create_date { get; set; }
    public string checkin_name { get; set; }
    public string street { get; set; }
    public string iso_country_code { get; set; }
    public string country { get; set; }
    public string postal_code { get; set; }
    public string admin_area { get; set; }
    public string sub_admin_area { get; set; }
    public string locality { get; set; }
    public string sub_locality { get; set; }
    public string thoroughfare { get; set; }
    public string sub_thoroughfare { get; set; }

    public string emp_code { get; set; }

    public int emp_type_idx { get; set; }
    public string emp_type_name { get; set; }

    public int prefix_idx { get; set; }
    public string prefix_name_th { get; set; }
    public string prefix_name_en { get; set; }

    public string emp_firstname_th { get; set; }
    public string emp_lastname_th { get; set; }
    public string emp_nickname_th { get; set; }
    public string emp_name_th { get; set; }
    public string emp_firstname_en { get; set; }
    public string emp_lastname_en { get; set; }
    public string emp_nickname_en { get; set; }
    public string emp_name_en { get; set; }

    public int aff_idx { get; set; }
    public string aff_name_th { get; set; }
    public string aff_name_en { get; set; }
    public int jobgrade_idx { get; set; }
    public string jobgrade_name { get; set; }
    public int joblevel_idx { get; set; }
    public string joblevel_name { get; set; }
    public int cost_idx { get; set; }
    public string cost_no { get; set; }
    public string cost_name { get; set; }

    public int org_idx { get; set; }
    public string org_name_th { get; set; }
    public string org_name_en { get; set; }
    public int wg_idx { get; set; }
    public string wg_name_th { get; set; }
    public string wg_name_en { get; set; }
    public int lw_idx { get; set; }
    public string lw_name_th { get; set; }
    public string lw_name_en { get; set; }
    public int dept_idx { get; set; }
    public string dept_name_th { get; set; }
    public string dept_name_en { get; set; }
    public int sec_idx { get; set; }
    public string sec_name_th { get; set; }
    public string sec_name_en { get; set; }
    public int pos_idx { get; set; }
    public string pos_name_th { get; set; }
    public string pos_name_en { get; set; }

    public int empgroup_idx { get; set; }
    public string empgroup_name_th { get; set; }
    public string empgroup_name_en { get; set; }

    public string emp_start_date { get; set; }
    public string emp_resign_date { get; set; }

    public string emp_org_mail { get; set; }
    public string emp_personal_mail { get; set; }
}

[Serializable]
public class tkn_checkin_setting_detail_m0 : tkn_checkin_detail {
    public int m0_idx { get; set; }
    public decimal range { get; set; }
    
    public int province_idx { get; set; }
    public string province { get; set; }
    public int amphure_idx { get; set; }
    public string amphure { get; set; }
    public int district_idx { get; set; }
    public string district { get; set; }
    public string places { get; set; } 

    public int status_zone { get; set; }
    public string zone_name { get; set; }  

    public int setting_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

[Serializable]
public class search_tkn_checkin_detail {
    public string s_emp_idx { get; set; }
    public string s_emp_code { get; set; }
    public string s_emp_name { get; set; }
    public string s_org_idx { get; set; }
    public string s_wg_idx { get; set; }
    public string s_lw_idx { get; set; }
    public string s_dept_idx { get; set; }
    public string s_sec_idx { get; set; }
    public string s_pos_idx { get; set; }
    public string s_emptype_idx { get; set; }
    public string s_emp_status { get; set; }
    public string s_start_date { get; set; }
    public string s_end_date { get; set; }

    public string s_postal_code { get; set; }
    public string s_admin_area { get; set; }

    //setting
    public string s_m0_idx { get; set; }
}