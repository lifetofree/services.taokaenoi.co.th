using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_dcc")]
public class data_dcc
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    //---------- car/par ----------//
    [XmlElement("cp_m0_actor_list")]
    public cp_m0_actor[] cp_m0_actor_list { get; set; }
    [XmlElement("cp_m0_document_status_list")]
    public cp_m0_document_status[] cp_m0_document_status_list { get; set; }
    [XmlElement("cp_m0_document_type_list")]
    public cp_m0_document_type[] cp_m0_document_type_list { get; set; }
    [XmlElement("cp_m1_document_type_list")]
    public cp_m1_document_type[] cp_m1_document_type_list { get; set; }
    [XmlElement("cp_m0_node_decision_list")]
    public cp_m0_node_decision[] cp_m0_node_decision_list { get; set; }

    [XmlElement("cp_u0_document_list")]
    public cp_u0_document[] cp_u0_document_list { get; set; }
    [XmlElement("cp_u1_document_list")]
    public cp_u1_document[] cp_u1_document_list { get; set; }
    //---------- car/par ----------//
}

#region car/par
[Serializable]
public class cp_m0_actor
{
    public int midx { get; set; }
    public string actor_name { get; set; }
    public string actor_desc { get; set; }
    public int actor_status { get; set; }
}

[Serializable]
public class cp_m0_document_status
{
    public int midx { get; set; }
    public string doc_status_name { get; set; }
    public int doc_status { get; set; }
}

[Serializable]
public class cp_m0_document_type
{
    public int midx { get; set; }
    public string type_name { get; set; }
    public int type_status { get; set; }
}

[Serializable]
public class cp_m1_document_type
{
    public int midx { get; set; }
    public int m0_idx { get; set; }
    public string type_name { get; set; }
    public int type_status { get; set; }
}

[Serializable]
public class cp_m0_node_decision
{
    public int midx { get; set; }
    public int m0_node_idx { get; set; }
    public string decision_name { get; set; }
    public int decision_status { get; set; }
}

[Serializable]
public class cp_u0_document
{
    [XmlElement("uidx")]
    public int uidx { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("req_rdept_idx")]
    public int req_rdept_idx { get; set; }
    [XmlElement("doc_code")]
    public string doc_code { get; set; }
    [XmlElement("doc_type")]
    public int doc_type { get; set; }
    [XmlElement("doc_sub_type")]
    public int doc_sub_type { get; set; }
    [XmlElement("doc_title")]
    public string doc_title { get; set; }
    [XmlElement("doc_detail")]
    public string doc_detail { get; set; }
    [XmlElement("res_rdept_idx")]
    public int res_rdept_idx { get; set; }
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("doc_file")]
    public int doc_file { get; set; }
    [XmlElement("doc_ref")]
    public int doc_ref { get; set; }
    [XmlElement("doc_hidden")]
    public int doc_hidden { get; set; }
    [XmlElement("deadline_date")]
    public string deadline_date { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
    [XmlElement("doc_status")]
    public int doc_status { get; set; }
    [XmlElement("current_node")]
    public int current_node { get; set; }
    [XmlElement("current_actor")]
    public int current_actor { get; set; }
    [XmlElement("coshare_list")]
    public string coshare_list { get; set; }
    [XmlElement("res_rsec_idx")]
    public int res_rsec_idx { get; set; }
    [XmlElement("req_rsec_idx")]
    public int req_rsec_idx { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("req_rdept_name")]
    public string req_rdept_name { get; set; }
    [XmlElement("doc_type_name")]
    public string doc_type_name { get; set; }
    [XmlElement("doc_sub_type_name")]
    public string doc_sub_type_name { get; set; }
    [XmlElement("res_rdept_name")]
    public string res_rdept_name { get; set; }
    [XmlElement("org_name")]
    public string org_name { get; set; }
    [XmlElement("doc_status_name")]
    public string doc_status_name { get; set; }
    [XmlElement("current_node_name")]
    public string current_node_name { get; set; }
    [XmlElement("current_actor_name")]
    public string current_actor_name { get; set; }
    [XmlElement("coshare_list_name")]
    public string coshare_list_name { get; set; }
    [XmlElement("res_rsec_name")]
    public string res_rsec_name { get; set; }
    [XmlElement("req_rsec_name")]
    public string req_rsec_name { get; set; }
}

[Serializable]
public class cp_u1_document
{
    [XmlElement("uidx")]
    public int uidx { get; set; }
    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("comment")]
    public string comment { get; set; }
    [XmlElement("doc_file")]
    public int doc_file { get; set; }
    [XmlElement("actor_idx")]
    public int actor_idx { get; set; }
    [XmlElement("from_node")]
    public int from_node { get; set; }
    [XmlElement("node_decision")]
    public int node_decision { get; set; }
    [XmlElement("to_node")]
    public int to_node { get; set; }
    [XmlElement("node_type")]
    public int node_type { get; set; }
    [XmlElement("doc_loop")]
    public int doc_loop { get; set; }
}
#endregion car/par