using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot ("data_cen_master")]
public class data_cen_master {
    public int return_code { get; set; }
    public string return_msg { get; set; }
    public int return_idx { get; set; }

    public string master_mode { get; set; }

    public cen_aff_detail_m0[] cen_aff_list_m0 { get; set; } //affilate
    public cen_empgroup_detail_m0[] cen_empgroup_list_m0 { get; set; } //employee group
    public cen_jobgrade_detail_m0[] cen_jobgrade_list_m0 { get; set; } //jobgrade
    public cen_joblevel_detail_m0[] cen_joblevel_list_m0 { get; set; } //joblevel
    public cen_costcenter_detail_m0[] cen_costcenter_list_m0 { get; set; } //cost center

    public cen_org_detail_m0[] cen_org_list_m0 { get; set; } //orgenization
    public cen_wg_detail_m0[] cen_wg_list_m0 { get; set; } //work group
    public cen_lw_detail_m0[] cen_lw_list_m0 { get; set; } //line work
    public cen_dept_detail_m0[] cen_dept_list_m0 { get; set; } //department
    public cen_sec_detail_m0[] cen_sec_list_m0 { get; set; } //section
    public cen_pos_detail_m0[] cen_pos_list_m0 { get; set; } //position

    public search_cen_master_detail[] search_cen_master_list { get; set; }
}

[Serializable]
public class cen_aff_detail_m0 {
    public int aff_idx { get; set; }
    public string aff_name_th { get; set; }
    public string aff_name_en { get; set; }
    public int cemp_idx { get; set; }
    public int aff_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

[Serializable]
public class cen_empgroup_detail_m0 {
    public int empgroup_idx { get; set; }
    public string empgroup_name_th { get; set; }
    public string empgroup_name_en { get; set; }
    public int cemp_idx { get; set; }
    public int empgroup_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

[Serializable]
public class cen_jobgrade_detail_m0 {
    public int jobgrade_idx { get; set; }
    public string jobgrade_name { get; set; }
    public int cemp_idx { get; set; }
    public int jobgrade_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

[Serializable]
public class cen_joblevel_detail_m0 {
    public int joblevel_idx { get; set; }
    public string joblevel_name { get; set; }
    public int cemp_idx { get; set; }
    public int joblevel_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

[Serializable]
public class cen_costcenter_detail_m0 {
    public int cost_idx { get; set; }
    public string cost_no { get; set; }
    public string cost_name { get; set; }
    public int cemp_idx { get; set; }
    public int cost_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

[Serializable]
public class cen_org_detail_m0 {
    public int org_idx { get; set; }
    public string org_name_th { get; set; }
    public string org_name_en { get; set; }
    public string cemp_idx { get; set; }
    public int org_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

[Serializable]
public class cen_wg_detail_m0 : cen_org_detail_m0 {
    public int wg_idx { get; set; }
    public string wg_name_th { get; set; }
    public string wg_name_en { get; set; }
    public int wg_status { get; set; }
}

[Serializable]
public class cen_lw_detail_m0 : cen_wg_detail_m0 {
    public int lw_idx { get; set; }
    public string lw_name_th { get; set; }
    public string lw_name_en { get; set; }
    public int lw_status { get; set; }
}

[Serializable]
public class cen_dept_detail_m0 : cen_lw_detail_m0 {
    public int dept_idx { get; set; }
    public string dept_name_th { get; set; }
    public string dept_name_en { get; set; }
    public int dept_status { get; set; }
}

[Serializable]
public class cen_sec_detail_m0 : cen_dept_detail_m0 {
    public int sec_idx { get; set; }
    public string sec_name_th { get; set; }
    public string sec_name_en { get; set; }
    public int sec_status { get; set; }
}

[Serializable]
public class cen_pos_detail_m0 : cen_sec_detail_m0 {
    public int pos_idx { get; set; }
    public int jobgrade_idx { get; set; }
    public string pos_name_th { get; set; }
    public string pos_name_en { get; set; }
    public int man_power { get; set; }
    public int pos_status { get; set; }
}

[Serializable]
public class cen_pos_emp_detail_r0 {
    public int pos_emp_idx { get; set; }
    public int emp_dx { get; set; }
    public int pos_idx { get; set; }
    public int flag_main { get; set; } //main = 1
    public int cemp_idx { get; set; }
    public int pos_emp_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

[Serializable]
public class search_cen_master_detail {
    public string s_aff_idx { get; set; }
    public string s_aff_name { get; set; }

    public string s_empgroup_idx { get; set; }
    public string s_empgroup_name { get; set; }

    public string s_org_idx { get; set; }
    public string s_org_name { get; set; }

    public string s_wg_idx { get; set; }
    public string S_wg_name { get; set; }

    public string s_lw_idx { get; set; }
    public string s_lw_name { get; set; }

    public string s_dept_idx { get; set; }
    public string s_dept_name { get; set; }

    public string s_sec_idx { get; set; }
    public string s_sec_name { get; set; }

    public string s_pos_idx { get; set; }
    public string s_pos_name { get; set; }

    public string s_jobgrade_idx { get; set; }
    public string s_jobgrade_name { get; set; }

    public string s_joblevel_idx { get; set; }
    public string s_joblevel_name { get; set; }

    public string s_cost_idx { get; set; }
    public string s_cost_name { get; set; }

    public string s_pos_emp_idx { get; set; }
    public string s_flag_main { get; set; }

    public string s_status { get; set; }
}