﻿using System;
using System.Xml.Serialization;


public class data_permission
{
    public int return_code { get; set; }
    public string return_msg { get; set; }

   
    public permission_detail[] permission_list { get; set; }
    public system_detail[] system_list { get; set; }

    public systemall_detail[] systemall_list { get; set; }
    public searchindex_detail[] searchindex_list { get; set; }

}


public class permission_detail
{
   
    public int menu_idx { get; set; }    
    public int emp_idx { get; set; }
    public int OrgIDX { get; set; }     
    public string OrgNameTH { get; set; } 
    public string OrgNameEN { get; set; } 
    public int RDeptIDX { get; set; }   
    public int DeptIDX { get; set; }
    public string DeptNameTH { get; set; }  
    public string DeptNameEN { get; set; }   

    public int RSecIDX { get; set; }    
    public int SecIDX { get; set; }  
    public string SecNameTH { get; set; }  
    public string SecNameEN { get; set; }  
    public int RPosIDX { get; set; }   
    public int PosIDX { get; set; }   
    public string PosNameTH { get; set; } 
    public string PosNameEN { get; set; }
    public int EODSPIDX { get; set; }
    //public int EmpIDX { get; set; } 
    public int EmpIDXUser { get; set; } 
    public int EStatus { get; set; }   
    public string CreateDate { get; set; }    
    public int EStaOut { get; set; }   
    public int JobGradeIDX1 { get; set; }  
    public int JobGradeIDX2 { get; set; }   
    public string FullNameTH { get; set; }

    public bool selected { get; set; }

}

public class system_detail
{
    public int system_idx { get; set; }
    public string system_name_en { get; set; }
    public string system_name_th { get; set; }

    public int menu_idx { get; set; }
    public int emp_idx { get; set; }
    public int OrgIDX { get; set; }
    public string OrgNameTH { get; set; }
    public string OrgNameEN { get; set; }
    public int RDeptIDX { get; set; }
    public int DeptIDX { get; set; }
    public string DeptNameTH { get; set; }
    public string DeptNameEN { get; set; }

    public int RSecIDX { get; set; }
    public int SecIDX { get; set; }
    public string SecNameTH { get; set; }
    public string SecNameEN { get; set; }
    
  

}

public class role_detail
{
    public int ods_idx { get; set; }
    public int role_idx { get; set; }
    public string role_name_th { get; set; }
    public string role_name_en { get; set; }
    public int role_status { get; set; }

    public int system_idx { get; set; }
    public string system_name_en { get; set; }
    public string system_name_th { get; set; }

    public int menu_idx { get; set; }
    public int emp_idx { get; set; }
    public int OrgIDX { get; set; }
    public string OrgNameTH { get; set; }
    public string OrgNameEN { get; set; }
    public int RDeptIDX { get; set; }
    public int DeptIDX { get; set; }
    public string DeptNameTH { get; set; }
    public string DeptNameEN { get; set; }

    public int RSecIDX { get; set; }
    public int SecIDX { get; set; }
    public string SecNameTH { get; set; }
    public string SecNameEN { get; set; }

    public int cemp_idx { get; set; }
    public string rolepermission_idxstring { get; set; }

    

}

public class systemall_detail
{
    public int system_idx { get; set; }
    public string system_name_en { get; set; }
    public string system_name_th { get; set; }

}

public class searchindex_detail
{

    public int menu_idx { get; set; }
    public int emp_idx { get; set; }
    public int OrgIDX { get; set; }
    public string OrgNameTH { get; set; }
    public string OrgNameEN { get; set; }
    public int RDeptIDX { get; set; }
    public int DeptIDX { get; set; }
    public string DeptNameTH { get; set; }
    public string DeptNameEN { get; set; }

    public int RSecIDX { get; set; }
    public int SecIDX { get; set; }
    public string SecNameTH { get; set; }
    public string SecNameEN { get; set; }
    public int RPosIDX { get; set; }
    public int PosIDX { get; set; }
    public string PosNameTH { get; set; }
    public string PosNameEN { get; set; }
    public int EODSPIDX { get; set; }
    //public int EmpIDX { get; set; } 
    public int EmpIDXUser { get; set; }
    public int EStatus { get; set; }
    public string CreateDate { get; set; }
    public int EStaOut { get; set; }
    public int JobGradeIDX1 { get; set; }
    public int JobGradeIDX2 { get; set; }
    public string FullNameTH { get; set; }

    public bool selected { get; set; }

}