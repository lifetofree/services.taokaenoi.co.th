﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_google_mailalert
/// </summary>
/// 

[Serializable]
[XmlRoot("data_google_mailalert")]
public class data_google_mailalert
{


    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("google_mail_list")]
    public google_mail_detail[] google_mail_list { get; set; }

    [XmlElement("google_mail_status_list")]
    public google_mail_status_detail[] google_mail_status_list { get; set; }

    
}

[Serializable]
public class google_mail_detail
{

    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }

    [XmlElement("u0_buy_idx")]
    public int u0_buy_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("emp_create")]
    public int emp_create { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("po_code")]
    public string po_code { get; set; }

    [XmlElement("lot_codebuy")]
    public string lot_codebuy { get; set; }

    [XmlElement("lot_code")]
    public string lot_code { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }

    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }

    [XmlElement("num_license")]
    public int num_license { get; set; }

    [XmlElement("price")]
    public int price { get; set; }

    [XmlElement("OrgNameTH_inlicense")]
    public string OrgNameTH_inlicense { get; set; }

    [XmlElement("DeptNameTH_inlicense")]
    public string DeptNameTH_inlicense { get; set; }

    [XmlElement("status_active")]
    public int status_active { get; set; }

    [XmlElement("name_license_gmail")]
    public string name_license_gmail { get; set; }

    [XmlElement("Count_EmpGoogleLicnese")]
    public int Count_EmpGoogleLicnese { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("status_document")]
    public string status_document { get; set; }

    [XmlElement("name_holder")]
    public string name_holder { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("email_license")]
    public string email_license { get; set; }

    [XmlElement("org_idx_holder")]
    public int org_idx_holder { get; set; }

    [XmlElement("org_name_th_holder")]
    public string org_name_th_holder { get; set; }

    [XmlElement("rdept_idx_holder")]
    public int rdept_idx_holder { get; set; }

    [XmlElement("dept_name_th_holder")]
    public string dept_name_th_holder { get; set; }

    [XmlElement("rsec_idx_holder")]
    public int rsec_idx_holder { get; set; }

    [XmlElement("sec_name_th_holder")]
    public string sec_name_th_holder { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("holder_gmail")]
    public string holder_gmail { get; set; }

    [XmlElement("type_buy")]
    public string type_buy { get; set; }


}

[Serializable]
public class google_mail_status_detail
{

    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }

    [XmlElement("u0_buy_idx")]
    public int u0_buy_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("emp_create")]
    public int emp_create { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("po_code")]
    public string po_code { get; set; }

    [XmlElement("lot_codebuy")]
    public string lot_codebuy { get; set; }

    [XmlElement("lot_code")]
    public int lot_code { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }

    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }

    [XmlElement("num_license")]
    public int num_license { get; set; }

    [XmlElement("price")]
    public int price { get; set; }

    [XmlElement("OrgNameTH_inlicense")]
    public string OrgNameTH_inlicense { get; set; }

    [XmlElement("DeptNameTH_inlicense")]
    public string DeptNameTH_inlicense { get; set; }

    [XmlElement("status_active")]
    public int status_active { get; set; }

    [XmlElement("name_license_gmail")]
    public string name_license_gmail { get; set; }

    [XmlElement("Count_EmpGoogleLicnese")]
    public int Count_EmpGoogleLicnese { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("status_document")]
    public string status_document { get; set; }

    [XmlElement("name_holder")]
    public string name_holder { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("email_license")]
    public string email_license { get; set; }

    [XmlElement("org_idx_holder")]
    public int org_idx_holder { get; set; }

    [XmlElement("org_name_th_holder")]
    public string org_name_th_holder { get; set; }

    [XmlElement("rdept_idx_holder")]
    public int rdept_idx_holder { get; set; }

    [XmlElement("dept_name_th_holder")]
    public string dept_name_th_holder { get; set; }

    [XmlElement("rsec_idx_holder")]
    public int rsec_idx_holder { get; set; }

    [XmlElement("sec_name_th_holder")]
    public string sec_name_th_holder { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("holder_gmail")]
    public string holder_gmail { get; set; }

    [XmlElement("type_buy")]
    public string type_buy { get; set; }


}