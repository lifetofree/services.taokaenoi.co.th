﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_it_news
/// </summary>
/// 
[Serializable]
[XmlRoot("data_it_news")]
public class data_it_news
{

    [XmlElement("return_code")]
    public string return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public m0_type_it_news_details[] m0_type_it_news_list { get; set; }
    [XmlElement("u0_it_news_list")]
    public u0_it_news_details[] u0_it_news_list { get; set; }
}
    #region m0_type_it_news_details

    [Serializable]
    public class m0_type_it_news_details
    {

        public int m0_news_idx { get; set; }
        public string type_it_news { get; set; }
        public int status_type_news { get; set; }

    }

    #endregion

    #region u0_it_news_details

    [Serializable]
    public class u0_it_news_details
    {


        [XmlElement("u0_news_idx")]
        public int u0_news_idx { get; set; }

        [XmlElement("title_it_news")]
        public string title_it_news { get; set; }

        [XmlElement("status_news")]
        public int status_news { get; set; }

        [XmlElement("details_news")]
        public string details_news { get; set; }

        [XmlElement("m0_news_idx")]
        public int m0_news_idx { get; set; }

        [XmlElement("name_type_news")]
        public string name_type_news { get; set; }

        [XmlElement("search")]
        public string search { get; set; }

        [XmlElement("sentmail")]
        public string sentmail { get; set; }

        [XmlElement("hasfile")]
        public string hasfile { get; set; }

        [XmlElement("pathfilepic")]
        public string pathfilepic { get; set; }

}

    #endregion
