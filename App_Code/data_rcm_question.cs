﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_rcm_question")]
public class data_rcm_question
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }
    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }
    [XmlElement("ReturnEmpIDX")]
    public string ReturnEmpIDX { get; set; }
    [XmlElement("ReturnCount")]
    public string ReturnCount { get; set; }

    public m0_typequest[] Boxm0_typequest { get; set; }
    public m0_topicquest[] Boxm0_topicquest { get; set; }
    public m0_typeanswer[] Boxm0_typeanswer { get; set; }
    public m0_question[] Boxm0_question { get; set; }
    public u0_answer[] Boxu0_answer { get; set; }
    public u1_answer[] Boxu1_answer { get; set; }
    [XmlElement("BoxSendEmailQuiz")]
    public sendemailquiz[] BoxSendEmailQuiz { get; set; }
    public l0_answer[] Boxl0_logquestion { get; set; }
    public m1_persondetail[] Boxm1_personlist { get; set; }

}

[Serializable]
public class m0_typequest
{
    public int m0_tqidx { get; set; }
    public string type_quest { get; set; }

}



[Serializable]
public class m0_typeanswer
{
    public int m0_taidx { get; set; }
    public string type_asn { get; set; }
    public int m0_toidx { get; set; }
    public string topic_name { get; set; }

}

[Serializable]
public class m0_topicquest
{
    public int m0_toidx { get; set; }
    public int m0_tqidx { get; set; }
    public int orgidx { get; set; }
    public int rdeptidx { get; set; }
    public int rsecidx { get; set; }
    public int rposidx { get; set; }
    public int CEmpIDX { get; set; }
    public int status { get; set; }
    public string topic_name { get; set; }
    public string OrgNameTH { get; set; }
    public string DeptNameTH { get; set; }
    public string SecNameTH { get; set; }
    public string PosNameTH { get; set; }
    public int m0_taidx { get; set; }
    public int no_choice { get; set; }
    public string type_quest { get; set; }
    public string total_score { get; set; }
    public string emp_name_th { get; set; }
    public string P_Name { get; set; }
    public string P_Description { get; set; }
    public string P_Location { get; set; }
    public int score_choice { get; set; }
    public string score_comment { get; set; }
    public int PIDX { get; set; }
    public string StatusDoc { get; set; }
    public string datedoing { get; set; }
    public int u0_anidx { get; set; }
    public int unidx { get; set; }
    public string P_m0_toidx { get; set; }
    public string identity_card { get; set; }
    public int emp_idx { get; set; }
    public string DateStart { get; set; }
    public string DateEnd { get; set; }
    public int IFSearchbetween { get; set; }
    public int acidx { get; set; }
    public int node_decision { get; set; }
    public string determine { get; set; }
    public string statusinterview { get; set; }

}

[Serializable]
public class m0_question
{
    public int m0_quidx { get; set; }
    public int m0_toidx { get; set; }

    public int m0_taidx { get; set; }
    public int no_choice { get; set; }

    public string quest_name { get; set; }
    public int pic_quest { get; set; }

    public string choice_a { get; set; }
    public int pic_a { get; set; }

    public string choice_b { get; set; }
    public int pic_b { get; set; }

    public string choice_c { get; set; }
    public int pic_c { get; set; }

    public string choice_d { get; set; }
    public int pic_d { get; set; }

    public int choice_ans { get; set; }
    public int CEmpIDX { get; set; }
    public int m0_tqidx { get; set; }
    public int condition { get; set; }
    public string answer { get; set; }
    public int u0_anidx { get; set; }
    public int unidx { get; set; }
    public int acidx { get; set; }
    public int staidx { get; set; }
    public int u1_anidx { get; set; }
    public string score { get; set; }
    public int node_decision { get; set; }
    public string determine { get; set; }

}

[Serializable]
public class u0_answer
{
    public int m0_toidx { get; set; }
    public int m0_taidx { get; set; }
    public int m0_tqidx { get; set; }
    public int CEmpIDX { get; set; }
    public string P_m0_toidx { get; set; }
    public int PIDX { get; set; }
    public int unidx { get; set; }
    public int acidx { get; set; }
    public int staidx { get; set; }
    public int u0_anidx { get; set; }
    public string determine { get; set; }


}

[Serializable]
public class u1_answer
{
    public int m0_quidx { get; set; }
    public string answer { get; set; }
    public int CEmpIDX { get; set; }
    public string score { get; set; }
    public int u1_anidx { get; set; }


}

[Serializable]
public class sendemailquiz
{
    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("P_Name")]
    public string P_Name { get; set; }

    [XmlElement("datedoing")]
    public string datedoing { get; set; }

    [XmlElement("JName")]
    public string JName { get; set; }

    [XmlElement("P_Location")]
    public string P_Location { get; set; }

    [XmlElement("P_Description")]
    public string P_Description { get; set; }

    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("total_score")]
    public string total_score { get; set; }

    [XmlElement("score_choice")]
    public string score_choice { get; set; }

    [XmlElement("score_comment")]
    public string score_comment { get; set; }

    [XmlElement("topic_name")]
    public string topic_name { get; set; }

    [XmlElement("statusinterview")]
    public string statusinterview { get; set; }

    [XmlElement("determine")]
    public string determine { get; set; }

    [XmlElement("adminname")]
    public string adminname { get; set; }

}

[Serializable]
public class l0_answer
{
    [XmlElement("u0_anidx")]
    public int u0_anidx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

}

[Serializable]
public class m1_persondetail
{
    public string topic_name_person { get; set; }
    public string P_Name { get; set; }
    public int CEmpIDX { get; set; }
    public int PIDX { get; set; }
    public int EmpIDX { get; set; }
    public string org_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_th { get; set; }
    public string pos_name_th { get; set; }
    public string emp_name_th { get; set; }
    public int m0_toidx { get; set; }
    public int P_EmpIDX { get; set; }
    public int TPIDX { get; set; }
    public string P_Description { get; set; }
    public string P_Location { get; set; }


}