﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_networkdevices
/// </summary>
public class data_network
{
    public int return_code { get; set; }
    public string return_msg { get; set; }

    public empinsert_detail[] empinsert_list { get; set; }
    public network_detail[] network_list { get; set; }
    public bindnetwork_detail[] bindnetwork_list { get; set; }
    public lognetwork_detail[] lognetwork_list { get; set; }

    public repairnetwork_detail[] repairnetwork_list { get; set; }

    public logrepairnetwork_detail[] logrepairnetwork_list { get; set; }

    public manetwork_detail[] manetwork_list { get; set; }

    public logmanetwork_detail[] logmanetwork_list { get; set; }

    public bindcutnetwork_detail[] bindcutnetwork_list { get; set; }

    public logrepaircutnetwork_detail[] logrepaircutnetwork_list { get; set; }

    public logmacutnetwork_detail[] logmacutnetwork_list { get; set; }

    public cutnetwork_detail[] cutnetwork_list { get; set; }

    public bindmovenetwork_detail[] bindmovenetwork_list { get; set; }

    public movenetwork_detail[] movenetwork_list { get; set; }

    public exportnetwork_detail[] exportnetwork_list { get; set; }

    public inexportnetwork_detail[] inexportnetwork_list { get; set; }

    public editnetwork_detail[] editnetwork_list { get; set; }

}

public class empinsert_detail
{

    public int emp_idx { get; set; }

    // employee name profile
    public string emp_name_en { get; set; }
    public string emp_name_th { get; set; }

    public string emp_firstname_en { get; set; }
    public string emp_lastname_en { get; set; }
    public string emp_firstname_th { get; set; }
    public string emp_lastname_th { get; set; }

    public string emp_nickname_en { get; set; }
    public string emp_nickname_th { get; set; }
    // employee name profile

    // employee general profile
    public int sex_idx { get; set; }
    public string sex_name_en { get; set; }
    public string sex_name_th { get; set; }

    public string emp_phone_no { get; set; }
    public string emp_mobile_no { get; set; }

    public string emp_email { get; set; }

    public string emp_birthday { get; set; }
    // employee general profile

    // employee type
    public int emp_type_idx { get; set; }
    public string emp_type_name { get; set; }
    // employee type

    // employee organization profile
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int rpos_idx { get; set; }

    public string org_name_en { get; set; }
    public string dept_name_en { get; set; }
    public string sec_name_en { get; set; }
    public string pos_name_en { get; set; }

    public string org_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_th { get; set; }
    public string pos_name_th { get; set; }

    public int jobgrade_idx { get; set; }
    public int jobgrade_level { get; set; }

    public int costcenter_idx { get; set; }
    public string costcenter_no { get; set; }
    public string costcenter_name { get; set; }
    // employee organization profile

    // employee start date, probation date and status
    public string emp_start_date { get; set; }
    public int emp_probation_status { get; set; }
    public string emp_probation_date { get; set; }
    // employee start date, probation date and status

    // employee profile date and status
    public string emp_createdate { get; set; }
    public string emp_updatedate { get; set; }
    public int emp_status { get; set; }
    // employee profile date and status

    // employee update password profile
    public string new_password { get; set; }
    public int type_reset { get; set; }
    // employee update password profile

    // vacation approver profile
    public int emp_idx_approve1 { get; set; }
    public int emp_idx_approve2 { get; set; }
    public string emp_approve1 { get; set; }
    public string emp_approve2 { get; set; }
    // vacation approver profile

    public string ip_address { get; set; }



}

public class network_detail
{

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }
    public int type_idx { get; set; }

    public string type_name { get; set; }

    public int category_idx { get; set; }

    public string category_name { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }
    public int company_idx { get; set; }

    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public string place_name { get; set; }

    public int room_idx { get; set; }
    public string room_name { get; set; }
 
    public string place_new_name { get; set; }
 
    public string room_new_name { get; set; }


    public int status_deviecs { get; set; }
    public int status_move { get; set; }
    
    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string pos_name_th { get; set; }

    public string status_approve { get; set; }

    public string date_purchase_new { get; set; }
    public string date_expire_new { get; set; }
    public int status_ma { get; set; }
    public string detail_ma { get; set; }
    public int company_new_idx { get; set; }
    public string company_name_new { get; set; }


    public string brand_name_edit { get; set; }
    public string generation_name_edit { get; set; }
    public int type_idx_edit { get; set; }
    public int category_idx_edit { get; set; }
    public string ip_address_edit { get; set; }
    public string serial_number_edit { get; set; }
    public int place_idx_edit { get; set; }
    public int room_idx_edit { get; set; }
    public string asset_code_edit { get; set; }

    public string user_name_edit { get; set; }
    public string pass_word_edit { get; set; }

}

public class bindnetwork_detail
{
    public int if_date { get; set; }

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }

    public int type_idx { get; set; }
    public string type_name { get; set; }

    public int category_idx { get; set; }
    public string category_name { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }

    public int company_idx { get; set; }
    public string company_name { get; set; }
   

    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public string place_name { get; set; }

    public int room_idx { get; set; }
    public string room_name { get; set; }

    public int place_new_idx { get; set; }
    public string place_name_new { get; set; }

    public int room_new_idx { get; set; }
    public string room_name_new { get; set; }


    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string pos_name_th { get; set; }

    public int status_move { get; set; }

    public string date_purchase_new { get; set; }
    public string date_expire_new { get; set; }
    public int status_ma { get; set; }
    public string detail_ma { get; set; }
    public int company_new_idx { get; set; }
    public string company_name_new { get; set; }

}

public class lognetwork_detail
{

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }

    public int type_idx { get; set; }
    public string type_name { get; set; }

    public int category_idx { get; set; }
    public string category_name { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }

    public int company_idx { get; set; }
    public string company_name { get; set; }


    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public string place_name { get; set; }

    public int room_idx { get; set; }
    public string room_name { get; set; }

    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }

    public string actor_des { get; set; }
    public string node_name { get; set; }

    public string status_name { get; set; }

    public string comment { get; set; }

    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string pos_name_th { get; set; }
}

public class repairnetwork_detail
{
    public int l0_devicerepair_idx { get; set; }

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }
    public int type_idx { get; set; }
    public int category_idx { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }
    public int company_idx { get; set; }

    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public int room_idx { get; set; }
    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string comment { get; set; }
    public string date_repair { get; set; }
}

public class logrepairnetwork_detail
{
    public int l0_devicerepair_idx { get; set; }

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }
    public int type_idx { get; set; }
    public int category_idx { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }
    public int company_idx { get; set; }

    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public int room_idx { get; set; }
    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string comment { get; set; }
    public string date_repair { get; set; }
}

public class manetwork_detail
{
    public int l0_devicema_idx { get; set; }

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }
    public int type_idx { get; set; }
    public int category_idx { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }
    public int company_idx { get; set; }

    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public int room_idx { get; set; }
    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string comment { get; set; }

    public string date_start { get; set; }
    public string date_end { get; set; }
}

public class logmanetwork_detail
{
    public int l0_devicescompany_idx { get; set; }

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }
    public int type_idx { get; set; }
    public int category_idx { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }
    public int company_idx { get; set; }

    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public int room_idx { get; set; }
    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string comment { get; set; }
  
    public string date_start { get; set; }

   
    public string date_end { get; set; }
}

public class bindcutnetwork_detail
{

    public int diff_date { get; set; }
    public int if_date { get; set; }

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }

    public int type_idx { get; set; }
    public string type_name { get; set; }

    public int category_idx { get; set; }
    public string category_name { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }

    public int company_idx { get; set; }
    public string company_name { get; set; }


    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public string place_name { get; set; }

    public int room_idx { get; set; }
    public string room_name { get; set; }

    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string pos_name_th { get; set; }
}

public class logrepaircutnetwork_detail
{
    public int l0_devicerepair_idx { get; set; }

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }
    public int type_idx { get; set; }
    public int category_idx { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }
    public int company_idx { get; set; }

    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public int room_idx { get; set; }
    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string comment { get; set; }
    public string date_repair { get; set; }
}

public class logmacutnetwork_detail
{
    public int l0_devicescompany_idx { get; set; }

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }
    public int type_idx { get; set; }
    public int category_idx { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }
    public int company_idx { get; set; }

    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public int room_idx { get; set; }
    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string comment { get; set; }

    public string date_start { get; set; }


    public string date_end { get; set; }
}

public class cutnetwork_detail
{

    public int u0_devicenetwork_idx { get; set; }


    public string diff_date { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }
    public int type_idx { get; set; }
    public int category_idx { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }
    public int company_idx { get; set; }

    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public int room_idx { get; set; }
    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string pos_name_th { get; set; }
}

public class bindmovenetwork_detail
{

    public int diff_date { get; set; }
    public int if_date { get; set; }

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }

    public int type_idx { get; set; }
    public string type_name { get; set; }

    public int category_idx { get; set; }
    public string category_name { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }

    public int company_idx { get; set; }
    public string company_name { get; set; }


    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public string place_name { get; set; }

    public int room_idx { get; set; }
    public string room_name { get; set; }

    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string pos_name_th { get; set; }
}

public class movenetwork_detail
{

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }
    public int type_idx { get; set; }
    public int category_idx { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }
    public int company_idx { get; set; }

    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public int room_idx { get; set; }

    public int place_new_idx { get; set; }
    public int room_new_idx { get; set; }

    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    
}

public class exportnetwork_detail
{
    
    public string เลขทะเบียนอุปกรณ์ { get; set; }
    public string สถานที่ติดตั้ง { get; set; }
    public string อาคาร { get; set; }
    public string ประเภทอุปกรณ์ { get; set; }
    public string ชนิดอุปกรณ์ { get; set; }
    public string ยี่ห้อ { get; set; }
    public string รุ่น { get; set; }
    public string บริษัทประกัน { get; set; }
    public string วันที่เริ่มประกัน { get; set; }
    public string วันที่หมดประกัน { get; set; }
    public string ชื่อผู้สร้างรายการ { get; set; }
    public string ชื่อผู้อนุมัติ { get; set; }
    public string รายละเอียดอุปกรณ์ { get; set; }


}

public class inexportnetwork_detail
{
    public int if_date { get; set; }

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }

    public int type_idx { get; set; }
    public string type_name { get; set; }

    public int category_idx { get; set; }
    public string category_name { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }

    public int company_idx { get; set; }
    public string company_name { get; set; }


    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public string place_name { get; set; }

    public int room_idx { get; set; }
    public string room_name { get; set; }

    public int place_new_idx { get; set; }
    public string place_name_new { get; set; }

    public int room_new_idx { get; set; }
    public string room_name_new { get; set; }


    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string pos_name_th { get; set; }

    public int status_move { get; set; }

    public string date_purchase_new { get; set; }
    public string date_expire_new { get; set; }
    public int status_ma { get; set; }
    public string detail_ma { get; set; }
    public int company_new_idx { get; set; }
    public string company_name_new { get; set; }

}

public class editnetwork_detail
{
    public int if_date { get; set; }

    public int u0_devicenetwork_idx { get; set; }

    public string u0_doc_code { get; set; }
    public string brand_name { get; set; }

    public int type_idx { get; set; }
    public string type_name { get; set; }

    public int category_idx { get; set; }
    public string category_name { get; set; }

    public string generation_name { get; set; }
    public string ip_address { get; set; }
    public string serial_number { get; set; }

    public int company_idx { get; set; }
    public string company_name { get; set; }


    public string date_purchase { get; set; }
    public string date_expire { get; set; }

    public int place_idx { get; set; }
    public string place_nameedit { get; set; }

    public int room_idx { get; set; }
    public string room_name { get; set; }

    public int place_new_idx { get; set; }
    public string place_name_new { get; set; }

    public int room_new_idx { get; set; }
    public string room_name_new { get; set; }


    public int status_deviecs { get; set; }

    public string user_name { get; set; }
    public string pass_word { get; set; }

    public string detail_devices { get; set; }
    public string asset_code { get; set; }

    public string register_number { get; set; }

    public int cemp_idx_create { get; set; }
    public int cemp_idx_approve { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }

    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int doc_decision { get; set; }

    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int cemp_idx { get; set; }

    public string status_desc { get; set; }
    public string name_create { get; set; }
    public string name_approve { get; set; }
    public string date_create { get; set; }

    public string pos_name_th { get; set; }

    public int status_move { get; set; }

    public string date_purchase_new { get; set; }
    public string date_expire_new { get; set; }
    public int status_ma { get; set; }
    public string detail_ma { get; set; }
    public int company_new_idx { get; set; }
    public string company_name_new { get; set; }

}

