﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_probation
/// </summary>
public class data_probation
{
    public string return_code { get; set; }
    public string return_msg { get; set; }

    public m0_type_evaluation_detail[] m0_type_evaluation_list { get; set; }
    public m0_name_evaluation_detail[] m0_name_evaluation_list { get; set; }
    public u0_name_probation_detail[] u0_name_probation_list { get; set; }
    public m0_name_assessor_detail[] m0_name_assessor_list { get; set; }
    public u0_assessment_detail[] u0_assessment_list { get; set; }
    public u0_result_assessment_detail[] u0_result_assessment_list { get; set; }


}

[Serializable]
public class m0_type_evaluation_detail
{
    public int m0_type_idx { get; set; }
    public string type_evaluation_name { get; set; }
    public int type_evaluation_ststus { get; set; }

}

[Serializable]
public class m0_name_evaluation_detail
{
    public int m0_topics_idx { get; set; }
    public string topics_name { get; set; }
    public int topics_status { get; set; }
    public int root_m0_topics_idx { get; set; }
    public int emps_type_probation { get; set; }
    public int emps_idx_topics { get; set; }

}

[Serializable]
public class u0_name_probation_detail
{
    public int u0_probation_idx { get; set; }
    public int emp_probation_idx { get; set; }
    public int org_probation_idx { get; set; }
    public int sec_probation_idx { get; set; }
    public int dept_probation_idx { get; set; }
    public int pos_probation_idx { get; set; }
    public int status_probation { get; set; }
    public int jobgrade_level { get; set; }
    public int actor_idx_probation { get; set; }
    public int node_idx_probation { get; set; }
    public int emp_assessor1_idx { get; set; }
    public int emp_assessor2_idx { get; set; }
    public int emp_idx_hr_director { get; set; }
    public string emps_fullname_probation { get; set; }
    public string emps_code_probation { get; set; }
    public string name_org_probation { get; set; }
    public string name_sec_probation { get; set; }
    public string name_dept_probation { get; set; }
    public string name_pos_probation { get; set; }
    public string date_start_probation { get; set; }
    public string date_doing_evaluation { get; set; }
    public string actor_probation_name { get; set; }
    public string node_probation_name { get; set; }
    public string node_probation_status { get; set; }

}


[Serializable]
public class m0_name_assessor_detail
{
    public int emp_assessor_idx { get; set; }
    public string name_assessor { get; set; }
}

[Serializable]
public class u0_assessment_detail
{
    public int u0_assessment_idx { get; set; }
    public int emp_idx_assessor1 { get; set; }
    public int emp_idx_assessor2 { get; set; }
    public int emp_idx_probation { get; set; }
    public int probation_status { get; set; }
    public int m0_actor_idx { get; set; }
    public int m0_node_idx { get; set; }
    public int emp_idx_hr_director { get; set; }
    public string probation_datetime { get; set; }
    public string probation_createdate { get; set; }
    public string emps_code_probation { get; set; }
    public string emps_fullname_probation { get; set; }
    public string name_pos_probation { get; set; }
    public string date_start_probation { get; set; }

}

[Serializable]
public class u0_result_assessment_detail
{
    public int idTopics { get; set; }
    public int full_score_result { get; set; }
    public float average_score_result { get; set; }
    public int status_result { get; set; }
    public int emp_idx_result { get; set; }
    public int type_idx_evaluation { get; set; }
    public string note_assessment { get; set; }
    public int number_of_assessment { get; set; }
    public int score_result { get; set; }
    public int emp_idx_assessor { get; set; }

}
