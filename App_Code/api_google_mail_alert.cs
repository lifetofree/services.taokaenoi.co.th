﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for api_google_mail_alert
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_google_mail_alert : System.Web.Services.WebService
{

    string _xml_in = "";
    string _ret_val = "";
    string _local_xml = "";
    string _mail_subject = "";
    string _mail_body = "";

    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    service_mail _serviceMail = new service_mail();
    data_google_mailalert _data_google_mailalert = new data_google_mailalert();


    public api_google_mail_alert()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    // Get Select Mail Alert Google License
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetGooglelicenseMailAlert(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("misConn", "data_google_mailalert", "service_google_mailalert", _xml_in, 20); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

}
