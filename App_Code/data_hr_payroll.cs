using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_hr_payroll")]
public class data_hr_payroll
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("slip_mode")]
    public string slip_mode { get; set; }

    public slip_data_detail_u0[] slip_data_list_u0 { get; set; }
    [XmlElement("en_slip_data_list_u0")]
    public en_slip_data_detail_u0[] en_slip_data_list_u0 { get; set; }
    [XmlElement("search_slip_data_list")]
    public search_slip_data_detail[] search_slip_data_list { get; set; }

    public es_paytype_m0_detail[] es_paytype_m0_list { get; set; }
    public es_paycde_m0_detail[] es_paycde_m0_list { get; set; }
}

[Serializable]
public class slip_data_detail_u0
{
    public int u0_idx { get; set; }

    public int emp_idx { get; set; }
    public string emp_code { get; set; }
    public string emp_name_th { get; set; }
    public int org_idx { get; set; }
    public string org_name_th { get; set; }
    public int rdept_idx { get; set; }
    public string dept_name_th { get; set; }
    public int rsec_idx { get; set; }
    public string sec_name_th { get; set; }
    public int rpos_idx { get; set; }
    public string pos_name_th { get; set; }

    [XmlElement("personal_email")]
    public string personal_email { get; set; }
    public string account_no { get; set; }
    public string start_date { get; set; }
    
    public decimal salary_rate { get; set; } //อัตรา
    public decimal slip_days { get; set; } //วัน
    public decimal slip_e_salary { get; set; } //เงินเดือน
    public decimal slip_e_position { get; set; } //ตำแหน่ง
    public decimal slip_e_ot10 { get; set; } //OTx1
    public decimal slip_e_ot15 { get; set; } //OTx1.5
    public decimal slip_e_ot20 { get; set; } //OTx2
    public decimal slip_e_ot30 { get; set; } //OTx3
    public decimal slip_e_living { get; set; } //ค่าครองชีพ
    public decimal slip_e_telephone { get; set; } //ค่าโทรศัพท์
    public decimal slip_e_allowance { get; set; } //เบี้ยขยัน
    public decimal slip_e_operating { get; set; } //ค่าปฏิบัติงาน
    public decimal slip_e_travel { get; set; } //ค่าเดินทาง
    public decimal slip_e_commission { get; set; } //ค่าคอมมิชชั่น
    public decimal slip_e_incentive { get; set; } //Incentive
    public decimal slip_e_guarantee { get; set; } //คืนค้ำประกัน
    public decimal slip_e_bonus { get; set; } //โบนัส
    public decimal slip_e_other { get; set; } //เงินได้อื่น
    public decimal slip_earnings { get; set; } //รวมรับ

    public decimal slip_d_absent { get; set; } //ขาดงาน
    public decimal slip_d_late { get; set; } //มาสาย
    public decimal slip_d_goback { get; set; } //หักกลับก่อน
    public decimal slip_d_over { get; set; } //หักลาเกินสิทธิ์
    public decimal slip_d_broken { get; set; } //หักบกพร่อง
    public decimal slip_d_aia { get; set; } //หักAIA
    public decimal slip_d_uniform { get; set; } //หักเครื่องแบบ
    public decimal slip_d_welfare { get; set; } //หักสวัสดิการ
    public decimal slip_d_damage { get; set; } //ของเสียหาย
    public decimal slip_d_other { get; set; } //หักอื่นๆ
    public decimal slip_d_tax { get; set; } //หักภาษี
    public decimal slip_d_fund { get; set; } //หักกองทุน
    public decimal slip_d_social { get; set; } //หักประกันสังคม
    public decimal slip_d_guarantee { get; set; } //ค้ำประกัน
    public decimal slip_d_loan { get; set; } //หักเงินกู้
    public decimal slip_d_legal { get; set; } //หักเงินกรมบังคับคดี
    public decimal slip_deduction { get; set; } //รวมจ่าย
    public decimal slip_net { get; set; } //สุทธิ

    [XmlElement("payroll_date")]
    public string payroll_date { get; set; } //วันที่จ่าย
    [XmlElement("slip_password")]
    public string slip_password { get; set; }
    public int cemp_idx { get; set; }
    public int u0_status { get; set; }

    [XmlElement("slip_mode")]
    public int slip_mode { get; set; }
}

[Serializable]
public class en_slip_data_detail_u0 : slip_data_detail_u0
{
    public string en_salary_rate { get; set; } //อัตรา
    public string en_slip_days { get; set; } //วัน
    public string en_slip_e_salary { get; set; } //เงินเดือน
    public string en_slip_e_position { get; set; } //ตำแหน่ง
    public string en_slip_e_ot10 { get; set; } //OTx1
    public string en_slip_e_ot15 { get; set; } //OTx1.5
    public string en_slip_e_ot20 { get; set; } //OTx2
    public string en_slip_e_ot30 { get; set; } //OTx3
    public string en_slip_e_living { get; set; } //ค่าครองชีพ
    public string en_slip_e_telephone { get; set; } //ค่าโทรศัพท์
    public string en_slip_e_allowance { get; set; } //เบี้ยขยัน
    public string en_slip_e_operating { get; set; } //ค่าปฏิบัติงาน
    public string en_slip_e_travel { get; set; } //ค่าเดินทาง
    public string en_slip_e_commission { get; set; } //ค่าคอมมิชชั่น
    public string en_slip_e_incentive { get; set; } //Incentive
    public string en_slip_e_guarantee { get; set; } //คืนค้ำประกัน
    public string en_slip_e_bonus { get; set; } //โบนัส
    public string en_slip_e_other { get; set; } //เงินได้อื่น
    public string en_slip_earnings { get; set; } //รวมรับ

    public string en_slip_d_absent { get; set; } //ขาดงาน
    public string en_slip_d_late { get; set; } //มาสาย
    public string en_slip_d_goback { get; set; } //หักกลับก่อน
    public string en_slip_d_over { get; set; } //หักลาเกินสิทธิ์
    public string en_slip_d_broken { get; set; } //หักบกพร่อง
    public string en_slip_d_aia { get; set; } //หักAIA
    public string en_slip_d_uniform { get; set; } //หักเครื่องแบบ
    public string en_slip_d_welfare { get; set; } //หักสวัสดิการ
    public string en_slip_d_damage { get; set; } //ของเสียหาย
    public string en_slip_d_other { get; set; } //หักอื่นๆ
    public string en_slip_d_tax { get; set; } //หักภาษี
    public string en_slip_d_fund { get; set; } //หักกองทุน
    public string en_slip_d_social { get; set; } //หักประกันสังคม
    public string en_slip_d_guarantee { get; set; } //ค้ำประกัน
    public string en_slip_d_loan { get; set; } //หักเงินกู้
    public string en_slip_d_legal { get; set; } //หักเงินกรมบังคับคดี
    public string en_slip_deduction { get; set; } //รวมจ่าย
    public string en_slip_net { get; set; } //สุทธิ
}

[Serializable]
public class search_slip_data_detail
{
    public string s_u0_idx { get; set; }

    public string s_emp_idx { get; set; }
    public string s_org_idx { get; set; }
    public string s_rdept_idx { get; set; }
    public string s_rsec_idx { get; set; }
    public string s_rpos_idx { get; set; }

    public string s_slip_year { get; set; }
    public string s_slip_mode { get; set; }
}

[Serializable]
public class es_paytype_m0_detail
{
    public int m0_idx { get; set; }
    public string type_name { get; set; }
    public int m0_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

[Serializable]
public class es_paycde_m0_detail
{
    public int m0_idx { get; set; }
    public string code_no { get; set; }
    public string code_name { get; set; }
    public int code_length { get; set; }
    public int display_length { get; set; }
    public int paytype_idx { get; set; }
    public int number_flag { get; set; }
    public int number_dec { get; set; }
    public int balance_flag { get; set; }
    public int balance_dec { get; set; }
    public string paycode_comment { get; set; }
    public int m0_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }

    public string paytype_name { get; set; }
}