using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_tpm_report")]
public class data_tpm_report
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public tpm_report_detail[] tpm_report_list { get; set; }
    public search_tpm_report_detail[] search_tpm_report_list { get; set; }
    public tpm_permission_detail[] tpm_permission_list { get; set; }

}

[Serializable]
public class tpm_report_detail
{
    public int u0_docidx { get; set; }
    public int emp_idx { get; set; }
    public string emp_code { get; set; }
    public string emp_name_th { get; set; }
    public int org_idx { get; set; }
    public string org_name_th { get; set; }
    public int rdept_idx { get; set; }
    public string dept_name_th { get; set; }
    public int rsec_idx { get; set; }
    public string sec_name_th { get; set; }
    public int rpos_idx { get; set; }
    public string pos_name_th { get; set; }
    public int group_idx { get; set; }
    public string group_name { get; set; }

    public decimal cal_1 { get; set; }
    public decimal cal_2 { get; set; }
    public decimal cal_3 { get; set; }
    public decimal cal_4 { get; set; }
    public decimal cal_5 { get; set; }

    public decimal sum_cal_1 { get; set; }
    public decimal sum_cal_2 { get; set; }
    public decimal sum_cal_3 { get; set; }
    public decimal sum_cal_4 { get; set; }
    public decimal sum_cal_5 { get; set; }

    public decimal score_cal_1 { get; set; }
    public decimal score_cal_2 { get; set; }
    public decimal score_cal_3 { get; set; }
    public decimal score_cal_4 { get; set; }
    public decimal score_cal_5 { get; set; }

    public int count_competency { get; set; }
}

[Serializable]
public class search_tpm_report_detail
{
    public string s_emp_idx { get; set; }
    public string s_report_mode { get; set; }
}

[Serializable]
public class tpm_permission_detail
{
    public int u0_idx { get; set; }
    public int emp_idx { get; set; }
    public int rpos_idx { get; set; }
    public string s_org_idx { get; set; }
    public int permission_level { get; set; }
    public int permission_status { get; set; }
}