﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.Configuration;

/// <summary>
/// Summary description for api_roombooking
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_roombooking : System.Web.Services.WebService
{


    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_roombooking _data_roombooking = new data_roombooking();
    service_mail _serviceMail = new service_mail();

    //sent e - mail
    string _local_xml = "";
    string _mail_subject = "";
    string _mail_body = "";

    string document_code_mail = "";
    string test_details_mail = "";
    string details_mail = "";
    string create_date_mail = "";

    string email_cemp = "";
    string email_hr = "";
    string email_hr_check = "";
    string email_cemptest = "";




    //string _email_qa = "qa_lab@taokaenoi.co.th";

    //static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    string _keynetworkroombooking = "passwordroombooking";//ConfigurationManager.AppSettings["keynetworkroombooking"];

    //string link_system_roombooking = "http://localhost/mas.taokaenoi.co.th/room-booking/";
    //string link_system_roombooking = "http://dev.taokaenoi.co.th/room-booking/";
    string link_system_roombooking = "http://mas.taokaenoi.co.th/room-booking/";



    public api_roombooking()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    // insert and update masterdata place roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRbkm0Place(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 110); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert and update masterdata equiment roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRbkm0Equiment(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 111); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert and update masterdata result use roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRbkm0ResultUse(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 112); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert and update masterdata food use roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRbkm0Food(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 113); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert and update masterdata room use roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRbkm0Room(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 114); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRoomBooking(string jsonIn)
    {
        if (jsonIn != null)
        {

            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_roombooking _data_roombooking = new data_roombooking();
            _data_roombooking = (data_roombooking)_funcTool.convertXmlToObject(typeof(data_roombooking), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 120); // return w/ json

            try
            {

                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_roombooking = (data_roombooking)_funcTool.convertXmlToObject(typeof(data_roombooking), _local_xml);

                if (_data_roombooking.return_code == 0) //user create
                {

                    string u0_docidx = _data_roombooking.rbk_u0_roombooking_list[0].u0_document_idx.ToString();
                    string link = link_system_roombooking + _funcTool.getEncryptRC4(u0_docidx, _keynetworkroombooking);

                    //check e-mail null
                    if (_data_roombooking.rbk_u0_roombooking_list[0].emp_email != null && _data_roombooking.rbk_u0_roombooking_list[0].emp_email != "-")
                    {
                        email_cemptest = "webmaster@taokaenoi.co.th";
                        email_cemp = _data_roombooking.rbk_u0_roombooking_list[0].emp_email.ToString(); //ของจริง
                    }

                    //place detail
                    int place_idx = _data_roombooking.rbk_u0_roombooking_list[0].place_idx;
                    string place_detail = _data_roombooking.rbk_u0_roombooking_list[0].place_name.ToString();
                    string emp_name_create = _data_roombooking.rbk_u0_roombooking_list[0].emp_name_th.ToString();

                    //string replyempcreate = "sirinyamod@hotmail.com";

                    if (place_idx == 6)
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "callcenter@drtobi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 4) //rjn
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_rjn@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 7) //ncp
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "yingwalai.ncp@gmail.com,duangduean.k@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 8 || place_idx == 9) //online
                    {
                        email_hr = "basis@taokaenoi.co.th,Sr.infra_mis@taokaenoi.co.th,it_support@taokaenoi.co.th,senior_support@taokaenoi.co.th";
                    }
                    else if (place_idx == 5) //mtt
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_mtt@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else
                    {
                        //email_hr = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }

                    string email = email_hr + "," + email_cemp + "," + email_cemptest; //ใช้ทดสอบ 
                                                                                       //string email = email_cemp + "," + _email_qa; // ของจริง
                    rbk_u0_roombooking_detail _temp_u0 = _data_roombooking.rbk_u0_roombooking_list[0];
                    //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการจองห้องประชุม";
                    _mail_subject = "[" + place_detail + "]" + " " + "[HR : RoomBooking ] - แจ้งรายการจองห้องประชุม" + " โดย " + emp_name_create;
                    _mail_body = _serviceMail.SentMailRoomBookingCreate(_data_roombooking.rbk_u0_roombooking_list[0], link);

                    _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);
                }
                else if (_data_roombooking.return_code == 2) //user edit
                {

                    string u0_docidx = _data_roombooking.rbk_u0_roombooking_list[0].u0_document_idx.ToString();

                    string link = link_system_roombooking + _funcTool.getEncryptRC4(u0_docidx, _keynetworkroombooking);
                    //string link = link_system_roombooking;// + _funcTool.getEncryptRC4(u0_docidx, _keynetworkroombooking);

                    //check e-mail null
                    if (_data_roombooking.rbk_u0_roombooking_list[0].emp_email != null && _data_roombooking.rbk_u0_roombooking_list[0].emp_email != "-")
                    {
                        email_cemptest = "webmaster@taokaenoi.co.th";
                        email_cemp = _data_roombooking.rbk_u0_roombooking_list[0].emp_email.ToString(); //ของจริง
                    }

                    //place detail
                    int place_idx = _data_roombooking.rbk_u0_roombooking_list[0].place_idx;
                    string place_detail = _data_roombooking.rbk_u0_roombooking_list[0].place_name.ToString();
                    string emp_nameuser_edit = _data_roombooking.rbk_u0_roombooking_list[0].emp_name_th.ToString();
                    //string replyempcreate = "sirinyamod@hotmail.com";

                    if (place_idx == 6)
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "callcenter@drtobi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 4) //rjn
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_rjn@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 7) //ncp
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "yingwalai.ncp@gmail.com,duangduean.k@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 8 || place_idx == 9) //online
                    {
                        email_hr = "it_support@taokaenoi.co.th,senior_support@taokaenoi.co.th,basis@taokaenoi.co.th,Sr.infra_mis @taokaenoi.co.th";
                    }
                    else if (place_idx == 5) //mtt
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_mtt@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else
                    {
                        //email_hr = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }

                    string email = email_hr + "," + email_cemp + "," + email_cemptest; //ใช้ทดสอบ
                                                                                       //string email = email_cemp + "," + _email_qa; // ของจริง
                    rbk_u0_roombooking_detail _temp_u0 = _data_roombooking.rbk_u0_roombooking_list[0];
                    //_mail_subject = "[HR : RoomBooking ] - แจ้งเปลี่ยนแปลงรายการจองห้องประชุม";
                    _mail_subject = "[" + place_detail + "]" + " " + "[HR : RoomBooking ] - แจ้งเปลี่ยนแปลงรายการจองห้องประชุม" + " โดย " + emp_nameuser_edit;

                    _mail_body = _serviceMail.SentMailRoomBookingUserEdit(_data_roombooking.rbk_u0_roombooking_list[0], link);

                    _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);
                }
                else if (_data_roombooking.return_code == 3) //hr edit
                {

                    string u0_docidx = _data_roombooking.rbk_u0_roombooking_list[0].u0_document_idx.ToString();
                    string link_hredit = link_system_roombooking + _funcTool.getEncryptRC4(u0_docidx, _keynetworkroombooking);

                    //check e-mail null
                    if (_data_roombooking.rbk_u0_roombooking_list[0].emp_email != null && _data_roombooking.rbk_u0_roombooking_list[0].emp_email != "-")
                    {
                        email_cemptest = "webmaster@taokaenoi.co.th";
                        email_cemp = _data_roombooking.rbk_u0_roombooking_list[0].emp_email.ToString(); //ของจริง
                    }

                    //place detail
                    int place_idx = _data_roombooking.rbk_u0_roombooking_list[0].place_idx;
                    string place_detail = _data_roombooking.rbk_u0_roombooking_list[0].place_name.ToString();
                    string emp_namehr_edit = _data_roombooking.rbk_u0_roombooking_list[0].emp_name_th.ToString();
                    //string replyempcreate = "sirinyamod@hotmail.com";

                    if (place_idx == 6)
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "callcenter@drtobi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 4) //rjn
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_rjn@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 7) //ncp
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "yingwalai.ncp@gmail.com,duangduean.k@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 8 || place_idx == 9) //online
                    {
                        email_hr = "it_support@taokaenoi.co.th,senior_support@taokaenoi.co.th,basis@taokaenoi.co.th,Sr.infra_mis @taokaenoi.co.th";
                    }
                    else if (place_idx == 5) //mtt
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_mtt@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else
                    {
                        //email_hr = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }

                    //string replyempcreate = "sirinyamod@hotmail.com";
                    string email = email_hr + "," + email_cemp + "," + email_cemptest; //ใช้ทดสอบ
                                                                                       //string email = email_cemp + "," + _email_qa; // ของจริง
                    rbk_u0_roombooking_detail _temp_u0 = _data_roombooking.rbk_u0_roombooking_list[0];
                    //_mail_subject = "[HR : RoomBooking ] - แจ้งเปลี่ยนแปลงรายการจองห้องประชุม";
                    _mail_subject = "[" + place_detail + "]" + " " + "[HR : RoomBooking ] - แจ้งเปลี่ยนแปลงรายการจองห้องประชุม" + " โดย " + emp_namehr_edit;

                    _mail_body = _serviceMail.SentMailRoomBookingHREdit(_data_roombooking.rbk_u0_roombooking_list[0], link_hredit);

                    _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);
                }
            }
            catch { }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // set Approve Wait RoomBooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetApproveWaitRoomBooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_roombooking _data_roombooking = new data_roombooking();
            _data_roombooking = (data_roombooking)_funcTool.convertXmlToObject(typeof(data_roombooking), _xml_in);


            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 121); // return w/ json

            try
            {
                // check return value then send mail
                _local_xml = _funcTool.convertJsonToXml(_ret_val);
                _data_roombooking = (data_roombooking)_funcTool.convertXmlToObject(typeof(data_roombooking), _local_xml);

                if (_data_roombooking.return_code == 0 && _data_roombooking.return_msg != "null") //hr edit
                {
                    // u0 //
                    //email_hr = "waraporn.teoy@gmail.com";
                    //email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 

                    string u0_docidx = _data_roombooking.rbk_u0_roombooking_list[0].u0_document_idx.ToString();
                    string link = link_system_roombooking + _funcTool.getEncryptRC4(u0_docidx, _keynetworkroombooking);

                    //check e-mail null
                    if (_data_roombooking.rbk_u0_roombooking_list[0].emp_email != null && _data_roombooking.rbk_u0_roombooking_list[0].emp_email != "-")
                    {
                        email_cemptest = "webmaster@taokaenoi.co.th"; //ใช้ทดสอบ
                        email_cemp = _data_roombooking.rbk_u0_roombooking_list[0].emp_email.ToString(); //ของจริง
                    }

                    //place detail
                    int place_idx = _data_roombooking.rbk_u0_roombooking_list[0].place_idx;
                    string place_detail = _data_roombooking.rbk_u0_roombooking_list[0].place_name.ToString();

                    //string replyempcreate = "sirinyamod@hotmail.com";

                    if (place_idx == 6)
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "callcenter@drtobi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 4) //rjn
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_rjn@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 7) //ncp
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "yingwalai.ncp@gmail.com,duangduean.k@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 8 || place_idx == 9) //online
                    {
                        email_hr = "it_support@taokaenoi.co.th,senior_support @taokaenoi.co.th,basis@taokaenoi.co.th,Sr.infra_mis @taokaenoi.co.th";
                    }
                    else if (place_idx == 5) //mtt
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_mtt@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else
                    {
                        //email_hr = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }

                    //string replyempcreate = "sirinyamod@hotmail.com";
                    string email = email_hr + "," + email_cemp + "," + email_cemptest; //ใช้ทดสอบ
                                                                                       //string email = email_cemp + "," + _email_qa; // ของจริง
                    rbk_u0_roombooking_detail _temp_u0 = _data_roombooking.rbk_u0_roombooking_list[0];
                    //_mail_subject = "[HR : RoomBooking ] - แจ้งพิจารณารายการจองห้องประชุม";
                    _mail_subject = "[" + place_detail + "]" + " " + "[HR : RoomBooking ] - แจ้งพิจารณารายการจองห้องประชุม";

                    _mail_body = _serviceMail.SentMailRoomBookingHRApprove(_data_roombooking.rbk_u0_roombooking_list[0], link);

                    _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);
                    // u0 //

                    // u1 //
                    //if (_data_roombooking.rbk_u1_roombooking_list.Length != 0 && _data_roombooking.rbk_u1_roombooking_list != null)
                    if (_data_roombooking.return_msg != "null")
                    {
                        int count = _data_roombooking.rbk_u1_roombooking_list.Length;
                        int i = 0;
                        foreach (var data in _data_roombooking.rbk_u1_roombooking_list)
                        {
                            //email_hr = "waraporn.teoy@gmail.com";
                            //email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 

                            string u1_docidx = _data_roombooking.rbk_u1_roombooking_list[i].u0_document_idx.ToString();
                            string link_u1 = link_system_roombooking + _funcTool.getEncryptRC4(u1_docidx, _keynetworkroombooking);

                            //place detail
                            int place_idx_replace = _data_roombooking.rbk_u1_roombooking_list[i].place_idx;
                            string place_detail_replace = _data_roombooking.rbk_u1_roombooking_list[i].place_name.ToString();


                            if (place_idx_replace == 6)
                            {
                                //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                                email_hr = "callcenter@drtobi.co.th"; //ใช้ทดสอบ
                            }
                            else if (place_idx_replace == 4) //rjn
                            {
                                //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                                email_hr = "hr_admin_rjn@taokaenoi.co.th"; //ใช้ทดสอบ 
                            }
                            else if (place_idx_replace == 7) //ncp
                            {
                                //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                                email_hr = "yingwalai.ncp@gmail.com,duangduean.k@taokaenoi.co.th"; //ใช้ทดสอบ 
                            }
                            else if (place_idx_replace == 8 || place_idx_replace == 9) //online
                            {
                                email_hr = "basis@taokaenoi.co.th,Sr.infra_mis@taokaenoi.co.th,it_support@taokaenoi.co.th,senior_support@taokaenoi.co.th";
                            }
                            else if (place_idx_replace == 5) //mtt
                            {
                                //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                                email_hr = "hr_admin_mtt@taokaenoi.co.th"; //ใช้ทดสอบ 
                            }
                            else
                            {
                                //email_hr = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ hr
                                email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 
                            }


                            if (_data_roombooking.rbk_u1_roombooking_list[i].emp_email != null && _data_roombooking.rbk_u1_roombooking_list[i].emp_email != "-")
                            {
                                email_cemptest = "webmaster@taokaenoi.co.th"; //ใช้ทดสอบ
                                email_cemp = _data_roombooking.rbk_u1_roombooking_list[i].emp_email.ToString(); //ของจริง
                            }

                            //string replyempcreate = "sirinyamod@hotmail.com";
                            string email_u1 = email_hr + "," + email_cemp + "," + email_cemptest; //ใช้ทดสอบ
                                                                                                  //string email = email_cemp + "," + _email_qa; // ของจริง
                            rbk_u1_roombooking_detail _temp_u1 = _data_roombooking.rbk_u1_roombooking_list[i];
                            _mail_subject = "[" + place_detail_replace + "]" + " " + "[HR : RoomBooking ] - แจ้งรายการถูกแทนที่การจองห้องประชุม";
                            //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการถูกแทนที่การจองห้องประชุม";

                            _mail_body = _serviceMail.SentMailRoomBookingHRApproveReplace(_data_roombooking.rbk_u1_roombooking_list[i], link_u1);

                            _serviceMail.SendHtmlFormattedEmailFull(email_u1, "", "", _mail_subject, _mail_body);

                            i++;
                        }
                    }
                    // u1 //

                }
                else if (_data_roombooking.return_code == 0 && _data_roombooking.return_msg == "null") //hr edit
                {
                    // u0 //
                    //email_hr = "waraporn.teoy@gmail.com";
                    //email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 

                    string u0_docidx = _data_roombooking.rbk_u0_roombooking_list[0].u0_document_idx.ToString();
                    string link = link_system_roombooking + _funcTool.getEncryptRC4(u0_docidx, _keynetworkroombooking);
                    string email = "";
                    email_cemptest = "webmaster@taokaenoi.co.th"; //ใช้ทดสอบ

                    int place_idx_detail = _data_roombooking.rbk_u0_roombooking_list[0].place_idx;
                    string place_detail = _data_roombooking.rbk_u0_roombooking_list[0].place_name.ToString();

                    //place detail
                    if (place_idx_detail == 6)
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "callcenter@drtobi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx_detail == 4) //rjn
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_rjn@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx_detail == 7) //ncp
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "yingwalai.ncp@gmail.com,duangduean.k@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx_detail == 8 || place_idx_detail == 9) //online
                    {
                        email_hr = "it_support@taokaenoi.co.th,senior_support@taokaenoi.co.th,basis@taokaenoi.co.th,Sr.infra_mis @taokaenoi.co.th";
                    }
                    else if (place_idx_detail == 5) //mtt
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_mtt@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else
                    {
                        //email_hr = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }

                    //check e-mail null
                    if (_data_roombooking.rbk_u0_roombooking_list[0].emp_email != null && _data_roombooking.rbk_u0_roombooking_list[0].emp_email != "-")
                    {
                        email_cemp = _data_roombooking.rbk_u0_roombooking_list[0].emp_email.ToString(); //ของจริง

                        //string replyempcreate = "sirinyamod@hotmail.com";
                        email = email_hr + "," + email_cemp + "," + email_cemptest; //ใช้ทดสอบ

                        //  email = email_cemptest;
                    }
                    else
                    {
                        email = email_hr + "," + email_cemptest; //ใช้ทดสอบ

                        // email = "kantida3620@gmail.com";
                    }

                    //string email = email_cemp + "," + _email_qa; // ของจริง
                    rbk_u0_roombooking_detail _temp_u0 = _data_roombooking.rbk_u0_roombooking_list[0];
                    //_mail_subject = "[HR : RoomBooking ] - แจ้งพิจารณารายการจองห้องประชุม";
                    _mail_subject = "[" + place_detail + "]" + " " + "[HR : RoomBooking ] - แจ้งพิจารณารายการจองห้องประชุม";

                    _mail_body = _serviceMail.SentMailRoomBookingHRApprove(_data_roombooking.rbk_u0_roombooking_list[0], link);

                    _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);
                    //// u0 //

                }
                else if (_data_roombooking.return_code == 4) //hr cancel
                {
                    //email_hr = "waraporn.teoy@gmail.com";
                    //email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 

                    string u0_docidx = _data_roombooking.rbk_u0_roombooking_list[0].u0_document_idx.ToString();
                    string link = link_system_roombooking + _funcTool.getEncryptRC4(u0_docidx, _keynetworkroombooking);
                    //string link = link_system_roombooking;// + _funcTool.getEncryptRC4(u0_docidx, _keynetworkroombooking);

                    //check e-mail null
                    if (_data_roombooking.rbk_u0_roombooking_list[0].emp_email != null && _data_roombooking.rbk_u0_roombooking_list[0].emp_email != "-")
                    {
                        email_cemptest = "webmaster@taokaenoi.co.th"; //ใช้ทดสอบ
                        email_cemp = _data_roombooking.rbk_u0_roombooking_list[0].emp_email.ToString(); //ของจริง
                    }


                    //place detail
                    int place_idx = _data_roombooking.rbk_u0_roombooking_list[0].place_idx;
                    string place_detail = _data_roombooking.rbk_u0_roombooking_list[0].place_name.ToString();

                    //string replyempcreate = "sirinyamod@hotmail.com";

                    if (place_idx == 6)
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "callcenter@drtobi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 4) //rjn
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_rjn@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 7) //ncp
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "yingwalai.ncp@gmail.com,duangduean.k@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx == 8 || place_idx == 9) //online
                    {
                        email_hr = "it_support@taokaenoi.co.th,senior_support@taokaenoi.co.th,basis@taokaenoi.co.th,Sr.infra_mis @taokaenoi.co.th";
                    }
                    else if (place_idx == 5) //mtt
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_mtt@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else
                    {
                        //email_hr = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }

                    //string replyempcreate = "sirinyamod@hotmail.com";
                    string email = email_hr + "," + email_cemp + "," + email_cemptest; //ใช้ทดสอบ
                                                                                       //string email = email_cemp + "," + _email_qa; // ของจริง
                    rbk_u0_roombooking_detail _temp_u0 = _data_roombooking.rbk_u0_roombooking_list[0];
                    //_mail_subject = "[HR : RoomBooking ] - ยกเลิกรายการจองห้องประชุม";
                    _mail_subject = "[" + place_detail + "]" + " " + "[HR : RoomBooking ] - ยกเลิกรายการจองห้องประชุม";

                    _mail_body = _serviceMail.SentMailRoomBookingUserCancel(_data_roombooking.rbk_u0_roombooking_list[0], link);

                    _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);

                }
                else if (_data_roombooking.return_code == 2) //user cancel
                {
                    //email_hr = "waraporn.teoy@gmail.com";
                    //email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 

                    string u0_docidx = _data_roombooking.rbk_u0_roombooking_list[0].u0_document_idx.ToString();
                    string link = link_system_roombooking + _funcTool.getEncryptRC4(u0_docidx, _keynetworkroombooking);
                    //string link = link_system_roombooking;// + _funcTool.getEncryptRC4(u0_docidx, _keynetworkroombooking);

                    //check e-mail null
                    if (_data_roombooking.rbk_u0_roombooking_list[0].emp_email != null && _data_roombooking.rbk_u0_roombooking_list[0].emp_email != "-")
                    {
                        email_cemptest = "webmaster@taokaenoi.co.th"; //ใช้ทดสอบ
                        email_cemp = _data_roombooking.rbk_u0_roombooking_list[0].emp_email.ToString(); //ของจริง
                    }

                    //place detail
                    int place_idx_detail = _data_roombooking.rbk_u0_roombooking_list[0].place_idx;
                    string place_detail = _data_roombooking.rbk_u0_roombooking_list[0].place_name.ToString();

                    //place detail
                    if (place_idx_detail == 6)
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "callcenter@drtobi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx_detail == 4) //rjn
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_rjn@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx_detail == 7) //ncp
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "yingwalai.ncp@gmail.com,duangduean.k@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else if (place_idx_detail == 8 || place_idx_detail == 9) //online
                    {
                        email_hr = "it_support@taokaenoi.co.th,senior_support@taokaenoi.co.th,basis@taokaenoi.co.th,Sr.infra_mis @taokaenoi.co.th";
                    }
                    else if (place_idx_detail == 5) //mtt
                    {
                        //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_mtt@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }
                    else
                    {
                        //email_hr = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ hr
                        email_hr = "hr_admin_center@taokaenoi.co.th"; //ใช้ทดสอบ 
                    }

                    //string replyempcreate = "sirinyamod@hotmail.com";
                    string email = email_hr + "," + email_cemp + "," + email_cemptest; //ใช้ทดสอบ
                                                                                       //string email = email_cemp + "," + _email_qa; // ของจริง
                    rbk_u0_roombooking_detail _temp_u0 = _data_roombooking.rbk_u0_roombooking_list[0];
                    //_mail_subject = "[HR : RoomBooking ] - ยกเลิกรายการจองห้องประชุม";
                    _mail_subject = "[" + place_detail + "]" + " " + "[HR : RoomBooking ] - ยกเลิกรายการจองห้องประชุม";

                    _mail_body = _serviceMail.SentMailRoomBookingUserCancel(_data_roombooking.rbk_u0_roombooking_list[0], link);

                    _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);
                }
            }
            catch { }


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRbkm0Conference_Devices(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 124); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRbkm0Conference_Devices(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 216); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DeleteRbkm0Conference_Devices(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 915); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master place roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRbkm0Place(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master equiment roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRbkm0Equiment(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 211); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master result use roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRbkm0ResultUse(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 212); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master food use roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRbkm0Food(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 213); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master room roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRbkm0Room(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 214); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in detail equiment roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRbkm0EquimentDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 215); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in detail Room Where Place roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRbkm0RoomDetailInPlace(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Search Room Create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRbkSearchRommCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 221); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Check Room In Create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCheckRoomInCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 222); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Search Detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchDetailRbk(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 223); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Select Detail Room Booking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailRoomBooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 230); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Select View Detail Room Booking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetViewDetailRoomBooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 231); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Select Wait Approve Detail Room Booking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetWaitApproveRoomBooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 232); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Select Count Wait Approve Detail Room Booking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCountWaitApproveRoomBooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 233); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Select View Wait Approve Detail Room Booking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetViewWaitApproveRoomBooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 234); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Select View Food Detail In RoomBooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetViewFoodDetailRbk(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 235); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Search Detail Approve In RoomBooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchDetailApproveRbk(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 236); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Select Log View Detail Room Booking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLogDetailRoomBooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 240); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Select Decision Detail Approve
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDecisionApprove(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 250); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Select Status Search Detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetStatusSearchDetailRbk(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 251); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Select Report Table Detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReportRoomTableRbk(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 260); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Select Report Graph Detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReportRoomGraphRbk(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 261); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Select Show Slider Room Detai
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetDetailRoomShowSlider(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 270); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Check Permission Roomboking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPermissionRoomBooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 271); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // delete detail in master place roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRbkm0PlaceDel(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 910); // return w/ json

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // delete detail in master equiment roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRbkm0EquimentDel(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 911); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // delete detail in master result use roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRbkm0ResultUseDel(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 912); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // delete detail in master food use roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRbkm0FoodDel(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 913); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // delete detail in master room use roombooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRbkm0RoomDel(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 914); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // search for show on calendar by PHON 2018-10-26
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRbkU0RoomOnCalendar(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 224); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // search for show on calendar by PHON 2018-10-26
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRbkU0RoomOnCalendar_mobile(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_roombooking", "service_roombooking", _xml_in, 225); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
}
