﻿using System;
using System.Xml.Serialization;
/// <summary>
/// Summary description for data_roombooking
/// </summary>
/// 

[Serializable]
[XmlRoot("data_overtime")]
public class data_overtime
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public ovt_u0_month_detail[] ovt_u0_month_list { get; set; }
    public ovt_m0_group_detail[] ovt_m0_group_list { get; set; }
    public ovt_m1_group_detail[] ovt_m1_group_list { get; set; }
    public ovt_m0_type_detail[] ovt_m0_type_list { get; set; }
    public ovt_m0_type_daywork_detail[] ovt_m0_type_daywork_list { get; set; }
    public ovt_m0_typeot_detail[] ovt_m0_typeot_list { get; set; }
    public ovt_m0_status_detail[] ovt_m0_status_list { get; set; }


    [XmlElement("ovt_u0_document_list")]
    public ovt_u0_document_detail[] ovt_u0_document_list { get; set; }
    [XmlElement("ovt_u1_document_list")]
    public ovt_u1_document_detail[] ovt_u1_document_list { get; set; }
    public ovt_u2_document_detail[] ovt_u2_document_list { get; set; }

    public ovt_m0_node_decision_detail[] ovt_m0_node_decision_list { get; set; }
    public ovt_employeeset_otmonth_detail[] ovt_employeeset_otmonth_list { get; set; }
    public ovt_timestart_otmonth_detail[] ovt_timestart_otmonth_list { get; set; }
    public ovt_timeend_otmonth_detail[] ovt_timeend_otmonth_list { get; set; }

    //-- import ot day --//
    [XmlElement("ovt_u0_document_day_list")]
    public ovt_u0_document_day_detail[] ovt_u0_document_day_list { get; set; }

    [XmlElement("ovt_u1_document_day_list")]
    public ovt_u1_document_day_detail[] ovt_u1_document_day_list { get; set; }

    [XmlElement("ovt_u2_document_day_list")]
    public ovt_u2_document_day_detail[] ovt_u2_document_day_list { get; set; }

    //-- import ot day --//

    //-- report --//
    public ovt_report_otmonth_detail[] ovt_report_otmonth_list { get; set; }
    //-- report --//

    //-- search --//
    public ovt_search_otmonth_detail[] ovt_search_otmonth_list { get; set; }

    //-- ot day --//
    public ovt_otday_detail[] ovt_otday_list { get; set; }
    public ovt_timestart_otday_detail[] ovt_timestart_otday_list { get; set; }
    public ovt_timeend_otday_detail[] ovt_timeend_otday_list { get; set; }
    public ovt_u0doc_otday_detail[] ovt_u0doc_otday_list { get; set; }
    public ovt_u1doc_otday_detail[] ovt_u1doc_otday_list { get; set; }
    public ovt_permission_admin_detail[] ovt_permission_admin_list { get; set; }
    public ovt_u2doc_otday_detail[] ovt_u2doc_otday_list { get; set; }

    //-- ot shift time rotate --//
    public ovt_shifttime_rotate_detail[] ovt_shifttime_rotate_list { get; set; }
    public ovt_shifttime_rotatestart_detail[] ovt_shifttime_rotatestart_list { get; set; }
    public ovt_shifttime_rotateend_detail[] ovt_shifttime_rotateend_list { get; set; }

    [XmlElement("ovt_u0doc_rotate_list")]
    public ovt_u0doc_rotate_detail[] ovt_u0doc_rotate_list { get; set; }

    [XmlElement("ovt_u1doc_rotate_list")]
    public ovt_u1doc_rotate_detail[] ovt_u1doc_rotate_list { get; set; }

    public ovt_m0type_shift_detail[] ovt_m0type_shift_list { get; set; }


}

public class ovt_m0type_shift_detail
{

    public int TimeIDX { get; set; }
    public string t_type_name { get; set; }
    public int t_status { get; set; }

}

[Serializable]
public class ovt_u1doc_rotate_detail
{

    [XmlElement("emp_type_idx")]
    public int emp_type_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("u0_doc_idx")]
    public int u0_doc_idx { get; set; }

    [XmlElement("u2_doc_idx")]
    public int u2_doc_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("STIDX")]
    public int STIDX { get; set; }

    [XmlElement("datetime_otstart")]
    public string datetime_otstart { get; set; }

    [XmlElement("datetime_start")]
    public string datetime_start { get; set; }

    [XmlElement("datetime_end")]
    public string datetime_end { get; set; }

    [XmlElement("shift_start")]
    public string shift_start { get; set; }

    [XmlElement("shift_end")]
    public string shift_end { get; set; }

    [XmlElement("day_off")]
    public string day_off { get; set; }

    [XmlElement("day_condition")]
    public int day_condition { get; set; }

    [XmlElement("m0_parttime_idx")]
    public int m0_parttime_idx { get; set; }

    [XmlElement("ot_before")]
    public string ot_before { get; set; }

    [XmlElement("hour_ot_before")]
    public string hour_ot_before { get; set; }

    [XmlElement("ot_after")]
    public string ot_after { get; set; }

    [XmlElement("hour_ot_after")]
    public string hour_ot_after { get; set; }

    [XmlElement("ot_holiday")]
    public string ot_holiday { get; set; }

    [XmlElement("hour_ot_holiday")]
    public string hour_ot_holiday { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("doc_code")]
    public string doc_code { get; set; }

    [XmlElement("remark_admin")]
    public string remark_admin { get; set; }

    [XmlElement("comment_head")]
    public string comment_head { get; set; }

    [XmlElement("comment_hr")]
    public string comment_hr { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("emp_time")]
    public string emp_time { get; set; }

    [XmlElement("parttime_start_time")]
    public string parttime_start_time { get; set; }

    [XmlElement("parttime_end_time")]
    public string parttime_end_time { get; set; }

    [XmlElement("parttime_break_start_time")]
    public string parttime_break_start_time { get; set; }

    [XmlElement("parttime_break_end_time")]
    public string parttime_break_end_time { get; set; }

    [XmlElement("holiday_name")]
    public string holiday_name { get; set; }

    [XmlElement("cemp_rsec_idx")]
    public int cemp_rsec_idx { get; set; }

    [XmlElement("parttime_name_th")]
    public string parttime_name_th { get; set; }

    [XmlElement("emp_approve1")]
    public string emp_approve1 { get; set; }

    [XmlElement("emp_approve2")]
    public string emp_approve2 { get; set; }

    [XmlElement("emp_idx_approve1")]
    public int emp_idx_approve1 { get; set; }

    [XmlElement("emp_idx_approve2")]
    public int emp_idx_approve2 { get; set; }

    [XmlElement("date_condition")]
    public int date_condition { get; set; }

    [XmlElement("holiday_idx")]
    public int holiday_idx { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("current_status")]
    public string current_status { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("current_artor")]
    public string current_artor { get; set; }

    [XmlElement("current_decision")]
    public string current_decision { get; set; }

    [XmlElement("current_status_name")]
    public string current_status_name { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("ot_before_max")]
    public string ot_before_max { get; set; }

    [XmlElement("ot_after_max")]
    public string ot_after_max { get; set; }

    [XmlElement("ot_holiday_max")]
    public string ot_holiday_max { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("joblevel")]
    public int joblevel { get; set; }

    [XmlElement("count_head_waitapprove")]
    public int count_head_waitapprove { get; set; }

    [XmlElement("count_hr_waitapprove")]
    public int count_hr_waitapprove { get; set; }

    [XmlElement("count_admin_waitapprove")]
    public int count_admin_waitapprove { get; set; }

    [XmlElement("count_all_waitapprove")]
    public int count_all_waitapprove { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_email_headuser")]
    public string emp_email_headuser { get; set; }

    [XmlElement("emp_email_admin")]
    public string emp_email_admin { get; set; }

    [XmlElement("emp_email_hr")]
    public string emp_email_hr { get; set; }

    [XmlElement("emp_name_th_create")]
    public string emp_name_th_create { get; set; }

    [XmlElement("sec_name_th_create")]
    public string sec_name_th_create { get; set; }

    [XmlElement("time_create_date")]
    public string time_create_date { get; set; }

    [XmlElement("emp_name_th_approve")]
    public string emp_name_th_approve { get; set; }

    [XmlElement("sec_name_th_approve")]
    public string sec_name_th_approve { get; set; }

    [XmlElement("create_date_approve")]
    public string create_date_approve { get; set; }

    [XmlElement("time_approve")]
    public string time_approve { get; set; }

    [XmlElement("time_idx")]
    public int time_idx { get; set; }

    [XmlElement("time_typename")]
    public string time_typename { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }


}

[Serializable]
public class ovt_u0doc_rotate_detail
{

    [XmlElement("u0_doc_idx")]
    public int u0_doc_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("cemp_rsec_idx")]
    public int cemp_rsec_idx { get; set; }

    [XmlElement("cemp_rpos_idx")]
    public int cemp_rpos_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("doc_code")]
    public string doc_code { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("time_create_date")]
    public string time_create_date { get; set; }

}

public class ovt_shifttime_rotateend_detail
{


    public int emp_idx { get; set; }
    public string emp_code { get; set; }
    public string emp_name_th { get; set; }
    public string pos_name_th { get; set; }
    public string emp_time { get; set; }
    public string parttime_start_time { get; set; }
    public string parttime_end_time { get; set; }
    public string parttime_break_start_time { get; set; }
    public string parttime_break_end_time { get; set; }
    public string day_off { get; set; }
    public int date_condition { get; set; }
    public int holiday_idx { get; set; }
    public string diff_minute_start { get; set; }
    public string diff_minute_end { get; set; }
    public string search_date { get; set; }
    public int rpos_idx_admim { get; set; }
    public int cemp_idx { get; set; }
    public string date_ot { get; set; }
    public string parttime_name_th { get; set; }
    public string time_scanin { get; set; }
    public string holiday_name { get; set; }
    public int diff_second_start { get; set; }
    public string diff_second_end { get; set; }
    public string time_scanout { get; set; }
    public string time_shiftstart { get; set; }
    public string time_shiftend { get; set; }

    public int minute_end { get; set; }
    public int minute_start { get; set; }
    public int holiday_check { get; set; }
    public int day_off_check { get; set; }
}

public class ovt_shifttime_rotatestart_detail
{


    public int emp_idx { get; set; }
    public string emp_code { get; set; }
    public string emp_name_th { get; set; }
    public string pos_name_th { get; set; }
    public string emp_time { get; set; }
    public string parttime_start_time { get; set; }
    public string parttime_end_time { get; set; }
    public string parttime_break_start_time { get; set; }
    public string parttime_break_end_time { get; set; }
    public string day_off { get; set; }
    public int date_condition { get; set; }
    public int holiday_idx { get; set; }
    public string diff_minute_start { get; set; }
    public string search_date { get; set; }
    public int rpos_idx_admim { get; set; }
    public int cemp_idx { get; set; }
    public string date_ot { get; set; }
    public string parttime_name_th { get; set; }
    public string time_scanin { get; set; }
    public string holiday_name { get; set; }
    public string time_shiftstart { get; set; }
    public string time_shiftend { get; set; }
    public int diff_second_start { get; set; }
    public string time_scanout { get; set; }
    public int minute_end { get; set; }
    public int minute_start { get; set; }

    public int holiday_check { get; set; }
    public int day_off_check { get; set; }


}

public class ovt_shifttime_rotate_detail
{

    public int emp_idx { get; set; }
    public string emp_code { get; set; }
    public string emp_name_th { get; set; }
    public string pos_name_th { get; set; }
    public string emp_time { get; set; }
    public string parttime_start_time { get; set; }
    public string parttime_end_time { get; set; }
    public string parttime_break_start_time { get; set; }
    public string parttime_break_end_time { get; set; }
    public string day_off { get; set; }
    public int date_condition { get; set; }
    public int holiday_idx { get; set; }
    public string search_date { get; set; }
    public int rpos_idx_admim { get; set; }
    public string date_ot { get; set; }
    public int cemp_idx { get; set; }
    public string parttime_name_th { get; set; }
    public string holiday_name { get; set; }
    public int emp_type_idx { get; set; }
    public int STIDX { get; set; }
    public int m0_parttime_idx { get; set; }
    public int u0_doc_idx { get; set; }
    public int u1_doc_idx { get; set; }
    public string datetime_otstart { get; set; }
    public string datetime_start { get; set; }
    public string datetime_end { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int staidx { get; set; }
    public string remark_admin { get; set; }
    public string comment_head { get; set; }
    public string comment_hr { get; set; }
    public string hour_ot_before { get; set; }
    public string hour_ot_after { get; set; }
    public string hour_ot_holiday { get; set; }
    public string ot_before { get; set; }
    public string ot_after { get; set; }
    public string ot_holiday { get; set; }
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int rpos_idx { get; set; }
    public string emp_idxvalue { get; set; }
    public int day_condition { get; set; }

}

public class ovt_m0_status_detail
{

    public int staidx { get; set; }
    public int cemp_idx { get; set; }
    public int emp_idx { get; set; }
    public string status_name { get; set; }
    public string status_desc { get; set; }
    public int status { get; set; }

}

public class ovt_permission_admin_detail
{

    public int rpos_idx { get; set; }
    public int cemp_idx { get; set; }
    public int emp_idx { get; set; }

}

public class ovt_u0doc_otday_detail
{

    public int u0_docday_idx { get; set; }
    public int cemp_idx { get; set; }
    public int emp_idx { get; set; }
    public string emp_code { get; set; }
    public string create_date { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int staidx { get; set; }
    public string emp_name_th { get; set; }
    public string doc_code { get; set; }
    public int rpos_idx { get; set; }
    public string pos_name_th { get; set; }
    public int time_idx { get; set; }
    public string time_typename { get; set; }
    public string shift_date { get; set; }
    public string shift_createdate { get; set; }
    public string shift_status { get; set; }
    public string announce_diary_date_start { get; set; }
    public string announce_diary_date_end { get; set; }
    public string parttime_name_th { get; set; }
    public string parttime_break_start_time { get; set; }
    public string parttime_break_end_time { get; set; }
    public string parttime_workdays { get; set; }
    public string parttime_day_off { get; set; }
    public string parttime_start_time { get; set; }
    public string parttime_end_time { get; set; }
    public int holiday_idx { get; set; }
    public string type_ot_name { get; set; }
    public int type_ot_status { get; set; }
    public string update_date { get; set; }
    public string date_condition { get; set; }
    public int m0_parttime_idx { get; set; }
    public int STIDX { get; set; }
    public int rsec_idx { get; set; }
    public int decision { get; set; }
    public string time_create_date { get; set; }
    public string org_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string actor_name { get; set; }
    public string status_name { get; set; }
    public string node_name { get; set; }


}

public class ovt_u1doc_otday_detail
{

    public int u1_docday_idx { get; set; }
    public int u0_docday_idx { get; set; }
    public int cemp_idx { get; set; }
    public int emp_idx { get; set; }
    public int condition { get; set; }
    public string emp_code { get; set; }
    public string create_date { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int staidx { get; set; }
    public string emp_name_th { get; set; }
    public string doc_code { get; set; }
    public string datetime_start { get; set; }
    public string datetime_end { get; set; }
    public string datetime_scanot_in { get; set; }
    public string datetime_scanot_out { get; set; }
    public string ot_before { get; set; }
    public string ot_after { get; set; }
    public string ot_holiday { get; set; }
    public int u1_docday_status { get; set; }
    public int rpos_idx { get; set; }
    public string pos_name_th { get; set; }
    public int time_idx { get; set; }
    public string time_typename { get; set; }
    public string shift_date { get; set; }
    public string shift_createdate { get; set; }
    public string shift_status { get; set; }
    public string announce_diary_date_start { get; set; }
    public string announce_diary_date_end { get; set; }
    public string parttime_name_th { get; set; }
    public string parttime_break_start_time { get; set; }
    public string parttime_break_end_time { get; set; }
    public string parttime_workdays { get; set; }
    public string parttime_day_off { get; set; }
    public string parttime_start_time { get; set; }
    public string parttime_end_time { get; set; }
    public int holiday_idx { get; set; }
    public string type_ot_name { get; set; }
    public int type_ot_status { get; set; }
    public string update_date { get; set; }
    public string date_condition { get; set; }
    public int m0_parttime_idx { get; set; }
    public int STIDX { get; set; }
    public int datetime_start_uidx { get; set; }
    public int datetime_end_uidx { get; set; }
    public int datetime_scanot_in_idx { get; set; }
    public int datetime_scanot_out_idx { get; set; }
    public string time_create_date { get; set; }
    public int rsec_idx { get; set; }
    public int decision { get; set; }

    public string org_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string actor_name { get; set; }
    public string status_name { get; set; }
    public string node_name { get; set; }
    public string emp_name_th_admin { get; set; }
    public string org_name_th_admin { get; set; }
    public string dept_name_th_admin { get; set; }
    public string create_date_admin { get; set; }
    public string time_create_date_admin { get; set; }
    public int rdept_idx { get; set; }
    public int joblevel { get; set; }
    public int count_waitapprove { get; set; }
    public int count_waitapprove_head { get; set; }
    public int count_waitapprove_hr { get; set; }
    public int count_waitapprove_adminedit { get; set; }
    public string hours_otbefore { get; set; }
    public string hours_otafter { get; set; }
    public string hours_otholiday { get; set; }
    public string comment_admin { get; set; }
    public string comment_head { get; set; }
    public string comment_hr { get; set; }
    public string comment { get; set; }



}

public class ovt_u2doc_otday_detail
{

    public int u2_docday_idx { get; set; }
    public int u1_docday_idx { get; set; }
    public int u0_docday_idx { get; set; }
    public int cemp_idx { get; set; }
    public int u0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int decision_idx { get; set; }
    public string comment { get; set; }
    public string create_date { get; set; }
    public string time_create { get; set; }
    public string current_artor { get; set; }
    public string current_decision { get; set; }
    public string current_status { get; set; }
    public string current_status_name { get; set; }
    public string update_date { get; set; }
    public int condition { get; set; }
    public int emp_idx { get; set; }
    public string date_start { get; set; }
    public string date_end { get; set; }
    public string time_start { get; set; }
    public string time_end { get; set; }
    public int m0_node_idx { get; set; }
    public int decision { get; set; }
    public int staidx { get; set; }
    public string status_name { get; set; }
    public string actor_name { get; set; }
    public string node_name { get; set; }
    public string emp_name_en { get; set; }
    public string emp_name_th { get; set; }
    public string org_name_en { get; set; }
    public string org_name_th { get; set; }
    public string dept_name_en { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_en { get; set; }
    public string sec_name_th { get; set; }
    public int rpos_idx { get; set; }
    public string pos_name_th { get; set; }
    public int count_waitApprove { get; set; }


}

public class ovt_otday_detail
{

    public int emp_idx { get; set; }
    public int emp_type_idx { get; set; }
    public string emp_code { get; set; }
    public string emp_name_th { get; set; }
    public int rpos_idx { get; set; }
    public string pos_name_th { get; set; }
    public int time_idx { get; set; }
    public string time_typename { get; set; }
    public string shift_date { get; set; }
    public string shift_createdate { get; set; }
    public string shift_status { get; set; }
    public string announce_diary_date_start { get; set; }
    public string announce_diary_date_end { get; set; }
    public string parttime_name_th { get; set; }
    public string parttime_break_start_time { get; set; }
    public string parttime_break_end_time { get; set; }
    public string parttime_workdays { get; set; }
    public string parttime_day_off { get; set; }
    public string parttime_start_time { get; set; }
    public string parttime_end_time { get; set; }
    public int holiday_idx { get; set; }
    public string type_ot_name { get; set; }
    public int type_ot_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public string date_condition { get; set; }
    public int m0_parttime_idx { get; set; }
    public int STIDX { get; set; }
    public string search_date { get; set; }

}

public class ovt_timestart_otday_detail
{

    public int month_idx { get; set; }
    public int uidx { get; set; }
    public int condition { get; set; }
    public string day { get; set; }
    public string date { get; set; }
    public string emp_code { get; set; }
    public string date_start { get; set; }
    public string date_end { get; set; }
    public int emp_idx { get; set; }
    public string dateot_from { get; set; }
    public string datetime_scan { get; set; }
    public string time_start { get; set; }
    public string time_otstart { get; set; }

    public string time_end { get; set; }
    public string time_otend { get; set; }
    public string log_time_start { get; set; }
    public string log_time_end { get; set; }

    public string log_time_otstart { get; set; }
    public string log_time_otend { get; set; }

    public string sum_time { get; set; }
    public string sum_hours_setotmonth { get; set; }
    public string parttime_workdays { get; set; }
    public string parttime_day_off { get; set; }
    public string log_time { get; set; }
    public string parttime_break_start_time { get; set; }
    public string parttime_break_end_time { get; set; }
    public string parttime_start_time { get; set; }
    public string parttime_end_time { get; set; }
    public int m0_parttime_idx { get; set; }

}

public class ovt_timeend_otday_detail
{

    public int month_idx { get; set; }
    public int uidx { get; set; }
    public int condition { get; set; }
    public string day { get; set; }
    public string date { get; set; }
    public string emp_code { get; set; }
    public string date_start { get; set; }
    public string date_end { get; set; }
    public int emp_idx { get; set; }
    public string dateot_from { get; set; }
    public string time_start { get; set; }
    public string time_end { get; set; }
    public string sum_time { get; set; }
    public string sum_hours_setotmonth { get; set; }
    public string parttime_workdays { get; set; }
    public string parttime_day_off { get; set; }
    public string log_time { get; set; }
    public string parttime_break_start_time { get; set; }
    public string parttime_break_end_time { get; set; }
    public string parttime_start_time { get; set; }
    public string parttime_end_time { get; set; }

}

public class ovt_search_otmonth_detail
{

    public int u0_doc_idx { get; set; }
    public string emp_code { get; set; }
    public string ot_before { get; set; }
    public string ot_after { get; set; }
    public string ot_holiday { get; set; }
    public string ot_total { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int condition { get; set; }
    public int month_idx { get; set; }
    public int year_idx { get; set; }
    public string condition_ot { get; set; }
    public string condition_otmonth { get; set; }
    public string remark_otmonth { get; set; }
    public int rsec_idx { get; set; }
    public int rdept_idx { get; set; }
    public int org_idx { get; set; }
    public int emp_idx { get; set; }
    public string costcenter { get; set; }
    public int rpos_idx { get; set; }
    public int emp_type { get; set; }
    public int m0_group_idx { get; set; }

    public int type_idx { get; set; }

    public string ovt_detail { get; set; }
    public int u0_ovt_status { get; set; }

    public int m0_node_idx { get; set; }


    public int m0_actor_idx { get; set; }


    public int decision { get; set; }


    public int staidx { get; set; }


    public string comment { get; set; }

    public string status_name { get; set; }


    public string actor_name { get; set; }


    public string node_name { get; set; }


    public string emp_name_en { get; set; }


    public string emp_name_th { get; set; }


    public string org_name_en { get; set; }

    public string org_name_th { get; set; }


    public string dept_name_en { get; set; }


    public string dept_name_th { get; set; }


    public string sec_name_en { get; set; }

    public string sec_name_th { get; set; }

    public string pos_name_th { get; set; }

    public int month_ot { get; set; }

    public string sumhours { get; set; }
    public string time_create_date { get; set; }

    public int joblevel { get; set; }

    public string month_ot_name { get; set; }


    public string emp_email { get; set; }


    public int count_waitapprove { get; set; }


    public int count_waitapprove_monthly { get; set; }


    public int count_waitapprove_daily { get; set; }


    public string emp_email_headuser { get; set; }


    public string emp_code_head { get; set; }


    public string emp_name_th_head { get; set; }


    public string org_name_th_head { get; set; }

    public string dept_name_th_head { get; set; }


    public string sec_name_th_head { get; set; }

    public string emp_mailhead { get; set; }


    public string pos_name_th_head { get; set; }


    public string current_decision { get; set; }


    public string comment_head { get; set; }


    public string group_name { get; set; }

    public string count_emp { get; set; }


    public int holiday_idx { get; set; }




}

public class ovt_report_otmonth_detail
{

    public int u0_doc_idx { get; set; }
    public string emp_code { get; set; }
    public string ot_before { get; set; }
    public string ot_after { get; set; }
    public string ot_holiday { get; set; }
    public string ot_total { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int condition { get; set; }
    public string condition_ot { get; set; }
    public string condition_otmonth { get; set; }
    public string remark_otmonth { get; set; }
    public int rsec_idx { get; set; }
    public int rdept_idx { get; set; }
    public int org_idx { get; set; }
    public int emp_idx { get; set; }
    public string costcenter { get; set; }
    public string date_payment { get; set; }
    public int time_idx { get; set; }
    public string time_typename { get; set; }
    public string emp_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string ot_x15 { get; set; }
    public string ot_x1 { get; set; }
    public string ot_x2 { get; set; }
    public string ot_x3 { get; set; }
    public string total_hours { get; set; }
    public string date_start { get; set; }
    public string date_end { get; set; }
    public int u0_doc1_idx { get; set; }
    public string u0_doc1_idx_value { get; set; }
    public string datetime_otstart { get; set; }
    public string datetime_start { get; set; }
    public string datetime_end { get; set; }
    public string remark_admin { get; set; }
    public string parttime_name_th { get; set; }
    public string pos_name_th { get; set; }
    public string detail_job { get; set; }
    public string comment { get; set; }
    public string emp_approve1 { get; set; }
    public string emp_approve2 { get; set; }
    public string org_name_th { get; set; }


}

public class ovt_m0_typeot_detail
{

    public int type_ot_idx { get; set; }
    public string type_ot_name { get; set; }
    public int type_ot_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }


}

public class ovt_m0_type_daywork_detail
{

    public int type_daywork_idx { get; set; }
    public string type_daywork_name { get; set; }
    public int type_daywork_status { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }


}

public class ovt_m0_type_detail
{

    public int type_idx { get; set; }
    public string type_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int type_status { get; set; }


}

public class ovt_m0_group_detail
{

    public int m0_group_idx { get; set; }
    public int condition { get; set; }
    public string group_name { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int m0_group_status { get; set; }
    public string count_emp { get; set; }

}

public class ovt_m1_group_detail
{

    public int m1_group_idx { get; set; }
    public int m0_group_idx { get; set; }
    public int condition { get; set; }
    public string group_name { get; set; }
    public string emp_name { get; set; }
    public string emp_code { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int m1_group_status { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_th { get; set; }

    public string emp_name_en { get; set; }
    public string emp_name_th { get; set; }
    public string org_name_en { get; set; }
    public string org_name_th { get; set; }
    public string dept_name_en { get; set; }
    public string sec_name_en { get; set; }
    public int rpos_idx { get; set; }
    public string pos_name_th { get; set; }

}

public class ovt_timestart_otmonth_detail
{

    public int month_idx { get; set; }
    public string day { get; set; }
    public string date { get; set; }
    public string emp_code { get; set; }
    public string date_start { get; set; }
    public int emp_idx { get; set; }
    public string dateot_from { get; set; }
    public string time_start { get; set; }
    public string time_end { get; set; }
    public string sum_time { get; set; }
    public string sum_hours_setotmonth { get; set; }
    public int month_idx_start { get; set; }
    public int month_idx_end { get; set; }

}

public class ovt_timeend_otmonth_detail
{

    public int month_idx { get; set; }
    public string day { get; set; }
    public string date { get; set; }
    public string emp_code { get; set; }
    public string date_start { get; set; }
    public string date_end { get; set; }
    public int emp_idx { get; set; }
    public string dateot_from { get; set; }
    public string time_start { get; set; }
    public string time_end { get; set; }
    public string sum_time { get; set; }
    public string sum_hours_setotmonth { get; set; }
    public int month_idx_start { get; set; }
    public int month_idx_end { get; set; }

}

public class ovt_u0_month_detail
{

    public int month_idx { get; set; }
    public string day { get; set; }
    public string date { get; set; }
    public string date_start { get; set; }
    public int emp_idx { get; set; }
    public string dateot_from { get; set; }
    public string time_start { get; set; }
    public string time_end { get; set; }
    public string sum_time { get; set; }
    public string sum_hours_setotmonth { get; set; }
    public int holiday_idx { get; set; }
    public string work_start { get; set; }
    public string work_finish { get; set; }
    public string day_off { get; set; }
    public string date_condition { get; set; }
    public string break_start { get; set; }
    public string break_finish { get; set; }
    public string TypeWork { get; set; }

}

[Serializable]
public class ovt_u0_document_detail
{
    [XmlElement("u0_doc_idx")]
    public int u0_doc_idx { get; set; }

    [XmlElement("m0_group_idx")]
    public int m0_group_idx { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("ovt_detail")]
    public string ovt_detail { get; set; }

    [XmlElement("u0_ovt_status")]
    public int u0_ovt_status { get; set; }

    [XmlElement("emp_type")]
    public int emp_type { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("month_ot")]
    public int month_ot { get; set; }

    [XmlElement("sumhours")]
    public string sumhours { get; set; }

    [XmlElement("time_create_date")]
    public string time_create_date { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("joblevel")]
    public int joblevel { get; set; }

    [XmlElement("month_ot_name")]
    public string month_ot_name { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("count_waitapprove")]
    public int count_waitapprove { get; set; }

    [XmlElement("count_waitapprove_monthly")]
    public int count_waitapprove_monthly { get; set; }

    [XmlElement("count_waitapprove_daily")]
    public int count_waitapprove_daily { get; set; }

    [XmlElement("emp_email_headuser")]
    public string emp_email_headuser { get; set; }

    [XmlElement("emp_code_head")]
    public string emp_code_head { get; set; }

    [XmlElement("emp_name_th_head")]
    public string emp_name_th_head { get; set; }

    [XmlElement("org_name_th_head")]
    public string org_name_th_head { get; set; }

    [XmlElement("dept_name_th_head")]
    public string dept_name_th_head { get; set; }

    [XmlElement("sec_name_th_head")]
    public string sec_name_th_head { get; set; }

    [XmlElement("emp_mailhead")]
    public string emp_mailhead { get; set; }

    [XmlElement("pos_name_th_head")]
    public string pos_name_th_head { get; set; }

    [XmlElement("current_decision")]
    public string current_decision { get; set; }

    [XmlElement("comment_head")]
    public string comment_head { get; set; }

    [XmlElement("group_name")]
    public string group_name { get; set; }

    [XmlElement("count_emp")]
    public string count_emp { get; set; }
}

[Serializable]
public class ovt_u1_document_detail
{
    [XmlElement("u0_doc1_idx")]
    public int u0_doc1_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("u0_doc_idx")]
    public int u0_doc_idx { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("dateot_from")]
    public string dateot_from { get; set; }

    [XmlElement("dateot_to")]
    public string dateot_to { get; set; }

    [XmlElement("time_form")]
    public string time_form { get; set; }

    [XmlElement("time_to")]
    public string time_to { get; set; }

    [XmlElement("settime")]
    public string settime { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("comment_hr")]
    public string comment_hr { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u1_ovt_status")]
    public int u1_ovt_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("set_condition_idx")]
    public int set_condition_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("detail_job")]
    public string detail_job { get; set; }

    [XmlElement("sum_hours")]
    public string sum_hours { get; set; }

    [XmlElement("time_create_date")]
    public string time_create_date { get; set; }

    [XmlElement("month_ot_name")]
    public string month_ot_name { get; set; }

    [XmlElement("m0_group_idx")]
    public int m0_group_idx { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("ovt_detail")]
    public string ovt_detail { get; set; }

    [XmlElement("u0_ovt_status")]
    public int u0_ovt_status { get; set; }

    [XmlElement("emp_type")]
    public int emp_type { get; set; }

    [XmlElement("month_ot")]
    public int month_ot { get; set; }

    [XmlElement("sumhours")]
    public string sumhours { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("joblevel")]
    public int joblevel { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("count_waitapprove")]
    public int count_waitapprove { get; set; }

    [XmlElement("count_waitapprove_monthly")]
    public int count_waitapprove_monthly { get; set; }

    [XmlElement("count_waitapprove_daily")]
    public int count_waitapprove_daily { get; set; }

    [XmlElement("emp_code_setotmonth")]
    public string emp_code_setotmonth { get; set; }

    [XmlElement("dateot_from_setotmonth")]
    public string dateot_from_setotmonth { get; set; }

    [XmlElement("dateot_to_setotmonth")]
    public string dateot_to_setotmonth { get; set; }

    [XmlElement("time_form_setotmonth")]
    public string time_form_setotmonth { get; set; }

    [XmlElement("time_to_setotmonth")]
    public string time_to_setotmonth { get; set; }

    [XmlElement("emp_name_th_setotmonth")]
    public string emp_name_th_setotmonth { get; set; }

    [XmlElement("group_name")]
    public string group_name { get; set; }

    [XmlElement("ot_before")]
    public string ot_before { get; set; }

    [XmlElement("hours_ot_before")]
    public string hours_ot_before { get; set; }

    [XmlElement("ot_after")]
    public string ot_after { get; set; }

    [XmlElement("hours_ot_after")]
    public string hours_ot_after { get; set; }

    [XmlElement("ot_holiday")]
    public string ot_holiday { get; set; }

    [XmlElement("hours_ot_holiday")]
    public string hours_ot_holiday { get; set; }

    [XmlElement("holiday_idx")]
    public int holiday_idx { get; set; }

    [XmlElement("work_start")]
    public string work_start { get; set; }

    [XmlElement("work_finish")]
    public string work_finish { get; set; }

    [XmlElement("month_idx_start")]
    public int month_idx_start { get; set; }

    [XmlElement("month_idx_end")]
    public int month_idx_end { get; set; }

    [XmlElement("day_off")]
    public string day_off { get; set; }

    [XmlElement("date_condition")]
    public int date_condition { get; set; }

    [XmlElement("ot_before_status")]
    public int ot_before_status { get; set; }

    [XmlElement("break_start")]
    public string break_start { get; set; }

    [XmlElement("TypeWork")]
    public string TypeWork { get; set; }

    [XmlElement("emp_name_th_create")]
    public string emp_name_th_create { get; set; }

    [XmlElement("sec_name_th_create")]
    public string sec_name_th_create { get; set; }

    [XmlElement("emp_email_create")]
    public string emp_email_create { get; set; }

    [XmlElement("create_date_create")]
    public string create_date_create { get; set; }

    [XmlElement("time_create_date_create")]
    public string time_create_date_create { get; set; }

    [XmlElement("emp_email_headuser")]
    public string emp_email_headuser { get; set; }

    [XmlElement("current_status")]
    public string current_status { get; set; }

    [XmlElement("emp_email_hr")]
    public string emp_email_hr { get; set; }

    [XmlElement("emp_name_th_approve")]
    public string emp_name_th_approve { get; set; }

    [XmlElement("sec_name_th_approve")]
    public string sec_name_th_approve { get; set; }

    [XmlElement("create_date_approve")]
    public string create_date_approve { get; set; }

    [XmlElement("time_create_date_approve")]
    public string time_create_date_approve { get; set; }

    [XmlElement("emp_email_admin")]
    public string emp_email_admin { get; set; }
    [XmlElement("break_finish")]
    public string break_finish { get; set; }

}

public class ovt_u2_document_detail
{

    public int u2_doc_idx { get; set; }
    public int u1_doc_idx { get; set; }
    public int u0_doc_idx { get; set; }
    public int cemp_idx { get; set; }
    public int u0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int decision_idx { get; set; }
    public string comment { get; set; }
    public string create_date { get; set; }
    public string time_create { get; set; }
    public string current_artor { get; set; }
    public string current_decision { get; set; }
    public string current_status { get; set; }
    public string current_status_name { get; set; }
    public string update_date { get; set; }
    public int condition { get; set; }
    public int emp_idx { get; set; }
    public string date_start { get; set; }
    public string date_end { get; set; }
    public string time_start { get; set; }
    public string time_end { get; set; }
    public int m0_node_idx { get; set; }
    public int decision { get; set; }
    public int staidx { get; set; }
    public string status_name { get; set; }
    public string actor_name { get; set; }
    public string node_name { get; set; }
    public string emp_name_en { get; set; }
    public string emp_name_th { get; set; }
    public string org_name_en { get; set; }
    public string org_name_th { get; set; }
    public string dept_name_en { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_en { get; set; }
    public string sec_name_th { get; set; }
    public int rpos_idx { get; set; }
    public string pos_name_th { get; set; }
    public int count_waitApprove { get; set; }
    public string status_booking { get; set; }
    public string date_time_chekbutton { get; set; }
    public string emp_email { get; set; }
    public string emp_email_sentto_hr { get; set; }
    public string decision_name_hr { get; set; }
    public string break_start { get; set; }
    public string break_finish { get; set; }


}

public class ovt_m0_node_decision_detail
{

    public int decision_idx { get; set; }
    public int noidx { get; set; }
    public string decision_name { get; set; }
    public string decision_desc { get; set; }
    public int decision_status { get; set; }


}

public class ovt_employeeset_otmonth_detail
{

    public int u0_doc_idx { get; set; }
    public int m0_group_idx { get; set; }
    public int type_idx { get; set; }
    public string ovt_detail { get; set; }
    public int u0_ovt_status { get; set; }
    public int emp_type { get; set; }
    public int cemp_idx { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
    public int m0_node_idx { get; set; }
    public int m0_actor_idx { get; set; }
    public int decision { get; set; }
    public int staidx { get; set; }
    public string comment { get; set; }
    public string status_name { get; set; }
    public string actor_name { get; set; }
    public string node_name { get; set; }
    public string emp_name_en { get; set; }
    public string emp_name_th { get; set; }
    public string org_name_en { get; set; }
    public string org_name_th { get; set; }
    public string dept_name_en { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_en { get; set; }
    public string sec_name_th { get; set; }
    public int rpos_idx { get; set; }
    public string pos_name_th { get; set; }
    public int month_ot { get; set; }
    public string sumhours { get; set; }
    public string time_create_date { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int joblevel { get; set; }
    public string month_ot_name { get; set; }
    public string emp_code { get; set; }
    public int org_idx { get; set; }
    public string emp_email { get; set; }
    public int count_waitapprove { get; set; }
    public int count_waitapprove_monthly { get; set; }
    public int count_waitapprove_daily { get; set; }

}

[Serializable]
public class ovt_u0_document_day_detail
{
    [XmlElement("u0_doc_idx")]
    public int u0_doc_idx { get; set; }

    [XmlElement("u0_loc_idx")]
    public int u0_loc_idx { get; set; }

    [XmlElement("u0_org_idx")]
    public int u0_org_idx { get; set; }

    [XmlElement("u0_dept_idx")]
    public int u0_dept_idx { get; set; }

    [XmlElement("u0_sec_idx")]
    public int u0_sec_idx { get; set; }

    [XmlElement("type_daywork_idx")]
    public int type_daywork_idx { get; set; }

    [XmlElement("type_daywork_name")]
    public string type_daywork_name { get; set; }

    [XmlElement("day_all_sum")]
    public string day_all_sum { get; set; }

    [XmlElement("type_ot_idx")]
    public int type_ot_idx { get; set; }

    [XmlElement("type_ot_name")]
    public string type_ot_name { get; set; }

    [XmlElement("dateot_from")]
    public string dateot_from { get; set; }

    [XmlElement("dateot_to")]
    public string dateot_to { get; set; }

    [XmlElement("time_form")]
    public string time_form { get; set; }

    [XmlElement("time_to")]
    public string time_to { get; set; }

    [XmlElement("u0_ovt_status")]
    public int u0_ovt_status { get; set; }

    [XmlElement("emp_type")]
    public int emp_type { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("month_ot")]
    public int month_ot { get; set; }

    [XmlElement("sumhours")]
    public string sumhours { get; set; }

    [XmlElement("time_create_date")]
    public string time_create_date { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("joblevel")]
    public int joblevel { get; set; }

    [XmlElement("month_ot_name")]
    public string month_ot_name { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }

}

[Serializable]
public class ovt_u1_document_day_detail
{
    [XmlElement("u0_doc1_idx")]
    public int u0_doc1_idx { get; set; }

    [XmlElement("u0_doc_idx")]
    public int u0_doc_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("month_ot")]
    public int month_ot { get; set; }

    [XmlElement("sumhours")]
    public string sumhours { get; set; }

    [XmlElement("time_create_date")]
    public string time_create_date { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("joblevel")]
    public int joblevel { get; set; }

    [XmlElement("month_ot_name")]
    public string month_ot_name { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("ot_day_26")]
    public string ot_day_26 { get; set; }

    [XmlElement("ot_day_27")]
    public string ot_day_27 { get; set; }

    [XmlElement("ot_day_28")]
    public string ot_day_28 { get; set; }

    [XmlElement("ot_day_29")]
    public string ot_day_29 { get; set; }

    [XmlElement("ot_day_30")]
    public string ot_day_30 { get; set; }

    [XmlElement("ot_day_31")]
    public string ot_day_31 { get; set; }

    [XmlElement("ot_day_1")]
    public string ot_day_1 { get; set; }

    [XmlElement("ot_day_2")]
    public string ot_day_2 { get; set; }

    [XmlElement("ot_day_3")]
    public string ot_day_3 { get; set; }

    [XmlElement("ot_day_4")]
    public string ot_day_4 { get; set; }

    [XmlElement("ot_day_5")]
    public string ot_day_5 { get; set; }

    [XmlElement("ot_day_6")]
    public string ot_day_6 { get; set; }

    [XmlElement("ot_day_7")]
    public string ot_day_7 { get; set; }

    [XmlElement("ot_day_8")]
    public string ot_day_8 { get; set; }

    [XmlElement("ot_day_9")]
    public string ot_day_9 { get; set; }

    [XmlElement("ot_day_10")]
    public string ot_day_10 { get; set; }

    [XmlElement("ot_day_11")]
    public string ot_day_11 { get; set; }

    [XmlElement("ot_day_12")]
    public string ot_day_12 { get; set; }

    [XmlElement("ot_day_13")]
    public string ot_day_13 { get; set; }

    [XmlElement("ot_day_14")]
    public string ot_day_14 { get; set; }

    [XmlElement("ot_day_15")]
    public string ot_day_15 { get; set; }

    [XmlElement("ot_day_16")]
    public string ot_day_16 { get; set; }

    [XmlElement("ot_day_17")]
    public string ot_day_17 { get; set; }

    [XmlElement("ot_day_18")]
    public string ot_day_18 { get; set; }

    [XmlElement("ot_day_19")]
    public string ot_day_19 { get; set; }

    [XmlElement("ot_day_20")]
    public string ot_day_20 { get; set; }

    [XmlElement("ot_day_21")]
    public string ot_day_21 { get; set; }

    [XmlElement("ot_day_22")]
    public string ot_day_22 { get; set; }

    [XmlElement("ot_day_23")]
    public string ot_day_23 { get; set; }

    [XmlElement("ot_day_24")]
    public string ot_day_24 { get; set; }

    [XmlElement("ot_day_25")]
    public string ot_day_25 { get; set; }

}

[Serializable]
public class ovt_u2_document_day_detail
{
    [XmlElement("u2_doc_idx")]
    public int u2_doc_idx { get; set; }

    [XmlElement("u1_doc_idx")]
    public int u1_doc_idx { get; set; }

    [XmlElement("u0_doc_idx")]
    public int u0_doc_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("current_artor")]
    public string current_artor { get; set; }

    [XmlElement("current_decision")]
    public string current_decision { get; set; }

    [XmlElement("current_status")]
    public string current_status { get; set; }

    [XmlElement("current_status_name")]
    public string current_status_name { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("date_start")]
    public string date_start { get; set; }

    [XmlElement("date_end")]
    public string date_end { get; set; }

    [XmlElement("time_start")]
    public string time_start { get; set; }

    [XmlElement("time_end")]
    public string time_end { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("count_waitApprove")]
    public int count_waitApprove { get; set; }

    [XmlElement("status_booking")]
    public string status_booking { get; set; }

    [XmlElement("date_time_chekbutton")]
    public string date_time_chekbutton { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_email_sentto_hr")]
    public string emp_email_sentto_hr { get; set; }

    [XmlElement("decision_name_hr")]
    public string decision_name_hr { get; set; }

}




