﻿using System;
using System.Xml.Serialization;

public class data_permission_set
{

    public string return_code { get; set; }
    public string return_msg { get; set; }

    public permission_set_detail[] permission_list { get; set; }
}


public class permission_set_detail
{
    public int peridx { get; set; }
    public string emp_idx_set { get; set; }
    public string rdept_idx_set { get; set; }
    public string rsec_idx_set { get; set; }
    public string rpos_idx_set { get; set; }
    public int p_system { get; set; }
    public int p_permission_type { get; set; }
    public int p_status { get; set; }
    public int p_emp_create { get; set; }
    public string org_idx_use { get; set; }
    public string rdept_idx_use { get; set; }
    public string rsec_idx_use { get; set; }
    public string rpos_idx_use { get; set; }
    public int type_action { get; set; }
    public string org_name_set { get; set; }
    public string rdep_name_set { get; set; }
    public string rsec_name_set { get; set; }
    public string rpos_name_set { get; set; }
    public string org_name_use { get; set; }
    public string rdep_name_use { get; set; }
    public string rsec_name_use { get; set; }
    public string rpos_name_use { get; set; }
    public string p_system_name { get; set; }
    public string p_permission_type_name { get; set; }
    public string SecNameTH { get; set; }
    public int RSecIDX { get; set; }
    public string PosNameTH { get; set; }
    public int RPosIDX { get; set; }

    public int orgidx_search { get; set; }
    public int rdeptidx_search { get; set; }
    public int rsecidx_search { get; set; }
    public int rposidx_search { get; set; }
    public int system_search { get; set; }

}

