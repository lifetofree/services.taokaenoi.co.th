﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_test_eye
/// </summary>
/// 

[Serializable]
[XmlRoot("data_test_eye")]
public class data_test_eye
{

    [XmlElement("ReturnCode")]
    public int ReturnCode { get; set; }
    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }


    public Test_store_eye_detail[] Test_store_eye_list { get; set; }
  

}


public class Test_store_eye_detail
{
    public int m0_ceridx { get; set; }
    public string cer_name { get; set; }
    public int cer_status { get; set; }
    public int cemp_idx { get; set; }
    public int uemp_idx { get; set; }
    public int condition { get; set; }
    public string cer_status_Name { get; set; }
    public int duplicate { get; set; }

}