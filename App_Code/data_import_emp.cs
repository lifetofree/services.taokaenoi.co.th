﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_import_emp
/// </summary>
/// 

[Serializable]
[XmlRoot("data_import_emp")]

public class data_import_emp
{

 
    public int return_code { get; set; }
    
    public string return_msg { get; set; }
  
    public int return_idx { get; set; }
    
    public int import_mode { get; set; }


  
    public import_emp_detail_u0[] import_emp_u0 { get; set; } //import_emp_u0

  
    public import_emp_detail_u1[] import_emp_u1 { get; set; } //import_emp_u1

}

[Serializable]

public class import_emp_detail_u0
{
 
    public int u0_import_idx { get; set; }
  
    public string import_filename { get; set; }
 
    public string import_rows { get; set; }
  
    public int cemp_idx { get; set; }
  
    public int import_status { get; set; }
 
    public string created_date { get; set; }
  
    public string update_date { get; set; }
}

[Serializable]

public class import_emp_detail_u1
{

 
    public int u1_import_idx { get; set; }


    public int u0_import_idx { get; set; }
  
    public string empcode_new { get; set; }
  
    public string prefix { get; set; }
   
    public string firstname_th { get; set; }
  
    public string lastname_th { get; set; }

    public string prefix_en { get; set; }

    public string firstname_en { get; set; }
   
    public string lastname_en { get; set; }
  
    public string emp_in { get; set; }
   
    public string position { get; set; }
  
    public string section { get; set; }
    
    public string department { get; set; }
   
    public string cost_center { get; set; }
   
    public string sex { get; set; }
   
    public string birthday { get; set; }
   
    public string passport { get; set; }
    
    public string passport_issued_at { get; set; }
   
    public string issued_date { get; set; }
    
    public string passport_exp_date { get; set; }
   
    public string work_permit_no { get; set; }
  
    public string work_permit_issued_at { get; set; }
   
    public string work_permit_issued_date { get; set; }
   
    public string work_permit_exp_date { get; set; }
  
    public string social_id { get; set; }
   
    public string account_no { get; set; }
  
    public string nationality { get; set; }
   
    public string race { get; set; }
  
    public string religion { get; set; }
  
    public string OrgTH { get; set; }
    
    public string identity_card { get; set; }
   
    public string idate_expired { get; set; }
   
    public string P_EmpCreate { get; set; }
    
    public string emp_approve1 { get; set; }
    
    public string emp_approve2 { get; set; }
    
    public string Affiliation_name { get; set; }
    
    public string location_name { get; set; }
    
    public string identity_issuat { get; set; }
    
    public string visa_startdate { get; set; }
    
    public string visa_enddate { get; set; }
    
    public string status_ma { get; set; }
    
    public string re_org { get; set; }
    
    public string re_wg { get; set; }
    
    public string re_lw { get; set; }
    
    public string re_dept { get; set; }
    
    public string re_sec { get; set; }
    
    public string re_pos { get; set; }
    
    public string re_joblevel { get; set; }
    
    public string re_costcenter { get; set; }
    
    public string re_empgroup { get; set; }
    
    public string created_date { get; set; }
    
    public string update_date { get; set; }
    
    public int cemp_idx { get; set; }
    
    public int import_emp_status { get; set; }

}