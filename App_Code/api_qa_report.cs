﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.Configuration;

/// <summary>
/// Summary description for api_roombooking
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_qa_report : System.Web.Services.WebService
{

    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_qa_report _data_qa_report = new data_qa_report();
    service_mail _serviceMail = new service_mail();

    string _local_xml = "";
    string _mail_subject = "";
    string _mail_body = "";

    public api_qa_report()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    // get test detail to header result
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetTestDetailToResult(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_report", "service_qa_lab_report", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

}
