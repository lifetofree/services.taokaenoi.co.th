﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DBConn
/// </summary>
public class DBConn
{

    private MySqlConnection connection;
    private string server;
    private string database;
    private string uid;
    private string password;


    public DBConn()
    {
        Initialize();
    }

    //Initialize values
    private void Initialize()
    {
        //server = ConfigurationManager.AppSettings["serverip"];//"10.211.55.2";
        //database = ConfigurationManager.AppSettings["dbname"];//"fslog";
        //uid = ConfigurationManager.AppSettings["userid"];//"fsdemo";
        //password = ConfigurationManager.AppSettings["userpassword"];//"123456";

        server = "172.16.11.28"; //ConfigurationManager.AppSettings["serverip"];//"10.211.55.2";
        database = "shopdemo_tkn"; //ConfigurationManager.AppSettings["dbname"];//"fslog";
        uid = "shoptkn"; //ConfigurationManager.AppSettings["userid"];//"fsdemo";
        password = "$F3PdE"; //ConfigurationManager.AppSettings["userpassword"];//"123456";

        string connectionString;
        connectionString = "SERVER=" + server + ";" + "DATABASE=" +
        database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

        connection = new MySqlConnection(connectionString);
    }

    //open connection to database
    private bool ConnOpen()
    {
        try
        {
            connection.Open();
            return true;
        }
        catch (MySqlException ex)
        {
            //When handling errors, you can your application's response based 
            //on the error number.
            //The two most common error numbers when connecting are as follows:
            //0: Cannot connect to server.
            //1045: Invalid user name and/or password.
            //switch (ex.Number)
            //{
            //    case 0:
            //        MessageBox.Show("Cannot connect to server.  Contact administrator");
            //        break;

            //    case 1045:
            //        MessageBox.Show("Invalid username/password, please try again");
            //        break;
            //}
            connection.Close();
            return false;
        }
    }

    //Close connection
    private bool ConnClose()
    {
        connection.Close();
        return true;
    }

    //Execute query
    public bool ConnCmd(string cmd)
    {
        try
        {
            this.ConnOpen();
            MySqlCommand _cmd = new MySqlCommand();
            _cmd.CommandText = cmd;
            _cmd.Connection = connection;

            _cmd.ExecuteNonQuery();
            this.ConnClose();

            return true;
        }
        catch (MySqlException ex)
        {
            this.ConnClose();
            return false;
        }
    }

    //Execute Stored Procedure in JSON format
    public string execMyJson(string data_name, string sp_name, string json_in, int action_type)
    {
        string retJson = "";
        string cmdText = "";

        MySqlCommand cmd = new MySqlCommand();
        cmd.Connection = connection;
        cmd.CommandType = CommandType.Text;

        cmdText = "CALL " + sp_name + "('" + json_in + "', " + action_type + ", " + "@json_out); SELECT @json_out;";
        cmd.CommandText = cmdText;

        //MySqlCommand cmd = new MySqlCommand(sp_name, connection);
        //cmd.CommandType = CommandType.StoredProcedure;

        //cmd.Parameters.Add("@json_in", MySqlDbType.Text).Value = json_in;
        //cmd.Parameters.Add("@action_type", MySqlDbType.Int32).Value = action_type;
        //cmd.Parameters.Add("@json_out", MySqlDbType.Text).Direction = ParameterDirection.Output;

        try
        {
            this.ConnOpen();
            using (IDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                    retJson += reader.GetString(0);
            }
            //cmd.ExecuteNonQuery();
            //retJson = cmd.Parameters["@json_out"].Value.ToString();
        }
        catch (Exception ex)
        {
            string _result_code = "\"return_code\": 100";
            string _result_msg = "\"return_msg\": " + ex.Message;

            retJson = "{\"" + data_name + "\": " + "{" + _result_code + "," + _result_msg + "}";
        }
        finally
        {
            this.ConnClose();
        }

        return retJson;
    }

}