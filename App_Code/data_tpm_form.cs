﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_tpm_form")]
public class data_tpm_form
{

    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }
    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }

    [XmlElement("Boxtpmm0_DocFormDetail")]
    public tpmm0_DocFormDetail[] Boxtpmm0_DocFormDetail { get; set; }

    [XmlElement("Boxtpmu0_DocFormDetail")]
    public tpmu0_DocFormDetail[] Boxtpmu0_DocFormDetail { get; set; }

    public tpmu1_DocFormDetail[] Boxtpmu1_DocFormDetail { get; set; }

    public tpmu2_DocFormDetail[] Boxtpmu2_DocFormDetail { get; set; }
}

[Serializable]
public class tpmm0_DocFormDetail
{
    public int empgroup_idx { get; set; }
    public string empgroup_nameth { get; set; }
    public int m0_typeidx { get; set; }
    public string type_name { get; set; }
    public int m1_typeidx { get; set; }
    public string m1_type_name { get; set; }
    public string m1_typeidx_comma { get; set; }
    public int status { get; set; }
    public string m2_type_name { get; set; }
    public int m2_typeidx { get; set; }
    public string pos_name { get; set; }
    public string posidx_comma { get; set; }
    public int posidx { get; set; }
    public int orgidx { get; set; }
    public string m2_typeidx_comma { get; set; }
    public string OrgNameTH { get; set; }
    public string create_date { get; set; }
    public int u0_typeidx { get; set; }
    public int r0_typeidx { get; set; }
    public int m0_point { get; set; }
    public int point_name { get; set; }
    public int setting_point { get; set; }
    public string rat_nameth { get; set; }
    public string rat_nameen { get; set; }
    public string definition_th { get; set; }
    public string definition_en { get; set; }
    public int m1_coreidx { get; set; }
    public string core_name { get; set; }
    public string type_core { get; set; }
    public int m2_coreidx { get; set; }
    public string core_nameen { get; set; }
    public string core_nameth { get; set; }
    public string form_name { get; set; }
    public int staidx { get; set; }
    public string status_name { get; set; }
    public int unidx { get; set; }
    public string namechoice { get; set; }
    public string node_desc { get; set; }
    public int noidx { get; set; }


}

[Serializable]
public class tpmu0_DocFormDetail
{
    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("m1_typeidx")]
    public int m1_typeidx { get; set; }

    [XmlElement("u0_typeidx")]
    public int u0_typeidx { get; set; }

    [XmlElement("m1_coreidx")]
    public int m1_coreidx { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("TIDX")]
    public int TIDX { get; set; }

    [XmlElement("jobgrade_level")]
    public int jobgrade_level { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("pos_name")]
    public string pos_name { get; set; }

    [XmlElement("u0_docidx")]
    public int u0_docidx { get; set; }

    [XmlElement("sum_mine_core_value")]
    public string sum_mine_core_value { get; set; }

    [XmlElement("sum_head_core_value")]
    public string sum_head_core_value { get; set; }

    [XmlElement("sum_mine_competency")]
    public string sum_mine_competency { get; set; }

    [XmlElement("sum_head_competency")]
    public string sum_head_competency { get; set; }

    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("form_name")]
    public string form_name { get; set; }

    [XmlElement("m0_typeidx")]
    public int m0_typeidx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("countapprove")]
    public int countapprove { get; set; }

    [XmlElement("m2_typeidx")]
    public int m2_typeidx { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("email1")]
    public string email1 { get; set; }

    [XmlElement("email2")]
    public string email2 { get; set; }

    [XmlElement("email_hr")]
    public string email_hr { get; set; }

    [XmlElement("approve1")]
    public string approve1 { get; set; }

    [XmlElement("approve2")]
    public string approve2 { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("m0idx")]
    public int m0idx { get; set; }

    [XmlElement("getyear")]
    public int getyear { get; set; }

    [XmlElement("rdepidx_comma")]
    public string rdepidx_comma { get; set; }

    [XmlElement("qty")]
    public int qty { get; set; }

    [XmlElement("complete")]
    public int complete { get; set; }

    [XmlElement("uncomplete")]
    public int uncomplete { get; set; }

    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

}

[Serializable]
public class tpmu1_DocFormDetail
{
    public int u0_docidx { get; set; }
    public int u1_docidx { get; set; }
    public int m0_typeidx { get; set; }
    public int m1_typeidx { get; set; }
    public string m0_point_mine { get; set; }
    public string remark_mine { get; set; }
    public string m0_point_head { get; set; }
    public string remark_head { get; set; }
}

[Serializable]
public class tpmu2_DocFormDetail
{
    public int u0_docidx { get; set; }
    public int u2_docidx { get; set; }
    public int m0_typeidx { get; set; }
    public string behavior_name { get; set; }
    public string comment_name { get; set; }
    public int cemp_idx { get; set; }
    public string FullNameTH { get; set; }
    public int m0_typeidx_choose { get; set; }
    public string m1_typeidx_comma { get; set; }
    public string typename { get; set; }

    //public string type_core { get; set; }
    //public string core_name { get; set; }
    //public int m1_coreidx { get; set; }
    //public int u0_typeidx { get; set; }
    //public string m1_type_name { get; set; }



}