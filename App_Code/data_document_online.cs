﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Xml.Serialization;

[Serializable]
[XmlRoot("Data_DocumentONLine")]
public class Data_DocumentONLine
{
   [XmlElement("ReturnCode")]
   public string ReturnCode { get; set; }
   [XmlElement("return_code")]
   public string return_code { get; set; }
   [XmlElement("return_msg")]
   public string return_msg { get; set; }
   [XmlElement("return_check")]
   public string return_check { get; set; }
   [XmlElement("return_Mail")]
   public string return_Mail { get; set; }
   [XmlElement("return_rdept_ids")]
   public string return_rdept_ids { get; set; }
   [XmlElement("return_emails")]
   public string return_emails { get; set; }


   //////กล่อง U0_Document_DAR
   [XmlElement("BoxU0_DocumentONLine")]
   public Details_U0_DocumentONLine[] BoxU0_DocumentONLine { get; set; }

   //////กล่อง U1_Document_DAR
   [XmlElement("BoxU1_DocumentONLine")]
   public Details_U1_DocumentONLine[] BoxU1_DocumentONLine { get; set; }

   //////กล่อง M0_TypeDocDar
   [XmlElement("Box_M0_TypeDocDar")]
   public Details_M0_TypeDocDar[] Box_M0_TypeDocDar { get; set; }

   //////กล่อง เรียก dropdown
   [XmlElement("Box_At_the_request")]
   public Details_At_the_request[] Box_At_the_request { get; set; }

   //////กล่อง เรียก ชื่อผู้รับ
   [XmlElement("Box_Name_recipients")]
   public Details_Name_recipients[] Box_Name_recipients { get; set; }

   [XmlElement("Box_R0_Org_Location")]
   public Details_R0_Org_Location[] Box_R0_Org_Location { get; set; }

   [XmlElement("Box_U0_new")]
   public Details_Box_U0_new[] Box_U0_new { get; set; }

   [XmlElement("Box_U0_new2")]
   public Details_Box_U0_new2[] Box_U0_new2 { get; set; }


   [XmlElement("Box_U0_new3")]
   public Details_Box_U0_new3[] Box_U0_new3 { get; set; }

   [XmlElement("Box_U0_mail")]
   public Details_Box_U0_mail[] Box_U0_mail { get; set; }

   [XmlElement("Box_U0_junk")]
   public Details_Box_U0_junk[] Box_U0_junk { get; set; }

   [XmlElement("Box_Delete")]
   public Box_Delete_junk[] Box_Delete { get; set; }

   [XmlElement("Bob_url")]
   public Bob_url_[] Bob_url { get; set; }

}

#region URL
[Serializable]
public class Bob_url_
{
   [XmlElement("uidxSUrl")]
   public int uidxSUrl { get; set; }

   [XmlElement("PatURL")]
   public string PatURL { get; set; }
}
#endregion

#region U1_DocumentONLine
[Serializable]
public class Details_Box_U0_junk
{
   [XmlElement("Email")]
   public string Email { get; set; }

   [XmlElement("R_Depart1")]
   public string R_Depart1 { get; set; }

}
#endregion

#region Delete
[Serializable]
public class Box_Delete_junk
{
   [XmlElement("midx1")]
   public int midx1 { get; set; }

   [XmlElement("midx2")]
   public int midx2 { get; set; }

   [XmlElement("DarNosendDelete")]
   public string DarNosendDelete { get; set; }
}
#endregion

#region U1_DocumentONLine
[Serializable]
public class Details_Box_U0_mail
{
   [XmlElement("Email")]
   public string Email { get; set; }

   [XmlElement("R_Depart1")]
   public string R_Depart1 { get; set; }

}
#endregion


#region เรียก dropdown ฝ่าย
[Serializable]
public class Details_Box_U0_new3
{

   [XmlElement("U0_idx")]
   public int U0_idx { get; set; }

   [XmlElement("Create_date2")]
   public string Create_date2 { get; set; }

   [XmlElement("Name_ISO")]
   public string Name_ISO { get; set; }

   [XmlElement("Code_ISO")]
   public string Code_ISO { get; set; }

   [XmlElement("Type_ISO")]
   public int Type_ISO { get; set; }

   [XmlElement("Status_Permis")]
   public int Status_Permis { get; set; }

   [XmlElement("OrgIDX")]
   public int OrgIDX { get; set; }

   [XmlElement("Status_Junk")]
   public int Status_Junk { get; set; }

   [XmlElement("Location_Org")]
   public int Location_Org { get; set; }

   [XmlElement("Status_Download")]
   public int Status_Download { get; set; }

   [XmlElement("RDeptIDX")]
   public int RDeptIDX { get; set; }

   [XmlElement("Doc_date")]
   public string Doc_date { get; set; }
}
#endregion


#region R0_Org_Location
[Serializable]
public class Details_R0_Org_Location
{
   [XmlElement("EmpIDX")]
   public int EmpIDX { get; set; }

   [XmlElement("OrgIDX")]
   public int OrgIDX { get; set; }

}

#endregion

#region เรียก dropdown ชื่อผู้รับ
[Serializable]
public class Details_Name_recipients
{
   [XmlElement("EmpCode")]
   public int EmpCode { get; set; }

   [XmlElement("RDeptID")]
   public int RDeptID { get; set; }

   [XmlElement("FullNameTH")]
   public string FullNameTH { get; set; }
}
#endregion

#region เรียก dropdown ฝ่าย
[Serializable]
public class Details_At_the_request
{
   [XmlElement("RDeptIDX")]
   public int RDeptIDX { get; set; }

   [XmlElement("DeptIDX")]
   public int DeptIDX { get; set; }

   [XmlElement("DeptNameEN")]
   public string DeptNameEN { get; set; }

   [XmlElement("DeptNameTH")]
   public string DeptNameTH { get; set; }
}
#endregion

#region เรียก dropdown ฝ่าย
[Serializable]
public class Details_Box_U0_new
{
   [XmlElement("Create_date")]
   public int Create_date { get; set; }
}
#endregion

#region เรียก dropdown ฝ่าย
[Serializable]
public class Details_Box_U0_new2
{
   [XmlElement("PatURL")]
   public string PatURL { get; set; }

   [XmlElement("idEmp")]
   public int idEmp { get; set; }

   [XmlElement("U0_idx")]
   public int U0_idx { get; set; }

   [XmlElement("Create_date2")]
   public int Create_date2 { get; set; }

   [XmlElement("Name_ISO")]
   public string Name_ISO { get; set; }

   [XmlElement("Code_ISO")]
   public string Code_ISO { get; set; }

   [XmlElement("Type_ISO")]
   public int Type_ISO { get; set; }

   [XmlElement("Type_ISOsend")]
   public int Type_ISOsend { get; set; }

   [XmlElement("Status_Permis")]
   public int Status_Permis { get; set; }

   [XmlElement("OrgIDX")]
   public int OrgIDX { get; set; }

   [XmlElement("Status_Junk")]
   public int Status_Junk { get; set; }

   [XmlElement("Location_Org")]
   public int Location_Org { get; set; }

   [XmlElement("Status_Download")]
   public int Status_Download { get; set; }

   [XmlElement("RDeptIDX")]
   public int RDeptIDX { get; set; }

   [XmlElement("RDeptIDX3")]
   public int RDeptIDX3 { get; set; }

   [XmlElement("RDeptIDX4")]
   public int RDeptIDX4 { get; set; }

   [XmlElement("Doc_date")]
   public string Doc_date { get; set; }

   [XmlElement("DarNo")]
   public string DarNo { get; set; }

   [XmlElement("DarNosend")]
   public string DarNosend { get; set; }

   [XmlElement("DeptNameEN")]
   public string DeptNameEN { get; set; }

   [XmlElement("Type_ISO_TXT")]
   public string Type_ISO_TXT { get; set; }

   [XmlElement("Name")]
   public string Name { get; set; }

   [XmlElement("Name_Doc")]
   public string Name_Doc { get; set; }
}
#endregion

////////////////กล่อง U0_DocumentONLine

#region U0_DocumentONLine
[Serializable]
public class Details_U0_DocumentONLine
{
   [XmlElement("PatURL")]
   public string PatURL { get; set; }

   [XmlElement("Type_ISO")]
   public int Type_ISO { get; set; }

   [XmlElement("Code_ISO")]
   public string Code_ISO { get; set; }

   [XmlElement("U0_idx1")]
   public int U0_idx1 { get; set; }

   [XmlElement("Status_Permis")]
   public int Status_Permis { get; set; }

   [XmlElement("Doc_date")]
   public string Doc_date { get; set; }

   [XmlElement("Status_Junk")]
   public int Status_Junk { get; set; }

   [XmlElement("Name_ISO")]
   public string Name_ISO { get; set; }

   [XmlElement("OrgIDX")]
   public int OrgIDX { get; set; }

   [XmlElement("Status_download")]
   public int Status_download { get; set; }

   [XmlElement("Location_Org")]
   public int Location_Org { get; set; }

   [XmlElement("Create_date")]
   public string Create_date { get; set; }

   [XmlElement("Create_time")]
   public string Create_time { get; set; }

   [XmlElement("R1_Depart")]
   public int R1_Depart { get; set; }

   [XmlElement("cempidx")]
   public int cempidx { get; set; }

   [XmlElement("DocumentCode")]
   public string DocumentCode { get; set; }

   [XmlElement("NameType_ISO")]
   public string NameType_ISO { get; set; }

   [XmlElement("NameDepart")]
   public string NameDepart { get; set; }

   [XmlElement("U0_idx")]
   public int U0_idx { get; set; }

   [XmlElement("RDeptIDX")]
   public int RDeptIDX { get; set; }

   [XmlElement("DarNo")]
   public string DarNo { get; set; }

   [XmlElement("RDeptID")]
   public int RDeptID { get; set; }

   [XmlElement("DeptNameEN")]
   public string DeptNameEN { get; set; }

   [XmlElement("DeptNameTH")]
   public string DeptNameTH { get; set; }

   [XmlElement("U0_idx_ref")]
   public int U0_idx_ref { get; set; }
   [XmlElement("r_dept_idx_ref")]
   public int r_dept_idx_ref { get; set; }
   [XmlElement("R_Depart")]
   public string R_Depart { get; set; }
   [XmlElement("r_dept_idx_ref_comma")]
   public string r_dept_idx_ref_comma { get; set; }
   [XmlElement("Name_Doc")]
   public string Name_Doc { get; set; }

   [XmlElement("type_iso_desc")]
   public string type_iso_desc { get; set; }
   [XmlElement("name_dept_desc")]
   public string name_dept_desc { get; set; }
   [XmlElement("name_iso_desc")]
   public string name_iso_desc { get; set; }
   [XmlElement("code_doc_desc")]
   public string code_doc_desc { get; set; }
   [XmlElement("dar_no_desc")]
   public string dar_no_desc { get; set; }
   [XmlElement("doc_date_desc")]
   public string doc_date_desc { get; set; }

   [XmlElement("filter_rdept_idx")]
   public string filter_rdept_idx { get; set; }
   [XmlElement("filter_type_iso")]
   public string filter_type_iso { get; set; }
   [XmlElement("filter_code_iso")]
   public string filter_code_iso { get; set; }
   [XmlElement("filter_document_name")]
   public string filter_document_name { get; set; }
   [XmlElement("filter_date_effective")]
   public string filter_date_effective { get; set; }

   [XmlElement("doc_rev")]
   public string doc_rev { get; set; }
   [XmlElement("doc_date_effective")]
   public string doc_date_effective { get; set; }
   [XmlElement("doc_name_iso")]
   public string doc_name_iso { get; set; }
   [XmlElement("doc_no")]
   public string doc_no { get; set; }
   [XmlElement("doc_name")]
   public string doc_name { get; set; }
   [XmlElement("doc_type_iso")]
   public string doc_type_iso { get; set; }
   [XmlElement("days_of_time")]
   public string days_of_time { get; set; }
   [XmlElement("emp_email")]
   public string emp_email { get; set; }
}

#endregion

////////////กล่อง U1_DocumentONLine

#region U1_DocumentONLine
[Serializable]
public class Details_U1_DocumentONLine
{

   [XmlElement("U0_idx")]
   public int U0_idx { get; set; }
   [XmlElement("U0idx")]
   public int U0idx { get; set; }
   [XmlElement("R_Depart")]
   public int R_Depart { get; set; }


}
#endregion

////////////กล่อง M0_TypeDocDar

#region Details_M0_TypeDocDar
[Serializable]
public class Details_M0_TypeDocDar
{
   [XmlElement("ISOIDX")]
   public int ISOIDX { get; set; }

   [XmlElement("Name_ISO")]
   public string Name_ISO { get; set; }

   [XmlElement("Status_ISO")]
   public int Status_ISO { get; set; }
}
#endregion