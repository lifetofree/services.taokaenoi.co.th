﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.Configuration;

/// <summary>
/// Summary description for api_roombooking
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class api_carbooking : System.Web.Services.WebService
{


    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_carbooking _data_carbooking = new data_carbooking();
    service_mail _serviceMail = new service_mail();

    //sent e - mail
    string _local_xml = "";
    string _mail_subject = "";
    string _mail_body = "";

    string document_code_mail = "";
    string test_details_mail = "";
    string details_mail = "";
    string create_date_mail = "";

    string email_cemp = "";
    string email_hr = "";
    string email_hr_check = "";
    string email_cemptest = "";
    string email_headtest = "";
    string email_head = "";

    string link_system_carbooking = "http://mas.taokaenoi.co.th/car-booking";

    public api_carbooking()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 


    }

    // insert and update masterdata place carbooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkm0Place(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 110); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert and update masterdata car detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkm0CarDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 111); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert m1 detail Tax
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkm1CarDetailTax(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 112); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert m0 Expenses
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkm0Expenses(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 113); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert m0 topic_ma
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkm0TopicMa(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 114); // return w/ json


        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // insert create car booking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkCarbooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_carbooking _data_carbooking = new data_carbooking();
            _data_carbooking = (data_carbooking)_funcTool.convertXmlToObject(typeof(data_carbooking), _xml_in);


            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 120); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_carbooking = (data_carbooking)_funcTool.convertXmlToObject(typeof(data_carbooking), _local_xml);

            if (_data_carbooking.return_code == 0) //user create
            {
                //check e-mail null
                if (_data_carbooking.cbk_u0_document_list[0].emp_email != null && _data_carbooking.cbk_u0_document_list[0].emp_email != "-")
                {
                    email_cemptest = "waraporn.teoy@gmail.com";
                    email_cemp = _data_carbooking.cbk_u0_document_list[0].emp_email.ToString(); //ของจริง
                }

                if (_data_carbooking.cbk_u0_document_list[0].emp_email_sentto_headuser != null && _data_carbooking.cbk_u0_document_list[0].emp_email_sentto_headuser != "-")
                {
                    email_headtest = "teoy_42@hotmail.com";
                    email_head = _data_carbooking.cbk_u0_document_list[0].emp_email_sentto_headuser.ToString(); //ของจริง
                }


                //email_hr = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ hr
                email_hr = "hr_admin_center@taokaenoi.co.th,nuchanart.ar@taokaenoi.co.th"; //ใช้ทดสอบ 

                string email = email_hr + "," + email_cemptest + "," + email_headtest + "," + email_cemp + "," + email_head; //ใช้ทดสอบ 
                //string email = email_cemp + "," + _email_qa; // ของจริง
                cbk_u0_document_detail _temp_u0 = _data_carbooking.cbk_u0_document_list[0];
                //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการจองห้องประชุม";
                _mail_subject = "[HR : CarBooking] - แจ้งรายการจองรถ";
                _mail_body = _serviceMail.SentMailCarBookingCreate(_data_carbooking.cbk_u0_document_list[0], link_system_carbooking);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);

            }
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // approve by Head User
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkApproveHeadUser(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_carbooking _data_carbooking = new data_carbooking();
            _data_carbooking = (data_carbooking)_funcTool.convertXmlToObject(typeof(data_carbooking), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 121); // return w/ json

            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_carbooking = (data_carbooking)_funcTool.convertXmlToObject(typeof(data_carbooking), _local_xml);

            if (_data_carbooking.return_code == 0) //user create
            {
                //check e-mail null
                if (_data_carbooking.cbk_u0_document_list[0].emp_email != null && _data_carbooking.cbk_u0_document_list[0].emp_email != "-")
                {
                    email_cemptest = "waraporn.teoy@gmail.com";
                    email_cemp = _data_carbooking.cbk_u0_document_list[0].emp_email.ToString(); //ของจริง
                }

                if (_data_carbooking.cbk_u0_document_list[0].emp_email_sentto_headuser != null && _data_carbooking.cbk_u0_document_list[0].emp_email_sentto_headuser != "-")
                {
                    email_headtest = "teoy_42@hotmail.com";
                    email_head = _data_carbooking.cbk_u0_document_list[0].emp_email_sentto_headuser.ToString(); //ของจริง
                }


                //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                email_hr = "hr_admin_center@taokaenoi.co.th,nuchanart.ar@taokaenoi.co.th"; //ใช้จริง

                string email = email_hr + "," + email_cemptest + "," + email_headtest + ',' + email_head + ',' + email_cemp; //ใช้ทดสอบ 
                //string email = email_head + "," + email_cemp + "," + ; email_hr// ของจริง
                cbk_u0_document_detail _temp_u0 = _data_carbooking.cbk_u0_document_list[0];
               
                _mail_subject = "[HR : CarBooking] - แจ้งพิจารณารายการจองรถ(ผู้มีสิทธิ์อนุมัติรายการ)";
                _mail_body = _serviceMail.SentMailCarBookingHeaduserApprove(_data_carbooking.cbk_u0_document_list[0], link_system_carbooking);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);

            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // approve by Hr Admin
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkApproveHrAdmin(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_carbooking _data_carbooking = new data_carbooking();
            _data_carbooking = (data_carbooking)_funcTool.convertXmlToObject(typeof(data_carbooking), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 122); // return w/ json


            // check return value then send mail
            _local_xml = _funcTool.convertJsonToXml(_ret_val);
            _data_carbooking = (data_carbooking)_funcTool.convertXmlToObject(typeof(data_carbooking), _local_xml);

            if (_data_carbooking.return_code == 0) //user create
            {
                //check e-mail null
                if (_data_carbooking.cbk_u0_document_list[0].emp_email != null && _data_carbooking.cbk_u0_document_list[0].emp_email != "-")
                {
                    email_cemptest = "waraporn.teoy@gmail.com";
                    email_cemp = _data_carbooking.cbk_u0_document_list[0].emp_email.ToString(); //ของจริง
                }

                //check e-mail null
                if (_data_carbooking.cbk_u0_document_list[0].emp_email_sentto_headuser != null && _data_carbooking.cbk_u0_document_list[0].emp_email_sentto_headuser != "-")
                {
                    email_headtest = "waraporn.teoy@gmail.com";
                    email_head = _data_carbooking.cbk_u0_document_list[0].emp_email_sentto_headuser.ToString(); //ของจริง
                }


                //email_hr = "teoy_42@hotmail.com"; //ใช้ทดสอบ hr
                email_hr = "hr_admin_center@taokaenoi.co.th,nuchanart.ar@taokaenoi.co.th"; //ของจริง 

                string email = email_hr + "," + email_cemptest + "," + email_headtest + "," + email_cemp + "," + email_head; //ใช้ทดสอบ 
                //string email = email_cemp + "," + _email_qa; // ของจริง
                cbk_u0_document_detail _temp_u0 = _data_carbooking.cbk_u0_document_list[0];
                //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการจองห้องประชุม";
                _mail_subject = "[HR : CarBooking] - แจ้งพิจารณารายการจองรถ (เจ้าหน้าที่ธุรการบุคคล)";
                _mail_body = _serviceMail.SentMailCarBookingHRApprove(_data_carbooking.cbk_u0_document_list[0], link_system_carbooking);

                _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);

            }

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // save expenses by Hr Admin
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkAddUseExpensesCarHr(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 123); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // user cancel car
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkUserCancel(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 124); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // save expenses car out by Hr Admin
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkAddUseExpensesCarOutHr(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 125); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // save detail car ma
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkSaveDetailCarMa(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 130); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master place carbooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0Place(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master typecar carbooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0TypeCar(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 211); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master detail typecar carbooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0DetailTypeCar(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 212); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master detail province
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0Province(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 213); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master detail admin
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0Admin(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 214); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master coscenter
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0Costcenter(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 215); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0Type(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 216); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master Brand Car
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0BrandCar(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 217); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master Engine Car
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0EngineCar(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 218); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail in master Fuel Car
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0FuelCar(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 219); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail Car Master
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0DetailCar(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail Car Master View
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0DetailCarView(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 221); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail type booking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0TypeBooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 222); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail TypeTravel
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0TypeTravel(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 223); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail m0 car use
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkm0CarUse(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 224); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select m0 expenses
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkM0Expenses(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 230); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select Tax Room Detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkLogTaxRoomDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 231); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // check car booking in datetime car busy
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkCarBusy(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 232); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select topic ma
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkTopicMA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 233); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select show detail car in create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkSearchCarBusyDetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 234); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail car booking all
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkDetailCarbooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 240); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select view detail car booking all
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkViewDetailCarbooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 241); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select wait detail approve
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkWaitDetailApprove(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 242); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select crete expenses by hr
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkCreateExpensesHr(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 243); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select car in use booking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkDetailCarInBooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 244); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select view car in use booking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkViewDetailCarUseHr(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 245); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select view Expenses car in use booking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkViewDetailCarUseExpenses(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 246); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select view detail caruse outplan
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkViewDetailCarUseOutplan(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 247); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select choice detail out plan
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkChoiceOutplan(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 248); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select log view detail car booking all
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkLogViewDetailCarbooking(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 250); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select count wait detail approve
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkCountWaitApprove(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 251); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select decision Approve
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkDecisionApprove(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 252); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select search detail index
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchCbkDetailIndex(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 260); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select search detail Type Show Calendar
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchCbkDetailTypeCalendar(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 261); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select search wait approve
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSearchCbkDetailWaitApprove(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 262); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select status search document detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkStatusDocumentSearch(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 263); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select status search report table detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkSearchReportTable(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 270); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select status search report graph detail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkSearchReportGraph(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 271); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select ddl car register ma
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkddlCarregisterMA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 280); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detailtype and costcenter where m0_car_idx
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkDetailTypeCostMA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 281); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select detail ma
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkDetailCarMA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 282); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select view detail ma
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkViewDetailCarMA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 283); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select view Topic detail ma
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkViewTopicDetailCarMA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 284); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // select log detail car ma
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkLogDataioCarMA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 285); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // search detail report table car ma
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCbkSearchReportTableCarMA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 286); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //// sent mail alert tax car
    //[WebMethod]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public void GetCbkSentMailAlert_TaxCar(string jsonIn)
    //{
    //    if (jsonIn != null)
    //    {
    //        // convert to xml
    //        _xml_in = _funcTool.convertJsonToXml(jsonIn);

    //        data_carbooking _data_carbooking = new data_carbooking();
    //        _data_carbooking = (data_carbooking)_funcTool.convertXmlToObject(typeof(data_carbooking), _xml_in);

    //        // execute and return
    //        _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 290); // return w/ json

    //        // check return value then send mail
    //        _local_xml = _funcTool.convertJsonToXml(_ret_val);
    //        _data_carbooking = (data_carbooking)_funcTool.convertXmlToObject(typeof(data_carbooking), _local_xml);

    //        if (_data_carbooking.return_code == 0) //sent mail tax car
    //        {
    //            int count = _data_carbooking.cbk_m0_car_list.Length;
    //            int i = 0;
    //            foreach (var data in _data_carbooking.cbk_m0_car_list)
    //            {

    //                string email = mail_hr_taxcar; //ใช้ทดสอบ 
    //                                               //string email = email_cemp + "," + _email_qa; // ของจริง
    //                cbk_m0_car_detail _temp_u0 = _data_carbooking.cbk_m0_car_list[i];
    //                //_mail_subject = "[HR : RoomBooking ] - แจ้งรายการจองห้องประชุม";
    //                _mail_subject = "[HR : CarBooking] - แจ้งเตือนก่อนวันครบกำหนดเสียภาษีรถ";
    //                _mail_body = _serviceMail.SentMailCarMATax(_data_carbooking.cbk_m0_car_list[i], link_system_carma);

    //                _serviceMail.SendHtmlFormattedEmailFull(email, "", "", _mail_subject, _mail_body);
    //            }
    //        }


    //    }

    //    Context.Response.Clear();
    //    Context.Response.ContentType = "application/json; charset=utf-8";
    //    Context.Response.Write(_ret_val);
    //}

    // delete detail in master place carbooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkm0PlaceDel(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 910); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // delete detail carbooking
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkm0DetailCarDel(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 911); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // delete m0 Expenses
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkm0ExpensesDel(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 912); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // delete m0 topic name
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetCbkm0TopicMADel(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("masConn", "data_carbooking", "service_carbooking", _xml_in, 913); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);

    }
}
