using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_sap_attach")]
public class data_sap_attach
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public sap_attach_detail_u0[] sap_attach_list_u0 { get; set; }
    public search_sap_attach_detail[] search_sap_attach_list { get; set; }
}

[Serializable]
public class sap_attach_detail_u0
{
    public int u0_idx { get; set; }

    public int emp_idx { get; set; }
    public string pr_no { get; set; }
    public string file_name { get; set; }
    public string file_comment { get; set; }
    public int u0_status { get; set; }
    public string create_date { get; set; }
    public string update_date { get; set; }
}

[Serializable]
public class search_sap_attach_detail
{
    public string s_u0_idx { get; set; }

    public string s_emp_idx { get; set; }
    public string s_pr_nopr_no { get; set; }
    public string s_file_name { get; set; }
}