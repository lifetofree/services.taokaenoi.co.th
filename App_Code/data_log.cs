using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_log")]
public class data_log
{
    public int return_code { get; set; }
    public string return_msg { get; set; }
    public m0_event_type[] m0_event_list { get; set; }
    public u0_log_detail[] u0_log_list { get; set; }
    public search_key[] search_key_list { get; set; }
}

[Serializable]
public class m0_event_type
{
    public int midx { get; set; }
    public string event_type { get; set; }
}

[Serializable]
public class u0_log_detail
{
    public Int64 uidx { get; set; }
    public string log_detail { get; set; }
    public int m0_event_type { get; set; }
    public int emp_idx { get; set; }
    public string ip_address { get; set; }
    public string create_date { get; set; }

    public string event_type { get; set; }
    public string emp_code { get; set; }
    public string emp_name_th { get; set; }
}

[Serializable]
public class search_key
{
    public string s_uidx { get; set; } // 1,2,3
    public string s_log_detail { get; set; } // xxx,xxx,xxx *** convert < to &lt; > to &gt;
    public string s_m0_event_type { get; set; } // 1,2,3
    public string s_emp_idx { get; set; } // 1,2,3
    public string s_ip_address { get; set; } // xxx,xxx,xxx
    public string s_create_date { get; set; } // BETWEEN ''24/05/2017 00:00'' AND ''24/05/2017 23:59'', &lt; ''dd/MM/YYYY HH:ss'', &gt; ''dd/MM/YYYY HH:ss''

    public string s_emp_code { get; set; } // 56xxxxxx,57xxxxxx
    public string s_emp_name_th { get; set; } // xxx,xxx,xxx
}