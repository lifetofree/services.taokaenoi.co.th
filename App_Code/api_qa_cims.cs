﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;

/// <summary>
/// Summary description for api_qa_lab
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

public class api_qa_cims : System.Web.Services.WebService
{

    string _xml_in = "";
    string _ret_val = "";
    function_tool _funcTool = new function_tool();
    service_execute _serviceExec = new service_execute();
    data_qa_cims _data_qa_cims = new data_qa_cims();
    service_mail _serviceMail = new service_mail();

    //sent e - mail
    string _local_xml = "";
    string _mail_subject = "";
    string _mail_body = "";

    string document_code_mail = "";
    string test_details_mail = "";
    string details_mail = "";
    string create_date_mail = "";
    string _email_qa = "waraporn.teoy@gmail.com";//"qa_lab@taokaenoi.co.th";
    string link_system_cimse_devices = "http://dev.taokaenoi.co.th/cims-tool";

    public api_qa_cims()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    //insert,update delete master data form cal name m1
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetFormTableResult(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 192); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data form cal name m1
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetFormTableResult(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 292); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update delete master data form cal name
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetFormCalName(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 191); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update delete master data Location
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetLocation(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 194); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update delete master data zone
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetLocationZone(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 195); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data form cal name
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetFormCalName(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 291); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master Location Name
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLocationName(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 294); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //select master Location Zone Name
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetLocationZoneName(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 296); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master status devices
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetStatusDevices(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 297); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select per manage ddl
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPerManageQA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 298); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data form cal name show ddl
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetFormCalddlName(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 293); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Set Forn Signature
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetFormSignature(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 193); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select report count devices
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReportDevices(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 410); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select detail report to export
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetReportDetailExportDevices(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 420); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert permission qa
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetPermissionManageQA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 510); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select permission qa
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPermissionManageQA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 520); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select place permission qa
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPermissionManagePlaceQA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 521); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select Per permission qa
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPermissionManagePerQA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 522); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select View Edit Per permission qa
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetPermissionManagePerQAView(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 523); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //check permission manage qa register
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetCheckPermissionQA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 530); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select Del Per permission qa
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetDelPermissionManagePerQA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 590); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update delete master data signature
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetSignature(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 193); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //select master data signature
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetSignature(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 295); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //insert,update master data unit
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetUnit(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 110); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data unit
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetUnit(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data unit
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteUnit(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 910); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master data equipment_type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetEquipment_type(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 120); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data equipment_type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetEquipment_type(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data equipment_type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteEquipment_type(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 920); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master data Place
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetPlace(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 130); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data Place
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetPlace(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 230); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data Place
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeletePlace(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 930); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master data Cal_type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetCal_type(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 140); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data Cal_type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetCal_type(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 240); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data Cal_type
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteCal_type(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 940); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master data Equipment_result
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetEquipment_result(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 150); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data Equipment_result
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetEquipment_result(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 250); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data Equipment_result
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteEquipment_result(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 950); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master data Brand
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetBrand(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 160); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data Brand
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetBrand(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 260); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data Brand
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteBrand(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 960); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master data Equipment_name
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetEquipment_name(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 170); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data Equipment_name
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetEquipment_name(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 270); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data Equipment_name
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteEquipment_name(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 970); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master data Investigate
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetInvestigate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 180); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data Investigate
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetInvestigate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 280); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data Investigate
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteInvestigate(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 980); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master data Setname
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetSetname(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 190); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data Setname
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetSetname(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 290); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data Setname
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteSetname(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 990); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master data Formdetail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetFormdetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 111); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data Formdetail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetFormdetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 211); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data Formdetail
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteFormdetail(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 911); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master data m0 lab
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetLab(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 112); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data m0 lab
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetLab(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 212); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data m0 lab
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteLab(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 912); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master data m1 lab
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetLabM1(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 113); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data m1 lab
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetLabM1(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 213); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data m1 lab
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteLabM1(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 913); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master data r0 form create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetR0FormCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 114); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data r0 form create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetR0FormCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 214); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data r0 form create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteR0FormCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 914); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master data r1 form create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetR1FormCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 115); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select master data r1 form create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetR1FormCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 215); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete master data r1 form create
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteR1FormCreate(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 915); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //select Registration Device
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetRegistrationDevice(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 310); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Search Registration Device
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSearchRegistrationDevice(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 320); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Set Resolution In master
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetM0_Resolution(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 121); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Set Frequency In master
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetM0_Frequency(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 122); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Set Range Of Use Regis Devices
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetRangeUseDevice(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 124); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Set Measuring Regis Devices
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetMeasuringDevice(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 125); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Set Acceptance Regis Devices
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SetAcceptanceDevice(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 126); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Master Resolution
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetM0_Resolution(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 221); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Data Range Of Use Yo regis
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetRangeUseRegis(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 224); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Data Measuring to regis
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetmeasuringRegis(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 225); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Acceptance Criteria to regis
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetAcceptanceRegis(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 226); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Get Master Frequency
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void GetM0_Frequency(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 222); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Delete M0 Resolution
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DelM0_Resolution(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 921); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Delete M0 Frequency
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void DelM0_Frequency(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 922); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //select decision node 1
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetSelectDecisionNode1(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 210); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select search tool for ID No.
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetSelectSearchTool(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 510); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select node doc tranfer.
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetDecisionTranfer(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 410); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select node doc cutoff
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetDecisionCutoff(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 411); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select node doc QAcutoff
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetDecisionQACutoff(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 412); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert master m0 registration device
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetRegistrationDevice(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 116); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select mm1 registration device
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetM1RegistrationDevice(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 216); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert,update master m0 registration device
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetM1RegistrationDevice(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 117); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //delete m1 registration device
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsDeleteM1RegistrationDevice(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 916); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //update master m0 registration device
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetM0RegistrationDevice(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 118); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //insert doc u0 registration device
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetDocumentTranfer(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_qa_cims _data_qa_cims_sentmail = new data_qa_cims();
            _data_qa_cims_sentmail = (data_qa_cims)_funcTool.convertXmlToObject(typeof(data_qa_cims), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 110); // return w/ json

            ////// check return value then send mail
            ////_local_xml = _funcTool.convertJsonToXml(_ret_val);
            ////_data_qa_cims_sentmail = (data_qa_cims)_funcTool.convertXmlToObject(typeof(data_qa_cims), _local_xml);

            ////if (_data_qa_cims_sentmail.return_code == 0)
            ////{

            ////    string email_cemp_create = "";
            ////    string email_cemptest = "";
            ////    string email_headcreate_tranfer = "";
            ////    email_cemptest = "waraporn.teoy@gmail.com";
            ////    //check e-mail null
            ////    if (_data_qa_cims_sentmail.qa_cims_u0_document_device_list[0].emp_email_create != null)
            ////    {
            ////        email_cemp_create = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ
            ////        //email_cemp_create = _data_qa_cims_sentmail.qa_cims_u0_document_device_list[0].emp_email_create.ToString(); //ของจริง
            ////        //email_headcreate_tranfer = _data_qa_cims_sentmail.qa_cims_u0_document_device_list[0].head_email_tranfer.ToString(); //ของจริง
            ////    }

            ////    string replyempcreate = "sirinyamod@hotmail.com";
            ////    string email = email_cemp_create + "," + email_cemptest; //ใช้ทดสอบ
            ////    //string email = email_cemp_create + "," + email_headcreate_tranfer; // ของจริง
            ////    qa_cims_u0_document_device_detail _temp_u0 = _data_qa_cims_sentmail.qa_cims_u0_document_device_list[0];
            ////    _mail_subject = "[QA LAB : CIMS ] - แจ้งโอนย้ายเครื่องมือ";

            ////    _mail_body = _serviceMail.SentMailTranferDevices(_data_qa_cims_sentmail.qa_cims_u0_document_device_list[0], link_system_cimse_devices);

            ////    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            ////}
            ////else
            ////{

            ////}
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Update doc u0 registration device
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetDocumentUpdate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_qa_cims _data_qa_cims_qa_sentmail = new data_qa_cims();
            _data_qa_cims_qa_sentmail = (data_qa_cims)_funcTool.convertXmlToObject(typeof(data_qa_cims), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 120); // return w/ json

            ////// check return value then send mail
            ////_local_xml = _funcTool.convertJsonToXml(_ret_val);
            ////_data_qa_cims_qa_sentmail = (data_qa_cims)_funcTool.convertXmlToObject(typeof(data_qa_cims), _local_xml);

            ////if (_data_qa_cims_qa_sentmail.return_code == 0)
            ////{

            ////    string email_cemp_create = "";
            ////    string email_cemptest = "";
            ////    string email_headcreate_tranfer = "";
            ////    email_cemptest = "waraporn.teoy@gmail.com";
            ////    //check e-mail null
            ////    if (_data_qa_cims_qa_sentmail.qa_cims_u0_document_device_list[0].emp_email_create != null)
            ////    {
            ////        email_cemp_create = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ
            ////        //email_cemp_create = _data_qa_cims_sentmail.qa_cims_u0_document_device_list[0].emp_email_create.ToString(); //ของจริง
            ////        //email_headcreate_tranfer = _data_qa_cims_sentmail.qa_cims_u0_document_device_list[0].head_email_tranfer.ToString(); //ของจริง
            ////    }

            ////    string replyempcreate = "sirinyamod@hotmail.com";
            ////    string email = email_cemp_create + "," + email_cemptest; //ใช้ทดสอบ
            ////    //string email = email_cemp_create + "," + email_headcreate_tranfer; // ของจริง
            ////    qa_cims_u0_document_device_detail _temp_u0 = _data_qa_cims_qa_sentmail.qa_cims_u0_document_device_list[0];
            ////    _mail_subject = "[QA LAB : CIMS ] - พิจารณารายการโอนย้าย(เจ้าหน้าที่ห้องปฏิบัติการ)";

            ////    _mail_body = _serviceMail.SentMailTranferQAApprove(_data_qa_cims_qa_sentmail.qa_cims_u0_document_device_list[0], link_system_cimse_devices);

            ////    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            ////}
            ////else
            ////{

            ////}
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select doc u1 registration device
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetDocEquipmentList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 310); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select log registration device
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetLogRegistrationDevice(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims_master", _xml_in, 217); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select list document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetEquipmentList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 310); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select list decision HeadSection
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetDecisionHeadSection(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 413); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Update doc u0 registration device Head Section
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetDocumentUpdateHeadSection(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_qa_cims _data_qa_cims_sentmailhead = new data_qa_cims();
            _data_qa_cims_sentmailhead = (data_qa_cims)_funcTool.convertXmlToObject(typeof(data_qa_cims), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 130); // return w/ json

            ////// check return value then send mail
            ////_local_xml = _funcTool.convertJsonToXml(_ret_val);
            ////_data_qa_cims_sentmailhead = (data_qa_cims)_funcTool.convertXmlToObject(typeof(data_qa_cims), _local_xml);

            ////if (_data_qa_cims_sentmailhead.return_code == 0)
            ////{

            ////    string email_cemp_create = "";
            ////    string email_cemptest = "";
            ////    string email_qa_lab = "qa_lab@taokaenoi.co.th ";
            ////    string email_tranfer = "";
            ////    email_cemptest = "waraporn.teoy@gmail.com";
            ////    //check e-mail null
            ////    if (_data_qa_cims_sentmailhead.qa_cims_u0_document_device_list[0].emp_email_create != null)
            ////    {
            ////        email_cemp_create = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ
            ////        //email_cemp_create = _data_qa_cims_sentmail.qa_cims_u0_document_device_list[0].emp_email_create.ToString(); //ของจริง
            ////        //email_tranfer = _data_qa_cims_sentmail.qa_cims_u0_document_device_list[0].email_tranfer.ToString(); //ของจริง
            ////    }

            ////    string replyempcreate = "sirinyamod@hotmail.com";
            ////    string email = email_cemp_create + "," + email_cemptest; //ใช้ทดสอบ
            ////    //string email = email_cemp_create + "," + email_qa_lab + "," + email_tranfer; // ของจริง
            ////    qa_cims_u0_document_device_detail _temp_u0 = _data_qa_cims_sentmailhead.qa_cims_u0_document_device_list[0];
            ////    _mail_subject = "[QA LAB : CIMS ] - พิจารณารายการโอนย้าย(หัวหน้าผู้สร้าง)";

            ////    _mail_body = _serviceMail.SentMailTranferHeadCreateApprove(_data_qa_cims_sentmailhead.qa_cims_u0_document_device_list[0], link_system_cimse_devices);

            ////    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            ////}
            ////else
            ////{

            ////}

        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select list decision HeadQA
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetDecisionHeadQA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 414); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Update doc u0 registration device Head QA
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetDocumentUpdateHeadQA(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);

            data_qa_cims _data_qa_cims_headqa_sentmail = new data_qa_cims();
            _data_qa_cims_headqa_sentmail = (data_qa_cims)_funcTool.convertXmlToObject(typeof(data_qa_cims), _xml_in);

            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 140); // return w/ json

            ////// check return value then send mail
            ////_local_xml = _funcTool.convertJsonToXml(_ret_val);
            ////_data_qa_cims_headqa_sentmail = (data_qa_cims)_funcTool.convertXmlToObject(typeof(data_qa_cims), _local_xml);

            ////if (_data_qa_cims_headqa_sentmail.return_code == 0)
            ////{

            ////    string email_cemp_create = "";
            ////    string email_cemptest = "";
            ////    string email_qa_tranfer = "qa_lab@taokaenoi.co.th ";
            ////    string email_headcreate_tranfer = "";
            ////    email_cemptest = "waraporn.teoy@gmail.com";
            ////    //check e-mail null
            ////    if (_data_qa_cims_headqa_sentmail.qa_cims_u0_document_device_list[0].emp_email_create != null)
            ////    {
            ////        email_cemp_create = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ
            ////        //email_cemp_create = _data_qa_cims_sentmail.qa_cims_u0_document_device_list[0].emp_email_create.ToString(); //ของจริง
            ////        //email_headcreate_tranfer = _data_qa_cims_sentmail.qa_cims_u0_document_device_list[0].head_email_tranfer.ToString(); //ของจริง
            ////    }

            ////    string replyempcreate = "sirinyamod@hotmail.com";
            ////    string email = email_cemp_create + "," + email_cemptest; //ใช้ทดสอบ
            ////    //string email = email_cemp_create + "," + email_headcreate_tranfer; // ของจริง
            ////    qa_cims_u0_document_device_detail _temp_u0 = _data_qa_cims_headqa_sentmail.qa_cims_u0_document_device_list[0];
            ////    _mail_subject = "[QA LAB : CIMS ] - พิจารณารายการโอนย้าย(เจ้าหน้าที่ห้องปฏิบัติการ)";

            ////    _mail_body = _serviceMail.SentMailTranferHeadQA(_data_qa_cims_headqa_sentmail.qa_cims_u0_document_device_list[0], link_system_cimse_devices);

            ////    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            ////}
            ////else if(_data_qa_cims_headqa_sentmail.return_code == 2) //receive tranfer 
            ////{
            ////    string email_cemp_create = "";
            ////    string email_cemptest = "";
            ////    string email_headcreate_tranfer = "";
            ////    email_cemptest = "waraporn.teoy@gmail.com";
            ////    //check e-mail null
            ////    if (_data_qa_cims_headqa_sentmail.qa_cims_u0_document_device_list[0].emp_email_create != null)
            ////    {
            ////        email_cemp_create = "waraporn.teoy@gmail.com"; //ใช้ทดสอบ
            ////        //email_cemp_create = _data_qa_cims_sentmail.qa_cims_u0_document_device_list[0].emp_email_create.ToString(); //ของจริง
            ////        //email_headcreate_tranfer = _data_qa_cims_sentmail.qa_cims_u0_document_device_list[0].head_email_tranfer.ToString(); //ของจริง
            ////    }

            ////    string replyempcreate = "sirinyamod@hotmail.com";
            ////    string email = email_cemp_create + "," + email_cemptest; //ใช้ทดสอบ
            ////    //string email = email_cemp_create + "," + email_headcreate_tranfer; // ของจริง
            ////    qa_cims_u0_document_device_detail _temp_u0 = _data_qa_cims_headqa_sentmail.qa_cims_u0_document_device_list[0];
            ////    _mail_subject = "[QA LAB : CIMS ] - พิจารณารายการโอนย้าย";

            ////    _mail_body = _serviceMail.SentMailTranferRecive(_data_qa_cims_headqa_sentmail.qa_cims_u0_document_device_list[0], link_system_cimse_devices);

            ////    _serviceMail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);
            ////}
            ////else
            ////{

            ////}
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select node doc QAtranfer
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetDecisionQATranfer(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 415); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //create document cal.
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetDocumentCalibration(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 610); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select document cal.
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetDocumentCalibration(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 220); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select details document cal.
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetDocumentDetails(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 221); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select details document cal.
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetCalibrationList(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 222); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select log document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetLogEquipmentList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 320); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select list decision Receiver
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetDecisionReceiver(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 416); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select list detail Creator
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetDetailCreator(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 417); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //Update doc u0 registration device Receiver
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetDocumentUpdateReceiver(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 150); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select lab document cal.
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetLabCalibrationInLab(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 223); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    // lab accept document cal.
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetAcceptCalibrationInLab(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 630); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //select lab  document cal.
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetLabCalibration(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 620); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    } 

    //Search Document
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SearchInDocumentDevices(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 710); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select search tool form
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetSelectSearchToolForm(string jsonIn)
    {
        if (jsonIn != null)
        {
            //convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 520); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //select u1 document into form record result
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetFormRecordList(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 330); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //select u1 document into form record result Header
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetFormRecordListHeader(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 340); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //select search form topics
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetSearchFormTopics(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 521); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //select decision node
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetSelectDecisionNode(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 211); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //set Record Calibration
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetRecordCalibration(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 160); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //get list doc in page supervisor 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetDocCalibrationSupervisor(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 224); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //get view result
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetResultCalibrationView(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 230); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //get view result
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetResultCalibrationTopicsView(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 231); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }

    //supervisor approve
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetApproveSupervisor(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 170); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //get view equipment document 1:1
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsGetViewDetails(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 232); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }


    //set record certificate 
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void CimsSetCertificate(string jsonIn)
    {
        if (jsonIn != null)
        {
            // convert to xml
            _xml_in = _funcTool.convertJsonToXml(jsonIn);
            // execute and return
            _ret_val = _serviceExec.actionExec("qaConn", "data_qa_cims", "service_qa_cims", _xml_in, 640); // return w/ json
        }

        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(_ret_val);
    }
}
