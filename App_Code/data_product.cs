using System;
using System.Xml.Serialization;

public class data_product
{
    public int return_code { get; set; }
    public string return_msg { get; set; }
    public M0_ProductType_detail[] M0_ProductType_list { get; set; }
    public M0_ProductList_detail[] M0_ProductList_list { get; set; }
}

public class M0_ProductType_detail
{
    public int ProTIDX { get; set; }
    public string ProTName { get; set; }
    public int ProTStatus { get; set; }

}

public class M0_ProductList_detail
{
    public int ProLIDX { get; set; }
    public int ProTIDX { get; set; }
    public string ProLName { get; set; }
    public int ProLWeight { get; set; }
    public string ProLFlavor { get; set; }
    public int ProLStatus { get; set; }

    public string ProTName { get; set; }

}
