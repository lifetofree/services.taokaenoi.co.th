﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_hr_manpower")]
public class data_hr_manpower
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }
    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }

    [XmlElement("Boxu0_DocumentDetail")]
    public U0_DocumentDetail[] Boxu0_DocumentDetail { get; set; }
    public U1_DocumentDetail[] Boxu1_DocumentDetail { get; set; }
    public U2_DocumentDetail[] Boxu2_DocumentDetail { get; set; }

}

[Serializable]
public class U0_DocumentDetail
{
    [XmlElement("sexidx")]
    public int sexidx { get; set; }

    [XmlElement("type_select")]
    public int type_select { get; set; }

    [XmlElement("sex_name")]
    public string sex_name { get; set; }

    [XmlElement("m0_hridx")]
    public int m0_hridx { get; set; }

    [XmlElement("hire_name")]
    public string hire_name { get; set; }

    [XmlElement("m0_thidx")]
    public int m0_thidx { get; set; }

    [XmlElement("typehire_name")]
    public string typehire_name { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("eduidx")]
    public int eduidx { get; set; }

    [XmlElement("education_name")]
    public string education_name { get; set; }

    [XmlElement("natidx")]
    public string natidx { get; set; }

    [XmlElement("nationality_name")]
    public string nationality_name { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("u0_docidx")]
    public int u0_docidx { get; set; }

    [XmlElement("orgidx")]
    public int orgidx { get; set; }

    [XmlElement("rdeptidx")]
    public int rdeptidx { get; set; }

    [XmlElement("rsecidx")]
    public int rsecidx { get; set; }

    [XmlElement("rposidx")]
    public int rposidx { get; set; }

    [XmlElement("doc_code")]
    public string doc_code { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("EmpTypeIDX")]
    public int EmpTypeIDX { get; set; }

    [XmlElement("EmpTypeName")]
    public string EmpTypeName { get; set; }

    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }

    [XmlElement("OrgNameTH_Req")]
    public string OrgNameTH_Req { get; set; }

    [XmlElement("DeptNameTH_Req")]
    public string DeptNameTH_Req { get; set; }

    [XmlElement("SecNameTH_Req")]
    public string SecNameTH_Req { get; set; }

    [XmlElement("PosNameTH_Req")]
    public string PosNameTH_Req { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("createdate")]
    public string createdate { get; set; }

    [XmlElement("enddate")]
    public string enddate { get; set; }


    [XmlElement("JobGradeIDX")]
    public int JobGradeIDX { get; set; }

    [XmlElement("u1_docidx")]
    public int u1_docidx { get; set; }

    [XmlElement("date_limit")]
    public int date_limit { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }

    [XmlElement("m0_typeidx")]
    public int m0_typeidx { get; set; }

    [XmlElement("m0type_listidx")]
    public int m0type_listidx { get; set; }

    [XmlElement("doc_refidx")]
    public int doc_refidx { get; set; }

    [XmlElement("reason_comment")]
    public string reason_comment { get; set; }

    [XmlElement("type_man")]
    public string type_man { get; set; }

    [XmlElement("type_list")]
    public string type_list { get; set; }

    [XmlElement("doc_code_ref")]
    public string doc_code_ref { get; set; }

    [XmlElement("Email")]
    public string Email { get; set; }

    [XmlElement("Email_Admin")]
    public string Email_Admin { get; set; }

    [XmlElement("noidx")]
    public int noidx { get; set; }

    [XmlElement("node_desc")]
    public string node_desc { get; set; }

    [XmlElement("IFSearchbetween")]
    public int IFSearchbetween { get; set; }

    [XmlElement("qty")]
    public int qty { get; set; }

    [XmlElement("date_receive")]
    public string date_receive { get; set; }

    [XmlElement("DG_Month")]
    public string DG_Month { get; set; }

    [XmlElement("DG_Year")]
    public string DG_Year { get; set; }

    [XmlElement("CheckRow")]
    public int CheckRow { get; set; }

}

/*
[Serializable]
public class U0_DocumentDetail
{
    public int sexidx { get; set; }
    public int type_select { get; set; }
    public string sex_name { get; set; }
    public int m0_hridx { get; set; }
    public string hire_name { get; set; }
    public int m0_thidx { get; set; }
    public string typehire_name { get; set; }
    public int condition { get; set; }
    public int eduidx { get; set; }
    public string education_name { get; set; }
    public string natidx { get; set; }
    public string nationality_name { get; set; }
    public int CEmpIDX { get; set; }
    public int u0_docidx { get; set; }
    public int orgidx { get; set; }
    public int rdeptidx { get; set; }
    public int rsecidx { get; set; }
    public int rposidx { get; set; }
    public string doc_code { get; set; }
    public int unidx { get; set; }
    public int acidx { get; set; }
    public int staidx { get; set; }
    public int doc_decision { get; set; }
    public int EmpTypeIDX { get; set; }
    public string EmpTypeName { get; set; }

    public string StatusDoc { get; set; }
    public string OrgNameTH { get; set; }
    public string DeptNameTH { get; set; }
    public string SecNameTH { get; set; }
    public string PosNameTH { get; set; }

    public string FullNameTH { get; set; }
    public string createdate { get; set; }
    public int JobGradeIDX { get; set; }
    public int u1_docidx { get; set; }
    public int date_limit { get; set; }
    public string comment { get; set; }
    public string status_name { get; set; }
    public string node_name { get; set; }
    public string actor_name { get; set; }

    public int LocIDX { get; set; }
    public string LocName { get; set; }

    public int m0_typeidx { get; set; }
    public int m0type_listidx { get; set; }
    public int doc_refidx { get; set; }
    public string reason_comment { get; set; }
    public string type_man { get; set; }
    public string type_list { get; set; }
    public string doc_code_ref { get; set; }
    public string Email { get; set; }
    public string Email_Admin { get; set; }


}


*/

[Serializable]
public class U1_DocumentDetail
{
    public int u1_docidx { get; set; }
    public int u0_docidx { get; set; }
    public int orgidx { get; set; }
    public int rdeptidx { get; set; }
    public int rsecidx { get; set; }
    public int posidx { get; set; }
    public int emp_typeidx { get; set; }
    public int m0_hridx { get; set; }
    public string hire_name { get; set; }
    public int m0_thidx { get; set; }
    public string typehire_name { get; set; }
    public int eduidx { get; set; }
    public string education_name { get; set; }
    public string experience { get; set; }
    public string language { get; set; }
    public int qty { get; set; }
    public string other { get; set; }
    public string date_receive { get; set; }
    public int CempIDX { get; set; }
    public string EmpTypeName { get; set; }
    public string OrgNameTH { get; set; }
    public string DeptNameTH { get; set; }
    public string SecNameTH { get; set; }
    public string PosNameTH { get; set; }
    public int date_limit { get; set; }
    public string doc_code { get; set; }
    public int unidx { get; set; }
    public int acidx { get; set; }
    public int staidx { get; set; }
    public int LocIDX { get; set; }
    public string LocName { get; set; }
    public int m0_typeidx { get; set; }
    public int m0type_listidx { get; set; }
    public int doc_refidx { get; set; }
    public string reason_comment { get; set; }
    public string type_man { get; set; }
    public string type_list { get; set; }
    public string createdate { get; set; }
    public string Approve1 { get; set; }
    public string Approve2 { get; set; }
    public string Approve3 { get; set; }
    public string ApproveDate1 { get; set; }
    public string ApproveDate2 { get; set; }
    public string ApproveDate3 { get; set; }
    public string CostNo { get; set; }

}

[Serializable]
public class U2_DocumentDetail
{
    public int sexidx { get; set; }
    public string sex_name { get; set; }
    public string natidx { get; set; }
    public string nationality_name { get; set; }
    public int u2_docidx { get; set; }
    public int u1_docidx { get; set; }
    public int sub_qty { get; set; }
    public int CheckRow_sex { get; set; }

}