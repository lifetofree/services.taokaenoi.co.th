﻿using System;
using System.Xml.Serialization;
/// <summary>
/// Summary description for Class1
/// </summary>
/// 
[Serializable]
[XmlRoot("data_carbooking")]
public class data_carbookingtest
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    public cbk_m0_place_detailtest[] cbk_m0_place_listtest { get; set; }
}

public class cbk_m0_place_detailtest
{
    public int place_idx { get; set; }
    public string place_name { get; set; }
    public string place_code { get; set; }

    public string create_date { get; set; }
    public string update_date { get; set; }
    public int status { get; set; }
    public int condition { get; set; }
}
