﻿using System;
using System.Xml.Serialization;


public class data_system
{
    public int return_code { get; set; }
    public string return_msg { get; set; }

    public int return_rdept_idx { get; set; }
    public int return_rsec_idx { get; set; }

    public menusystem_detail[] menusystem_list { get; set; }

    public empmenusystem_detail[] empmenusystem_list { get; set; }

}

public class menusystem_detail
{
    
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }

    public int menu_idx { get; set; }   
    public string menu_name_th { get; set; }    
    public string menu_name_thlist { get; set; }   
    public string menu_name_thtest { get; set; }  
    public string menu_name_en { get; set; }  
    public string menu_url { get; set; }   
    public int menu_relation { get; set; }   
    public int menu_order { get; set; }    
    public string menu_icon { get; set; }  
    public int cemp_idx { get; set; }  
    public string create_date { get; set; }   
    public string update_date { get; set; }  
    public int menu_status { get; set; }   
    public int menu_level { get; set; }
  
    public int system_idx { get; set; }
    public string system_name_th { get; set; }
    public int MenuIDX1 { get; set; }

}

public class empmenusystem_detail
{
    public int emp_idx { get; set; }

    // employee organization profile
    public int org_idx { get; set; }
    public int rdept_idx { get; set; }
    public int rsec_idx { get; set; }
    public int rpos_idx { get; set; }

    public string org_name_en { get; set; }
    public string dept_name_en { get; set; }
    public string sec_name_en { get; set; }
    public string pos_name_en { get; set; }

    public string org_name_th { get; set; }
    public string dept_name_th { get; set; }
    public string sec_name_th { get; set; }
    public string pos_name_th { get; set; }

   

}



