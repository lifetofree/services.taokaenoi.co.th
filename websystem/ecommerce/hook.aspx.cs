using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_ecommerce_hook : System.Web.UI.Page {

    protected void Page_Load (object sender, EventArgs e) {
        string[] keys = Request.Form.AllKeys;
        for (int i = 0; i < keys.Length; i++) {
            Response.Write (keys[i] + ": " + Request.Form[keys[i]] + "<br>");
        }
    }
}